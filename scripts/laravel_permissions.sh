#!/bin/bash

# Setup the various file and folder permissions for Laravel
chown -R apache:apache /var/www/html/
find /var/www/html -type d -exec chmod 775 {} +
find /var/www/html/public -type d -exec chmod 777 {} +
find /var/www/html -type f -exec chmod 664 {} +
find /var/www/html/storage/ -type d -exec chmod 777 {} +
find /var/www/public/ -type f -exec chmod 777 {} +
find /var/www/storage/ -type f -exec chmod 777 {} +
chgrp -R apache /var/www/html/storage /var/www/html/bootstrap/cache
chmod -R ug+rwx /var/www/html/storage /var/www/html/bootstrap/cache

echo yes | yum install php71-mbstring
# Clear any previous cached views and optimize the application
php /var/www/html/artisan cache:clear
php /var/www/html/artisan view:clear
php /var/www/html/artisan config:cache
php /var/www/html/artisan optimize
php /var/www/html/artisan route:cache