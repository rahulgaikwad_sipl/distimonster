#!/bin/bash

# Stop web server
httpd_running=`pgrep httpd`
if [[ -n  $httpd_running ]]; then
    service httpd stop
fi

# Remove existing html pages
if [ -d /var/www/html ]; then
    rm -rf /var/www/html
fi
mkdir -vp /var/www/html