<?php

return [
    'TITLE_ADMIN_LOGIN' => 'DistiMonster :: Login',
    'REQUIRED_PASSWORD' => 'Please enter the password',
    'REQUIRED_CONFIRM_PASSWORD' => 'Please enter the password again',
    'REQUIRED_EMAIL' => 'Please enter your email address',
    'VALID_EMAIL_ADDRESS' => 'Please enter your valid email address',
    'ALREADY_EMAIL_ADDRESS' => 'The given email is already used before. Please use another.',
    'SITE_NAME' => 'DistiMonster',
    'INVALID_URL' => 'Invalid URL',
    'COMMON_ERROR' => 'Oops. There is a problem. Please try again later.',
    'AJAX_REQUEST_ERROR' => 'Invalid URL',
    'REQUIRED' => 'The values are required.',
    'INVALID_EMAIL_ADDRESS' => 'Invalid email address.',
    'LOGIN_SUCCESS' => 'You are now logged in successfully.',
    'INVALID_EMAIL_OR_PASSWORD' => 'The email or password you entered is incorrect.',
    'SUBSCRIPTION_EXPIRED' => 'Sorry, your subscription is expired, please contact with site Administrator.',
    'ERROR_FORGOT_PASSWORD' => 'It seems the given email address does not exist.',
    'DEACTIVE_ERROR_FORGOT_PASSWORD' => 'It seems the given email address currently deactivated by admin.',
    'FORGOT_SUCCESS' => 'Your temporary password has been emailed to you. Please check your email.',
    'USER_ACCOUNT_DEACTIVATED' => 'Your account has been deactivated. Please contact the team at DistiMonster',
    'SUCCESSFULLY_REGISTER' => 'Congratulations, your account has been created and an email with details has been sent to your registered email address.',
    'FAILED_REGISTER'=>'Oops. There is a problem in registration. Please try again later.',
    'ALERT_ACCOUNT_ALREADY_ACTIVATED' => 'It seems your account is already activated!',
    'SUCCESS_ACCOUNT_ACTIVATED' => 'Your account has been activated successfully! Now you can search and place orders easily.',
    'ALERT_INVALID_KEY' => 'Invalid key has been provided.',
    'SUCCESS_FORGOT_MSG' => 'Reset password link has been sent to your registered email address.',
    'ERROR_INVALID_FORGOT_PASSWORD_TOKEN' => 'It seems you have given an invalid token.',
    'SUCCESS_PASSWORD_CHANGED' => 'Your password has been changed successfully.',
    'PROFILE_SUCCESSFULLY_UPDATED' => 'Your profile has been updated successfully.',
    'REQUIRED_YOUR_NAME' => 'Please enter your name',
    'REQUIRED_BUSINESS_NAME' => 'Please enter your business name',
    'REQUIRED_SUB_TITLE' => 'Please enter a sub title',
    'REQUIRED_DESCRIPTION' => 'Please enter your description. Maximum 500 characters',
    'REQUIRED_CITY' => 'Please enter a city',
    'REQUIRED_COUNTRY' => 'Please select a country',
    'NO_RECORDS_FOUND' => 'Oops. No records found!',
    'SETTING_SUCCESS' => 'Your account settings has been updated successfully.',
    'WELCOME_USER_SUBJECT'=>'Welcome to DistiMonster.',
    'CRON_PAYMENT_SUCCESS'=>'Distimonster - Subscription Payment Successful.',
    'CRON_PAYMENT_FAILURE'=>'Distimonster - Subscription Payment Failed.',
    'CRON_PAYMENT_RENEW_SUCCESS'=>'Distimonster - Subscription Renewal Payment Successful.',
    'TRIAL_EXPIRY_SOON'=>'Distimonster - Trial Period Expiry.',
    'SUBSCRIPTION_EXPIRY_SOON'=>'Distimonster - Subscription Expiry Soon.',
    'RESET_PASSWORD_SUBJECT'=>'DistiMonster: Reset Password',
    'RESET_PASSWORD_LINK_SUCCESS'=>'A password reset link has been sent on your provided email successfully.',
    'PASSWORD_RESET_LINK_EXPIRED'=>'Your password reset link is expired now. Request a new password reset link.',
    'RESET_PASSWORD_SUCCESS'=>'Your password has been reset successfully, You can login now.',
    'INCORRECT_CURRENT_PASSWORD'=>'Your current password is not correct.',
    'EMAIL_CHANGE_SUCCESS'=>'Your email has been changed successfully.',
    'EMAIL_PASS_CHANGE_SUCCESS'=>'Your email & password has been changed successfully.',
    'PASSWORD_CHANGE_SUCCESS'=>'Your password has been changed successfully.',
    'PROFILE_UPDATE_SUCCESS'=>'Your profile has been updated successfully.',
    'LOGOUT_SUCCESS'=>'You have been logged out successfully.',
    'PAYPAL_ORDER_ERROR'=>'Currently there is some error in processing your order with PayPal.Please try again later.',
    'NET_TERM_ORDER'=>'Currently there is some error in processing your order with Net Terms Account .Please try again later.',
    'CASH_ON_DELIVERY'=>'Currently there is some error in processing your order with Cash On Delivery.Please try again later.',
    'ORDER_SUCCESS'=>'Your order is completed successfully.',
    'SUCCESS_NET_TERM'=>'Thanks for registering with a Net Terms Account. Your account status is in pending review stage and you will be notified soon.',
    'DUPLICATE_NET_TERM_REQUEST'=>'Sorry your request for a net term account is already booked with us.',
    'PAYPAL_PAYMENT_FAILED_ERROR'=>'Sorry payment is failed. Please try again later.',
    'PAYPAL_PAYMENT_SUCCESS_ORDER_ERROR'=>'We have received payment successfully but there is some error in order processing use below order id to contact us.',
    'PAYPAL_PAYMENT_FAILED'=>'Sorry payment is failed. Please try again later.',
    'LOGIN_REQUIRED_TO_QUOTE'=>'Please login to submit this quote.',
    'QUOTE_NOT_EXIST'=>'Quote ID does not exist in our record.',
    'BOM_NOT_EXIST'=>'BOM ID does not exist in our record.',
    'QUOTE_DELETE_SUCCESS'=>'Quote deleted successfully.',
    'BOM_SHARED_SUCCESS'=>'BOM shared successfully.',
    'BOM_SHARED_FAILED'=>'Sorry, unable to share BOM with selected user.',
    'ITEM_ADDED_IN_CART_SUCCESS'=>'Item quantity added in cart successfully.',
    'ITEM_ALREADY_EXIST_IN_CART'=>'This item already exist in cart.',
    'ITEM_OUT_OF_STOCK'=>'Quantity does not exist in stock. Click below link to check from other sources.',
    'ITEM_ADD_TO_CART_SUCCESS'=>'Item added in cart successfully.',
    'NEWSLETTER_SUBSCRIPTION_SUBJECT'=>'DistiMonster: Newsletter subscription completed successfully.',
    'NEWSLETTER_SUBSCRIPTION_SUCCESS'=>'You have signup newsletter successfully.',
    'NEWSLETTER_SUBSCRIPTION_ALREADY'=>'This email is already exist in our list. Please try with different email address.',
    'ALREADY_LOGGEDIN'=>'Oops! You are already loggedin.',
    'EMAIL_EXIST'=>'Available',
    'EMAIL_NOT_EXIST'=>'Not Available',
    'BOM_UPLOAD_LOGIN_ERROR'=>'Please login to upload BOM.',
    'BOM_USE_ONLY_ONE_FIELD'=>'Please select only one method to upload BOM.',
    'BOM_USE_ONE_FILED'=>'Please select any one field to upload bom.',
    'BOM_INPUT_FORMAT_IMPROPER'=>'Parts are not found in proper format to upload BOM.',
    'BOM_MAX_PARTS'=>'Only 5 part number is accepted, upload csv or xlsx for more parts.',
    'SUBJECT_MAIL_BOM_TO_ADMIN'=>'New BOM uploaded from a user',
    'SUBJECT_MAIL_BOM_TO_USER'=>'Bom submitted successfully to DistiMonster.',
    'BOM_REQUIRED_FILES'=>'\'Only CSV/Xlsx file formats are supported.',
    'BOM_SUCCESS_DELETE'=>'Bom deleted successfully.',
    'BOM_NO_PART_FOUND'=>'Sorry no part found in bom.',
    'ITEM_SUCCESS_CART'=>'Item added successfully in the cart.',
    'PROMOCODE_EXPIRED'=>'Promo code is expired now.',
    'FREE_SHIPPING_CODE'=>'Free shipping code is applied successfully. Now your shipping will be free for this order.',
    'PROMOCODE_APPLY_SUCCESS'=>'Promo code applied successfully.',
    'NO_PROMO_CODE_MATCHED'=>'No Promo code matched.',
    'PROMO_CODE_NOT_FOUND'=>'No Promo code found.',
    'PROMOCODE_REMOVED'=>'Promo code removed successfully.',
    'NEWS_LETTER_UNSUBSCRIBE_SUCCESS'=>'Unsubscribe successfully from Newsletter list.',
    'SUBSCRIBE_EMAIL_NOT_FOUND'=>'Email not found.',
    'QUOTE_ITEM_SUCCESS'=>'Item added successfully to the Quote Request.',
    'QUOTE_ITEM_UPDATE_SUCCESS'=>'Item updated successfully to the Quote Request.',
    'ITEM_REMOVE_QUOTE_SUCCESS'=>'Item is removed successfully.',
    'QUOTE_LIST_EMPTY_SUCCESS'=>'Removed all item successfully.',
    'SUCCESS_QUOTE_SAVED'=>'Successfully saved your quote.',
    'SUCCESS_QUOTE_ADD_TO_CART'=>'Item added successfully to the cart.',
    'SUCCESS_QUOTE_UPDATE_TO_CART'=>'Item updates successfully in the cart.',
    'ERROR_QUOTE_ADD_TO_CART'=>'Sorry unable to add quote items to cart because some items are not in stock.',
    'CHECKOUT_CART_EMPTY'=>'There is no item in cart to check out.',
    'CHECKOUT_LOGIN_ERROR'=>'Please login to checkout from your cart.',
    'CHECKOUT_EMPTY_SHIPPING_ADD'=>'Please select shipping address.',
    'SHIPPING_ADD_SAVE_SUCCESS'=>'Address added successfully.',
    'SHIPPING_ADD_SAVE_ERROR'=>'Oops.There is some error to add new address.',
    'SHIPPING_UPDATE_SAVE_SUCCESS'=>'Address updated successfully.',
    'SHIPPING_UPDATE_SAVE_ERROR'=>'Oops.There is some error to update address.',
    'NEW_PART_MAIL_SUBJECT'=>'A new Product request received',
    'NEW_PART_MAIL_SUBJECT_USER'=>'A new Product request received for user',
    'NEW_PART_REQUEST_SUCCESS'=>'Your quote has been sent successfully. We will notify you as soon as it is processed.',
    'CONTACT_NUMBER_MIN_MESSAGE'=>'Contact Number consists of at least 9 characters',
    'CONTACT_NUMBER_MAX_MESSAGE'=>'Contact Number should not be exceed 15 characters',
    'NEW_PART_REQUEST_NOT_LOGIN'=>'Please login to submit this quote.',
    'REQUEST_A_DEMO_SUBJECT'=>'DistiMonster: Request for a live demo',
    'REQUEST_A_DEMO_SUCCESS'=>'Thanks for your live demo request a member of our team will contact you soon.',
    'ADMIN_MAIL'=>'tonny.delson@gmail.com',
    'ARROW_API_URL'=>'http://api.arrow.com/itemservice/v3/en/',
    'ORBWEAVER_API_URL'=>'https://app.orbweaver.com/api/v3/',
    'ARROW_LOGIN_NAME'=>'stock-point',
    'ARROW_API_KEY'=>'572e0bb1535403b94afe3ade26db8b2000157b7891786642ad7838bd98d85272',
    'PRICE_INCREMENT'=>0,
    'CONSOLIDATED_SHIPPING_CHARGE'=> 25,
    'EXTRA_CREDIT_CARD_CHARGE'=> 3,
    'APP_CONSTANT'=>[
        'PAYMENT_STATUS'=>[
            'pending'   =>0,
            'done'      =>1,
            'failed'    =>2,
            'refunded'  =>3,
        ],
        'PAYMENT_STATUS_NAME'=>[
            'pending'   =>'Pending',
            'done'      =>'Done',
            'failed'    =>'Failed',
            'refunded'  =>'Refunded',
        ],
        'ORDER_SHIPPING_STATUS'=>[
            'shipped'=>'Shipped',
            'pending'=>'Pending',
            'intransit'=> 'In Transit',
            'delivered'=> 'Delivered',
            'partially_shipped'=> 'Partially Shipped'
        ],
        'QUOTE_STATUS'=>[
            'Pending'=>0,
            'Approved'=>1,
            'Rejected'=>2,
            'Expired'=>3,
            'Ordered'=>4
        ],
    ],
    'PAY_PAL_PRO'=>[
        'API_VERSION'=> 85.0,
        'URL'=>'https://api-3t.paypal.com/nvp',
        'USER_NAME'=>'info_api1.distimonster.com',
        'PASSWORD'=>'VEPAZCQRM3G6KNQK',
        'SIGNATURE'=>'AmZMsCt8zpQ5IJGOAJlygSCd6P9jAT0es7cycM7qWgjWTjmBSJZ0OTy0',
    ],

   /* 'PAY_PAL_PRO'=>[
        'API_VERSION'=> 85.0,
        'URL'=>'https://api-3t.sandbox.paypal.com/nvp',
        'USER_NAME'=>'distibusinessuk_api1.mailinator.com',
        'PASSWORD'=>'KXRJVD66DXXPDH32',
        'SIGNATURE'=>'A-44B-s8iXo8dUutxeGRdc1PH-69AUIDe2o9Op69ZG45DDiayQrsgWFX',
    ], */
    'COMMON_EMPTY_FIELD_MESSAGE'=>'You can\'t leave this empty.',
    'NET_TERM_ACCOUNT_STATUS'=>[
        'Pending'=>0,
        'Approved'=>1,
        'In Progress'=>2,
        'Rejected'=>3
    ],
    'PAYMENT_METHOD'=>[
        'paypal'=>1,
        'net_term_account'=>2,
        'credit_card'=>3,
        'cash_on_delivery'=>4
    ],
    'SHIPPING_METHOD_TYPE'=>[
        'ups_ground'=>1,
        'ups_blue'=>2,
        'ups_orange'=>3
    ],
    'NET_ACCOUNT_DOC_PATH'=>'public/files',
    'NEWS_CHARACTERS'=>20,
    'PART_SEARCH_LIMIT'=>9,
    /*My Bio Start*/
    'ARE_YOU_A'=>[
        'Buyer'=>'Buyer',
        'Engineer'=>'Engineer',
        'Engineering Buyer'=>'Engineering Buyer',
        'Other'=>'Other'
    ],
    
    'YOUR_COMPANY_A'=>[
        'OEM'=>'OEM',
        'ODM'=>'ODM',
        'EMS'=>'EMS',
        'University'=>'University',
        'Other'=>'Other'
    ],
    
    'YOUR_MAIN_INDUSTRY'=>[
        'Consumer Electronics'=>'Consumer Electronics',
        'Medical'=>'Medical',
        'Industrial'=>'Industrial',
        'Computers / Peripherals'=>'Computers / Peripherals',
        'Automotive/Transportation'=>'Automotive/Transportation',
        'Aerospace & Defense'=>'Aerospace & Defense',
        'Instrumentation / Controls'=>'Instrumentation / Controls',
        'Telecom'=>'Telecom',
        'University'=>'University',
        'Other'=>'Other'
    ],
    'ARE_YOU_A_ERROR' => 'Please select are you a.',
    'YOUR_COMPANY_A_ERROR' => 'Please select your company a.',
    'YOUR_MAIN_INDUSTRY_ERROR' => 'Please select your main industry.',
    'BIO_UPDATE_SUCCESS'=>'Your BIO has been updated successfully.',
    'SUBUSER_ADDED_SUCCESS'=>'Team member added successfully.',
    'SUBUSER_ADDED_ERROR'=>'Error in adding Team members.',
    'ORBWEAVER_DISTRIBUTOR_NAME' => 'Avnet',
    'ARROW_DISTRIBUTOR_NAME' => 'Arrow',
    'DEFAULT_DISTRIBUTOR_NAME' => 'Distimonster',
    'PLAN_ERROR' => 'Please select a plan.',
    /*My Bio End*/
    /*Subcription PayPal Constant*/
    'SUBSCRIPTION_CLIENT_ID' => "ARLq_JOWJp8g5f0T_AvvMpofOt9lKDrH53iNxm8oSpTNGznb-5jGIxPN93lRvGrPsEcFDBUV7opuiwMz",
    'SUBSCRIPTION_CLIENT_SECRET' => "EHrkuc5CkZSf2A_TuxKz2EI2n3X-mfg7KIqQR1doYSXXIJDcg01HeJzcYaAaTAI5aOfS0jZAckuMZeEV",
    'SUBSCRIPTION_AUTH_URL' => "https://api.sandbox.paypal.com/v1/oauth2/token",
    'SUBSCRIPTION_VAULT_URL' => "https://api.sandbox.paypal.com/v1/vault/credit-cards",
    'GET_SUBSCRIPTION_VAULT_URL' => "https://api.sandbox.paypal.com/v1/vault/credit-cards/",
    'FEDEX_TRACKING_URL' => "https://www.fedex.com/apps/fedextrack/index.html",
    'UPS_TRACKING_URL' => "https://www.ups.com/track",
    'SUBSCRIPTION_PLANS'=>[
        'LEVEL_1'=>[
            'NAME' => 'Level 1',
            'MONTHLY_COST' => 700,
            'FREE_USER' => 1,
            'EXTRA_COST' => 75,
        ],
        'LEVEL_2'=>[
            'NAME' => 'Level 2',
            'MONTHLY_COST' => 900,
            'FREE_USER' => 2,
            'EXTRA_COST' => 50,
        ]
    ], 
];
