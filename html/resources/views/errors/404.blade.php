@extends('frontend.layouts.without-search')
@section('content')
    <section class="gray-bg shipping-cart shipping-addres complete-order">
        <div class="container clearfix">

            <div class="row clearfix text-center">
                <div class="col-md-12">
                    <div class="complete-big-icon"><img src="{{url('frontend/assets/images/404-img.png')}}"  alt="404-img" ></div>
                    <h2 class="block-title left-block-title">Page Not Found</h2>
                </div>
                <div class="col-md-12">
                    <p class="f-s14">The page you were trying to get does not exist! It is probably our fault but just in case,<br/> please check that the URL you entered is correct.</p>
                    <a href="{{url('/')}}" class="btn btn-primary m-t15" title="Continue Shopping">Go to Homepage</a>
                </div>
            </div>

        </div>
    </section>
@stop