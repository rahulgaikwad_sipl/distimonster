<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Quote</title>
    <link rel="stylesheet" href="style.css" media="all" />
</head>
<body style="position: relative; color: #555555;background: #FFFFFF; font-family: Arial, sans-serif; font-size: 14px; text-align: left; ">
<header class="clearfix" style=" padding:0px 0 10px 0;margin-bottom: 10px;border-bottom: 1px solid #AAAAAA;">
    <img src="<?php echo $_SERVER["DOCUMENT_ROOT"]; ?>/frontend/assets/images/logo.png" alt="image" style="width:116px; height: 70px;"/>
</header>
<main>
    <div id="details" class="clearfix">
        <div id="client">
            <div style="font-size:13px;margin-bottom:15px;">Thank you for your interest in DistiMonster.com - the online marketplace for the electronic components industry and  traceable electronic components in the world.
            </div>
        </div>
    </div>
    <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;border-collapse: collapse;border-spacing: 0;margin-bottom: 20px;">
        <thead>
        <tr>
            <th style="color: #000;font-size: 0.7rem;background:#DDDDDD;padding:10px;white-space: nowrap;font-weight: normal;">Part Number</th>
            <th style="color: #000;font-size:0.7rem;background:#DDDDDD;padding:10px;white-space: nowrap;font-weight: normal;">Distributor Name</th>
            <th style="color: #000;font-size:0.7rem;background:#DDDDDD;padding:10px;white-space: nowrap;font-weight: normal;">Quantity</th>
            <th style="color: #000;font-size:0.7rem;background:#DDDDDD;padding:10px;white-space: nowrap;font-weight: normal;">Internal Part#</th>
            <th style="color: #000;font-size:0.7rem;background:#DDDDDD;padding:10px;white-space: nowrap;font-weight: normal;">Actual Price</th>
            <th style="color: #000;font-size:0.7rem;background:#DDDDDD;padding:10px;white-space: nowrap;font-weight: normal;">Requested Price</th>
            <th style="color: #000;font-size:0.7rem;background:#DDDDDD;padding:10px;white-space: nowrap;font-weight: normal;">Approved Price</th>

        </tr>
        </thead>
        <tbody>
        <?php
        $quoteSubTotal= 0;
        foreach($quoteItems as $item){ ?>
        <tr>
            <td style="padding: 10px;background: #EEEEEE;text-align: left;border-bottom: 1px solid #FFFFFF;">{{$item->item_name}} ({{ $item->manufacturer_name }})</td>
            <td style="padding: 10px;background: #EEEEEE;text-align: left;border-bottom: 1px solid #FFFFFF;">@if(!empty($item->distributor_name)){{$item->distributor_name}}@else N/A @endif</td>
            <td style="padding: 10px;background: #EEEEEE;text-align: left;border-bottom: 1px solid #FFFFFF;">@if(!empty($item->quantity)){{$item->quantity}}@else N/A @endif</td>
            <td style="padding: 10px;background: #EEEEEE;text-align: left;border-bottom: 1px solid #FFFFFF;">@if(!empty($item->customer_part_id)){{$item->customer_part_id}}@else N/A @endif</td>
            <td style="padding: 10px;background: #EEEEEE;text-align: left;border-bottom: 1px solid #FFFFFF;">${{$item->price}}</td>
            <td style="padding: 10px;background: #EEEEEE;text-align: left;border-bottom: 1px solid #FFFFFF;">@if(!empty($item->requested_cost)) ${{$item->requested_cost}} @else N/A @endif</td>
            <td style="padding: 10px;background: #EEEEEE;text-align: left;border-bottom: 1px solid #FFFFFF;">@if(!empty($item->approved_price)) ${{$item->approved_price}} @else N/A @endif</td>
        </tr>
        <?php
        $quoteSubTotal = $quoteSubTotal+ ($item->quantity * $item->price);
        }
        ?>
        </tbody>
        <tfoot>
        {{--<tr>--}}
            {{--<td></td>--}}
            {{--<td></td>--}}
            {{--<td></td>--}}
            {{--<td style="padding:10px;background: #FFFFFF;border-bottom:none;font-size: 1.2em;white-space: nowrap;border-bottom:1px solid #AAAAAA">SUBTOTAL</td>--}}
            {{--<td style="padding: 10px;background: #FFFFFF;border-bottom:none;font-size: 1.2em;white-space: nowrap;border-bottom:1px solid #AAAAAA">$ {{number_format($quoteSubTotal,2,'.',',')}}</td>--}}
        {{--</tr>--}}
        </tfoot>
    </table>
    <div id="details" class="clearfix">
        <div id="client">
            <div style="font-size:13px;margin-bottom:15px;"><b>Note</b> :- {{$quoteInfo[0]->note?$quoteInfo[0]->note:'N/A'}}
            </div>
        </div>
    </div>
    <div id="details" class="clearfix">
        <div id="client">
            <div style="font-size:13px;margin-bottom:15px;">All items are non-cancellable and non-returnable. All orders should be placed online at https://distimonster.com
        Any order placed under this quote is subject to Distimonster's terms and conditions, which can be found https://distimonster.com/terms-of-use

        Shipping charges are to be calculated and charged at the time of delivery and dependent upon the amount of parts ordered, carrier chosen as well as your ship-to location.

        The quickest payment method is with a major credit card or through PayPal. You may also pay by wire transfer (for which a $25 fee will be added to cover bank fees). Please note that it takes an additional 2-3 business days to process a wire transfer.

        If you would like to set up a new Line of Credit with us, you can start the application process from here https://distimonster.com/apply-net-term-account. Typically, the approval process (credit check, business verification, and administration) takes 3-4 business days.           </div>
        </div>
    </div>
    <div id="details" class="clearfix">
        <div id="client">
            <div style="font-size:13px;margin-bottom:15px;">
                Sincerely,
              </div>
            </div>
          </div>
          <div id="details" class="clearfix">
              <div id="client">
                  <div style="font-size:13px;margin-bottom:15px;">

Sales Support Team
</div>
</div>
</div>
<div id="details" class="clearfix">
    <div id="client">
        <div style="font-size:13px;margin-bottom:15px;">
sales@distimonster.com
</div>
</div>
</div>
<div id="details" class="clearfix">
    <div id="client">
        <div style="font-size:13px;margin-bottom:15px;">
            +1 818 857 5788
</div>
</div>
</div>
<div id="details" class="clearfix">
    <div id="client">
        <div style="font-size:13px;margin-bottom:15px;">
https://distimonster.com
</div>
</div>
</div>


</main>
<footer style="color:#777777;width:100%;height:15px;position:absolute;bottom:0;border-top:1px solid #AAAAAA;padding: 8px 0;text-align:center;">
  <p> Invoice was created on a computer and is valid without the signature and seal.</p>
    <p style="text-align: left;">*This ID is for internal use only and does not reflect anything about the MPN</p>
</footer>
</body>
</html>
