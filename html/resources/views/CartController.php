<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use Cart;
use Event;
use Session;
use App\Http\Controllers\Controller;
class CartController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    public function addToCart(Request $request){
      try {
        $data               = $request->all();
        $productId          = $request->productId;
        $manufacturer       = $request->manufacturer;
        $sourcePartId       = $request->sourcePartId;
        $quantity           = $request->quantity;
        $dateCode           = $request->dateCode;
        $customerPartNo     = $request->customerPartNo;
        $stockAvailability  = $request->stockAvailability;
        $price              = 0;
        $resaleList         = array();
        $availableData      = array();
        $dateCode           = '';
        $manufacturerCode   = $request->mfrCd;
        $availableStock     = 0;
        $minQty             = 0;
            /* Check item is exist on cart or not  */
           $itemExist =  Cart::instance('shoppingcart')->content()->where('id', $productId)->where('options.sourcePartId', $sourcePartId);
           if(empty(json_decode($itemExist))){
                $price = 0;
                $resaleList = array();
                /* Price calculation on the bases of Qty */
                $productDetails = Session::get('productDetails');
                foreach ($productDetails as $key => $productDetail) {
                   foreach ($productDetail['sources'] as $sourcesKey => $sources) {
                        foreach ($sources['sourcesPart'] as $sourcePartKey => $sourcesPart) {
                           if($sourcesPart['sourcePartId'] == $sourcePartId){
                             $dateCode = $sourcesPart['dateCode'];
                             if($sourcesPart['inStock']){
                              $availableStock = $sourcesPart['Availability'][0]['fohQty'];
                             }else{
                               $availableStock = 0;
                             }
                             $availableData = $sourcesPart['Availability'];
                             foreach ($sourcesPart['Prices']['resaleList'] as $key => $prices) {
                                if($quantity >= $prices['minQty'] && $quantity <= $prices['maxQty'] ){
                                     $price = $prices['price'];
                                     $resaleList = $sourcesPart['Prices']['resaleList'];
                                     break;
                                }
                               }
                               $minQty = $sourcesPart['Prices']['resaleList'][0]['minQty'];
                             }
                        }
                   }
                }
              //  if($price > 0){
                  /* Check mim quantity of items */
                  if($quantity < $minQty){
                     return response(['status' => false, 'minQtyError' => true, 'data' => '', 'message' => 'Minimum quantity: '.$minQty,'minQty' => $minQty]);
                  }
                  /* Check available stock of items */
                  if($quantity <= $availableStock){
                  Cart::instance('shoppingcart')->add([
                    ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price,3),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability]]
                ]);
                }else{
                  if($availableStock == 0){
                      return response(['status' => false, 'data' => '', 'message' => 'Item is out of stock.']);
                  }else{
                      return response(['status' => false, 'stockError' => true, 'data' => '', 'message' => 'Availabile stock: '.$availableStock,'stock' => $availableStock]);
                  }
                }
                 $totalPrice = 0;
                 foreach(Cart::instance('shoppingcart')->content() as $row){
                    $totalPrice = $totalPrice + $row->price * $row->qty;
                 } 
                 $itemCount = Cart::instance('shoppingcart')->content()->count();
                return response(['status' => true,'data' => '','itemPrice' => number_format($price,3),'totalPrice'=> $totalPrice,'itemCount'=>$itemCount,'message' => 'Item added successfully in the cart.','availableData'=>$availableData ]);
             /* }else{
                   return response(['status' => false, 'data' => '', 'message' => 'Something Wrong']);
              }*/
            }else{
              /* Item Update in the cart */
               foreach ($itemExist as $index => $data) {
                $rowId = $data->rowId;
                $resaleList = $data->options->resaleList;
                $dateCode = $data->options->dateCode;
                $availableData = $data->options->availableData;
                $availableStock = $availableData[0]['fohQty'];
               }
              
              foreach ($resaleList as $key => $prices) {
                                if($quantity >= $prices['minQty'] && $quantity <= $prices['maxQty'] ){
                                     $price = $prices['price'];
                                     break;
                                }
                               }
                  /* Check mim quantity of items */
                 $minQty = $resaleList[0]['minQty'];
                 if($quantity < $minQty){
                     return response(['status' => false, 'minQtyError' => true, 'data' => '', 'message' => 'Minimum quantity: '.$minQty,'minQty' => $minQty]);
                  }
                 /* Check available stock of items */
                 if($quantity <= $availableStock){             
                 Cart::instance('shoppingcart')->update($rowId,
                    ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price,3),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability]]
                );
                 }else{
                  if($availableStock == 0){
                      return response(['status' => false, 'data' => '', 'message' => 'Item is out of stock.']);
                  }else{
                      return response(['status' => false, 'stockError' => true, 'data' => '', 'message' => 'Availabile stock: '.$availableStock,'stock' => $availableStock]);
                  }
                } 
                 $totalPrice = 0;
                 foreach(Cart::instance('shoppingcart')->content() as $row){
                    $totalPrice = $totalPrice + $row->price * $row->qty;
                 } 
                 $itemCount = Cart::instance('shoppingcart')->content()->count();
                return response(['status' => true, 'data' => '', 'message' => 'Item updates successfully in the cart','itemPrice' => number_format($price,3),'totalPrice'=> $totalPrice,'itemCount' => $itemCount,'availableData '=> $availableData ]);
            }
      } catch (\Exception $ex) {
        return response(['status' => true, 'data' => $ex->getMessage()]);
      }
    }

    public function getCart(){
        return view('frontend.cart.cart');
    }
    public function removeCartItem(Request $request){
      $data         = $request->all();
      $productId    = $request->product_id;
      Cart::instance('shoppingcart')->remove($productId);
      return response(['status' => true, 'data' => $data]);
    }
    public function emptyCart(Request $request){
        Cart::instance('shoppingcart')->destroy();
        return response(['status' => true, 'data' => '']);
    }
}
