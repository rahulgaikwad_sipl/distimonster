<tr>
    <td>
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
                <td align="left" valign="top"><!--Footer Bottom Part Start-->
                    <table border="0" align="center" cellpadding="0" cellspacing="0" style="background:#000;color:#fff;font-size:12px;width:650px;max-width:100%;">
                        <tr>
                            <td align="center" valign="top" style="padding:20px 32px 20px 32px;">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center" valign="top" style="font:Normal 13px Arial, Helvetica, sans-serif;color:#fff;font-size:12px;line-height:18px;padding:0 0 20px;">19821 Nordhoff Place Ste 114 Chatsworth, CA 91311 | <a href="mailto:sales@distimonster.com" style="color:#fff;font-size:12px;text-decoration:none;"><strong style="color:#fff;font-size:12px;font-weight:400;">sales@distimonster.com</strong></a> | +1 818 857 5788</td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="font:Normal 13px Arial, Helvetica, sans-serif;color:#fff;line-height:18px;padding:0 0 20px;width:25%;">
                                            <a href="https://twitter.com/distimonster"  target="_blank" title="Twitter"><img src="{{url('frontend/assets/images/twitter-icon.png')}}" alt="Twitter"></a>&nbsp;&nbsp;&nbsp;
                                        <!--  <a href="https://www.facebook.com/" title="Facebook"><img src="{{url('frontend/assets/images/facebook-icon.png')}}" alt="Facebook"></a>&nbsp;&nbsp;&nbsp;
                                                <a href="https://twitter.com/" title="Twitter"><img src="{{url('frontend/assets/images/twitter-icon.png')}}" alt="Twitter"></a>&nbsp;&nbsp;&nbsp;
                                                <a href="https://plus.google.com/" title="Google+"><img src="{{url('frontend/assets/images/googleplus-icon.png')}}" alt="Google+"></a>&nbsp;&nbsp;&nbsp;
                                                <a href="https://www.linkedin.com/" title="Linkedin"><img src="{{url('frontend/assets/images/linkedin-icon.png')}}" alt="Linkedin"></a>-->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="font:Normal 13px Arial, Helvetica, sans-serif;color:#fff;line-height:18px;">&copy; {{ config('app.name') }} {{date('Y') }}. All rights reserved</td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table>

                    <!--Footer Bottom Part End--></td>
            </tr> <!-- // main tr-->
        </table></td>
</tr>
</table>
