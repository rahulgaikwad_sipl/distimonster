<!DOCTYPE html>
<!-- saved from url=(0022)http://13.126.15.151/# -->
<html lang="en-US">

<head>
    @include('frontend.partials.head')
</head>

      
<body data-container="body" class="cms-home cms-index-index page-layout-1column">
        
<div class="page-wrapper"><div class="main-header"><div class="header-top"><div class="container"><ul class="header-top-left">
    <li><i class="fa fa-envelope" aria-hidden="true"></i> Email: <a href="mail:sales@distimonster.com">sales@distimonster.com</a></li>
    <li><i class="fa fa-phone" aria-hidden="true"></i> Phone: <a href="tel:(000) 123-4567">(000) 123-4567</a></li>
</ul><ul class="header links">        <li class="greet welcome" data-bind="scope: &#39;customer&#39;">
            <i class="fa fa-user-o" aria-hidden="true"></i>
            <!-- ko if: customer().fullname  --><!-- /ko -->
            <!-- ko ifnot: customer().fullname  -->
            <span data-bind="html:&quot;Default welcome msg!&quot;">Default welcome msg!</span>
                        <!-- /ko -->
        </li>
        
    <li class="authorization-link" data-label="or">
    <a href="http://13.126.15.151/customer/account/login/">
        Sign In    </a>
</li>
<li><a href="http://13.126.15.151/customer/account/create/">Register</a></li></ul></div></div><div class="panel wrapper"><div class="panel header"><a class="action skip contentarea" href="http://13.126.15.151/#contentarea"><span>Skip to Content</span></a>
</div></div></div>    <div class="sections nav-sectionsXX custom-nav">
                <div class="section-items nav-sectionsXX custom-nav-items" role="tablist">
                                            <div class="section-item-title nav-sectionsXX custom-nav-item-title active" data-role="collapsible" role="tab" data-collapsible="true" aria-controls="store.menu" aria-selected="false" aria-expanded="true" tabindex="0">
                    <a class="nav-sectionsXX custom-nav-item-switch" data-toggle="switch" href="http://13.126.15.151/#store.menu"></a>
                </div>
                <div class="section-item-content nav-sectionsXX custom-nav-item-content" id="store.menu" data-role="content" role="tabpanel" aria-hidden="false">
<!-- <nav class="navigation custom-nav" data-action="navigation">
    <ul data-mage-init='{"menu":{"responsive":true, "expanded":true, "position":{"my":"left top","at":"left bottom"}}}'>
                    </ul>
</nav> -->

<!--Navigation-->
<div class="custom-nav">
    <div class="container">
        <div class="navbar-collapse collapse" id="navbar" "="">
        <div class="row">
            <div class="col-sm-3">
                <div class="all-product-menu clearfix">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="http://13.126.15.151/#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">All Products <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="http://13.126.15.151/#">Action</a></li>
                                <li><a href="http://13.126.15.151/#">Another action</a></li>
                                <li><a href="http://13.126.15.151/#">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-9">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="http://13.126.15.151/#">All Manufacturers <span class="sr-only">(current)</span></a></li>
                    <li><a href="http://13.126.15.151/#">About Us</a></li>
                    <li><a href="http://13.126.15.151/#">News &amp; Updates</a></li>
                    <li class="dropdown">
                        <a href="http://13.126.15.151/#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Support <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="http://13.126.15.151/#">Action</a></li>
                            <li><a href="http://13.126.15.151/#">Another action</a></li>
                            <li><a href="http://13.126.15.151/#">Something else here</a></li>
                        </ul>
                    </li>
                    <li><a href="http://13.126.15.151/#">BOM</a></li>
                    <li class="dropdown">
                        <a href="http://13.126.15.151/#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="http://13.126.15.151/#">Action</a></li>
                            <li><a href="http://13.126.15.151/#">Another action</a></li>
                            <li><a href="http://13.126.15.151/#">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    </div>
</div>
<!--Navigation end--></div>
                                    </div>
    </div>
<main id="maincontent" class="page-main"><a id="contentarea" tabindex="-1"></a>
<div class="page-title-wrapper">
    <h1 class="page-title">
        <span class="base" data-ui-id="page-title-wrapper">Customer Login</span>    </h1>
    </div>
<div class="page messages"><div data-placeholder="messages"></div>
<div data-bind="scope: 'messages'">
    <!-- ko if: cookieMessages && cookieMessages.length > 0 --><!-- /ko -->
    <!-- ko if: messages().messages && messages().messages.length > 0 --><!-- /ko -->
</div>

</div><div class="columns"><div class="column main"><input name="form_key" type="hidden" value="GYdtjzY981CvhE3t"><div id="authenticationPopup" data-bind="scope:'authenticationPopup'" style="display: none;">
    <script>
        window.authenticationPopup = {"autocomplete":"off","customerRegisterUrl":"http:\/\/13.126.15.151\/customer\/account\/create\/","customerForgotPasswordUrl":"http:\/\/13.126.15.151\/customer\/account\/forgotpassword\/","baseUrl":"http:\/\/13.126.15.151\/"};
    </script>
    <!-- ko template: getTemplate() -->
<!-- /ko -->
</div>
<div class="login-container"><div class="block block-customer-login">
    <div class="block-title">
        <strong id="block-customer-login-heading" role="heading" aria-level="2">Registered Customers</strong>
    </div>
    <div class="block-content" aria-labelledby="block-customer-login-heading">
        <form class="form form-login" action="http://13.126.15.151/customer/account/loginPost/" method="post" id="login-form" novalidate="novalidate">
            <input name="form_key" type="hidden" value="GYdtjzY981CvhE3t">            <fieldset class="fieldset login" data-hasrequired="* Required Fields">
                <div class="field note">If you have an account, sign in with your email address.</div>
                <div class="field email required">
                    <label class="label" for="email"><span>Email</span></label>
                    <div class="control">
                        <input name="login[username]" value="" autocomplete="off" id="email" type="email" class="input-text" title="Email" data-validate="{required:true, 'validate-email':true}" aria-required="true">
                    </div>
                </div>
                <div class="field password required">
                    <label for="pass" class="label"><span>Password</span></label>
                    <div class="control">
                        <input name="login[password]" type="password" autocomplete="off" class="input-text" id="pass" title="Password" data-validate="{required:true}" aria-required="true">
                    </div>
                </div>
                                <div class="actions-toolbar">
                    <div class="primary"><button type="submit" class="action login primary" name="send" id="send2"><span>Sign In</span></button></div>
                    <div class="secondary"><a class="action remind" href="http://13.126.15.151/customer/account/forgotpassword/"><span>Forgot Your Password?</span></a></div>
                </div>
            </fieldset>
        </form>
    </div>
</div>
<div class="block block-new-customer">
    <div class="block-title">
        <strong id="block-new-customer-heading" role="heading" aria-level="2">New Customers</strong>
    </div>
    <div class="block-content" aria-labelledby="block-new-customer-heading">
        <p>Creating an account has many benefits: check out faster, keep more than one address, track orders and more.</p>
        <div class="actions-toolbar">
            <div class="primary">
                <a href="http://13.126.15.151/customer/account/create/" class="action create primary"><span>Create an Account</span></a>
            </div>
        </div>
    </div>
</div>
</div>
</div></div></main>


    <footer class="page-footer"><div id="footer"><!--START: Footer-->
<div class="container footer">
    <div class="row bdr-bottom p-b50">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <h4>Contact</h4>
            <ul class="address-list">
                <li class="address-list-item"><i class="fa fa-home" aria-hidden="true"></i> Lorem 76d, Ipsum Dollor, North Michaelchester, CF99 6QQ</li>
                <li class="address-list-item"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:sales@distimonster.com">sales@distimonster.com</a></li>
                <li class="address-list-item"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:(000) 123-4567">(000) 123-4567</a></li>
            </ul>
            <div class="payment-card"><img src="frontend/files/payment-card.png" class="img-responsive"></div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <h4>Categories</h4>
            <ul class="list">
                <li class="list-item"><a href="http://13.126.15.151/#">Proin Imperdiet</a></li>
                <li class="list-item"><a href="http://13.126.15.151/#">Lorem Ut</a></li>
                <li class="list-item"><a href="http://13.126.15.151/#">Hendrerit Tincidunt</a></li>
                <li class="list-item"><a href="http://13.126.15.151/#">Enim Elit</a></li>
                <li class="list-item"><a href="http://13.126.15.151/#">Sagittis Sem</a></li>
                <li class="list-item"><a href="http://13.126.15.151/#">Sagittis Sem</a></li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <h4>More Info</h4>
            <ul class="list">
                <li class="list-item"><a href="http://13.126.15.151/#">About DistiMonster</a></li>
                <li class="list-item"><a href="http://13.126.15.151/#">News &amp; Updates</a></li>
                <li class="list-item"><a href="http://13.126.15.151/#">Privacy Policy and Terms of Use</a></li>
                <li class="list-item"><a href="http://13.126.15.151/#">Site Map</a></li>
                <li class="list-item"><a href="http://13.126.15.151/#">FAQs</a></li>
                <li class="list-item"><a href="http://13.126.15.151/#">Return Policy</a></li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <h4>Signup for Newsletter</h4>
            <form class="newsletter-form">
                <p>Get the latest news, special offers and other discount information.</p>
                <div class="newsletter-form-item">
                    <input type="text" name="emailNewsletter" placeholder="Your email address">
                    <input type="button" value="Submit" title="Submit" class="newsletter-btn">
                </div>
            </form>
            <ul class="social-list">
                <!-- <li class="social-list-item"><a href="http://13.126.15.151/#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li> -->
                <li class="social-list-item"><a href="https://twitter.com/distimonster"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                <!-- <li class="social-list-item"><a href="http://13.126.15.151/#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                <li class="social-list-item"><a href="http://13.126.15.151/#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li> -->
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <p class="copyright">© Copyright DistiMonster. All rights Reserved, 2018</p>
        </div>
    </div>

</div>
<!-- <div class="footer-container">
    <div class="footer">
                <p class="bugs">Help Us Keep Magento Healthy - <a
            href="http://www.magentocommerce.com/bug-tracking"
            target="_blank"><strong>Report All Bugs</strong></a>
        </p>
        <address>&copy; Copyright DistiMonster. All rights Reserved, 2018</address>
    </div>
</div> -->
<script type="text/javascript">
require(['jquery', 'jquery/ui'], function($){ 
     //your js code here  
 });
</script></div></footer>
</div>    

<div class="modals-wrapper"><!--
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
-->

<aside role="dialog" class="modal-popup popup-authentication
               modal-slide
               _inner-scroll" aria-describedby="modal-content-7" data-role="modal" data-type="popup" tabindex="0">
    <div data-role="focusable-start" tabindex="0"></div>
    <div class="modal-inner-wrap" data-role="focusable-scope">
        <header class="modal-header">
            
            <button class="action-close" data-role="closeBtn" type="button">
                <span>Close</span>
            </button>
        </header>
        <div id="modal-content-7" class="modal-content" data-role="content"><div class="block-authentication" data-bind="afterRender: setModalElement, blockLoader: isLoading" style="">
    <div class="block block-new-customer" data-bind="attr: {&#39;data-label&#39;: $t(&#39;or&#39;)}" data-label="or">
        <div class="block-title">
            <strong id="block-new-customer-heading" role="heading" aria-level="2" data-bind="i18n: &#39;Checkout as a new customer&#39;">Checkout as a new customer</strong>
        </div>
        <div class="block-content" aria-labelledby="block-new-customer-heading">
            <p data-bind="i18n: &#39;Creating an account has many benefits:&#39;">Creating an account has many benefits:</p>
            <ul>
                <li data-bind="i18n: &#39;See order and shipping status&#39;">See order and shipping status</li>
                <li data-bind="i18n: &#39;Track order history&#39;">Track order history</li>
                <li data-bind="i18n: &#39;Check out faster&#39;">Check out faster</li>
            </ul>
            <div class="actions-toolbar">
                <div class="primary">
                    <a class="action action-register primary" data-bind="attr: {href: registerUrl}" href="http://13.126.15.151/customer/account/create/">
                        <span data-bind="i18n: &#39;Create an Account&#39;">Create an Account</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="block block-customer-login" data-bind="attr: {&#39;data-label&#39;: $t(&#39;or&#39;)}" data-label="or">
        <div class="block-title">
            <strong id="block-customer-login-heading" role="heading" aria-level="2" data-bind="i18n: &#39;Checkout using your account&#39;">Checkout using your account</strong>
        </div>
        <!-- ko foreach: getRegion('messages') -->
        <!-- ko template: getTemplate() -->
<div data-role="checkout-messages" class="messages" data-bind="visible: isVisible(), click: removeAll">
    <!-- ko foreach: messageContainer.getErrorMessages() --><!--/ko-->
    <!-- ko foreach: messageContainer.getSuccessMessages() --><!--/ko-->
</div>
<!-- /ko -->
        <!--/ko-->
        <!-- ko foreach: getRegion('before') --><!-- /ko -->
        <div class="block-content" aria-labelledby="block-customer-login-heading">
            <form class="form form-login" method="post" data-bind="event: {submit: login }" id="login-form">
                <div class="fieldset login" data-bind="attr: {&#39;data-hasrequired&#39;: $t(&#39;* Required Fields&#39;)}" data-hasrequired="* Required Fields">
                    <div class="field email required">
                        <label class="label" for="email"><span data-bind="i18n: &#39;Email Address&#39;">Email Address</span></label>
                        <div class="control">
                            <input name="username" id="email" type="email" class="input-text" data-bind="attr: {autocomplete: autocomplete}" data-validate="{required:true, &#39;validate-email&#39;:true}" autocomplete="off">
                        </div>
                    </div>
                    <div class="field password required">
                        <label for="pass" class="label"><span data-bind="i18n: &#39;Password&#39;">Password</span></label>
                        <div class="control">
                            <input name="password" type="password" class="input-text" id="pass" data-bind="attr: {autocomplete: autocomplete}" data-validate="{required:true}" autocomplete="off">
                        </div>
                    </div>
                    <!-- ko foreach: getRegion('additional-login-form-fields') -->
                    <!-- ko template: getTemplate() -->
<!-- ko if: (isRequired() && getIsVisible())--><!-- /ko -->
<!-- /ko -->
                    <!-- /ko -->
                    <div class="actions-toolbar">
                        <input name="context" type="hidden" value="checkout">
                        <div class="primary">
                            <button type="submit" class="action action-login secondary" name="send" id="send2">
                                <span data-bind="i18n: &#39;Sign In&#39;">Sign In</span>
                            </button>
                        </div>
                        <div class="secondary">
                            <a class="action" data-bind="attr: {href: forgotPasswordUrl}" href="http://13.126.15.151/customer/account/forgotpassword/">
                                <span data-bind="i18n: &#39;Forgot Your Password?&#39;">Forgot Your Password?</span>
                            </a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div></div>
        
    </div>
    <div data-role="focusable-end" tabindex="0"></div>
</aside></div></body></html>