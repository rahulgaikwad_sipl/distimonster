<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.partials.head')
</head>
<body>
<div class="entire-site">
    <div style="display: none;" class="loader"></div>
    {{--<div class="splash" style="display: none;"> <img src="{{url('frontend/assets/images/ajax-loader.gif')}}" /></div>--}}
    @include('frontend.partials.topbar')

    <nav class="navbar ">
        <div class="nav-container custom-header">
            <div class="container">

                <div class="row">
                    <div class="navbar-header col-md-4">
                        <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('frontend/images/logo.png') }}" class="img-responsive"></a>
                    </div>
                    <div class="col-md-8 col-sm-6 hidden-sm">
                        <div class="row" >
                            <div class="col-sm-12 m-b25" >

                                    {!! Form::open(['method' => 'get', 'url' => url('search'), 'class'=>"form minisearch" ,'id'=>"search_part_number",'data-parsley-validate' => true]) !!}
                                    <div class="field search clearfix">
                                        <input id="part_name" type="text" name="partNumber" data-parsley-errors-container="#error-container"  data-parsley-required   data-parsley-required-message="Please enter any part number" value="{{ app('request')->input('partNumber') }}" placeholder="Search for product" class="input-text" />
                                        <button type="submit" title="Search" class="action search" >
                                            <span>Search</span>
                                        </button>
                                    </div>
                                    <div id="error-container"> </div>
                                    {!! Form::close() !!}
                                    <div class="search-history">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#search-history">Search History <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    </div>

                            </div>
                            @if (Auth::check())
                                <div class="col-sm-12">
                                    <h4 class="myacc-heading">Quick Account Options</h4>
                                    <ul class="myacc-menu">
                                        <li><a href="{{ url('account-settings/'.Auth::user()->id)}}">Account Setting</a></li>
                                        <li><a href="{{url('order-history')}}">Order History</a></li>
                                        <li><a href="{{url('saved-quotes')}}">Saved Quotes</a></li>
                                        <li><a href="{{url('my-bom')}}">My BOM</a></li>
                                        <li><a href="{{url('apply-net-term-account')}}">Apply for a Net Term Account</a></li>
                                    </ul>
                                </div>
                            @endif
                        </div>
                    </div>
                <!-- <div class="col-md-6">
                    <ul class="nav navbar-nav navbar-right checkout m-t15 hidden-xs">
                        @if(Cart::instance('quotecart')->content()->count() > 0)
                    <li><a href="{{url('/quote-cart')}}"><div class="icon"><img src="{{url('frontend/assets/images/icon-quote-cart.png')}}" alt="Quote Requests" />  <em class="cart-count" id="quoteCount">{{Cart::instance('quotecart')->content()->count()}}</em>  </div> <span>Quote Requests</span> <br><span style="font-weight: 500; color: #000;"> </span></a></li>
                        @else
                    <li><a href="{{url('/quote-cart')}}"><div class="icon"><img src="{{url('frontend/assets/images/icon-quote-cart.png')}}" alt="Quote Requests" />  <em style="display: none;" class="cart-count" id="quoteCount"></em>  </div> <span>Quote Requests</span> <br><span style="font-weight: 500; color: #000;"> </span></a></li>
                        @endif
                @if(Cart::instance('shoppingcart')->content()->count() > 0)
                    <li><a href="{{url('/cart')}}"><div class="icon"><img src="{{url('frontend/assets/images/icon-cart.png')}}" alt="Your Cart" /> <em class="cart-count" id="cartCount">{{Cart::instance('shoppingcart')->content()->count()}}</em></div><span>Your Cart</span> <br><span id="totalPrice">{{Cart::instance('shoppingcart')->subtotal()}}</span></a></li>
                        @else
                    <li><a href="{{url('/cart')}}"><div class="icon"><img src="{{url('frontend/assets/images/icon-cart.png')}}" alt="Your Cart" /> <em class="cart-count" style="display: none;" id="cartCount"></em></div><span>Your Cart</span> <br><span id="totalPrice"></span></a></li>
                        @endif
                        </ul>


                        <ul class="myacc-menu">
                            <li><a href="#">Account Setting</a></li>
                            <li><a href="#">Order History</a></li>
                            <li><a href="#">Saved Quotes</a></li>
                            <li><a href="#">My BOM</a></li>
                            <li><a href="#">Apply dor a Net Term Account</a></li>
                        </ul>
                    </div> -->
                </div>





            </div>
        </div>
    </nav>
    @include('frontend.partials.navigation')
    @yield('content')
    @include('frontend.partials.footer')
    @yield('javascript')
    <!-- old: hccid=40037455 -->
    <script type="text/javascript">function add_chatinline(){var hccid=60761561;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
        add_chatinline(); </script>
    {{--@if (Auth::check())--}}
    {{--<!-- Sliding div starts here -->--}}
        {{--<div id="slider" style="right:0px;">--}}
            {{--<div id="sidebar" onclick="open_panel()"><img src="{{url('frontend/assets/images/myaccount.png')}}" ></div>--}}
            {{--<div id="header">--}}
                {{--<h2>My Account</h2>--}}
                {{--<div>--}}
                    {{--<a class="dropdown-item" href="{{ url('account-settings/'.Auth::user()->id)}}">Account Settings</a>--}}
                    {{--<a class="dropdown-item" href="{{url('order-history')}}">Order History</a>--}}
                    {{--<a class="dropdown-item" href="{{url('saved-quotes')}}">Saved Quotes</a>--}}
                    {{--<a class="dropdown-item" href="{{url('my-bom')}}">My BOM</a>--}}
                    {{--<a class="dropdown-item" href="{{url('apply-net-term-account')}}">Apply for a Net<br>Term Account</a>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- Sliding div ends here -->--}}
    {{--@endif--}}
</div>
</body>
</html>