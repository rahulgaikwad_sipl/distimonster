<!DOCTYPE html>
<html lang="en">
<head>
    @include('frontend.partials.head')
</head>
<body>
<div class="entire-site">
    <div style="display: none;" class="loader monster-bom-loader">
        <div class="loader-img">
            &nbsp;
            <div id="loader_note_message" style="display: none;">
                <div class="alert alert-info">
                    <strong>Please be Patient,</strong> The Monster Is Processing Your Request Against Millions of Items
                </div>
            </div>
        </div>
    </div>
    
    @include('frontend.partials.topbar')
    @include('frontend.partials.header')
    @include('frontend.partials.navigation')
    @yield('content')
    @include('frontend.partials.footer')
    @yield('javascript')
    <!-- old: hccid=40037455 -->
    <script type="text/javascript">function add_chatinline(){var hccid=60761561;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
add_chatinline(); </script>
@if (Auth::check())
    <!-- Sliding div starts here -->
    <!-- <div id="slider" style="right:0px;">
        <div id="sidebar" onclick="open_panel()"><img src="{{url('frontend/assets/images/myaccount.png')}}" ></div>
        <div id="header">
            <h2>My Account</h2>
            <div>
                <a class="dropdown-item" href="{{ url('account-setting/'.Auth::user()->id)}}">Account Settings</a>
                <a class="dropdown-item" href="{{url('order-history')}}">Order History</a>
                <a class="dropdown-item" href="{{url('saved-quotes')}}">Saved Quotes</a>
                <a class="dropdown-item" href="{{url('my-bom')}}">My BOM</a>
                <a class="dropdown-item" href="{{url('apply-net-term-account')}}">Apply for a Net<br>Term Account</a>
            </div>
        </div>
    </div> -->
    <!-- Sliding div ends here -->
    @endif

</div>
</body>
</html>