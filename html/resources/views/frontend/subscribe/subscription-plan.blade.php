@extends('frontend.layouts.app')
@section('title', '| Subscribe')
@section('content')
<!--Start CONSOLIDATION Section-->
<section class="light-blue p-t40 p-b30" id="subscription-plan">
    <div class="container">
        <div class="text-center w-100 paln-and-pricing mt-3 mb-5">
            <h2>CONSOLIDATION WITH INTELLIGENCE</h2>
            <h4>Design and buy components on one site.</h4>
        </div>
        <div class="row m-b15 plan-with-pricing">
            <div class="col-md-3">
                <img src="{{url('frontend/assets/images/consolidation-girl-img-new.jpg')}}"
                    class="img-responsive img-box-effect">
            </div>
            <div class="col-md-9 mt-md-2">              
                <p class="m-b5 hm-bar-icon p-t0"><img src="{{url('frontend/assets/images/green-bar-one.png')}}" class="img-responsive">
                </p>
                <p class="p-t0" id="plan-with-pricing-txt">
                    Ditch spending hours looking on multiple sites for components. Use Monster-BOM to upload your BOM or search for components by entering individual part numbers. Generate one PO and procure all components per project through DistiMonster. Streamline your workflow with DistiMonster — one source at a flat monthly or yearly rate.
                </p>
            </div>
        </div>
    </div>
</section>
<!--CONSOLIDATION Section end-->
<!--Distimonster Section Start-->
<section class="paln-and-pricing">
    <div class="container">
        <div class="text-center w-100 mt-5 mb-3">
            <h2>Distimonster Plan & pricing</h2>
            <h4>Try us out for 30 days for FREE.</h4>
        </div>
        <div class="row">
        @if(!empty($subscription_plans))
            @foreach($subscription_plans as $plan)
            <!--Single plan Start-->
            <div class="col-lg-6">
                <div class="card-deck mb-3 mt-3 text-center card-custom">
                    <div class="card">
                        <div class="card-header">
                            @if($plan->type == "Core")
                                <h4 class="font-weight-normal">CORE PLAN</h4>
                            @elseif($plan->type == "Pro")
                                <div class="ribon-with-text">
                                    <img src="{{url('frontend/assets/images/plan-ribon.png')}}" class="img-responsive">
                                    <span class="available-text-wrap">
                                        <span class="available-text">AVAILABLE<span> OCTOBER 1, 2019
                                            </span>
                                </div>
                                <h4 class="font-weight-normal">PRO PLAN</h4>
                            @endif
                        </div>
                        <div class="card-body">
                            <div class="card-body-inner">
                            <ul class="list-unstyled mt-3 mb-3">
                                <li class="plan-description plan-brdr-btm">
                                    <span class="saving-text">20% SAVINGS 
                                    @if($plan->type == "Core")
                                    <span class="bastvalue-text">A<span class="doller-sign">$</span>8388 VALUE</span>
                                    @elseif($plan->type == "Pro")
                                        <span class="bastvalue-text">A<span class="doller-sign">$</span>10,788 VALUE</span>
                                    @endif            
                                    </span>
                                    <span class="costing-text">
                                    @if($plan->type == "Core")    
                                        <span class="dollar-sign-sup"><span class="doller-sign dollar-sign-sup">$</span></span>{{$plan->yearly_price}}
                                    @elseif($plan->type == "Pro")
                                        <span class="dollar-sign-sup"><span class="doller-sign dollar-sign-sup">$</span></span>{{$plan->yearly_price}}
                                    @endif
                                    </span>
                                    <span class="user-text-wrap"><span class="user-text">
                                    @if($plan->type == "Core")    
                                        1 user
                                    @elseif($plan->type == "Pro")
                                        2 users
                                    @endif
                                    </span> year</span>
                                </li>                                
                                <li class="plan-description plan-brdr-btm">
                                    <span class="saving-text plan-or-text">OR</span>
                                    <span class="costing-text">
                                        @if($plan->type == "Core")    
                                            <span class="dollar-sign-sup"><span class="doller-sign dollar-sign-sup">$</span></span>{{$plan->monthly_price}}
                                        @elseif($plan->type == "Pro")
                                            <span class="dollar-sign-sup"><span class="doller-sign dollar-sign-sup">$</span></span>{{$plan->monthly_price}}
                                        @endif 
                                    </span>
                                    <span class="user-text-wrap"><span class="user-text">
                                    @if($plan->type == "Core")    
                                        1 user
                                    @elseif($plan->type == "Pro")
                                        2 users
                                    @endif</span> month</span>
                                </li>
                                @if($plan->type == "Core") 
                                    <!-- Add subscriber start -->
                                    <li class="plan-description add-user-new">
                                    <span class="saving-text plan-or-text">ADD</span>
                                        <span class="costing-text">
                                        <span class="dollar-sign-sup-add"><span class="doller-sign dollar-sign-sup" style="font-size: 1.2rem;">$</span></span>{{$plan->user_unit_price}}
                                        </span>
                                        <span class="user-text-wrap"><span class="user-text">
                                        1 user</span> month
                                        <span class="mx-two-text mx-two-text2">add max {{$plan->extra_users}} subscribers</span>
                                    </span>
                                    </li>
                                    <!-- Add subscriber End -->
                                @endif
                            </ul>
                            <i class="clearfix"></i>
                            @if($plan->type == "Core")
                                <ul class="list-unstyled mt-3 pb-2 ul-list-text">
                                    <li>
                                        <i class="fa fa-check"></i> Monster-BOM upload
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i> buy with a single PO
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i> personal dashboard
                                    </li>
                                    <li>
                                        <ul class="list-unstyled inner-list-text">
                                            <li><span class="text-leftbox">-</span> <span class="text-rightbox">share with other DistiMonster subscribers in your company</span></li>
                                            <li><span class="text-leftbox">-</span> <span class="text-rightbox">manage Monster-BOMs, orders/reorders</span></li> 
                                        </ul>
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i> 5M data sheet access
                                    </li>
                                    <li class="overflow-text">
                                        <i class="fa fa-check"></i>Add maximum of two subscribers for <span class="doller-sign">$</span>{{$plan->user_unit_price}}/each per month
                                    </li>
                                </ul>
                            @elseif($plan->type == "Pro")
                                <ul class="list-unstyled mt-3 pb-2 ul-list-text">
                                    <li>
                                        <i class="fa fa-check"></i> Monster-BOM upload
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i> buy with a single PO
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i> personal dashboard
                                    </li>
                                    <li>
                                        <ul class="list-unstyled inner-list-text">
                                            <li><span class="text-leftbox">-</span> <span class="text-rightbox">share with other DistiMonster subscribers in your company</span></li>
                                            <li><span class="text-leftbox">-</span> <span class="text-rightbox">manage Monster-BOMs, orders/reorder</span></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i> 5M data sheet access
                                    </li>
                                    <li class="overflow-text">
                                        <i class="fa fa-check"></i> <span>cross-reference semiconductor and IP&E data<span>
                                    </li>
                                    <li>
                                        <ul class="list-unstyled inner-list-text">
                                            <li><span class="text-leftbox">-</span> <span class="text-rightbox">compare similar parts; view package types</span></li>
                                            <li><span class="text-leftbox">-</span> <span class="text-rightbox">categories are grouped with identified upgrades/downgrades</span></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i> EOL alerts
                                    </li>
                                    <li>
                                        <i class="fa fa-check"></i> PCN alerts
                                    </li>
                                </ul>
                            @endif
                            </div>
                            @if($plan->type == "Core")
                                <a href="{{url('subscribe/'.base64_encode($plan->type))}}" type="button" class="btn btn-custom plan_type getfree-trial-btn" name="plan_type">GET FREE TRIAL</a>
                            @elseif($plan->type == "Pro")    
                                <button type="button" class="btn btn-custom">AVAILABLE OCT &nbsp;1</button>
                            @endif        
                        </div>
                    </div>
                </div>
            </div>
            <!--Single plan End-->
            @endforeach
            @endif
        </div>
    </div>
</section>
<!--Distimonster Section End-->
<div class="text-center w-100 paln-and-pricing my-5">
    <h2>FOR VOLUME DISCOUNTS OR MORE INFORMATION <br />CONTACT US AT 818-857-5788.</h2>
</div>
@stop
@section('javascript')
<script>
$('#register_form').parsley();
</script>
@endsection