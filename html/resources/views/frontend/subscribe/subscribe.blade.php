@extends('frontend.layouts.app')
@section('title', '| Subscribe')
@section('content')
<link href="{{url('frontend/css/subscribe_style.css')}}" rel="stylesheet">
<!--Register Section-->
<section class="gray-bg p-b75">
<div class="container clearfix">
    <div class="col-md-12">
        <div class="shdow-box">
            <div class="register-box-content">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="block-title text-center">Consolidation with Intelligence</h2>
                        <div class="col-md-12">
                            <form class="form cf msfrm" name="subscribe_frm" id="subscribe_frm" method="POST" action="javascript:void(0);" data-parsley-validate="">
                                {{ csrf_field() }}
                                <input type="hidden" name="plan_type" id="planType" value="{{$planType}}"/>
                                <input type="hidden" name="user_total_amount" id="userTotalAmount" value="0"/>
                                <input type="hidden" name="grand_total_amount" id="grandTotalAmount" value="0"/>
                                @if(!empty($subscription_plans))
                                        @foreach($subscription_plans as $plan)
                                            @if(base64_decode($planType) == $plan->type)
                                                <input type="hidden" name="monthly_price" id="monthlyPrice" value="{{base64_encode($plan->monthly_price)}}"/>
                                                <input type="hidden" name="yearly_price" id="yearlyPrice" value="{{base64_encode($plan->yearly_price)}}"/>
                                                <input type="hidden" name="user_unit_price" id="userUnitPrice" value="{{base64_encode($plan->user_unit_price)}}"/>
                                                <input type="hidden" name="free_users" id="freeUsers" value="{{base64_encode($plan->free_users)}}"/>
                                            @endif
                                        @endforeach
                                @endif
                                <div class="wizard">
                                    <div class="wizard-inner">
                                        <div class="connecting-line"></div>
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="nav-item m-l0"><a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Company Information" class="nav-link active"><span class="round-tab"><i class="fa fa-user-plus"></i></span></a></li>
                                            <li role="presentation" class="nav-item m-auto"><a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="User Information" class="nav-link disabled"><span class="round-tab"><i class="fa fa-info"></i></span></a>
                                            <a href="javascript:void(0);" style="display:none;" class="back-button"><span class="round-txt" ><i class="fa fa-angle-left"></i>BACK</span></a>
                                            </li>
                                            <li role="presentation" class="nav-item m-r0"><a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Billing Information" class="nav-link disabled"><span class="round-tab"><i class="fa fa-money"></i></span></a>
                                            <a href="javascript:void(0);" style="display:none;" class="back-button"><span class="round-txt" ><i class="fa fa-angle-left"></i>BACK</span></a>    
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                        <div class="tab-pane active text-center" role="tabpanel" id="step1">
                                            <h1 class="text-md-center reg-heading">Company Information</h1>
                                            <div style="clear:both;">&nbsp;</div>
                                            <h2 class="index-heading text-center green-bg free-text cstmre-heading">STEP ONE TO FREE 30 DAY TRIAL.</h2>
                                            <div style="clear:both;">&nbsp;</div>
                                            <div class="row">
                                                <div class="col-md-6 pull-left">
                                                    <div class="form-group">
                                                        <div class="col-md-12 p-l0">
                                                            <input type="text" name="company_name" id="company_name" value="{{ old('company_name') }}" placeholder="Company Name*" required="" class="form-control" tabindex="1" data-parsley-required-message="Please enter company name."/>
                                                            <p class="help-block"></p>
                                                            @if($errors->has('company_name'))
                                                                <p class="help-block">
                                                                    {{ $errors->first('company_name')}}
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-12 p-l0">
                                                            <select name="company_type" id="company_type" required="" onchange="showOther(this.value)" class="form-control" tabindex="4" data-parsley-required-message="Please select company type.">
                                                                <option value="">-- Select Company Type --</option>
                                                                <option value="OEM &dash; Original Equipment Manufacturer">OEM &dash; Original Equipment Manufacturer</option>
                                                                <option value="ODM &dash; Original Design Manufacturer">ODM &dash; Original Design Manufacturer</option>
                                                                <option value="EMS &dash; Electronic Manufacturing Service">EMS &dash; Electronic Manufacturing Service</option>
                                                                <option value="University">University</option>
                                                                <option value="Other">Other</option>
                                                            </select>
                                                            <p class="help-block"></p>
                                                            @if($errors->has('company_type'))
                                                                <p class="help-block">
                                                                    {{ $errors->first('company_type') }}
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group company_type_hidden" style="display:none;">
                                                        <div class="col-md-12 p-l0">
                                                            <input type="text" name="company_type_other" id="company_type_other" class="form-control" placeholder="Company Type" tabindex="6"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 pull-right">
                                                    {{--<div class="form-group">
                                                        <div class="col-md-12 p-l0">
                                                            <input type="number" name="year_of_establish" id="year_of_establish" value="{{ old('year_of_establish') }}" placeholder="Year of Established* (eg: 1961, 1985 etc)" required="" max="{{date('Y')}}" class="form-control" maxlength="4" tabindex="2" data-parsley-required-message="Please enter year of established."/>
                                                            <p class="help-block"></p>
                                                            @if($errors->has('year_of_establish'))
                                                                <p class="help-block">
                                                                    {{ $errors->first('year_of_establish') }}
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>--}}
                                                    <div class="form-group">
                                                        <div class="col-md-12 p-l0">
                                                            <select name="employee" id="employee" required="" class="form-control" tabindex="3" data-parsley-required-message="Please select number of employee.">
                                                                <option value="">-- Select Number of Employee --</option>
                                                                <option value="1-9">1-9</option>
                                                                <option value="10-49">10-49</option>
                                                                <option value="50-99">50-99</option>
                                                                <option value="100-249">100-249</option>
                                                                <option value="250-499">250-499</option>
                                                                <option value="500+">500+</option>
                                                            </select>
                                                            <p class="help-block"></p>
                                                            @if($errors->has('employee'))
                                                                <p class="help-block">
                                                                    {{ $errors->first('employee') }}
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-12 p-l0">
                                                            <select name="annual_component_purchase" id="annual_component_purchase" required="" class="form-control" tabindex="5" data-parsley-required-message="Please select annual component purchase.">
                                                                <option value="">-- Select Annual Component Purchase --</option>
                                                                <option value="0 - 99,999">0 - 99,999</option>
                                                                <option value="100,000 - 249,999">100,000 - 249,999</option>
                                                                <option value="250,000 - 499,999">250,000 - 499,999</option>
                                                                <option value="500,000 - 999,999">500,000 - 999,999</option>
                                                                <option value="1,000,000 - 4,999,999">1,000,000 - 4,999,999</option>
                                                                <option value="5,000,000 - 9,999,999">5,000,000 - 9,999,999</option>
                                                                <option value="10,000,000 +">10,000,000 +</option>
                                                            </select>
                                                            <p class="help-block"></p>
                                                            @if($errors->has('annual_component_purchase'))
                                                                <p class="help-block">
                                                                    {{ $errors->first('annual_component_purchase') }}
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-12 p-l0">
                                                            <textarea name="know_about_distimonster" id="know_about_distimonster" required="" class="form-control" placeholder="Please indicate how you found out about DistiMonster" tabindex="7" data-parsley-required-message="Please enter how you found Distimonster.">{{ old('know_about_distimonster') }}</textarea>
                                                            <p class="help-block"></p>
                                                            @if($errors->has('know_about_distimonster'))
                                                                <p class="help-block">
                                                                    {{ $errors->first('know_about_distimonster') }}
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                     
                                            <div class="form-group">
                                                <ul class="list-inline text-md-center">
                                                    <li><button type="button" id="step_1_btn" class="btn btn-lg btn-common next-step next-button" tabindex="8">Get Started Now</button></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="tab-pane" role="tabpanel" id="step2">
                                            <h1 class="text-md-center reg-heading">Subscriber Information</h1>
                                            <div style="clear:both;">&nbsp;</div>
                                            <h2 class="index-heading text-center green-bg free-text cstmre-heading">ON YOUR WAY TO 30 DAY FREE TRIAL.</h2>
                                            <div style="clear:both;">&nbsp;</div>
                                            <div class="row">
                                                <div class="col-md-6 pull-left">
                                                    <div class="form-group">
                                                        <div class="col-md-11 p-l0">
                                                            <input type="text" name="name" id="name" value="{{ old('name') }}" placeholder="First Name*" required="" class="form-control" tabindex="1" data-parsley-required-message="Please enter your first name."/>
                                                            <p class="help-block"></p>
                                                            @if($errors->has('name'))
                                                                <p class="help-block">
                                                                    {{ $errors->first('name')}}
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <div class="col-md-11 p-l0">
                                                            <input type="text" name="department" id="department" value="{{ old('department') }}" placeholder="Department" required="" class="form-control" tabindex="3"  data-parsley-required-message="Please enter your department name."/>
                                                            <p class="help-block"></p>
                                                            @if($errors->has('department'))
                                                                <p class="help-block">
                                                                    {{ $errors->first('department') }}
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <div class="col-md-11 p-l0">
                                                            <input type="text" name="contact_number" id="contact_number" value="{{ old('contact_number') }}" placeholder="Phone Number" required="" class="form-control" tabindex="5" data-parsley-required-message="Please enter your valid contact number." data-parsley-pattern="^[0-9]*$" data-parsley-pattern-message="Please enter only numbers." />
                                                            <p class="help-block"></p>
                                                            @if($errors->has('contact_number'))
                                                                <p class="help-block">
                                                                    {{ $errors->first('contact_number') }}
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 pull-right">
                                                    <div class="form-group">
                                                        <div class="col-md-11 p-l0">
                                                            <input type="text" name="last_name" id="last_name" value="{{ old('last_name') }}" placeholder="Last Name*" required="" class="form-control" tabindex="2" data-parsley-required-message="Please enter your last name."/>
                                                            <p class="help-block"></p>
                                                            @if($errors->has('last_name'))
                                                                <p class="help-block">
                                                                    {{ $errors->first('last_name')}}
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-11 p-l0">
                                                            <input type="text" name="title_or_job" id="title_or_job" value="{{ old('title_or_job') }}" placeholder="Title or  Job Function" required="" class="form-control" tabindex="4" data-parsley-required-message="Please enter your title or job function."/>
                                                            <p class="help-block"></p>
                                                            @if($errors->has('title_or_job'))
                                                                <p class="help-block">
                                                                    {{ $errors->first('title_or_job') }}
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-11 p-l0">
                                                            <input type="text" name="email" id="email" value="{{ old('email') }}" placeholder="Email Address" required="" class="form-control unique_email" tabindex="6" data-parsley-required-message="Please enter your valid email." pattern="{{config('app.patterns.email')}}" data-parsley-trigger="focusout" data-parsley-remote="{{url('check-newemail')}}" data-parsley-remote-message="Email address is already in use."/>
                                                            <p class="help-block"></p>
                                                            @if($errors->has('email'))
                                                                <p class="help-block">
                                                                    {{ $errors->first('email') }}
                                                                </p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-11 p-l0">
                                                            <p class="add-sub-text">Add up to two additional subscribers for $100 each per month.
                                                            </p>
                                                            <p>
                                                            <span class="m-r10"><input type="radio" name="add_user" id="add_user" class="add_user" value="yes"/>&nbsp; <label class="f-s16">Add Subscriber</label></span>
                                                            <span>
                                                            <input type="radio" name="add_user" id="add_user" class="add_user" value="no" checked="checked" tabindex="7"/>&nbsp; <label class="f-s16">Not at this time (you can add at a later date)</label>
                                                        </span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="czContainer" style="width:90% !important;" class="subscribers-block">
                                                    <p id="addSubscriberTopNote" style="display:none;" class="f-x-s16">
                                                    Register additional subscriber(s) below. They will receive a DistiMonster welcome email with their login information.
                                                    </p>    
                                                    <div id="first">
                                                        <div class="col-md-12 recordset">
                                                        <div class="add-new-member">
                                                            <table class="edit-listing ">
                                                                <tr>
                                                                    <td class="p-l0"><input type="text" name="user_first_name[]" placeholder="First Name" required="" class="form-control dynamic_frm" data-parsley-required-message="Please enter member first name."/></td>
                                                                    <td> <input type="text" name="user_last_name[]" placeholder="Last Name" required="" class="form-control dynamic_frm" data-parsley-required-message="Please enter member last name."/></td>
                                                                    <td class="email-block"><input type="email" name="user_email[]" placeholder="Email" required="" class="form-control dynamic_frm unique_email" data-parsley-required-message="Please enter member email."/>
                                                                    <ul class="parsley-errors-list filled customexist" style="display: none;">
                                                                        <li class="parsley-remote">Email address is already in use.</li>
                                                                    </ul></td>
                                                                    <td>  <textarea name="user_note[]" placeholder="Note" class="form-control dynamic_frm" style="border-radius:100px"></textarea></td>
                                                                </tr>
                                                                <hr />
                                                            </table>
                                                            </div>
                                                            <div class="clearfix">&nbsp;</div>
                                                    </div>
                                                </div>
                                            </div>
                                            </div>
                                            <div style="clear:both;">&nbsp;</div>
                                            <div style="clear:both;">&nbsp;</div>
                                            <div class="form-group">
                                                <ul class="list-inline text-md-center">
                                                    <li><button type="button" id="step_2_btn" class="btn btn-lg btn-common next-step next-button" tabindex="8">Next Step</button></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="tab-pane" role="tabpanel" id="step3">
                                            <h1 class="text-md-center text-center reg-heading">Billing Information</h1>
                                            <div style="clear:both;">&nbsp;</div>
                                            <h2 class="index-heading text-center green-bg free-text cstmre-heading">NO WORRIES — TRY  BEFORE YOU BUY.</h2>
                                            <div style="clear:both;">&nbsp;</div>
                                            <div class="row clearfix">
                                                    <div class="col-md-6" pull-left>
                                                        <div class="form-group" style="display:none;">
                                                            <div class="col-md-11 p-l0">
                                                                <input type="text" name="billing_company_name" id="billing_company_name" value="{{ old('billing_company_name') }}" placeholder="Company Name*" class="form-control" tabindex="1" data-parsley-required-message="Please enter billing company name."/>
                                                                <p class="help-block"></p>
                                                                @if($errors->has('billing_company_name'))
                                                                    <p class="help-block">
                                                                        {{ $errors->first('billing_company_name')}}
                                                                    </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-11 p-l0">
                                                                <select name="billing_country" id="billing_country" onchange="getState(this.value);" required="" class="form-control" tabindex="4" data-parsley-required-message="Please select billing country.">
                                                                    <option value="">-- Select Country --</option>
                                                                    @foreach($countries as $key => $country)
                                                                    @if(!empty($key))
                                                                    <option {{ $key == 231? 'selected="selected"': ''}} value="{{$key}}">{{$country}}</option>
                                                                    @endif
                                                                    @endforeach
                                                                </select>
                                                                <p class="help-block"></p>
                                                                @if($errors->has('billing_country'))
                                                                    <p class="help-block">{{ $errors->first('billing_country') }}</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-11 p-l0">
                                                                <select name="billing_state" id="billing_state" onchange="getCity(this.value)" class="form-control" required="" tabindex="5" data-parsley-required-message="Please select billing state.">
                                                                    <option value="">-- Select State --</option>
                                                                </select>
                                                                <p class="help-block"></p>
                                                                @if($errors->has('billing_state'))
                                                                    <p class="help-block">{{$errors->first('billing_state')}}</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-11 p-l0">
                                                                <select name="billing_city" id="billing_city" class="form-control" required="" tabindex="6" data-parsley-required-message="Please select billing city.">
                                                                    <option value="">-- Select City --</option>
                                                                </select>
                                                                <p class="help-block"></p>
                                                                @if($errors->has('billing_city'))
                                                                    <p class="help-block">
                                                                        {{ $errors->first('billing_city') }}
                                                                    </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-11 p-l0">
                                                                <select name="billing_cycle" id="billingCycle" required="" class="form-control" tabindex="9" data-parsley-required-message="Please select billing.">
                                                                    <option value="">-- Select Billing --</option>
                                                                    <option value="quarterly">Quarterly</option>
                                                                    <option value="yearly">Yearly</option>
                                                                </select>
                                                                <p class="help-block"></p>
                                                                @if($errors->has('billing_cycle'))
                                                                    <p class="help-block">{{ $errors->first('billing_cycle') }}</p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6" pull-right>
                                                        <div class="form-group">
                                                            <div class="col-md-11 p-l0">
                                                                <input type="text" name="billing_street_address" id="billing_street_address" value="{{ old('billing_street_address') }}" placeholder="Billing Street Address*" required="" class="form-control" tabindex="2" data-parsley-required-message="Please enter billing address."/>
                                                                <p class="help-block"></p>
                                                                @if($errors->has('billing_street_address'))
                                                                    <p class="help-block">
                                                                        {{ $errors->first('billing_street_address')}}
                                                                    </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-11 p-l0">
                                                                <input name="zip_code" id="zip_code" value="{{ old('zip_code') }}" placeholder="Billing Zip Code*" required="" class="form-control" maxlength="10" tabindex="3" data-parsley-required-message="Please enter valid Zip code."/>
                                                                <p class="help-block"></p>
                                                                @if($errors->has('zip_code'))
                                                                    <p class="help-block">
                                                                        {{ $errors->first('zip_code') }}
                                                                    </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-11 p-l0">
                                                                <input type="text" name="billing_phone" id="billing_phone" class="form-control" required="" placeholder="Billing Phone Number" tabindex="7" value="{{old('billing_phone')}}"  data-parsley-required-message="Please enter billing phone number."/>
                                                                <p class="help-block"></p>
                                                                @if($errors->has('billing_phone'))
                                                                    <p class="help-block">
                                                                        {{ $errors->first('billing_phone') }}
                                                                    </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-11 p-l0">
                                                                <input type="text" name="billing_contact_email" id="billing_contact_email" class="form-control" required="" placeholder="Billing Contact Email" tabindex="8" value="{{old('billing_contact_email')}}" data-parsley-required-message="Please enter billing email."/>
                                                                <p class="help-block"></p>
                                                                @if($errors->has('billing_contact_email'))
                                                                    <p class="help-block">
                                                                        {{ $errors->first('billing_contact_email') }}
                                                                    </p>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            <div style="clear:both;">&nbsp;</div>
                                            <div style="clear:both;">&nbsp;</div>
                                            <fieldset id="billingBlock" style="display:none;">
                                                <h1 class="text-md-center text-md-center reg-heading">Payment Information</h1>
                                                <div class="row">
                                                    <div class="col-md-12 shdow-box p-t20 m-t25 payment-block">
                                                        <div class="form-group m-b0">
                                                            <table class="table">
                                                                <tr>
                                                                    <th>Subscription Plan</th>
                                                                    <td id="billing"></td>
                                                                </tr >
                                                                <tr>
                                                                    <th>Plan Cost</th>
                                                                    <td id="planCost"></td>
                                                                </tr>
                                                                <tr>
                                                                    <th>Additional Subscribers</th>
                                                                    <td id="additionalSubscribers"></td>
                                                                </tr>
                                                                <tr class="subscriber-total">
                                                                    <th>Total</th>
                                                                    <td id="grandTotal"></td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="card-payment col-md-12 shdow-box p-t20 m-t25" id="paymentSection" style="display: block">
                                                        <blockquote>
                                                            <p class="greennote-text">No charge for first 30 days. Billing cycle will begin after first 30 days. You will receive an email notification 5–7 days before the 30 day free trial ends.</p>
                                                        </blockquote>
                                                        <div class="col-md-12">
                                                            <label>Credit Card</label>
                                                            <input type="radio" class="payment_type" name="payment_type" value="credit" required="" data-parsley-required-message="Please select payment method."/> 
                                                            <label>Invoice Me</label>
                                                            <input type="radio" class="payment_type" name="payment_type" value="invoice_me" required="" data-parsley-required-message="Please select payment method."/>   
                                                        </div> 
                                                        <div class="col-md-12" id="credit_card_block" style="display:none;">
                                                           <div class="form-group">
                                                              <div class="col-md-12 p-l0 p-r0">
                                                                  <input type="tel" value="" class = 'form-control card_details' placeholder="Card Number" id="card_number" name="card_number" tabindex="9" data-parsley-required-message="Please enter valid credit card number.">
                                                              </div>
                                                           </div>
                                                           <div class="form-group">
                                                              <div class="row">
                                                                 <div class="col-md-6 p-r5">
                                                                     <input type="number" value="" class ="form-control card_details" placeholder="Expiry Month (MM)" maxlength="2" id="expiry_month" name="expiry_month" tabindex="10" max="12" data-parsley-required-message="Please enter your card expiry month.">
                                                                 </div>
                                                                 <div class="col-md-6 p-l5">
                                                                     <input type="number" value="" class="form-control card_details" placeholder="Expiry Year (YYYY)" minlength="4" maxlength="4" id="expiry_year" name="expiry_year" tabindex="11" min="{{date('Y')}}" data-parsley-required-message="Please enter your card expiry year.">
                                                                 </div>
                                                              </div>
                                                           </div>
                                                           <div class="form-group">
                                                              <div class="col-md-12 p-l0 p-r0">
                                                                 <input type="tel" value="" class ="form-control card_details" placeholder="CVV" id="cvv" name="cvv" tabindex="12" maxlength="4" data-parsley-required-message="Please enter your CVV number.">
                                                              </div>
                                                           </div>
                                                           <div class="form-group m-b0">
                                                              <div class="row">
                                                                 <div class="col-md-6 p-r5">
                                                                    <input type="text"  value="{{ old('first_name_on_card') }}" class = 'form-control' placeholder="First Name " id="first_name_on_card" name="first_name_on_card" tabindex="13" data-parsley-required-message="Please enter your first name which is mentioned on the card.">
                                                                 </div>
                                                                 <div class="col-md-6 p-l5">
                                                                    <input type="text"  value="{{ old('last_name_on_card') }}" class = 'form-control' placeholder="Last Name" id="last_name_on_card" name="last_name_on_card" tabindex="14" data-parsley-required-message="Please enter your last name which is mentioned on the card.">
                                                                 </div>
                                                              </div>
                                                           </div>
                                                           <div class="form-group">
                                                              <div class="col-md-12 p-l0 p-r0">
                                                                 <input type="hidden" name="card_type" id="card_type" value="visa"/>
                                                              </div>
                                                           </div>
                                                           <div id="orderInfo" style="display: none;"></div>
                                                        </div>
                                                        <div class="col-md-12" id="invoice_me_block" style="display:none;">
                                                            <div class="form-group">
                                                                <div class="col-md-12 p-l0 p-r0">
                                                                    <input type="text" value="" class ="form-control invoice_me_input" placeholder="Accounts Payable Contact Name" id="contact_name" name="contact_name" maxlength="200" data-parsley-required-message="Please enter your accounts payable contact name.">
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <div class="col-md-12 p-l0 p-r0">
                                                                    <input type="email" value="" class ="form-control invoice_me_input" placeholder="Accounts Payable Contact Email" id="contact_email" name="contact_email" maxlength="200" data-parsley-required-message="Please enter your accounts payable contact email.">
                                                                </div>
                                                            </div>    
                                                        </div>
                                                     </div>
                                                </div>
                                            </fieldset>
                                            <div style="clear:both;">&nbsp;</div>
                                            <div style="clear:both;">&nbsp;</div>
                                            <div class="form-group">
                                                <ul class="list-inline text-md-center">
                                                    <li><button type="submit" id="step_3_btn" class="btn btn-lg btn-common next-button" tabindex="15">Submit</button></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!--Register Section-->
@stop
@section('javascript')
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="{{url('frontend/js/jquery.czMore-1.5.3.2.js')}}"></script>
<script src="{{url('frontend/js/masking.js')}}"></script>
<script src="{{url('frontend/js/subscribe.js')}}"></script>
@endsection