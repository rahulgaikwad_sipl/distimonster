<!--START:Top Navigation-->
<!--Header-->
<nav class="navbar p-t0 p-b0">
    <div class="nav-container custom-header p-b0">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right d-md-none d-lg-none" type="button" data-toggle="collapse"
                data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-brand mb-0 "><i class="fa fa-bars"
                        aria-hidden="true"></i></span></button>
            <div class="row">
                <div class="navbar-header col-md-2">
                    <a class="navbar-brand" href="{{url('/')}}"><img src="{{url('frontend/images/logo.png') }}"
                            class="img-responsive"></a>
                </div>
                <div class="col-md-10 ">
                    <div class="row">
                        <div class="col-md-12">
                            <!--START: Header Top-->
                            <div class="header-top">
                                <div class="text-right">
                                    <ul class="nav navbar-nav navbar-right checkout m-t15 hidden-xs">
                                        @if(Cart::instance('quotecart')->content()->count() > 0)
                                        <li><a href="{{url('/quote-cart')}}">
                                                <div class="icon">
                                                    <span class="fa-icon"><i class="fa fa-file-text"
                                                            aria-hidden="true"></i></span>
                                                    <em class="cart-count"
                                                        id="quoteCount">{{Cart::instance('quotecart')->content()->count()}}</em>
                                                    <span style="font-weight: 500; color: #000; vertical-align: bottom">
                                                        Quote Requests</span>
                                                </div>
                                            </a>
                                        </li>
                                        @else
                                        <li><a href="{{url('/quote-cart')}}">
                                                <div class="icon">
                                                    <span class="fa-icon"><i class="fa fa-file-text"
                                                            aria-hidden="true"></i></span>
                                                    <em style="display: none;" class="cart-count" id="quoteCount"></em>
                                                    <span> Quote Requests</span>
                                                </div>
                                            </a>
                                        </li>
                                        @endif
                                        @if(Cart::instance('shoppingcart')->content()->count() > 0)
                                        <li><a href="{{url('/cart')}}">
                                                <div class="icon">
                                                    <span class="fa-icon"><i class="fa fa-shopping-cart"
                                                            aria-hidden="true"></i></span>
                                                    <em class="cart-count"
                                                        id="cartCount">{{Cart::instance('shoppingcart')->content()->count()}}</em>
                                                    <span> Cart</span>
                                                </div>
                                            </a>
                                        </li>
                                        @else
                                        <li><a href="{{url('/cart')}}">
                                                <div class="icon">
                                                    <span class="fa-icon"><i class="fa fa-shopping-cart"
                                                            aria-hidden="true"></i></span>
                                                    <em class="cart-count" id="cartCount" style="display:none">0</em>
                                                    <span class="cartCount">Cart</span></div><br><span
                                                    id="totalPrice"></span>
                                            </a></li>
                                        @endif
                                    </ul>
                                    <ul class="header-top-right">
                                        @if (Auth::check())
                                        <li class="hello-user dropdown">
                                            <a href="#" class="dropdown-toggle" href="javascript:void(0)"
                                                id="dropdown02" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                <span class="fa-icon"><i class="fa fa-user-o"
                                                        aria-hidden="true"></i></span>
                                                Hello {{ Auth::user()->name? Auth::user()->name:'Guest!'}}</a>
                                            <div class="dropdown-menu" aria-labelledby="dropdown02">
                                                <a class="dropdown-item"
                                                    href="{{ url('account-settings/'.Auth::user()->id)}}">Account
                                                    Settings</a>
                                                <a class="dropdown-item" href="{{url('order-history')}}">Order
                                                    History</a>
                                                <a class="dropdown-item" href="{{url('quotes')}}">Quotes</a>
                                                <a class="dropdown-item" href="{{url('my-bom')}}">Monster-BOM</a>
                                                <a class="dropdown-item" href="{{url('my-bio')}}">My BIO</a>
                                                @if(Auth::user()->is_sub_user == 0)
                                                <a class="dropdown-item" href="{{url('my-team')}}">My Team</a>
                                                @endif
                                                {{-- <a class="dropdown-item" href="{{url('requested-lead-times')}}">Requested
                                                Lead Times</a> --}}
                                                <?php /*<a class="dropdown-item" href="{{url('apply-net-term-account')}}">Apply for a Net Term Account</a>*/?>
                                            </div>
                                        </li>
                                        <li><a href="{{ url('logout-user')}}">Logout</a></li>
                                        @else
                                        <li class="hello-user"><span class="fa-icon"><i class="fa fa-user-o"
                                                    aria-hidden="true"></i></span><span> Hello Guest!</span></li>
                                        <li>
                                            <div class=" authentication-right-btn">
                                                <a href="{{ url('login')}}">Login &nbsp;or&nbsp;
                                                Register</a>
                                            </div>
                                        </li>
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            <!--END: Header Top-->
                        </div>
                    </div>

                    <div class="row p-t15 m-b15">
                        <div class="col-lg-12 col-md-12 col-sm-12 inner-page-search">
@if ((Route::getCurrentRoute()->uri() != '/'))
                            {!! Form::open(['method' => 'get', 'url' => url('search'), 'class'=>"form minisearch"
                            ,'id'=>"search_part_number",'data-parsley-validate' => true]) !!}
                            <div class="field search clearfix">
                                <?php if(!empty(app('request')->input('partNumber'))){
                        $sr_param = app('request')->input('partNumber');
                    }else{
                        $sr_param = Session::get('search_part_name');
                    }?>
                                <input id="part_name" type="text" name="partNumber" value="{{$sr_param}}"
                                    data-parsley-errors-container="#error-container" data-parsley-required
                                    data-parsley-required-message="Please enter any part number"
                                    placeholder="Search by part number" class="input-text" />
                                <input type="hidden" name="slug_holder" id="slug_holder" value="" />
                                <button type="submit" title="Search" class="action search">
                                    <span>Search</span>
                                </button>
                            </div>
                            <div id="error-container"> </div>
                            <div class="search-history">
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#search-history">Search
                                    History <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            </div>
                            {!! Form::close() !!}                            
                            @endif
                        </div>                     
                    </div>
                    @if (Auth::check())
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="quick-acc">
                                <h4 class="myacc-heading">Quick Account Options</h4>
                                <ul class="myacc-menu">
                                    <li><a href="{{ url('dashboard/')}}">Dashboard</a></li>
                                    <li><a href="{{ url('account-settings/'.Auth::user()->id)}}">Account Settings</a>
                                    </li>
                                    <li><a href="{{url('order-history')}}">Order History</a></li>
                                    <li><a href="{{url('quotes')}}">Quotes</a></li>
                                    <li><a href="{{url('my-bom')}}">Monster-BOM</a></li>
                                    @if(!Session::get("mybio_added"))
                                        <li><a href="{{url('my-bio')}}" style="color:#1889BD;"><strong>My BIO</strong></a></li>
                                    @else
                                        <li><a href="{{url('my-bio')}}">My BIO</a></li>
                                    @endif
                                    
                                    @if(Auth::user()->is_sub_user == 0)
                                    <li><a href="{{url('my-team')}}">My Team</a></li>
                                    @endif
                                    {{--<li><a href="{{url('requested-lead-times')}}">Requested Lead Times</a></li>--}}
                                    <?php /*<li><a href="{{url('apply-net-term-account')}}">Apply for a Net Term Account</a></li>*/?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif

                </div>
                <!-- <div class="col-md-6">
                    <ul class="nav navbar-nav navbar-right checkout m-t15 hidden-xs">
                        @if(Cart::instance('quotecart')->content()->count() > 0)
                            <li><a href="{{url('/quote-cart')}}"><div class="icon"><img src="{{url('frontend/assets/images/icon-quote-cart.png')}}" alt="Quote Requests" />  <em class="cart-count" id="quoteCount">{{Cart::instance('quotecart')->content()->count()}}</em>  </div> <span>Quote Requests</span> <br><span style="font-weight: 500; color: #000;"> </span></a></li>
                        @else
                            <li><a href="{{url('/quote-cart')}}"><div class="icon"><img src="{{url('frontend/assets/images/icon-quote-cart.png')}}" alt="Quote Requests" />  <em style="display: none;" class="cart-count" id="quoteCount"></em>  </div> <span>Quote Requests</span> <br><span style="font-weight: 500; color: #000;"> </span></a></li>
                        @endif
                        @if(Cart::instance('shoppingcart')->content()->count() > 0)
                                <li><a href="{{url('/cart')}}"><div class="icon"><img src="{{url('frontend/assets/images/icon-cart.png')}}" alt="Your Cart" /> <em class="cart-count" id="cartCount">{{Cart::instance('shoppingcart')->content()->count()}}</em></div><span>Your Cart</span> <br><span id="totalPrice">{{Cart::instance('shoppingcart')->subtotal()}}</span></a></li>
                        @else
                            <li><a href="{{url('/cart')}}"><div class="icon"><img src="{{url('frontend/assets/images/icon-cart.png')}}" alt="Your Cart" /> <em class="cart-count" style="display: none;" id="cartCount"></em></div><span>Your Cart</span> <br><span id="totalPrice"></span></a></li>
                        @endif
                    </ul>
                
                
                    <ul class="myacc-menu">
                        <li><a href="#">Account Setting</a></li>
                        <li><a href="#">Order History</a></li>
                        <li><a href="#">Quotes</a></li>
                        <li><a href="#">My BOM</a></li>
                        <li><a href="#">Apply dor a Net Term Account</a></li>
                    </ul>
                </div> -->
            </div>
        </div>
        @if (!Auth::check())
                        @if ((Route::getCurrentRoute()->uri() != '/coming-soon') || (Route::getCurrentRoute()->uri() ==
                        '/'))
                            <div class="free-trial-box">
                                <a href="{{url('/subscription-plan')}}" class="btn btn-success text-right free-trial"
                                    style="pointer-events: visible;">START YOUR FREE TRIAL</a>
                            </div>
                        @endif
                        @endif
    </div>
</nav>

<!--Header end-->
<!--Navigation-->