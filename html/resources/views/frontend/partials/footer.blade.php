<!--START: Footer-->
@inject('request', 'Illuminate\Http\Request')
<h2 class="index-heading text-center green-bg free-text"><a href="{{url('/subscription-plan')}}">FREE — TRY DISTIMONSTER FOR 30 DAYS.</a></h2>
<section id="footer">
    <div class="container footer">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 text-center">
               <img src="{{url('frontend/assets/images/disti-white-logo.png')}}" class="img-responsive">
                <ul class="address-list">
               <li class="address-list-item">
                 <a href="https://twitter.com/distimonster" title="Twitter" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
             <li class="address-list-item">
                 <a href="https://fb.me/distimonster" title="Facebook" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li class="address-list-item">
                 <a href="https://www.linkedin.com/company/distimonster/about/?viewAsMember=true" title="Linkedin" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
             <li class="address-list-item">
                 <a href="https://www.instagram.com/distimonster/" title="Instagram" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 p-t50">
                
                <ul class="list m-b0 bar-heading">
                    <li class="list-item"><a href="{{url('products')}}">PRODUCTS</a></li>
                    <li class="list-item"><a href="{{url('bom')}}">Monster-BOM</a></li>
                    <li class="list-item"><a href="{{url('return-policy')}}">SUPPORT</a></li>
                    <li class="list-item"><a href="{{url('about')}}">ABOUT</a></li>
                    <li class="list-item"><a href="{{url('faq')}}">FAQs</a></li>
                    <li class="list-item"><a href="{{url('contact')}}">CONTACT</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 blank-title p-t50">
                  
                <ul class="list bar-heading">
                    <li class="list-item"><a href="{{url('return-policy')}}">Return Policy</a></li>
                    
                    <li class="list-item"><a href="{{url('news-updates')}}">News &amp; Updates</a></li>

                    <li class="list-item"><a href="{{url('terms-of-use')}}" target="_blank">Privacy Policy &amp;<br> Terms of Use</a></li>
                    
                    <li class="list-item"><a href="{{url('sitemap')}}">SITE MAP</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 p-t50">
                <h3 class="bar-heading">Sign up <span style="text-transform: lowercase;font-size: 14px;">for the<br></span>DISTIMONSTER <span style="text-transform: capitalize;font-size: 14px;">Newsletter</span> </h3>
                    {!! Form::open(['method' => 'post','class'=>"newsletter-form" ,'id'=>"newsletter_subscribe",'data-parsley-validate' => true]) !!}
                    <p>Get the latest news, special offers and other discount information.</p>
                    <div class="newsletter-form-item">
                        {!! Form::email('emailNewsletter', old('emailNewsletter'), ['pattern' => config('app.patterns.email'),'data-parsley-errors-container'=>'#errorContainer' ,'id'=>'emailNewsletter','placeholder' => 'Email Address','data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE') ]) !!}
                        <a onclick="footer.signUpNewsLetter()" class="newsletter-btn"> </a>
                        {{--<input type="button" value="Submit" title="Submit" class="newsletter-btn" />--}}
                    </div>
                </form> <div id="errorContainer"></div>
               <!--  <ul class="social-list">
                    <li class="social-list-item"><a href="#" title="Facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li class="social-list-item"></li>
                    <li class="social-list-item"><a href="#" title="Google+"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li class="social-list-item"><a href="#" title="Linkedin"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                </ul> -->
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3"><div class="payment-card"><img src="{{url('frontend/assets/images/payment-card.png')}}" class="img-responsive"></div></div>
            <div class="col-lg-9">
                <p class="copyright">&copy; 2019 Copyright DistiMonster. All rights Reserved. </p>
            </div>
        </div>
    </div>
    <a id="back-to-top" href="javascript:void(0)" class="back-to-top" role="button" title="" data-toggle="tooltip" data-placement="left"><i class="fa fa-angle-right"></i></a>
</section>
<!-- Modal -->
<div class="modal" id="quote-detail-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalLabel">Request a Quote</h4>
                <button type="button" class="close" data-dismiss="modal" onclick="quoteModule.closeAddToQuoteModel()" aria-label="Close">
                    <span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon" /></span>
                </button>
            </div>
            <div class="modal-body">
                {!! Form::open(['class'=>'form-horizontal','method' => 'POST', 'id' => 'add_quote_detail_form', 'data-parsley-validate' => true]) !!}
                <div class="price-content-box">
                    <div class="price-content-body">
                        <div class="clearfix m-b5 f-s14"></div>
                        <div class="form-group">
                            <div class="col-md-12 p-l0">
                                <input type="text" name="popupRequestedQuantity" data-parsley-required data-parsley-type="digits"  data-parsley-maxlength="10"  value="" id="popupRequestedQuantity" placeholder="Requested Quantity" class="quantity-box"  min="1"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 p-l0">
                                <input type="text" name="popupRequestedCost" data-parsley-pattern="^\s*(?=.*[1-9])\d*(?:\.\d{1,6})?\s*$" data-parsley-required data-parsley-maxlength="20" data-parsley-type="number" value="" id="popupRequestedCost" placeholder="Target Price (per unit in $)" class="quantity-box" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12 p-l0">
                                <input type="text" readonly name="popupRequestedDeliveryDate" value="" class="requested-delivery-date quantity-box" id="popupRequestedDeliveryDate" placeholder="Requested Delivery Date"  />
                                <input type="hidden" id="quote_type" name="quote_type" value="" />
                            </div>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
            <div class="modal-footer">
                <button type="submit" data-dismiss="modal" class="btn btn-primary btn-outline" title="Cancel" onclick="quoteModule.closeAddToQuoteModel()"><span class="img-icon"><img src="{{url('frontend/assets/images/pop-up-close-icon.png')}}" alt="pop-up-close-icon" /></span>Cancel</button>
                <input type="hidden" name="cartItems" id="cartItems"  value="" />
                <button type="button" onclick="quoteModule.addToQuoteDetailFromModal()" class="btn btn-primary" title="Add To Quote List"><span class="img-icon"><img src="{{url('frontend/assets/images/add-white-icon.png')}}" alt="pop-up-edit-icon" /></span>Add To Quote List </button>
            </div>

        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="search-history" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title f-s18" id="modalLabel">Recent Searches</h4>
                <button id="close_search_popup" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon" /></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <ul id="recent_search_holder">
                            <?php
                            if(Session::has('searchProducts')) {
                            $allSearchProducts = Session::get('searchProducts');
                            foreach($allSearchProducts as $key=>$search)
                            {
                            ?>

                            <li class="clearfix">
                                <a href="{{url('search?partNumber='.$search['product_name'])}}">
                                    <span class="pull-left f-s14 black font-w500">"{{ $search['product_name'] }}"</span>
                                    <span class="pull-right text-gray f-s14">{{ $search['total_counter'] }} Results</span>
                                </a>
                            </li>
                            <?php } } else { ?>
                            No Recent Searches
                            <?php } ?>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="modal-footer text-right">
                <button type="button"  id="clearSearch" onclick="productListModule.clearSearchHistory()" class="btn btn-primary btn-outline" title="Clear History"><span class="img-icon"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon" /></span>Clear History</button>
            </div>
        </div>
    </div>
</div>

<!--END: Footer-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script type="text/javascript"  src="{{asset('frontend/js/jquery.min.js') }}"></script>
<script src="{{ url('frontend/js/cookiesConsent.js') }}"></script>
<script type="text/javascript"  src="{{asset('frontend/js/jquery-migrate.min.js') }}"></script>
<script type="text/javascript" src="{{asset('frontend/js/tether.min.js') }}"></script>
<script type="text/javascript" src="{{url('frontend/js/site.js')}}"></script>
<script type="text/javascript"  src="{{url('frontend/js/new-bootstrap.min.js')}}"></script>
<script type="text/javascript"  src="{{url('frontend/js/bootstrap-datepicker.min.js')}}"></script>
<script  type="text/javascript"  src="{{ asset('frontend/js/parsley.min.js') }}"></script>
<script  type="text/javascript"  src="{{ asset('frontend/js/select2.full.js') }}"></script>
<script type="text/javascript"  src="{{ asset('adminlte/js/notify.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/payments.js') }}"></script>
<script type="text/javascript"  src="{{ asset('frontend/js/sweetalert.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/jquery.creditCardValidator.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/jquery.easy-autocomplete.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/common.js') }}"></script>
<script type="text/javascript">
$( document ).ready(function() {
$('#shipping_option_id').select2({
    dropdownAutoWidth: true
});
});
    var successOptions = {
        autoHideDelay: 8000,
        showAnimation: "fadeIn",
        hideAnimation: "fadeOut",
        hideDuration: 700,
        style: 'bootstrap',
        arrowShow: false,
        className: "success"
    };

    var errorOptions = {
        autoHideDelay: 8000,
        showAnimation: "fadeIn",
        hideAnimation: "fadeOut",
        hideDuration: 700,
        style: 'bootstrap',
        arrowShow: false,
        className: "error"
    };

    var warningOptions = {
        autoHideDelay: 5000,
        showAnimation: "fadeIn",
        hideAnimation: "fadeOut",
        hideDuration: 700,
        style: 'bootstrap',
        arrowShow: false,
        className: "warn"
    };

    $.notify('<?php echo  Session::get('success')?>', successOptions);
    $.notify('<?php echo  Session::get('failure')?>', errorOptions);
    $.notify('<?php echo  Session::get('warning')?>', warningOptions);
</script>
