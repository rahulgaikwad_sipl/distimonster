<ul class="tabs shdow-box">
	<li  class="{{Request::is('faq') ? 'active' : '' }}"rel="tab1"> <a href="{{url('faq')}}">Frequently Asked Questions</a></li>
	<li class="{{Request::is('return-policy') ? 'active' : '' }}" rel="tab2"><a href="{{url('return-policy')}}">Return Policy</a></li>
	<li class="{{Request::is('contact') ? 'active' : '' }}" rel="tab3"> <a href="{{url('contact')}}">Contact</a></li>
	<li class="{{Request::is('request-a-demo') ? 'active' : '' }}"  rel="tab4"> <a href="{{url('request-a-demo')}}">Request a Demo</a></li>
	<li rel="tab5"> <a href="javascript:void(0);" id="chat_link">Live Chat</a></li>
</ul>