<!--START: Banner-->
<div class="banner custom-header">
    <img src="{{url('frontend/assets/images/main-banner.png')}}" class="w-full img-responsive d-none d-sm-block">
    <div class="banner-caption">
        <!--        <h2 class="text-center">One Stop Shop for all Electronic Components</h2>
                <h5 class="banner-h5 text-center">Access to millions of in stock components & data sheets from manufacturers and franchised distributors</h5>-->
        <div class="col-md-12 banner-btm-heading"><h2 class="text-center" style="color: white;">CONSOLIDATION WITH INTELLIGENCE</h2>
            <p class="sub-heading">Design and buy components on one site.</p>

        </div>
        <div class="row">
            <div class="col-md-6 buyer">
                <div>
                    <a href="{{url('/login')}}" class="btn btn-success m-t0" style="pointer-events: visible;">Buyer</a>                   
                </div>
            </div>
            <div class="col-md-6 engineer">
                <div class="pull-right">
                    <a href="{{url('/engineering')}}" class="btn btn-primary">Engineer</a>
                </div>
        </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="col-md-12 clearfix">
            {!! Form::open(['method' => 'get', 'url' => url('search'), 'class'=>"form minisearch" ,'id'=>"search_part_number",'data-parsley-validate' => true]) !!}
            <div class="field search clearfix">
                <?php
                if(!empty(app('request')->input('partNumber'))){
                    $searchPN = app('request')->input('partNumber');
                }else{
                    $searchPN = !empty($search_part_name)?$search_part_name:'';
                }
                ?>
                <input id="part_name" type="text" name="partNumber" value="<?php echo $searchPN;?>"  data-parsley-errors-container="#error-container"  data-parsley-required   data-parsley-required-message="Please enter any part number" placeholder="Search by part number" class="input-text" />
                <input type="hidden" name="slug_holder" id="slug_holder" value=""/>
                <button type="submit" title="Search" class="action search" >
                    <span>Search</span>
                </button>
            </div>
            <div id="error-container"> </div>
            {!! Form::close() !!}
            <div class="search-history">
                <a href="javascript:void(0)" data-toggle="modal" data-target="#search-history">Search history <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <a href="{{url('/bom')}}" class="Upload-instant-quotes">Upload a BOM here for instant quotes </a>
            </div>
        </div>
        
    </div>
</div>
<!--END: Banner-->

<h2 class="index-heading text-center">DISTIMONSTER — 1 Source, 1 PO, 1 Shipment</h2>
<section class="light-blue p-t40 p-b30 multi-vendor">
    <div class="container">
        <h2 class="text-center">THIS IS WHAT YOU’VE BEEN ASKING FOR.</h2>
        <p class="text-center">No more hours of searching multiple vendor sites trying to find electronic parts.</p>
        <div class="row m-t15 mb-4 pb-3 buyer-row w-88">
        <div class="col-md-3 mw-fix">
            <img src="{{url('frontend/assets/images/side-img-one-new.jpg')}}" class="img-responsive img-box-effect">
        </div>
        <div class="col-md-9 mt-m-15 mw-fix-two m-t15">
<p class="m-b5 p-t0 hm-bar-icon"><img src="{{url('frontend/assets/images/green-bar-one.png')}}" class="img-responsive"></p>
            <p class="p-t0 buyer-txt m-t3">
                For the BUYER: View and procure millions of parts with pricing and delivery options to meet your needs.
Simplify your workload with ONE PO upload. Use your personalized dashboard for easy reorder.
            </p>
        </div>
    </div>   
    <div class="row w-88 engineer-row">
        <div class="col-md-3 mw-fix">
            <img src="{{url('frontend/assets/images/side-img-two-new.jpg')}}" class="img-responsive img-box-effect">
        </div>
        <div class="col-md-9 mt-m-15 mw-fix-two">
            <p class="m-b5 p-t0 hm-bar-icon"><img src="{{url('frontend/assets/images/blue-bar.png')}}" class="img-responsive"></p>
            <p class="p-t0 engineer-txt m-t3">                
                For the ENGINEER: A seamless, single source for EDA design, parts information and
                NPI fulfillment. As you design, build a Monster-BOM for immediate procurement. Easily share designs with anyone with a single click. Plus, you’ll have access to
                millions of data sheets.
            </p>
        </div>
    </div>
    </div>
</section>