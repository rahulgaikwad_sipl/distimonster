<ul class="tabs shdow-box" id="tabMenu">
		<li class="{{ Request::is('dashboard') ? 'active' : '' }}"><a href="{{url('dashboard')}}">Dashboard</a></li>
		<li class="{{ Request::is('account-settings/*') ? 'active' : '' }}" > <a href="{{url('account-settings/'.Auth::user()->id)}}">Account Settings </a> </li>
		<li class="{{ Request::is('order-history') ? 'active' : '' }}"><a href="{{url('order-history')}}">Order History</a></li>
		<li class="{{ Request::is('quotes') ? 'active' : '' }}"><a href="{{url('quotes')}}">Quotes</a></li>
		@if(Request::is('quotes') || Request::is('pending-quotes') || Request::is('processed-quotes')|| Request::is('expired-quotes') || Request::is('ordered-quotes') )
		<li class="p-l0 p-r0 p-t0 saved-quote-submenu">
		<ul class="tabs shdow-box ">
				<li class="{{ Request::segment(1) == 'pending-quotes' ? 'active' : '' }}"><a href="{{url('pending-quotes')}}">Pending RFQ's</a></li>
				<li class="{{ Request::segment(1) == 'processed-quotes' ? 'active' : '' }}"><a href="{{url('processed-quotes')}}">Processed Quotes</a></li>
		</ul></li>
         @endif                   
		<li class="{{ Request::is('my-bom') ? 'active' : '' }}"><a href="{{url('my-bom')}}">Monster-BOM</a></li>
		@if(Request::is('my-bom') || Request::is('saved-bom') || Request::is('shared-bom') || Request::is('ordered-bom'))
		<li class="p-l0 p-r0 p-t0 saved-quote-submenu">
			<ul class="tabs shdow-box ">
				<li class="{{ Request::segment(1) == 'saved-bom' ? 'active' : '' }}"><a href="{{url('saved-bom')}}">Saved Monster-BOM</a></li>
				<li class="{{ Request::segment(1) == 'shared-bom' ? 'active' : '' }}"><a href="{{url('shared-bom')}}">Shared Monster-BOM</a></li>
				<li class="{{ Request::segment(1) == 'ordered-bom' ? 'active' : '' }}"><a href="{{url('ordered-bom')}}">Ordered Monster-BOM</a></li>
			</ul>
		</li>
		@endif 
		@if(!Session::get("mybio_added"))  
			<li class="{{ Request::is('my-boi') ? 'active' : '' }}"><a style="color:#1889BD;" href="{{url('my-bio')}}"><strong>My BIO</strong></a></li>
		@else
			<li class="{{ Request::is('my-boi') ? 'active' : '' }}"><a href="{{url('my-bio')}}">My BIO</a></li>
		@endif
		@if(Auth::user()->parent_user_id==0)
			<li class="{{ Request::is('my-team') ? 'active' : '' }}"><a href="{{url('my-team')}}">My Team</a></li>
		@endif
	</ul>