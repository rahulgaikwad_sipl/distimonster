
@if ($paginator->hasPages())
<ul class="pagination pull-right">

@if ($paginator->onFirstPage())
    <li class="page-item">
        <a class="page-link disabled"  aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
        </a>
    </li>
@else
    <li class="page-item">
        <a href="{{ $paginator->previousPageUrl() }}"  class="page-link" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
        </a>
    </li>
@endif
    {{-- Pagination Elements --}}
@foreach ($elements as $element)
    {{-- "Three Dots" Separator --}}
    @if (is_string($element))
        <li class="disabled"><span>{{ $element }}</span></li>
        @endif
    {{-- Array Of Links --}}
    @if (is_array($element))
        @foreach ($element as $page => $url)
            @if ($page == $paginator->currentPage())
                <li class="page-item active"><a href="#" class="page-link">{{ $page }}</a></li>
                @else
                <li class="page-item"><a href="{{ $url }}" class="page-link">{{ $page }}</a></li>
                @endif
        @endforeach
    @endif
@endforeach

@if ($paginator->hasMorePages())
    <li class="page-item">
        <a href="{{ $paginator->nextPageUrl() }}" class="page-link" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
        </a>
    </li>
@else
    <li class="page-item">
        <a class="page-link disabled" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
        </a>
    </li>
@endif
</ul>
@endif
