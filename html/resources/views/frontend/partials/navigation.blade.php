<!--Navigation-->
<div class="custom-nav custom-nav-header">
    <div class="container">
        <nav class="navbar navbar-toggleable-md navbar-inverse p-l0">
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{url('what-is-distimonster')}}">WHAT IS DISTIMONSTER</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="{{url('products')}}">PRODUCTS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('bom')}}">MONSTER-BOM</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('return-policy')}}">SUPPORT</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('about')}}">ABOUT</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{url('contact')}}">Contact</a>
                    </li>
<!--                    <li class="nav-item">
                        <a class="nav-link" href="{{url('news-updates')}}">News & Updates</a>
                    </li>-->
                    
                    @if (Auth::check())
                    <li class="nav-item">
   <div class="dropdown">
      <a class="nav-link dropbtn" href="javascript:void(0)" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">MY ACCOUNT
      <i class="fa fa-caret-down"></i>
      </a>
      <div class="dropdown-content">
            <a class="nav-link" href="{{ url('dashboard/')}}">Dashboard</a>
      <a class="nav-link" href="{{ url('account-settings/'.Auth::user()->id)}}">Account Settings</a>
      <a class="nav-link" href="{{url('order-history')}}">Order History</a>
      <a class="nav-link" href="{{url('quotes')}}">Quotes</a>
      <a class="nav-link" href="{{url('my-bom')}}">Monster-BOM</a>
      <a class="nav-link" href="{{url('my-bio')}}">My BIO</a>
      @if(Auth::user()->is_sub_user == 0)
        <a class="nav-link" href="{{url('my-team')}}">My Team</a>
      @endif  
      {{--<a class="nav-link" href="{{url('requested-lead-times')}}">Requested Lead Times</a>--}}
      <?php /*<a class="nav-link" href="{{url('apply-net-term-account')}}">Apply for a Net Term Account</a>*/?>
      </div>
   </div>
</li>
                    @endif
                </ul>

            </div>
        </nav>

        <div class="navbar-collapse collapse"  id="navbar">
        <div class="row">
            <div class="col-md-9 col-sm-10">
                <ul class="nav navbar-nav">
                    {{--<li class="active"><a href="#">All Manufacturers <span class="sr-only">(current)</span></a></li>--}}
                    <li><a href="{{url('about')}}">About</a></li>
                    <li><a  href="{{url('news-updates')}}">News &amp; Updates</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Support <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{url('faq')}}">FAQs</a></li>
                            <li><a href="{{url('return-policy')}}">Return Policy</a></li>
                            <li><a href="{{url('contact')}}">Contact</a></li>
                        </ul>
                    </li>
                    <li><a href="{{url('bom')}}">BOM</a></li>
                    @if (Auth::check())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('account-settings/'.Auth::user()->id)}}">Account Settings</a></li>
                            <li> <a href="{{url('order-history')}}">Order History</a></li>
                            <li><a href="{{url('my-quotes')}}">My Quotes</a></li>
                            <li><a href="{{url('my-bom')}}">MonsterBOM</a></li>
                            <li><a href="{{url('my-bio')}}">My BIO</a></li>
                            @if(Auth::user()->is_sub_user == 0)
                                <li><a href="{{url('my-team')}}">My Team</a></li>
                            @endif    
                            {{--<li><a href="{{url('requested-lead-times')}}">Requested Lead Times</a></li>--}}
                            <?php /* <li> <a href="{{url('apply-net-term-account')}}">Apply for a Net Term Account</a></li>*/ ?>
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
<!--Navigation end-->