@extends('frontend.layouts.app')
@section('content')
    {{--@include('frontend.partials.banner')--}}
    <!--START: Products-->
    <section class="gray-bg product-detail">
        <div class="container clearfix">
            <?php //echo "<pre>"; print_r($details);?>
            <div class="row">
                <div class="col-md-8">
                    <h2 class="block-title">{{$details['finalData'][0]['partNum']}}</h2>
                    <div class="block-sub-title">{{$details['finalData'][0]['manufacturer']}}</div>
                    <p class="description">{{$details['finalData'][0]['desc']}}</p>
                    <div class="sold" id="soldBy"></div>
                    {{--<div class="available-from"><span>Available from</span> <span class="text-underline">{{count($details['finalData'][0]['sources'][0]['sourcesPart'])}} sellers</span></div>--}}
                    <div class="view-specification"><a href="javascript:void(0)">View Specifications</a></div>
                    <div class="btn-dot"><a href="javascript:void(0)"><span class="img-icon"><img src="{{url('frontend/assets/images/pdf-icon.png')}}" alt="add-icon" /></span> Datasheet</a></div>
                </div>
                <div class="col-md-4">
                    <div class="shdow-box">
                        <div class="price-content-box" id="priceContentBox" style="display: none">
                            <input type="hidden" id="hiddenQtyInputId" name="hiddenQtyInputId" value= '' />
                            <input type="hidden" id="hiddenId" name="hiddenId" value= '' />
                            <input type="hidden" id="hiddenManufacturer" name="hiddenManufacturer" value= '' />
                            <input type="hidden" id="hiddenSourcePartId" name="hiddenSourcePartId" value= '' />
<<<<<<< HEAD
                            <input type="hidden" id="hiddenStockAvailability" name="hiddenStockAvailability" value= '' />
=======
                            <input type="hidden" id="hiddenSourcePartIdCompare" name="hiddenSourcePartIdCompare" value= '' />

>>>>>>> 9323b9b73732885e5004d179f88a835749bdc6ff
                            <div class="row price-box-heading">
                                <div class="col-md-4">
                                    <h2 class="block-title">Price</h2>
                                </div>
                                <div class="col-md-8">
                                    <div class="price-title black text-right font-w500" id="selected-item-price"></div>
                                </div>
                            </div>
                            <div class="price-content-body">
                                <div class="stock"><span class="text-gray" id="stockCount"></span> <span class="black" id="availablePart"></span></div>
                                <div class="estimate m-t10"><span class="img-icon"><img src="{{ url('frontend/assets/images/estimate-icon.png')}}" alt="add-icon" /></span> <span class="text-gray">Estimated Shipping:</span> <span class="black">5-7 days</span></div>
                                <div class="shipping"><span class="img-icon"><img src="{{ url('frontend/assets/images/shipping-icon.png')}}" alt="add-icon" /></span> <span class="text-gray">Urgent Shipping:</span> <span class="black">(+) $150 (Next day)</span></div>
                                <div class="quantity-div">
                                    <p class="text-gray" id="minInc"></p>
                                    <div class="quantity-box">
                                        <form id='myform' method='POST' action='#' class="numbo">
                                            <input  class="quantity" id="quantity" placeholder="Quantity" name='quantity' value='1' class='qty' style="margin-bottom: 0px !important"
                                            onkeyup="cartModule.onchangeQtyOfSelectProduct($('#hiddenSourcePartId').val())"" />
                                            <input id="qtyplus" onclick="cartModule.increaseQtyOfSelectProduct($('#hiddenSourcePartId').val())" type='button' value='+' class='qtyplus ' field='quantity' style="font-weight: bold;" />
                                            <input id="qtyminus" onclick="cartModule.decreaseQtyOfSelectProduct($('#hiddenSourcePartId').val())" type='button' value='-' class='qtyminus' field='quantity' style="font-weight: bold;" />

                                        </form>
                                    </div>
                                     <input  class="customerPartNo quantity-box" id="customerPartNo" name='customerPartNo' value='' placeholder="Customer Part No #" />
                                </div>
                            </div>
                            <div class="price-content-footer">
                                <button type="button" id="addToQuote"  onclick="quoteModule.addToQuoteOfSelectedProduct()" class="btn btn-primary btn-outline" title="Request specific prices"><span class="img-icon"><img src="{{url('frontend/assets/images/add-quote-icon.png')}}" alt="add-quote-icon" /></span>Add to Request for Quote</button>
                                <button type="button" id="addToCart" onclick="cartModule.addToCartOfSelectedProduct()"  class="btn btn-primary pull-right" title="Adds items to cart for immediate purchase"><span class="img-icon"><img src="{{url('frontend/assets/images/cart-bag-icon.png')}}" alt="cart-bag-icon" /></span>Add to Cart</button>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!--Franchise Distributor-->
            <div class="row franchise-distributor">
                <div class="col-md-12">
                    <h2 class="block-title">Available Franchise Distributor</h2>
                    @if(!empty($details['finalData'][0]['sources']))
                        @foreach($details['finalData'][0]['sources'] as $sources)
                            @foreach($sources['sourcesPart'] as $sellerInfo)
                            <div class="shdow-box product-listing-btm-space franchise-distributor-content" id="box_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>">
                                <div class="row franchise-distributor-header">
                                    <div class="col-md-6">
                                        <div class="sellers-id"><span>Source Part ID:</span> {{$sellerInfo['sourcePartId']}} </div>
                                    </div>
                                    <div class="col-md-6 text-right sellers-id">
                                        <span class="text-gray f-s12">Franchised Distributor</span>
                                    </div>
                                </div>
                                <div class="rowd franchise-distributor-body">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <?php echo "<pre>"; print_r($sellerInfo['Prices']); echo "</pre>";?>
                                                    <div class="sellers-detail">
                                                        <div class="price"><span class="black f-s18"> $6,354.20 </span> <span class="f-s14 text-gray"> @if($sellerInfo['inStock'])({{  $sellerInfo['Availability'][0]['fohQty']}} Available) @endif</span></div>
                                                        @if(!empty($sellerInfo['Prices']))
                                                            <div class="ratio f-s14 text-gray"><span>Minimum: {{$sellerInfo['Prices']['resaleList'][0]['minQty']}}</span><span>Increment: 1</span></div>
                                                        @endif
                                                            <p class="text-gray m-b0 f-s14">Availability Message: <span class="black">{{  $sellerInfo['Availability'][0]['availabilityMessage']}}</span></p>

                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="warranty-detail f-s14 ">
                                                        <p class="text-gray warranty-text">Warranty: <span class="black">Manufacturer</span></p>
                                                        <p class="text-gray m-b0">Date Code: <span class="black">{{!empty($sellerInfo['dateCode'])?$sellerInfo['dateCode'].'+':'' }}</span></p>
                                                    </div>
                                                </div>


                                                <div class="col-md-4">
                                                    <div class="estimated-detail f-s14">
                                                        <div class="text-gray estimated-text"><span class="img-icon"><img src="{{url('frontend/assets/images/estimate-icon.png')}}" alt="add-icon" /></span> Estimated Shipping: <span class="black">5-7 days</span></div>
                                                        <div class="text-gray"><span class="img-icon"><img src="{{url('frontend/assets/images/shipping-icon.png')}}" alt="add-icon" /></span>Urgent Shipping: <span class="black">(+)&nbsp; $150 &nbsp;(Next day)</span></div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-2 p-r0 text-right">
                                          <!--  <button onclick="cartModule.addToCart('quantity_<?php echo  str_replace(':', '_', $sellerInfo['sourcePartId']); ?>' ,'<?php echo $details['finalData'][0]['partNum']; ?>','<?php echo $details['finalData'][0]['manufacturer']; ?>','<?php echo $sellerInfo['sourcePartId'];?>');" type="submit" class="btn btn-primary" title="Add to Cart"><span class="img-icon"><img src="{{url('frontend/assets/images/cart-bag-icon.png')}}" alt="cart-bag-icon" /></span>Add to Cart</button> -->
                                          @if(!empty($sellerInfo['Prices']))
                                                <button onclick="cartModule.selectProduct(<?php echo $sellerInfo['Availability'][0]['fohQty']; ?>,<?php echo $sellerInfo['inStock']; ?>,<?php echo $sellerInfo['Prices']['resaleList'][0]['minQty'] ?>,<?php echo $sellerInfo['Prices']['resaleList'][0]['price'] ?>,'quantity_<?php echo  str_replace(':', '_', $sellerInfo['sourcePartId']); ?>' ,'<?php echo $details['finalData'][0]['partNum']; ?>','<?php echo $details['finalData'][0]['manufacturer']; ?>','<?php echo $sellerInfo['sourcePartId'];?>');"  class="btn btn-primary select-buttons" title="Select" id="select_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>">Select</button>
                                          @else
                                                  <button onclick="cartModule.selectProduct(<?php echo $sellerInfo['Availability'][0]['fohQty']; ?>,<?php echo $sellerInfo['inStock']; ?>,0,0,'quantity_<?php echo  str_replace(':', '_', $sellerInfo['sourcePartId']); ?>' ,'<?php echo $details['finalData'][0]['partNum']; ?>','<?php echo $details['finalData'][0]['manufacturer']; ?>','<?php echo $sellerInfo['sourcePartId'];?>');"  class="btn btn-primary" title="Select" id="select_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>">Select</button>
                                           @endif
                                           </div>
                                    </div>
                                </div>
                                @if(!empty($sellerInfo['Prices']))
                                <div class="row franchise-distributor-footer">

                                    <div class="col-md-12">
                                        @foreach($sellerInfo['Prices']['resaleList'] as $tierPrices)
                                        <div class="input-group" onclick="cartModule.selectProduct(<?php echo $sellerInfo['Availability'][0]['fohQty']; ?>,<?php echo $sellerInfo['inStock']; ?>,<?php echo $tierPrices['minQty'] ?>,<?php echo number_format($tierPrices['price'], 2, '.', ',')?>,'quantity_<?php echo  str_replace(':', '_', $sellerInfo['sourcePartId']); ?>' ,'<?php echo $details['finalData'][0]['partNum']; ?>','<?php echo $details['finalData'][0]['manufacturer']; ?>','<?php echo $sellerInfo['sourcePartId'];?>');" >
                                            <div class="input-group-prepend">
                                                <div class="input-group-text qtyHolder_<?php echo $sellerInfo['sourcePartId'];?>" id="btnGroupAddon2">+{{$tierPrices['minQty']}}</div>
                                            </div>
                                            <div data-priceHold="{{$tierPrices['displayPrice']}}" data-qtyHold="{{$tierPrices['minQty']}}" class="form-control priceHolder_<?php echo str_replace(':', '_', $sellerInfo['sourcePartId']);?>">${{number_format($tierPrices['displayPrice'], 2, '.', ',')}}</div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                                    @endif
                            </div>
                            @endforeach
                        @endforeach
                    @endif
            </div>
        </div>
            <!--Franchise Distributor-->

            <div class="row franchise-distributor specification">
                <div class="col-md-12">
                    <h2 class="block-title">Specifications</h2>
                    <div class="shdow-box product-listing-btm-space franchise-distributor-content">

                        <div class="row">
                            <div class="col-md-3 f-s14 text-gray">
                                <ul>
                                    <li>Manufacturer: </li>
                                    <li>Product Category:</li>
                                    <li>Access Time (Max):</li>
                                    <li>Address Bus:</li>
                                    <li>Boot Type:</li>
                                    <li>Cell Type:</li>
                                    <li>Interface Type:</li>
                                    <li>Mounting:</li>
                                    <li>Operating Temp Range:</li>
                                    <li>Package Type:</li>
                                    <li>Packaging:</li>
                                </ul>
                            </div>
                            <div class="col-md-3 f-s14 text-gray">
                                <ul>
                                    <li class="red text-uper">Manufacturer: </li>
                                    <li class="red text-uper">Product Category:</li>
                                    <li class="black">Access Time (Max):</li>
                                    <li class="black">Address Bus:</li>
                                    <li class="black">Boot Type:</li>
                                    <li class="black">Cell Type:</li>
                                    <li class="black">Interface Type:</li>
                                    <li class="black">Mounting:</li>
                                    <li class="black" >Operating Temp Range:</li>
                                    <li class="black">Package Type:</li>
                                    <li class="black">Packaging:</li>
                                </ul>
                            </div>
                            <div class="col-md-3 f-s14 text-gray">
                                <ul>
                                    <li>Manufacturer: </li>
                                    <li>Product Category:</li>
                                    <li>Access Time (Max):</li>
                                    <li>Address Bus:</li>
                                    <li>Boot Type:</li>
                                    <li>Cell Type:</li>
                                    <li>Interface Type:</li>
                                    <li>Mounting:</li>
                                    <li>Operating Temp Range:</li>
                                    <li>Package Type:</li>
                                    <li>Packaging:</li>
                                </ul>
                            </div>
                            <div class="col-md-3 f-s14 text-gray">
                                <ul>
                                    <li>Manufacturer: </li>
                                    <li>Product Category:</li>
                                    <li>Access Time (Max):</li>
                                    <li>Address Bus:</li>
                                    <li>Boot Type:</li>
                                    <li>Cell Type:</li>
                                    <li>Interface Type:</li>
                                    <li>Mounting:</li>
                                    <li>Operating Temp Range:</li>
                                    <li>Package Type:</li>
                                    <li>Packaging:</li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>



        
    </section>
    <!--<section>
        <div class="container">
            <div class="product">

                <h2>Product Details Page</h2>
                <div class="slide-controle">
                    <div class="slide-next"><i class="fa fa-angle-right" aria-hidden="true"></i></div>
                    <div class="slide-prev"><i class="fa fa-angle-left" aria-hidden="true"></i></div>
                </div>
                <div class="row">


                    <div class="col-sm-12 col-lg-3 col-md-3 col-xs-12">
                        <div class="product-item">
                            <div class="product-img text-center">
                                <a href="JavaScript:void(0)"><img src="{{url('frontend/images/product-1.jpg')}}" class="img-responsive" alt=""></a>
                            </div>
                            <div class="product-details">
                                <h3><a href="JavaScript:void(0)">ACM3P-4-AC1-R-C</a></h3>
                                <p>RF Amp Chip Single GP 5GHz 5.25V...</p>
                                <span class="price">$ 65.00</span>
                            </div>
                            <div class="wishlist"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a></div>

                        <input type="button" id='add-to-cart' onclick="cartModule.addToCart(1);" value="Add To Cart">
                    </div>
                </div>
            </div>
        </div>
    </section>-->
@stop
