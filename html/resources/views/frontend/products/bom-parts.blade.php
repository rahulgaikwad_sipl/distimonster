@extends('frontend.layouts.app')
@section('content')
    <!--START: Products list-->
    <section class="gray-bg p-t15">
        <div class="container clearfix">
        </div>
        <?php  //echo "<pre>"; print_r(json_encode($finalData));echo "</pre>";?>
        <p>This Is BOM Separate View</p>
        @if(isset($bom_id))
            <div class="col-lg-12 text-right p-b15" id="add_all_to_cart" style="display: none;">
                <button type="button" class="m-t9 btn btn-primary btn-outline p-tb5 "  onclick="getAllPartForQuote();" title="Add All to Quote"><span class="img-icon"><img src="{{url('frontend/assets/images/add-quote-icon.png')}}" alt="add-quote-icon" /></span>Add All to Cart</button>
            </div>
            <div class="col-lg-12 text-right p-b15">
                <button type="button" class="m-t9 btn btn-primary btn-outline p-tb5 "  onclick="getAllPartForQuote();" title="Add All to Quote"><span class="img-icon"><img src="{{url('frontend/assets/images/add-quote-icon.png')}}" alt="add-quote-icon" /></span>Add All to Quote</button>
            </div>
        @endif
        <div class="col-lg-12">
            <div class="table-dresponsive">
                <table class="table  table-bom-product table-striped">
                    <tbody>
                    <tr class="f-s16 font-w500 black">
                        <th><input type="hidden" value="1" /></th>
                        <th>Part#</th>
                        <th>Price</th>
                        <th width="200">Quantity</th>
                        <th>Stock</th>
                        <th>Description</th>
                        <th width="350">Category</th>
                        <th>Action</th>
                    </tr>

                    @foreach ($finalData as  $key=>$data)
                        @if(isset($data['partNum']))
                            <form name="bompart_{{$data['partNum']}}" id="bompart_{{str_replace(' ', '_', $data['itemId'])}}-{{str_replace(' ', '_', $data['mfrCd'])}}" data-parsley-validate="true" >
                                <tr class="f-s14 text-gray">
                                    <td><input type="checkbox" class="checkbox-qty"  data-itemid="{{$data['itemId'] }}" value="1" id="checkbox_{{$data['itemId']}}" /></td>
                                    <td>
                                        <div class="hind-font text-pink" href="javascript:void(0)">MPN: {{$data['partNum']}}</div>
                                        <div class="block m-t5 m-b10">
                                            @if($data['EnvData']['compliance'][0]['displayLabel'] != "")
                                                <span class="img-icon"><img src="{{url('frontend/assets/images/rohslogo.png')}}" alt="pending" /></span>RoHS Compliant
                                            @endif
                                        </div>
                                        <?php   $sheetData =  \Helpers::getDataSheet($data['partNum'],$data['manufacturer']);?>
                                        <div class="block m-t10 m-b10">
                                            @if(!empty($sheetData))
                                                <a target="_blank" href="{{$sheetData[0]['datasheet_url']}}" class="data-sheet"><img src="{{url('/frontend/assets/images/pdf-icon.png')}}" width="12px"> Datasheet</a>
                                            @endif
                                        </div>
                                        Manufacturer: <span class="black font-w500">{{$data['manufacturer']}}</span>
                                    </td>
                                    <td>
                                    <span class="black font-w500" id="item_price_div_25360740">
                                    <?php $lowestPrice = 0; ?>
                                        @if(!empty($data['sources']))
                                            @foreach($data['sources'] as $sources)
                                                <?php
                                                $lowestPrice =  \Helpers::getLowestPrice($sources);
                                                // echo json_encode($lowestPrice);
                                                ?>
                                            @endforeach
                                        @endif
                                        $<?php echo number_format($lowestPrice, 3, '.', ','); ?>
                                    </span>
                                        <br>
                                        <!-- Large modal -->
                                    <?php $availabilityQty = 0; ?>
                                    @if(!empty($data['sources']))
                                        @foreach($data['sources'] as $sources)
                                            @foreach($sources['sourcesPart'] as $sellerInfo)
                                                @if($sellerInfo['inStock'])
                                                    <?php  $availabilityQty =  $availabilityQty+$sellerInfo['Availability'][0]['fohQty'];?>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @endif
                                    <!-- Show Buying option only if  instock-->
                                    {{--@if($availabilityQty)--}}
                                    <!--<a class="" onclick="alogoForQtySeperation({{json_encode($data['sources'])}},{{$data['itemId']}},{{$availabilityQty}}, '','{{str_replace(' ', '_', $data['mfrCd'])}}')" data-target=".bs-example-modal-lg-<?php echo  $key; ?>" data-toggle="modal" href="#" type="button">Buying Options</a>-->
                                        {{--@endif--}}
                                        <a class="" onclick="alogoForQtySeperation({{json_encode($data['sources'])}},{{$data['itemId']}},{{$availabilityQty}}, '','{{str_replace(' ', '_', $data['mfrCd'])}}')" data-target=".bs-example-modal-lg-<?php echo  $key; ?>" data-toggle="modal" href="#" type="button">Buying Options</a>
                                        <div aria-labelledby="myLargeModalLabel" class="modal fade bs-example-modal-lg-<?php echo  $key; ?>" role="dialog" tabindex="-1">
                                            <div class="modal-dialog modal-lg modal-custom" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bdr-none">
                                                        <div class="modal-title">
                                                            <h4>Buying options for {{$data['partNum']}}</h4>

                                                        </div>
                                                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon"></span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row p-b5">
                                                            <div class="col-md-6"><span class="f-s18 text-gray">Quantity: </span><span class="black font-w500 f-s18 requestQtyDiv" id="requestQtyDiv"></span> </div>
                                                            <div class="col-md-6 text-right">
                                                                <span class="f-s18 text-gray">Available stock: </span><span class="text-right black font-w500 f-s18" id="totalStockQty"> {{$availabilityQty}}</span>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="show-data">
                                                        @if(!empty($data['sources']))
                                                            @foreach($data['sources'] as $sources)
                                                                @foreach($sources['sourcesPart'] as $sellerInfo)
                                                                    <!--removed check if item not in stock-->
                                                                        {{--@if($sellerInfo['inStock'])--}}
                                                                        @if(!empty($sellerInfo['Prices']['resaleList']))
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <?php $minQty = 0; ?>
                                                                                    <?php  $minQty = $sellerInfo['Prices']['resaleList'][0]['minQty'];?>
                                                                                    <p> Source Id: {{$sellerInfo['sourcePartId'] }}</p>
                                                                                    <input class="bor-radius-none customerPartNo form-control quantity-box pop-quantity-input pop-quantity-input-{{$data['itemId']}}-{{str_replace(' ', '_', $data['mfrCd'])}}" onkeypress="return isNumber(event)" data-source="{{str_replace(':', '_',$sellerInfo['sourcePartId'])}}" data-parsley-max="{{$sellerInfo['Availability'][0]['fohQty']}}"  id="quantity_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>" name="quantities[]" value=""  placeholder="Quantity" />
                                                                                    <span id="message_{{str_replace(':', '_',$sellerInfo['sourcePartId'])}}" class="error-max-qty text-red" style="display: none; color: red; font-size: 11px;">Entered quantity is more than available quantity.<br/></span>

                                                                                    <p class="m-b0">Increments of 1</p>
                                                                                    <p>Minimum <?php echo $minQty; ?></p>
                                                                                    <input class="bor-radius-none quantity form-control quantity-box" id="customerPartNo_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>" name="customerPartNo[]" placeholder="Internal part#" value="" />
                                                                                    <input type="hidden" name="productId[]" value="{{$data['partNum']}}" />
                                                                                    <input type="hidden" name="manufacturer[]" value="{{$data['manufacturer']}}" />
                                                                                    <input type="hidden" name="mfrCd[]" value="{{$data['mfrCd']}}" />
                                                                                    <input type="hidden" name="sourcePartId[]" value="{{$sellerInfo['sourcePartId']}}" />
                                                                                    <input type="hidden" name="stockAvailability[]" value="{{$sellerInfo['inStock']}}" />
                                                                                    <input type="hidden" name="dateCode[]" value="{{$sellerInfo['dateCode']}}" />
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <div class="num-avability">
                                                                                        @foreach($sellerInfo['Prices']['resaleList'] as $resaleList)
                                                                                            <p><?php echo  $resaleList['minQty']; ?>+</p>
                                                                                        @endforeach
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-2">
                                                                                    <div class="num-avability">
                                                                                        @foreach($sellerInfo['Prices']['resaleList'] as $resaleList)
                                                                                            <p>$<?php echo  $resaleList['price']; ?></p>
                                                                                        @endforeach
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-4">
                                                                                    <h6>
                                                                                        <?php  echo $sellerInfo['Availability'][0]['fohQty'];
                                                                                        ?>
                                                                                    </h6>
                                                                                    <h6>
                                                                                        <?php  echo $sellerInfo['Availability'][0]['availabilityMessage'];
                                                                                        ?>
                                                                                    </h6>
                                                                                    <!--<h6>Cut Strip</h6>
                                                                                    <p>Can ship from the United States today</p>
                                                                                    <p class="m-b5"><span><label>PIPELINE :</label> 16 Jul: 85000</span></p>
                                                                                    <p><span><label>LEAD TIME : </label> 9 weeks</span></p>
                                                                                    -->
                                                                                </div>
                                                                            </div>
                                                                            <hr>
                                                                        @endif
                                                                        {{--@endif--}}
                                                                    @endforeach
                                                                @endforeach
                                                            @endif
                                                        </div>
                                                    </div>
                                                    @if(!empty($data['sources']))
                                                        @if($availabilityQty >0)
                                                            <div class="modal-footer p-t0"><button type="button" onClick="submitAddCard('{{str_replace(' ', '_', $data['itemId'])}}-{{str_replace(' ', '_', $data['mfrCd'])}}')" class="btn btn-primary" title="Adds items to cart for immediate purchase">Done & Add to cart</button></div>
                                                        @endif
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="productDetails" value="{{json_encode($data)}}"></input>
                                    </td>
                                    <td>
                                        <div class="text-center">
                                            <input class="bor-radius-none quantity form-control quantity-box quantity-input" onkeypress="return isNumber(event)" id="quantity_{{$data['itemId']}}" data-quantity="{{ $availabilityQty?$availabilityQty:0 }}" name="quantity" placeholder="Enter Quantity" value="{{$data['requestedQty']}}">
                                            <span id="message_{{$data['itemId']}}" class="error-max-qty text-red" style="display: none; color: red; font-size: 11px;">Entered quantity is more than available quantity.<br/></span>
                                            <input  type="hidden" class="bor-radius-none customerPartNo form-control quantity-box" id="customerPartNoID_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" name='customerPartNoQuote' value='' placeholder="Internal Part#" />
                                            @if($sources['sourcesPart'][0]['Prices'])
                                                <input  type="hidden" class="bor-radius-none customerPartNo form-control quantity-box" value='<?php echo json_encode($sources['sourcesPart'][0]['Prices']['resaleList'])?>' id="resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" name='resaleList'  placeholder="resaleList" />
                                            @else
                                                <input  type="hidden" class="bor-radius-none customerPartNo form-control quantity-box" value='<?php echo json_encode([])?>' id="resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" name='resaleList'  placeholder="resaleList" />
                                            @endif
                                            <input type="hidden" class="bor-radius-none customerPartNo form-control quantity-box"  value='<?php echo json_encode($sources['sourcesPart'][0]['Availability'])?>' id="availableData_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" name='availableData'  placeholder="availableData" />

                                            @if($availabilityQty)
                                                <button class="btn btn-primary p-tb5 m-t10" type="button" onclick="alogoForQtySeperation({{json_encode($data['sources'])}},{{$data['itemId']}},{{$availabilityQty}},'{{str_replace(' ', '_',$data['partNum'])}}','{{str_replace(' ', '_', $data['mfrCd'])}}')" name="button" value="" title="Adds items to cart for immediate purchase"><span class="img-icon"><img src="{{url('frontend/assets/images/cart-bag-icon.png')}}" alt="add-quote-icon"></span> Add to cart </button>
                                            @endif

                                            <input type="hidden" class="all-quote-items" data-allquote="1"  data-fohqty="<?php echo $sources['sourcesPart'][0]['Availability'][0]['fohQty']; ?>" data-qtyinput="quantity_<?php echo $data['itemId']; ?>"  data-partnum="<?php echo $data['partNum']; ?>"  data-manufacturer="<?php echo $data['manufacturer']; ?>"  data-mfrcd="{{$data['mfrCd']}}" data-sourcepartid="{{$sources['sourcesPart'][0]['sourcePartId']}}" data-customerpartnoinput="customerPartNoID_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>"  data-instock="<?php echo $sources['sourcesPart'][0]['inStock']?>"  data-resalelistinput="resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" data-availabledatainput="availableData_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" data-datecode="<?php echo $sources['sourcesPart'][0]['dateCode'];?>"/>

                                            @if(!empty($sources['sourcesPart'][0]['Prices']))
                                            <!--<button type="button" id="addToQuote"  class="m-t9 btn btn-primary btn-outline" title="Add to Request for Quote"><span class="img-icon"><img src="{{url('frontend/assets/images/add-quote-icon.png')}}" alt="add-quote-icon" /></span>Add to Request for Quote</button>-->
                                                <button type="button" id="addToQuote"  onclick="productListModule.addToQuoteProductListing('<?php echo $sources['sourcesPart'][0]['Availability'][0]['fohQty']; ?>','quantity_<?php echo $data['itemId']; ?>' ,'<?php echo $data['partNum']; ?>','<?php echo $data['manufacturer']; ?>', '<?php if(isset($data['mfrCd'])){echo $data['mfrCd'];}else{ echo $data['manufacturer']; } ?>','<?php echo $sources['sourcesPart'][0]['sourcePartId'];?>','customerPartNoID_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>','<?php echo $sources['sourcesPart'][0]['inStock']?>' ,'resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>','availableData_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>','<?php echo $sources['sourcesPart'][0]['dateCode'];?>')" class="m-t9 btn btn-primary btn-outline p-tb5 " title="Request specific prices"><span class="img-icon"><img src="{{url('frontend/assets/images/add-quote-icon.png')}}" alt="add-quote-icon" /></span>Add to Request for Quote</button>
                                            @endif
                                        </div>

                                    </td>
                                    <td>

                                        @if($availabilityQty)
                                            <span class="f-s14 success font-w500">
                                        (<?php echo $availabilityQty; ?> Available)
                                        <input type="hidden" name="totalAvailableQty" value="{{$availabilityQty}}"></input>
                                    </span>
                                        @else
                                            <span class="f-s14 cancelled font-w500">
                                        No Stock
                                       <input type="hidden" name="totalAvailableQty" value="0"></input>
                                    </span>
                                    @endif

                                    <!--   <div>
                                <span>
                                Minimum: 8
                                </span>
                                <span>
                                Increment: 1
                                </span>
                                </div>
                                <input id="source_min_qty_25360740" type="hidden" value="8">
                                <p class="">
                                Availability Message:
                                <span class="black">
                                In Stock
                                </span>
                                </p>
                                </input> -->
                                    </td>
                                    <td>

                                        {{$data['desc']}}
                                    </td>
                                    <td width="350">
                                        @if(!empty($sheetData))
                                            {{$sheetData[0]['category']}}
                                        @endif
                                    </td>
                                    <td class="action">
                                        <ul class="list-inline">
                                            <li class="list-inline-item">
                                                <a class="f-s14 hind-font" onClick="removePartRow(this)" href="javascript:void(0)">
                                <span class="img-icon">
                                <img alt="view-icon" id="remove-qty-{{ $key }}" src="{{url('frontend/assets/images/remove-icon.png')}}"/>
                                </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </td>
                                </tr>
                            </form>
                        @else
                        @endif
                    @endforeach
                    @if(isset($noPartFound))
                        @foreach($noPartFound as $key=>$value)
                            <tr class="f-s14 text-gray">
                                <td><input type="hidden" value="1" /></td>
                                <td>
                                    <a class="hind-font" href="javascript:void(0)"><span class="f-s14 cancelled  font-w500">MPN: {{$value['partNo']}}</span></a>

                                    <br>
                                    <span class="f-s14 cancelled font-w500">
                                                No exact matches found
                                     </span>
                                </td>
                                <td>N/A</td>
                                <td width="200">N/A</td>
                                <td>N/A</td>
                                <td>N/A</td>
                                <td width="350">N/A</td>
                                <td class="action">
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <a class="f-s14 hind-font" onClick="removePartRow(this)" href="javascript:void(0)">
                                        <span class="img-icon">
                                        <img alt="view-icon" id="remove-qty-{{ $key }}" src="{{url('frontend/assets/images/remove-icon.png')}}"/>
                                        </span>
                                            </a>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </section>
@stop

@section('javascript')
    <script>
        function removePartRow(obj){
            $(obj).parent().closest("tr").remove();
        }
        function alogoForQtySeperationRecFunc(sources, qty,totalAvailableQty, partNo,partId,manufacturer) {
            var qty = qty;
            var i = 0;
            var sourcesArrayLength = sources.length;
            /* Fot getting lowest price from all the remains source */
            for(i=0;i<sourcesArrayLength;i++){
                if(i<sourcesArrayLength-1){
                    if(lowestPrice > sources[i].sourcesPart[0].Prices.resaleList[0].price){
                        lowestPrice =  sources[i].sourcesPart[0].Prices.resaleList[0].price;
                        lowestPriceArrayIndex = i;
                    }
                }
            }
            if(lowestPriceArrayIndex < sourcesArrayLength && sources[lowestPriceArrayIndex].sourcesPart[0].Availability.length > 0){
                var availableQty    =   sources[lowestPriceArrayIndex].sourcesPart[0].Availability[0].fohQty;
                sourcePartId        =   sources[lowestPriceArrayIndex].sourcesPart[0].sourcePartId;
                sourcePartId        =   sourcePartId.replace(":","_");
                /* console.log("call if", sourcePartId,"qty = ",qty,"availableQty = ",availableQty,"totalAvailableQty = ",totalAvailableQty); */
                if(qty > availableQty){
                    /* console.log("set value by if in sourcePartId = ",sourcePartId ,"availableQty =",availableQty);*/
                    $("#quantity_"+sourcePartId).val(availableQty);
                    sources[0].sourcesPart.splice(lowestPriceArrayIndex, 1);
                    if(qty-availableQty > 0){
                        alogoForQtySeperationRecFunc(sources,qty-availableQty,totalAvailableQty, partNo,partId,manufacturer);
                    }
                }else{
                    /* console.log("call else");*/
                    if(availableQty < qty){
                        /*console.log("set value by else if in sourcePartId = ",sourcePartId ,"availableQty =",availableQty);*/
                        $("#quantity_"+sourcePartId).val(availableQty);
                    }else{
                        /* console.log("set value by else else in sourcePartId = ",sourcePartId ,"qty =",qty);*/
                        $("#quantity_"+sourcePartId).val(qty);
                    }
                }
            }
            if(partNo != ''){
                addToCart(partId+'-'+manufacturer);
            }
        }
        var lowestPrice = 0;
        var lowestPriceArrayIndex = 0;
        var sourcePartId = 0;
        function alogoForQtySeperation(sources,partId,totalAvailableQty,partNo,manufacturer){
            sources[0].sourcesPart = sources[0].sourcesPart.filter(function(el) {
                return el.Availability[0]['fohQty'] !== 0;
            });
            var inputTxt = $('#quantity_'+partId).val();
            $('.requestQtyDiv').html(inputTxt);
            if(inputTxt == ''){
                $('#message_'+partId).text('Please enter quantity.').show();
                return false;
            }else if(parseInt(inputTxt) > parseInt(totalAvailableQty)){
                $('#message_'+partId).text('Entered quantity is more than available quantity.').show();
                return false;
            }else{
                $('#message_'+partId).hide();
            }

            var sourcePartIdForBlank = '';
            for(i=0;i<sources.length;i++){
                for(spi=0;spi<sources[i].sourcesPart.length;spi++){
                    //console.log("sources[i].sourcesPart[spi].sourcePartId = ",sources[i].sourcesPart[spi].sourcePartId);
                    sourcePartIdForBlank =  sources[i].sourcesPart[spi].sourcePartId;
                    sourcePartIdForBlank = sourcePartIdForBlank.replace(":","_");
                    $("#quantity_"+sourcePartIdForBlank).val('');
                    if(sources[i].sourcesPart[spi].Availability[0]['fohQty'] == 0){
                        /* console.log("zero foh qty, spi ==",spi);console.log("sources at 360 line no = ",JSON.stringify(sources))*/
                        sources[i].sourcesPart.splice(spi,1);
                        /* onsole.log("sources at 362 line no = ",JSON.stringify(sources))*/
                    }
                }
            }
            lowestPrice = sources[0].sourcesPart[0].Prices.resaleList[0].price;
            lowestPriceArrayIndex = 0;
            sourcePartId = sources[0].sourcesPart[0].sourcePartId;
            /*console.log(JSON.stringify(sources));*/
            var qty = $("#quantity_"+partId).val();
            alogoForQtySeperationRecFunc(sources,qty,totalAvailableQty, partNo,partId,manufacturer);
        }
        function submitAddCard(partNo) {
            var count = 0;
            $('.pop-quantity-input-'+partNo).each(function() {
                var inputQty = $(this).val();
                var avaQty = $(this).attr('data-parsley-max');
                var sourcePartId = $(this).data('source');
                /*  console.log("inputQty===",inputQty,"avaQty==",avaQty,"sourcePartId==",sourcePartId,"partNo===",partNo);       */
                if(parseInt(inputQty) > parseInt(avaQty)){
                    count++;
                    $('#message_'+sourcePartId).show();
                }else{
                    $('#message_'+sourcePartId).hide();
                }
            });
            if(count > 0){
                return false;
            }else{
                addToCart(partNo);
            }
        }
        $.fn.serializeObject = function() {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function() {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };
        function addToCart(partNo){
            console.log("partId form "+partNo);
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var url = "/add-bom-part-to-cart";
            $.ajax({
                type: "POST",
                url: BASE_URL + url,
                data:$('#bompart_'+partNo).serialize(),
                beforeSend: function() {
                    $(".loader").css("display",'block');
                },
                success: function(data) {
                    $(".loader").css("display",'none');
                    /*    console.log(data);*/
                    if (data.status) {
                        var currentUrl = window.location.href;
                        if (currentUrl.indexOf('cart') >= 0) {
                            location.reload();
                        } else {
                            window.location.href = "/cart";
                        }
                    }else {
                        alert(data.message);
                    }
                },
                error: function(data) {
                    $(".loader").css("display",'none');
                    console.log('Error:', data);
                }
            });
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        $('.quantity-input').on('change',function(){
            var inputQty = $(this).val();
            var avaQty = $(this).data('quantity');
            if(inputQty == ''){
                $(this).parent().find('.error-max-qty').html('Please enter quantity.<br/>').show();
            }else if(parseInt(inputQty) > parseInt(avaQty)){
                $(this).parent().find('.error-max-qty').html('Entered quantity is more than available quantity.<br/>').show();
            }else{
                $(this).parent().find('.error-max-qty').hide();
            }
        });
        $('.pop-quantity-input').on('change',function(){
            var inputQty = $(this).val();
            var avaQty = $(this).attr('data-parsley-max');
            var sourcePartId = $(this).data('source');
            if(parseInt(inputQty) > parseInt(avaQty)){
                $('#message_'+sourcePartId).show();
            }else{
                $('#message_'+sourcePartId).hide();
            }
        });

        function getAllPartForQuote() {
            var allBomPartArray = [];
            $("input[data-allquote]").each(function(){
                var datecode            = $(this).data('datecode');
                var availabledatainput  = $(this).data('availabledatainput');
                var resalelistinput     = $(this).data('resalelistinput');
                var instock             = $(this).data('instock');
                var customerpartnoinput = $(this).data('customerpartnoinput');
                var sourcepartid        = $(this).data('sourcepartid');
                var mfrcd               = $(this).data('mfrcd');
                var manufacturer        = $(this).data('manufacturer');
                var partnum             = $(this).data('partnum');
                var qtyinput            = $(this).data('qtyinput');
                var fohqty              = $(this).data('fohqty');

                var quoteItemObj =  {
                    quantity                :   $("#"+qtyinput).val(),
                    productId               :   partnum,
                    manufacturer            :   manufacturer,
                    mfrCd                   :   mfrcd,
                    sourcePartId            :   sourcepartid,
                    customerPartNo          :   $("#"+customerpartnoinput).val(),
                    stockAvailability       :   instock,
                    resaleListData          :   $("#"+resalelistinput).val(),
                    availableData           :  $("#"+availabledatainput).val(),
                    dateCode                :   datecode,
                    requestedQuantity       :   $("#"+qtyinput).val(),
                    requestedCost           :   '',
                    requestedDeliveryDate   :   ''
                };
                allBomPartArray.push(quoteItemObj);
                //quoteModule.addToQuote(row.qtyInputId, row.mpn, row.manufacturer, row.mfrCd,row.sourcePartId, row.customerPartNo, row.stockAvailability,row.resaleListData,row.availableData,row.dateCode,popupRequestedQuantity,popupRequestedCost,popupRequestedDeliveryDate);
            });

            var url = "/add-all-bom-to-quote";
            $.ajax({
                type: "POST",
                url: BASE_URL + url,
                data: {
                    allBomPartArray: allBomPartArray
                },
                beforeSend: function() {
                    $(".loader").css("display",'block');
                },
                success: function(response) {
                    $(".loader").css("display",'none');
                    if (response.status) {
                        $.notify(response.message, "success");
                        $("#quoteCount").html(response.quoteItemCount);
                    }else{
                        $.notify(response.message, "error");
                    }
                },
                error: function(error) {
                    $(".loader").css("display",'none');
                    $.notify(error, "error");
                }
            });

        }




    </script>
@endsection

