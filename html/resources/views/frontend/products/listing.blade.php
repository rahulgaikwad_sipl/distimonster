    @extends('frontend.layouts.app')
    <?php
    $part =  app('request')->input('partNumber');
    ?>
    @if($part)
            @section('title', ' | '.$part)
        @else
            @section('title', '')
    @endif
    @section('content')
        <!--START: Products list-->
        <section class="gray-bg p-t15">
            <div class="container clearfix">
            </div>
            @if(isset($bom_id))
				<div class="container-fluid">
				<div class="row">
                <div class="col-lg-10 text-left p-b15" id="add_all_to_quote">
				<div class="form-inline">
                    @if (Auth::check())
						
					
                    <!-- <button type="button" class="m-t9 btn btn-primary btn-outline p-tb5 " onclick="addProduct('addAllSelectedPartsToCart');" title="Adds items to cart for immediate purchase"><span class="img-icon"><img src="{{url('frontend/assets/images/cart-small-icon.png')}}" alt="add-quote-icon" /></span>Add all to Cart</button> -->
                  

				<div class="form-group"> 
    <div class="col-sm-12">
      <div class="checkbox">
	   <label class="f-s14 font-weight-bold text-blue mt-3"  title="All items with available stock will be added to your cart">
                    <input type="checkbox" class="selectall m-r5" id = "orderAllitem" disabled="disabled"  onclick="addProduct('addAllSelectedPartsToCart', this); " style="margin-top: -20px;"/>
           SELECT ALL ITEMS TO ORDER <br> BASED ON BEST PRICE</label>
		   
       
      </div>
    </div>
  </div>      @else
                    <?php $redirectUrl = $_SERVER['REQUEST_URI'].'/'.$bom_id;?>
                    <button class="m-t9 btn btn-primary btn-outline p-tb5 " type="button" name="button"  onclick="window.location.href='{{ url('login'.$redirectUrl)}}'" value="" title="Adds items to cart for immediate purchase"><span class="img-icon"><img src="{{url('frontend/assets/images/cart-small-icon.png')}}" alt="add-quote-icon" /></span> Add all to Cart </button>
                    @endif
					<div class="form-group"> 
    <div class="col-sm-12">
      <div class="checkbox">
	   <label class="text-blue f-s14 font-weight-bold" title="Request lead times for items out of stock">
         <input type="checkbox" class="selectall m-r5" id = "quoteAllitem" disabled="disabled" onclick="addProduct('getAllPartForQuote', this);"/>
                 
				ADD ITEMS TO RFQ</label>
		   
       
      </div>
    </div>
  </div>
  
                    
             
					 

<div class="form-group"> 
    <div class="col-sm-12">
      <div class="checkbox">
	   <label class="text-blue f-s14 font-weight-bold" title="Request a quote on items that are unidentified">
                            <input type="checkbox" class="selectall m-r5" id = "addUnavailableAllitem" disabled="disabled" onclick="addProduct('addUnavailableProductToQuote', this);"/>
   ADD UNIDENTIFIED ITEMS TO RFQ</label>
		   
       
      </div>
    </div>
  </div>                   
				<span class="text-right m-r9">
                        <button type="button" id="add_mpn" class="btn btn-primary btn-outline p-tb5 requestquote-btn-custom" title="Submit" onclick="itemSelection();">Submit</button>
                     </span>
					 </div>
                </div>
                 <div class="col-lg-2">      
				 <button type="button" id="save_bom" class="btn btn-primary btn-outline p-tb5 requestquote-btn-custom pull-right" title="{{ Request::segment(1) == 'bom' ? 'SAVE' : 'UPDATE' }} MONSTER-BOM" data-target="#save-bom-modal" data-toggle="modal">{{ Request::segment(1) == 'bom' ? 'SAVE' : 'UPDATE' }} MONSTER-BOM</button>    
       </div>
	   </div>
	   </div>
            @endif
            <div class="col-lg-12">
                <div class="table-dresponsive">
                    <table class="table  table-bom-product table-striped product-list-table table-fixed">
                        <tbody>
                        <tr class="f-s16 font-w500 black">
                        <?php /*     
                        @if(isset($bom_id))<th class="v-align text-center"><div class="select-all">Select All<br/><input  class="checked_all" type="checkbox" value="1" /></div></th>@endif */ ?>
                            @if(isset($bom_id))
                            <th class="v-align text-center">Item Selection</th>
                            @endif
                            <th>Distributor</th>
                            <th width="200">Part#</th>
                            <th>Price</th>
                            <th width="150">Quantity</th>
                            <th>Stock</th>
                            <th>Description</th>
                            <th width="150">Category</th>
                        </tr>

                        @foreach ($finalData as  $key=>$data)
						   
                            @if(isset($data['partNum']))
                                <form name="bompart_{{$data['partNum']}}" id="bompart_{{str_replace(' ', '_', $data['itemId'])}}_{{str_replace(' ', '_', $data['mfrCd'])}}" data-parsley-validate="true" >
                                    <?php $availabilityQty = 0; ?>
                                    @if(!empty($data['sources']))
                                        @foreach($data['sources'] as $sources)
                                            @foreach($sources['sourcesPart'] as $sellerInfo)
                                                @if($sellerInfo['inStock'])
                                                    <?php  $availabilityQty =  $availabilityQty+$sellerInfo['Availability'][0]['fohQty'];?>
                                                @endif
                                            @endforeach
                                        @endforeach
                                    @endif
                                    <tr class="f-s14 text-gray">
                                    <?php  $lowestPrice = 0; ?>    
                                        @if (Auth::check())    
                                            @if(!empty($data['sources']))
                                                @foreach($data['sources'] as $sources)
                                                    <?php
                                                    $lowestPrice =  \Helpers::getLowestPrice($sources);
                                                    ?>
                                                @endforeach
                                            @endif
                                        @endif  
                                        @if(isset($bom_id))
                                            <input type="hidden" name="bomId" value="{{$bom_id}}" />
                                            
                                            <td class="text-center">
											
											
											<?php
												/*print_r($data['bestPrice']);
												if(!empty($data['bestPrice']){
													if($data['mpnFound']){
														$data['requestedQty'] = 
													}
												}*/
												?>
											
											@if(!empty($data['bestPrice']) || $availabilityQty < 0)
                                            <div>Select Item
                                            <input type="checkbox" class="checkbox-qty {{ $availabilityQty > 0 ? 'in_stock' : 'out_of_stock'}}" data-requestedPrice="{{$lowestPrice}}" data-distributor="{{$data['distributorName']}}" data-formid="bompart_{{str_replace(' ', '_', $data['itemId'])}}_{{str_replace(' ', '_', $data['mfrCd'])}}" data-availabledatainput="availableData_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" data-datecode="<?php echo $sources['sourcesPart'][0]['dateCode'];?>" data-resalelistinput="resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>"  data-instock="<?php echo Helpers::getInStockFromSources($sources['sourcesPart']);?>"  data-customerpartnoinput="<?php echo !empty($data['internalPart']) ? $data['internalPart'] : '';?>" data-sourcepartid="{{$sources['sourcesPart'][0]['sourcePartId']}}" data-partnum="<?php echo $data['partNum']; ?>"  data-manufacturer="<?php echo $data['manufacturer']; ?>"   data-fohqty="<?php echo $sources['sourcesPart'][0]['Availability'][0]['fohQty']; ?>" data-qtyinput="quantity_<?php echo $data['itemId']; ?>"  data-source="{!! base64_encode(json_encode($data['sources'])) !!}" data-itemid="{{$data['itemId']}}" data-availabilityqty="{{$availabilityQty}}" data-partnum="{{$data['partNum']}}" data-mfrcd="{{str_replace(' ', '_', $data['mfrCd'])}}"  id="checkbox_{{$data['itemId']}}"/>
                                            </div>
											@endif
                                        </td>
                                        @endif
                                        @if(!empty($data['distributorName']))
                                        <td><strong>{{$data['distributorName']}}</strong><input type="hidden" name="distributorName" id="distributorName" value="{{$data['distributorName']}}"/></td>
                                        @else
                                        <td>N/A</td>
                                        @endif
                                        <td>
                                            <div class="hind-font text-pink" href="javascript:void(0)">MPN: {{$data['partNum']}}</div>
                                            <div class="block m-t5 m-b10">
                                                @if(!empty ($data['EnvData']['compliance']))
                                                    @if($data['EnvData']['compliance'][0]['displayLabel'] != "")
                                                        <span class="img-icon"><img src="{{url('frontend/assets/images/rohslogo.png')}}" alt="pending" /></span>RoHS Compliant
                                                    @endif
                                                @endif
                                            </div>
                                            <?php   $sheetData =  \Helpers::getDataSheet($data['partNum'],$data['manufacturer']);?>
                                            <div class="block m-t10 m-b10">
                                                @if(!empty($sheetData) && Auth::check())
                                                    <a target="_blank" href="{{$sheetData[0]['datasheet_url']}}" class="data-sheet"><img src="{{url('/frontend/assets/images/pdf-icon.png')}}" width="12px"> Datasheet</a>
                                                @else
                                                <?php $redirectUrl = $_SERVER['REQUEST_URI'];?>
                                                    <a target="_blank" href="javascript:void(0);" class="data-sheet" onclick="window.location.href='{{ url('login'.$redirectUrl)}}'"><img src="{{url('/frontend/assets/images/pdf-icon.png')}}" width="12px"> Datasheet</a>
                                                @endif
                                            </div>
                                            Manufacturer: <span class="black font-w500">{{$data['manufacturer']}}</span><br>
                                            <?php if(!empty($data['internalPart'])) { ?>
                                                Internal Part: <span class="black font-w500">{{$data['internalPart']}}</span>
                                            <?php } ?>
                                            
                                        </td>
                                        <td>
                                        @if (Auth::check())
                                        <span class="black font-w500" id="item_price_div_25360740">
                                        <?php  $lowestPrice = 0; ?>
                                                @if(!empty($data['sources']))
                                                    @foreach($data['sources'] as $sources)
                                                        <?php
                                                        $lowestPrice =  \Helpers::getLowestPrice($sources);
                                                        ?>
                                                    @endforeach
                                                @endif
                                               $<?php echo number_format($lowestPrice, 3, '.', ','); ?>
                                        </span>
                                            <br>
                                            <!-- Large modal -->
                                        <!-- Show Buying option only if  instock-->
                                        @if($availabilityQty)<a class="" onclick="alogoForQtySeperation({{json_encode($data['sources'])}},'{{$data['itemId']}}',{{$availabilityQty}}, '','{{str_replace(' ', '_', $data['mfrCd'])}}', '<?php echo !empty($data['distributorName'])?$data['distributorName']:'N/A'; ?>')" data-target=".bs-example-modal-lg-{{$data['itemId']}}" data-toggle="modal" href="#" type="button">Price Breaks</a>@endif
                                            <div aria-labelledby="myLargeModalLabel" class="modal fade bs-example-modal-lg-{{$data['itemId']}}" role="dialog" tabindex="-1">
                                                <?php echo $data['partNum'];?>
                                                <div class="modal-dialog modal-lg modal-custom" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header bdr-none">
                                                            <div class="modal-title">
                                                                <h4>Price Breaks for {{$data['partNum']}}</h4>
                                                            </div>
                                                            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon"></span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row p-b5">
                                                                <div class="col-md-6"><span class="f-s18 text-gray">Quantity: </span><span class="black font-w500 f-s18 requestQtyDiv pinck-color" id="requestQtyDiv"></span> </div>
                                                                <div class="col-md-6 text-right">
                                                                    <span class="f-s18 text-gray">Available stock: </span><span class="text-right black font-w500 f-s18  green-color" id="totalStockQty"> {{$availabilityQty}}</span>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <div class="show-data">
                                                                @if(!empty($data['sources']))
                                                                    <table cellspacing="0" cellpadding="0" border="0" class="buying-options-table">
                                                                        <tr>
                                                                            {{--<th>Source Id</th>--}}
                                                                            <th>Distributor</th>
                                                                            <th>Min Quantity</th>
                                                                            <th>Price</th>
                                                                            <th>Available Qty</th>
                                                                            <th>Quantity</th>
                                                                        </tr>
                                                                        @foreach($data['sources'] as $sources)
                                                                            @foreach($sources['sourcesPart'] as $key=>$sellerInfo)
                                                                            <!--removed check if item not in stock-->
                                                                                @if(isset($sellerInfo['Prices']) && $sellerInfo['Availability'][0]['fohQty'] >0)
                                                                                     @if(!empty($sellerInfo['Prices']['resaleList']))
                                                                                    <tr>
                                                                                        <td>
                                                                                            <?php $minQty = 0; ?>
                                                                                            <?php  $minQty = $sources['sourcesPart'][$key]['minimumOrderQuantity'];?>
                                                                                            <?php echo !empty($sellerInfo['displayName'])?$sellerInfo['displayName']:'N/A'; ?>
                                                                                            {{--{{$sellerInfo['sourcePartId'] }}--}}
                                                                                            <input class="bor-radius-none quantity form-control quantity-box internal-part_pop{{$data['itemId']}}_{{str_replace(' ', '_', $data['mfrCd'])}}" id="customerPartNo_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>" name="customerPartNo[]" placeholder="Internal part#" data-parsley-maxlength="30"  value="<?php echo !empty($data['internalPart']) ? $data['internalPart'] : '';?>" />
                                                                                                <input  type="hidden" class="bor-radius-none quantity form-control quantity-box" value="{{$minQty}}" id="sourceMinQty_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>" name="sourceMinQty[]" placeholder="Source Min Qty" data-parsley-maxlength="10"  />
                                                                                        </td>
                                                                                        <td>
                                                                                            @foreach($sellerInfo['Prices']['resaleList'] as $resaleList)
                                                                                            <?php echo  $resaleList['minQty']; ?>+ <br/>
                                                                                        @endforeach
                                                                                        </td>
                                                                                        <td>
                                                                                            @foreach($sellerInfo['Prices']['resaleList'] as $resaleList)
                                                                                            $<?php echo  !empty($resaleList['price']) ? $resaleList['price'] : ''; ?> <br/>
                                                                                        @endforeach
                                                                                        </td>
                                                                                        <td>
                                                                                            @if($sellerInfo['Availability'][0]['fohQty'] >0)
                                                                                                <?php  echo $sellerInfo['Availability'][0]['fohQty']; ?><br />
                                                                                                <span class="green-color"><?php  echo $sellerInfo['Availability'][0]['availabilityMessage']; ?></span>
                                                                                            @else
                                                                                                <?php  echo $sellerInfo['Availability'][0]['fohQty']; ?><br />
                                                                                                <span class="red-color"><?php  echo $sellerInfo['Availability'][0]['availabilityMessage']; ?></span>
                                                                                            @endif
                                                                                        </td>
                                                                                        <td>
                                                                                            @if($sellerInfo['Availability'][0]['fohQty'] >0)
                                                                                            <input class="bor-radius-none customerPartNo form-control quantity-box pop-quantity-input pop-quantity-input_{{$data['itemId']}}_{{str_replace(' ', '_', $data['mfrCd'])}}" onpaste="return false;" onkeypress="return isNumber(event)" data-source="{{str_replace(':', '_',$sellerInfo['sourcePartId'])}}" data-parsley-min="{{$minQty}}"  data-parsley-max="{{$sellerInfo['Availability'][0]['fohQty']}}"  id="quantity_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>" name="quantities[]" value=""  placeholder="Quantity" />
                                                                                            @else
                                                                                                <input class="bor-radius-none customerPartNo form-control quantity-box pop-quantity-input pop-quantity-input_{{$data['itemId']}}_{{str_replace(' ', '_', $data['mfrCd'])}}" onpaste="return false;" onkeypress="return isNumber(event)" data-source="{{str_replace(':', '_',$sellerInfo['sourcePartId'])}}" data-parsley-max="{{$sellerInfo['Availability'][0]['fohQty']}}"  id="quantity_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>" name="quantities[]" value=""  placeholder="Quantity" />
                                                                                            @endif

                                                                                            <span id="message_{{str_replace(':', '_',$sellerInfo['sourcePartId'])}}" class="error-max-qty text-red" style="display: none; color: red; font-size: 11px;">Entered quantity is more than available quantity.<br/></span>
                                                                                             <i class="info-text">Increments of 1 <br />Minimum <?php echo $minQty; ?></i>
                                                                                             <input type="hidden" name="productId[]" value="{{$data['partNum']}}" />
                                                                                            <input type="hidden" name="manufacturer[]" value="{{$data['manufacturer']}}" />
                                                                                            <input type="hidden" name="mfrCd[]" value="{{$data['mfrCd']}}" />
                                                                                            <input type="hidden" name="sourcePartId[]" value="{{$sellerInfo['sourcePartId']}}" />
                                                                                            <input type="hidden" name="stockAvailability[]" value="{{$sellerInfo['inStock']}}" />
                                                                                            <input type="hidden" name="dateCode[]" value="{{$sellerInfo['dateCode']}}" />
                                                                                        </td>
                                                                                    </tr>
                                                                                @endif
                                                                                @endif
                                                                            @endforeach
                                                                        @endforeach
                                                                    </table>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        @if(!empty($data['sources']))
                                                            @if($availabilityQty >0)
                                                                <div class="modal-footer p-t0"><button type="button" onClick="submitAddCart('{{str_replace(' ', '_', $data['itemId'])}}_{{str_replace(' ', '_', $data['mfrCd'])}}')" class="btn btn-primary" title="Adds items to cart for immediate purchase">Done & Add to cart</button></div>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="productDetails" value="{{json_encode($data)}}"></input>
                                            @else
                                            @if(isset($bom_id)) 
                                                <?php $bomId = '/'.$bom_id;?>
                                            @else
                                                <?php $bomId = '';?>
                                            @endif
                                            <?php $redirectUrl = $_SERVER['REQUEST_URI'];?>
                                            <a class="m-t9 btn btn-primary btn-outline p-tb5 " href="{{ url('login'.$redirectUrl.$bomId)}}" title="Please login to view the price.">Login</a>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="text-center">
                                                @if($availabilityQty>0)
                                                <input class="bor-radius-none quantity form-control quantity-box quantity-input" onpaste="return false;" onkeypress="return isNumber(event)" id="quantity_{{$data['itemId']}}" data-quantity="{{ $availabilityQty?$availabilityQty:0 }}" name="quantity"  data-parsley-type="digits"  required  max='{{ $availabilityQty?$availabilityQty:0 }}'  min="1" placeholder="Enter Quantity" value="{{$data['requestedQty']}}">
                                                <span id="message_{{$data['itemId']}}" class="error-max-qty text-red" style="display: none; color: red; font-size: 11px;">Entered quantity is more than available quantity.<br/></span>
                                                @else
                                                    <input class="bor-radius-none quantity form-control quantity-box quantity-input" onpaste="return false;" onkeypress="return isNumber(event)" id="quantity_{{$data['itemId']}}" data-quantity="{{ $availabilityQty?$availabilityQty:0 }}" name="quantity"  data-parsley-type="digits"  required   placeholder="Enter Quantity" value="{{$data['requestedQty']}}">
                                                    <span id="message_{{$data['itemId']}}" class="error-max-qty text-red" style="display: none; color: red; font-size: 11px;">Entered quantity is more than available quantity.<br/></span>
                                                @endif
                                                    <input  type="hidden" class="bor-radius-none customerPartNo form-control quantity-box" id="customerPartNoID_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" name='customerPartNoQuote' value='' placeholder="Internal Part#"  />
                                                @if(isset($sources['sourcesPart'][0]['Prices']))
                                                    <input  type="hidden" class="bor-radius-none customerPartNo form-control quantity-box" value='<?php echo json_encode($sources['sourcesPart'][0]['Prices']['resaleList'])?>' id="resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" name='resaleList'  placeholder="resaleList" />
                                                @else
                                                    <input  type="hidden" class="bor-radius-none customerPartNo form-control quantity-box" value='<?php echo json_encode([])?>' id="resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" name='resaleList'  placeholder="resaleList" />
                                                @endif
                                                <input type="hidden" class="bor-radius-none customerPartNo form-control quantity-box"  value='<?php echo json_encode($sources['sourcesPart'][0]['Availability'])?>' id="availableData_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" name='availableData'  placeholder="availableData" />

                                                @if($availabilityQty)
                                                
                                                    @if (Auth::check())
                                                    <?php $distributorName = !empty($data['distributorName'])?$data['distributorName']:config('constants.DEFAULT_DISTRIBUTOR_NAME');?>
                                                    <button class="btn btn-primary p-tb5 m-t10" type="button" onclick="alogoForQtySeperation({{json_encode($data['sources'])}},'{{$data['itemId']}}','{{$availabilityQty}}','{{str_replace(' ', '_',$data['partNum'])}}','{{str_replace(' ', '_', $data['mfrCd'])}}', '{{$distributorName}}', true)" name="button" value="" title="Adds items to cart for immediate purchase"><span class="img-icon"><img src="{{url('frontend/assets/images/cart-bag-icon.png')}}" alt="add-quote-icon"></span> Add to cart </button>
                                                    @else
                                                    <button class="btn btn-primary p-tb5 m-t10" type="button" name="button"  onclick="window.location.href='{{ url('login'.$redirectUrl)}}'" value="" title="Adds items to cart for immediate purchase"><span class="img-icon"><img src="{{url('frontend/assets/images/cart-bag-icon.png')}}" alt="add-quote-icon"></span> Add to cart </button>
                                                    @endif
                                                @endif

                                                <input type="hidden" class="all-quote-items" data-allquote="1"  data-fohqty="<?php echo $sources['sourcesPart'][0]['Availability'][0]['fohQty']; ?>" data-qtyinput="quantity_<?php echo $data['itemId']; ?>"  data-partnum="<?php echo $data['partNum']; ?>"  data-manufacturer="<?php echo $data['manufacturer']; ?>"  data-mfrcd="{{$data['mfrCd']}}" data-sourcepartid="{{$sources['sourcesPart'][0]['sourcePartId']}}" data-customerpartnoinput="<?php echo !empty($data['internalPart']) ? $data['internalPart'] : '';?>"  data-instock="<?php echo $sources['sourcesPart'][0]['inStock']?>"  data-resalelistinput="resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" data-availabledatainput="availableData_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" data-datecode="<?php echo $sources['sourcesPart'][0]['dateCode'];?>"/>

                                                @if(!empty($sources['sourcesPart'][0]['Prices']))
                                                     @if (Auth::check())
                                                        <button type="button" id="addToQuote"  onclick="productListModule.addToQuoteProductListing('<?php echo !empty($data['distributorName'])?$data['distributorName']:'N/A'; ?>', '<?php echo $sources['sourcesPart'][0]['minimumOrderQuantity']?>', '<?php echo $sources['sourcesPart'][0]['Availability'][0]['fohQty']; ?>','quantity_<?php echo $data['itemId']; ?>' ,'<?php echo $data['partNum']; ?>','<?php echo $data['manufacturer']; ?>', '<?php if(isset($data['mfrCd'])){echo $data['mfrCd'];}else{ echo $data['manufacturer']; } ?>','<?php echo $sources['sourcesPart'][0]['sourcePartId'];?>','<?php echo !empty($data['internalPart']) ? $data['internalPart'] : $sources['sourcesPart'][0]['sourcePartId'];?>','<?php echo $sources['sourcesPart'][0]['inStock']?>' ,'resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>','availableData_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>','<?php echo $sources['sourcesPart'][0]['dateCode'];?>', '<?php echo $availabilityQty > 0 ? 'in_stock' : 'out_of_stock'; ?>')" class="m-t9 btn btn-primary btn-outline p-tb5" title="{{ !empty($availabilityQty) ? 'Request specific prices' : 'Request Lead Time'}}"><span class="img-icon"><img src="{{url('frontend/assets/images/add-quote-icon.png')}}" alt="add-quote-icon" /></span>Add to RFQ</button>  
                                                     @else
                                                     <button type="button" id="addToQuote"  onclick="window.location.href='{{ url('login'.$redirectUrl)}}'" class="m-t9 btn btn-primary btn-outline p-tb5 " title="Add to RFQ"><span class="img-icon"><img src="{{url('frontend/assets/images/add-quote-icon.png')}}" alt="add-quote-icon" /></span>Add to RFQ</button>
                                                    @endif
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            @if($availabilityQty)
                                                <span class="f-s14 success font-w500">
                                                (<?php echo $availabilityQty; ?> Available)
                                                    <input type="hidden" name="totalAvailableQty" value="{{$availabilityQty}}" />
                                                </span>
                                            @else
                                                <span class="f-s14 cancelled font-w500">Out of stock
                                                    <input type="hidden" name="totalAvailableQty" value="0" />
                                                </span>
                                            @endif
                                        </td>
                                        <td>{{$data['desc']}}</td>
                                        <td width="350">
                                            @if(!empty($sheetData))
                                                {{$sheetData[0]['category']}}
                                            @else
                                                N/A
                                            @endif
                                        </td>
                                    </tr>
                                </form>
                            @else
                            @endif
                        @endforeach
                        @if(isset($noPartFound))
                            @foreach($noPartFound as $key=>$value)
                                <tr class="f-s14 text-gray">
                                    <td class="v-align text-center">
                                    <div>Select Item</div>
                                        <input type="hidden" value="1" />
                                        <input type="checkbox" class="checkbox-qty checkbox-nopart-found" data-internalpart="{{ !empty($value['internalPart']) ? $value['internalPart'] : ''}}" data-xlxsquantity="{{$value['QTY']}}" value="{{$value['partNum']}}"/>
                                    </td>
                                    <td>
                                        <a class="hind-font" href="javascript:void(0)"><span class="f-s14 cancelled  font-w500">MPN: {{$value['partNum']}}</span></a>
                                        <br>
                                        <span class="f-s14 cancelled font-w500">
                                                    We are sorry we could not find your part
                                         </span>
                                    </td>
                                    <td><?php if(!empty($value['internalPart'])) { ?>
                                                Internal Part: <span class="black font-w500">{{$value['internalPart']}}</span>
                                            <?php }else{
                                                echo 'N/A';
                                            } ?>
                                    </td>
                                    <td width="200">N/A</td>
                                    <td>
                                        <?php /*<button type="button" class="m-t9 btn btn-primary btn-outline p-tb5 requestquote-btn-custom request_product-btn" title="Request Lead Time"><span class="img-icon"><img src="{{url('frontend/assets/images/add-quote-icon.png')}}" alt="add-quote-icon" /></span>Add Unavailable to RFQ</button> */ ?>
                                        <button type="button" id="addToQuote"  onclick="productListModule.addToQuoteProductListing('N/A', '0', '0', '<?php echo $value['QTY']; ?>', '<?php echo $value['partNum']; ?>', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'not_found')" class="m-t9 btn btn-primary btn-outline p-tb5 requestquote-btn-custom" title="Request Lead Time"><span class="img-icon"><img src="{{url('frontend/assets/images/add-quote-icon.png')}}" alt="add-quote-icon" /></span>Add Unavailable to RFQ</button>
                                    </td>
                                    <td>N/A</td>
                                    <td width="350">N/A</td>
                                    <td width="350">N/A</td>
                                </tr>
                            @endforeach
                                <div class="modal" id="request-part-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="modalLabel">Request Lead Time</h4>
                                                <button type="button" class="close" data-dismiss="modal" onclick="quoteModule.closeAddToQuoteModel()" aria-label="Close">
                                                    <span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon" /></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['class'=>'form-horizontal','method' => 'POST', 'id' => 'request_part_detail_form', 'data-parsley-validate' => true]) !!}
                                                <div class="price-content-box">
                                                    <div class="price-content-body">
                                                        <div class="clearfix m-b5 f-s14"></div>
                                                        <div class="form-group">
                                                            <div class="col-md-12 p-l0">
                                                                <input type="hidden" name="popupPartNumber[]" required=""  data-parsley-required  data-parsley-maxlength="50"  value="{{$part}}" id="popupPartNumber" placeholder="MPN*" class="quantity-box"  data-parsley-minlength="2"/>
                                                                <input type="hidden" name="popupInternalPart[]" required=""  data-parsley-required  data-parsley-maxlength="50"  value="" id="popupInternalPart" data-parsley-minlength="2"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-12 p-l0">
                                                                <input type="text" readonly name="popupExpectedDeliveryDate" value="" class="requested-delivery-date quantity-box" id="popupExpectedDeliveryDate" placeholder="Expected Delivery Date"  />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12 p-l0">
                                                                <input type="hidden" name="popupRequestedQuantities[]" data-parsley-required data-parsley-type="digits"  data-parsley-maxlength="10"  value="" id="popupRequestedQuantities" placeholder="Requested Quantity*" class="quantity-box" min="1"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-12 p-l0">
                                                                <input type="text" name="popupExpectedUnitPrice" data-parsley-pattern="^\s*(?=.*[1-9])\d*(?:\.\d{1,6})?\s*$"  data-parsley-maxlength="20" data-parsley-type="number" value="" id="popupExpectedUnitPrice" placeholder="Target Price (per unit in $)" class="quantity-box"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                    <div class="col-md-12 p-l0">
                                        <input type="text" name="popupLeadQuoteName" value="" id="popupLeadQuoteName" placeholder="Quote Name" class="quantity-box"/>
                                    </div>
                                </div>
                                                        <div class="form-group">
                                                            <div class="col-md-12 p-l0">
                                                                <input type="hidden" id="popupCategoryName">
                                                              </div>
                                                        </div>
                                                        <div id="cat_error"></div>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" data-dismiss="modal" class="btn btn-primary btn-outline" title="Cancel" onclick="quoteModule.closeAddToQuoteModel()"><span class="img-icon"><img src="{{url('frontend/assets/images/pop-up-close-icon.png')}}" alt="pop-up-close-icon" /></span>Cancel</button>
                                                <button type="button" onclick="addToRequestedPart()" class="btn btn-primary" title="Request Lead Time"><span class="img-icon"><img src="{{url('frontend/assets/images/add-white-icon.png')}}" alt="pop-up-edit-icon" /></span>Request Lead Time</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal" id="save-bom-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="savebomModalLabel">MONSTER-BOM</h4>
                                                <button type="button" class="close" data-dismiss="modal" onclick="bom.closeSaveBomModel()" aria-label="Close">
                                                    <span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon" /></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                {!! Form::open(['class'=>'form-horizontal','method' => 'POST', 'id' => 'save_bom_form', 'data-parsley-validate' => true]) !!}
                                                <div class="price-content-box">
                                                    <div class="price-content-body">
                                                        <div class="clearfix m-b5 f-s14"></div>
                                                        <div class="form-group">
                                                            <div class="col-md-12 p-l0">
                                                                <input type="text" name="bomName" value="{{!empty($bom_name)?$bom_name:''}}" id="bomName" placeholder="Monster-BOM Name*" class="quantity-box" data-parsley-required/>
                                                                <input type="hidden" name="updateBomId" value="{{!empty($bom_id)?$bom_id:''}}" id="updateBomId" data-parsley-required/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" data-dismiss="modal" class="btn btn-primary btn-outline" title="Cancel" onclick="bom.closeSaveBomModel()"><span class="img-icon"><img src="{{url('frontend/assets/images/pop-up-close-icon.png')}}" alt="pop-up-close-icon" /></span>Cancel</button>
                                                <button type="button" onclick="saveBomAjax()" class="btn btn-primary" title="Request Lead Time"><span class="img-icon"><img src="{{url('frontend/assets/images/add-white-icon.png')}}" alt="pop-up-edit-icon" /></span>Save</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
        </section>
    @stop
@section('javascript')
    <script src="{{ asset('frontend/js/product-list.js') }}"></script>
    <script src="{{ asset('frontend/js/jquery-listnav.min.js') }}"></script>
    <script src="{{ asset('frontend/js/category-part-search.js') }}"></script>
    <script src="{{ asset('frontend/js/bom-upload.js') }}"></script>
@endsection

