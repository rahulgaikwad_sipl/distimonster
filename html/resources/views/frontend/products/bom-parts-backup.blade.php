@extends('frontend.layouts.app')
@section('content')
<!--START: Products list-->
<section class="gray-bg" style="background: #fff;">
    <div class="container clearfix">
    </div>
    <?php  echo "<pre>"; print_r(json_encode($finalData));echo "</pre>";?>
    <div class="col-lg-12">
        <div class="table-dresponsive">
            <table class="table  table-bom-product table-striped">
                <tbody>
                    <tr class="f-s16 font-w500 black">
                        <th>
                            <input type="hidden" value="1" />
                    </th>
                    <th>
                        Part#
                    </th>
                    <th>
                        Price
                    </th>
                    <th width="200">
                        Quantity
                    </th>
                    <th>
                        Stock
                    </th>
                    <th>
                        Description
                    </th>
                    <th width="350">
                        Category
                    </th>
                    <th>
                        Action
                    </th>
                </tr>

                @foreach ($finalData as  $key=>$data)
                    <form name="bompart_{{$data['partNum']}}" id="bompart_{{$data['partNum']}}" data-parsley-validate="true" >

                    <tr class="f-s14 text-gray">
                    <td>
                        <input type="checkbox" class="checkbox-qty" data-itemid="{{ $key }}" value="1" id="checkbox_{{$key}}" />
                </td>
                <td>
                    <a class="hind-font" href="javascript:void(0)">
                       MPN: {{$data['partNum']}}
                   </a>
                   <br>
                    <?php   $sheetData =  \Helpers::getDataSheet($data['partNum'],$data['manufacturer']);?>

                    <div class="block m-t10 m-b10">
                        @if($data['EnvData']['compliance'][0]['displayLabel'] != "")
                            <span class="img-icon"><img src="{{url('frontend/assets/images/rohslogo.png')}}" alt="pending" /></span>RoHS Compliant
                        @endif
                    </div>
                    <div class="block m-t10 m-b10">
                        @if(!empty($sheetData))
                            <a target="_blank" href="{{$sheetData[0]['datasheet_url']}}" class="data-sheet"><img src="{{url('/frontend/assets/images/pdf-icon.png')}}" width="12px"> Datasheet</a>
                        @endif

                        </div>
                   Manufacturer: {{$data['manufacturer']}}
                   <br>
               </br>
           </td>
           <td>
            <span class="" id="item_price_div_25360740">
             <?php $lowestPrice = 0; ?>
             @if(!empty($data['sources']))
                 @foreach($data['sources'] as $sources)
                     <?php   $lowestPrice =  \Helpers::getLowestPrice($sources);?>
                 @endforeach
             @endif
             $<?php echo $lowestPrice; ?>
         </span>
         <br>
         <!-- Large modal -->
         <?php $availabilityQty = 0; ?>
         @if(!empty($data['sources']))
         @foreach($data['sources'] as $sources)
         @foreach($sources['sourcesPart'] as $sellerInfo)
         @if($sellerInfo['inStock'])
         <?php  $availabilityQty =  $availabilityQty+$sellerInfo['Availability'][0]['fohQty'];
         ?>
         @endif
         @endforeach
         @endforeach
         @endif
         @if($availabilityQty)
         <a class="" onclick="alogoForQtySeperation({{json_encode($data['sources'])}},{{$data['itemId']}},{{$availabilityQty}}, '')" data-target=".bs-example-modal-lg-<?php echo  $key; ?>" data-toggle="modal" href="#" type="button">
            Buying Options
        </a>

        @endif
           <div aria-labelledby="myLargeModalLabel" class="modal fade bs-example-modal-lg-<?php echo  $key; ?>" role="dialog" tabindex="-1">
                <div class="modal-dialog modal-lg modal-custom" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">
                                Buying options for {{$data['partNum']}}
                            </h4>
                            <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                                <span aria-hidden="true">
                                    <img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon">
                                </span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div class="show-data">
                             @if(!empty($data['sources']))
                             @foreach($data['sources'] as $sources)
                             @foreach($sources['sourcesPart'] as $sellerInfo)
                             @if($sellerInfo['inStock'])
                             <div class="row">
                                <div class="col-md-4">
                                    <?php $minQty = 0; ?>

                                    <?php  $minQty = $sellerInfo['Prices']['resaleList'][0]['minQty'];
                                    ?>
                                    <input onkeypress="return isNumber(event)" class="bor-radius-none customerPartNo form-control quantity-box pop-quantity-input pop-quantity-input-{{$data['partNum']}}" data-source="{{str_replace(':', '_',$sellerInfo['sourcePartId'])}}"  data-parsley-max="{{$sellerInfo['Availability'][0]['fohQty']}}"  id="quantity_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>" name="quantities[]" value="" placeholder="Quantity" />
                                        <span id="message_{{str_replace(':', '_',$sellerInfo['sourcePartId'])}}" class="error-max-qty text-red" style="display: none; color: red; font-size: 11px;">Entered quantity is more than available quantity.<br/></span>

                                    <p class="m-b0">Increments of 1</p>
                                    <p>Minimum <?php echo $minQty; ?></p>
                                    <input class="bor-radius-none quantity form-control quantity-box" id="customerPartNo_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>" name="customerPartNo[]" placeholder="Internal part#" value="" />

                                        <input type="hidden" name="productId[]" value="{{$data['partNum']}}" />
                                <input type="hidden" name="manufacturer[]" value="{{$data['manufacturer']}}" />
                                <input type="hidden" name="mfrCd[]" value="{{$data['mfrCd']}}" />
                                <input type="hidden" name="sourcePartId[]" value="{{$sellerInfo['sourcePartId']}}" />
                                <input type="hidden" name="stockAvailability[]" value="{{$sellerInfo['inStock']}}" />
                                <input type="hidden" name="dateCode[]" value="{{$sellerInfo['dateCode']}}" />

                            </div>
                            <div class="col-md-2">
                                <div class="num-avability">
                                 @foreach($sellerInfo['Prices']['resaleList'] as $resaleList)
                                 <p><?php echo $resaleList['minQty']; ?>+</p>
                                 @endforeach
                             </div>
                         </div>
                         <div class="col-md-2">
                            <div class="num-avability">
                             @foreach($sellerInfo['Prices']['resaleList'] as $resaleList)
                             <p>$<?php echo  $resaleList['price']; ?></p>
                             @endforeach
                         </div>
                     </div>
                     <div class="col-md-4">
                        <h6>
                            <?php  echo $sellerInfo['Availability'][0]['fohQty'];
                            ?>
                        </h6>
                        <h6>
                            <?php  echo $sellerInfo['Availability'][0]['availabilityMessage'];
                            ?>
                        </h6>

<!--            <h6>Cut Strip</h6>
                <p>Can ship from the United States today</p>
                <p class="m-b5"><span><label>PIPELINE :</label> 16 Jul: 85000</span></p>
                <p><span><label>LEAD TIME : </label> 9 weeks</span></p>
            -->
        </div>
    </div>
    @endif
    @endforeach
    @endforeach
    @endif

</div>

</div>
<div class="modal-footer"><button type="button" onClick="submitAddCard('{{$data['partNum']}}')" class="btn btn-primary">Done & Add to cart</button></div>
</div>
</div>
</div>
            <input type="hidden" name="productDetails" value="{{json_encode($data)}}" />


</td>
<td>
    <div class="">
    <input class="bor-radius-none quantity form-control quantity-box quantity-input" onkeypress="return isNumber(event)" data-quantity="{{ $availabilityQty?$availabilityQty:0 }}"  id="quantity_{{$data['itemId']}}" name="quantity" placeholder="quantity" value="{{$data['requestedQty']}}">
    <span id="message_{{$data['itemId']}}" class="error-max-qty text-red" style="display: none; color: red; font-size: 11px;">Entered quantity is more than available quantity.<br/></span>
        @if($availabilityQty)
            <input class="btn btn-primary p-tb5 m-t10" type="button" onclick="alogoForQtySeperation({{json_encode($data['sources'])}},{{$data['itemId']}},{{$availabilityQty}}, '{{$data['partNum']}}')" name="button" value="Add to cart" />
        @endif
    </div>

</td>
<td>
    <span class="f-s14 text-gray">
       @if($availabilityQty)
       (<?php echo $availabilityQty; ?> Available)
       @else
       No Stock
       @endif
   </span>
                      <!--   <div>
                            <span>
                                Minimum: 8
                            </span>
                            <span>
                                Increment: 1
                            </span>
                        </div>
                        <input id="source_min_qty_25360740" type="hidden" value="8">
                            <p class="">
                                Availability Message:
                                <span class="black">
                                    In Stock
                                </span>
                            </p>
                        </input> -->
                    </td>
                    <td>
                      {{$data['desc']}}
                  </td>
                  <td width="350">
                      @if(!empty($sheetData))
                         {{$sheetData[0]['category']}}
                      @endif
                </td>
                <td class="action">
                    <ul class="list-inline">
                        <li class="list-inline-item">
                            <a class="f-s14 hind-font"  onClick="removePartRow(this)" href="#">
                                <span class="img-icon">
                                    <img alt="view-icon" id="remove-qty-{{ $key }}" src="{{url('frontend/assets/images/remove-icon.png')}}"/>
                                </span>
                            </a>
                        </li>
                    </ul>
                </td>
            </tr>
                    </form>
            @endforeach

        </tbody>
    </table>
</div>
</div>
</section>
@stop
<script type="text/javascript">


    function removePartRow(obj){
       console.log( $(".table-bom-product > tbody > tr").length);
        $(obj).parent().closest("tr").remove();
    }
    function alogoForQtySeperationRecFunc(sources, qty,totalAvailableQty, partNo)
    {   //
        var qty = qty;
        var i = 0;
        var sourcesArrayLength = sources.length;

        /* Fot getting lowest price from all the remains source */
        for(i=0;i<sourcesArrayLength;i++){
            if(i<sourcesArrayLength-1){
               if(lowestPrice > sources[i].sourcesPart[0].Prices.resaleList[0].price){
                 lowestPrice =  sources[i].sourcesPart[0].Prices.resaleList[0].price;
                 lowestPriceArrayIndex = i;
             }
         }
     }

     if( lowestPriceArrayIndex < sourcesArrayLength && sources[lowestPriceArrayIndex].sourcesPart[0].Availability.length > 0){
         var availableQty = sources[lowestPriceArrayIndex].sourcesPart[0].Availability[0].fohQty;
         sourcePartId =  sources[lowestPriceArrayIndex].sourcesPart[0].sourcePartId;
         sourcePartId = sourcePartId.replace(":","_");
         console.log("call if", sourcePartId,"qty = ",qty,"availableQty = ",availableQty,"totalAvailableQty = ",totalAvailableQty);
         if(qty > availableQty){
            console.log("set value by if in sourcePartId = ",sourcePartId ,"availableQty =",availableQty);
            $("#quantity_"+sourcePartId).val(availableQty);
            sources.splice(lowestPriceArrayIndex, 1);
            if(qty-availableQty > 0){
                alogoForQtySeperationRecFunc(sources,qty-availableQty,totalAvailableQty, partNo);
            }
        }else{
            console.log("call else");
            if(availableQty < qty){
              console.log("set value by else if in sourcePartId = ",sourcePartId ,"availableQty =",availableQty);
              $("#quantity_"+sourcePartId).val(availableQty);
          }else{
              console.log("set value by else else in sourcePartId = ",sourcePartId ,"qty =",qty);
              $("#quantity_"+sourcePartId).val(qty);
          }
      }

  }
        if(partNo != ''){
            addToCart(partNo)
        }

}

/*var sourcesPart = [ { "packSize1":1, "sourcePartNumber":"", "sourcePartId":"24545557", "dateCode":"", "manufacturerLeadTime":0, "inStock":true, "Availability":[ { "fohQty":4700, "availabilityCd":"INSTK", "availabilityMessage":"In Stock", "pipeline":[ ] } ], "Prices":{ "resaleList":[ { "displayPrice":"0.064", "price":0.064, "minQty":248, "maxQty":499 }, { "displayPrice":"0.0525", "price":0.0525, "minQty":500, "maxQty":999 }, { "displayPrice":"0.043", "price":0.043, "minQty":1000, "maxQty":99999999 } ] } }, { "packSize2":5000, "sourcePartNumber":"", "sourcePartId":"26724317", "dateCode":"1802", "manufacturerLeadTime":0, "inStock":true, "Availability":[ { "fohQty":120000, "availabilityCd":"INSTK", "availabilityMessage":"In Stock", "pipeline":[ ] } ], "Prices":{ "resaleList":[ { "displayPrice":"0.0332", "price":0.0332, "minQty":5000, "maxQty":99999999 } ] } } ];
*/
var lowestPrice = 0;
var lowestPriceArrayIndex = 0;
var sourcePartId = 0;
//Parse input to an Integer
//alogoForQtySeperation(sourcesPart,100);

function alogoForQtySeperation(sources,partId,totalAvailableQty, partNum){
    var inputTxt = $('#quantity_'+partId).val();
    if(inputTxt == ''){
        $('#message_'+partId).text('Please enter quantity.').show();
        return false;
    }else if(parseInt(inputTxt) > parseInt(totalAvailableQty)){
        $('#message_'+partId).text('Entered quantity is more than available quantity.').show();
        return false;
    }else{
        $('#message_'+partId).hide();
    }

    lowestPrice = sources[0].sourcesPart[0].Prices.resaleList[0].price;
    lowestPriceArrayIndex = 0;
    sourcePartId = sources[0].sourcesPart[0].sourcePartId;
    var sourcePartIdForBlank = '';
    for(i=0;i<sources.length;i++){
        for(spi=0;spi<sources[i].sourcesPart.length;spi++){
            //console.log("sources[i].sourcesPart[spi].sourcePartId = ",sources[i].sourcesPart[spi].sourcePartId);
            sourcePartIdForBlank =  sources[i].sourcesPart[spi].sourcePartId;
            sourcePartIdForBlank = sourcePartIdForBlank.replace(":","_");
            $("#quantity_"+sourcePartIdForBlank).val('');
        }
    }

    var qty = $("#quantity_"+partId).val();
    alogoForQtySeperationRecFunc(sources,qty,totalAvailableQty, partNum);
}

function submitAddCard(partNo) {
    var count = 0;
    $('.pop-quantity-input-'+partNo).each(function() {
        var inputQty = $(this).val();
        var avaQty = $(this).attr('data-parsley-max');
        var sourcePartId = $(this).data('source');
        if(parseInt(inputQty) > parseInt(avaQty)){
            count++;
            $('#message_'+sourcePartId).show();
        }else{
            $('#message_'+sourcePartId).hide();
        }
    });

    if(count > 0){
        return false;
    }else{
        addToCart(partNo);
    }
}

function addToCart(partNo){
    console.log("call addToCart function");
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var url = "/add-bom-part-to-cart";
    $.ajax({
        type: "POST",
        url: BASE_URL + url,
        data:$('#bompart_'+partNo).serialize(),
        success: function(data) {
            console.log(data);
            if (data.status) {
                var currentUrl = window.location.href;
                if (currentUrl.indexOf('cart') >= 0) {
                    location.reload();
                } else {
                    window.location.href = "/cart";
                }
            } else {
                alert(data.message);
                       /* if (data.minQtyError) {
                            $("#" + qtyInputId).val(data.minQty);
                        } else if (data.stockError) {
                            $("#" + qtyInputId).val(data.stock);
                        }*/
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
}

</script>

@section('javascript')
    <script>
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        $('.quantity-input').on('change',function(){
            var inputQty = $(this).val();
            var avaQty = $(this).data('quantity');
            if(inputQty == ''){
                $(this).parent().find('.error-max-qty').html('Please enter quantity.<br />').show();
            }else if(parseInt(inputQty) > parseInt(avaQty)){
                $(this).parent().find('.error-max-qty').html('Entered quantity is more than available quantity.<br />').show();
            }else{
                $(this).parent().find('.error-max-qty').hide();
            }
        });

        $('.pop-quantity-input').on('change',function(){
            var inputQty = $(this).val();
            var avaQty = $(this).attr('data-parsley-max');
            var sourcePartId = $(this).data('source');
            if(parseInt(inputQty) > parseInt(avaQty)){
                $('#message_'+sourcePartId).show();
            }else{
                $('#message_'+sourcePartId).hide();
            }
        });



        // $('.checkbox-qty').on('change',function(){
        //     var key = $(this).data('itemid');
        //     if($(this).is(':checked')){
        //         $('#remove-qty-'+key).hide();
        //     }
        //     else
        //     {
        //         $('#remove-qty-'+key).show();
        //     }
        //
        // });

    </script>
@endsection