@extends('frontend.layouts.app')
@section('content')

    <!--START: Products list-->
    <section class="gray-bg">
        <div class="container clearfix">
                <div class="col-md-12">
                    <div id="afs-accordion">
                        <!--Product Listing Content-->
                        <div class="shdow-box product-listing-btm-space">
                            <div class="product-listing-content clearfix p-20">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h2><a href="javascript:void(0)">MPN: {{$finalData->partNum}}</a></h2>
                                        <h3> Manufacturer: {{$finalData->manufacturer}}</h3>
                                        <p class="discription">{{$finalData->desc}}</p>
                                    </div>
                                </div>
                                <!--Product Listing Content-->
                                <a href="javascript:void(0)" class="btn btn-outline m-t9 test" title="View Detail">View Detail</a>
                                <div class="content">
                                    <hr class="custon-sep" />
                                    <!--Franchise Distributor-->
                                    <div class="row franchise-distributor m-t15">
                                        <div class="col-md-12">
                                            <h3 class="block-title"><b>Similar Available Part</b></h3>
                                            @foreach($finalData->sources as $sources)
                                                @foreach($sources->sourcesPart as $sellerInfo)
                                                    @if($sellerInfo->inStock)
                                                        <div class="shdow-box product-listing-btm-space franchise-distributor-content" id="box_<?php echo str_replace(':', '_',$sellerInfo->sourcePartId);?>">
                                                            <div class="row franchise-distributor-header">
                                                                <div class="col-md-6">
                                                                    <div class="sellers-id"><span>Source Part ID <a href="javascript:void(0)" data-toggle="tooltip" title="This ID is for internal use only and does not reflect anything about the MPN."><img src="{{url('frontend/assets/images/question.png')}}" alt="Error" /></a>:</span> {{$sellerInfo->sourcePartId}} </div>
                                                                </div>
                                                            </div>
                                                            <div class="rowd franchise-distributor-body">
                                                                <div class="row">
                                                                    <div class="col-md-9">
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <div class="sellers-detail">
                                                                                    <div class="price">
                                                                                        <span class="black f-s18" id="item_price_div_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId);?>"> {{!empty($sellerInfo->Prices)? "$".number_format($sellerInfo->Prices->resaleList[0]->displayPrice, 3, '.', ',')  :'N/A' }} </span>
                                                                                        <span class="f-s14 text-gray"> @if($sellerInfo->inStock)({{  $sellerInfo->Availability[0]->fohQty}} Available) @endif</span>
                                                                                    </div>
                                                                                    @if(!empty($sellerInfo->Prices))
                                                                                        <div class="ratio f-s14 text-gray"><span>Minimum: {{$sellerInfo->Prices->resaleList[0]->minQty}}</span><span>Increment: 1</span></div>
                                                                                        <input type="hidden" id="source_min_qty_<?php echo str_replace(':', '_', $sellerInfo->sourcePartId);?>" value="{{$sellerInfo->Prices->resaleList[0]->minQty}}">
                                                                                    @endif
                                                                                    <p class="text-gray m-b0 f-s14">Availability Message: <span class="black">{{  $sellerInfo->Availability[0]->availabilityMessage}}</span></p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="warranty-detail f-s14 ">
                                                                                    <p class="text-gray warranty-text">Warranty: <span class="black">Manufacturer</span></p>
                                                                                    <p class="text-gray m-b0">Date Code: <span class="black">{{!empty($sellerInfo->dateCode)?$sellerInfo->dateCode.'+':'' }}</span></p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-md-4">
                                                                                <div class="quantity-div">
                                                                                    <p class="text-gray" id="minInc"></p>
                                                                                    @if(!empty($sellerInfo->Prices))
                                                                                        <div class="quantity-box">
                                                                                            <form id='myform' method='POST' action='#' class="numbo">
                                                                                                <input  class="quantity" id="quantity_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId); ?>" placeholder="Quantity" name='quantity' value='1' class='qty' style="margin-bottom: 0px !important" onkeyup="productListModule.onchangeQtyOfSelectProduct('item_price_div_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId);?>','quantity_<?php echo  str_replace(':','_', $sellerInfo->sourcePartId);?>','<?php echo $sellerInfo->sourcePartId; ?>')" />
                                                                                                <input id="qtyplus" onclick="productListModule.increaseQtyOfSelectProduct('item_price_div_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId);?>','quantity_<?php echo  str_replace(':','_', $sellerInfo->sourcePartId);?>','<?php echo $sellerInfo->sourcePartId; ?>')" type='button' value='+' class='qtyplus ' field='quantity' style="font-weight: bold;" />
                                                                                                <input id="qtyminus" onclick="productListModule.decreaseQtyOfSelectProduct('item_price_div_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId);?>','quantity_<?php echo  str_replace(':','_', $sellerInfo->sourcePartId);?>','<?php echo $sellerInfo->sourcePartId; ?>')" type='button' value='-' class='qtyminus' field='quantity' style="font-weight: bold;" />
                                                                                            </form>
                                                                                        </div>
                                                                                    @else
                                                                                        <div class="quantity-box">
                                                                                            <form id='myform' method='POST' action='#' class="numbo">
                                                                                                <input  class="quantity" id="quantity_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId); ?>" placeholder="Quantity" name='quantity' value='1' class='qty' style="margin-bottom: 0px !important" />
                                                                                                <input id="qtyplus" onclick="productListModule.increaseQtyOfSelectProduct('item_price_div_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId);?>','quantity_<?php echo  str_replace(':','_', $sellerInfo->sourcePartId);?>','<?php echo $sellerInfo->sourcePartId; ?>')" type='button' value='+' class='qtyplus ' field='quantity' style="font-weight: bold;" />
                                                                                                <input id="qtyminus" onclick="productListModule.decreaseQtyOfSelectProduct('item_price_div_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId);?>','quantity_<?php echo  str_replace(':','_', $sellerInfo->sourcePartId);?>','<?php echo $sellerInfo->sourcePartId; ?>')" type='button' value='-' class='qtyminus' field='quantity' style="font-weight: bold;" />
                                                                                            </form>
                                                                                        </div>
                                                                                    @endif
                                                                                    <input  class="bor-radius-none customerPartNo form-control quantity-box" id="customerPartNo_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId); ?>" name='customerPartNo' value='' placeholder="Internal Part#" />
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3 text-right">
                                                                        @if($sellerInfo->inStock)
                                                                            <button type="button" id="addToCart" onclick="accountSettingModule.addSimilarPartToCart('<?php echo $sellerInfo->Availability[0]->fohQty; ?>','quantity_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId); ?>' ,'<?php echo $finalData->partNum; ?>','<?php echo $finalData->manufacturer; ?>','<?php echo $finalData->mfrCd; ?>','<?php echo $sellerInfo->sourcePartId;?>','customerPartNo_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId); ?>','<?php echo $sellerInfo->inStock?>','<?php echo $sellerInfo->dateCode;?>')"  class="btn btn-primary pull-right m-t9" title="Adds items to cart for immediate purchase"><span class="img-icon"><img src="{{url('frontend/assets/images/cart-bag-icon.png')}}" alt="cart-bag-icon" /></span>Add to Cart</button>
                                                                        @endif
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            @if(!empty($sellerInfo->Prices))
                                                                <div class="row franchise-distributor-footer">
                                                                    <div class="col-md-12">
                                                                        @foreach($sellerInfo->Prices->resaleList as $tierPrices)
                                                                            <div class="input-group" onclick="productListModule.selectProductList('item_price_div_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId);?>','<?php echo $sellerInfo->Availability[0]->fohQty; ?>','<?php echo $sellerInfo->inStock; ?>','<?php echo $tierPrices->minQty ?>','<?php echo $tierPrices->price;?>','quantity_<?php echo  str_replace(':', '_', $sellerInfo->sourcePartId); ?>' ,'<?php echo $finalData->partNum; ?>','<?php echo $finalData->manufacturer; ?>','<?php echo $sellerInfo->sourcePartId;?>');" >
                                                                                <div class="input-group-prepend">
                                                                                    <div class="input-group-text qtyHolder_<?php echo $sellerInfo->sourcePartId;?>" id="btnGroupAddon2">+{{$tierPrices->minQty}}</div>
                                                                                </div>
                                                                                <div data-priceHold="{{number_format($tierPrices->displayPrice, 3, '.', ',')}}" data-qtyHold="{{$tierPrices->minQty}}" class="form-control priceHolder_<?php echo str_replace(':', '_', $sellerInfo->sourcePartId);?>">${{number_format($tierPrices->displayPrice, 3, '.', ',')}}</div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </div>
                                                    @endif
                                                @endforeach
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <!--Franchise Distributor-->
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        </div>
    </section>
@stop