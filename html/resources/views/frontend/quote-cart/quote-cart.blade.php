@extends('frontend.layouts.app')
@section('title', '| Quote List')
@section('content')
<style>
.padr0{padding-right:0 !important;}
.table-grid td{vertical-align:top;}
.table-grid td:first-child{text-align:right;padding-right:10px;padding-bottom:6px;}
.price-ml{margin-left:10%;}
.pl-15{padding-left:15px;}
</style>
    <!--Cart Listing Section-->
    <section class="gray-bg shipping-cart">
        <div class="container clearfix">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active"><span>Requests For Quotes</span></li>
                    </ul>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-9">
                    <h2 class="block-title">Requests For Quotes</h2>
                </div>
                @if(count(Cart::instance('quotecart')->content()) > 0)
                    <div class="col-md-3 text-right">
                        {{--<div class="d-inline-block f-s16">--}}
                            {{--<a href="javascript:void(0)" class="f-s16 hind-font"><span class="img-icon"><i class="fa fa-shopping-cart"></i></span>Add to Cart</a>--}}
                        {{--</div>--}}
                        <div class="d-inline-block f-s16">
                            <a  title="Clear All" onclick="quoteModule.emptyQuoteCart()" href="javascript:void(0)" class="f-s16 hind-font"><span class="img-icon"><i class="fa fa-times-circle"></i></span> Clear All</a>
                        </div>
                    </div>
                @endif
            </div>
            <hr/>
            <div class="row">
                <div class="col-md-12">
                    @if(count(Cart::instance('quotecart')->content()) > 0)
                        <div><input class="checked_all" type="checkbox" value="1"> Select All</div>
                    @endif    
                </div>
                <div class="col-md-9">
                    <div class="row cart-heading">
                        @if(count(Cart::instance('quotecart')->content()) > 0)

                            <div class="col-md-3 text-center">
                                <span class="f-s16 font-w500">Product Detail</span>
                            </div>
                            <div class="col-md-3 text-center">
                                <span class="f-s16 font-w500">Price</span>
                            </div>
                            <div class="col-md-2 text-center">
                                <span class="f-s16 font-w500">Quantity</span>
                            </div>
                            <div class="col-md-4 text-center">
                                <span class="f-s16 font-w500">Subtotal</span>
                            </div>
                        @else
                            <div class="col-md-12">
                                <span class="f-s16 font-w500">Quote list is empty</span>
                            </div>
                        @endif
                    </div>
                    <?php // echo "<pre>"; print_r(Cart::instance('quotecart')->content()); echo "</pre>";?>
                    <!--Shiiping Listing Content-->
                    <?php foreach(Cart::instance('quotecart')->content() as $row) :?>
                    <div class="shdow-box ">
                        <div class="shipping-listing-content clearfix">
                            <div class="row">
                                <div class="col-md-12">
                                    <h6 class="pl-15"><a href="javascript:void(0)" id="" class="hind-font"> MPN: {{$row->name}}</a></h6>
                                </div>
                                <div class="col-md-3 product-detail-content padr0">
                                    <table class="table-grid" width="100%">
                                        <tr class="text-gray f-s14" style="display: none;">
                                            <td valign="top">Source Id <a href="javascript:void(0)" data-toggle="tooltip" title="This ID is for internal use only and does not reflect anything about the MPN."><i class="fa fa-question-circle"></i></a>: </td>
                                            <td valign="top" class="black">{{$row->options->sourcePartId}}</td>
                                        </tr>
                                        <tr class="text-gray f-s14">
                                            <td valign="top">Manufacturer: </td>
                                            <td valign="top" class="black"><b class="f-s14">{{$row->options->has('manufacturer') ? $row->options->manufacturer : ''}}</b></td>
                                        </tr>
                                        </tr>
                                        <tr class="text-gray f-s14">
                                            <td>Date Code:</td>
                                            <td class="black">{{$row->options->dateCode?$row->options->dateCode:'N/A'}}</td>
                                        </tr>
                                        <tr class="text-gray f-s14">
                                            <td>Distributor:</td>
                                            <td class="black"><strong>{{$row->options->distributorName?$row->options->distributorName:'N/A'}}</strong></td>
                                        </tr>
                                        <tr class="text-gray f-s14">
                                            <td>Internal Part#:</td>
                                            <td class="black">{{$row->options->customerPartNo?$row->options->customerPartNo:'N/A'}}</td>
                                        </tr>
                                        <tr class="text-gray f-s14">
                                            <td class="black">
                                                <?php 
                                                    if($row->options->quoteType == "not_found"){
                                                        echo '<span class="f-s14 cancelled font-w500">Not Found</span>';
                                                    }else if($row->options->quoteType == "in_stock"){
                                                        echo '<span class="f-s14 success font-w500">In Stock</span>';
                                                    }else if($row->options->quoteType == "out_of_stock"){
                                                        echo '<span class="f-s14 cancelled font-w500">Out of Stock</span>';
                                                    }
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3 product-price">
                                    <table class="table-grid price-ml">
                                        <!-- <tr class="text-gray f-s14">
                                            <td >Actual:</td>
                                            <td class="black">${{number_format($row->price, 3, '.', ',')}}</td>
                                        </tr> -->
                                        <tr class="text-gray f-s14">
                                            <td >Requested: </td>
                                            <td class="black">
                                                @if($row->options->requestedCost == "")
                                                    N/A
                                                @else
                                                  ${{number_format($row->options->requestedCost, 3, '.', ',')}}
                                           @endif
                                            </td>
                                        </tr>
                                    </table>
                                   </div>
                                <div class="col-md-2 product-quantity">
                                    <table class="table-grid">
                                        <!-- <tr class="text-gray f-s14">
                                            <td >Actual:</td>
                                            <td class="black">{{$row->qty}}
                                                <input type="hidden" min="1" max="99999" onblur="quoteModule.changeQtyToQuote('quantity_<?php //echo  str_replace(':', '_', $row->options->sourcePartId); ?>' ,'<?php //echo $row->id; ?>','<?php //echo $row->options->manufacturer; ?>', '<?php //echo $row->options->manufacturerCode; ?>', '<?php //echo $row->options->sourcePartId;?>','<?php //echo $row->options->customerPartNo;?>','<?php //echo $row->options->stockAvailability;?>','resaleList_<?php //echo  str_replace(':', '_', $row->options->sourcePartId); ?>','availableData_<?php //echo  str_replace(':', '_', $row->options->sourcePartId); ?>','<?php// echo $row->options->dateCode; ?>','<?php //echo $row->options->requestedQuantity; ?>','<?php //echo $row->options->requestedCost; ?>','<?php //echo $row->options->requestedDeliveryDate;?>');" id='quantity_<?php //echo  str_replace(':', '_', $row->options->sourcePartId); ?>' name='quantity' value='{{$row->qty}}' class='qty' style="margin-bottom: 0px !important" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                                            </td>
                                        </tr> -->
                                        <tr class="text-gray f-s14">
                                            <td >Requested:</td>
                                            <td class="black">{{$row->options->requestedQuantity}}</td>
                                        </tr>
                                    </table>
                                    <div > <span class="black"></span></div>
                                    <div class="text-gray f-s14"> <span class="black"></span></div>
                                </div>
                                <div class="col-md-4 product-subtotal">
                                    <table class="table-grid">
                                        <!-- <tr class="text-gray f-s14">
                                            <td >Actual:</td>
                                            <td><span class="black">${{number_format($row->price * $row->qty, 3, '.', ',')}}</span></td>
                                        </tr> -->
                                         <tr class="text-gray f-s14">
                                            <td >Requested:</td>
                                            <td class="black">${{number_format($row->options->requestedCost * $row->options->requestedQuantity, 3, '.', ',')}}</td>
                                        </tr>
                                         <tr class="text-gray f-s14">
                                            <td colspan="2" style="text-align:left;">Requested Delivery Date:
 <span class="black">{{$row->options->requestedDeliveryDate?$row->options->requestedDeliveryDate:'N/A'}}</span></td>
                                         </tr>
                                    </table>
                                </div>
                                <input type="hidden" class="bor-radius-none customerPartNo form-control quantity-box" value='<?php echo json_encode($row->options->resaleList)?>' id="resaleList_<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>" name='resaleList'  placeholder="resaleList" />
                                <input type="hidden" class="bor-radius-none customerPartNo form-control quantity-box"  value='<?php echo json_encode($row->options->availableData)?>' id="availableData_<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>" name='availableData'  placeholder="availableData" />
                            </div>
                            <div class="shipping-footer">
                                <div class="row">
                                    <div class="col-md-4 text-left">
                                        <span  class="f-s16 hind-font text-left"><input type="checkbox" required="" name="select_dsitributor[]" id="select_dsitributor" value="{{$row->rowId}}" class="checkbox-qty"/> Select for Quote</span>
                                    </div>
                                    <div class="col-md-8 text-right">
                                        <div class="d-inline-block f-s16">
                                        <!-- <a href="javascript:void(0)" data-toggle="modal" data-target="#edititem" class="f-s16 hind-font"><span class="img-icon"><img src="{{url('frontend/assets/images/edit-icon.png')}}" alt="edit-icon" /></span> Edit</a> -->
                                            <a data-toggle="modal" data-target="#edititem" href="javascript:void(0)" onclick='quoteModule.showEditPopup("<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>",<?php echo json_encode($row); ?>)' class="f-s16 hind-font"><span class="img-icon"><i class="fa fa-edit"></i></span>Edit</a>
                                        </div>
                                        @if($row->options->stockAvailability ==1 )
                                            <div class="d-inline-block f-s16">
                                                <a title="Adds items to cart for immediate purchase" href="javascript:void(0)" onclick="quoteModule.addUnsavedQuoteToCart('<?php echo $row->options->distributorName ?>', '<?php echo $row->options->sourceMinQuantity ?>', 'quantity_<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>' ,'availableData_<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>' ,'<?php echo $row->id; ?>','<?php echo $row->options->manufacturer; ?>','<?php echo $row->options->manufacturerCode; ?>','<?php echo $row->options->sourcePartId;?>','<?php echo $row->options->customerPartNo;?>');" class="f-s16 hind-font"><span class="img-icon"><i class="fa fa-shopping-cart"></i></span>Add to Cart</a>
                                            </div>
                                        @endif
                                        <div class="d-inline-block f-s16">
                                            <a title="Remove" href="javascript:void(0)" class="f-s16 hind-font" onclick="quoteModule.removeQuoteCartItem('<?php echo $row->rowId;?>')"><span class="img-icon"><i class="fa fa-trash"></i></span>Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                <!--Shipping Listing Content-->
                </div>
                @if(Cart::instance('quotecart')->content()->count())
                    <div class="col-md-3">
                        <div class="shdow-box shipping-right-content">
                            <div class="price-content-box">
                                <div class="row price-box-heading">
                                    <div class="col-md-10">
                                        <h2 class="block-title">Quote Summary</h2>
                                    </div>
                                    <div class="col-md-2">
                                        &nbsp;
                                    </div>
                                </div>
                                {!! Form::open(['url' => url('submit-quote'),'method' => 'POST', 'id' => 'quote_form', 'data-parsley-validate' => ""]) !!}
                                <div class="price-content-body">
                                    <ul>
                                        <li class="f-s16 black font-w500 clearfix">
                                            <?php
                                            $finalTotal =  0;
                                            $subTotal=0;
                                            foreach(Cart::instance('quotecart')->content() as $row) {
                                                $subTotal =  floatval($row->price) *$row->qty;
                                                $finalTotal =  $subTotal+$finalTotal;
                                            }?>
                                                <span class="">
                                                      {!! Form::text('quoteName', old('quoteName'), ['class' => 'bor-radius-none form-control quantity-box', 'placeholder' => 'Enter Quote Name','data-parsley-maxlength' => 100, 'required'=>'', 'data-parsley-required-message'=>'Please enter quote name.']) !!}
                                                </span>
                                                <span class="">
                                                {!! Form::textarea('note', null, ['data-parsley-maxlength'=>255,'rows'=>'3' ,'cols'=>'5' ,'id' => 'note','placeholder'=>'Note ( Max. 255 characters are allowed )', 'class' => 'form-control']) !!}
                                              </span>
                                                   <!-- <span class="pull-left m-t10">Sub Total</span>
                                                        <span class="pull-right m-t10">$<?php //echo  number_format($finalTotal, 2, '.', ','); //number_format(Cart::instance('quotecart')->subtotal(), 2, '.', ',') ?></span>-->
                                        </li>
                                    </ul>
                                </div>
                                <div class="price-content-footer">
                                    <input type="hidden" name="hiddenItem" id="hiddenItem" value=""/>
                                    {!! Form::button('Submit Quote', ['class' => 'btn btn-primary', 'id' => 'submit_quote_frm','title'=>'Submit Quote']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                @endif
            </div>
        </div>
    </section>
    <!--Cart Listing Section-->
    <!-- Modal -->
    <div class="modal" id="edititem" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabel">Update Item</h4>
                    <button type="button" class="close" data-dismiss="modal" onclick="cartModule.closeEditItemModel()" aria-label="Close">
                        <span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon" /></span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['class'=>'form-horizontal','method' => 'POST', 'id' => 'edit_quote_detail_form', 'data-parsley-validate' => true]) !!}
                        <div class="price-content-box">
                            <div class="price-content-body">
                                <div class="clearfix m-b5 f-s14">
                                    <div class="text-gray">Source Id: <a href="javascript:void(0)" class="f-s18 hind-font" id="popupItemSourcePartId"></a> </div>
                                </div>
                                <div style="display: none;" class="clearfix m-b5">
                                    <span  class="black f-s18 font-w500 pull-left" id="popupItemPrice"></span>
                                </div>
                                <div style="display: none;" class="clearfix m-b5 f-s14">
                                    <div class="text-gray">Available: <span class="black font-w500" id="popupAvailableItem"></span>  </div>
                                </div>
                                <div style="display: none;" class="clearfix m-b5 f-s14">
                                    <div class="text-gray">Minimum: <span class="black font-w500" id="popupMinimumQty"></span>&nbsp;&nbsp;&nbsp;Increment: <span class="black font-w500">1</span></div>
                                </div>
                                <div class="clearfix m-b5 f-s14"></div>
                                <div style="display: none;" class="quantity">
                                    <div class="quantity-box">
                                        <form id='myform' method='POST' action='#' class="numbo">
                                            <input type='text' data-parsley-errors-container="#quote-qty-error"   data-parsley-required data-parsley-type="digits"  data-parsley-maxlength="10" name='quantity' id="popupQuantity" placeholder="Actual Quantity" value='1' class='qty' style="margin-bottom: 0px !important" />
                                            <input type='button' onclick="cartModule.increaseQtyOfPopupProduct()" value='+' class='qtyplus ' field='quantity' style="font-weight: bold;" />
                                            <input type='button' onclick="cartModule.decreaseQtyOfPopupProduct()" value='-' class='qtyminus' field='quantity' style="font-weight: bold;" />
                                        </form>
                                    </div>
                                </div>
                                <span id="quote-qty-error"></span>

                                <div class="quantity customer-part f-s14 text-gray">
                                        <input type="text"  name="popupRequestedQuantityEdit" data-parsley-required data-parsley-type="digits"  data-parsley-maxlength="10" id="popupRequestedQuantityEdit" placeholder="Requested Quantity"  min="1" class="quantity-box" />
                                </div>
                                <div class="quantity customer-part f-s14 text-gray">
                                        <input type="text"  name="popupRequestedCostEdit"  data-parsley-pattern="^\s*(?=.*[1-9])\d*(?:\.\d{1,6})?\s*$"  data-parsley-required data-parsley-maxlength="20" value="" id="popupRequestedCostEdit" placeholder="Target Price (per unit in $)" class="quantity-box" />
                                </div>
                                <div class="quantity customer-part f-s14 text-gray">
                                        <input type="text" readonly name="popupRequestedDeliveryDateEdit" value=""  class="requested-delivery-date quantity-box" id="popupRequestedDeliveryDateEdit" placeholder="Requested Delivery Date"  />
                                </div>
                                <div class="quantity customer-part f-s14 text-gray">
                                    <input type="text" name="popupCustomerPartNo" value=""  id="popupCustomerPartNo" placeholder="Internal Part#" class="quantity-box" maxlength="30"/>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
                <div class="modal-footer">
                    <button type="submit" data-dismiss="modal" class="btn btn-primary btn-outline" title="Cancel" onclick="cartModule.closeEditItemModel()"><span class="img-icon"><img src="{{url('frontend/assets/images/pop-up-close-icon.png')}}" alt="pop-up-close-icon" /></span>Cancel</button>
                    <input type="hidden" name="hiddenQtyId" id="hiddenQtyId" value="" />
                    <input type="hidden" name="hiddenSourceMinQty" id="hiddenSourceMinQty" value="" />
                    <input type="hidden" name="cartItems" id="cartItems"  value="" />

                    <button type="button" onclick="quoteModule.editItemFromPopup()" class="btn btn-primary" title="Update"><span class="img-icon"><img src="{{url('frontend/assets/images/pop-up-edit-icon.png')}}" alt="pop-up-edit-icon" /></span>Update</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
@stop
@section('javascript')
<script type="text/javascript">
$(document).on('click', '#submit_quote_frm', function(e){
    var isValid = true;
    var hiddenItem = $("#hiddenItem").val();
    if (hiddenItem=='') isValid = false;
    if(isValid){
            $('#quote_form').trigger("submit");
    }else{
            $.notify('Please select atleast 1 item before submit quote.', "error");
    }
});
</script>
@endsection