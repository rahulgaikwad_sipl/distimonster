@extends('frontend.layouts.app')
@section('title', '| Dashboard')
<!-- <link href="{{url('/adminlte/css/AdminLTE.min.css')}}" rel="stylesheet"> -->

@section('content')
    <section class="gray-bg account-custome-view top-pad-with-bradcrumb">
        <div class="container-fluid container-w-80 clearfix user-dashboard user-admin">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active"><span>Dashboard</span></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
                   
                    @include('frontend.partials.sidebar')
                        <div class="tab_container">
                            <h2 class="block-title left-block-title">Dashboard</h2>
                            <div id="tab2" class="tab_content">
                                
                                            <div class="row">
                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="{{ url('/pending-quotes')}}">
                                                        <div class="small-box bg-green">
                                                            <div class="inner">
                                                                <h3>
                                                                    @if(isset($quote) && !empty($quote))
                                                                        {{$quote}}
                                                                    @else
                                                                        0
                                                                    @endif    
                                                                </h3>
                                                                <p>Pending RFQs</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-hourglass-start"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="{{ url('/processed-quotes')}}">
                                                        <div class="small-box bg-darkblue">
                                                            <div class="inner">
                                                                <h3>
                                                                    @if(isset($processedQuote) && !empty($processedQuote))
                                                                        {{$processedQuote}}
                                                                    @else
                                                                        0
                                                                    @endif    
                                                                </h3>
                                                                <p>Processed Quotes</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-newspaper-o"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="{{ url('/ordered-quotes')}}">
                                                        <div class="small-box bg-darkorange">
                                                            <div class="inner">
                                                                <h3>
                                                                    @if(isset($orderedQuote) && !empty($orderedQuote))
                                                                        {{$orderedQuote}}
                                                                    @else
                                                                        0
                                                                    @endif    
                                                                </h3>
                                                                <p>Ordered Quotes</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-file-text-o"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="{{ url('/order-history?tab=1&page=1') }}">
                                                        <div class="small-box bg-yellowgreen">
                                                            <div class="inner">
                                                                <h3>
                                                                    @if(isset($orders_shipped) && !empty($orders_shipped))
                                                                        {{$orders_shipped}}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </h3>
                                                                <p>Shipped Orders</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="{{ url('/order-history?tab=2&page=1') }}">
                                                        <div class="small-box bg-yellow">
                                                            <div class="inner">
                                                                <h3>
                                                                    @if(isset($orders_pending_ship) && !empty($orders_pending_ship))
                                                                        {{$orders_pending_ship}}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </h3>
                                                                <p>In Process Orders</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-clipboard"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="{{ url('/order-history?tab=4&page=1') }}">
                                                        <div class="small-box bg-darkpink">
                                                            <div class="inner">
                                                                <h3>
                                                                    @if(isset($orders_delivered) && !empty($orders_delivered))
                                                                        {{$orders_delivered}}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </h3>
                                                                <p>Delivered Orders</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-shopping-bag"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="javascript:void(0);">
                                                        <div class="small-box bg-purple">
                                                            <div class="inner">
                                                                <h3>
                                                                    @if(isset($working_boms) && !empty($working_boms))
                                                                        {{$working_boms}}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </h3>
                                                                <p>Working Monster-BOM</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-wpforms"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="{{ url('/saved-bom') }}">
                                                        <div class="small-box bg-gray">
                                                            <div class="inner">
                                                                <h3>
                                                                    @if(isset($saved_boms) && !empty($saved_boms))
                                                                        {{$saved_boms}}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </h3>
                                                                <p>Saved Monster-BOM</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-floppy-o"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="{{ url('/shared-bom') }}">
                                                        <div class="small-box bg-brown">
                                                            <div class="inner">
                                                                <h3>
                                                                    @if(isset($shared_boms) && !empty($shared_boms))
                                                                        {{$shared_boms}}
                                                                    @else
                                                                        0
                                                                    @endif
                                                                </h3>
                                                                <p>Shared Monster-BOM</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-share-square-o"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        
                                   
                                
                            </div>
                            <!-- #tab2 End-->
                        </div>
                        <!-- .tab_container -->
                    </div>
                    <div class="shdow-box">
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
@section('javascript')
    <script>
        $('#register_form').parsley();
    </script>
@endsection




