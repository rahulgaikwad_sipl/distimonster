@extends('frontend.layouts.app')
@section('title', '| Payment Error')
@section('content')
    <section class="gray-bg shipping-cart shipping-addres complete-order">
        <div class="container clearfix">
            <div class="row clearfix text-center">
                <div class="col-md-12">
                    {{--<div class="complete-big-icon"><img src="{{url('frontend/assets/images/complete-big-icon.png')}}" alt="complete-big-icon" ></div>--}}
                    <h2 class="block-title left-block-title">There is some error in order processing.Please contact us with below order.</h2>
                </div>
                <div class="col-md-12">
                    <p class="f-s14">Your order number is {{$orderId}}.</p>
                    <a href="{{url('/')}}" class="btn btn-primary m-t15" title="Continue Shopping">Or Continue Shopping</a>
                </div>
            </div>

        </div>
    </section>
@stop