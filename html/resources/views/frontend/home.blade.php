@extends('frontend.layouts.app')
@section('title', '| Home')
@section('content')
@include('frontend.partials.banner')
<!--<section class="news-update">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="block-title">News &amp; Updates <a href="{{url('news-updates')}}" class="view-all">View All</a></h2>
                <div class="news-updates-block">
                    @if(!$news->isEmpty())
                        <ul>
                            @foreach ($news as $newsPost)
                            <li class="clearfix">
                                <div class="news-img">
                                    @if($newsPost->image)
                                    <a href="{{url('news/'.$newsPost->id)}}" title="Image Not Found"><img width="100" src="{{url('/uploads/'.$newsPost->image)}}"/></a>
                                    @else
                                    <a href="{{url('news/'.$newsPost->id)}}" title="Default Image"><img width="100" src="{{url('frontend/images/logo.png')}}"/></a>
                                    @endif
                                </div>
                                <div class="news-content">
                                    <div class="nc-top"><a href="{{url('news/'.$newsPost->id)}}">{{$newsPost->title}}</a><span class="date">{{ date("m/d/Y", strtotime($newsPost->created_at))}}</span></div>
                                    <p>{!!  implode(' ', array_slice(explode(' ', $newsPost->description), 0, config('constants.NEWS_CHARACTERS')))  !!}</p>
                                    @if(str_word_count($newsPost->description) > config('constants.NEWS_CHARACTERS'))
                                       <a href="{{url('news/'.$newsPost->id)}}" class="read-more">Read More</a>
                                    @endif
                                </div>
                            </li>
                            @endforeach
                        </ul>
                        @else
                        <p>No news is published from {{ config('app.name') }}</p>
                    @endif
                </div>
            </div>
            <div class="col-md-6">
                <h2 class="block-title">Product Lines</h2>
                <div class="suppliers-block">
                    <ul class="clearfix">
                        <li><img src="{{url('frontend/assets/images/logo-koa.jpg')}}" alt="Koa" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-microchip.jpg')}}" alt="Microchip" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-nxp.jpg')}}" alt="Nxp" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-epson.jpg')}}" alt="Epson" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-cosel.jpg')}}" alt="Cosel" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-kyocera.jpg')}}" alt="Kyocera" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-sanken.jpg')}}" alt="Sanken" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-jrc.jpg')}}" alt="Jrc" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-ricoh.jpg')}}" alt="Ricoh" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-murata.jpg')}}" alt="Murata" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-cypress.jpg')}}" alt="Cypress" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-nkk.jpg')}}" alt="Nkk" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-bellnix.jpg')}}" alt="Bellnix" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-honeywell.jpg')}}" alt="Honeywell" /></li>
                        <li><img src="{{url('frontend/assets/images/logo-toshiba.jpg')}}" alt="Toshiba" /></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!--@include('frontend.partials.section-special')-->
@stop