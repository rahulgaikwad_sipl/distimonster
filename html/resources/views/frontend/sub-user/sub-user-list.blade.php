@extends('frontend.layouts.app')
@section('title', '| My Team')
@section('content')
<section class="gray-bg account-custome-view top-pad-with-bradcrumb">
    <div class="container-fluid container-w-80 clearfix user-dashboard user-admin">
        <div class="row pagination">
            <div class="col-md-12">
                <ul class="breadcrumb f-s14 text-gray p-l0">
                    <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">My Account</a></li>
                    <li class="breadcrumb-item active"><span>My Team</span></li>
                </ul>
                @if(!empty($subscription_plans) && count($user) <= $subscription_plans->extra_users)
                    <?php $display = (count($user) == $subscription_plans->extra_users) ? 'display:none': '';?>
                    <a style="float:right;<?php echo $display; ?>" class="btn btn-primary btn-outline p-tb5 requestquote-btn-custom" href="{{url('/add-sub-user')}}" titlle="Add Team Members"><span class="img-icon"><img src="{{url('frontend/assets/images/add-quote-icon.png')}}" alt="add-quote-icon" /></span>Add Team Members</a>
                @endif
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="row">
            <div class="col-md-12">
                <div class="tabs_wrapper">
                        @include('frontend.partials.sidebar')
                       <div class="tab_container">
                        <h3 class="tab_drawer_heading" rel="tab2">My Team</h3>
                        <div id="tab2" class="tab_content">
                            <div class="shdow-box edit-account-setting">
                                <div class="edit-account-setting-content edit-custom-setting">
                                    <p>You can add a maximum of two team members within your company here at the cost of $100 a month per member. If you require more than two please contact us for volume priceing.</p>
                                    <div id="czContainer" style="width: 100% !important;">
                                        <div id="first">
                                            
                                            @if(Auth::user())
                                                <table id="subUserListing" class="table  table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>First Name</th>
                                                            <th>Last Name</th>
                                                            <th>Email</th>
                                                            <th>Note</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($user AS $subUser)
                                                        <tr class="shdow-box">
                                                            <td>{{$subUser->name}}</td>
                                                            <td>{{$subUser->last_name}}</td>
                                                            <td>{{$subUser->email}}</td>
                                                            <td>@if(isset($subUser->note) && !empty($subUser->note))
                                                                    {{
                                                                        $subUser->note
                                                                    }}
                                                                @else
                                                                    N/A
                                                                @endif
                                                            </td>
                                                        </tr>
                                                     @endforeach      
                                                    </tbody>
                                                </table>
                                                @endif
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- #tab2 End-->
                    </div>
                    <!-- .tab_container -->
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('javascript')
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#subUserListing').DataTable();
    });
</script>
@endsection