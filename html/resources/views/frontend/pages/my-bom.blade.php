@extends('frontend.layouts.app')
@section('title', '| '.$title)
@section('content')
    <section class="gray-bg top-pad-with-bradcrumb">
        <div class="container-fluid clearfix container-w-80 user-admin">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active"><span>{{$title}}</span></li>

                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
 @include('frontend.partials.sidebar')
                                          
					   <div class="tab_container">
                            <h3 class="tab_drawer_heading" rel="tab5">Tab 5</h3>
                            <div id="tab5" class="tab_content">
                                <h2 class="block-title left-block-title">{{$title}}</h2>
                                @if(count($bomList)>0)
                                    @foreach($bomList->chunk(2) as $items)
                                        <div class="row ">
                                            @foreach($items as $item)
                                                <div class="col-md-6">
                                                    <div class="shdow-box my-quote-detail">
                                                        <h3 class="f-s18"><a href="{{url($item->id.'/bom-details')}}" onclick="show_loader()">
                                                        @if(!empty($item->bom_name))    
                                                            {{$item->bom_name}}
                                                        @else
                                                            BOM - {{$item->id}}
                                                        @endif
                                                        </a></h3>
                                                        <div class="f-s14 text-gray btm-space">Total Parts: <span class="black font-w500">{{$item->bom_parts_count}}</span></div>
                                                        <div class="f-s14 text-gray btm-space">Last modified: <span class="black font-w500">{{ date('m/d/Y', strtotime($item->updated_at)) }}</span></div>
                                                        <div class="my-quote-detail-footer ">
                                                            <ul class="list-inline m-b0">
                                                                <li class="list-inline-item">
                                                                    <a href="{{url('download-bom/'.$item->id)}}" class="f-s16 font-w500 hind-font"><span class="img-icon"><i class="fa fa-download"></i></span>Download</a>
                                                                </li>
                                                                <li class="list-inline-item">
                                                                    <a href="javascript:void(0)" onclick="bom.removeBomItem('<?php  echo $item->id;?>')" class="f-s16 font-w500 hind-font"><span class="img-icon"><i class="fa fa-trash"></i></span>Remove</a>
                                                                </li>
                                                                {{--@if(!empty($subUser) && count($subUser)>0)--}}
                                                                <li class="list-inline-item" style="float: right;">
                                                                    <a href="#" data-target=".share_modal_{{$item->id}}" data-toggle="modal" type="button"><i class="fa fa-share"></i> Share</a>
                                                                </li>
                                                                {{--@endif--}}
                                                                <li class="list-inline-item" style="float: right; padding: 0px 7px;">
                                                                    <a href="javascript:void(0)" bom-id="{{$item->id}}" class="notes_modal" type="button"><i class="fa fa-file-text"></i> Notes   @if(!empty($item->unread)) <span class="badge badge-notify">{{$item->unread}}</span>@endif</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--@if(!empty($subUser) && count($subUser)>0)--}}
                                                            <div aria-labelledby="modalLabel" class="modal share_modal_{{$item->id}}" role="dialog" tabindex="-1" style="z-index: 1050;">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <div class="modal-title">
                                                                                <h4 class="m-b0">Share BOM</h4>
                                                                            </div>
                                                                            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon"></span></button>
                                                                        </div>
                                                                        <form method="POST" name="bom_share_frm" class="share_frm_{{$item->id}}" id="bom_share_frm" data-parsley-validate="">
                                                                        <div class="modal-body">
                                                                            <div class="row p-b5">
                                                                                <div class="col-md-12">
                                                                                    <div class="my-bom-checklist">
                                                                                <?php 
                                                                                $shared_exploded_user = 0;
                                                                                if(!empty($item->shared_user_id)){
                                                                                    $shared_exploded_user = explode(',',$item->shared_user_id);
                                                                                }
                                                                                $i=0;
                                                                                ?>
                                                                                 <div class="row">
                                                                                @foreach($subUser as $sub)
                                                                               
                                                                                    <div class="form-check form-check-inline col-md-5">
                                                                                            <?php $check = ''; ?>
                                                                                            @if(is_array($shared_exploded_user) && in_array($sub->id, $shared_exploded_user)) 
                                                                                            <?php $check = 'checked="checked"'; ?>
                                                                                            @endif
                                                                                            <input type="checkbox" required="" name="user_name[]" class="form-check-input" id="user_name" {{$check}} value="{{$sub->id}}" data-parsley-required-message="Please select atleast one user to share BOM."/><label class="form-check-label" for="inlineCheckbox1">{{$sub->name}} {{$sub->last_name}}</label>
                                                                                    </div>
                                                                               
                                                                                    <input type="hidden" name="name_hidden[]" value="{{$sub->name}} {{$sub->last_name}}"/>
                                                                                    <input type="hidden" name="email_hidden[]" value="{{$sub->email}}"/>
                                                                                <?php $i++;?>
                                                                                
                                                                                @endforeach
                                                                                </div>
                                                                                </div>
                                                                                <input type="hidden" name="bom_id_hidden" value="{{$item->id}}"/>
                                                                        </div>
                                                                        <div class="col-md-12"><textarea name="bom_note" maxlength="500" class="bom_note form-control bdr-radius100"></textarea></div>
                                                                        </div>
                                                                        
                                                                        </div>
                                                                        
                                                                        <div class="modal-footer p-t0">
                                                                            <button type="button" onClick="shareBOM('share_frm_{{$item->id}}')" class="btn btn-primary">Share Now</button>
                                                                        </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                {{--@endif--}}
                                            @endforeach
                                        </div>
                                    @endforeach
                                    <div aria-labelledby="modalLabel" id="notes_modals" class="modal" role="dialog" tabindex="-1" style="z-index: 1050;">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <div class="modal-title">
                                                        <h4 class="m-b0">Share History Notes</h4>
                                                    </div>
                                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon"></span></button>
                                                </div>
                                                <div class="modal-body">
                                                <div id="contents">                                     
                                                </div>
                                            </div>
                                            <div class="modal-footer p-t0"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div aria-labelledby="myLargeModalLabel" id="notes_modal" class="modal fade" role="dialog" tabindex="-1">
                                            <div class="modal-dialog modal-lg modal-custom" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bdr-none">
                                                        <div class="modal-title">
                                                            <h4>Share History Notes</h4>
                                                        </div>
                                                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon"></span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <hr>
                                                        <div id="content">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <div class="pagination-list text-right clearfix">
                                        {{ $bomList->appends(request()->except('page'))->links('frontend.partials.pagination') }}
                                    </div>
                                @else
                                    <div class="alert alert-info">
                                        {{$noBomMsg}}
                                    </div>
                                @endif
                            </div>

                        </div>
                        <!-- .tab_container -->
                    </div>
                    <div class="shdow-box">
                    </div>
                </div>
            </div>
        </div>
    </section>
	 <style>
	/* CSS used here will be applied after bootstrap.css */
.badge-notify{
   background:red;
   position:relative;
 }
  </style>
@stop
@section('javascript')
    <script>
        function show_loader() {
            $(".loader").css("display", 'block');
            $("#loader_note_message").css("display", 'block');
        }
    </script>
@endsection