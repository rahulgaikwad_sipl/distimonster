@extends('frontend.layouts.app')
<?php
 if($quoteInfo[0]->is_unnamed){
     $quoteName  = 'Unnamed';
     }else{
     $quoteName  = $quoteInfo[0]->quote_name;
     }

?>
    @section('title', '| Quote Details'.' - '.$quoteName)

@section('content')
    <!--MAIN SECTION START:-->
    <section class="gray-bg shipping-cart">
        <div class="container clearfix">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('quotes')}}">Quotes</a></li>
                        <li class="breadcrumb-item active"><span>Quote Detail</span></li>
                    </ul>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-7">
                    <h2 class="block-title">
                        @if($quoteInfo[0]->is_unnamed)
                            Unnamed
                        @else
                            {{$quoteInfo[0]->quote_name?$quoteInfo[0]->quote_name:"Quote-".$quoteInfo[0]->id}}
                        @endif
                        <span class="text-gray font-w-normal f-s14 m-l10">{{ date('l, m/d/Y', strtotime($quoteInfo[0]->created_at)) }}</span>
                    </h2>
                </div>
                <div class="col-md-5 text-right quote-detail-right-icon">
                    <ul class="list-inline m-b0">
                        <li class="list-inline-item"><a href="{{url('download-quote/'.$quoteInfo[0]->id)}}" class="f-s16 font-w500 hind-font"><span class="img-icon"><i class="fa fa-download"></i></span> Download</a></li>
                        <li class="list-inline-item" id="removeButton"><a href="javascript:void(0)" onclick="quoteModule.removeQuote('<?php echo $quoteInfo[0]->id?>',0)"  class="f-s16 font-w500 hind-font"><span class="img-icon"><i class="fa fa-trash"></i></span>Remove</a></li>
                    </ul>
                </div>
            </div>
            <hr class="m-b0" />
            <div class="row">
                <div class="col-md-12">
                    <?php if(count($quoteItems ) > 1 && $quoteInfo[0]->is_approved == 1){ ?>
                        <button type="button" class="btn btn-primary pull-right m-t5 addToCartAll" title="Adds items to cart for immediate purchase"><span class="img-icon"><i class="fa fa-shopping-cart"></i></span>Add to Cart</button>
                        <?php } ?>
                    <div class="f-s18 text-gray top-space total-parts">
                        Total Parts: <span class="black font-w500">{{count($quoteItems)}}</span>
                    </div>
                    <hr class="m-b0">
                </div>
                 <div class="col-md-12">
                     <table class="table table-responsive table-w100">
                    <tbody><tr class="f-s16 font-w500 black">
                    @if($quoteInfo[0]->is_approved ==1) 
                    <th class="v-align text-center"><div class="select-all">Select All<br><input class="checked_all" type="checkbox" value="1" /></div></th>
                    @endif
                    <th width="300">Product Detail</th>
                    <th class="text-center">Quantity</th>
                    <th class="text-center">Price</th>
                    <th class="text-center">Delivery Date</th>
                    <th class="text-center">Total</th>
                   <th width="70">Action</th>
                </tr>
            </tbody>
        
            
                    <!--quoteItems Listing Content-->
                    <?php
                        $quoteSubTotal= 0;
                        foreach($quoteItems as $item){
                    ?>
                    <tr class="shdow-box f-s14 text-gray">
                        @if($quoteInfo[0]->is_approved ==1)        
                    <td  class="text-center v-align">
                            <?php $itemExistCheck = Cart::instance('shoppingcart')->content()->where('id', $item->item_name)->where('options.sourcePartId', $item->source_part_id)->where('options.isQuoteApprovedItem', 1); ?>
                                  <?php if($quoteInfo[0]->is_approved == 2 || $quoteInfo[0]->is_approved == 3 || $quoteInfo[0]->is_approved == 4 || ($quoteInfo[0]->is_approved == 1 && !empty(json_decode($itemExistCheck))) || count($quoteItems ) == 1){ ?>
                                  <?php }else{ ?>
                                    @if(!$item->is_ordered)
                                    <div>Select Item</div>
                                <input type="checkbox" class="checkbox-qty" id="checkbox_{{$item->item_name}}" name="selectedProducts[]" value="{{$item->item_name}}" data-itemName="{{$item->item_name}}" data-sourcePartId="{{$item->source_part_id}}" data-manufacturerName="{{$item->manufacturer_name}}" data-manufacturerCode="{{$item->manufacturerCode}}" data-customerPartId="{{$item->customer_part_id}}" data-dateCode="{{$item->date_code}}" data-isApproved="{{$item->is_approved}}" data-approvedPrice="{{$item->approved_price}}" data-approvedQuantity="{{$item->approved_quantity}}" data-isQuoted="{{$item->is_quoted}}" data-id="{{$item->id}}" data-quoteInfoId="{{$quoteInfo[0]->id}}" data-quantity="{{$item->quantity}}" data-method="<?php if($quoteInfo[0]->is_approved ==1 && !$item->is_ordered && empty(json_decode($itemExistCheck))) echo 'addApprovedQuoteItemToCartFromQuote'; else if($quoteInfo[0]->is_approved ==0) echo 'addToCartFromQuote'; else if(!empty(json_decode($itemExistCheck))) echo 'noMethod'; ?>" data-message="<?php if(!empty(json_decode($itemExistCheck))) echo 'Already Added to Cart'; ?>" data-distributorName="<?php echo $item->distributor_name?strtolower($item->distributor_name):'not_available'; ?>"/>
                                @endif
                            <?php } ?>
                            </td>
                            @endif
                                <td>
                                    <table class="table-grid">
                                        <tr><td colspan="2" class="text-left"><h2 class="f-s18"><a href="javascript:void(0)">MPN: {{$item->item_name}}</a></h2></td></tr>
                                        <tr class="f-s14 text-gray" style="display: none;">
                                            <td>Source Id:</td>
                                            <td class="black font-w500">{{$item->source_part_id}}</td>
                                        </tr>
                                        <tr class="f-s14 text-gray">
                                            <td>Manufacturer:</td>
                                            <td class="black font-w500"> {{$item->manufacturer_name}}</td>
                                        </tr>
                                        <tr class="f-s14 text-gray">
                                            <td>Distributor:</td>
                                            <td class="black font-w500">{{$item->distributor_name?$item->distributor_name:'N/A'}}</td>
                                        </tr>
                                        <tr>
                                            <td>Internal Part#: </td>
                                            <td class="black font-w500">{{$item->customer_part_id?$item->customer_part_id:'N/A'}}</td>
                                        </tr>
                                    </table>
                                    
                        </td>
                                <td>
                                    <table class="table-grid">
                                        <tr class="f-s14 text-gray">
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <!-- <tr class="f-s14 text-gray">
                                            <td>Actual:</td>
                                            <td class="black font-w500">{{$item->quantity}}</td>
                                        </tr> -->
                                        <tr class="f-s14 text-gray">
                                            <td>Requested:</td>
                                            <td class="black font-w500">{{$item->requested_quantity}}</td>
                                        </tr>
                                        <tr class="f-s14 text-gray">
                                            @if($item->is_approved)
                                            <td>Approved:</td>
                                            <td class="black font-w500">{{$item->approved_quantity}}</td>
                                               @endif
                                        </tr>
                                    </table>
                                  </td>

                                <td>
                                    <table class="table-grid">
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <!-- <tr class="f-s14 text-gray">
                                            <td>Actual:</td>
                                            <td class="black font-w500">${{number_format($item->price, 3, '.', ',')}}</td>
                                        </tr> -->
                                        <tr class="f-s14 text-gray">
                                            <td>Requested:</td>
                                            <td class="black font-w500">${{number_format($item->requested_cost, 3, '.', ',')}}</td>
                                        </tr>
                                        <tr class="f-s14 text-gray">
                                             @if($item->is_approved)
                                            <td>Approved: </td>
                                            <td class="black font-w500">${{number_format($item->approved_price, 3, '.', ',')}}</td>
                                             @endif
                                        </tr>
                                    </table>
                                   </td>
                                <td>
                                    <table class="table-grid">
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <!-- <tr>
                                            <td>Actual:</td>
                                            <td class="black font-w500">N/A</td>
                                        </tr> -->
                                        <tr>
                                            <td>Requested:</td>
                                            <td class="black font-w500">{{ $item->requested_delivery_date ? date('m/d/Y', strtotime($item->requested_delivery_date)) :'N/A'}}</td>
                                        </tr>
                                        <tr>
                                            @if($item->is_approved)
                                            <td>Approved:</td>
                                            <td class="black font-w500">{{$item->approved_delivery_date ? date('m/d/Y', strtotime($item->approved_delivery_date)) :'N/A'}}</td>
                                            @endif
                                        </tr>
                                    </table>
                                 </td>
                                <td>
                                    <table class="table-grid">
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                        <!-- <tr>
                                            <td>Actual:</td>
                                            <td class="black font-w500">$<?php  //$actPrice= $item->price*$item->quantity; echo number_format($actPrice, 3, '.', ',') ?> </td>
                                        </tr> -->
                                        <tr>
                                            <td>Requested:</td>
                                            <td class="black font-w500">$<?php $reqTotal= $item->requested_cost*$item->requested_quantity;?>{{number_format($reqTotal, 3, '.', ',')}}</td>
                                        </tr>
                                        <tr>
                                             @if($item->is_approved)
                                            <td>Approved:</td>
                                            <td class="black font-w500">$<?php $appTotal=$item->approved_price*$item->approved_quantity;?>{{number_format($appTotal,3, '.', ',')}}</td>
                                             @endif
                                        </tr>
                                    </table>
                                </td>
                                    @if($quoteInfo[0]->is_approved ==1)
                                        @if($item->is_ordered)
                                                <td>
                                                    <table class="table-grid">
                                                        <tr><td colspan="2">&nbsp;</td></tr>
                                                        <tr><td><span class="already-added">This is already ordered</span></td></tr>
                                                    </table>
                                                </td>
                                        @else
                                            <?php $itemExist =   Cart::instance('shoppingcart')->content()->where('id', $item->item_name)->where('options.sourcePartId', $item->source_part_id)->where('options.isQuoteApprovedItem', 1); ?>
                                                @if(empty(json_decode($itemExist)))

                                                    <td><button type="button" onclick="accountSettingModule.addApprovedQuoteItemToCartFromQuote('<?php echo $item->item_name; ?>','<?php echo $item->source_part_id; ?>','<?php echo $item->manufacturer_name; ?>', '<?php echo $item->manufacturerCode; ?>', '<?php echo $item->customer_part_id; ?>','<?php echo $item->date_code?>','<?php echo $item->is_approved?>','<?php echo $item->approved_price?>','<?php echo $item->approved_quantity;?>','<?php echo  $item->is_quoted;?>','<?php echo  $item->id;?>','<?php echo $quoteInfo[0]->id?>')" id="addToCart" class="btn btn-primary pull-right m-t9" title="Adds items to cart for immediate purchase"><span class="img-icon"><i class="fa fa-shopping-cart"></i></span>Add to Cart</button></td>
                                                @else
                                                    <td><button type="button"  class="btn btn-success pull-right m-t9" title="Added to Cart">Added to Cart</button></td>
                                                @endif
                                        @endif
                                    @elseif($quoteInfo[0]->is_approved == 1)
                                        <td><button type="button" onclick="accountSettingModule.addToCartFromQuote('<?php echo $item->item_name;?>','<?php echo $item->source_part_id;?>','<?php echo $item->quantity;?>','<?php echo $item->manufacturer_name;?>', '<?php echo $item->manufacturerCode;?>', '<?php echo $item->customer_part_id;?>', '<?php echo $item->distributor_name?strtolower($item->distributor_name):'not_available'; ?>')" id="addToCart" class="btn btn-primary pull-right m-t9" title="Adds items to cart for immediate purchase"><span class="img-icon"><i class="fa fa-shopping-cart"></i></span>Add to Cart</button></td>
                                    @elseif($quoteInfo[0]->is_approved == 2)
                                        <td class="action"><span class="img-icon rejected"><img src="{{url('frontend/assets/images/rejected.png')}}" alt="rejected" /></span><span class="cancelled">Rejected</span></td>
                                    @elseif($quoteInfo[0]->is_approved == 3)
                                        <td><span class="expiry">Expired</span></td>
                                    @elseif($quoteInfo[0]->is_approved == 4)
                                        <td>
                                            <table class="table-grid">
                                                <tr><td colspan="2">&nbsp;</td></tr>
                                                <tr><td><span class="already-added">This is already ordered</span></td></tr>
                                            </table>
                                        </td>
                                    @endif
                            
                    </tr>
                    <tr class="blank-space p-0">
                        <td colspan="6" class="p-0">&nbsp;</td>
                    </tr>
                    <?php
                         $quoteSubTotal = $quoteSubTotal+ ($item->quantity * $item->price);
                        }
                    ?>
                    <!--quoteItems Listing Content-->
                    <tr>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <?php if(count($quoteItems ) > 1 && $quoteInfo[0]->is_approved == 1){ ?>
                        <td> <button type="button" class="btn btn-primary pull-right m-t9 addToCartAll" title="Adds items to cart for immediate purchase"><span class="img-icon"><i class="fa fa-shopping-cart"></i></span>Add to Cart</button></td>
                        <?php } ?>
                    </tr>
               
            </div>
            </table>
                 </div>

        </div>
        {!! Form::open(['url' => url('similar-parts'), 'class'=>'form-horizontal','method' => 'GET', 'id' => 'similar_parts_form']) !!}
        {!! Form::hidden('partNumber', null, ['id' => 'partNumber']) !!}
        {!! Form::hidden('manufacturerName', null, ['id' => 'manufacturerName']) !!}
        {!! Form::hidden('sourcePartId', null, ['id' => 'sourcePartId']) !!}
        {!! Form::hidden('similar_parts', null, ['id' => 'similar_parts']) !!}
        {!! Form::close() !!}

        <div class="modal" role="dialog"  id="commonModal"  tabindex="-1"  aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="commonModalLabel" ></h4>
                        <button type="button" class="close" data-dismiss="modal" onclick="accountSettingModule.closeCommonModel()" aria-label="Close">
                            <span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon" /></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="error-infor" id="commonModalMessage">
                        </div>
                        <br/>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" onclick="accountSettingModule.closeCommonModel()" type="submit" class="btn btn-primary btn-outline" title="Cancel" ><span class="img-icon"><img src="{{url('frontend/assets/images/pop-up-close-icon.png')}}" alt="pop-up-close-icon" /></span>Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="qtyFoundModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modalLabel">Success</h4>
                        <button type="button" class="close" data-dismiss="modal" onclick="accountSettingModule.closeQtyFoundModel()" aria-label="Close">
                            <span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon" /></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="error-infor" id="qtyFoundMessage">
                        </div>
                        <br/>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" onclick="accountSettingModule.closeQtyFoundModel()"  type="submit" class="btn btn-primary btn-outline" title="Cancel" ><span class="img-icon"><img src="{{url('frontend/assets/images/pop-up-close-icon.png')}}" alt="pop-up-close-icon" /></span>Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- No Qty Found  Modal -->
        <div class="modal" id="otherSourceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modalLabel">Show Similar Parts</h4>
                        <button type="button" class="close" data-dismiss="modal" onclick="accountSettingModule.closeOtherSourceModal()" aria-label="Close">
                            <span aria-hidden="true"><img src="{{url('frontend/assets/images/close-icon.png')}}" alt="close-icon" /></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="error-infor" id="otherSourceMessage">
                        </div>
                        <div class="error-infor">
                            <a href="javascript:void(0)" onclick="accountSettingModule.showSimilarParts()" >Click Here</a>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" onclick="accountSettingModule.closeOtherSourceModal()"  type="submit" class="btn btn-primary btn-outline" title="Cancel" ><span class="img-icon"><img src="{{url('frontend/assets/images/pop-up-close-icon.png')}}" alt="pop-up-close-icon" /></span>Cancel</button>
                    </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--SECTION END:-->

@stop
