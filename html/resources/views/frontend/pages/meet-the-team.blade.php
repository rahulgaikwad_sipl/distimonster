@extends('frontend.layouts.app')
@section('title', '| Meet the team')
@section('content')

<!--START: Cart Page-->
<section class="gray-bg top-pad-with-bradcrumb">
    <div class="container clearfix">
        <div class="row pagination">
            <div class="col-md-12">
                <ul class="breadcrumb f-s14 text-gray p-l0">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active"><span>Meet The Team</span></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="tabs_wrapper faq-block">
                    @include('frontend.partials.about')
                    <div class="tab_container" style="min-height: 191px;">
                        <a href="{{url('faq')}}">
                            <h3 class="tab_drawer_heading" rel="tab1">FAQs</h3>
                        </a>
                        <!-- #tab1 End-->
                        <h3 class="tab_drawer_heading" rel="tab2">Meet The Team</h3>
                        <div id="tab2" class="tab_content" style="display: none;">
                            <h2 class="block-title left-block-title">Meet The Team</h2>
                            <div class="shdow-box edit-account-setting">
                                <div class="edit-account-setting-content f-s14 text-gray">
                                    <ul class="edit-user-content meet-the-team-list">
                                        <li>
                                            <div class="media">
                                                <img src="{{url('frontend/images/allison-levine-pic.jpeg')}}"
                                                    class="team-pics" alt="">
                                                <div class="media-body pl-3">
                                                    <b>Allison Levine: CEO &amp; Founder </b> <a target="_blank"
                                                        href="{{url('https://www.linkedin.com/in/allisonlevine/')}}">
                                                        <img src="{{url('frontend/images/iconfinder_linkedin_294671.png') }}"
                                                            class="img-responsive">
                                                    </a>
                                                    <span class="year-content">
                                                        30+ years in electronic component distribution; Founder & CEO of a mid-tier distributor.  Started from ground zero and created a team to grow into a multimillion-dollar organization. Strong entrepreneurial experience and passion for success. 
                                                    </span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img src="{{url('frontend/images/pat-larocca.jpg')}}"
                                                    class="team-pics" alt="">
                                                <div class="media-body pl-3">
                                                    <b>Pat LaRocca: President & Co-founder </b> <a target="_blank"
                                                        href="{{url('https://www.linkedin.com/in/pat-larocca-3861953/')}}">
                                                        <img src="{{url('frontend/images/iconfinder_linkedin_294671.png') }}"
                                                            class="img-responsive">
                                                    </a>
                                                    <span class="year-content">
                                                        25+ years in distribution; executive experience at Arrow and Avnet. Digital innovator, leading teams that transformed the component supply chain industry.  Passion for networking and relationship building that enabled repeated digital success.
                                                    </span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img src="{{url('frontend/images/tony-harris-pic.jpg')}}"
                                                    class="team-pics" alt="">
                                                <div class="media-body pl-3">
                                                    <b>Tony Harris: Chief Strategy Officer </b> <a target="_blank"
                                                        href="{{url('https://www.linkedin.com/in/tony-harris-098a383/')}}">
                                                        <img src="{{url('frontend/images/iconfinder_linkedin_294671.png') }}"
                                                            class="img-responsive">
                                                    </a>
                                                    <span class="year-content">
                                                        Thought leader and subject matter expert on digital marketing and eCommerce, experiential marketing, trademark/intellectual property matters, strategic planning and corporate/crisis communications.  Digi-Key innovator.
                                                    </span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img src="{{url('frontend/images/uwe-knor-pic.jpg')}}"
                                                    class="team-pics" alt="">
                                                <div class="media-body pl-3">
                                                    <b>Uwe Knor: Advisor </b> <a target="_blank"
                                                        href="{{url('https://www.linkedin.com/in/uweknorr/')}}">
                                                        <img src="{{url('frontend/images/iconfinder_linkedin_294671.png') }}"
                                                            class="img-responsive">
                                                    </a>
                                                    <span class="year-content">
                                                        Extensive technology background in electronics, semiconductors, EDA, CAD, CAM. Experienced in international business development, marketing, sales and general management from startup to publicly traded companies. PhD in electronics.
                                                    </span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img src="{{url('frontend/images/jim-hogan-pic.jpg')}}"
                                                    class="team-pics" alt="">
                                                <div class="media-body pl-3">
                                                    <b>James H. Hogan: Advisor </b>
                                                    <a target="_blank"
                                                        href="{{url('https://www.linkedin.com/in/jameshhogan/')}}">
                                                        <img src="{{url('frontend/images/iconfinder_linkedin_294671.png') }}"
                                                            class="img-responsive">
                                                    </a>
                                                    <span class="year-content">
                                                        40+ years in semiconductor design and manufacturing; managing partner/Vista Ventures.
Executive and board director in EDA, intellectual property, semiconductor equipment, material science and IT companies.
</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img src="{{url('frontend/images/sean-okane-pic.jpg')}}"
                                                    class="team-pics" alt="">
                                                <div class="media-body pl-3">
                                                    <b>Sean O’Kane: Advisor</b>
                                                    <a target="_blank"
                                                        href="{{url('https://www.linkedin.com/in/sean-o-kane-a45b35/')}}">
                                                        <img src="{{url('frontend/images/iconfinder_linkedin_294671.png') }}"
                                                            class="img-responsive">
                                                    </a>
                                                    <span class="year-content">
                                                       Executive at Cadence; executive producer/host of ChipEstimate.TV, a high tech online TV show. Experienced executive director with a demonstrated history of working in the computer software industry. </span>
                                                </div>
                                            </div>
                                        </li>
                                </div>
                            </div>
                        </div>
                        <!-- #tab2 End-->
                        <a href="{{url('contact-us')}}">
                            <h3 class="tab_drawer_heading" rel="tab1">Contact Us</h3>
                        </a>

                        <!-- #tab3 End-->
                    </div>
                    <!-- .tab_container -->
                </div>
                <div class="shdow-box">
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('javascript')
<script>
$('#chat_link').click(function() {
    MyLiveChat.InlineChatDoExpand();
    //$('.mylivechat_sprite').trigger('click');
});
</script>
@endsection