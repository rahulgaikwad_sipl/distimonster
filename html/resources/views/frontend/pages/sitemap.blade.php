@extends('frontend.layouts.app')
@section('title', '| Site Map')
@section('content')

    <!--START: Sitemap Page-->
    <section class="gray-bg shipping-cart sitemap">
         <div class="container clearfix ">
            <div class="row pagination">
               <div class="col-md-12">
                  <ul class="breadcrumb f-s14 text-gray p-l0">
                     <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                     <li class="breadcrumb-item active"><span>Site Map</span></li>
                  </ul>
               </div>
            </div>
            <!-- <div class="row clearfix">
               <div class="col-md-7">
                  <h2 class="block-title">Site Map</h2>
               </div>
            </div> -->
            
            <div class="row sitemap-content">
               <div class="col-md-4">
                  <h4 class="f-s18 black font-w500">Main Menu</h4>
                  <hr />
                  <ul>
                     {{--<li><a href="javascript:void(0)"><h6>All Manufacturers</h6></a></li>--}}
                     <li><a href="{{url('/about')}}"><h6>About</h6></a></li>
                     <li><a href="{{url('/news-updates')}}"><h6>News & Updates</h6></a></li>
                     <li><h6>Support</h6>
                        <ul>
                           <li><a href="{{url('/faq')}}">FAQs</a></li>
                           <li><a href="{{url('/return-policy')}}">Return Policy</a></li>
                           <li><a href="{{url('/contact')}}">Contact</a></li>
                        </ul>
                     </li>
                     <li><a href="{{url('/bom')}}"><h6>Monster-BOM</h6></a></li>
                      @if(Auth::user())
                       <li><a href=href="javascript:void(0)"><h6>My Account</h6></a>
                        <ul>
                            <li><a href="{{url('account-settings/'.Auth::user()->id)}}">Account Setttings</a></li>
                           <li><a href="{{url('/order-history')}}">Order History</a></li>
                           <li><a href="{{url('/quotes')}}">Quotes</a></li>
                           <li><a href="{{url('/my-bom')}}">MonsterBOM</a></li>
                           <li><a href="{{url('/my-bio')}}">My Bio</a></li>
                           @if(Auth::user()->is_sub_user == 0)
                              <li><a href="{{url('/my-team')}}">My Team</a></li>
                            @endif  
                           <li><a href="{{url('/requested-lead-times')}}">Requested Lead Times</a></li>
                           <!-- <li><a href="{{url('/apply-net-term-account')}}">Apply Net Term Account</a></li> -->
                        </ul>
                           @endif
                     </li>
                     <li><a href="{{url('/terms-of-use')}}"><h6>Privacy Policy and Terms of Use</h6></a></li>
                  </ul>
  
               </div>
               {{--<div class="col-md-4">--}}
                  {{--<h4 class="f-s18 black font-w500">Categories</h4>--}}
                  {{--<hr  />--}}
                   {{--<ul class="pull-left categories-left">--}}
                     {{--<li><a href="javascript:void(0)">Category 1</a>--}}
                        {{--<ul>--}}
                           {{--<li><a href="javascript:void(0)">Sub Category 1</a>--}}
                              {{--<ul>--}}
                                 {{--<li><a href="javascript:void(0)">Product 1</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 2</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 3</a></li>--}}
                              {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li><a href="javascript:void(0)">Sub Category 2</a>--}}
                              {{--<ul>--}}
                                  {{--<li><a href="javascript:void(0)">Product 1</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 2</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 3</a></li>--}}
                              {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li><a href="javascript:void(0)">Sub Category 3</a>--}}
                              {{--<ul>--}}
                                 {{--<li><a href="javascript:void(0)">Product 1</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 2</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 3</a></li>--}}
                              {{--</ul>--}}
                        {{--</li>--}}
                         {{--<li><a href="javascript:void(0)">Sub Category 4</a>--}}
                              {{--<ul>--}}
                                {{--<li><a href="javascript:void(0)">Product 1</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 2</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 3</a></li>--}}
                              {{--</ul>--}}
                        {{--</li>--}}
                         {{--<li><a href="javascript:void(0)">Sub Category 5</a>--}}
                              {{--<ul>--}}
                                 {{--<li><a href="javascript:void(0)">Product 1</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 2</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 3</a></li>--}}
                              {{--</ul>--}}
                        {{--</li>--}}
                     {{--</ul>--}}
                     {{--</li>--}}
                  {{--</ul>--}}
{{--</div>--}}
                {{--<div class="col-md-4">--}}
                  {{--<h4 class="f-s18 black font-w500">&nbsp;</h4>--}}
                   {{--<ul class="pull-left p-t20">--}}
                     {{--<li><a href="javascript:void(0)">Category 2</a>--}}
                        {{--<ul>--}}
                           {{--<li><a href="javascript:void(0)">Sub Category 1</a>--}}
                              {{--<ul>--}}
                                 {{--<li><a href="javascript:void(0)">Product 1</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 2</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 3</a></li>--}}
                              {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li><a href="javascript:void(0)">Sub Category 2</a>--}}
                              {{--<ul>--}}
                                  {{--<li><a href="javascript:void(0)">Product 1</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 2</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 3</a></li>--}}
                              {{--</ul>--}}
                        {{--</li>--}}
                        {{--<li><a href="javascript:void(0)">Sub Category 3</a>--}}
                              {{--<ul>--}}
                                 {{--<li><a href="javascript:void(0)">Product 1</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 2</a></li>--}}
                                 {{--<li><a href="javascript:void(0)">Product 3</a></li>--}}
                              {{--</ul>--}}
                        {{--</li>--}}
               {{----}}
                     {{--</ul>--}}
                     {{--</li>--}}
                  {{--</ul>--}}
               {{--</div>--}}
            </div>
         </div>
      </section>
@stop
