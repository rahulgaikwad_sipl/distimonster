@extends('frontend.layouts.app')
@section('title', '| Request a Demo')
@section('content')

    <!--START: Contact Page-->
    <section class="gray-bg top-pad-with-bradcrumb">
         <div class="container clearfix">
            <div class="row pagination">
               <div class="col-md-12">
                  <ul class="breadcrumb f-s14 text-gray p-l0">
                     <li class="breadcrumb-item"><a href="{{url('')}}">Home</a></li>
                     <li class="breadcrumb-item active"><span>Request a Demo</span></li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="tabs_wrapper faq-block">
						@include('frontend.partials.help')
                     <div class="tab_container" style="min-height: 191px;">
                       <h3 class="tab_drawer_heading d_active" rel="tab4">Request a Demo</h3>
                        <div id="tab4" class="tab_content">
                           <h2 class="block-title left-block-title text-left">Request a Demo</h2>
                           <hr>
                           <span>If you would like a live demo of our platform please fill out the following information and a member of our team will contact you to schedule a demo.</span>
                           <form id="request_a_demo" data-parsley-validate="true" method="POST" action="{{ url('save-request-a-demo')}}">
                                       {{ csrf_field() }} 
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" id="company_name" name="company_name" value="" placeholder="Company Name*" data-parsley-required data-parsley-required-message="{{config('constants.COMMON_EMPTY_FIELD_MESSAGE')}}" maxlength="255">
                                                <p class="help-block"></p>
                                                @if($errors->has('company_name'))
                                                    <p class="help-block">
                                                        {{ $errors->first('company_name') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" id="contact_name" name="contact_name" value="" placeholder="Contact Name*" data-parsley-required data-parsley-required-message="{{config('constants.COMMON_EMPTY_FIELD_MESSAGE')}}" maxlength="255">
                                                <p class="help-block"></p>
                                                @if($errors->has('contact_name'))
                                                    <p class="help-block">
                                                        {{ $errors->first('contact_name') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="email" class="form-control" id="email" name="email" value="" placeholder="Email*" data-parsley-required data-parsley-required-message="{{config('constants.COMMON_EMPTY_FIELD_MESSAGE')}}" maxlength="255">
                                                <p class="help-block"></p>
                                                @if($errors->has('email'))
                                                    <p class="help-block">
                                                        {{ $errors->first('email') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" id="phone" name="phone" value="" placeholder="Phone*" data-parsley-required data-parsley-required-message="{{config('constants.COMMON_EMPTY_FIELD_MESSAGE')}}" minlength="12" maxlength="15" pattern="^[0-9]*$" data-parsley-pattern-message="Phone number is invalid">
                                                <p class="help-block"></p>
                                                @if($errors->has('phone'))
                                                    <p class="help-block">
                                                        {{ $errors->first('phone') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group m-t25">
                                            <div class="col-md-12">
                                             <input type="submit" class="btn btn-primary" value="Send" title="Send">
                                            </div>
                                        </div>
                                        </form>
                        </div>
                        <!-- #tab3 End-->
                     </div>
                     <!-- .tab_container -->
                  </div>
                  <div class="shdow-box">
                  </div>
               </div>
            </div>
         </div>
      </section>
@stop
@section('javascript')
   <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNibHtOFv3-YKJOco-Zta63ZPiS7T1CEw&callback=initMap"></script>
   <script>
      $('#chat_link').click(function () {
          MyLiveChat.InlineChatDoExpand();
          //$('.mylivechat_sprite').trigger('click');
      });
   </script>
@endsection


