@extends('frontend.layouts.app')
@section('title', '| Order History')
@section('content')

    <section class="gray-bg account-custome-view top-pad-with-bradcrumb">
        <div class="container-fluid clearfix container-w-80 user-admin">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active"><span>Order History</span></li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
                         @include('frontend.partials.sidebar')
                                           <div class="tab_container">
                            <h3 class="tab_drawer_heading" rel="tab3">Tab 3</h3>
                            <div id="tab3" class="tab_content order-history active ">
                                <h2 class="block-title left-block-title">Order History</h2>
                                <?php
                                $activeTab = '2';
                                if(request()->has('tab')){
                                    $activeTab = request()->get('tab');
                                }
                                ?>
                                <div class="">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link {{($activeTab == '2')?'active':''}}" data-toggle="tab" data-id="2" href="#pending">In Process</a>
                                        </li>
                                        <?php /*
                                        <li class="nav-item">
                                            <a class="nav-link {{($activeTab == '3')?'active':''}}" data-toggle="tab" data-id="3" href="#transit">In Transit</a>
                                        </li> */ ?>
                                        <li class="nav-item">
                                            <a class="nav-link {{($activeTab == '1')?'active':''}}" data-toggle="tab" data-id="1" href="#shipped">Shipped</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link {{($activeTab == '4')?'active':''}}" data-toggle="tab" data-id="4" href="#delivered">Delivered</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div id="pending" class="tab-pane {{($activeTab == '2')?'active':''}}">
                                            @if($activeTab == '2')
                                            @if($orders->isEmpty())
                                                <h2 class="block-title left-block-title">Currently you do not have any pending orders</h2>
                                            @else
<div class="table-responsive">
                                                <table class="table  table-hover table-bordered">
                                                    <tr class="f-s16 font-w500 black">
                                                        <th>Order#</th>
                                                        <th>Date</th>
                                                        <th>Ship To </th>
                                                        <th>Order Total</th>
                                                        <th width="150px;">Status</th>
                                                        <!-- <th>Tracking Information</th> -->
                                                        <th></th>
                                                    </tr>
                                                    @foreach($orders as $order)
                                                        <tr class="shdow-box f-s14 text-gray">
                                                            <td>{{$order->id}}</td>
                                                            <td>{{date('m/d/Y', strtotime($order->order_date)) }}  </td>
                                                            <td>{{$order->name}} {{$order->last_name}}</td>
                                                            <td>${{number_format($order->order_total_amount,2, '.', ',')}}</td>
                                                            @if($order->order_shipping_status  == 1 )
                                                                <td><span class="delivered"></span> {{config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.shipped')}}</td>
                                                            @elseif($order->order_shipping_status  == 2)
                                                                <td><span class="pending"></span>{{($order->partially_shipped  == 1) ? config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.partially_shipped') : config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.pending')}}</td>
                                                            @elseif($order->order_shipping_status  == 3)
                                                                <td><span class="failed"></span> {{ config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.intransit')}}</td>
                                                            @elseif($order->order_shipping_status  == 4)
                                                                <td><span class="delivered"></span> {{ config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.delivered')}}</td>
                                                            @endif
                                                            {{--<td>@if(!empty($order->order_tracking_info)){{$order->order_tracking_info}}@else N/A @endif</td>--}}
                                                            <td class="action">
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item">
                                                                        <a href="{{url('/order-details/'.$order->id)}}" class="f-s14 hind-font"><span class="img-icon"><i class="fa fa-eye"></i></span> View Order</a>
                                                                    </li>
                                                                    <li class="list-inline-item">
                                                                        <a href="{{url('/reorder/'.$order->id)}}" class="f-s14 hind-font" onclick="show_loader()"><span class="img-icon"><i class="fa fa-undo"></i></span> Reorder</a>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <!-- <tr class="blank-space p-0">
                                                            <td colspan="6" class="p-0">&nbsp;</td>
                                                        </tr> -->
                                                    @endforeach
                                                </table>
                                            </div>

                                                <div class="pagination-list text-right clearfix">
                                                    {{ $orders->appends(['page'=>request()->except('page'),'tab'=>request()->get('tab')])->links('frontend.partials.pagination') }}
                                                </div>

                                            @endif
                                            @endif
                                        </div>
                                        <div id="transit" class="tab-pane {{($activeTab == '3')?'active':''}}">
                                            @if($activeTab == '3')
                                            @if($orders->isEmpty())
                                                <h2 class="block-title left-block-title">Currently you do not have any orders</h2>
                                            @else
<div class="table-responsive">
                                            <table class="table table-hover table-bordered">
                                                    <tr class="f-s16 font-w500 black">
                                                        <th>Order#</th>
                                                        <th>Date</th>
                                                        <th>Ship To </th>
                                                        <th>Order Total</th>
                                                        <th>Status</th>
                                                        <th>Tracking Information</th>
                                                        <th></th>
                                                    </tr>
                                                    @foreach($orders as $order)
                                                        <tr class="shdow-box f-s14 text-gray">
                                                            <td>{{$order->id}}</td>
                                                            <td>{{date('m/d/Y', strtotime($order->order_date)) }}  </td>
                                                            <td>{{$order->name}} {{$order->last_name}}</td>
                                                            <td>${{number_format($order->order_total_amount,2, '.', ',')}}</td>
                                                            @if($order->order_shipping_status  == 1 )
                                                                <td><span class="delivered"></span> {{config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.shipped')}}</td>
                                                            @elseif($order->order_shipping_status  == 2)
                                                                <td><span class="pending"></span> {{config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.pending')}}</td>
                                                            @elseif($order->order_shipping_status  == 3)
                                                                <td><span class="failed"></span> {{ config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.intransit')}}</td>
                                                            @elseif($order->order_shipping_status  == 4)
                                                                <td><span class="delivered"></span> {{ config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.delivered')}}</td>
                                                            @endif
                                                            <td>@if(!empty($order->order_tracking_info)){{$order->order_tracking_info}}@else N/A @endif</td>
                                                            <td class="action">
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item">
                                                                        <a href="{{url('/order-details/'.$order->id)}}" class="f-s14 hind-font"><span class="img-icon"><i class="fa fa-eye"></i></span> View Order</a>
                                                                    </li>
                                                                    <li class="list-inline-item">
                                                                        <a href="{{url('/reorder/'.$order->id)}}" class="f-s14 hind-font" onclick="show_loader()"><span class="img-icon"><img src="{{url('frontend/assets/images/reorder-icon.png')}}" alt="reorder-icon" /></span> Reorder</a>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <!-- <tr class="blank-space p-0">
                                                            <td colspan="6" class="p-0">&nbsp;</td>
                                                        </tr> -->
                                                    @endforeach
                                                </table></div>

                                                <div class="pagination-list text-right clearfix">
                                                    {{ $orders->appends(['page'=>request()->except('page'),'tab'=>request()->get('tab')])->links('frontend.partials.pagination') }}
                                                </div>

                                            @endif
                                                @endif
                                        </div>
                                        <div id="shipped" class="tab-pane {{($activeTab == '1')?'active':''}}">
                                            @if($activeTab == '1')
                                            @if($orders->isEmpty())
                                                <h2 class="block-title left-block-title">Currently you do not have any shipped orders</h2>
                                            @else
<div class="table-responsive">
                                            <table class="table table-hover table-bordered">
                                                    <tr class="f-s16 font-w500 black">
                                                        <th>Order#</th>
                                                        <th>Date</th>
                                                        <th>Ship To </th>
                                                        <th>Order Total</th>
                                                        <th>Status</th>
                                                        <th>Tracking Information</th>
                                                        <th></th>
                                                    </tr>
                                                    @foreach($orders as $order)
                                                        <tr class="shdow-box f-s14 text-gray">
                                                            <td>{{$order->id}}</td>
                                                            <td>{{date('m/d/Y', strtotime($order->order_date)) }}  </td>
                                                            <td>{{$order->name}} {{$order->last_name}}</td>
                                                            <td>${{number_format($order->order_total_amount,2, '.', ',')}}</td>
                                                            @if($order->order_shipping_status  == 1 )
                                                                <td><span class="delivered"></span> {{config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.shipped')}}</td>
                                                            @elseif($order->order_shipping_status  == 2)
                                                                <td><span class="pending"></span> {{config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.pending')}}</td>
                                                            @elseif($order->order_shipping_status  == 3)
                                                                <td><span class="failed"></span> {{ config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.intransit')}}</td>
                                                            @elseif($order->order_shipping_status  == 4)
                                                                <td><span class="delivered"></span> {{ config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.delivered')}}</td>
                                                            @endif
                                                            <td>@if(!empty($order->order_tracking_info)){{$order->order_tracking_info}}@else N/A @endif</td>
                                                            <td class="action">
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item">
                                                                        <a href="{{url('/order-details/'.$order->id)}}" class="f-s14 hind-font"><span class="img-icon"><i class="fa fa-eye"></i></span> View Order</a>
                                                                    </li>
                                                                    <li class="list-inline-item">
                                                                        <a href="{{url('/reorder/'.$order->id)}}" class="f-s14 hind-font" onclick="show_loader()"><span class="img-icon"><img src="{{url('frontend/assets/images/reorder-icon.png')}}" alt="reorder-icon" /></span> Reorder</a>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <!-- <tr class="blank-space p-0">
                                                            <td colspan="6" class="p-0">&nbsp;</td>
                                                        </tr> -->
                                                    @endforeach
                                                </table>
                                            </div>

                                                <div class="pagination-list text-right clearfix">
                                                    {{ $orders->appends(['page'=>request()->except('page'),'tab'=>request()->get('tab')])->links('frontend.partials.pagination') }}
                                                </div>

                                            @endif
                                                @endif
                                        </div>
                                        <div id="delivered" class="tab-pane {{($activeTab == '4')?'active':''}}">
                                            @if($activeTab == '4')
                                                @if($orders->isEmpty())
                                                    <h2 class="block-title left-block-title">Currently you do not have any delivered orders</h2>
                                                @else
<div class="table-responsive">
                                                <table class="table table-hover table-bordered">
                                                        <tr class="f-s16 font-w500 black">
                                                            <th>Order#</th>
                                                            <th>Date</th>
                                                            <th>Ship To </th>
                                                            <th>Order Total</th>
                                                            <th>Status</th>
                                                            <th>Tracking Information</th>
                                                            <th></th>
                                                        </tr>
                                                        @foreach($orders as $order)
                                                            <tr class="shdow-box f-s14 text-gray">
                                                                <td>{{$order->id}}</td>
                                                                <td>{{date('m/d/Y', strtotime($order->order_date)) }}  </td>
                                                                <td>{{$order->name}} {{$order->last_name}}</td>
                                                                <td>${{number_format($order->order_total_amount,2, '.', ',')}}</td>
                                                                @if($order->order_shipping_status  == 1 )
                                                                    <td><span class="delivered"></span> {{config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.shipped')}}</td>
                                                                @elseif($order->order_shipping_status  == 2)
                                                                    <td><span class="pending"></span> {{config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.pending')}}</td>
                                                                @elseif($order->order_shipping_status  == 3)
                                                                    <td><span class="failed"></span> {{ config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.intransit')}}</td>
                                                                @elseif($order->order_shipping_status  == 4)
                                                                    <td><span class="delivered"></span> {{ config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.delivered')}}</td>
                                                                @endif
                                                                <td>@if(!empty($order->order_tracking_info)){{$order->order_tracking_info}}@else N/A @endif</td>
                                                                <td class="action">
                                                                    <ul class="list-inline">
                                                                        <li class="list-inline-item">
                                                                            <a href="{{url('/order-details/'.$order->id)}}" class="f-s14 hind-font"><span class="img-icon"><i class="fa fa-eye"></i></span> View Order</a>
                                                                        </li>
                                                                        <li class="list-inline-item">
                                                                            <a href="{{url('/reorder/'.$order->id)}}" class="f-s14 hind-font" onclick="show_loader()"><span class="img-icon"><img src="{{url('frontend/assets/images/reorder-icon.png')}}" alt="reorder-icon" /></span> Reorder</a>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                            <!-- <tr class="blank-space p-0">
                                                                <td colspan="6" class="p-0">&nbsp;</td>
                                                            </tr> -->
                                                        @endforeach
                                                    </table></div>

                                                    <div class="pagination-list text-right clearfix">
                                                        {{ $orders->appends(['page'=>request()->except('page'),'tab'=>request()->get('tab')])->links('frontend.partials.pagination') }}
                                                    </div>

                                                @endif
                                            @endif
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- .tab_container -->
                    </div>
                    <div class="shdow-box">
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
@section('javascript')
    <script>

        $('#register_form').parsley();

        $(document).ready(function(){
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $(".loader").css("display",'block');
                var currentTab = $(e.target).attr('data-id');
                var currentUrl = window.location.href;
                var redirectUrl = updateQueryStringParameter(currentUrl, 'tab', currentTab);
                 redirectUrl = updateQueryStringParameter(redirectUrl, 'page', 1);
                window.location.assign(redirectUrl);
            });
        });

        function show_loader() {
            $(".loader").css("display", 'block');
            $("#loader_note_message").css("display", 'block');
        }

        function updateQueryStringParameter(uri, key, value) {
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            }
            else {
                return uri + separator + key + "=" + value;
            }
        }
    </script>
@endsection




