@extends('frontend.layouts.app')
@section('content')

        <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>DistiMonster</title>
    <!-- Bootstrap -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Hind:400,500,600,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
    <link href="assets/css/custom-style.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!--START: Header Top-->
<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-7">
                <ul class="header-top-left">
                    <li><i class="fa fa-envelope" aria-hidden="true"></i> Email: <a href="mail:sales@distimonster.com">sales@distimonster.com</a></li>
                    <li><i class="fa fa-phone" aria-hidden="true"></i> Phone: <a href="tel:(000) 123-4567">(000) 123-4567</a></li>
                </ul>
            </div>
            <div class="col-md-6 col-sm-5">
                <ul class="header-top-right">
                    <li class="hello-user"><i class="fa fa-user-o" aria-hidden="true"></i> Hello Guest!</li>
                    <li><a href="#">Login</a> / <a href="#">Register</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--END: Header Top-->
<!--START:Top Navigation-->
<!--Header-->
<nav class="navbar custom-header">
    <div class="nav-container">
        <div class="container">
            <div class="row">
                <div class="navbar-header col-md-2 col-sm-2">
                    <a class="navbar-brand" href="index.html"><img src="assets/images/logo.png" class="img-responsive"></a>
                </div>
                <div class="col-md-7 col-sm-6">
                    <form class="form minisearch" id="search_mini_form" action="" method="get">
                        <div class="field search clearfix">
                            <input id="search" type="text" name="q" value="" placeholder="Search for product" class="input-text" />
                            <button type="submit" title="Search" class="action search" disabled="">
                                <span>Search</span>
                            </button>
                        </div>
                    </form>
                    <div class="search-history">
                        <a href="javascript:void(0)">Search History <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <ul class="nav navbar-nav navbar-right checkout m-t15 hidden-xs">
                        <li>
                            <a href="#">
                                <div class="icon"><img src="assets/images/icon-quote-cart.png" alt="Quote Cart" /></div>
                                <span>Quote Cart</span> <br>Empty
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <div class="icon"><img src="assets/images/icon-cart.png" alt="Your Cart" /> <em class="cart-count">05</em></div>
                                <span>Your Cart</span> <br>$256
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
<!--Header end-->
<!--Navigation-->
<div class="custom-nav">
    <div class="container">
        <nav class="navbar navbar-toggleable-md navbar-inverse p-l0">
            <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">ssdd</span>
            </button>
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="about-us.html">About Us <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="newupdate.html">News & Updates</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Support</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="contact-privacy.html">FAQs</a>
                            <a class="dropdown-item" href="contact-privacy.html">Return Policy</a>
                            <a class="dropdown-item" href="contact-privacy.html">Contact Us</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="javascript:void(0)">BOM</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="javascript:void(0)" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">My Account</a>
                        <div class="dropdown-menu" aria-labelledby="dropdown01">
                            <a class="dropdown-item" href="account-setting.html">Account Settings</a>
                            <a class="dropdown-item" href="account-setting.html">Order History</a>
                            <a class="dropdown-item" href="account-setting.html">My Quotes</a>
                            <a class="dropdown-item" href="account-setting.html">My BOM</a>
                        </div>
                    </li>
                </ul>

            </div>
        </nav>
        <div class="navbar-collapse collapse"  id="navbar"">
        <div class="row">
            <div class="col-md-3 col-sm-2">
                <div class="all-product-menu clearfix">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">All Products <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-9 col-sm-10">
                <ul class="nav navbar-nav">
                    <li class="active"><a href="#">All Manufacturers <span class="sr-only">(current)</span></a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">News &amp; Updates</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Support <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                        </ul>
                    </li>
                    <li><a href="#">BOM</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
<!--Navigation end-->
<!--END:Top Navigation-->
<!--Product Listing Section-->
<section class="gray-bg shipping-cart shipping-addres">
    <div class="container clearfix">
        <!---Wizard Step-->
        <div class="row wizard-step">
            <div class="col-md-7 offset-md-3 text-center">
                <ul>
                    <li class="active">
                        <a href="javascript:void(0)">
                            <span class="circle"><i class="fa fa-check"></i></span>
                            <span>Shipping</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <span class="circle">2</span>
                            <span>Review & Payment</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!---Wizard Step-->
        <hr/>

        <div class="row clearfix">
            <div class="col-md-9">
                <h2 class="block-title left-block-title">Shipping Address</h2>
            </div>
        </div>



        <div class="row">
            <div class="col-md-8">
                <!--Shiiping Address Listing Content-->
                <div class="shdow-box">
                    <!--Shipping Addres-->
                    <div class="shipping-address-content-second">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="address-detail-list f-s14 text-gray">
                                    <p>Carroll Smith</p>
                                    <p>7546 N Green Meadows Est</p>
                                    <p>Fairland, IN, 46126</p>
                                    <p>(000) 123-4567</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="right-block-link text-right">
                                    <a href="javascript:void(0)" class="f-s16 font-w500"><span class="img-icon"><img src="assets/images/shipping-here-icon.png" alt="shipping-here-icon" /></span> Ship Here</a>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!--Shipping Addres-->
                </div>
                <!--Shiiping Address Listing Content-->
                <!--Shiiping Address Listing Content-->
                <div class="shdow-box">
                    <!--Shipping Addres-->
                    <div class="shipping-address-content-second">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="address-detail-list f-s14 text-gray">
                                    <p>Carroll Smith</p>
                                    <p>7546 N Green Meadows Est</p>
                                    <p>Fairland, IN, 46126</p>
                                    <p>(000) 123-4567</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="right-block-link text-right">
                                    <a href="javascript:void(0)" class="f-s16 font-w500 hind-font"><span class="img-icon"><img src="assets/images/shipping-here-icon.png" alt="shipping-here-icon" /></span> Ship Here</a>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!--Shipping Addres-->
                </div>
                <!--Shiiping Address Listing Content-->

                <!--Shiiping Address Listing Content-->
                <div class="shdow-box list-check-active">
                    <a href="javascript:void(0)">
                        <span class="circle"><i class="fa fa-check"></i></span>
                    </a>
                    <!--Shipping Addres-->
                    <div class="shipping-address-content-second">
                        <div class="row">
                            <div class="col-md-9">
                                <div class="address-detail-list f-s14 text-gray">
                                    <p>Carroll Smith</p>
                                    <p>7546 N Green Meadows Est</p>
                                    <p>Fairland, IN, 46126</p>
                                    <p>(000) 123-4567</p>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="right-block-link text-right">
                                    <a href="javascript:void(0)" class="f-s16 font-w500 hind-font"><span class="img-icon"><img src="assets/images/shipping-here-icon.png" alt="shipping-here-icon" /></span> Ship Here</a>
                                </div>
                            </div>
                        </div>

                    </div>

                    <!--Shipping Addres-->
                </div>
                <!--Shiiping Address Listing Content-->

                <div class="add-new-address"><a href="javascript:void(0)" class="f-s16 font-w500 hind-font"><span class="img-icon"><img src="assets/images/add-round-icon.png" alt="Add New Address" /></span> Add New Address</a></div>
                <!--Shiiping Address Listing Content-->
                <div class="shdow-box">
                    <!--Shipping Addres-->
                    <div class="shipping-address-content-second">
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <h2 class="block-title">Shipping Methods</h2>

                            </div>

                            <div class="col-md-12 shipping-method">
                                <div class="form-check form-check-inline f-s14 text-gray">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1">
                                    <label class="form-check-label">$80</label>
                                </div>

                                <div class="form-check form-check-inline f-s14 text-gray m-b10">
                                    <!-- <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2"> -->
                                    <label class="form-check-label">Domestic Shipping Charges</label>
                                </div>

                                <div class="form-check form-check-inline f-s14 text-gray m-b10">
                                    <!-- <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="3"> -->
                                    <label class="form-check-label">Shipping Charges</label>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-check form-check-inline f-s14 text-gray m-b10">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1">
                                    <label class="form-check-label">$80</label>
                                </div>

                                <div class="form-check form-check-inline f-s14 text-gray">
                                    <!-- <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2"> -->
                                    <label class="form-check-label">Domestic Shipping Charges</label>
                                </div>

                                <div class="form-check form-check-inline f-s14 text-gray">
                                    <!-- <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="3"> -->
                                    <label class="form-check-label">Shipping Charges</label>
                                </div>
                            </div>


                        </div>

                    </div>

                    <!--Shipping Addres-->
                </div>
                <!--Shiiping Address Listing Content-->


            </div>

            <div class="col-md-4">
                <div class="shdow-box shipping-right-content m-t0">
                    <div class="price-content-box">
                        <div class="row price-box-heading">
                            <div class="col-md-10">
                                <h2 class="block-title">Order Summary</h2>
                            </div>
                            <div class="col-md-2">
                                &nbsp;
                            </div>
                        </div>
                        <div class="price-content-body">
                            <div class="f-s14 font-w500 item-cart-count">4 Items in cart</div>
                            <ul class="scroll">

                                <li class="clearfix ">
                                    <div class="row shipping-address-right-summary">
                                        <div class="col-md-6">
                                            <div><a href="javascript:void(0)" class="hind-font">B82496X001</a></div>
                                            <div class="f-s12">EPCOS</div>
                                            <div class="f-s12 text-gray quantity">Quantity: 1</div>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <div class="black f-s16">$6,354.20</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <div class="row shipping-address-right-summary">
                                        <div class="col-md-6">
                                            <div><a href="javascript:void(0)" class="hind-font">B82496X001</a></div>
                                            <div class="f-s12">EPCOS</div>
                                            <div class="f-s12 text-gray quantity">Quantity: 1</div>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <div class="black f-s16">$6,354.20</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <div class="row shipping-address-right-summary">
                                        <div class="col-md-6">
                                            <div><a href="javascript:void(0)" class="hind-font">B82496X001</a></div>
                                            <div class="f-s12">EPCOS</div>
                                            <div class="f-s12 text-gray quantity">Quantity: 1</div>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <div class="black f-s16">$6,354.20</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <div class="row shipping-address-right-summary">
                                        <div class="col-md-6">
                                            <div><a href="javascript:void(0)" class="hind-font">B82496X001</a></div>
                                            <div class="f-s12">EPCOS</div>
                                            <div class="f-s12 text-gray quantity">Quantity: 1</div>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <div class="black f-s16">$6,354.20</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <div class="row shipping-address-right-summary">
                                        <div class="col-md-6">
                                            <div><a href="javascript:void(0)" class="hind-font">B82496X001</a></div>
                                            <div class="f-s12">EPCOS</div>
                                            <div class="f-s12 text-gray quantity">Quantity: 1</div>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <div class="black f-s16">$6,354.20</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <div class="row shipping-address-right-summary">
                                        <div class="col-md-6">
                                            <div><a href="javascript:void(0)" class="hind-font">B82496X001</a></div>
                                            <div class="f-s12">EPCOS</div>
                                            <div class="f-s12 text-gray quantity">Quantity: 1</div>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <div class="black f-s16">$6,354.20</div>
                                        </div>
                                    </div>
                                </li>
                                <li class="clearfix">
                                    <div class="row shipping-address-right-summary">
                                        <div class="col-md-6">
                                            <div><a href="javascript:void(0)" class="hind-font">B82496X001</a></div>
                                            <div class="f-s12">EPCOS</div>
                                            <div class="f-s12 text-gray quantity">Quantity: 1</div>
                                        </div>
                                        <div class="col-md-6 text-right">
                                            <div class="black f-s16">$6,354.20</div>
                                        </div>
                                    </div>
                                </li>

                            </ul>
                        </div>
                        <div class="price-content-footer">
                            <button type="submit" class="btn btn-primary" title="Proceed to Checkout">Proceed to Checkout</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>






    </div>
</section>
<!--Product Listing Section-->
<!--START: Footer-->
<section id="footer">
    <div class="container footer">
        <div class="row bdr-bottom p-b20">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4>Contact</h4>
                <ul class="address-list">
                    <li class="address-list-item"><i class="fa fa-home" aria-hidden="true"></i> Lorem 76d, Ipsum Dollor, North Michaelchester, CF99 6QQ</li>
                    <li class="address-list-item"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:sales@distimonster.com">sales@distimonster.com</a></li>
                    <li class="address-list-item"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:(000) 123-4567">(000) 123-4567</a></li>
                </ul>
                <div class="payment-card"><img src="assets/images/payment-card.png" class="img-responsive"></div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4>More Info</h4>
                <ul class="list">
                    <li class="list-item"><a href="#">About DistiMonster</a></li>
                    <li class="list-item"><a href="#">News &amp; Updates</a></li>
                    <li class="list-item"><a href="#">Privacy Policy and Terms of Use</a></li>
                    <li class="list-item"><a href="#">Site Map</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 blank-title">
                <h4>&nbsp;</h4>
                <ul class="list">
                    <li class="list-item"><a href="#">FAQs</a></li>
                    <li class="list-item"><a href="#">Return Policy</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <h4>Signup for Newsletter</h4>
                <form class="newsletter-form">
                    <p>Get the latest news, special offers and other discount information.</p>
                    <div class="newsletter-form-item">
                        <input type="text" name="emailNewsletter" placeholder="Your email address" />
                        <input type="button" value="Submit" title="Submit" class="newsletter-btn" />
                    </div>
                </form>
                <ul class="social-list">
                    <!-- <li class="social-list-item"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li> -->
                    <li class="social-list-item"><a href="https://twitter.com/distimonster"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <!-- <li class="social-list-item"><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                    <li class="social-list-item"><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li> -->
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <p class="copyright">© Copyright <span>DistiMonster</span>. All rights Reserved, 2018</p>
            </div>
        </div>
    </div>
</section>
<!--END: Footer-->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="assets/js/site.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
</body>
</html>
@stop
