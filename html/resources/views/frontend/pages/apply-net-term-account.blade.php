@extends('frontend.layouts.app')
@section('title', '| Apply Net Terms Account')
@section('content')
    <section class="gray-bg top-pad-with-bradcrumb">
        <div class="container clearfix">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active"><span>Apply for a Net Term Account</span></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
                        <ul class="tabs shdow-box" id="tabMenu">
                            <li rel="tab2"><a href="{{url('account-settings/'.Auth::user()->id)}}">Account Settings</a></li>
                            <li><a href="{{url('order-history')}}">Order History</a></li>
                            <li  rel="tab4"><a href="{{url('saved-quotes')}}">Quotes</a></li>
                            <li  rel="tab5"><a href="{{url('my-bom')}}">My BOM</a></li>
                            <li rel="tab7"><a href="{{url('my-bio')}}">My BIO</a></li>
                            <li class="active" rel="tab6"><a href="{{url('apply-net-term-account')}}">Apply for a Net Terms Account</a></li>
                        </ul>
                        <div class="tab_container">
                            <h3 class="tab_drawer_heading" rel="tab6">Apply for a Net Terms Account</h3>
                            <div id="tab6" class="tab_content net-terms-content">
                                <h2 class="block-title left-block-title">Apply for a Net Terms Account</h2>
                                @if(empty($isRequestExist))
                                <div class="shdow-box edit-account-setting">
                                    <div class="edit-account-setting-content">
                                        {!! Form::open(['files' => true,'method' => 'POST', 'class'=>'form-horizontal' ,'id'=>'net_term_account', 'data-parsley-validate','url' => 'apply-net-term-account','enctype' => 'multipart/form-data',]) !!}
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::text('first_name', !empty($user->name)? $user->name:old('first_name'), ['readonly'=>true, 'class' => 'form-control', 'placeholder' => 'First Name*', 'data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.name_min'),'data-parsley-maxlength' => config('app.fields_length.name_max'),'data-parsley-minlength-message' => $validationMessage['first_name.min'],'data-parsley-maxlength-message' => $validationMessage['first_name.max']]) !!}
                                                    <p class="help-block"></p>
                                                    @if($errors->has('first_name'))
                                                        <p class="help-block">
                                                            {{ $errors->first('first_name') }}
                                                        </p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::text('last_name', !empty($user->last_name)? $user->last_name:old('last_name'), ['readonly'=>true, 'class' => 'form-control', 'placeholder' => 'Last Name*', 'data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.name_min'),'data-parsley-maxlength' => config('app.fields_length.name_max'),'data-parsley-minlength-message' => $validationMessage['last_name.min'],'data-parsley-maxlength-message' => $validationMessage['last_name.max']]) !!}
                                                    <p class="help-block"></p>
                                                    @if($errors->has('last_name'))
                                                        <p class="help-block">
                                                            {{ $errors->first('last_name') }}
                                                        </p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::email('email', !empty($user->email)? $user->email:old('email'), ['readonly'=>true, 'class' => 'form-control', 'data-parsley-required','placeholder' => 'Email','id'=>'email']) !!}
                                                    <p class="help-block"></p>
                                                    @if($errors->has('email'))
                                                        <p class="help-block">
                                                            {{ $errors->first('email') }}
                                                        </p>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    {!! Form::select('account_type', $accountType, null, ['id' => 'account_type','data-parsley-required', 'data-parsley-required-message'=>$validationMessage['account_type.required'],'class'=>'form-control']) !!}
                                                    @if ($errors->has('account_type'))
                                                        <span class="help-block">
                                                            <strong>{{ $errors->first('account_type') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="form-group ">
                                                <div class="col-md-12">
                                                    <div class="net-file-upload">
                                                        <label for="exampleInputFile" class="f-s14 text-gray">Upload Credit Reference document (Please upload your standard credit reference form that includes your bank information and at least 3 trade references )</label>

                                                        {!! Form::file('credit_reference_doc[]', ['multiple','data-parsley-max-file-size'=>"1", 'data-parsley-pdf-file'=>'pdf', 'data-parsley-required','id' => 'credit_reference_doc', 'class'=>'form-control-file f-s14 text-gray' ,'data-parsley-required-message'=>$validationMessage['credit_reference_doc.required']]) !!}
                                                        <p class="help-block"></p>
                                                        <?php //print_r($errors);?>
                                                        @if($errors->has('credit_reference_doc[]'))
                                                            <p class="help-block">
                                                                {{ $errors->first('credit_reference_doc[]') }}
                                                            </p>
                                                        @endif



                                                        {{--{!! Form::file('resale_doc', ['data-parsley-required','id' => 'resale_doc', 'class'=>'form-control-file f-s14 text-gray' , 'aria-describedby'=>'fileHelp','data-parsley-required-message'=>$validationMessage['resale_doc.required']]) !!}--}}
                                                        {{--<p class="help-block"></p>--}}
                                                        {{--@if($errors->has('resale_doc'))--}}
                                                            {{--<p class="help-block">--}}
                                                                {{--{{ $errors->first('resale_doc') }}--}}
                                                            {{--</p>--}}
                                                        {{--@endif--}}
                                                        {{--<label for="exampleInputFile" class="f-s14 text-gray">Upload Cod Application document </label>--}}
                                                        {{--{!! Form::file('cod_application_doc', ['data-parsley-required','id' => 'cod_application_doc', 'class'=>'form-control-file f-s14 text-gray' , 'aria-describedby'=>'fileHelp','data-parsley-required-message'=>$validationMessage['cod_application_doc.required']]) !!}--}}
                                                        {{--<p class="help-block"></p>--}}
                                                        {{--@if($errors->has('cod_application_doc'))--}}
                                                            {{--<p class="help-block">--}}
                                                                {{--{{ $errors->first('cod_application_doc') }}--}}
                                                            {{--</p>--}}
                                                        {{--@endif--}}
                                                        {{--<label for="exampleInputFile" class="f-s14 text-gray">Upload  Net Terms Application document </label>--}}
                                                        {{--{!! Form::file('net_terms_application_doc', ['data-parsley-file-extension'=>'pdf','data-parsley-required','id' => 'net_terms_application_doc', 'class'=>'form-control-file f-s14 text-gray' , 'aria-describedby'=>'fileHelp','data-parsley-required-message'=>$validationMessage['net_terms_application_doc.required']]) !!}--}}
                                                        {{--<p class="help-block"></p>--}}
                                                        {{--@if($errors->has('net_terms_application_doc'))--}}
                                                            {{--<p class="help-block">--}}
                                                                {{--{{ $errors->first('net_terms_application_doc') }}--}}
                                                            {{--</p>--}}
                                                        {{--@endif--}}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label class="text-gray f-s14 ">Download sample reference documents</label>
                                                    <div class="clearfix"></div>
                                                    <div class="btn-dot"><a target="_blank" href="{{url('sample-document/California_Resale_Certificate.pdf')}}"><span class="img-icon"><img src="{{url('frontend/assets/images/pdf-icon.png')}}" alt="Resale Certificate" /></span> Resale Certificate</a></div>
                                                    <div class="btn-dot"><a target="_blank" href="{{url('sample-document/Cod_Application.pdf')}}"><span class="img-icon"><img src="{{url('frontend/assets/images/pdf-icon.png')}}" alt="Cod Application" /></span> Cod Application </a></div>
                                                    <div class="btn-dot"><a target="_blank" href="{{url('sample-document/Net_Terms_Application.pdf')}}"><span class="img-icon"><img src="{{url('frontend/assets/images/pdf-icon.png')}}" alt="Net Term Application" /></span> Net Term Application</a></div>
                                                </div>
                                            </div>

                                            <div class="form-group m-t25">
                                                <div class="col-md-12">
                                                    <button type="submit" class="btn btn-primary" title="Submit">Submit</button>
                                                </div>
                                            </div>
                                    {!! Form::close() !!}
                                        <!-- /form -->
                                    </div>
                                </div>
                                    @else
                                    <?php // echo "<pre>"; print_r($isRequestExist->toArray()); echo "</pre>";?>
                                    <div>
                                        You have already applied  for <strong>{{ $isRequestExist[0]->requested_account_type}} </strong>  and current status for request is
                                       <Strong>
                                        @if($isRequestExist[0]->status == 0)
                                            <span>Pending.</span>
                                        @endif
                                        @if($isRequestExist[0]->status == 1)
                                            <span>Approved.</span>
                                        @endif
                                        @if($isRequestExist[0]->status == 2)
                                            <span>In Progress.</span>
                                        @endif
                                        @if($isRequestExist[0]->status == 3)
                                            <div>Rejected.</div>
                                        @endif
                                       </Strong>
                                    </div>
                                @endif
                            </div>
                            <!-- #tab6 End -->
                        </div>
                        <!-- .tab_container -->
                    </div>
                    <div class="shdow-box">
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('javascript')
    <script>
        $(document).ready(function() {
            window.ParsleyValidator.addValidator('fileExtension', function (value, requirement) {
                alert(value);
                    var fileExtension = value.split('.').pop();
                    return fileExtension === requirement;
                }, 32).addMessage('en', 'fileextension', 'The extension doesn\'t match the required');
        });
    </script>
@endsection


