@extends('frontend.layouts.app')
@section('title', '| '.$title)
@section('content')
    <section class="gray-bg top-pad-with-bradcrumb">
        <div class="container-fluid clearfix container-w-80 user-admin">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active"><span>{{$title}}</span></li>

                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
                        @include('frontend.partials.sidebar')
                            <div class="tab_container">
                            <h3 class="tab_drawer_heading" rel="tab5">Tab 5</h3>
                            <div id="tab5" class="tab_content">
                                <h2 class="block-title left-block-title">{{$title}}</h2>
                                @if(count($quotes)>0)
                                    @foreach($quotes->chunk(2) as $items)
                                        <div class="row ">
                                            @foreach($items as $item)
                                                <div class="col-md-6">
                                                    <div class="shdow-box my-quote-detail">
                                                        @if($item->is_unnamed)
                                                            <h3 class="f-s18"><a href="{{url($item->id.'/quote-details')}}" >Unnamed</a></h3>
                                                        @else
                                                            <h3 class="f-s18"><a href="{{url($item->id.'/quote-details')}}" >{{$item->quote_name?$item->quote_name:"Unnamed-".$item->id}}</a></h3>
                                                        @endif
                                                        <div class="f-s14 text-gray btm-space">Quote Status:
                                                            @if($item->is_approved	  == 0 )
                                                                <span class="img-icon waiting-icon">
                                                                    <!-- <img src="{{url('frontend/assets/images/pending.png')}}" alt="pending" />--><span class="waiting waiting-btn">Pending</span></span> 
                                                                @elseif($item->is_approved ==1)
                                                                <span class="img-icon approved"><img src="{{url('frontend/assets/images/approved.png')}}" alt="Processed" /><span class="success">Processed</span></span>
                                                                @elseif($item->is_approved ==2)
                                                                <span class="img-icon rejected"><img src="{{url('frontend/assets/images/rejected.png')}}" alt="rejected" /><span class="cancelled">Rejected</span></span>
                                                                @elseif($item->is_approved ==3)
                                                                <span class="img-icon"><img src="{{url('frontend/assets/images/expiry.png')}}" alt="expired" /><span class="expiry">Expired</span></span>
                                                                @elseif($item->is_approved ==4)
                                                                <span class="img-icon"><img src="{{url('frontend/assets/images/cart-small-icon.png')}}" alt="ordered" /><span class="textprimary">Ordered</span></span>
                                                                @endif
                                                        </div>
                                                        <div class="f-s14 text-gray btm-space">Total Parts: <span class="black font-w500">{{$item->part_counts}}</span></div>
                                                        <div class="f-s14 text-gray btm-space">Last modified: <span class="black font-w500">{{ date('m/d/Y', strtotime($item->created_at)) }}</span></div>
                                                        {{--<div class="f-s14 text-gray top-space">Subtotal: <span class="black font-w500">${{number_format($item->part_total,2,'.',',')}}</span></div>--}}
                                                        <div class="my-quote-detail-footer ">
                                                            <ul class="list-inline m-b0">
                                                                @if($item->is_approved ==1 && $item->is_responded_lead_time ==1)
                                                                    <li class="list-inline-item">
                                                                        <a href="{{url('add-quote-to-cart/'.$item->id)}}" class="f-s16 font-w500 hind-font btn-dark-green"><span class="img-icon"><i class="fa fa-shopping-cart"></i></span>Add Quote to Cart</a>
                                                                    </li>
                                                                @endif
                                                                <li class="list-inline-item text-right">
                                                                <?php
                                                                $msg = '';
                                                                if($item->is_requested_lead_time == 1 && $item->is_responded_lead_time == 0){
                                                                    $msg = 'Requested Lead Time';
                                                                }else{
                                                                    $msg = 'Responded Lead Time';
                                                                }
                                                                ?>
                                                                    <a href="javascript:void(0)"  onclick="quoteModule.removeQuote('<?php echo $item->id;?>' ,1, '<?php echo $msg; ?>')"  class="f-s16 font-w500 hind-font pull-right remove-icon-pois"><span class="img-icon"><i class="fa fa-trash"></i></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                        </div>
                                    @endforeach
                                    <div class="pagination-list text-right clearfix">
                                        {{ $quotes->appends(request()->except('page'))->links('frontend.partials.pagination') }}
                                    </div>
                                @else
                                    <div class="alert alert-info">
                                        {{$notFoundMsg}}
                                    </div>
                                @endif
                            </div>

                        </div>
                        <!-- .tab_container -->
                    </div>
                    <div class="shdow-box">
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop