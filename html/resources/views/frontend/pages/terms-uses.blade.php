@extends('frontend.layouts.app')
@section('title', '| Terms of Use')
@section('content')

    <!--START: Terms of use Page-->
    <section class="gray-bg top-pad-with-bradcrumb">
         <div class="container clearfix">
            <div class="row pagination">
               <div class="col-md-12">
                  <ul class="breadcrumb f-s14 text-gray p-l0">
                     <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>

                     <li class="breadcrumb-item active"><span>Terms of Use</span> </li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="tabs_wrapper faq-block">
                     <ul class="tabs shdow-box">
                        <li class="active" rel="tab1">Terms of Use</li>
                        <!-- <li rel="tab2">Terms and Conditions of Sale</li> -->
                        <li rel="tab3">Privacy Policy</li>
                        <li rel="tab4">Terms and Conditions of Export</li>
                        <li rel="tab5">Cookies Policies</li>
                        <!-- <li rel="tab5">Cookie Policy</li> -->
                     </ul>
                     <div class="tab_container">
                        <h3 class="d_active tab_drawer_heading" rel="tab1">Terms of Use</h3>
                        <div id="tab1" class="tab_content order-history terms-use-content">
                           <h2 class="block-title left-block-title">Terms of Use </h2><span>Last updated: May 20, 2018</span>
                           <hr/>
                           <div class="f-s18 black font-w500 terms-use-heading"></div>
                           <p class="f-s14 text-gray">Please read these Terms of Use (&quot;Terms&quot;, &quot;Terms of Use&quot;) carefully before using the
                              http://www.distimonster.com website (the &quot;Service&quot;) operated by DistiMonster (&quot;us&quot;, &quot;we&quot;,
                              or &quot;our&quot;).
                              <p class="f-s14 text-gray">Your access to and use of the Service is conditioned upon your acceptance of and
                              compliance with these Terms. These Terms apply to all visitors, users and others who wish
                              to access or use the Service.</p>
                              <p class="f-s14 text-gray">By accessing or using the Service you agree to be bound by these Terms. If you disagree
                                 with any part of the terms then you do not have permission to access the Service.</p>
                           </p>

                           <div class="f-s18 black font-w500 terms-use-heading">Purchases</div>
                           <p class="f-s14 text-gray">
                              If you wish to purchase any product or service made available through the Service
                              (&quot;Purchase&quot;), you may be asked to supply certain information relevant to your Purchase
                              including, without limitation, your credit card number, the expiration date of your credit
                              card, your billing address, and your shipping information.
                           </p>
                           <p class="f-s14 text-gray">
                              You represent and warrant that: (i) you have the legal right to use any credit card(s) or
                              other payment method(s) in connection with any Purchase; and that (ii) the information
                              you supply to us is true, correct and complete.
                           </p>
                           <p class="f-s14 text-gray">
                              The service may employ the use of third party services for the purpose of facilitating
                              payment and the completion of Purchases. By submitting your information, you grant us the
                              right to provide the information to these third parties subject to our Privacy Policy.
                           </p>
                           <p class="f-s14 text-gray">
                              We reserve the right to refuse or cancel your order at any time for reasons including but not
                              limited to: product or service availability, errors in the description or price of the product or
                              service, error in your order or other reasons.
                           </p>
                           <p class="f-s14 text-gray">
                              We reserve the right to refuse or cancel your order if fraud or an unauthorized or illegal
                              transaction is suspected.
                           </p>

                            <div class="f-s18 black font-w500 terms-use-heading">Availability, Errors and Inaccuracies</div>
                           <p class="f-s14 text-gray">
                              We are constantly updating product and service offerings on the Service. We may
                              experience delays in updating information on the Service and in our advertising on other
                              web sites. The information found on the Service may contain errors or inaccuracies and may
                              not be complete or current. Products or services may be mispriced, described inaccurately,
                              or unavailable on the Service and we cannot guarantee the accuracy or completeness of any
                              information found on the Service.
                           </p>
                           <p class="f-s14 text-gray">We therefore reserve the right to change or update information and to correct errors,
                           inaccuracies, or omissions at any time without prior notice.</p>


                           <div class="f-s18 black font-w500 terms-use-heading"> Contests, Sweepstakes and Promotions</div>
                           <p class="f-s14 text-gray"> Any contests, sweepstakes or other promotions (collectively, &quot;Promotions&quot;) made available
                           through the Service may be governed by rules that are separate from these Terms
                           Conditions. If you participate in any Promotions, please review the applicable rules as well
                           as our Privacy Policy. If the rules for a Promotion conflict with these Terms of Use, the
                           Promotion rules will apply.
                           </p>

                           <div class="f-s18 black font-w500 terms-use-heading"> Accounts</div>
                           <p class="f-s14 text-gray">When you create an account with us, you guarantee that you are above the age of 18, and
                              that the information you provide us is accurate, complete, and current at all times.
                              Inaccurate, incomplete, or obsolete information may result in the immediate termination of
                              your account on the Service.</p>
                           <p class="f-s14 text-gray">You are responsible for maintaining the confidentiality of your account and password,
                              including but not limited to the restriction of access to your computer and/or account. You
                              agree to accept responsibility for any and all activities or actions that occur under your
                              account and/or password, whether your password is with our Service or a third-party
                              service. You must notify us immediately upon becoming aware of any breach of security or
                              unauthorized use of your account.</p>
                           <p class="f-s14 text-gray">We reserve the right to refuse service, terminate accounts, remove or edit content, or cancel
                              orders in our sole discretion.</p>

                            <div class="f-s18 black font-w500 terms-use-heading">Intellectual Property</div>
                           <p class="f-s14 text-gray">
                              The Service and its original content, features and functionality are and will remain the
                              exclusive property of DistiMonster and its licensors. The Service is protected by copyright,
                              trademark, and other laws of both the United States and foreign countries. Our trademarks
                              and trade dress may not be used in connection with any product or service without the
                              prior written consent of DistiMonster.
                           </p>

                             <div class="f-s18 black font-w500 terms-use-heading">Links To Other Web Sites</div>
                              <p class="f-s14 text-gray">
                                 Our Service may contain links to third party web sites or services that are not owned or
                                 controlled by DistiMonster
                              </p>
                           <p class="f-s14 text-gray">
                              DistiMonster has no control over, and assumes no responsibility for the content, privacy
                              policies, or practices of any third party web sites or services. We do not warrant the
                              offerings of any of these entities/individuals or their websites.
                           </p>
                           <p class="f-s14 text-gray">
                              You acknowledge and agree that DistiMonster shall not be responsible or liable, directly or
                              indirectly, for any damage or loss caused or alleged to be caused by or in connection with
                              use of or reliance on any such content, goods or services available on or through any such
                              third party web sites or services.
                           </p>
                           <p class="f-s14 text-gray">
                              We strongly advise you to read the terms and conditions and privacy policies of any third
                              party web sites or services that you visit.
                           </p>

                           <div class="f-s18 black font-w500 terms-use-heading">Termination</div>
                           <p class="f-s14 text-gray">
                              We may terminate or suspend your account and bar access to the Service immediately,
                              without prior notice or liability, under our sole discretion, for any reason whatsoever and
                              without limitation, including but not limited to a breach of the Terms.
                           </p>
                           <p class="f-s14 text-gray">
                              If you wish to terminate your account, you may simply discontinue using the Service.
                           </p>
                           <p class="f-s14 text-gray">
                              All provisions of the Terms which by their nature should survive termination shall survive
                              termination, including, without limitation, ownership provisions, warranty disclaimers,
                              indemnity and limitations of liability.
                           </p>


                           <div class="f-s18 black font-w500 terms-use-heading">Indemnification</div>
                           <p class="f-s14 text-gray">You agree to defend, indemnify and hold harmless DistiMonster and its licensee and
                              licensors, and their employees, contractors, agents, officers and directors, from and against
                              any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses
                              (including but not limited to attorney&#39;s fees), resulting from or arising out of a) your use and
                              access of the Service, by you or any person using your account and password, or b) a breach
                              of these Terms.</p>
                           <div class="f-s18 black font-w500 terms-use-heading">Limitation Of Liability</div>
                           <p class="f-s14 text-gray">In no event shall DistiMonster, nor its directors, employees, partners, agents, suppliers, or
                              affiliates, be liable for any indirect, incidental, special, consequential or punitive damages,
                              including without limitation, loss of profits, data, use, goodwill, or other intangible losses,
                              resulting from (i) your access to or use of or inability to access or use the Service; (ii) any
                              conduct or content of any third party on the Service; (iii) any content obtained from the
                              Service; and (iv) unauthorized access, use or alteration of your transmissions or content,
                              whether based on warranty, contract, tort (including negligence) or any other legal theory,
                              whether or not we have been informed of the possibility of such damage, and even if a
                              remedy set forth herein is found to have failed of its essential purpose.</p>
                           <div class="f-s18 black font-w500 terms-use-heading">Disclaimer</div>
                           <p class="f-s14 text-gray">Your use of the Service is at your sole risk. The Service is provided on an &quot;AS IS&quot; and &quot;AS
                              AVAILABLE&quot; basis. The Service is provided without warranties of any kind, whether express
                              or implied, including, but not limited to, implied warranties of merchantability, fitness for a
                              particular purpose, non-infringement or course of performance.</p>
                           <p class="f-s14 text-gray">DistiMonster its subsidiaries, affiliates, and its licensors do not warrant that a) the Service
                              will function uninterrupted, secure or available at any particular time or location; b) any
                              errors or defects will be corrected; c) the Service is free of viruses or other harmful
                              components; or d) the results of using the Service will meet your requirements.</p>
                           <div class="f-s18 black font-w500 terms-use-heading">Exclusions</div>
                           <p class="f-s14 text-gray">Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or
                              limitation of liability for consequential or incidental damages, so the limitations above may
                              not apply to you.</p>
                           <div class="f-s18 black font-w500 terms-use-heading">Governing Law</div>
                           <p class="f-s14 text-gray">These Terms shall be governed and construed in accordance with the laws of California,
                              United States, without regard to its conflict of law provisions.</p>
                           <p class="f-s14 text-gray">Our failure to enforce any right or provision of these Terms will not be considered a waiver
                              of those rights. If any provision of these Terms is held to be invalid or unenforceable by a
                              court, the remaining provisions of these Terms will remain in effect. These Terms constitute
                              the entire agreement between us regarding our Service, and supersede and replace any
                              prior agreements we might have had between us regarding the Service.</p>

                           <div class="f-s18 black font-w500 terms-use-heading">Changes</div>
                           <p class="f-s14 text-gray">We reserve the right, at our sole discretion, to modify or replace these Terms at any time. If
                              a revision is material we will provide at least 30 days notice prior to any new terms taking
                              effect. What constitutes a material change will be determined at our sole discretion.</p>
                           <p class="f-s14 text-gray">By continuing to access or use our Service after any revisions become effective, you agree to
                              be bound by the revised terms. If you do not agree to the new terms, you are no longer
                              authorized to use the Service.</p>
                           <div class="f-s18 black font-w500 terms-use-heading">Contact Us</div>
                           <p class="f-s14 text-gray">If you have any questions about these Terms, please contact us.</p>

                        </div>
                        <!-- #tab2 End-->
                        <h3 class="tab_drawer_heading" rel="tab3">Privacy Policy</h3>
                        <div id="tab3" class="tab_content order-history contact-us text-left">
                           <h2 class="block-title left-block-title text-left">Privacy Policy</h2><span>Effective date: May 09, 2018</span>

                           <div class="shdow-box edit-account-setting">
                              <div class="edit-account-setting-content f-s14 text-gray">
                                 <p>DistiMonster (&quot;us&quot;, &quot;we&quot;, or &quot;our&quot;) operates the www.distimonster.com website (the
                                    &quot;Service&quot;).
                                    This page informs you of our policies regarding the collection, use, and disclosure of
                                    personal data when you use our Service and the choices you have associated with that data.
                                    We use your data to provide and improve the Service. By using the Service, you agree to the
                                    collection and use of information in accordance with this policy. Unless otherwise defined in
                                    this Privacy Policy, terms used in this Privacy Policy have the same meanings as in our
                                    Terms and Conditions, accessible from www.distimonster.com</p>
                                    <div class="f-s18 black font-w500 terms-use-heading">Information Collection And Use</div>
                                    <p class="f-s14 text-gray">We collect several different types of information for various purposes to provide and
                                       improve our Service to you.</p>
                                 <div class="f-s18 black font-w500 terms-use-heading">Types of Data Collected</div>
                                 <div class="f-s18 black font-w500 terms-use-heading">Personal Data</div>
                                 <p class="f-s14 text-gray">While using our Service, we may ask you to provide us with certain personally identifiable
                                    information that can be used to contact or identify you (&quot;Personal Data&quot;). Personally
                                    identifiable information may include, but is not limited to:</p>
                                 <ul class="f-s14 text-gray">
                                    <li>Email address</li>
                                    <li>First name and last name</li>
                                    <li>Phone number</li>
                                    <li>Address, State, Province, ZIP/Postal code, City</li>
                                    <li> Cookies and Usage Data</li>
                                 </ul>
                                 <p class="f-s14 text-gray">We may use your Personal Data to contact you with newsletters, marketing or promotional
                                    materials and other information that may be of interest to you. You may opt out of receiving
                                    any, or all, of these communications from us by following the unsubscribe link or
                                    instructions provided in any email we send.</p>

                                 <div class="f-s18 black font-w500 terms-use-heading">Usage Data</div>
                                 <p class="f-s14 text-gray">We may also collect information how the Service is accessed and used (&quot;Usage Data&quot;). This
                                    Usage Data may include information such as your computer&#39;s Internet Protocol address (e.g.
                                    IP address), browser type, browser version, the pages of our Service that you visit, the time
                                    and date of your visit, the time spent on those pages, unique device identifiers and other
                                    diagnostic data.</p>
                                 <div class="f-s18 black font-w500 terms-use-heading">Location Data</div>
                                 <p class="f-s14 text-gray">We may use and store information about your location if you give us permission to do so
                                    (“Location Data”). We use this data to provide features of our Service, to improve and
                                    customize our Service.</p>
                                 <p class="f-s14 text-gray">You can enable or disable location services when you use our Service at any time, through
                                    your device settings.</p>

                                 <div class="f-s18 black font-w500 terms-use-heading">Tracking Cookies Data</div>
                                 <p class="f-s14 text-gray">We use cookies and similar tracking technologies to track the activity on our Service and
                                    hold certain information.</p>
                                 <p class="f-s14 text-gray">
                                    Cookies are files with small amount of data which may include an anonymous unique
                                    identifier. Cookies are sent to your browser from a website and stored on your device.
                                    Tracking technologies also used are beacons, tags, and scripts to collect and track
                                    information and to improve and analyze our Service.
                                 </p>
                                 <p class="f-s14 text-gray">
                                    You can instruct your browser to refuse all cookies or to indicate when a cookie is being
                                    sent. However, if you do not accept cookies, you may not be able to use some portions of our
                                    Service.
                                 </p>
                                 <p class="f-s14 text-gray"> Examples of Cookies we use:</p>
                                 <ul class="f-s14 text-gray">
                                    <li>Session Cookies. We use Session Cookies to operate our Service</li>
                                    <li>Preference Cookies. We use Preference Cookies to remember your preferences and
                                       various settings.</li>
                                    <li>Security Cookies. We use Security Cookies for security purposes.</li>
                                 </ul>
                                 <div class="f-s18 black font-w500 terms-use-heading">Use of Data</div>
                                 <p class="f-s14 text-gray">DistiMonster uses the collected data for various purposes:</p>
                                 <ul class="f-s14 text-gray">
                                    <li>To provide and maintain our Service</li>
                                    <li>To notify you about changes to our Service</li>
                                    <li>To allow you to participate in interactive features of our Service when you choose to do
                                       so</li>
                                    <li>To provide customer support</li>
                                    <li>To gather analysis or valuable information so that we can improve our Service</li>
                                    <li>To monitor the usage of our Service</li>
                                    <li>To detect, prevent and address technical issues</li>
                                    <li>To provide you with news, special offers and general information about other goods,
                                       services and events which we offer that are similar to those that you have already
                                       purchased or enquired about unless you have opted not to receive such information</li>
                                 </ul>
                                 <div class="f-s18 black font-w500 terms-use-heading">Transfer Of Data</div>
                                 <p class="f-s14 text-gray">Your information, including Personal Data, may be transferred to — and maintained on —
                                    computers located outside of your state, province, country or other governmental
                                    jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>
                                 <p class="f-s14 text-gray">If you are located outside United States and choose to provide information to us, please note
                                    that we transfer the data, including Personal Data, to United States and process it there.</p>
                                 <p class="f-s14 text-gray">Your consent to this Privacy Policy followed by your submission of such information
                                    represents your agreement to that transfer.</p>
                                 <p class="f-s14 text-gray">DistiMonster will take all steps reasonably necessary to ensure that your data is treated
                                    securely and in accordance with this Privacy Policy and no transfer of your Personal Data
                                    will take place to an organization or a country unless there are adequate controls in place
                                    including the security of your data and other personal information.</p>

                                 <div class="f-s18 black font-w500 terms-use-heading">Disclosure Of Data</div>

                                 <div class="f-s18 black font-w500 terms-use-heading">Business Transaction</div>
                                 <p class="f-s14 text-gray"> If DistiMonster is involved in a merger, acquisition or asset sale, your Personal Data may be
                                 transferred. We will provide notice before your Personal Data is transferred and becomes
                                    subject to a different Privacy Policy.</p>
                                 <div class="f-s18 black font-w500 terms-use-heading">Disclosure for Law Enforcement</div>
                                 <p class="f-s14 text-gray">Under certain circumstances, DistiMonster may be required to disclose your Personal Data
                                 if required to do so by law or in response to valid requests by public authorities (e.g. a court
                                    or a government agency).</p>
                                 <div class="f-s18 black font-w500 terms-use-heading">Legal Requirements</div>
                                 <p class="f-s14 text-gray"> DistiMonster may disclose your Personal Data in the good faith belief that such action is
                                    necessary to:</p>

                                 <ul class="f-s14 text-gray">
                                    <li>To comply with a legal obligation</li>
                                    <li>To protect and defend the rights or property of DistiMonster</li>
                                    <li>To prevent or investigate possible wrongdoing in connection with the Service</li>
                                    <li>To protect the personal safety of users of the Service or the public</li>
                                    <li>To protect against legal liability</li>
                                 </ul>


                                 <div class="f-s18 black font-w500 terms-use-heading">Security Of Data</div>
                                 <p class="f-s14 text-gray">The security of your data is important to us, but remember that no method of transmission
                                 over the Internet, or method of electronic storage is 100% secure. While we strive to use
                                 commercially acceptable means to protect your Personal Data, we cannot guarantee its
                                    absolute security.</p>

                                 <div class="f-s18 black font-w500 terms-use-heading"> &quot;Do Not Track&quot; Signals </div>
                                 <p class="f-s14 text-gray">We do not support Do Not Track (&quot;DNT&quot;). Do Not Track is a pref0erence you can set in your
                                    web browser to inform websites that you do not want to be tracked.</p>
                                 <p class="f-s14 text-gray">
                                    You can enable or disable Do Not Track by visiting the Preferences or Settings page of your
                                    web browser.
                                 </p>

                                 <div class="f-s18 black font-w500 terms-use-heading">Service Providers</div>
                                 <p class="f-s14 text-gray">We may employ third party companies and individuals to facilitate our Service (&quot;Service
                                 Providers&quot;), to provide the Service on our behalf, to perform Service-related services or to
                                    assist us in analyzing how our Service is used.</p>
                                 <p class="f-s14 text-gray">These third parties have access to your Personal Data only to perform these tasks on our
                                    behalf and are obligated not to disclose or use it for any other purpose.</p>

                                 <div class="f-s18 black font-w500 terms-use-heading">Behavioral Remarketing</div>
                                 <p class="f-s14 text-gray">DistiMonster uses remarketing services to advertise on third party websites to you after
                                 you visited our Service. We and our third-party vendors use cookies to inform, optimize and
                                    serve ads based on your past visits to our Service.</p>

                                 <div class="f-s18 black font-w500 terms-use-heading"> Google AdWords</div>
                                 <p class="f-s14 text-gray"> Google AdWords remarketing service is provided by Google Inc.
                                 You can opt-out of Google Analytics for Display Advertising and customize the
                                 Google Display Network ads by visiting the Google Ads Settings page:
                                 http://www.google.com/settings/ads
                                 Google also recommends installing the Google Analytics Opt-out Browser Add-on -
                                 https://tools.google.com/dlpage/gaoptout - for your web browser. Google Analytics
                                 Opt-out Browser Add-on provides visitors with the ability to prevent their data from
                                 being collected and used by Google Analytics.
                                 For more information on the privacy practices of Google, please visit the Google
                                    Privacy Terms web page: http://www.google.com/intl/en/policies/privacy/</p>
                                 <div class="f-s18 black font-w500 terms-use-heading">Twitter</div>
                                 <p class="f-s14 text-gray">Twitter remarketing service is provided by Twitter Inc.
                                 You can opt-out from Twitter&#39;s interest-based ads by following their instructions:
                                 https://support.twitter.com/articles/20170405
                                 You can learn more about the privacy practices and policies of Twitter by visiting
                                    their Privacy Policy page: https://twitter.com/privacy</p>





                                 <div class="f-s18 black font-w500 terms-use-heading"> Payments</div>
                                 <p class="f-s14 text-gray"> We may provide paid products and/or services within the Service. In that case, we use
                                    third-party services for payment processing (e.g. payment processors).</p>

                                 <p class="f-s14 text-gray">We will not store or collect your payment card details. That information is provided directly
                                 to our third-party payment processors whose use of your personal information is governed
                                 by their Privacy Policy. These payment processors adhere to the standards set by PCI-DSS
                                 as managed by the PCI Security Standards Council, which is a joint effort of brands like Visa,
                                 Mastercard, American Express and Discover. PCI-DSS requirements help ensure the secure
                                    handling of payment information.</p>

                                 <p class="f-s14 text-gray"> The payment processors we work with are:</p>
                                 <div class="f-s18 black font-w500 terms-use-heading"> PayPal or Braintree</div>
                                 <p class="f-s14 text-gray">Their Privacy Policy can be viewed at
                                    https://www.paypal.com/webapps/mpp/ua/privacy-full</p>

                                 <div class="f-s18 black font-w500 terms-use-heading"> Links To Other Sites</div>
                                 <p class="f-s14 text-gray">Our Service may contain links to other sites that are not operated by us. If you click on a
                                 third party link, you will be directed to that third party&#39;s site. We strongly advise you to
                                    review the Privacy Policy of every site you visit.</p>
                                 <p class="f-s14 text-gray">We have no control over and assume no responsibility for the content, privacy policies or
                                    practices of any third party sites or services.</p>


                                 <div class="f-s18 black font-w500 terms-use-heading">Children&#39;s Privacy</div>
                                 <p class="f-s14 text-gray">Our Service does not address anyone under the age of 18 (&quot;Children&quot;).</p>
                                 <p class="f-s14 text-gray">We do not knowingly collect personally identifiable information from anyone under the age
                                 of 18. If you are a parent or guardian and you are aware that your Children has provided us
                                 with Personal Data, please contact us. If we become aware that we have collected Personal
                                 Data from children without verification of parental consent, we take steps to remove that
                                    information from our servers.</p>



                                 <div class="f-s18 black font-w500 terms-use-heading">Changes To This Privacy Policy</div>
                                 <p class="f-s14 text-gray"> We may update our Privacy Policy from time to time. We will notify you of any changes by
                                    posting the new Privacy Policy on this page.</p>
                                 <p class="f-s14 text-gray"> We will let you know via email and/or a prominent notice on our Service, prior to the
                                    change becoming effective and update the &quot;effective date&quot; at the top of this Privacy Policy.</p>
                                 <p class="f-s14 text-gray">You are advised to review this Privacy Policy periodically for any changes. Changes to this
                                    Privacy Policy are effective when they are posted on this page.</p>


                                 <div class="f-s18 black font-w500 terms-use-heading">Contact Us</div>
                                 <p class="f-s14 text-gray">If you have any questions about this Privacy Policy, please contact us:</p>
                                 <p class="f-s14 text-gray"> By email: info@distimonster.com</p>










                              </div>
                           </div>
                        </div>
                        <!-- #tab3 End-->
                        <h3 class="tab_drawer_heading" rel="tab4">Terms and Conditions of Export</h3>
                        <div id="tab4" class="tab_content order-history contact-us text-left">
                           <h2 class="block-title left-block-title text-left">Terms and Conditions of Export</h2>
                            <div class="shdow-box edit-account-setting">
                              <div class="edit-account-setting-content f-s14 text-gray">

                                 <div class="f-s18 black font-w500 terms-use-heading"> A) Limits on use of the Product</div>
                                 <p class="f-s14 text-gray">The products sold by DistiMonster.com (“Seller”) are not intended for, and by ordering them you
                                    agree that they will not be used in, life support systems, human implantation, nuclear facilities or
                                    systems or any other application where product failure could lead to loss of life or catastrophic
                                    property damage. If you breach this agreement, you agree to indemnify Seller for and hold Seller
                                    harmless against any loss, cost or damage to Seller which arises or results from such breach.
                                 </p>
                                 <div class="f-s18 black font-w500 terms-use-heading"> B) Compliance with the law</div>
                                 <p class="f-s14 text-gray">You understand that the commodities, software and/or technology you purchase or receive from
                                    Seller may be subject to export, re-export, or other restrictions under the laws of the country of
                                    manufacture, the country of the seller/distributor, and the country in which you reside. Therefore,
                                    you, on behalf of yourself and your subsidiaries and affiliates (collectively, “Customer”), agree to
                                    abide by all applicable laws and regulations relating to the export and re-export of such commodities,
                                    software and/or technology and the direct products thereof in relation to goods obtained by
                                    Customer. In particular:</p>
                                 <ul class="f-s14 text-gray">

                                    <li> 1. Customer understands that, in accordance with 22 CFR 122 U.S. International Traffic in
                                    Arms Regulation (“ITAR”) , all manufacturers, exporters, and brokers of U.S.-origin defense
                                    articles, defense services, or related technical data, as defined on the U.S. Munitions List
                                    (USML), must register with the U.S. Department of State Directorate of Defense Trade
                                       Controls (DDTC).</li>
                                    <li>2. Customer understands that the U.S. origin commodities, software and/or technology
                                    exported from the U.S., and foreign manufactured products that incorporate more than de-
                                    minimis (i.e., 10%) U.S.-origin content are subject to U.S. re-export authorization under the
                                    U.S. Export Administration Regulations (“EAR”). In the event of re-export, Customer will
                                    ensure that all required permissions (i.e., export licenses, permits, etc., as applicable) will be
                                    obtained by the exporter. This includes export authorizations required for deemed exports
                                    (as defined in the EAR §734.2 and ITAR §120.17) to foreign persons. Further, Customer
                                    understands that any re-export of any U.S.-origin ITAR controlled item, or any re-export of a
                                    foreign-produced end-item that incorporates any U.S.-origin ITAR controlled component,
                                       requires re-export authorization from the U.S. Department of State DDTC.</li>
                                    <li>3. Customer certifies that the commodities, software and/or technology will not be used, sold,
                                    re-exported or incorporated into products used directly or indirectly in the design,
                                    development, production, stockpiling, or use of chemical or biological weapons, nuclear
                                    programs, missiles, and maritime nuclear propulsion projects except as authorized under
                                    applicable laws and regulations relating to the manufacture, export and/or re-export of these
                                       items.</li>
                                    <li>4. Customer certifies that the commodities, software and/or technology will not be used, sold,
                                    re-exported or incorporated into products for use by military, police or intelligence entities, for
                                    any space applications, or for use in foreign vessels or aircraft except as authorized under
                                       applicable laws and regulations relating to the manufacture, export and/or re-export of items.</li>
                                    <li>
                                       5. Customer certifies that the commodities, software and/or technology will not be used directly
                                       or indirectly, sold, re-exported or incorporated into products for the benefit of persons or
                                       entities named on any United States denied or restricted party list, including the Entity List at
                                       Part 744 of the Export Administration Regulations, persons designated by the U.S.
                                       government as Specially Designated Global Terrorists (SDGTs), Specially Designated Terrorists (SDTs), Foreign Terrorist Organizations (FTOs) on the Specially Designated
                                       National (SDN) list; or other applicable government denied or restricted party list.
                                    </li>
                                    <li>
                                       6. Customer certifies that the commodities, software and/or technology will not be exported or
                                       re-exported directly or indirectly, diverted or transshipped to or via any country in violation of
                                       any United Nations, United States, European Union or any other applicable embargo, nor
                                       shipped or moved to a Free Trade Zone/Area except as authorized under applicable laws
                                       and regulations.
                                    </li>
                                    <li>
                                       7. Customer certifies that it is not an embassy, agency or subdivision of, or otherwise affiliated
                                       with a non-U.S. government.
                                    </li>
                                    <li>
                                       8. Customer agrees that when requested by Seller it will provide Seller with an end use/user
                                       statement, in form and substance satisfactory with Seller, certifying as to the use of the
                                       applicable commodities, software and/or technology.
                                    </li>
                                    <li>
                                       9. Customer certifies that, if it is unable to fulfill the above certifications, or if any of the
                                       information provided by Customer in this document shall change at any time, it will advise
                                       Seller of the changes in writing before placing further orders. This certification shall survive
                                       the term and termination of the relationship between the Customer and Seller.
                                    </li>
                                 </ul>
                                 {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>--}}
                                 {{--<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore te feugait nulla facilisi.</p>--}}
                              </div>
                           </div>
                        </div>
                        <!-- #tab4 End-->
                        <h3 class="tab_drawer_heading" rel="tab4">Terms and Conditions of Export</h3>
                        <div id="tab5" class="tab_content order-history contact-us text-left">
                           <h2 class="block-title left-block-title text-left">Cookies Policies</h2>
                            <div class="shdow-box edit-account-setting">
                              <div class="edit-account-setting-content f-s14 text-gray">
                                 <p>DistiMonster takes your privacy seriously and we only use your personal information to administer your account and to provide the products and services you have requested from us.Our website uses cookies to improve the overall best experience we can to our customers. We also use cookies to analyze site usage for our own internal communications. Please refer to our Privacy and Cookie Policy for more information.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- .tab_container -->
                  </div>
                  <div class="shdow-box">
                  </div>
               </div>
            </div>
         </div>
      </section>
@stop
