@extends('frontend.layouts.app')
@section('title', '| News | '.$getNewsDetailsById['title'])
@section('content')

<!--START: Cart Page-->
<section class="gray-bg news-update-content">
 <div class="container clearfix">
  <div class="row clearfix">
   <div class="col-md-9">
    <h2 class="block-title">{{ $getNewsDetailsById['title'] }}</h2>
  </div>
</div>
<div class="shdow-box my-quote-detail news-detail">
    <div class="row">
      @if($getNewsDetailsById['image'] != '')
        <div class="col-md-4">
          <img width="100%" src="{{url('/uploads/'.$getNewsDetailsById['image'])}}" alt="News Logo" />
        </div>
          @else
            <div class="col-md-4">
                <img src="{{url('frontend/images/logo.png')}}" alt="News Logo" />
            </div>
        @endif
        <div class="col-md-8">
            <div class="news-update-content">
              <p><?php echo $getNewsDetailsById['description']; ?></p>
            </div>
        </div>
    </div>
</div>
</div>
</section>
@stop