@extends('frontend.layouts.app')
@section('title', '| News & Update')
@section('content')

<!--START: Cart Page-->
<section class="gray-bg news-update-content">
 <div class="container clearfix">
  <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active"><span>News and Updates</span></li>
                    </ul>
                </div>
            </div>
<div class="row">
 <div class="col-md-12">
  <div class="card-columns">
      @if(!$news->isEmpty())
           @foreach ($news as $new)
               <div @if($new->image) class="card" @else class="card p-0" @endif >
                 @if($new->image)
                  <a href="{{url('news/'.$new->id)}}" title="News Logo"><img height="260" width="375" src="{{url('/uploads/'.$new->image)}}" alt="News Logo" /></a>
                 @else
                    <a href="{{url('news/'.$new->id)}}" title="News Logo"><img  width="80%" src="{{url('frontend/images/logo.png')}}" alt="News Logo"/></a>
                 @endif
                 <div class="card-block">
                   <a href="{{url('news/'.$new->id)}}"><h4 class="card-title">{{ $new->title }}</h4></a>
                   <p class="card-text">{!!  implode(' ', array_slice(explode(' ', $new->description), 0, config('constants.NEWS_CHARACTERS')))  !!}</p>
                   @if(str_word_count($new->description) > config('constants.NEWS_CHARACTERS'))
                     <a href="{{url('news/'.$new->id)}}" class="read-more">Read More</a>
                  @endif
                 </div>
               </div>
           @endforeach

          @else
          <p>No News is published from {{ config('app.name') }}</p>
        @endif
 </div>
</div>
</div>

<div class="pagination-list text-right clearfix">
  {{ $news->appends(request()->except('page'))->links('frontend.partials.pagination') }}
</div>


</div>

</section>
@stop
