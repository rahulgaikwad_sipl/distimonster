@extends('frontend.layouts.app')
@section('title', '| My BIO')
@section('content')
    <section class="gray-bg top-pad-with-bradcrumb">
        <div class="container-fluid clearfix container-w-80 user-admin">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active"><span>My BIO</span></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
                     @include('frontend.partials.sidebar')
                     	<div class="tab_container">
                            <h3 class="tab_drawer_heading" rel="tab2">My BIO</h3>
                            <div id="tab2" class="tab_content">
                                <h2 class="block-title left-block-title">My BIO</h2>
                                <div class="shdow-box edit-account-setting">
                                    <div class="edit-account-setting-content">
                                        <div class="col-md-12">
                                            <h3>BIO Information</h3>
                                        </div>
                                        {!! Form::model($user, ['url' => url('my-bio/'), 'method' => 'POST','files' => true,'id' => 'my_bio_form', 'data-parsley-validate' => true]) !!}
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php $areYouA = config('constants.ARE_YOU_A');?>
                                                <select class="form-control" name="are_you_a" id="are_you_a" required="" data-parsley-required-message="{{config('constants.ARE_YOU_A_ERROR')}}">
                                                    <option value="">Select Are You A</option>
                                                    @foreach($areYouA as $key => $value)
                                                    <option value="{{$key}}" @if($user->are_you_a==$key) selected="selected" @endif>{{$value}}</option>
                                                    @endforeach
                                                </select>
                                                <p class="select1" @if($user->are_you_a!='Other')  style="display:none;" @endif><input type="text" name="are_you_a_text" id="are_you_a_text" class="form-control" value="{{$user->are_you_a_text}}" placeholder="Are you a" data-parsley-validate-if-empty data-parsley-conditionalvalue='["[name=\"are_you_a\"] option:selected", "Other"]' maxlength="100"/></p>
                                                <p class="help-block"></p>
                                                @if($errors->has('are_you_a'))
                                                    <p class="help-block">
                                                        {{ $errors->first('are_you_a') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php $yourCompanyA = config('constants.YOUR_COMPANY_A');?>
                                                <select class="form-control" name="your_company_a" id="your_company_a" required="" data-parsley-required-message="{{config('constants.YOUR_COMPANY_A_ERROR')}}">
                                                    <option value="">Select Your Company A</option>
                                                    @foreach($yourCompanyA as $key => $value)
                                                    <option value="{{$key}}" @if($user->your_company_a==$key) selected="selected" @endif>{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>    
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <p class="select2" @if($user->your_company_a!='Other')  style="display:none;" @endif><input type="text" name="your_company_a_text" id="your_company_a_text" class="form-control" value="{{$user->your_company_a_text}}" placeholder="Your company a" data-parsley-validate-if-empty data-parsley-conditionalvalue='["[name=\"your_company_a\"] option:selected", "Other"]' maxlength="100"/></p>
                                                <p class="help-block"></p>
                                                @if($errors->has('your_company_a'))
                                                    <p class="help-block">
                                                        {{ $errors->first('your_company_a') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php $yourMainIndustry = config('constants.YOUR_MAIN_INDUSTRY');?>
                                                <select class="form-control" name="your_main_industry" id="your_main_industry" required="" data-parsley-required-message="{{config('constants.YOUR_MAIN_INDUSTRY_ERROR')}}">
                                                    <option value="">Select Your Main Industry</option>
                                                    @foreach($yourMainIndustry as $key => $value)
                                                    <option value="{{$key}}" @if($user->your_main_industry==$key) selected="selected" @endif>{{$value}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>    
                                        <div class="form-group">
                                            <div class="col-md-12">    
                                                <p class="select3" @if($user->your_main_industry!='Other')  style="display:none;" @endif><input type="text" name="your_main_industry_text" id="your_main_industry_text" class="form-control" value="{{$user->your_main_industry_text}}" placeholder="What is your main Industry." data-parsley-validate-if-empty data-parsley-conditionalvalue='["[name=\"your_main_industry\"] option:selected", "Other"]' maxlength="100"/></p>
                                                <p class="help-block"></p>
                                                @if($errors->has('your_main_industry'))
                                                    <p class="help-block">
                                                        {{ $errors->first('your_main_industry') }}
                                                    </p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group m-t25">
                                            <div class="col-md-12">
                                                <input type="hidden" name="user_id" value="{{$user->id}}"/>
                                                {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                        <!-- /form -->
                                    </div>
                                </div>
                            </div>
                            <!-- #tab2 End-->
                        </div>
                        <!-- .tab_container -->
                    </div>
                    <div class="shdow-box">
                    </div>
                </div>
            </div>
        </div>
    </section>

@stop
@section('javascript')
<script>
window.ParsleyValidator.addValidator('conditionalvalue', function (value, requirements) {
        if (requirements[1] == $(requirements[0]).val() && '' == value)
            return false;
        return true;
    }, 32).addMessage('en', 'conditionalvalue', 'This field is required.');

$('#my_bio_form').parsley();
$(document).ready(function(){
    /*Are you a*/
    $("#are_you_a").change(function(){
        var areYouA = $(this).val();
        if(areYouA=="Other"){
            $(".select1").show();
        }else{
            $(".select1").hide();
        }
    });
    
    /*Your company a*/
    $("#your_company_a").change(function(){
        var areYouA = $(this).val();
        if(areYouA=="Other"){
            $(".select2").show();
        }else{
            $(".select2").hide();
        }
    });
    
    /*Your main industry*/
    $("#your_main_industry").change(function(){
        var areYouA = $(this).val();
        if(areYouA=="Other"){
            $(".select3").show();
        }else{
            $(".select3").hide();
        }
    });
});
</script>
@endsection