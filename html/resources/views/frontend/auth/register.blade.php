@extends('frontend.layouts.app')
@section('title', '| Register')
@section('content')
    <!--Register Section-->
    <section class="gray-bg p-b75">
        <div class="container clearfix">
            <div class="col-md-12">
                <div class="shdow-box">
                    <div class="register-box-content">
                        <div class="row">
                            <div class="col-md-6 bdr-right">
                                <h2 class="block-title"><span class="img-icon">
                                    <!-- <img src="{{url('frontend/assets/images/create-account-icon.png')}}" class="img-responsive"> -->
                                    <i class="fa fa-user-circle-o" aria-hidden="true"></i> </span>Create Account</h2>
                                {!! Form::open(['method' => 'POST', 'class'=>'form-horizontal' ,'id'=>'register_form', 'data-parsley-validate','url' => 'register-user',]) !!}
                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        {!! Form::text('first_name', old('first_name'), ['class' => 'form-control', 'placeholder' => 'First Name*', 'data-parsley-pattern'=>config('app.patterns.name'),'data-parsley-required',  'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE') ,'data-parsley-minlength' => config('app.fields_length.name_min'),'data-parsley-maxlength' => config('app.fields_length.name_max'),'data-parsley-minlength-message' => $validationMessage['first_name.min'],'data-parsley-maxlength-message' => $validationMessage['first_name.max']]) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('first_name'))
                                            <p class="help-block">
                                                {{ $errors->first('first_name') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => 'Last Name*', 'data-parsley-pattern'=>config('app.patterns.name'),'data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.name_min'),'data-parsley-maxlength' => config('app.fields_length.name_max'),'data-parsley-minlength-message' => $validationMessage['last_name.min'],'data-parsley-maxlength-message' => $validationMessage['last_name.max']]) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('last_name'))
                                            <p class="help-block">
                                                {{ $errors->first('last_name') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email Address*','data-parsley-required','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'pattern' => config('app.patterns.email'),"data-parsley-trigger" => "focusout", "data-parsley-remote" => url('check-newemail'),'data-parsley-remote-message'=>$validationMessage['email.unique']]) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('email'))
                                            <p class="help-block">
                                                {{ $errors->first('email') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        {!! Form::text('contact_number', old('contact_number'), ['class' => 'form-control', 'placeholder' => 'Contact Number*','data-parsley-required', 'data-parsley-required data-parsley-pattern'=>'^[\d\+\-\.\(\)\/\s]*$', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'), 'data-parsley-minlength' => config('app.fields_length.contact_number_min'), 'data-parsley-maxlength'=> config('app.fields_length.contact_number_max'),'data-parsley-minlength-message'=>config('constants.CONTACT_NUMBER_MIN_MESSAGE') ,'data-parsley-maxlength-message' =>config('constants.CONTACT_NUMBER_MAX_MESSAGE')]) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('contact_number'))
                                            <p class="help-block">
                                                {{ $errors->first('contact_number') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        {!! Form::password('password', ['id'=>'password','class' => 'form-control', 'placeholder' => 'Password*','data-parsley-required','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.password_min'),'data-parsley-maxlength' => config('app.fields_length.password_max'),'data-parsley-minlength-message' => $validationMessage['password.min'],'data-parsley-maxlength-message' => $validationMessage['password.max']]) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('password'))
                                            <p class="help-block">
                                                {{ $errors->first('password') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        {!! Form::password('cpassword', ['class' => 'form-control', 'placeholder' => 'Confirm Password*','data-parsley-required','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-equalto-message'=>'Please enter same as password.','data-parsley-equalto'=>"#password" ]) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('cpassword'))
                                            <p class="help-block">
                                                {{ $errors->first('cpassword') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 p-l0 checkbox-rember">
                                        <div class="checkbox-sign-up checkbox p-t10 ">
                                        {{ Form::checkbox('subscribe_newsletter', 1, null, ['class' => ' ']) }}
                                        {!! Form::label('subscribe_newsletter', trans('quickadmin.register.fields.newsletter_check'), ['class' => 'control-label m-b0']) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('subscribe_newsletter'))
                                            <p class="help-block">
                                                {{ $errors->first('subscribe_newsletter') }}
                                            </p>
                                        @endif
                                    </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-md-11 p-l0 ">
                                        <div class="regis-caption">
                                            <p>By creating an account, you agree to DistiMonster <a target="_blank" href="{{url('terms-of-use')}}">Privacy Policy and Terms of Use</a>.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        {!! Form::submit('Create Account', ['class' => 'btn btn-primary','title'=>'Create Account']) !!}
                                    </div>
                                </div>
                            {!! Form::close() !!}
                            <!-- /form -->
                            </div>
                            <div class="col-md-6">
                                <div class="left-pad-content">
                                    <h2 class="block-title"><span class="img-icon"><i class="fa fa-sign-in" aria-hidden="true"></i></span> Already have an account?</h2>
                                    <p>If you already have an account with us, just click “Login” below, and we’ll take you to your account in no time. Simply you need to enter your registered email address and password for accessing account.</p>
                                    <div class="form-group">
                                        <div class="col-md-11 p-l0">
                                            <a  href="{{ url('login')}}" class="btn btn-outline btn-primary" title="Login">Login</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Register Section-->
@stop
@section('javascript')
    <script>
        $('#register_form').parsley();
    </script>
@endsection
