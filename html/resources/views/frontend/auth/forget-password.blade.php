@extends('frontend.layouts.app')
@section('title', '| Forgot Password')
@section('content')

<!--Register Section-->
<section class="gray-bg">
 <div class="container clearfix">
    <div class="col-md-12">
       <div class="shdow-box">
          <div class="register-box-content login-box-content">
             <div class="row">
                <div class="col-md-6 bdr-right">
                   <h2 class="block-title"><span class="img-icon"><img src="{{url('frontend/assets/images/login-icon.png')}}" class="img-responsive"></span>@lang('quickadmin.forgot-password.top_heading')</h2>
                   {!! Form::open(['method' => 'POST', 'id'=>'forgotpassword_form', 'data-parsley-validate','url' => 'forget-password','class' => 'form-horizontal']) !!}
                   @lang('quickadmin.forgot-password.get_link_message')

                   <div class="form-group m-t15">
                       <div class="col-md-11 p-l0">
                           {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email*','data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'pattern' => config('app.patterns.email')]) !!}
                           <p class="help-block"></p>
                           @if($errors->has('email'))
                           <p class="help-block">
                            {{ $errors->first('email') }}
                        </p>
                        @endif
                    </div>
                </div>

                {!! Form::submit('Reset My Password', ['class' => 'btn btn-primary m-t15','title'=>'Reset My Password']) !!}
                {!! Form::close() !!}


                <!-- /form -->
            </div>
            <div class="col-md-6">
               <div class="left-pad-content">
                                    <h2 class="block-title">Already have an account?</h2>
                   <p>If you already have an account with us, just click “Login” below, and we’ll take you to your account in no time. Simply you need to enter your registered email address and password for accessing account.</p>
                                    <div class="form-group">
                                        <div class="col-md-11 p-l0">
                                            <a  href="{{ url('login')}}" class="btn btn-outline btn-primary" title="Login">Login</a>
                                        </div>
                                    </div>
                                </div>
       </div>
   </div>
</div>
</div>
</div>
</div>
</section>
<!--Register Section-->







@stop
@section('javascript')
<script>
    $('#forgotpassword_form').parsley();
</script>
@endsection
