@extends('frontend.layouts.app')
@section('title', '| Login')
@section('content')


    <!--Register Section-->
    <section class="gray-bg">
        <div class="container clearfix">
            @if(session()->has('registration_success'))
                <div class="col-md-12 registration_success_message">
                    {!! session('registration_success') !!}
                </div>
            @endif
            <div class="col-md-12">
                <div class="shdow-box">
                    <div class="register-box-content login-box-content">
                        <div class="row">
                            <div class="col-md-6 bdr-right">
                                <h2 class="block-title"><span class="img-icon"><img src="{{url('frontend/assets/images/login-icon.png')}}" class="img-responsive"></span>Login</h2>
                                {!! Form::open(['method' => 'POST', 'id'=>'login_form', 'data-parsley-validate','url' => 'login','class'=>'form-horizontal']) !!}
                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        {!! Form::email('email', (isset($_COOKIE['distim_uid']) && $_COOKIE['distim_uid'] != '') ? Crypt::decrypt($_COOKIE['distim_uid']): '',['class' => 'form-control', 'placeholder' => 'Email Address*','data-parsley-required','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'pattern' => config('app.patterns.email')]) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('email'))
                                            <p class="help-block">
                                                {{ $errors->first('email') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        {!! Form::password('password',['id'=>'password','class' => 'form-control', 'placeholder' => 'Password*','data-parsley-required','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.password_min'),'data-parsley-maxlength' => config('app.fields_length.password_max')]) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('password'))
                                            <p class="help-block">
                                                {{ $errors->first('password') }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 p-l0 checkbox-rember f-s14">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <div class="input-group">
                                                    <div class="checkbox">
                                                        <label class="forgot">
                                                            {{ Form::checkbox('remember_me', 1, null, ['id'=>'login-remember',(isset($_COOKIE['distim_uid']) && $_COOKIE['distim_uid'] != '') ? "checked": '']) }} Remember me</label>
                                                            <div class="help-block"></div>
                                                            @if($errors->has('remember_me'))
                                                                <div class="help-block">
                                                                    {{ $errors->first('remember_me') }}
                                                                </div>
                                                            @endif

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5 text-right">
                                                <a href="{{ URL::to('forget-password')}}" class="forgot">Forgot Password?</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-md-11 p-l0 ">
                                        <div class="regis-caption login-caption">
                                            <p>By logging in you agree to DistiMonster <a target="_blank" href="{{url('terms-of-use')}}">Privacy Policy and Terms of Use</a>.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        <input type="hidden" name="redirect_uri" value="{{$redirectLogin}}"/>
                                        {!! Form::submit('Login', ['class' => 'btn btn-primary','title'=>'Login']) !!}
                                    </div>
                                </div>
                            {!! Form::close() !!}
                                <!-- /form -->
                            </div>
                            <div class="col-md-6">
                                <div class="left-pad-content">
                                    <h2 class="block-title">Don't have an account?</h2>
                                    <p>Register for a DistiMonster account and get a 30 day FREE TRIAL. DistiMonster is your single source to search and buy parts. Use one PO to purchase all your components from multiple distributers. You asked for simple—that’s DistiMonster.</p>
                                    <p>
                                        Want more information? We would be happy to schedule a DistiMonster demonstration for you or your company. Contact us at 818-857-5788 or submit a Request for <a href="{{url('request-a-demo')}}">Demo</a>.
                                    </p>
                                    <div class="form-group">
                                        <div class="col-md-11 p-l0">
                                            <a href="{{ url('subscription-plan')}}" class="btn btn-success free-trial" title="REGISTER & START YOUR FREE TRIAL">REGISTER & START YOUR FREE TRIAL</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Register Section-->

@stop
@section('javascript')
<script>
    $('#register_form').parsley();
</script>
@endsection
