@foreach ($parts as $k=>$row)
    <li>
	        <div class="shdow-box my-quote-detail">
	            <div class="hind-font">
	            	MPN:
	            	<a class="f-s18" href="{{url('search-part/'.$categorySlug.'/'.base64_encode($row->manufacturer_part_number))}}">{{$row->manufacturer_part_number}}</a>
	            </div>
				<div class="block m-t5 m-b10">
				    Manufacturer: <span class="black font-w500"> {{$row->manufacturer_name}} </span>
				</div>
				<div class="block m-t5 m-b10">
					Description:<br/><span class="black font-w500">{{$row->part_description ? $row->part_description:'N/A'}}</span>
				</div>
				<div class="block m-t10 m-b10">
					@if($row->datasheet_url != '')
				    <a target="_blank" href="{{$row->datasheet_url }}" class="data-sheet"><img src="{{url('/frontend/assets/images/pdf-icon.png')}}" width="12px"> Datasheet</a>
					@endif
				</div>
				<div class="block m-t10 m-b10">
					<a href="{{url('search-part/'.$categorySlug.'/'.base64_encode($row->manufacturer_part_number))}}"> Search Price</a>
				</div>
					           
	        </div>
    	</li>
@endforeach
