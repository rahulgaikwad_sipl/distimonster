@extends('frontend.layouts.app')
@section('title', '| Category - '.$category)
@section('content')
    <section class="gray-bg shipping-cart categories-details-page">
        <div class="container clearfix ">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('/categories')}}">Categories</a></li>
                        <li class="breadcrumb-item active"><span>Products</span></li>
                    </ul>
                </div>
            </div>
                <div class="row clearfix">
                <div class="col-md-7">
                    <h2 class="block-title" id="label_name">{{$category}}</h2>
                </div>
            </div>
            <div class="row aboutus clearfix">
                <div class="col-md-12">
                    <div class="shdow-box ">
                        <div class="search-filter-input">
                             <input type="hidden" id="category_name" value="{{$categorySlug}}" >
                            <div style="display: none" id="part_search_div" >
                                <div class="search-filter-left">
                                    <input  class="form-control"  type="text" id="part_search_box" required  minlength="2" placeholder="Search by part number here" title="Search by part number here">
                                    <a onclick="searchPartList()" class="search-btn"> Search </a>
                                </div>
                                <button class="btn btn-primary pull-right m-t9" onclick="clearPartSearch()"> Reset </button>
                            </div>

                              <!--  <div class="col-md-4">

                                    <select class="form-control m-bot15" id="category_name">
                                            @foreach($categoryFilter as $cat)
                                                <option {{ $category == $cat['name'] ? 'selected="selected"' : '' }} value="{{$cat['slug']}}">{{$cat['name']}}</option>
                                            @endForeach
                                    </select>
                                </div>-->    
                        </div>
                        <ul id="part-container" class="pull-left categories-left"></ul>
                        <div style="display: none" id="bottom-loader">

                        </div>
                
                    </div>
                </div>
            </div>
        </div>
    
    </section>
@stop
@section('javascript')
    <script src="{{ asset('frontend/js/jquery-listnav.min.js') }}"></script>
    <script src="{{ asset('frontend/js/category-part-search.js') }}"></script>
    <script>
        $(document).ready(function () {
                setTimeout(function () {
                    getBoxData();
                }, 100);
        });

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
                if (isAjaxRequest == true) {
                    getBoxData();
                }
            }
        });
    </script>
@endsection

