@extends('frontend.layouts.app')
@section('title', '| Products')

@section('content')
    <section class="gray-bg shipping-cart categories-page">
        <div class="container clearfix ">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active"><span>Products</span></li>
                    </ul>
                </div>
            </div>
            {{--<div class="row clearfix">--}}
               {{--<div class="col-md-7">--}}
                  {{--<h2 class="block-title">Categories</h2>--}}
               {{--</div>--}}
            {{--</div>--}}

            <?php //echo "<pre>";print_r($catergories);?>
                <div class="row clearfix">
                    <div class="col-md-7">
                        <h2 class="block-title">Products</h2>
                    </div>
                </div>
                <div class="row aboutus clearfix">
                    <div class="col-md-12">
                        <div class="shdow-box ">
                            <div class="search-filter-input">
                                <input class="form-control" type="text" id="myInput" onkeyup="searchCategory()" placeholder="Search by category name here..">
                            </div>
                            <ul id="myList" class="categories-left">
                                @foreach($catergories  as $item)
                                    <li><a href="{{url('category/'.$item['slug'])}}">{{ $item['name'] }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
    </section>
@stop
@section('javascript')
    <script src="{{ asset('frontend/js/jquery-listnav.min.js') }}"></script>
    <script src="{{ asset('frontend/js/category-part-search.js') }}"></script>

@endsection
