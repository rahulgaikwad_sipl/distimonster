@extends('frontend.layouts.app')
@section('title', '|  Shipping ')
@section('content')
    <section class="gray-bg shipping-cart shipping-addres">
        <div class="container clearfix">
            <!---Wizard Step-->
            <div class="row wizard-step">
                <div class="col-md-12">
                    <ul class="text-center">
                        <li class="active">
                            <a href="javascript:void(0)">
                                <span class="circle"><i class="fa fa-check"></i></span>
                                <span>Shipping</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"  onclick="checkOutModule.goToPaymentReview()">
                                <span class="circle">2</span>
                                <span>Review & Payment</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!---Wizard Step-->
            <hr/>
            <div class="row clearfix">
                <div class="col-md-9">
                    <h2 class="block-title left-block-title">Shipping Address</h2>
                    <?php
                     if(count($addresses) > 0){
                         $addressLength = count( $addresses ) - 1;
                         $defaultSelectedAddressId =   $addresses[$addressLength]->id;
                     }else{
                         $defaultSelectedAddressId  =  '';
                     }
                    ?>
                </div>
            </div>
            <div id="shippingAddrError"> </div>
            {!! Form::open(['url' => url('review-order'), 'autocomplete'=>'off', 'method' => 'POST', 'id' => 'shipping_form','data-parsley-validate'=>""]) !!}

            <input  value="{{ Session::get('selectedAddressId')?Session::get('selectedAddressId') :$defaultSelectedAddressId}}" type="hidden"  data-parsley-errors-container="#shippingAddrError" name="shipping_address" id="shipping-address" >
            <div class="row">

                <div class="col-md-8">
                    <!--Shiiping Address Listing Content-->
                    @foreach($addresses as $key =>$address)
                        <div @if(Session::get('selectedAddressId') == $address->id ) class="shdow-box remove-selected-class list-check-active" @else   @if( empty(Session::get('selectedAddressId')) &&  $key == ( count( $addresses ) - 1 ) ) class="shdow-box remove-selected-class list-check-active " @else class="shdow-box remove-selected-class" @endif  @endif   id="select_address_box_{{$address->id}}">
                            <a class="remove-check" @if(Session::get('selectedAddressId') == $address->id ) style="display: block;" @else  @if(  empty(Session::get('selectedAddressId')) && $key == ( count( $addresses ) - 1 ) )  style="display: block";  @else  style="display: none;"   @endif @endif id="check_{{$address->id}}" href="javascript:void(0)">
                                <span class="circle"><i class="fa fa-check"></i></span>
                            </a>
                            <div class="shipping-address-content-second ">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="address-detail-list f-s14 text-gray">
                                            <p>{{$address->first_name}} {{$address->last_name}}</p>
                                            <p>{{$address->address_line1}}</p>
                                            @if(!empty($address->address_line2))
                                                <p>{{$address->address_line2}}</p>
                                            @endif
                                            <p>{{$address->city_name}},   {{$address->state_name}}  {{$address->country_name}}, {{$address->zip_code}}</p>
                                            <p>{{$address->contact_number}}</p>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="right-block-link text-right">
                                            <a onclick="checkOutModule.selectShippingAddress('<?php echo "select_address_box_".$address->id;?>','<?php echo "check_".$address->id;?>','<?php echo $address->id;?>')" href="javascript:void(0)" class="f-s16 font-w500 m-r10"><span class="img-icon"><img src="{{url('frontend/assets/images/shipping-here-icon.png')}}" alt="shipping-here-icon" /></span> Ship Here</a>

                                            <a href="{{url('update-shipping-address/'.$address->id)}}" class="f-s16 hind-font font-w500 m-r10"><span class="img-icon"><img src="{{url('frontend/assets/images/edit-thine-icon.png')}}" alt="edit-icon" /></span> Edit</a>
                                            <a  href="javascript:void(0)" class="f-s16 hind-font" onclick="checkOutModule.removeShippingAddress('<?php echo $address->id;?>')"><span class="img-icon"><i class="fa fa-trash"></i></span> Remove</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                @endforeach
                    <div class="add-new-address"><a href="{{url('add-shipping-address')}}" class="f-s16 font-w500 hind-font"><span class="img-icon"><img src="{{url('frontend/assets/images/add-round-icon.png')}}" alt="Add New Address" /></span> Add New Address</a></div>
                    <!--Shiiping Address Listing Content-->
                    <div class="shdow-box">
                        <!--Shipping Addres-->
                        <div class="shipping-address-content-second">
                            <div class="row clearfix">
                                <div class="col-md-12">
                                    <h2 class="block-title">Shipping Options</h2>
                                </div>
                                <div class="form-check form-check-inline f-s14 text-gray m-b15">
                                    <label class="shipping-heading">Shipping costs are to be calculated & added during product delivery</label>
                                </div>
                                @foreach($shippingMethods as $shippingMethod)
                                    <input type="hidden" name="shipping_method" id="shipping_method" value="{{$shippingMethod->id}}">
                                    {{--<div class="col-md-12 shipping-method shipping-costs">
                                        <div class="form-check form-check-inline f-s14 text-gray">
                                            <input  onclick ="checkOutModule.showShippingMethodType()" class="form-check-input" type="checkbox" checked="checked" data-parsley-required-message="Please check the box to confirm shipping cost price calculation during delivery." data-parsley-errors-container="#optionsError" data-parsley-group="first" data-parsley-required  name="shipping_method" id="shipping_method" value="{{$shippingMethod->id}}">
                                                <input  onclick ="checkOutModule.showShippingMethodType()" class="form-check-input" type="checkbox"  @if(Session::get('selectedShippingMethodId') == $shippingMethod->id)  checked="checked" @else @endif data-parsley-required-message="Please check the box to confirm shipping cost price calculation during delivery." data-parsley-errors-container="#optionsError" data-parsley-group="first" data-parsley-required  name="shipping_method" id="shipping_method" value="{{$shippingMethod->id}}">
                                                <input @if(Session::get('selectedShippingMethodId') == $shippingMethod->id )  checked="checked" @else @endif data-parsley-required-message="Please select at least one shipping method" data-parsley-errors-container="#optionsError" data-parsley-group="first" data-parsley-required class="form-check-input" type="radio" name="shipping_method" id="shipping_method_{{$shippingMethod->id}}" value="{{$shippingMethod->id}}">

                                                <label class="form-check-label">${{$shippingMethod->shipping_charges}}</label>
                                            </div>
                                        <div class="form-check form-check-inline f-s14 text-gray m-b10">
                                            <!-- <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2"> -->
                                            {{--<label for ="shipping_method" class="form-check-label check-label">{{$shippingMethod->shipping_method_name}}</label>
                                        </div>
                                    </div>--}}
                                    @endforeach
                                <div id="optionsError"> </div>
                            </div>
                            <div class="row">
						
                                <div class="col-md-12 shdow-box p-b10 p-t10 m-t25" id="shipping_method_type">
                                {{--<div class="col-md-12 shdow-box p-b10 p-t10 m-t25" id="shipping_method_type" @if(Session::get('selectedShippingMethodId') == $shippingMethod->id) style="display: block" @else style="display: none" @endif >--}}
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label>Shipping:</label>
                                            {!! Form::select('shipping_option_id', Helpers::getShippingOption(),Session::get('selectedShippingMethodId'), ['id' => 'shipping_option_id', 'name'=>'shipping_method_type_input','data-parsley-required', 'data-parsley-required-message'=>'Please select a Shipping.','class'=>'form-control']) !!}
											@if ($errors->has('shipping_option_id'))
											    <span class="help-block">
											        <strong>{{ $errors->first('shipping_option_id') }}</strong>
											    </span>
											@endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6 p-b10 p-t10 m-t25">
                                            <label> Account Number: </label>
                                            {!! Form::text('shipping_account_number', old('shipping_account_number'), ['class' => 'bor-radius-none form-control quantity-box', 'placeholder' => 'Account Number','data-parsley-maxlength' => 100]) !!}
											 <p><em style="font-size: 0.7125rem!important;">by supplying your account number you accept all shipping charges made on your behalf. Any shipping charges that are rejected by your company will be re-billed to you directly from DistiMonster along with a penalty fee of $50.00 and calculated interest charges if the bill is not paid within 30 days</em></p>
                                        </div>
                                        <div class="col-md-6  p-b10 p-t10 m-t25">
                                            <label> Requested Delivery Date: </label>
                                            {!! Form::text('requested_delivery_date', old('requested_delivery_date'), ['readonly','id'=>'requested_delivery_date','class' => 'bor-radius-none form-control quantity-box requested-delivery-date', 'placeholder' => 'Requested Delivery Date']) !!}
                                        </div>
                                        </div>
                                    <div class="row">
                                        <div class="col-md-6  p-b10 p-t10 m-t25">
                                            <label> Note:</label>
                                            {!! Form::textarea('shipping_notes', null, ['data-parsley-maxlength'=>255,'rows'=>'3' ,'cols'=>'5' ,'id' => 'shipping_notes','placeholder'=>'Note(Max. 255 characters are allowed)*', 'class' => 'form-control']) !!}
                                        </div>
                                        <div class="col-md-6 p-b10 p-t10 m-t25">
                                            <label> Purchase Order Number: </label>
                                            {!! Form::text('shipping_purchase_order_number', old('shipping_purchase_order_number'), ['class' => 'bor-radius-none form-control quantity-box', 'placeholder' => 'Purchase Order Number','data-parsley-maxlength' => 20]) !!}
										</div> 
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6  p-b10 p-t10 m-t25">
                                            <label>1 Shipment Option:</label>
                                            <select name="consolidated_shipping_charge" id="consolidated_shipping_charge" class="form-control quantity-box">
                                                <option value="no">No</option>
                                                <option value="yes">Yes</option>
                                            </select>
                                            @if ($errors->has('consolidated_shipping_charge'))
											    <span class="help-block">
											        <strong>{{ $errors->first('consolidated_shipping_charge') }}</strong>
											    </span>
											@endif
                                            <p><em style="font-size: 0.7125rem!important;">
                                                if you require all items to be shipped in 1 shipment please select yes. There is a fee added for this service of ${{ number_format( config('constants.CONSOLIDATED_SHIPPING_CHARGE'),2, '.', ',')}} please note this could delay the delivery as we will have all product shipped to one location and then packed into one shipment.
                                    </em>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!--Shipping Addres-->
                    </div>
                    <!--Shiiping Address Listing Content-->
                    </div>

                <div class="col-md-4">
                    <div class="shdow-box shipping-right-content m-t0">
                        <div class="price-content-box">
                            <div class="row price-box-heading">
                                <div class="col-md-10">
                                    <h2 class="block-title">Order Summary</h2>
                                </div>
                                <div class="col-md-2">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="price-content-body">
                                <div class="f-s14 font-w500 item-cart-count">{{Cart::instance('shoppingcart')->content()->count()}} Items in cart</div>
                                <ul class="scroll">
                                    <?php foreach(Cart::instance('shoppingcart')->content() as $row) { ?>
                                    <li class="clearfix ">
                                        <div class="row shipping-address-right-summary">
                                            <div class="col-md-6">
                                                <div><a href="javascript:void(0)" class="hind-font">{{$row->name}}</a></div>
                                                <div class="f-s12">{{$row->options->has('manufacturer') ? $row->options->manufacturer : ''}}</div>
                                                <div class="f-s12 text-gray quantity">Quantity: {{ isset($row->qty) ? $row->qty : 'N/A' }}</div>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <div class="black f-s16">${{number_format($row->price*$row->qty, 2, '.', ',') }}</div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <div class="price-content-footer">
                                <button type="submit" class="btn btn-primary" title="Proceed to Checkout">Proceed to Checkout</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </section>
@stop