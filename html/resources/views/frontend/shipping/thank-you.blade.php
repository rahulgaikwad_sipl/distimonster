@extends('frontend.layouts.app')
@section('title', '|  Thank You')
@section('content')
    <section class="gray-bg shipping-cart shipping-addres complete-order">
        <div class="container clearfix">

            <div class="row clearfix text-center">
                <div class="col-md-12">
                    <div class="complete-big-icon"><img src="{{url('frontend/assets/images/complete-big-icon.png')}}" alt="complete-big-icon" ></div>
                    <h2 class="block-title left-block-title">Thank you for the order</h2>
                </div>
                <div class="col-md-12">
                    <p class="f-s14">Your order number is {{$orderId}}.</p>
                    @if(!empty($txNumber) && $txNumber!== 'cod')
                    <p class="f-s14">Your Payment Transaction number is <b>{{$txNumber}}</b>.</p>
                    @endif
                    <p class="f-s14">We’ve sent a confirmation email with order details and a link to track it’s progress.</p>
                    <a href="{{url('/')}}" class="btn btn-primary m-t15" title="Continue Shopping">Continue Shopping</a>
                </div>
            </div>

        </div>
    </section>
@stop

@section('javascript')
    <script>
        $(document).on('ready', function() {
            window.history.pushState(null, "", window.location.href);
            window.onpopstate = function() {
                window.history.pushState(null, "", window.location.href);
            };
        });
    </script>
@endsection