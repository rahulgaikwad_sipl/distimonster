@extends('frontend.layouts.app')
<?php //echo "<pre>"; print_r($address); die("====");
//if($address->count() > 0){
if(!empty($address)){ 
    $action = 'Update';
}else{
    $action = 'Add';
}

?>


@section('title', '| '.$action.' Shipping Address')
@section('content')
    <!--Address Listing Section-->
    <section class="gray-bg shipping-cart shipping-addres">
        <div class="container clearfix">
            <!---Wizard Step-->
            <div class="row wizard-step">
                <div class="col-md-7 offset-md-3 text-center">
                    <ul>
                        <li class="active">
                            <a href="javascript:void(0)">
                                <span class="circle"><i class="fa fa-check"></i></span>
                                <span>Shipping</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0)">
                                <span class="circle">2</span>
                                <span>Review & Payment</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!---Wizard Step-->
            <hr/>
            <div class="row clearfix">
                <div class="col-md-9">
                    <h2 class="block-title left-block-title">Shipping Address</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <!--Shiiping Listing Content-->
                    <div class="shdow-box">
                        <!--Shipping Addres-->

                        <div class="shipping-address-content">
                            @if(!empty($address))
                                {!! Form::model($address, ['url' => url('update-shipping-address/'.$address->id), 'method' => 'POST', 'id' => 'shipping_address_form', 'data-parsley-validate' => true]) !!}
                            @else
                                {!! Form::open(['url' => url('add-shipping-address'), 'class'=>'form-horizontal','method' => 'POST', 'id' => 'shipping_address_form', 'files' => true, 'data-parsley-validate' => true]) !!}
                            @endif

                            <div class="form-group">
                                <div class="col-md-12 p-l0">
                                    {!! Form::text('first_name', !empty($user->name)? $user->name:old('first_name'), ['id' => 'first_name', 'placeholder'=>'First Name*' ,'class' => 'form-control', 'data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'), 'data-parsley-minlength'=> config('app.fields_length.name_min'),'data-parsley-maxlength' => config('app.fields_length.name_max'),'data-parsley-minlength-message' => $validationMessage['name.min'],'data-parsley-maxlength-message' => $validationMessage['name.max']]) !!}
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 p-l0">
                                    {!! Form::text('last_name', !empty($user->last_name)? $user->last_name:old('last_name'), ['id' => 'last_name', 'placeholder'=>'Last Name*', 'class' => 'form-control', 'data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.name_min'),'data-parsley-maxlength' => config('app.fields_length.name_max'),'data-parsley-minlength-message' => $validationMessage['last_name.min'],'data-parsley-maxlength-message' => $validationMessage['last_name.max']]) !!}
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 p-l0">
                                    {!! Form::text('company_name', !empty($user->company_name)? $user->company_name:old('company_name'), ['id' => 'company_name', 'placeholder'=>'Company*', 'class' => 'form-control', 'data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.company_name_min'),'data-parsley-maxlength' => config('app.fields_length.company_name_max'),'data-parsley-minlength-message' => $validationMessage['company_name.min'],'data-parsley-maxlength-message' => $validationMessage['company_name.max']]) !!}
                                    @if ($errors->has('company_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('company_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 p-l0">
                                    {!! Form::text('contact_number', !empty($user->contact_number)? $user->contact_number:old('contact_number'), ['id' => 'contact_number', 'placeholder'=>'Phone Number*', 'class' => 'form-control', 'data-parsley-required', 'data-parsley-pattern'=>'^[\d\+\-\.\(\)\/\s]*$' , 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.contact_number_min'),'data-parsley-maxlength' => config('app.fields_length.contact_number_max'),'data-parsley-minlength-message' => $validationMessage['contact_number.min'],'data-parsley-maxlength-message' => $validationMessage['contact_number.max']]) !!}
                                    @if ($errors->has('contact_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('contact_number') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                                <div class="form-group">
                                    <div class="col-md-12 p-l0">
                                        {!! Form::text('address_line1', !empty($user->billing_address_line1)? $user->billing_address_line1:old('billing_address_line1'), ['id' => 'address_line1', 'placeholder'=>'Address Line 1*', 'class' => 'form-control', 'data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.address_min'),'data-parsley-maxlength' => config('app.fields_length.address_max'),'data-parsley-minlength-message' => $validationMessage['address.min'],'data-parsley-maxlength-message' => $validationMessage['address.max']]) !!}
                                        @if ($errors->has('address_line1'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('address_line1') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 p-l0">
                                        {!! Form::text('address_line2', !empty($user->billing_address_line2)? $user->billing_address_line2:old('billing_address_line2'), ['id' => 'address_line2', 'placeholder'=>'Address Line 2 (Optional)', 'class' => 'form-control','data-parsley-minlength' => config('app.fields_length.address_min'),'data-parsley-maxlength' => config('app.fields_length.address_max'),'data-parsley-minlength-message' => $validationMessage['address.min'],'data-parsley-maxlength-message' => $validationMessage['address.max']]) !!}
                                        @if ($errors->has('address_line2'))
                                            <span class="help-block">
                                            <strong>{{ $errors->first('address_line2') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                </div>
                            <div class="form-group">
                                <div class="col-md-12 p-l0">
                                    {!! Form::select('country_id', $countries, !empty($user->country_id)? $user->country_id :'', ['onChange' => 'checkOutModule.changeCountry()' , 'id' => 'country_id','data-parsley-required', 'data-parsley-required-message'=>'Please select a country.','class'=>'form-control']) !!}
                                    @if ($errors->has('country_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('country_id') }}</strong>
                                    </span>
                                    @endif

                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12 p-l0">

                                    {!! Form::select('state_id', $states, !empty($user->state_id)? $user->state_id :'', ['onChange' => 'checkOutModule.changeState()','id' => 'state_id', 'data-parsley-required', 'data-parsley-required-message'=>'Please select a state.','class'=>'form-control']) !!}
                                    @if ($errors->has('state_id'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('state_id') }}</strong>
                                    </span>
                                    @endif

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 p-l0">

                                    {!! Form::select('city', $cities, !empty($user->city)? $user->city :'', ['id' => 'city', 'data-parsley-required', 'data-parsley-required-message'=>'Please select a city.','class'=>'form-control']) !!}
                                    @if ($errors->has('city'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('city') }}</strong>
                                    </span>
                                    @endif

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 p-l0">
                                    {!! Form::text('zip_code', !empty($user->zip_code)? $user->zip_code : old('zip_code'), ['id' => 'zip_code', 'placeholder'=>'Zip Code*', 'class' => 'form-control', 'data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.zip_code_min'),'data-parsley-maxlength' => config('app.fields_length.zip_code_max'),'data-parsley-minlength-message' => $validationMessage['zip_code.min'],'data-parsley-maxlength-message' => $validationMessage['zip_code.max'],'data-parsley-type'=>"integer"]) !!}
                                    @if ($errors->has('zip_code'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('zip_code') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 p-l0">
                                    {!! Form::submit('Save', ['class' => 'btn btn-primary','title'=>'Save']) !!}
                                    <a class="btn btn-primary" href="{{ url('shipping') }}" title="Cancel">Cancel</a>
                                </div>

                            </div>
                            {!! Form::close() !!}
                            {{--<div class="row clearfix">--}}
                                {{--<div class="col-md-12">--}}
                                    {{--<h2 class="block-title">Shipping Address</h2>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-12 shipping-method">--}}
                                    {{--<div class="form-check form-check-inline f-s14 text-gray">--}}
                                        {{--<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1" checked="checked">--}}
                                        {{--<label class="form-check-label">$80</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="form-check form-check-inline f-s14 text-gray m-b10">--}}
                                        {{--<!-- <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2"> -->--}}
                                        {{--<label class="form-check-label">Domestic Shipping Charges</label>--}}
                                    {{--</div>--}}
                                    {{--<div class="form-check form-check-inline f-s14 text-gray m-b10">--}}
                                        {{--<!-- <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="3"> -->--}}
                                        {{--<label class="form-check-label">Shipping Charges</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--<div class="col-md-12 shipping-method">--}}
                                    {{--<div class="form-check form-check-inline f-s14 text-gray m-b10">--}}
                                        {{--<input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1">--}}
                                        {{--<label class="form-check-label">$0.00</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="form-check form-check-inline f-s14 text-gray">--}}
                                        {{--<!-- <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2"> -->--}}
                                        {{--<label class="form-check-label p-l10">Fixed</label>--}}
                                    {{--</div>--}}

                                    {{--<div class="form-check form-check-inline f-s14 text-gray">--}}
                                        {{--<!-- <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="3"> -->--}}
                                        {{--<label class="form-check-label">Flat Rate</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <!--Shipping Addres-->
                    </div>
                    <!--Shipping Listing Content-->
                </div>

                <div class="col-md-4">
                    <div class="shdow-box shipping-right-content m-t0">
                        <div class="price-content-box">
                            <div class="row price-box-heading">
                                <div class="col-md-10">
                                    <h2 class="block-title">Order Summary</h2>
                                </div>
                                <div class="col-md-2">
                                    &nbsp;
                                </div>
                            </div>
                            <div class="price-content-body">
                                <div class="f-s14 font-w700 item-cart-count">{{Cart::instance('shoppingcart')->content()->count()}} Item in cart</div>
                                <ul class="scroll">

                                    <?php foreach(Cart::instance('shoppingcart')->content() as $row) { ?>
                                    <li class="clearfix ">
                                        <div class="row shipping-address-right-summary">
                                            <div class="col-md-6">
                                                <div><a href="javascript:void(0)" class="hind-font">{{$row->name}}</a></div>
                                                <div class="f-s12">{{$row->options->has('manufacturer') ? $row->options->manufacturer : ''}}</div>
                                                <div class="f-s12 text-gray quantity">Quantity: {{$row->qty}}</div>
                                            </div>
                                            <div class="col-md-6 text-right">
                                                <div class="black f-s16">${{number_format($row->price*$row->qty, 2, '.', ',') }}</div>
                                            </div>
                                        </div>
                                    </li>
                                    <?php } ?>

                                </ul>
                            </div>
                            {{--<div class="price-content-footer">--}}
                                {{--<button type="submit" class="btn btn-primary" title="Proceed to Checkout">Proceed to Checkout</button>--}}
                            {{--</div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop

@section('javascript')
    <script>
        /*
          function for get get companies of selected business
           */
        /*
   Get all active companies of the selected business
    */

        $('#shipping_address_form').parsley();
    </script>
@endsection

<!--Address Listing Section-->