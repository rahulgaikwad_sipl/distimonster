<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=2.0" />
    <title>DistiMonster - Successfully registration</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Hind:400,500,600,700" rel="stylesheet">
    <style type="text/css">
        body{background:#f8f8f8;width:100%;margin:0px;padding:0px;text-align:center;font-family:'Poppins', Arial, sans-serif;}
        html{width:100%;}
        img{border:0px; text-decoration:none;outline:none;}
        a, a:hover{color:#ee7716;text-decoration:none;}
        table{border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;}
        .main-bg{}
        a:hover{text-decoration:none;}
        table,td,a,p{font-family:'Poppins', Arial, sans-serif;}
        @media only screen and (max-width:640px){body{width:auto!important;}
            .main{width:440px !important;margin:0px;padding:0px;}
            .two-left, .m-width{width:100% !important;text-align: center!important;}
            .m-width{text-align:left !important;}
            .image-hide{display:none !important;}
            .m-width{margin-top:0px;}
            .m-padding{padding-left:0px !important;text-align:center !important;}
            h4{margin-bottom:6px;width:100% !important}
            .m-width-space{width:8% !important;}
            .img-reponsive img{width:100% !important;}
        }
        @media only screen and (max-width:479px) {
            body{width:auto!important;}
            .main{width:280px !important;margin:0px;padding:0px;}
            .two-left{text-align: center !important;}
        }
        .img-top,.img-bottom{background:#f5f5f5;}
        .img-top{border-left:1px solid #f5f5f5;border-right:1px solid #f5f5f5;border-top: 1px solid #f5f5f5;border-radius:5px 5px 0 0;-moz-border-radius:5px 5px 0 0;-ms-border-radius:5px 5px 0 0;-o-border-radius:5px 5px 0 0;-webkit-border-radius:5px 5px 0 0;}
        .img-bottom{border-radius:0 0 5px 5px;-moz-border-radius:0 0  5px 5px;-ms-border-radius:0 0 5px 5px;-o-border-radius:0 0 5px 5px;-webkit-border-radius:0 0  5px 5px;border-left:1px solid #f5f5f5;border-right:1px solid #f5f5f5;border-bottom:1px solid #f5f5f5;}
    </style>
</head>
<body style="color:#f5f5f5;">
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="border-top:4px solid #D258A1;">
    <tr>
        <td>
            <table border="0" align="center" cellpadding="0" cellspacing="0" style="width:650px;max-width:100%;">
                <tr>
                    <td align="center" style="padding:17px 0 28px;">
                        <a href="{{url('/')}}" target="_blank" title="{{ config('app.name') }}"><img src="{{url('frontend/assets/images/logo.png')}}" alt="DistiMonster" style="display:block;width:100% !important;height:auto !important;max-width:116px; " /></a> <!--/ logo-image-->
                    </td>
                    <!--//logo-->
                </tr> <!-- // main tr-->
            </table>
        </td>
    </tr><!--logo-->
    <tr>
        <td>
            <table border="0" align="center" cellpadding="0" cellspacing="0" style="text-align:center;width:650px;max-width:100%;background-color:#D258A1;">
                <tr>
                    <td valign="top" style="background-color:#D258A1;font-size:15px;color:#fff;height:32px;vertical-align:middle;width:235px;">
                        <a target="_blank" style="color:#fff;" href="{{url('/about-us')}}" title="About Us">About Us</a>
                    </td><!--link first-->
                    <td valign="top" style="background-color:#D258A1;font-size:15px;color:#fff;height:32px;vertical-align:middle;width:270px;">
                        <a target="_blank" style="color:#fff;" href="{{url('/news-updates')}}" title="News & Updates">News &amp; Updates</a>
                    </td><!--link second-->
                    <td valign="top" style="background-color:#D258A1;font-size:15px;color:#fff;height:32px;vertical-align:middle;width:270px;">
                        <a target="_blank" style="color:#fff;" href="{{url('/bom')}}" title="BOM">BOM</a>
                    </td><!--link third-->
                </tr>
            </table>
        </td>
    </tr><!--links-->
    <tr>
        <td align="center" valign="top" style="padding:0px;"><!--main section start-->
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:650px;max-width:100%;">
                <tr>
                    <td align="left" valign="top" bgcolor="#f5f5f5"><!--content section-->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:100%;">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                        <tr>
                                            <td align="left" valign="top" style="font:Bold 14px Arial, Helvetica, sans-serif; color:#666;padding:0px;background:#fff;padding:25px 25px;">
                                                <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                                    <tr><td style="height:25px;">&nbsp;</td></tr><!--spacing-->
                                                    <tr>
                                                        <td>
                                                            <table style="border:0 none;border-spacing:0;font-family:'Poppins', Arial, sans-serif;color:#000;font-weight:normal;text-align:left;" align="center" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" style="font-size:16px;color:#333;font-weight:normal;line-height:23px;padding-bottom:10px;">Hello {{ ucwords($name)  }}, </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Your account has been successfully setup by  {{ config('app.name') }}  now you can login with your email and password.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Email: {{ $email }} </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Password: {{ $password }} </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr><!--//single list-->
                                                    <tr>
                                                        <td>
                                                            <table style="border:0 none;border-spacing:0;font-family:'Poppins', Arial, sans-serif;color:#000;font-weight:normal;text-align:left;width:600px;padding-bottom:40px" align="center" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="center">
                                                                        <a href="{{url('/login')}}" title="LOGIN" style="color:#0185c6;text-decoration:none" target="_blank">
                                                                            <table cellpadding="0" cellspacing="0">
                                                                                <tr>
                                                                                    <td style="padding:5px 10px;background:#D258A1;border-radius:150px;font-size:17px;padding:0.75rem 1.4375rem;text-align:center;" align="center" valign="middle" width="110"><span style="color:#fff;">LOGIN</span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr><!--//single list-->
                                                    <tr><td style="border-left:1px solid #f5f5f5;border-right:1px solid #f5f5f5;height:25px;">&nbsp;</td></tr><!--spacing-->
                                                </table>

                                            </td>
                                        </tr>
                                        <!--footer content-->
                                    </table><!--// center content-->
                                </td>
                            </tr>
                        </table><!--content section end-->
                    </td>
                </tr><!-- // main tr-->
            </table>
        </td><!-- //main section start-->
    </tr><!--//main wrap tr-->
    <tr>
        <td>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" valign="top"><!--Footer Bottom Part Start-->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" style="background:#000;color:#fff;font-size:12px;width:650px;max-width:100%;">
                            <tr>
                                <td align="center" valign="top" style="padding:20px 32px 20px 32px;">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td align="center" valign="top" style="font:Normal 13px Arial, Helvetica, sans-serif;color:#fff;font-size:12px;line-height:18px;padding:0 0 20px;">Lorem 76d, Ipsum Dollor, North Michaelchester, CF99 6QQ | <a href="mailto:sales@distimonster.com" style="color:#fff;font-size:12px;text-decoration:none;"><strong style="color:#fff;font-size:12px;font-weight:400;">sales@distimonster.com</strong></a> | (000) 123-4567</td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top" style="font:Normal 13px Arial, Helvetica, sans-serif;color:#fff;line-height:18px;padding:0 0 20px;width:25%;">
                                                <a href="https://www.facebook.com/" title="Facebook"><img src="{{url('frontend/assets/images/facebook-icon.png')}}" alt="Facebook"></a>&nbsp;&nbsp;&nbsp;
                                                <a href="https://twitter.com/" title="Twitter"><img src="{{url('frontend/assets/images/twitter-icon.png')}}" alt="Twitter"></a>&nbsp;&nbsp;&nbsp;
                                                <a href="https://plus.google.com/" title="Google+"><img src="{{url('frontend/assets/images/googleplus-icon.png')}}" alt="Google+"></a>&nbsp;&nbsp;&nbsp;
                                                <a href="https://www.linkedin.com/" title="Linkedin"><img src="{{url('frontend/assets/images/linkedin-icon.png')}}" alt="Linkedin"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" valign="top" style="font:Normal 13px Arial, Helvetica, sans-serif;color:#fff;line-height:18px;">&copy; {{ config('app.name') }} 2018. All rights reserved</td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table>

                        <!--Footer Bottom Part End--></td>
                </tr> <!-- // main tr-->
            </table></td>
    </tr>
</table>
</body>
</html>