@extends('emails.layout.master')
@section('content')
    <tr>
        <td align="center" valign="top" style="padding:0px;"><!--main section start-->
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:650px;max-width:100%;">
                <tr>
                    <td align="left" valign="top" bgcolor="#f5f5f5"><!--content section-->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:100%;">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                        <tr>
                                            <td align="left" valign="top" style="font:Bold 14px Arial, Helvetica, sans-serif; color:#666;padding:0px;background:#fff;padding:25px 25px;">
                                                <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                                    <tr>
                                                        <td style="height:25px;">&nbsp;</td></tr><!--spacing-->
                                                    <tr>
                                                        <td>
                                                            <table style="border:0 none;border-spacing:0;font-family:'Poppins', Arial, sans-serif;color:#000;font-weight:normal;text-align:left;" align="center" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" style="font-size:16px;color:#333;font-weight:normal;line-height:23px;padding-bottom:10px;">Dear {{$billingFirstName}} {{$billingLastName}},</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">This payment to Distimonster is for renewal of your subscription and has been charged from your saved credit card on {{date("m-d-Y", strtotime($paymentDate))}}.</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 589px;">
                                                                        <table cellpadding="0" align="center" cellspacing="0" width="589" style="color: #444;font-family: 'Mallanna', sans-serif;font-size:14px;line-height:100%;width:589px;">
                                                                            <tr><td colspan="5" width="589"><br></td>
                                                                            </tr>
                                                                            <tr>
                                                                                <th align="center">Pay To</th>
                                                                                <th align="center">Amount Paid</th>
                                                                                <th align="center">Transaction ID</th>
                                                                                <th align="center">Payment Date</th>
                                                                            </tr>
                                                                            <tr>
                                                                                <td colspan="5" height="2"  width="589">
                                                                                    <hr style="border-top:1px dotted #666;">
                                                                                </td>
                                                                            </tr>
                                                                            <tr>
                                                                                <td align="center">Distimonster</td>
                                                                                <td align="center">{{$grandTotalAmount}}</td>
                                                                                <td align="center">{{$transactionId}}</td>
                                                                                <td align="center">{{date("m-d-Y", strtotime($paymentDate))}}</td>
                                                                            </tr>
                                                                            <tr><td colspan="5"  width="589"><br></td></tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr><!--//single list-->
                                                    <tr>
                                                        <td style="height:23px;width:100px;">&nbsp;</td>
                                                    </tr><!--//space-->
                                                    <tr>
                                                        <td style="font-size:14px;color:#333;line-height:121%;padding:0">Please contact site administrator in case of any questions.</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-left:1px solid #f5f5f5;border-right:1px solid #f5f5f5;height:25px;">&nbsp;</td></tr><!--spacing-->
                                                    <table style="border:0 none;border-spacing:0;font-family:'Poppins', Arial, sans-serif;color:#000;font-weight:normal;text-align:left;width:600px;padding-bottom:40px" align="center" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td>Thank you,</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="border-left:1px solid #f5f5f5;border-right:1px solid #f5f5f5;height:15px;">&nbsp;</td></tr><!--spacing-->
                                                    <tr>
                                                        <td><img src="{{url('frontend/assets/images/logo.png')}}" width="80px"/></td>
                                                    </tr>
                                                </table>
                                                </table>
                                            </td>
                                        </tr>
                                        <!--footer content-->
                                    </table><!--// center content-->
                                </td>
                            </tr>
                        </table><!--content section end-->
                    </td>
                </tr><!-- // main tr-->
            </table>
        </td><!-- //main section start-->
    </tr><!--//main wrap tr-->
    @stop