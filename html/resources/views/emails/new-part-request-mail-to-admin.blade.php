@extends('emails.layout.master')
@section('content')
<tr>
    <td align="center" valign="top" style="padding:0px;"><!--main section start-->
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:650px;max-width:100%;">
                <tr>
                    <td align="left" valign="top" bgcolor="#f5f5f5"><!--content section-->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:100%;">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                        <tr>
                                            <td align="left" valign="top" style="font:Bold 14px Arial, Helvetica, sans-serif; color:#666;padding:0px;background:#fff;padding:25px 25px;">
                                                <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                                    <tr><td style="height:25px;">&nbsp;</td></tr><!--spacing-->
                                                    <tr>
                                                        <td>
                                                            <table style="border:0 none;border-spacing:0;font-family:'Poppins', Arial, sans-serif;color:#000;font-weight:normal;text-align:left;" align="center" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" style="font-size:16px;color:#333;font-weight:normal;line-height:23px;padding-bottom:10px;">Hello Admin,</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">A new product request has been made by <b>{{$name}}</b>. Please find below details for the request.</td>
                                                                </tr>
                                                                <?php if(!is_array($partNumber)){?>
                                                                <tr>
                                                                    <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Part Number: {{$partNumber}}</td>
                                                                </tr>
                                                                <?php }?>
                                                                <?php if(!is_array($expectedQuantity)){?>
                                                                <tr>
                                                                    <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Expected Quantity: {{$expectedQuantity}}</td>
                                                                </tr>
                                                                <?php }?>
                                                                <?php if(!is_array($expectedUnitPrice) && !is_array($partNumber)){?>
                                                                <tr>
                                                                    <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Expected Unit Price : ${{$expectedUnitPrice}}</td>
                                                                </tr>
                                                                <?php }?>
                                                                <tr>
                                                                    <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">
                                                                        <a href="{{url('admin/requested-product')}}" title="requested part" style="color:#0185c6;text-decoration:none" target="_blank">Click here </a>to check more details and reply.</td>
                                                                </tr>

                                                            </table>
                                                        </td>
                                                    </tr><!--//single list-->
                                                    <tr><td style="border-left:1px solid #f5f5f5;border-right:1px solid #f5f5f5;height:25px; color:#333;">Thanks</td></tr><!--spacing-->
                                                </table>

                                            </td>
                                        </tr>
                                        <!--footer content-->
                                    </table><!--// center content-->
                                </td>
                            </tr>
                        </table><!--content section end-->
                    </td>
                </tr><!-- // main tr-->
            </table>
        </td><!-- //main section start-->
</tr><!--//main wrap tr-->
@stop

