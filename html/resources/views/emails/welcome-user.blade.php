@extends('emails.layout.master')
@section('content')
    <tr>
        <td align="center" valign="top" style="padding:0px;"><!--main section start-->
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:650px;max-width:100%;">
                <tr>
                    <td align="left" valign="top" bgcolor="#f5f5f5"><!--content section-->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:100%;">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                        <tr>
                                            <td align="left" valign="top" style="font:Bold 14px Arial, Helvetica, sans-serif; color:#666;padding:0px;background:#fff;padding:25px 25px;">
                                                <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                                    <tr><td style="height:25px;">&nbsp;</td></tr><!--spacing-->
                                                    <tr>
                                                        <td>
                                                            <table style="border:0 none;border-spacing:0;font-family:'Poppins', Arial, sans-serif;color:#000;font-weight:normal;text-align:left;" align="center" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" style="font-size:16px;color:#333;font-weight:normal;line-height:23px;padding-bottom:10px;">Hello {{ ucwords($firstName)  }}, </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">We are really excited that you created your account on DistiMonster. We are here to make your job of
                                                                        sourcing and purchasing product easier and quicker, 1 Source, 1 PO, 1 Shipment. If at any time you need assistance we are here, you can call and talk with one of our representatives or if you prefer we are online with a live chat as well. If you would like to set up a demo to get the most out of DistiMonster please click here to set up a time.</td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr><!--//single list-->
                                                    <tr>
                                                        <td>
                                                            <table style="border:0 none;border-spacing:0;font-family:'Poppins', Arial, sans-serif;color:#000;font-weight:normal;text-align:left;width:600px;padding-bottom:40px" align="center" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="center">
                                                                        <a href="{{url('/login')}}" title="LOGIN" style="color:#0185c6;text-decoration:none" target="_blank">
                                                                            <span style="padding:5px 10px;background:#0E88CA;border-radius:150px;font-size:17px;padding:0.75rem 1.4375rem;text-align:center; color:#fff;">LOGIN</span>


                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr><!--//single list-->
                                                    <tr><td style="border-left:1px solid #f5f5f5;border-right:1px solid #f5f5f5;height:25px;">&nbsp;</td></tr><!--spacing-->
                                                </table>

                                            </td>
                                        </tr>
                                        <!--footer content-->
                                    </table><!--// center content-->
                                </td>
                            </tr>
                        </table><!--content section end-->
                    </td>
                </tr><!-- // main tr-->
            </table>
        </td><!-- //main section start-->
    </tr><!--//main wrap tr-->
    @stop