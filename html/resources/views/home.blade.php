@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <a href="{{ route('admin.users.index') }}">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$users}}</h3>

                        <p>User Registrations</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-ios-people-outline"></i>
                    </div>
                </div>
            </a>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <a href="{{ url('admin/orders') }}">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>{{$orders}}</h3>

                        <p>Orders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                </div>
            </a>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <a href="{{ url('admin/quotes/'.base64_encode('0')) }}">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$quote}}</h3>
                        <p>Pending RFQ's</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-clipboard"></i>
                    </div>
                </div>
            </a>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <a href="{{ url('admin/orders') }}">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3> $ {{ number_format($orderMoney, 2, '.', '') }}</h3>
                        <p>Total Revenues</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-money"></i>
                    </div>
                </div>
            </a>
        </div>
        
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <a href="{{ url('admin/orders/'.base64_encode('0').'/'.base64_encode('payment_status')) }}">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$orders_pending}}</h3>
                        <p>Pending Payments</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                </div>
            </a>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <a href="{{ url('admin/orders/'.base64_encode('1').'/'.base64_encode('payment_status')) }}">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$orders_done}}</h3>
                        <p>Done Payments</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                </div>
            </a>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <a href="{{ url('admin/orders/'.base64_encode('2').'/'.base64_encode('payment_status')) }}">                
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$orders_failed}}</h3>
                        <p>Failed Payments</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                </div>
            </a>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <a href="{{ url('admin/orders/'.base64_encode('3').'/'.base64_encode('payment_status')) }}">
                <div class="small-box bg-teal">
                    <div class="inner">
                        <h3>{{$orders_refunded}}</h3>
                        <p>Refunded Payments</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                </div>
            </a>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <a href="{{ url('admin/orders/'.base64_encode('1').'/'.base64_encode('shipment_status')) }}">
                <div class="small-box bg-teal">
                    <div class="inner">
                        <h3>{{$orders_shipped}}</h3>
                        <p>Shipped Orders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-truck"></i>
                    </div>
                </div>
            </a>
        </div>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <a href="{{ url('admin/orders/'.base64_encode('2').'/'.base64_encode('shipment_status')) }}">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>{{$orders_pending_ship}}</h3>
                        <p>Pending Orders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-truck"></i>
                    </div>
                </div>
            </a>
        </div>

        <!-- ./col -->
        <?php /*<div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <a href="{{ url('admin/orders/'.base64_encode('3').'/'.base64_encode('shipment_status')) }}">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3>{{$orders_intransit}}</h3>
                        <p>In Transit Orders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-truck"></i>
                    </div>
                </div>
            </a>
        </div> */ ?>

        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <a href="{{ url('admin/orders/'.base64_encode('4').'/'.base64_encode('shipment_status')) }}">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>{{$orders_delivered}}</h3>
                        <p>Delivered Orders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-truck"></i>
                    </div>
                </div>
            </a>
        </div>
    </div>

@endsection
