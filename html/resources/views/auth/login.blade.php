@extends('admin.layouts.auth')
@section('content')
    <div class="login-logo">
        <a href="{{url('/')}}"><img src="{{url('frontend/images/logo.png') }}"></a>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">{{ ucfirst(config('app.name')) }} @lang('quickadmin.qa_login')</div>
                <div class="panel-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>@lang('quickadmin.qa_whoops')</strong> @lang('quickadmin.qa_there_were_problems_with_input'):
                            <br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        {!! Form::open(['class'=>"form-horizontal" ,'role'=>'form', 'method' => 'POST', 'url' => url('admin/login'), 'enctype' =>"multipart/form-data", 'data-parsley-validate' ]) !!}
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Email*</label>
                            <div class="col-md-6">
                                {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email', 'data-parsley-required' => 'true','data-parsley-required-message' => 'Email is required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Password*</label>
                            <div class="col-md-6">
                                {!! Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'data-parsley-required' => true,'data-parsley-required-message' => 'Password is required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4"></div>
                              <div class="col-md-3">
                                <label><input type="checkbox" name="remember"> @lang('quickadmin.qa_remember_me')</label>
                            </div>
                            <div class="col-md-3">
                                <a href="{{ url('admin/password/reset') }}">Forgot your password?</a>
                            </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-success">Login</button>
                                </div>

                            </div>

                        </div>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
@endsection
