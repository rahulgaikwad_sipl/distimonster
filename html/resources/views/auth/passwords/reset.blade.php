@extends('admin.layouts.auth')

@section('content')
    <div class="login-logo">
        <a href="{{url('/')}}"><img src="{{url('frontend/images/logo.png') }}"></a>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">@lang('quickadmin.qa_reset_password')</div>
                <div class="panel-body">



                        {!! Form::open(['class'=>"form-horizontal" ,'role'=>'form', 'method' => 'POST', 'url' =>url('admin/password/reset'), 'enctype' =>"multipart/form-data", 'data-parsley-validate' ]) !!}

                        <input type="hidden"
                               name="_token"
                               value="{{ csrf_token() }}">
                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">Email*</label>

                            <div class="col-md-6">
                                {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email', 'data-parsley-required' => 'true','data-parsley-required-message' => 'Email is required']) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Password*</label>

                            <div class="col-md-6">
                                {!! Form::password('password', ['id'=>'password', 'class' => 'form-control', 'placeholder' => 'Password', 'data-parsley-required' => true,'data-parsley-required-message' => 'Password is required','data-parsley-minlength' => config('app.fields_length.password_min'),'data-parsley-maxlength' => config('app.fields_length.password_max')]) !!}

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Confirm Password*</label>

                            <div class="col-md-6">
                                {!! Form::password('password_confirmation', ['class' => 'form-control', 'placeholder' => 'Confirm Password','data-parsley-required','data-parsley-required-message'=>'Confirm Password is required' ,'data-parsley-equalto-message'=>'Please enter same as password.','data-parsley-equalto'=>"#password" ]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit"
                                        class="btn btn-success"
                                        style="margin-right: 15px;">
                                    @lang('quickadmin.qa_reset_password')
                                </button>
                                <a class="btn btn-close btn-success" style="margin-right: 15px;" href="{{url('admin')}}">
                                    Back to Login
                                </a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
