@extends('admin.layouts.auth')

@section('content')
    <div class="login-logo">
        <a href="{{url('/')}}"><img src="{{url('frontend/images/logo.png') }}"></a>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset password</div>
                <div class="panel-body">
                        {!! Form::open(['class'=>"form-horizontal" ,'role'=>'form', 'method' => 'POST', 'url' =>url('admin/password/email'), 'enctype' =>"multipart/form-data", 'data-parsley-validate' ]) !!}

                        <input type="hidden"
                               name="_token"
                               value="{{ csrf_token() }}">

                        <div class="form-group">
                            <label class="col-md-4 control-label">Email*</label>
                            <div class="col-md-6">
                                {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email', 'data-parsley-required' => 'true','data-parsley-required-message' => 'Email is required']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit"
                                        class="btn btn-success"
                                        style="margin-right: 15px;">
                                    Reset password
                                </button>
                                <a class="btn btn-success btn-close" style="margin-right: 15px;" href="{{url('admin')}}">
                                    Back to Login
                                </a>
                            </div>
                        </div>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection