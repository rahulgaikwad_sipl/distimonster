<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 26/03/18
 * Time: 5:12 PM
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Bom;
use DataTables;
class BomController  extends Controller
{
    public function index()
    {
        return view('admin.bom.index');
    }

    public function getBomList(Request $request){
        try {
            if ($request->ajax()) {
                $columns = [
                    'boms.id',
                    'boms.full_name',
                    'boms.company_name',
                    'boms.email',
                    'boms.contact_no',
                    'boms.address'
                ];
                $boms = Bom::select($columns)->whereNull('boms.deleted_at');
                return Datatables::of($boms)
                    ->editColumn('id', function ($boms) {
                        $return    =  'BOM-'.$boms->id;
                        return $return;
                    })->make();
//                    ->editColumn('order_date', function ($orders) {
//                        return date('d/m/Y, h:i a', strtotime($orders->order_date));
//                        //$orders->order_date ? with(new Carbon($orders->order_date))->format('m/d/Y') : '';
//                    })
//                    ->editColumn('order_total_amount', function ($orders) {
//                        return number_format($orders->order_total_amount,2, '.', ',');
//                        //$orders->order_date ? with(new Carbon($orders->order_date))->format('m/d/Y') : '';
//                    })
//                    ->addColumn('action', function ($orders) {
//                        $data['order'] = $orders;
//                        $return = view('admin.orders.partials.action',$data)->render();
//                        return $return;
//                    })->rawColumns(['status', 'action'])->make(true);
            }
        } catch (\Exception $ex) {
            return redirect()->route('bom.index')->with('failure', 'Something went wrong');
        }
    }
}