@extends('admin.layouts.app')
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Requested Product</li>
        </ol>
    </section>
    <h3 class="page-title">Requested Product</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table border="0" width="100%"  class="table table-striped table-bordered table-hover testing" id="requestedproduct" >
                        <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>Customer Email id</th>
                            <th>Customer Phone</th>
                            <th>MPN</th>
                            <th>Category</th>
                            <th>Requested Quantity</th>
                            <th>Requested Delivery Date</th>
                            <th>Target Price (per unit)</th>
                            <th>Requested On Date</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            requestedProduct.loadReqProductTable();
        });
    </script>
@endsection


