@extends('admin.layouts.app')
@section('content')
    <h3 class="page-title">Boms</h3>
    <div class="panel panel-default">
        <div class="panel-heading">
            Bom List
        </div>
        <div class="panel-body table-responsive">
            <table border="0" width="100%"  class="table table-striped table-bordered table-hover testing" id="bomtable" >
                <thead>
                <tr>
                    <th>Bom Id</th>
                    <th>Customer Name</th>
                    <th>Company Name</th>
                    <th>Email</th>
                    <th>Contact Number</th>
                    <th>Address</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            bomModule.loadBomDataTable();
        });
    </script>
@endsection


