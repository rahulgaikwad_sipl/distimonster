@if($order->payment_status  == 0 )
    <a class='label label-primary' title="Click To Change Status"  onclick="orderModule.changePaymentStatus('<?php echo  $order->payment_status  ?>',  '<?php echo $order->id ?>')"  href="javascript:void(0);">{{config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.pending')}}</a>
@elseif($order->payment_status  == 1)
    <a class='label label-success' title="Click To Change Status"  <?php if($order->payment_method_name == 'COD'){ echo 'onclick="orderModule.changePaymentStatus('.$order->payment_status.',  '.$order->id.')"';} ?>  href="javascript:void(0);">{{config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.done')}}</a>
@elseif($order->payment_status  == 2)
    <a class='label label-danger' title="Click To Change Status"  onclick="orderModule.changePaymentStatus('<?php echo  $order->payment_status  ?>',  '<?php echo $order->id ?>')"  href="javascript:void(0);">{{config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.failed')}}</a>
@elseif($order->payment_status  == 3)
    <a class='label label-info' title="Click To Change Status"  onclick="orderModule.changePaymentStatus('<?php echo  $order->payment_status  ?>',  '<?php echo $order->id ?>')"  href="javascript:void(0);">{{config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.refunded')}}</a>
@endif
