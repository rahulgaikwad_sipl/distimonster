@extends('admin.layouts.app')
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Orders</li>
        </ol>
    </section>
    <h3 class="page-title">Orders</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                   <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="shipment_status_id" class="col-sm-3 control-label">Shipment Status:</label>
                            <div class="col-sm-8">
                                <select name="shipment_status_id" id="shipment_status_id" class="form-control drop_down">
                                    <?php
                                    foreach($orderStatus  as $list => $value){
                                        $selected = ('"'.$shipment_status.'"' == '"'.$list.'"') ? 'selected="selected"' : '';
                                        if($value == 'Select Status'){
                                            echo '<option value="'.$list.'">Select shipment status</option>';
                                        }else{
                                            echo '<option '.$selected.' value="'.$list.'">'.ucwords($value).'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="payment_status_id" class="col-sm-3 control-label">Payment Status:</label>
                            <div class="col-sm-8">
                                <select name="payment_status_id" id="payment_status_id" class="form-control drop_down">
                                    <?php
                                    foreach($paymentStatus  as $list => $value){
                                        $selected = ('"'.$payment_status.'"' == '"'.$list.'"') ? 'selected="selected"' : '';
                                        if($value == 'Select Status'){
                                            echo '<option value="'.$list.'">Select payment status</option>';
                                        }else{
                                            echo '<option '.$selected.' value="'.$list.'">'.ucwords($value).'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-2 ">
                    </div>
                    <div class="col-lg-2 text-right">
                        <div class="form-group  label-static">
                            <a class="btn btn-success" href="{{ url('admin/export-order') }}">Download Excel</a>
                        </div>
                    </div>
                </div>
                    <table border="0" width="100%"  class="table table-striped table-bordered table-hover testing" id="orderTable" >
                        <thead>
                        <tr>
                            <th>Order Id</th>
                            <th>Customer Name</th>
                            <th>Order Date</th>
                            <th>Total</th>
                            <th>Shipment Status</th>
                            <th>Payment Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>

                <div class="modal fade" id="update-shipping-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Update Shipping Status</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <!-- column -->
                                <div class="col-md-12">
                                    <!-- form start -->
                                    {!! Form::open(['method' => 'POST', 'id'=>'updateShippingStatusForm','data-parsley-validate' => true]) !!}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="requestStatus" class="col-sm-4 control-label">Status:<span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                {!! Form::select('shippingStatus', $orderStatus, null,array('id'=>'shippingStatus','class' => 'form-control', 'data-parsley-required' => true)) !!}

                                            </div>
                                        </div>
                                        <div class="clearfix">&nbsp;</div>
                                        <div class="form-group">
                                            <label for="requestStatus" class="col-sm-4 control-label hidden">Tracking Information:<span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                <textarea name="trackingInfo" id="trackingInfo" class ="form-control trackingInfo hidden" placeholder="Enter tracking information."></textarea>
                                                <!-- data-parsley-required ="true" -->
                                            </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="orderId" id="orderId" />
                                        <input type="hidden" id="_token" name="_token" value="{{{ csrf_token() }}}" />

                                    </div>
                                    <!-- /.box-body -->
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-default waves-effect update-data-from-delete-form">Close</button>
                            <a href="javascript:void(0)" onclick="orderModule.updateShippingStatus()" class="btn btn-success">Update </a>
                        </div>
                    </div>
                </div>
            </div>
                <div class="modal fade" id="update-payment-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Update Payment Status</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <!-- column -->
                                <div class="col-md-12">
                                    <!-- form start -->
                                    {!! Form::open(['method' => 'POST', 'id'=>'updatePaymentStatusForm','data-parsley-validate' => true]) !!}
                                    <div class="box-body">
                                        <div class="form-group">
                                            <label for="paymentStatus" class="col-sm-4 control-label">Status:<span class="required">*</span></label>
                                            <div class="col-sm-8">
                                                {!! Form::select('paymentStatus', $paymentStatus, null,array('id'=>'paymentStatus','class' => 'form-control', 'data-parsley-required' => true)) !!}

                                            </div>
                                        </div>
                                        <input type="hidden" class="form-control" name="orderId" id="orderId" />
                                        <input type="hidden" id="_token" name="_token" value="{{{ csrf_token() }}}" />

                                    </div>
                                    <!-- /.box-body -->
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" data-dismiss="modal" class="btn btn-default waves-effect update-data-from-delete-form">Close</button>
                            <a href="javascript:void(0)" onclick="orderModule.updatePaymentStatus()" class="btn btn-success pull-right">Update </a>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>
        $(document).ready(function() {
            orderModule.loadOrderDataTable();
        });
        $(document).on('change', '#shipment_status_id, #payment_status_id', function(){
            orderModule.loadOrderDataTableAgain();
        });
    </script>
@endsection


