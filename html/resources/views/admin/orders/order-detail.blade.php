@extends('admin.layouts.app')
@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="{{url('admin/home')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li> <a href="{{url('admin/orders')}}">Orders</a></li>
        <li class="active">Order Detail</li>
    </ol>
</section>
<br/>

<section class="content">
<!-- Default box -->
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-6">
                    <h2>
                        <a href="#">{{$order[0]->customer_name}} {{$order[0]->last_name}}</a> <br />
                        <small>{{$order[0]->email}}</small> <br />
                        <small>Order Id: <strong>{{$order[0]->id}}</strong></small>
                    </h2>
                </div>
                <div class="col-md-6">
                    {{--<a href="{{ url('admin/quotes') }}">Back</a>--}}
                    <div class="pull-right">
                        <a class="btn btn-link btn-xs" href="{{ url('admin/orders') }}"><i class="fa fa-arrow-circle-o-left" style="font-size:2.5rem;"></i></a>
                    </div>
                </div>

                {{--<div class="col-md-3 col-md-offset-3">--}}
                 {{--<h2><a href="{{route('admin.orders.invoice.generate', $order['id'])}}" class="btn btn-primary btn-block">Download Invoice</a></h2>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <h4> <i class="fa fa-shopping-cart"></i> Ordered Items</h4>
                    <table class="table">
                        <thead>
                        <th>MPN</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Manufacturer Name</th>
                        <th>Distributor Name</th>
                        <th>Internal Part#</th>
                        <th>Price Total</th>
                        <th>Is Quoted Item</th>
                        <th>Quote Name</th>
                        </thead>
                        <tbody>
                        @foreach($orderDetail as $item)
                            <tr>
                                <td>
                                    @if(!empty($item->item_name)){{ $item->item_name }}@else N/A @endif
                                </td>
                                <td>
                                    @if(!empty($item->quantity)){{ $item->quantity }}@else N/A @endif
                                </td>
                                <td>
                                    @if(!empty($item->price))${{number_format($item->price ,2, '.', ',')}}@else N/A @endif
                                </td>
                                <td>
                                    @if(!empty($item->manufacturer_name)){{ $item->manufacturer_name}}@else N/A @endif
                                </td>
                                <td style="display:none;">
                                    @if(!empty($item->source_part_id)){{ $item->source_part_id}}@else N/A @endif
                                </td>
                                <td>
                                    @if(!empty($item->distributor_name)){{ $item->distributor_name}}@else N/A @endif
                                </td>
                                <td>
                                    @if(!empty($item->customer_part_id)){{ $item->customer_part_id?$item->customer_part_id:'N/A'}}@else N/A @endif
                                </td>
                                <td>
                                    @if(!empty($item->quantity) && !empty($item->price))${{number_format($item->quantity*$item->price ,2, '.', ',')}}@else N/A @endif
                                </td>
                                <td>{{$item->is_quote_item?'Yes':'No'}}</td>
                                <td>{{$item->quote_name? $item->quote_name :'N/A'}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="box">
        <div class="box-body">
            <h4> <i class="fa fa-shopping-bag"></i> Order Information</h4>
            <table class="table">
                <tbody>
                <tr>
                    <td class="col-md-2">PO Number</td>
                    <td class="col-md-2">Date</td>
                    <td class="col-md-2">Customer Name</td>
                    <td class="col-md-2">Payment Method</td>
                    <td class="col-md-2">Payment Status</td>
                    <td class="col-md-2">Shipment Status</td>
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td>{{ $order[0]->shipping_purchase_order_number }}</td>
                    <td>{{ date('m/d/Y, h:i a', strtotime($order[0]->order_date))}}</td>
                    <td>{{ $order[0]->customer_name }} {{  $order[0]->last_name }}</td>
                    <td><strong>{{$order[0]->payment_method_name}}</strong></td>
                    @if($order[0]->payment_status  == 0 )
                        <td><span class="label label-primary"> {{config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.pending')}}</span></td>
                    @elseif($order[0]->payment_status  == 1)
                        <td> <span class="label label-success"> {{config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.done')}}</span> </td>
                    @elseif($order[0]->payment_status  == 2)
                        <td><span class="label label-danger"> {{config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.failed')}}</span></td>
                    @elseif($order[0]->payment_status  == 3)
                        <td><span class="label label-info"> {{config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.refunded')}}</span></td>
                    @endif

                    @if($order[0]->order_shipping_status  == 1 )
                        <td><span class="label label-primary"> {{config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.shipped')}}</span></td>
                    @elseif($order[0]->order_shipping_status  == 2)
                        <td><span class="label label-danger"> 
                        {{($order[0]->partially_shipped  == 1) ? config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.partially_shipped') : config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.pending')}}
                        </span></td>
                    @elseif($order[0]->order_shipping_status  == 3)
                        <td><span class="label label-warning"> {{config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.intransit')}}</span></td>
                    @elseif($order[0]->order_shipping_status  == 4)
                        <td><span class="label label-success"> {{config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.delivered')}}</span></td>
                    @endif
                </tr>
                </tbody>
                <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="bg-warning">Subtotal</td>
                    <td class="bg-warning">${{ number_format($order[0]->order_sub_total,2, '.', ',') }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="bg-warning">Tax</td>
                    <td class="bg-warning">${{ number_format($order[0]->order_tax_amount,2, '.', ',') }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="bg-warning">Discount</td>
                    <td class="bg-warning">${{number_format( $order[0]->order_discount_amount,2, '.', ',')}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="bg-warning">Shipping Charges</td>
                    <td class="bg-warning">${{ number_format( $order[0]->order_shipping_charges_amount,2, '.', ',')}}</td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td class="bg-success text-bold">Order Total</td>
                    <td class="bg-success text-bold">${{ number_format($order[0]->order_total_amount,2, '.', ',')}}</td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <h4> <i class="fa fa-map-marker"></i> Shipping Address</h4>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>@if(!empty($order[0]->address_shipping)){{$order[0]->address_shipping}}@else N/A @endif</td>

                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <h4> <i class="fa fa-envelope"></i> Billing Address</h4>
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>@if(!empty($order[0]->address_billing)){{$order[0]->address_billing}}@else N/A @endif</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <h4> <i class="fa fa-truck"></i> Shipping Details</h4>
                    <table class="table">
                        <thead>
                        <th>Shipping Method Type</th>
                        <th>Shipping Account Number</th>
                        <th>Note</th>
                        <th>Request Delivery Date</th>
                        </thead>
                        <tbody>
                        <tr>
						     @if($order[0]->shipping_method_type)
							    {{Helpers::getShippingOptionById($order[0]->shipping_method_type)->name}}
							@endif
                            <td>@if(!empty($order[0]->shipping_account_number)){{ $order[0]->shipping_account_number }}@else N/A @endif</td>
                            <td>@if(!empty($order[0]->shipping_notes)){{ $order[0]->shipping_notes }}@else N/A @endif</td>
                            <td>{{ ($order[0]->requested_delivery_date == "0000-00-00 00:00:00" || $order[0]->requested_delivery_date == NULL) ? 'N/A' : date('m/d/Y', strtotime($order[0]->requested_delivery_date))}}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="box"> 
        <div class="box-body"> 
            <form method="post" class="tracking_info_form" action="{{url('admin/orders/update-tracking-info')}}" data-parsley-validate=true>
                {{ csrf_field() }}
                <div class="row">  
                    <div class="col-md-12"> 
                        <h4> <i class="fa fa-map-marker"></i> Tracking Information</h4>
                        <div class="table-responsive">
                            <table class="table">
                            <thead>
                                <tr>
                                    <th>Distributor Name</th>
                                    <th>MPN</th>
                                    <th>Ordered Qty</th>
                                    <th>Shipped Qty</th>
                                    <th>Tracking Number</th>
                                    <th>Shipping Status</th>
                                    <th>Date of Shipping</th>
                                    <th>Tracking Website</th>
                                    <th>Note</th>
                                </tr>
                            </thead>    
                            <tbody>
                            @foreach($orderDetail as $item)
                                <tr>
                                    <td>{{ !empty($item->distributor_name)?ucwords($item->distributor_name) : 'N/A'}}</td>
                                    <td>{{ $item->item_name }}</td>
                                    <td>{{ $item->quantity }}</td>
                                    <td>
                                        <input type="text" class="form-control" name="shipped_qty[]" id="shipped_qty" placeholder="Shipped Qty" value="{{!empty($item->shipped_qty) ? $item->shipped_qty : ''}}" data-parsley-trigger="keyup" data-parsley-type="digits">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" name="tracking_number[]" id="tracking_number" placeholder="Tracking Number" value="{{!empty($item->tracking_number) ? $item->tracking_number : ''}}" maxlength="50">
                                        <input type="hidden" name="order_detail_id[]" value="{{!empty($item->id) ? $item->id : ''}}">
                                        <input type="hidden" name="order_id" value="{{$order[0]->id}}">
                                        <input type="hidden" name="source_part_id[]" value="{{!empty($item->source_part_id) ? $item->source_part_id : ''}}">
                                        <input type="hidden" name="distributor_name[]" value="{{!empty($item->distributor_name) ? $item->distributor_name : ''}}">
                                        <input type="hidden" name="ordered_qty[]" value="{{!empty($item->quantity) ? $item->quantity : ''}}">
                                    </td>
                                    <td>
                                        <select name="shipment_status_id[]" id="shipment_status_id" class="form-control drop_down">
                                        <?php
                                        foreach($orderStatus  as $list => $value){
                                            $getShippingStatus = !empty($item->shipping_status) ? $item->shipping_status : '';
                                            $selected = ($list == $getShippingStatus) ? 'selected' : '';
                                            if($value == 'Select Status'){
                                                echo '<option value="'.$list.'">Shipping Status</option>';
                                            }else{
                                                echo '<option '.$selected.' value="'.$list.'">'.ucwords($value).'</option>';
                                            }
                                        } ?>
                                    </select>
                                    </td>
                                    <td>
                                        <input type="text" name="date_of_shipping[]" id="date_of_shipping" class="date_of_shipping form-control" placeholder="Date of Shipping" value="@if(!empty($item->date_of_shipping)){{ date('m/d/Y', strtotime($item->date_of_shipping)) }}@endif">
                                    </td>
                                    <td>
                                        <select name="tracking_website[]" class="form-control drop_down">
                                            <option value="">Select Tracking Website</option>
                                            <option value="fedex" {{($item->tracking_website == 'fedex') ? 'selected="selected"': ''}}>
                                                FedEx
                                            </option>
                                            <option value="ups" {{($item->tracking_website == 'ups') ? 'selected="selected"': ''}}>
                                                UPS
                                            </option>
                                        </select>
                                    </td>
                                    <td>
                                        <textarea name="note[]" class="form-control" id="note" maxlength="200" placeholder="Note">{{!empty($item->note) ? $item->note : ''}}</textarea>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>  
                    </div>                  
                        <!-- <table class="table">
                            <thead>
                            <th>Shipping Method Type</th>
                            <th>Shipping Account Number</th>
                            <th>Note</th>
                            <th>Request Delivery Date</th>
                            </thead>
                            <tbody>
                            <tr>
                                @if($order[0]->shipping_method_type == 1)
                                <td>UPS Ground</td>
                                @elseif($order[0]->shipping_method_type == 2)
                                <td>UPS Blue</td>
                                @elseif($order[0]->shipping_method_type ==3)
                                <td>UPS Orange</td>
                                @endif
                                <td>{{ $order[0]->shipping_account_number }}</td>
                                <td>{{ $order[0]->shipping_notes }}</td>
                                <td>{{ ($order[0]->requested_delivery_date == "0000-00-00 00:00:00" || $order[0]->requested_delivery_date == NULL) ? 'N/A' : date('m/d/Y', strtotime($order[0]->requested_delivery_date))}}</td>
                            </tr>
                            </tbody>
                        </table> -->
                    </div>
                </div>
                <input type="submit" name="submit_tracking_info" id="submit_tracking_info" class="btn btn-success pull-right" value="Submit" style="margin-right:5px;">
            </form>
        </div>
    </div>

</section>
@stop
