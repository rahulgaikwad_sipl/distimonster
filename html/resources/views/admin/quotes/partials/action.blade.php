@can('user_edit')
    <a href="{{url('/admin/quote-details/'.$quote->id)}}" class="btn btn-xs btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>
@endcan