@extends('admin.layouts.app')
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/quotes')}}">Quotes</a></li>
            <li class="active">Quote Details</li>
        </ol>
    </section>
    <br/>
<section class="content">
<!-- Default box -->
    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-md-6">
                    <h2>
                        <a href="#">{{$quoteInfo[0]->quote_name}}</a> <br />
                        <small>Created on: <strong>{{ date('l, m/d/Y', strtotime($quoteInfo[0]->created_at)) }}</strong></small><br />
                        <small>Total Parts: <strong>{{count($quoteItems)}}</strong></small><br />
                        <small>Quote Status: <strong>{{ \App\Quote::getStatusName($quoteInfo[0]->is_approved) }}</strong></small><br />
                        <small>User Name: <strong>{{ $quoteInfo[0]->name}} {{$quoteInfo[0]->last_name?$quoteInfo[0]->last_name:''}}</strong></small>
                    </h2>
                </div>
                <div class="col-md-6">
                        {{--<a href="{{ url('admin/quotes') }}">Back</a>--}}
                    <div class="pull-right">
                        <a class="btn btn-link btn-xs" href="{{ url('admin/quotes') }}"><i class="fa fa-arrow-circle-o-left" style="font-size:2.5rem;"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="approval_item" class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <h4><i class="fa fa-shopping-cart"></i> Quoted Items</h4>
                    <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>MPN</th>
                            <th>Distributor Name</th>
                            <th>Quantity</th>
                            <th>Requested Quantity</th>
                            <th>Price</th>
                            <th>Requested Per Unit Price </th>
                            <th>Requested Delivery Date</th>
                            <th>Manufacturer Name</th>
                            <th>Customer Part#</th>
                            <th>Price Total</th>
                        </tr>

                        </thead>
                        <tbody>
                            <?php
                            $quoteSubTotal= 0;
                            foreach($quoteItems as $item){
                                ?>
                            <tr>
                                <td>@if(!empty($item->item_name)){{ $item->item_name }}@else N/A @endif</td>
                                <td style="display:none;">{{ $item->source_part_id}}</td>
                                <td>@if(!empty($item->distributor_name)){{ $item->distributor_name}}@else N/A @endif</td>
                                <td>@if(!empty($item->quantity)){{ $item->quantity }}@else 0 @endif</td>
                                <td>@if(!empty($item->requested_quantity)){{ $item->requested_quantity }}@else 0 @endif</td>
                                <td>$@if(!empty($item->price)){{$item->price}} @else 0 @endif</td>
                                <td>$@if(!empty($item->requested_cost)){{$item->requested_cost}}@else 0 @endif</td>
                                <td>{{ $item->requested_delivery_date?date('m/d/Y', strtotime($item->requested_delivery_date)): 'N/A'}}</td>
                                <td>@if(!empty($item->manufacturer_name)){{ $item->manufacturer_name}}@else N/A @endif</td>
                                <td>{{ $item->customer_part_id?$item->customer_part_id:'N/A'}}</td>
                                <td>${{number_format($item->quantity*$item->price ,2, '.', ',')}}</td>
                            </tr>
                            <?php
                            $quoteSubTotal = $quoteSubTotal+ ($item->quantity * $item->price);
                            }
                            ?>
                        <tr>
                            
                            <th colspan="10" class="text-right">Subtotal: $ {{number_format($quoteSubTotal,2,'.',',')}}</th>
                        </tr>
                        </tbody>
                        @if($quoteInfo[0]->is_approved == 0)
                        <tfoot id="action-btn">
                        <tr>
                            <td colspan="10">
                                <div class="row text-right">
                                    <button data-url="{{ url('/admin/quote-reject/'.$quoteInfo[0]->id) }}" class="btn btn-danger reject_btn">Reject</button>
                                    <button class="btn btn-primary approve_btn">Reply</button>
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                        @endif
                    </table>
                </div>
                </div>
            </div>
        </div>
    </div>

    <div id="approval_form" class="box hide">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <h4><i class="fa fa-check-circle"></i> Approved Items</h4>
                    </div>
                    {!! Form::open(['url' => url('/admin/approve-quote-details/'.$quoteInfo[0]->id), 'method' => 'POST', 'id' => 'item_approve_form', 'data-parsley-validate' => true]) !!}

                    <table class="table">
                        <thead>
                        <tr>
                            <th>MPN</th>
                            <th>Distributor Name</th>
                            <th>Approved Quantity</th>
                            <th>Approved Per Unit Price</th>
                            <th>Approved Delivery Date</th>
                        </tr>

                        </thead>
                        <tbody>
                        <?php
                        $quoteSubTotal= 0;
                        foreach($quoteItems as $item){
                        ?>
                        <tr>
                            <td>{{ $item->item_name }}</td>
                            <td style="display:none;">{{ $item->source_part_id}}</td>
                            <td>{{ $item->distributor_name}}</td>
                            <td>
                                <div class="form-group">
                                    {!! Form::hidden('quote_detail_id[]', $item->id, ['class' => 'form-control']) !!}
                                    <?php $requestedQuantity = !empty($item->requested_quantity) ? $item->requested_quantity : null; ?>
                                    {!! Form::text('approved_quantity[]', $requestedQuantity, ['class' => 'form-control', 'placeholder' => 'Enter Approved Qty','data-parsley-maxlength'=>'10','data-parsley-pattern'=>'^\d+(\.\d+)?$',  'data-parsley-type'=>'digits' ,'required' => 'true']) !!}
                                    <label>Requested Quantity: </label> {{$item->requested_quantity}}
                                    <p class="help-block"></p>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    <?php $requestedCost = !empty($item->requested_cost) ? $item->requested_cost : null; ?>
                                    {!! Form::number('approved_price[]', $requestedCost , ['class' => 'form-control', 'placeholder' => 'Enter Approved Price', 'min'=>'0.001', 'step'=>'0.001', 'data-parsley-error-message'=>'Please enter valid price.', 'data-parsley-required-message'=>'Please enter price.', 'data-parsley-maxlength'=>'10', 'required' => 'true']) !!}
                                    <label>Requested Per Unit Price: </label> {{$item->requested_cost}}
                                    <p class="help-block"></p>
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    {!! Form::text('approved_delivery_date[]',null, ['class' => 'form-control approved_delivery_date', 'data-parsley-trigger'=>'onchange', 'placeholder' => 'Delivery Date', 'readonly'=>'readonly']) !!}
                                    <p class="help-block"></p>
                                </div>
                            </td>
                        </tr>
                        <?php
                        $quoteSubTotal = $quoteSubTotal+ ($item->quantity * $item->price);
                        }
                        ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <th colspan="5">
                                <div class="col-md-12 text-right">
                                    <button type="button" class="btn btn-default approval-cancle">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Reply</button>
                                </div>
                            </th>
                        </tr>
                        </tfoot>
                    </table>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

</section>
@stop

@section('javascript')
    <script>
        $(function () {
            $('.approve_btn').on('click', function(){
                $('#approval_form').removeClass('hide');
                $('#action-btn').addClass('hide');
                $('#item_approve_form').parsley().reset();
            });

            $('.approval-cancle').on('click', function(){
                $('#approval_form').addClass('hide');
                $('#action-btn').removeClass('hide');
            });

            $('.approved_delivery_date')
                .datepicker({
                    startDate: '1d',
                    format: 'dd/mm/yyyy',
                    autoclose: true,
                });

            $('.reject_btn').on('click', function () {
                var url = $(this).data('url');
                swal({
                        title: 'Are you sure you want to reject this quote?',
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        cancelButtonText: "No",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            window.location.assign(url);
                        }
                    });
            })

        })
    </script>
    @endsection
