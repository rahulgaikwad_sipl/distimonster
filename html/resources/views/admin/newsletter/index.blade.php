@inject('request', 'Illuminate\Http\Request')
@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Newsletter</li>
        </ol>
    </section>

    <h3 class="page-title">Newsletter</h3>
    <div class="row">
        <div class="col-md-12">
        <div class="box panel-default">

                {!! Form::open(['method' => 'POST', 'url'=> url('/admin/send-newsletter'), 'id'=>'sendNewsLetter','data-parsley-validate' => true]) !!}
            <div class="box-body">
                <div class="col-md-12">
                <table border="0" width="100%" class="table table-striped table-bordered table-hover testing" id="scroll-table">
                    <thead>
                    <tr>
                        <th sorting="false"><input type="checkbox" id="checkAll">&nbsp;&nbsp;All</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(count($users) > 0)
                        @foreach($users as $user)
                            <tr>
                                <td><input type="checkbox" data-parsley-required-message="Please check at-least on checkbox" data-parsley-errors-container="#check-error" name="users[]" data-parsley-required="true" value="{{ $user->subscriber_email }}" class="check" />&nbsp;&nbsp;{{ $user->subscriber_email }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
                    <span id="check-error"></span>
                </div>
                <div class="col-md-9">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Compose News Letter</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="form-group">
                                <input name="subject" data-parsley-required="true" value="{!! @$newsSetting->subject !!}" class="form-control" placeholder="Subject:">
                            </div>
                            <div class="form-group">
                                <textarea name="message" data-parsley-errors-container="#message-error" id="compose-textarea" class="form-control" style="height: 200px">{!! @$newsSetting->message !!}</textarea>
                            </div>

                            <span id="message-error"></span>
                        </div>
                        <!-- /.card-body -->
                        <div class="box-footer">
                            <a class="btn btn-default btn-close" href="{{ url('admin/home/index') }}">Cancel</a>
                                <button type="submit" class="btn btn-success"> Send</button>
                        </div>
                        <!-- /.card-footer -->
                    </div>
                    <!-- /. box -->
                </div>
            </div>
                {!! Form::close() !!}

        </div>
        </div>
    </div>
@stop

@section('javascript')
<script>
    $(document).ready(function() {
        $('#scroll-table').DataTable( {
            "scrollY":        "300px",
            "scrollCollapse": true,
            "paging":         false,
            "ordering": false
        } );

        CKEDITOR.replace( 'compose-textarea', {
            // Define the toolbar groups as it is a more accessible solution.
            toolbarGroups: [
                {"name":"basicstyles","groups":["basicstyles"]},
                {"name":"links","groups":["links"]},
                {"name":"paragraph","groups":["list","blocks"]},
                {"name":"document","groups":["mode"]},
                {"name":"insert","groups":["insert"]},
                {"name":"styles","groups":["styles"]}
            ],
            // Remove the redundant buttons from toolbar groups defined above.
            removeButtons: 'Underline,Strike,Subscript,Superscript,Anchor,Styles,Specialchar'
        } );

        CKEDITOR.on('instanceReady', function () {
            $('#compose-textarea').attr('required', '');
            $.each(CKEDITOR.instances, function (instance) {
                CKEDITOR.instances[instance].on("change", function (e) {
                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                        $('form').parsley();
                    }
                });
            });
        });

        $("#checkAll").change(function(){
            $(".check").prop('checked', $(this).prop("checked"));
        });


        $('.check').change(function(){

            if(false == $(this).prop("checked")){
                $("#checkAll").prop('checked', false);
            }

            if ($('.check:checked').length == $('.check').length ){
                $("#checkAll").prop('checked', true);
            }
        });
    } );
</script>
@endsection

