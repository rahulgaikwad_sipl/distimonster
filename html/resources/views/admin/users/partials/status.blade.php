@if($user->status == 1 )
    <a class="change-visibility" onclick="userModule.changeUserStatus('<?php echo $user->id; ?>','<?php echo $user->status; ?>' )"  class="btn btn-xs btn-info">{{config('app.status.active')}}</a>
@else
    <a class="change-visibility" onclick="userModule.changeUserStatus('<?php echo $user->id; ?>','<?php echo $user->status; ?>' )"  class="btn btn-xs btn-info">{{config('app.status.inactive')}}</a>
@endif