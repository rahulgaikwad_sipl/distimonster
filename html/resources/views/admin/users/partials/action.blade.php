@can('user_view')
<a href="{{ route('admin.users.show',[$user->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-eye" title="View BIO" aria-hidden="true"></i></a>
@endcan

@can('user_edit')
    <a href="{{ route('admin.users.edit',[$user->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o" title="Edit User Info" aria-hidden="true"></i></a>
@endcan

@can('user_delete')
    <button class="red fl btn btn-xs btn-danger" onclick="userModule.confirmUserDelete('<?php echo $user->id; ?>','Are you sure you want to delete this user?')" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button>
@endcan