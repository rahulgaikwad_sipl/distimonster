<input type="checkbox" class="toggle-switch" {{ ($user->status == 1)?'checked':'' }}
data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger"
       data-msg="Are you sure you want to {{ ($user->status == 1)?'Deactivate':'Activate' }} this user?"
       data-href="/admin/update-user-status"  data-id="{{$user->id}}"  data-status="{{$user->status}}" data-size="small"
>
      