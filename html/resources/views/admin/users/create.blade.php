@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="{{url('admin/users')}}">Users</a></li>
            @if(!empty($user))
                <li class="active">Update User</li>
            @else
                <li class="active">Add User</li>
            @endif

        </ol>
    </section>
    <br/>
    <section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="box">
                    <div class="box-header with-border">
                        @if(!empty($user))
                            <h3 class="box-title">Update User</h3>
                        @else
                            <h3 class="box-title">Add User</h3>
                        @endif
                    </div>
                    @if(!empty($user))
                        {!! Form::model($user, ['method' => 'PUT', 'route' => ['admin.users.update', $user->id],'data-parsley-validate' => true]) !!}
                    @else
                        {!! Form::open(['method' => 'POST', 'route' => ['admin.users.store'], 'data-parsley-validate' => true]) !!}
                    @endif
                        <div class="box-body">
                                <div class="form-group">
                                    <label class='control-label'> First Name <span class="text-danger">*</span> </label>
                                    {!! Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','minlength' => config('app.fields_length.name_min'),'maxlength' => config('app.fields_length.name_max'),'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE')]) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('name'))
                                        <p class="help-block">
                                            {{ $errors->first('name') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class='control-label'> Last Name <span class="text-danger">*</span> </label>
                                    {!! Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','minlength' => config('app.fields_length.name_min'),'maxlength' => config('app.fields_length.name_max'),'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE')]) !!}
                                    <p class="help-block"></p>
                                    @if($errors->has('last_name'))
                                        <p class="help-block">
                                            {{ $errors->first('last_name') }}
                                        </p>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label class='control-label'> Email <span class="text-danger">*</span> </label>
                                    <input type="text" name="email" id="email" value="{{ $user->email?:old('email') }}" placeholder="Email Address" required="" class="form-control" tabindex="6" data-parsley-required-message="Please enter your valid email." pattern="{{config('app.patterns.email')}}" data-parsley-trigger="focusout" data-parsley-remote="{{url('admin/check-newemails')}}" data-parsley-remote-message="Email address is already in use."/>
                                    <p class="help-block email_err"></p>
                                    @if($errors->has('email'))
                                        <p class="help-block">
                                            {{ $errors->first('email') }}
                                        </p>
                                    @endif
                                </div>

                            @if(empty($user))

                                    <div class="form-group">
                                        <label class='control-label'> Password <span class="text-danger">*</span> </label>
                                        {!! Form::password('password', ['id'=>'password','class' => 'form-control', 'placeholder' => '', 'required' => '','data-parsley-minlength' => config('app.fields_length.password_min'),'data-parsley-maxlength' => config('app.fields_length.password_max'),'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE')]) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('password'))
                                            <p class="help-block">
                                                {{ $errors->first('password') }}
                                            </p>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <label class='control-label'> Confirm  Password <span class="text-danger">*</span> </label>
                                        {!! Form::password('cpassword', ['class' => 'form-control', 'placeholder' => '', 'required' => '', 'data-parsley-equalto'=>'#password','data-parsley-equalto-message'=>"Confirm Password must equal to password."]) !!}
                                        <p class="help-block"></p>
                                        @if($errors->has('cpassword'))
                                            <p class="help-block">
                                                {{ $errors->first('cpassword') }}
                                            </p>
                                        @endif
                                    </div>
                            @endif
                            {!! Form::hidden('role_id', 2, ['class' => 'form-control', 'placeholder' => '']) !!}

                                <div class="form-group">
                                    <label class='control-label'> Status <span class="text-danger">*</span> </label>
                                    {!! Form::select('status', $status, !empty($user->status)? $user->status :old('status'), ['class' => 'form-control select2 status', 'data-parsley-errors-container'=>'#error_status', 'required' => '','data-parsley-required-message'=>'Please select status.']) !!}
                                    <div id="error_status"> </div>
                                    <p class="help-block"></p>
                                    @if($errors->has('status'))
                                        <p class="help-block">
                                            {{ $errors->first('status') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class='control-label'> Subscription Plan <span class="text-danger">*</span> </label>
                                    {!! Form::select('plan_type', $plans, !empty($user->plan_id)? $user->plan_id : old('plan_type'), ['class' => 'form-control select2', 'data-parsley-errors-container'=>'#error_plan_type', 'required' => '','data-parsley-required-message'=>'Please select subscription plan.']) !!}
                                    <div id="error_plan_type"> </div>
                                    <p class="help-block"></p>
                                    @if($errors->has('plan_type'))
                                        <p class="help-block">
                                            {{ $errors->first('plan_type') }}
                                        </p>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label class='control-label'> Billing <span class="text-danger">*</span> </label>
                                    {!! Form::select('billing_cycle', $billingCycle, !empty($user->billing_cycle)? $user->billing_cycle : old('billing_cycle'), ['class' => 'form-control select2', 'data-parsley-errors-container'=>'#error_billing_cycle', 'required' => '','data-parsley-required-message'=>'Please select billing.']) !!}
                                    <div id="error_billing_cycle"> </div>
                                    <p class="help-block"></p>
                                    @if($errors->has('billing_cycle'))
                                        <p class="help-block">
                                            {{ $errors->first('billing_cycle') }}
                                        </p>
                                    @endif
                                </div>
                        </div>
                    <div class="box-footer">
                        <a class="btn btn-default btn-close" href="{{ route('admin.users.index') }}">Cancel</a>
                        {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-success']) !!}
                    </div>
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
    </section>

@stop

