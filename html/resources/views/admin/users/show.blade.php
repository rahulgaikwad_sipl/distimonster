@extends('admin.layouts.app')

@section('content')
    <h3 class="page-title"><b>User Deatails: </b>{{$user->name}} {{$user->last_name}}</h3>

    <div class="panel panel-default">
        
        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
				<div class="panel-heading">
            <b>My BIO</b>
        </div>

                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Are you a?</th>
                            <td field-key='name'>{{ $bio->are_you_a }}</td>
                            @if($bio->are_you_a=='Other')
                            <td field-key='name'>{{ $bio->are_you_a_text }}</td>
                            @else
                            <td field-key='name'>&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            <th>Is your company a?</th>
                            <td field-key='email'>{{ $bio->your_company_a }}</td>
                            @if($bio->your_company_a=='Other')
                            <td field-key='name'>{{ $bio->your_company_a_text }}</td>
                            @else
                            <td field-key='name'>&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            <th>What is your main Industry?</th>
                            <td field-key='role'>{{ $bio->your_main_industry }}</td>
                            @if($bio->your_main_industry=='Other')
                            <td field-key='name'>{{ $bio->your_main_industry_text }}</td>
                            @else
                            <td field-key='name'>&nbsp;</td>
                            @endif
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td field-key='role'>{{ $user->billing_address_line1 . $user->billing_address_line2 . $user->zip_code?:'N/A' }}</td>
                            <td field-key='name'>&nbsp;</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td field-key='role'>{{ $user->contact_number?:'N/A' }}</td>
                            <td field-key='name'>&nbsp;</td>
                        </tr>
                        <tr>
                            <th>Accounts Payable Contact Name</th>
                            <td field-key='role'>{{ $user->a_c_contact_name?:'N/A' }}</td>
                            <td field-key='name'>&nbsp;</td>
                        </tr>
                        <tr>
                            <th>Accounts Payable Email</th>
                            <td field-key='role'>{{ $user->a_c_contact_email?:'N/A' }}</td>
                            <td field-key='name'>&nbsp;</td>
                        </tr>
                    </table>
                </div>
				  <div class="col-md-6">
				  <div class="panel-heading"><b>Company Information</b></div>

                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Company Name</th>
                            <td field-key='name'>{{ $user->company_name?:'N/A' }}</td>
							 <th>Year of Established</th>
                            <td field-key='name'>{{ $user->year_of_establish?:'N/A' }}</td>
                           
                        </tr>
                         <tr>
                            <th>Number of Employee</th>
                            <td field-key='name'>{{ $user->employee?:'N/A' }}</td>
							 <th> Company Type</th>
                            <td field-key='name'>{{ $user->company_type?:'N/A' }}</td>
                           
                        </tr>
                         <tr>
                            <th>Annual Component Purchase</th>
                            <td field-key='name'>{{ $user->annual_component_purchase?:'N/A' }}</td>
							 <th>How you found Distimonster</th>
                            <td field-key='name'>{{ $user->know_about_distimonster?:'N/A' }}</td>
                           
                        </tr>
                    </table>
                  <div class="panel-heading"><b>Subscription Information</b></div>

                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Subscription Date</th>
                            <td field-key='name'>{{date('m/d/Y', strtotime($user->plan_purchase_date))}}</td>
						</tr>
                        <tr>
                            <th>Subscription Expiry Date</th>
                            <td field-key='name'>{{date('m/d/Y', strtotime($user->plan_expiry_date))}}</td>
						</tr>
                        <tr>
                            <th>Subscription Free Trial Expiry Date</th>
                            <td field-key='name'>{{date('m/d/Y', strtotime($user->free_trial_expiry))}}</td>
						</tr>
                    </table>  
                </div>
            </div>
			
            <h3 style="margin-bottom:20px;">Team Members</h3>
            <table id="subUserListing" class="table table-striped table-bordered table-hover testing">
                                                    <thead>
                                                        <tr>
                                                            <th>First Name</th>
                                                            <th>Last Name</th>
                                                            <th>Email</th>
                                                            <th>Note</th>
                                                            <th>Subscription Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($subUserList AS $subUser)
                                                        <tr>
                                                            <td>{{$subUser->name}}</td>
                                                            <td>{{$subUser->last_name}}</td>
                                                            <td>{{$subUser->email}}</td>
                                                            <td>@if(isset($subUser->note) && !empty($subUser->note))
                                                                    {{
                                                                        $subUser->note
                                                                    }}
                                                                @else
                                                                    NA
                                                                @endif
                                                            </td>
                                                            <td>{{date('m/d/Y', strtotime($subUser->plan_purchase_date))}}</td>
                                                        </tr>
                                                     @endforeach      
                                                    </tbody>
                                                </table>
            <p>&nbsp;</p>

            <a href="{{ route('admin.users.index') }}" class="btn btn-default">@lang('quickadmin.qa_back_to_list')</a>
        </div>
    </div>
@stop
@section('javascript')
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#subUserListing').DataTable();
    });
</script>
@endsection