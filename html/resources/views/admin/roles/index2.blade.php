@inject('request', 'Illuminate\Http\Request')
@extends('admin.layouts.app')
@section('content')
<h3 class="page-title">@lang('quickadmin.news.title')</h3>
@can('user_create')
<p>
    <a href="{{ route('admin.news.create') }}" class="btn btn-success">@lang('quickadmin.qa_add_new')</a>
</p>
@endcan

<div class="panel panel-default">
    <div class="panel-heading">
        @lang('quickadmin.qa_list')
    </div>

    <div class="panel-body table-responsive">
        <table border="0" width="100%"  class="table table-striped table-bordered table-hover testing" id="roleTable" >
            <thead>
                <tr>
                    <th>@lang('quickadmin.roles.fields.title')</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@stop

@section('javascript') 
<script>
    $(document).ready(function() {
      /* $('#roleTable').DataTable({
        processing: true,
        order: [[ 1, "asc" ]],
        serverSide: true,
        ajax: BASE_URL+'/admin/role/role-list',
        columns: [
        {data: 'title', name: 'title',className: "text-left"},
        {data: 'action', name: 'action',orderable: false,searchable: false,className: "text-center"},
        ]
    });*/
   });
  
</script>
@endsection
