@if($requestData->status == 0)
<a class='label label-info' title="Click To Change Status"  onclick="netTermRequestModule.changeRequestStatus('<?php echo  $requestData->status  ?>',  '<?php echo $requestData->id ?>', '<?php echo $requestData->net_account_type ?>')"  href="javascript:void(0);">Pending</a>
@endif

@if($requestData->status == 1)
    <div class='label label-success' title="Approved"  href="javascript:void(0);">Approved</div>
@endif

@if($requestData->status == 2)
    <a class='label label-warning' title="Click To Change Status"  onclick="netTermRequestModule.changeRequestStatus('<?php echo  $requestData->status  ?>',  '<?php echo $requestData->id ?>', '<?php echo $requestData->net_account_type ?>')"  href="javascript:void(0);">In Progress</a>
@endif

@if($requestData->status == 3)
    <a class='label label-danger' title="Rejected" href="javascript:void(0);">Rejected</a>
@endif