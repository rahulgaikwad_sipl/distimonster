@inject('request', 'Illuminate\Http\Request')
@extends('admin.layouts.app')
@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li class="active">Promo Code</li>
        </ol>
    </section>
    <h3 class="page-title">Promo Codes</h3>
    <div class="row">
        <div class="col-md-12">

            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="status_id" class="control-label">Status: </label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="status_id" id="status_id" class="form-control drop_down">
                                        <option value="">Select Status</option>
                                        <?php
                                        foreach($couponsStatus  as $list => $value){
                                            echo '<option value="'.$list.'">'.ucwords($value).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                        </div>
                        <div class="col-lg-2 pull-right">
                            @can('user_create')
                                <div class="form-group  label-static">
                                    <a href="{{ route('admin.coupons.create') }}" class="btn btn-success">Add New Promo Code</a>
                                </div>
                            @endcan
                        </div>
                    </div>

                        <table border="0" width="100%" class="table table-striped table-bordered table-hover testing" id="couponTable">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Code</th>
                                <th>Discount Percentage</th>
                                <th>Order Limit</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>

                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript') 
    <script>
        $(document).ready(function() {
            couponModule.loadCouponDataTable();
                $(document).on('change', '.toggle-switch', function () {
                    var msg = $(this).data('msg');
                    var url = $(this).data('href');
                    var id = $(this).data('id');
                    var status = $(this).data('status');
                    var $this = $(this);
                    var state = $this.prop('checked');
                    $this.prop('checked', !state).bootstrapToggle('destroy').bootstrapToggle();
                    swal({
                            title: msg,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes",
                            cancelButtonText: "No",
                            closeOnConfirm: true,
                            closeOnCancel: true
                        },
                        function (isConfirm) {
                            if (isConfirm) {
                                couponModule.changeCouponStatus(id,status,url)
                            }
                        });
                });
            });
        $(document).on('change', '#status_id', function(){
            couponModule.loadCouponDataTableAgain();
        });
        @can('user_delete')
            window.route_mass_crud_entries_destroy = '{{ route('admin.users.mass_destroy') }}';
        @endcan
    </script>
@endsection