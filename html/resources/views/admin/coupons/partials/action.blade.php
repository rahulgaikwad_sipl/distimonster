@can('user_edit')
    <a href="{{ route('admin.coupons.edit',[$coupon->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
@endcan

@can('user_delete')
    <button class="red fl btn btn-xs btn-danger" onclick="couponModule.confirmCouponDelete('<?php echo $coupon->id; ?>','Are you sure you want to delete this coupon ?')" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button>
@endcan