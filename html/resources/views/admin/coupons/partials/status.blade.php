<input type="checkbox" class="toggle-switch" {{ ($coupon->status == 1)?'checked':'' }}
data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger"
       data-msg="Are you sure you want to {{ ($coupon->status == 1)?'Deactivate':'Activate' }} this coupon ?"
       data-href="/admin/update-coupon-status"  data-id="{{$coupon->id}}"  data-status="{{$coupon->status}}" data-size="small"
>