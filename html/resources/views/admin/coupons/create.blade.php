@extends('admin.layouts.app')

@section('content')
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="{{url('admin/coupons')}}">Promo Code</a></li>
            @if(!empty($coupon))
                <li class="active">Update Promo Code</li>
            @else
                <li class="active">Add Promo Code</li>
            @endif
        </ol>
    </section>
    <br/>
    <section class="content">
        <div class="row">
        <div class="col-md-12">

            <div class="box">

                    <div class="box-header with-border">
                        @if(!empty($coupon))
                            <h3 class="box-title">
                                Update Promo Code
                            </h3>
                        @else
                            <h3 class="box-title">
                                Add Promo Code
                            </h3>
                        @endif
                    </div>
                    @if(isset($coupon) && count($coupon->toArray()) > 0)
                        {!! Form::model($coupon, ['method' => 'PUT', 'route' => ['admin.coupons.update', $coupon->id],'data-parsley-validate' => true]) !!}
                    @else
                        {!! Form::open(['method' => 'POST', 'route' => ['admin.coupons.store'], 'data-parsley-validate' => true]) !!}
                    @endif
                <div class="box-body">

                    <div class="form-group">
                        <label class='control-label'> Promo Code <span class="text-danger">*</span> </label>
                        {!! Form::text('code', old('code'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','minlength' => config('app.fields_length.name_min'),'maxlength' => config('app.fields_length.name_max')]) !!}
                        <p class="help-block"></p>
                        @if($errors->has('code'))
                            <p class="help-block">
                                {{ $errors->first('code') }}
                            </p>
                        @endif
                    </div>

                    <div class="form-group">
                        {!! Form::checkbox('is_free', 1, (isset($coupon)&& ($coupon->is_free==1)) ? true : false, ['class' => 'is_free_shipping']) !!}
                        {!! Form::label('is_free', 'Its a free shipping promo code') !!}
                    </div>
                    <div class="form-group">
                            <label class='control-label'> Discount Percentage <span class="text-danger">*</span> </label>

                            {!! Form::text('discount_percentage', old('discount_percentage'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','pattern' => config('app.patterns.number'),'data-parsley-type'=>'digits', 'data-parsley-min'=>0,'max'=>99]) !!}
                            <p class="help-block"></p>
                            @if($errors->has('discount_percentage'))
                                <p class="help-block">
                                    {{ $errors->first('discount_percentage') }}
                                </p>
                            @endif

                    </div>
                    <div class="form-group">

                            <label class='control-label'> Order Limit <span class="text-danger">*</span> </label>
                            {!! Form::text('order_limit', old('order_limit'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','pattern' => config('app.patterns.number'),'min'=>1]) !!}
                            <p class="help-block"></p>
                            @if($errors->has('order_limit'))
                                <p class="help-block">
                                    {{ $errors->first('order_limit') }}
                                </p>
                            @endif
                    </div>
                    <div class="form-group">

                            <label class='control-label'> Minimum Cart Amount <span class="text-danger">*</span> </label>
                            {!! Form::text('minimum_cart_amount', old('minimum_cart_amount'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','pattern' => config('app.patterns.number'),'min'=>1]) !!}
                            <p class="help-block"></p>
                            @if($errors->has('minimum_cart_amount'))
                                <p class="help-block">
                                    {{ $errors->first('minimum_cart_amount') }}
                                </p>
                            @endif

                    </div>
                    <div class="form-group">
                        {!! Form::label('start_date', 'Start Date', ['class' => 'control-label']) !!}<span class="text-danger">*</span>
                        {!! Form::text('start_date' , (isset($coupon)) ? date('m/d/Y', strtotime($coupon->start_date)) : '' , ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('start_date'))
                                <p class="help-block">
                                    {{ $errors->first('start_date') }}
                                </p>
                            @endif

                    </div>


                    <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::label('end_date', 'End Date', ['class' => 'control-label']) !!}<span class="text-danger">*</span>
                            {!! Form::text('end_date', (isset($coupon)) ? date('m/d/Y', strtotime($coupon->end_date)) : '' , ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('end_date'))
                                <p class="help-block">
                                    {{ $errors->first('end_date') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    {!! Form::hidden('role_id', 2, ['class' => 'form-control', 'placeholder' => '']) !!}
                    <div class="row">
                        <div class="col-xs-12 form-group">
                            <label class='control-label'>Status<span class="text-danger">*</span> </label>
                            {!! Form::select('status', $status, !empty($coupon->status)? $coupon->status :'', ['class' => 'form-control select2 status','data-parsley-errors-container'=>'#error_status', 'required' => '']) !!}
                            <div id="error_status"> </div>
                            <p class="help-block"></p>
                            @if($errors->has('status'))
                                <p class="help-block">
                                    {{ $errors->first('status') }}
                                </p>
                            @endif
                        </div>
                    </div>

                </div>
                <div class="box-footer">
                    <a class="btn btn-default btn-close" href="{{ route('admin.coupons.index') }}">Cancel</a>
                    {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-success']) !!}
                </div>
                    {!! Form::close() !!}

            </div>
        </div>
    </div>
    </section>

@stop