@extends('admin.layouts.app')

@section('content')
<h3 class="page-title">@lang('quickadmin.news.title')</h3>

{!! Form::model($news, ['method' => 'PUT', 'route' => ['admin.news.update', $news->id], 'enctype' =>"multipart/form-data"]) !!}

<div class="panel panel-default">
    <div class="panel-heading">
        @lang('quickadmin.qa_edit')
    </div>

    <div class="panel-body">
        <div class="row">
            <div class="col-xs-12 form-group">
                {!! Form::label('title', trans('quickadmin.news.fields.title').'*', ['class' => 'control-label']) !!}
                {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                <p class="help-block"></p>
                @if($errors->has('title'))
                <p class="help-block">
                    {{ $errors->first('title') }}
                </p>
                @endif
            </div>

            <div class="col-xs-12 form-group">
                {!! Form::label('description', trans('quickadmin.news.fields.description').'*', ['class' => 'control-label']) !!}
                {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => '', 'required' => '', 'id' => 'description']) !!}
                <p class="help-block"></p>
                @if($errors->has('description'))
                <p class="help-block">
                    {{ $errors->first('description') }}
                </p>
                @endif
            </div>

            {!! Form::label('image', trans('quickadmin.news.fields.image').'*', ['class' => 'control-label']) !!}
            {!! Form::file('image', ['class' => 'form-control', 'placeholder' => '', 'id' => 'image']) !!}
            @if($news->image)
            <img width="100" src="{{url('/uploads/'.$news->image)}}" />
            @endif
            @if($errors->has('image'))
            <p class="help-block">
                {{ $errors->first('image') }}
            </p>
            @endif
            <div id="error_image"> </div>
            {!! Form::label('status', 'Status*', ['class' => 'control-label']) !!}
            {!! Form::select('status', $status, !empty($news->status)? $news->status :'', ['class' => 'form-control select2', 'required' => '']) !!}
            @if($errors->has('status'))
                <p class="help-block">
                    {{ $errors->first('status') }}
                </p>
            @endif
        </div>

    </div>
</div>

{!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
{!! Form::close() !!}
@stop
@section('javascript')
<script>
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('description')
})
</script>
@endsection
