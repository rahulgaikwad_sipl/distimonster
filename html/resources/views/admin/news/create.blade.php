@extends('admin.layouts.app')

@section('content')

    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="{{url('admin/news')}}">News</a></li>
            @if(!empty($news))
                <li class="active">Update News</li>
            @else
                <li class="active">Add News</li>
            @endif
        </ol>
    </section>
    <br/>
    <section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="box">

                    <div class="box-header with-border">
                        @if(!empty($news))
                        <h3 class="box-title">Update News</h3>
                        @else
                            <h3 class="box-title">Add News</h3>
                        @endif
                    </div>
                    @if(!empty($news))
                        {!! Form::model($news, ['method' => 'PUT', 'route' => ['admin.news.update', $news->id], 'enctype' =>"multipart/form-data",'data-parsley-validate' => true]) !!}
                    @else
                        {!! Form::open(['method' => 'POST', 'route' => ['admin.news.store'], 'enctype' =>"multipart/form-data", 'data-parsley-validate' => true ]) !!}
                    @endif
                        <div class="box-body">
                            <div class="form-group">
                                <label class='control-label'> Title <span class="text-danger">*</span> </label>
                                {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '','minlength' => config('app.fields_length.name_min'),'maxlength' => 120, 'data-parsley-required','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE')]) !!}
                                @if($errors->has('title'))
                                    <p class="help-block">
                                        {{ $errors->first('title') }}
                                    </p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label class='control-label'> Description <span class="text-danger">*</span> </label>
                                {!! Form::textarea('description', old('description'), ['class' => 'form-control', 'placeholder' => '', 'data-parsley-errors-container'=>'#error_description', 'id' => 'description','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE')]) !!}
                                @if($errors->has('description'))
                                    <p class="help-block">
                                        {{ $errors->first('description') }}
                                    </p>
                                @endif
                                <div id="error_description"> </div>
                            </div>
                            <div class="form-group">
                                <label class='control-label'> Image  </label>
                                {!! Form::file('image', ['class' => 'form-control', 'placeholder' => '', 'id' => 'image', 'data-parsley-image-file'=>'png,jpg','data-parsley-max-file-size'=>"1"]) !!}
                                @if(!empty($news))
                                    @if(file_exists(url('/uploads/'.$news->image)))
                                        <img width="100" src="{{url('/uploads/'.$news->image)}}" />
                                    @else
                                        <img width="80px" src="{{url('/uploads/image_not_available.jpg')}}" />
                                    @endif
                                @endif
                                @if($errors->has('image'))
                                    <p class="help-block">
                                        {{ $errors->first('image') }}
                                    </p>
                                @endif
                                <div id="error_image"> </div>
                            </div>
                            <div class="form-group">
                                <label class='control-label'> Status <span class="text-danger">*</span> </label>
                                {!! Form::select('status', $status, !empty($news->status)? $news->status :'', ['class' => 'form-control select2 status','data-parsley-errors-container'=>'#error_status', 'required' => '']) !!}
                                <div id="error_status"> </div>
                                @if($errors->has('status'))
                                    <p class="help-block">
                                        {{ $errors->first('status') }}
                                    </p>
                                @endif
                            </div>

                        </div>
                        <div class="box-footer">
                            <a class="btn btn-default btn-close" href="{{ route('admin.news.index') }}">Cancel</a>
                            {!! Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-success']) !!}
                        </div>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
    </section>


@stop
@section('javascript')
<script>
  $(function () {
    CKEDITOR.replace('description');
    });
</script>
<script>
        CKEDITOR.on('instanceReady', function () {
            $('#description').attr('required', '');
            $.each(CKEDITOR.instances, function (instance) {
                CKEDITOR.instances[instance].on("change", function (e) {
                    for (instance in CKEDITOR.instances) {
                        CKEDITOR.instances[instance].updateElement();
                        $('form').parsley().validate();
                    }
                });
            });
        });
    </script>
@endsection