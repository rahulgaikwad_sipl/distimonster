@extends('admin.layouts.app')

@section('content')

    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="{{url('admin/home')}}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="{{url('admin/news')}}">News</a></li>
            <li class="active">News Details</li>
        </ol>
    </section>
<br/>
    <section class="content">
        <div class="row">
            <div class="col-md-12">

                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="page-title">@lang('quickadmin.news.title')</h3>
                    </div>
                    <div class="box-body table-responsive">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="col-sm-4 control-label">@lang('quickadmin.news.fields.title')</label>
                                <div class="col-sm-8">{{ $news->title }}</div>
                            </div>
                            <div class="col-md-12">
                                <label class="col-sm-4 control-label">@lang('quickadmin.news.fields.description')</label>
                                <div class="col-sm-8">{!! $news->description !!}</div>
                            </div>
                            @if($news->image)
                                <div class="col-md-12">
                                    <label class="col-sm-4 control-label">@lang('quickadmin.news.fields.image')</label>
                                    <div class="col-sm-8"> 
                                        @if(file_exists(url('/uploads/'.$news->image)))
                                            <img width="300px" src="{{url('/uploads/'.$news->image)}}" />
                                        @else
                                            <img width="80px" src="{{url('/uploads/image_not_available.jpg')}}" />
                                        @endif
                                    </div>
                                </div>
                            @endif
                        </div><!-- Nav tabs -->

                        <p>&nbsp;</p>

                        <a href="{{ route('admin.news.index') }}" class="btn btn-default btn-close">@lang('quickadmin.qa_back_to_list')</a>
                    </div>

                </div>
            </div>
        </div>
@stop
