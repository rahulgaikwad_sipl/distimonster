<input type="checkbox" class="toggle-switch" {{ ($news->status == 1)?'checked':'' }}
data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger"
       data-msg="Are you sure you want to {{ ($news->status == 1)?'Deactivate':'Activate' }} this news."
       data-href="/admin/update-news-status"  data-id="{{$news->id}}"  data-status="{{$news->status}}" data-size="small"
>
      