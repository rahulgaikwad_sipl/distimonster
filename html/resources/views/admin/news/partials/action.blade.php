@can('role_view')
    <a href="{{ route('admin.news.show',[$news->id]) }}" class="btn btn-xs btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>
@endcan
@can('role_edit')
    <a href="{{ route('admin.news.edit',[$news->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
@endcan
@can('role_delete')
    <button class="red fl btn btn-xs btn-danger" onclick="userModule.confirmNewsDelete('<?php echo $news->id; ?>','Are you sure you want to delete this news?')" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button>
@endcan