<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;
use App\Mail\AdminAddUser;

class AdminAddNewUserMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $data;
    public $subject;
    public $to  ;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($to,$data,$subject)
    {
        $this->to       = $to;
        $this->data     = $data;
        $this->subject  = $subject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->to)->send(new AdminAddUser($this->data,$this->subject));
    }
}
