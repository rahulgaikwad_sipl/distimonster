<?php
use App\Order;
use App\ShippingAddress;
use App\Jobs\SendOrderDetailMailJob;
use App\Mail\SendOrderDetailMail;
use Carbon\Carbon;
class Helpers
{
  public static function sessionFlush(){

         $allSearchProducts = Session::get('searchProducts');
         Auth::logout();
         Session::flush('success', 'Some goodbye message');
         Session(['searchProducts' => $allSearchProducts]);
     }
     public static function curlRequest($url)
     {
         $curl = curl_init();
         curl_setopt_array($curl, array(
             CURLOPT_URL => $url,
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_ENCODING => "",
             CURLOPT_TIMEOUT => 30000,
             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_CUSTOMREQUEST => "GET",
             CURLOPT_HTTPHEADER => array(
                 // Set Here Your Requesred Headers
                 'Content-Type: application/json',
             ),
         ));
         $response = curl_exec($curl);
         $err = curl_error($curl);
         curl_close($curl);
         if ($err) {
             return $err;
         } else {
             return $response;
         }

     }
     
     public static function curlRequestOrbweaver($url)
     {
         $curl = curl_init();
         curl_setopt_array($curl, array(
             CURLOPT_URL => $url,
             CURLOPT_RETURNTRANSFER => true,
             CURLOPT_ENCODING => "",
             CURLOPT_TIMEOUT => 30000,
             CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
             CURLOPT_CUSTOMREQUEST => "GET",
             CURLOPT_HTTPHEADER => array(
                 // Set Here Your Requesred Headers
                 'Content-Type: application/json',
                 'x-orbweaver-licensekey: PVDTF-75BQH-SALXB-5A9MM-904Y2',
             ),
         ));
         $response = curl_exec($curl);
         $err = curl_error($curl);
         curl_close($curl);
         if ($err) {
             return $err;
         } else {
             return $response;
         }

     }

     public static function  getOrderDetailsById($id){
         $orderDetails= DB::table('order_details')
             ->where('order_details.order_id', '=', $id)
             ->select('order_details.id','order_details.item_name','order_details.quantity','order_details.price','order_details.manufacturer_name','order_details.source_part_id','order_details.customer_part_id','order_details.is_quote_item','order_details.quote_id','quotes.quote_name')
             ->leftjoin('quotes','order_details.quote_id','=','quotes.id')
             ->get()->toArray();
         return $orderDetails;
     }

     public static function getLowestPrice($sources){
         $allMinPriceQtyArray = array();
         $lowestPrice = 0;
         $i = 0;
         foreach($sources['sourcesPart'] as $sellerInfo){
             //Remove check if item  in stock
                if(!empty($sellerInfo['Prices']['resaleList'])){
                    $sourcePartMinPriceArray['minQty']         =  $sellerInfo['Prices']['resaleList'][$i]['minQty'];
                    $sourcePartMinPriceArray['displayPrice']   =  !empty($sellerInfo['Prices']['resaleList'][$i]['displayPrice']) ? $sellerInfo['Prices']['resaleList'][$i]['displayPrice'] : 0;
                    $allMinPriceQtyArray []   = $sourcePartMinPriceArray;
                }else{
                    $allMinPriceQtyArray [] =  [];
                }
         }
         if(empty($allMinPriceQtyArray)){
             return $lowestPrice;
         }else{
             
             $numbers = array_column($allMinPriceQtyArray, 'displayPrice');
             if(count($numbers) == 1){
              return  $lowestPrice =  min($numbers);
             }else{
                $retur = !empty(array_diff(array_map('floatval', $numbers), array(0)))?array_diff(array_map('floatval', $numbers), array(0)):[0.00];
               return $lowestPrice  = min($retur);
             }

         }
     }


     public static function getDataSheet($manufacturerPartNumber,$manufacturerName){
        $partDetails      = \DB::select("CALL searchManufacturer('$manufacturerPartNumber','$manufacturerName')");
        return  json_decode(json_encode($partDetails), True);
     }


     public  static function update_price(&$item, $key) {
         $incPercent   =    config('constants.PRICE_INCREMENT');
         if ($key == 'displayPrice') {
             $item  =  Helpers::calculatePercent($item,$incPercent);
         }
         if ($key == 'price') {
             $item   = Helpers::calculatePercent($item,$incPercent);
         }
     }

     public static function calculatePercent($price, $percentage){
         $actualPrice            =   number_format($price, 3, '.', '');
         $calculatePercentValue  = ($actualPrice*$percentage)/100;
         $incrementedPrice       = ($calculatePercentValue + $actualPrice);
         return  number_format($incrementedPrice, 3, '.', '');
     }

     public static function uploadNetTermDoc($fileDir, $file, $fileOriginalName){
         if (!file_exists($fileDir)){
             mkdir($fileDir,  0777, true);
             $uploaded                          =  $file->move(base_path($fileDir), $fileOriginalName);
             $uploadedFilePathName              = $fileOriginalName;
         }
         else{
             $uploaded                           = $file->move(base_path($fileDir), $fileOriginalName);
             $uploadedFilePathName               = $fileOriginalName;
         }
         return $uploadedFilePathName;
     }

     public static function sendOrderSuccessMail($id){
         $data['order']              = Order::getOrderInfoById($id);
         $data['shippingAddress']    = ShippingAddress::getShippingAddressById($data['order'][0]->shipping_address_id);
         $data['orderDetail']        = Order::getOrderDetailsById($data['order'][0]->id);
         $subject   = 'DistiMonster: Order Placed Successfully';
          Mail::to($data['order'][0]->email)->send(new SendOrderDetailMail($data,$subject));
     }

     public static function getBillingAddressByUserId($userId){
             $billingAddress = DB::table('users')
                 ->where('users.id', '=', $userId)
                 ->select('users.name','users.last_name','users.billing_address_line1','users.billing_address_line2','users.zip_code', 'countries.name AS country_name','states.name AS state_name','cities.name AS city_name')
                 ->leftJoin('countries','users.country_id','=','countries.id')
                 ->leftJoin('cities','users.city','=','cities.id')
                 ->leftJoin('states','users.state_id','=','states.id')
                 ->get()->toArray();
             return $billingAddress;
         }

     /*
      * Method to strip tags globally.
     */
     public static function globalXssClean()
     {
         // Recursive cleaning for array [] inputs, not just strings.
         $sanitized = static::arrayStripTags(Request::all());
         Request::merge($sanitized);
     }

     /**
      * Method to strip tags
      *
      * @param $array
      * @return array
      */
     public static function arrayStripTags($array)
     {
         $result = array();

         foreach ($array as $key => $value) {
             // Don't allow tags on key either, maybe useful for dynamic forms.
             $key = strip_tags($key);

             // If the value is an array, we will just recurse back into the
             // function to keep stripping the tags out of the array,
             // otherwise we will set the stripped value.
             if (is_array($value)) {
                 $result[$key] = static::arrayStripTags($value);
             } else {
                 // I am using strip_tags(), you may use htmlentities(),
                 // also I am doing trim() here, you may remove it, if you wish.
                 $result[$key] = trim(strip_tags($value));
             }
         }

         return $result;
     }

     /**
      * Escape output
      *
      * @param $value
      * @return string
      */
     public static function sanitizeOutput($value)
     {
         return addslashes($value);
     }

     /**
      * Send success ajax response
      *
      * @param string $message
      * @param array $result
      * @return array
      */
     public static function sendSuccessAjaxResponse($message = '', $result = [])
     {
         $response = [
             'status'    => true,
             'message'   => $message,
             'data'      => $result
         ];

         return $response;
     }

     /**
      * Send failure ajax response
      *
      * @param string $message
      * @return array
      */
     public static function sendFailureAjaxResponse($message = '')
     {
         $message = $message == '' ? config('app.message.default_error') : $message;

         $response = [
             'status'    => false,
             'message'   => $message
         ];

         return $response;
     }

     /**
      * Upload file
      * @param $file
      * @param $dir
      * @return mixed|string
      */
     public static function uploadFiles($file, $dir)
     {
         $date = new DateTime();
         $currentTimeStamp = $date->getTimestamp();
         $fileOriginalName = $currentTimeStamp.'_'.$file->getFilename().'.'.$file->getClientOriginalExtension();


         //Store file to folder
         $file->move($dir, $fileOriginalName);
         $finalDoc = $fileOriginalName;

         //Get file mime type
         $mimeType = $file->getClientMimeType();

         //Optimize if uploaded file is image
         if(self::checkImageMimes($mimeType)) {
             $imageToResize = $dir . $fileOriginalName;

             $imageName = pathinfo($fileOriginalName, PATHINFO_FILENAME);
             $finalDoc = $fileOriginalName;

             //Convert png to jpg
             if ($mimeType == 'image/png') {
                 $convertedFile = $imageName . '.jpg';
                 $convertedFilePath = $dir . $convertedFile;
                 $imageToResize = self::convertToJpg($imageToResize, $convertedFilePath);
                 $finalDoc = $convertedFile;
             }

             //Resize image if width is greater than 1200
             $imageWidth = self::getImageWidth($imageToResize);

             if ($imageWidth > 1200) {
                 $finalDoc = self::resizeImage($imageToResize);
             }
         }

         return $finalDoc;
     }

     /**
      * Create thumbnail of image
      * @param $existingImageDir
      * @param $existingImageFile
      * @param $thumbnailDir
      * @return mixed
      */
     public static function createImageThumbnail($existingImageDir, $existingImageFile, $thumbnailDir, $onlyThumb = false)
     {
         if ($onlyThumb) {
             $thumbnailToResize = $existingImageDir.$existingImageFile;

         } else {
             $thumbnailToResize = $thumbnailDir.$existingImageFile;
             //Copy original image to thumbnail folder
             File::copy($existingImageDir.$existingImageFile, $thumbnailToResize);
         }

         //Get image width
         $imageWidth = self::getImageWidth($thumbnailToResize);

         //Get image height
         $imageHeight = self::getImageHeight($thumbnailToResize);

         $imageToCreateThumb = imagecreatetruecolor(150, 150);
         $thumbImage = imagecreatefromjpeg($thumbnailToResize);
         imagecopyresampled($imageToCreateThumb, $thumbImage, 0, 0, 0, 0, 150, 150, $imageWidth, $imageHeight);

         // Output
         imagejpeg($imageToCreateThumb, $thumbnailToResize, 100);

         return $existingImageFile;
     }

     /**
      * Check if given mime is available in allowed mimes list
      * @param $mime
      * @return bool
      */
     public static function checkImageMimes($mime)
     {
         $mimes = array('image/jpeg',
             'image/jpg',
             'image/bmp',
             'image/png'
         );

         if(in_array($mime, $mimes)){
             return true;
         }
         return false;
     }

     /**
      * Convert image to jpg
      * @param $imageToConvert
      * @param $convertedFile
      * @return string
      */
     public static function convertToJpg($imageToConvert, $convertedFile)
     {
         $img = Image::make($imageToConvert)->encode('jpg', 80)->save($convertedFile);
         unlink($imageToConvert);
         return $img->dirname.'/'.$img->basename;
     }

     /**
      * Get image width
      * @param $image
      * @return mixed
      */
     public static function getImageWidth($image)
     {
         return Image::make($image)->width();
     }

     /**
      * Get image height
      * @param $image
      * @return mixed
      */
     public static function getImageHeight($image) {
         return Image::make($image)->height();
     }

     /**
      * Resize image
      * @param $fileToResize
      * @return mixed
      */
     public static function resizeImage($imageToResize)
     {
         $img = Image::make($imageToResize)->resize(1200, null)->encode('jpg', 80)->save();
         return $img->basename;
     }

     /*
      * Convert date
      */
     public static function convertDate($convertDate)
     {
         if($convertDate !=''){
             $convertDate = str_replace('/', '-', $convertDate);
             return date('Y-m-d', strtotime($convertDate));
         }
     }

     /*
      * Convert date
      */
     public static function convertDateMDY($convertDate)
     {
         if($convertDate !=''){
             return date('Y-m-d', strtotime($convertDate));
         }
     }

     /**
      * function for send email
      */
     public static function sendEmail($template, $data, $toEmail, $toName, $subject, $fromName = '', $fromEmail = '')
     {
         \Mail::send($template, $data, function ($message) use($toEmail, $toName, $subject, $data, $fromName, $fromEmail) {
             $message->to($toEmail, $toName);
             $message->subject($subject);

             if($fromEmail !='' && $fromName !='') {
                 $message->from($fromEmail, $fromName);
             }
         });
     }



     public static function checkItemsInArray($capital_item_consumptions)
     {
         $itemCount =0;
         foreach ($capital_item_consumptions as $i=>$category){
             foreach ($category['category_items'] as $j=>$items){
                 if(count($items)){
                     $itemCount = $itemCount +count($items);
                 }
             }
         }
         return $itemCount;
     }

     public static function arraySortByKey(&$array, $key) {
         $sorter=array();
         $ret=array();
         reset($array);
         foreach ($array as $ii => $va) {
             $sorter[$ii]=$va[$key];
         }
         asort($sorter);
         foreach ($sorter as $ii => $va) {
             $ret[$ii]=$array[$ii];
         }
         $array=$ret;
     }

     /**
      * function to create index array with named key of other array
      * @param  array $array
      * @param string $key
      * @return array
      *///


     public static function arrayGroupByKey(array $array, $key)
     {
         if (!is_string($key) && !is_int($key) && !is_float($key) && !is_callable($key) ) {
             trigger_error('array_group_by(): The key should be a string, an integer, or a callback', E_USER_ERROR);
             return null;
         }
         $func = (!is_string($key) && is_callable($key) ? $key : null);
         $_key = $key;
         // Load the new array, splitting by the target key
         $grouped = [];
         foreach ($array as $value) {
             $key = null;
             if (is_callable($func)) {
                 $key = call_user_func($func, $value);
             } elseif (is_object($value) && isset($value->{$_key})) {
                 $key = $value->{$_key};
             } elseif (isset($value[$_key])) {
                 $key = $value[$_key];
             }
             if ($key === null) {
                 continue;
             }
             $grouped[$key][] = $value;
         }
         // Recursively build a nested grouping if more parameters are supplied
         // Each grouped array value is grouped according to the next sequential key
         if (func_num_args() > 2) {
             $args = func_get_args();
             foreach ($grouped as $key => $value) {
                 $params = array_merge([ $value ], array_slice($args, 2, func_num_args()));
                 $grouped[$key] = call_user_func_array('array_group_by', $params);
             }
         }
         return $grouped;
     }
     
     public static function  getCountryStateCityName($tableName, $getField, $whereField, $whereFieldValue){
         $dynamicDetails= DB::table($tableName)
                                         ->where($whereField, '=', $whereFieldValue)
                                         ->select($getField)
                                         ->first();
         return $dynamicDetails->$getField;
     }
	 public static function getShippingOption(){
		  $shippingOption = DB::table('shipping_option')
                 ->where('status', '=', 1)
                 ->pluck('name','id')->toArray();
             return $shippingOption;
	 }
    public static function getShippingOptionById($id){
		  $shippingname = DB::table('shipping_option')
		         ->select('name')
                 ->where('id', '=', $id)
                 ->first();
             return $shippingname;
    }
    public static function viewProfile($userId = null) {
        $user = null;
		$user = DB::table('users')->where('id', $userId)->first();
		if(!empty($user)){
			return $user->name. ' '.$user->last_name; 
		}
        return 'N/A';
     
    }
    /* Call orbweaver Api */
    public static function searchProductOrbweaver($partNumber, $slug_holder = null, $partNum = null, $requestedQty = null, $reorderArray = null){
        if($partNum != null){
            $partNumber = $partNum;
        }else{
            $partNumber     = $partNumber;
        }
        $partArray      = array (array('partNum'=>urlencode($partNumber)));
        $finalArray     =  json_encode($partArray);
        $partNumberLength = strlen($partNumber);
        $slugHolder = $slug_holder;
        $dt = [];
        if(!empty($slugHolder)){
            Session(['search_part_name' => $partNumber]);
            return redirect('/category/'.$slugHolder);
        }
        // using for token search end point
        $url= config('constants.ORBWEAVER_API_URL').'part/lookup?lookup_value='.$partNumber.'&authorized=true';
        $response = Helpers::curlRequestOrbweaver($url);
		
	    $finalProductData = [];
        $priceData        = [];
        $result           = json_decode($response,true);
		 if(isset($result) && !empty($result['offers'])){
		    $bestPrice        = Helpers::bestPrice($result,$partNumber,10000);
		     $resultListData = $result['offers'];
           // using for token search end point
            $arrowArray = [];
            $avnetArray = [];
            
            foreach ($resultListData as $partList){
                if($partList['datasource_name'] == 'Avnet'){
                    $avnetArray[] = $partList; 
                } else {
                    $arrowArray[] = $partList; 
                }
            }
            
            if(!empty($avnetArray) || !empty($arrowArray)){
				$finalProductDataAvnet = Helpers::mergeMpn($avnetArray,$requestedQty, $reorderArray,$bestPrice);
				$finalProductArrow = Helpers::mergeMpn($arrowArray,$requestedQty, $reorderArray,$bestPrice);
				$finalProductData = array_merge($finalProductDataAvnet,$finalProductArrow);
				//$finalProductData['bestPrice'] = $bestPrice;
			}
        }
        
		return $finalProductData;
		  
    }

    public static function bestPrice($result,$partNumber,$qty){
		$priceData =[];
		foreach($result['offers'] as $offer){
		if(!empty($offer['prices']) && $offer['mpn']==$partNumber){
				$priceData[$offer['available_qty']] = $offer['prices'];
				$supplierName[]                     = $offer['supplier_name'];
				$availableQty[]                     = $offer['available_qty'];
				$supplierPartNumber[]               = $offer['supplier_part_number'];
			}
		}
		
		$maxPrice = 100000;
		$minPrice = [];
		$i=0;
		foreach ($priceData as $keySup=>$val){
			if($availableQty[$i]>=$qty){
				foreach($val as $key=>$val1){
					if($val1['from_qty']<=$qty && $val1['to_qty']>=$qty){ 
				    if($val1['unit_price'] < $maxPrice && $val1['unit_price']!=0){
					    $minPrice  = ['price'=>$val1['unit_price'],'from_qty'=>$val1['from_qty'],'to_qty'=>$val1['to_qty'],'supplier_part_number'=>$supplierPartNumber[$i],'supplier_name'=>$supplierName[$i],'available_qty'=>$availableQty[$i],'mpn'=>$partNumber,'remaing_qty'=>0];
                     }
					}
				}   
			}
         $i++;			
		}
		if(empty($minPrice)){
			$minPrice  =  Helpers::breackPrice($result,$qty,$partNumber);
		}
		return $minPrice; 
	}
	public static function breackPrice($result,$qty,$partNumber){
		$priceData=[];
		$maxPrice = 100000;
		$minPrice = [];
		$i=0;
		foreach($result['offers'] as $offer){
			if(!empty($offer['prices']) && $offer['mpn']==$partNumber && $offer['available_qty']!=0){
					$priceData[$offer['available_qty']] = $offer['prices'];
					$supplierName[$offer['available_qty']]                     = $offer['supplier_name'];
					$supplierPartNumber[$offer['available_qty']]               = $offer['supplier_part_number'];
				}
			}
		if(!empty($priceData)){	
			krsort($supplierName);
			krsort($supplierPartNumber);
			krsort($priceData,SORT_NUMERIC );
			$remaingQty = $qty;
			foreach ($priceData as $keySup=>$val){
				foreach($val as $key=>$val1){
						if($val1['from_qty']<=$qty && $val1['to_qty']>=$qty){ 
							if($val1['unit_price'] < $maxPrice && $val1['unit_price']!=0){
								$remaingQty  = ($remaingQty - $keySup); 
								$minPrice[]  = ['price'=>$val1['unit_price'],'from_qty'=>$val1['from_qty'],'to_qty'=>$val1['to_qty'],'supplier_part_number'=>$supplierPartNumber[$keySup],'supplier_name'=>$supplierName[$keySup],'available_qty'=>$keySup,'mpn'=>$partNumber,'remaing_qty'=>$remaingQty,'mpnFound'=>$i];
							 }
							}
						}  
				   $i++;		
				}
		}
	 return $minPrice;		
    }

	public static function mergeMpn($arrowArray,$requestedQty, $reorderArray,$bestPrice){
        $mergeMpnProductData = [];
	    if(!empty($arrowArray)){
				$previousMpn = '';
				$nextMpn = '';
				$commanProduct = []; 
				$mergeMpnProductData = [];
				$totalProduct = (count($arrowArray)-1);
			    foreach($arrowArray as $key=>$keyVal){
          			$arrowSingleMpn = [];
                    $arrowSingleMpn[] = $keyVal;
					$nextMpn = $key < $totalProduct ?$arrowArray[$key+1]['mpn']:$keyVal['mpn']; 
					
					if(($keyVal['mpn']== $previousMpn)||($nextMpn == $keyVal['mpn'])){
						$commanProduct[] = $keyVal;
					}else{
					    $mergeMpnProductData[]= Helpers::prepareData($arrowSingleMpn, $requestedQty, $reorderArray,$bestPrice);
					}
					if($nextMpn != $keyVal['mpn'] && !empty($commanProduct)){
					   $mergeMpnProductData[]= Helpers::prepareData($commanProduct, $requestedQty, $reorderArray,$bestPrice);
  					   $commanProduct = [];
					}
					$previousMpn = $keyVal['mpn'];
                }
				
				if(!empty($commanProduct)){
				    $mergeMpnProductData[]= Helpers::prepareData($commanProduct, $requestedQty, $reorderArray,$bestPrice);
  					$commanProduct = [];
		    	}
				
		}
		return  $mergeMpnProductData;
	}
    /* Prepare data for the view output */
    public static function prepareData($apiData, $requestedQty = '', $reorderArray = '',$bestPrice=''){
		$productData=array();
        if(!empty($apiData)){
            foreach ($apiData as $partList){
			  if(!empty($partList['mpn'])){
                $productData=[];
                $newSourceArray = [];
                $productData['distributorName'] =  $partList['datasource_name'];
                $productData['itemId'] = preg_replace("/[^a-zA-Z0-9]/", "", $partList['supplier_part_number'].'_'.$partList['manufacturer_id']);
                $productData['partNum'] = $partList['mpn'];
                if(!empty($reorderArray)){
                    $productData['requestedQty']   = $reorderArray['orderedQty'];
                    $productData['orderedSourcePartId'] = $reorderArray['orderedSourcePartId'];
                    $productData['orderedCustomerPartId']  = $reorderArray['orderedCustomerPartId'];
                }else
                    $productData['requestedQty'] = $requestedQty;
                
                $productData['manufacturer'] = $partList['manufacturer_name'];
                if(!empty($reorderArray))
                    $productData['mfrCd'] = $reorderArray['orderedManufacturerCode'];
                else
                    $productData['mfrCd'] = '';
                $productData['desc'] =  $partList['description'];
                if(isset($partList['compliance'])){
                    $compliance = [];
                    foreach($partList['compliance'] as $comArray){
                        foreach($comArray as $comKey){
                            if($comArray['name']){
                                $comArray['displayLabel'] = $comArray['name'];
                            }
                            if($comArray['value']){
                                $comArray['displayValue'] = $comArray['value'];
                            }
                        }
                        unset($comArray['name']);
                        unset($comArray['value']);
                        array_push($compliance, $comArray);
                    }
                    $productData['EnvData']['compliance']   =  $compliance;
                }
                $newSourceCombineArray = array();
                $newIndex = 0;
                $productData['sources_count'] = count($apiData);
                $sourceArray = array();
                $newSourcePartArray = [];
                $sourceArray['sourceCd'] =  '';
                $sourceArray['currency'] =  'USD';
				$sourceArray['bestPrice'] = '';

                //$newSourceCombineArray['displayName'] =$partList['datasource_name'];
                $newSourceCombineArray['sourceCd'] =  '';
                $newSourceCombineArray['currency'] =  'USD';
			
                foreach($apiData as $sourcePart) {
                    $sourcePartArray = array();
                    $sourcePartArray['displayName']      =  $sourcePart['datasource_name'];
                    $sourcePartArray['packSize']         =  $sourcePart['order_mult_qty'];
                    $sourcePartArray['minimumOrderQuantity'] =  $sourcePart['min_order_qty'];

                    $sourcePartArray['sourcePartNumber']    =  '';
                    if(!empty($reorderArray)){
                        $sourcePartArray['orderedSourcePartId'] = $reorderArray['orderedSourcePartId'];
                        $sourcePartArray['orderedCustomerPartId']  = $reorderArray['orderedCustomerPartId'];
                        $sourcePartArray['sourcePartId'] = $sourcePart['supplier_part_number'];
                    }else
                        $sourcePartArray['sourcePartId'] = $sourcePart['supplier_part_number'];

                    if(!empty($sourcePart['prices'])){
                        $priceArrays = [];
                        foreach($sourcePart['prices'] as $priceArray){
                            foreach($priceArray as $priceKey){
                                $priceArray['displayPrice'] = $priceArray['unit_price'];
                                $priceArray['price'] = $priceArray['unit_price'];
                                $priceArray['minQty'] = $priceArray['from_qty'];
                                $priceArray['maxQty'] = $priceArray['to_qty'];
                            }
                            unset($priceArray['from_qty']);
                            unset($priceArray['to_qty']);
                            unset($priceArray['unit_price']);
                            unset($priceArray['currency']);
                            $sourcePartArray['Prices']['resaleList'][] = $priceArray;
                        }
                    }else{
                        $sourcePartArray['Prices']['resaleList']  =  [];
                    }
					$productData['bestPrice'] = '';
				if (count($bestPrice) == count($bestPrice, COUNT_RECURSIVE)){
					if(!empty($bestPrice['supplier_name']) && $bestPrice['supplier_name'] == $partList['datasource_name'])
					  $productData['bestPrice'] = $bestPrice;
					}else{
						if(!empty($bestPrice[0]['supplier_name']) && $bestPrice[0]['supplier_name'] == $partList['datasource_name']){
					        $productData['bestPrice'] = $bestPrice[0];
					    }else if(!empty($bestPrice[1]['supplier_name']) && $bestPrice[1]['supplier_name'] == $partList['datasource_name']){
							 $productData['bestPrice'] = $bestPrice[1];
						}
					}
                    $sourcePartArray['Availability'][0]['fohQty'] =  $sourcePart['available_qty'];
                    $sourcePartArray['Availability'][0]['availabilityCd'] =  '';
                    $sourcePartArray['Availability'][0]['availabilityMessage'] =  !empty($sourcePart['available_qty'])?'In Stock':'No Stock';
                    $sourcePartArray['Availability'][0]['pipeline'] =  array();

                    $sourcePartArray['customerSpecificPricing'] = array();
                    $sourcePartArray['customerSpecificInventory'] = array();                                    
                    $sourcePartArray['dateCode'] = '';
                    $sourcePartArray['resources'] = array();

                    if($sourcePart['available_qty']>0 && !empty($partList['prices'])){
                        $sourcePartArray['inStock'] =  1;
                    }else{
                        $sourcePartArray['inStock'] =  0;
                    }
                    $sourcePartArray['mfrLeadTime'] = $sourcePart['manufacturer_lead_time'];
                    $sourcePartArray['isNcnr'] = '';
                    $sourcePartArray['isNpi'] = '';
                    $sourcePartArray['isASA'] = '';
                    $sourcePartArray['requestQuantity'] = 0;
                    $sourcePartArray['productCode'] = '';
                    $sourcePartArray['iccLevels'] = array();

                    $sourcePartArray['cloudMfrCode'] = '';
                    $sourcePartArray['eccnCode'] = '';
                    $sourcePartArray['htsCode'] = '';
                    $sourcePartArray['countryOfOrigin'] = '';
                    $sourcePartArray['locationId'] = '';
                    $sourcePartArray['containerType'] = $sourcePart['pkg_type'];

                    $newSourcePartArray[] =  $sourcePartArray;
                    $newSourceCombineArray['sourcesPart'][] = $sourcePartArray;
                }
                $sourceArray['sourcesPart']   =   $newSourcePartArray ;
                $newSourceArray[] =  $sourceArray;
                $productData['sources'][]   =   $newSourceCombineArray;
            }
            }
        }
		return $productData;
    }

    /* Get in stock status of a MPN in multiple sources */
    public static function getInStockFromSources($mpnSourcesArray){
        $inStock = 0;
        foreach($mpnSourcesArray as $mpnVal){
            if($mpnVal['inStock']){
                $inStock = $mpnVal['inStock'];   
                break;
            }
        }
        return $inStock;
    }
}
