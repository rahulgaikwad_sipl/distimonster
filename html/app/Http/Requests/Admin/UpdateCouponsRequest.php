<?php
namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCouponsRequest extends FormRequest
{
    /**
     * Determine if the coupon is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'order_limit'            => 'required',
            'minimum_cart_amount'    => 'required',
            'start_date'             => 'required',
            'end_date'               => 'required',
            'status'                 => 'required'
        ];
    }
}
