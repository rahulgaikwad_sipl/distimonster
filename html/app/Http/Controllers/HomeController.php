<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Cart;
use App\News;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    protected $userSessionData;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('created_at', 'desc')->take(3)->where('status', 1)->whereNull('deleted_at')->get();
        return view('frontend.home', compact('news'));
    }

    /**
     * What is Distimonster Page.
     *
     * @return \Illuminate\Http\Response
     */
    public function whatIsDistimonster()
    {
        $news = News::orderBy('created_at', 'desc')->take(3)->where('status', 1)->whereNull('deleted_at')->get();
        return view('frontend.what_is_distimonster', compact('news'));

    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function comingSoon()
    {
        return view('frontend.coming_soon');
    }
    /**
     * Show the application login page.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('frontend.login');
    }
}
