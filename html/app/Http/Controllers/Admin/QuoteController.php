<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 04/04/18
 * Time: 3:19 PM
 */

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Mail\QuoteApprovalMail;
use App\QueteDetail;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\Quote;
use DataTables;

class QuoteController extends Controller
{
    public function index($status = NULL)
    {
        
        if(!empty($status)){
            $data['status'] = base64_decode($status); 
        }else{
            $data['status'] = $status; 
        }

        $data['quoteStatus'] = config('constants.APP_CONSTANT.QUOTE_STATUS');
        return view('admin.quotes.index', $data);
    }

    public function getQuoteList(Request $request){
        try {
            if ($request->ajax()) {
                $columns = [
                    'quotes.id',
                    'quotes.quote_name',
                    'quotes.is_approved',
                    \DB::raw('count(quote_details.id) as part_counts'),
                    'quotes.created_at',
                    'users.name AS firstname',
                    'users.last_name AS lastname',
                ];
                $quotes = Quote::select($columns)
                    ->join('users','quotes.user_id','=','users.id')
                    ->join('quote_details','quotes.id','=','quote_details.quote_id')
                    ->groupBy('quote_details.quote_id')
                    ->where('quotes.is_requested_lead_time', 0)
                    ->whereNull('quotes.deleted_at');
                $status_id = $request->input('status_id');
                if(!empty($status_id) || $status_id == '0'){
                    $quotes->where('quotes.is_approved',$status_id); 
                }    
                    
                return Datatables::of($quotes)
                    ->editColumn('id', function ($quotes) {
                        $return    =  'Quote-'.$quotes->id;
                        return $return;
                    })->editColumn('firstname', function ($quotes) {
                        $return    =  $quotes->firstname.' '.$quotes->lastname;
                        return $return;
                    })->editColumn('created_at', function ($quotes) {
                        $return    =  date('m/d/Y', strtotime($quotes->created_at));
                        return $return;
                    })->editColumn('is_approved', function ($quotes) {
                        $return    =  Quote::getStatusName($quotes->is_approved);
                        return $return;
                    })->addColumn('action', function ($quotes) {
                        $data['quote'] = $quotes;
                        $return = view('admin.quotes.partials.action',$data)->render();
                        return $return;
                    })->rawColumns(['action'])->make(true);
                }
            } catch (\Exception $ex) {
                return redirect()->route('bom.index')->with('failure', 'Something went wrong');
            }
        }



        public function quoteDetails(Request $request, $id){
            try{
                $quoteInfo          = Quote::getQuoteInfoWithUserById($id);
                if($quoteInfo->isEmpty()){
                    return redirect()->to('admin/quotes')->with('failure', 'Quote id does not exist in our record.');
                }else{
                    $data['quoteInfo']=   $quoteInfo;
                    $data['quoteItems'] = Quote::getQuoteDetailById($id);
                    return view('admin.quotes.quote-detail', $data);
                }
            }catch (\Exception $ex) {
                return redirect()->to('admin/quotes')->with('failure', 'Oops. There is a problem. Please try again later');
            }
        }

    public function approveQuoteDetails(Request $request, $id){
        try{
            $quoteDetailIds     = $request->input('quote_detail_id');
            $approvedQuantity   = $request->input('approved_quantity');
            $approvedPrice =     $request->input('approved_price');
            $deliveryDate = $request->input('approved_delivery_date');
            $requestRows = count($quoteDetailIds);

            if(empty($quoteDetailIds)){
                return redirect()->to('admin/quote-details/'.$id)->with('failure', 'Quote id does not exist in our record.');
            }else{ 
                $i = 0;
                foreach ($quoteDetailIds as $key => $detailId){
                    $i++;
                    $quoteDetail = QueteDetail::find($detailId);
                    if($quoteDetail){
                        $quoteDetail->approved_quantity = $approvedQuantity[$key];
                        $quoteDetail->approved_price = $approvedPrice[$key];
                        $quoteDetail->approved_delivery_date = \Helpers::convertDate($deliveryDate[$key]);
                        $quoteDetail->approved_date = date('Y-m-d');
                        $quoteDetail->is_approved = 1;
                        $quoteDetail->is_quoted   = 1;
                        $quoteDetail->save();
                    }
                }
                if($i == $requestRows){
                    $quote = Quote::find($id);
                    $quote->is_approved = 1;
                    $quote->approved_date = date('Y-m-d');
                    if(isset($request->is_responded_lead_time) && ($request->is_responded_lead_time == 1)) {
                        $quote->is_responded_lead_time = 1;
                        $redirectTo = 'admin/requested-lead-times';
                        $successMsg = 'Requested Product has been approved successfully.';
                    } else {
                        $redirectTo = 'admin/quotes';
                        $successMsg = 'Quote has been approved successfully.';
                    }
                    $quote->save();
                    $userInfo = User::find($quote->user_id); 
                    $data['firstName'] = ucfirst($userInfo->name);
                    $data['quote_name'] = $quote->quote_name;
                    $data['quote_id'] = $quote->id;
                    /*$subject                        = 'DistiMonster: Quote# ' .$quote->quote_name . ' Approved';*/
                    $subject = 'Request for Quote Processed';
                    \Mail::to($userInfo->email)->send(new QuoteApprovalMail($data, $subject));

                }
                return redirect()->to($redirectTo)->with('success', $successMsg);
            }
        }catch (\Exception $ex) {
            return redirect()->to($redirectTo)->with('failure', 'Oops. There is a problem. Please try again later');
        }
    }

    public function quoteReject(Request $request, $id){
        try{
            $qoute = Quote::findOrFail($id);
            $qoute->is_approved = 2;
            $qoute->save();
            return redirect()->to('admin/quotes')->with('success', 'Quote has been rejected successfully.');
        }catch(ModelNotFoundException $ex) {
            return redirect()->to('admin/quotes')->with('failure', 'Oops. There is a problem. Please try again later');
        }catch (\Exception $ex) {
            return redirect()->to('admin/quotes')->with('failure', 'Oops. There is a problem. Please try again later');
        }
    }

    public function quoteRejectReqLeadTime(Request $request, $id) {
        try{
            $qoute = Quote::findOrFail($id);
            $qoute->is_approved = 2;
            $qoute->save();
            return redirect()->to('admin/requested-lead-times')->with('success', 'Request Lead Time has been rejected successfully.');
        }catch(ModelNotFoundException $ex) {
            return redirect()->to('admin/requested-lead-times')->with('failure', 'Oops. There is a problem. Please try again later');
        }catch (\Exception $ex) {
            return redirect()->to('admin/requested-lead-times')->with('failure', 'Oops. There is a problem. Please try again later');
        }
    }
}