<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 04/04/18
 * Time: 3:19 PM
 */

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Mail\QuoteApprovalMail;
use App\QueteDetail;
use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use App\RequestedProduct;
use DataTables;
use App\Quote;

class RequestedProductController extends Controller
{
    public function index($status = NULL) {        
        if(!empty($status)){
            $data['status'] = base64_decode($status); 
        }else{
            $data['status'] = $status; 
        }
        $data['quoteStatus'] = config('constants.APP_CONSTANT.QUOTE_STATUS');
        return view('admin.requested-product.index', $data);
    }

    public function requestedProductList(Request $request){
        try {
            if ($request->ajax()) {
                $columns = [
                    'quotes.id',
                    'quotes.quote_name',
                    'quotes.is_approved',
                    \DB::raw('count(quote_details.id) as part_counts'),
                    'quotes.created_at',
                    'users.name AS firstname',
                    'users.last_name AS lastname',
                ];
                $quotes = Quote::select($columns)
                    ->join('users','quotes.user_id','=','users.id')
                    ->join('quote_details','quotes.id','=','quote_details.quote_id')
                    ->groupBy('quote_details.quote_id')
                    ->where('quotes.is_requested_lead_time', 1)
                    ->whereNull('quotes.deleted_at');
                $status_id = $request->input('status_id');
                if(!empty($status_id) || $status_id == '0'){
                    $quotes->where('quotes.is_approved',$status_id); 
                }    
                    
                return Datatables::of($quotes)
                    ->editColumn('id', function ($quotes) {
                        $return    =  'Quote-'.$quotes->id;
                        return $return;
                    })->editColumn('firstname', function ($quotes) {
                        $return    =  $quotes->firstname.' '.$quotes->lastname;
                        return $return;
                    })->editColumn('created_at', function ($quotes) {
                        $return    =  date('m/d/Y', strtotime($quotes->created_at));
                        return $return;
                    })->editColumn('is_approved', function ($quotes) {
                        $return    =  Quote::getStatusName($quotes->is_approved);
                        return $return;
                    })->addColumn('action', function ($quotes) {
                        $data['quote'] = $quotes;
                        $return = view('admin.requested-product.partials.action',$data)->render();
                        return $return;
                    })->rawColumns(['action'])->make(true);
                }
            } catch (\Exception $ex) {
                return redirect()->route('bom.index')->with('failure', 'Something went wrong');
            }
        }


        public function requestedProductDetails(Request $request, $id){
            try{
                $quoteInfo          = Quote::getQuoteInfoWithUserById($id);
                if($quoteInfo->isEmpty()){
                    return redirect()->to('admin/requested-product')->with('failure', 'Quote id does not exist in our record.');
                }else{
                    $data['quoteInfo']=   $quoteInfo;
                    $data['quoteItems'] = Quote::getQuoteDetailById($id);
                    return view('admin.requested-product.requested-product-detail', $data);
                }
            }catch (\Exception $ex) {
                return redirect()->to('admin/requested-product')->with('failure', 'Oops. There is a problem. Please try again later');
            }
        }
}