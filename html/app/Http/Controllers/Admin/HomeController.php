<?php

namespace App\Http\Controllers\Admin;
use Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreRolesRequest;
use App\Http\Requests\Admin\UpdateRolesRequest;
use App\User;
use App\Order;
use App\Quote;
use DB;

class HomeController extends Controller
{   
    /**
     * Display a dashboard page
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['users']  = User::where('role_id',2)->whereNull('deleted_at')->count();
        $data['orders'] = Order::where('orders.order_status',1)
                            ->join('users','orders.users_id','=','users.id')
                            ->join('payment_methods','orders.payment_method_id','=','payment_methods.id')
                            ->orderBy('orders.id', 'DESC')
                            ->whereNull('orders.deleted_at')
                            ->count();
        for($i=0; $i<=3;$i++){
            $orders_status = Order::where('orders.order_status',1)
                            ->where('orders.payment_status',$i) 
                            ->join('users','orders.users_id','=','users.id')
                            ->join('payment_methods','orders.payment_method_id','=','payment_methods.id')
                            ->orderBy('orders.id', 'DESC')
                            ->whereNull('orders.deleted_at')
                            ->count();
            if($i == '0')
                $data['orders_pending'] = $orders_status;                 
            elseif($i == '1')
                $data['orders_done'] = $orders_status;
            elseif($i == '2')
                $data['orders_failed'] = $orders_status;
            elseif($i == '3')
                $data['orders_refunded'] = $orders_status;
        }
        
        for($j=1; $j<=4;$j++){
            $orders_ship_status = Order::where('orders.order_status',1)
                            ->where('orders.order_shipping_status',$j) 
                            ->join('users','orders.users_id','=','users.id')
                            ->join('payment_methods','orders.payment_method_id','=','payment_methods.id')
                            ->orderBy('orders.id', 'DESC')
                            ->whereNull('orders.deleted_at')
                            ->count();
            if($j == '1')
                $data['orders_shipped'] = $orders_ship_status;                 
            elseif($j == '2')
                $data['orders_pending_ship'] = $orders_ship_status;
            elseif($j == '3')
                $data['orders_intransit'] = $orders_ship_status;
            elseif($j == '4')
                $data['orders_delivered'] = $orders_ship_status;
        }
                                                                     
        $quote = Quote::select('quotes.id')
                    ->join('quote_details','quotes.id','=','quote_details.quote_id')
                    ->where('quotes.is_approved','0')
                    ->whereNull('quotes.deleted_at')->groupBy('quote_details.quote_id')->get(); 
                  
        $data['quote'] = count($quote);
        $data['orderMoney'] =  Order::where('orders.order_status',1)->whereNull('orders.deleted_at')->sum('orders.order_total_amount','total_revenue');

        return view('home',$data);
    }

    /**
     * Display a login page
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {    
       if (Auth::check()) {
            if (Auth::user()->role_id == 1) {
                return redirect('/admin/home');
            } else  {
                return redirect('admin/login');
            }
        }
         return view('auth.login');
    }
}