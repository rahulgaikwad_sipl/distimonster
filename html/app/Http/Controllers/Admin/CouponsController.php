<?php

namespace App\Http\Controllers\Admin;

use App\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreCouponsRequest;
use App\Http\Requests\Admin\UpdateCouponsRequest;
use DataTables;

class CouponsController extends Controller
{
    /**
     * Display a listing of Coupons.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (! Gate::allows('user_access')) {
            return abort(401);
        }
        $coupons = Coupon::all();
        $couponsStatus = \App\Status::get()->pluck('name', 'id')->toArray();
        return view('admin.coupons.index', compact('coupons','couponsStatus'));
    }

    public function getCouponList(Request $request){
        try {
            if ($request->ajax()) {
                $columns = [
                    'id',
                    'code',
                    'discount_percentage',
                    'order_limit',
                    'status',
                ];
                $coupon = Coupon::select($columns)
                    ->whereNull('deleted_at');
                $status_id = $request->input('status_id');
                if(!empty($status_id)){
                    $coupon->where('status',$status_id); 
                }
                    
                return Datatables::of($coupon)
                    ->addColumn('status', function ($coupon) {
                        $data['coupon'] = $coupon;
                        $return = view('admin.coupons.partials.status',$data)->render();
                        return $return;
                    })
                    ->addColumn('action', function ($coupon) {
                        $data['coupon'] = $coupon;
                        $return = view('admin.coupons.partials.action',$data)->render();
                        return $return;
                    })->rawColumns(['status', 'action'])->make(true);
            }
        } catch (\Exception $ex) {
                return redirect()->route('drm.index')->with('failure', 'Something went wrong');
        }
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = [];
        $roles = \App\Role::get()->pluck('title', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $status = \App\Status::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        return view('admin.coupons.create', compact('roles','status','user'));
    }

    /**
     * Store a newly created Coupon in storage.
     *
     * @param  \App\Http\Requests\StoreCouponRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //Save user details
            $coupon = Coupon::addCoupon($request);
            if($coupon['validation_fails']) {
                //Redirect user back with input if server side validation fails
                return redirect()->route('admin.coupons.create')->withErrors($coupon['validator'])->withInput();
            }
            if ($coupon['coupon_saved']) {
                return redirect()->route('admin.coupons.index')->with('success', 'New Promo Code has been created successfully.');
            } else {
                return redirect()->route('admin.coupons.create')->with('failure','Oops. There is a problem. Please try again later.');
            }
        } catch (\Exception $ex) {
            return redirect()->route('admin.coupons.create')->with('failure','Oops. There is a problem. Please try again later.');
        }
    }

    /**
     * Show the form for editing Coupon.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        if (! Gate::allows('user_edit')) {
            return abort(401);
        }
        $roles          = \App\Role::get()->pluck('title', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $status         = \App\Status::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        return view('admin.coupons.create', compact('coupon', 'roles','status'));
    }

    /**
     * Update Coupon in storage.
     *
     * @param  \App\Http\Requests\UpdateCouponsRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCouponsRequest $request, Coupon $coupon )
    {
        try {
            //Save coupon details
            $couponData = Coupon::updateCoupon($request,$coupon->id);
            if($couponData['validation_fails']) {
                //Redirect user back with input if server side validation fails
                return redirect()->route('admin.coupons.edit', $coupon->id)->withErrors($couponData['validator'])->withInput();
            }
            if ($couponData['coupon_saved']) {
                return redirect()->route('admin.coupons.index')->with('success', 'Promo Code details updated successfully.');
            } else {
                return redirect()->route('admin.coupons.index')->with('failure','Oops. There is a problem. Please try again later.');
            }
        } catch (\Exception $ex) {
            return redirect()->route('admin.coupons.index')->with('failure', 'Oops. There is a problem. Please try again later.');
        }
    }


    public function deleteCoupon(Request $request)
    {
        try {
            $data = $request->all();
            $user = Coupon::findOrFail($request->input('couponId'));
            $user->deleted_at = date('Y-m-d h:i:s');
            if ($user->save()) {
                return response(['success' => true, 'data' => '', 'message' => 'Record Deleted Successfully.']);
            } else {
                return response(['success' => false, 'data' => '', 'message' => 'Oops. There is a problem. Please try again later.']);
            }
        }
        catch (\Exception $ex) {
            return response(['success' => false, 'data' => '', 'message' => $ex->getCode()]);
        }
    }


    public function updateCouponStatus(Request $request){
        $data = $request->all();
        $coupon                       = Coupon::findOrFail($request->input('id'));
        if($request->input('status') == 1){
            $coupon->status               = 2;
        }else{
            $coupon->status               = 1;
        }
        if ($coupon->save()) {
            return ['success'=>true,'status' => 200, 'validation_fails' => false];
        } else {
            return ['success'=>false,'status' => 404, 'validation_fails' => false];
        }
    }


}
