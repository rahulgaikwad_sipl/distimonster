<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 09/03/18
 * Time: 6:21 PM
 */

namespace App\Http\Controllers\Admin;

use App\User;
use App\Order;
use App\ShippingAddress;
use App\Status;
use App\ShippingDetail;
use Illuminate\Http\Request;
use App\Mail\AdminAddUser;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use DataTables;
use App\Jobs\SendOrderDetailMailJob;
use Carbon\Carbon;
use App\Mail\ChangeShippingStatusMail;
use App\Mail\ChangePaymentStatusMail;
use Rap2hpoutre\FastExcel\FastExcel;
use DB;

class OrderController extends Controller
{
    public function index($status = NULL, $status_type = NULL)
    {
        if (! Gate::allows('user_access')) {
            return abort(401);
        }

        $status_type = (!empty($status_type)) ? base64_decode($status_type) : '';
        $data['payment_status'] = '';
        $data['shipment_status'] = '';
        if($status_type == 'payment_status'){
            $data['payment_status'] = (!empty($status)) ? base64_decode($status) : $status; 
        }elseif($status_type == 'shipment_status'){
            $data['shipment_status'] = (!empty($status)) ? base64_decode($status) : $status; 
        }
        
        $data['users']= User::all();
        $data['orderStatus'] = Order::$shippingStatus;
        $data['paymentStatus'] = Order::$paymentStatus;
        return view('admin.orders.index',$data);
    }

    public function getOrderList(Request $request){
        try {
            if ($request->ajax()) {
                $columns = [
                    'orders.id',
                    'users.name',
                    'users.last_name',
                    'orders.order_date',
                    'orders.order_total_amount',
                    'orders.order_shipping_status',
                    'orders.order_tracking_info',
                    'orders.payment_status',
                    'payment_methods.payment_method_name',
                    'orders.partially_shipped'
                ];
                $orders = Order::select($columns)
                    ->join('users','orders.users_id','=','users.id')
                    ->join('payment_methods','orders.payment_method_id','=','payment_methods.id')
                    ->where('orders.order_status',1)
                    //->orderBy('orders.id', 'DESC')
                    ->whereNull('orders.deleted_at');
                $shipment_status_id = $request->input('shipment_status_id');
                if(!empty($shipment_status_id) || $shipment_status_id == '0'){
                    $orders->where('orders.order_shipping_status',$shipment_status_id); 
                }
                $payment_status_id = $request->input('payment_status_id');
                if(!empty($payment_status_id) || $payment_status_id == '0'){
                    $orders->where('orders.payment_status',$payment_status_id); 
                }
                return Datatables::of($orders)
                    ->editColumn('name', function ($orders) {
                        return $orders->name.' '.$orders->last_name;
                    })
                    ->addColumn('shipping_status', function ($orders) {
                        $data['order'] = $orders;
                        $return = view('admin.orders.partials.shipping-status',$data)->render();
                        return $return;
                    })
                    ->addColumn('payment_status', function ($orders) {
                        $data['order'] = $orders;
                        $return = view('admin.orders.partials.payment-status',$data)->render();
                        return $return;
                    })


                    ->editColumn('order_date', function ($orders) {
                        return date('m/d/Y, h:i a', strtotime($orders->order_date));
                        //$orders->order_date ? with(new Carbon($orders->order_date))->format('m/d/Y') : '';
                    })
                    ->editColumn('order_total_amount', function ($orders) {
                        return number_format($orders->order_total_amount,2, '.', ',');
                        //$orders->order_date ? with(new Carbon($orders->order_date))->format('m/d/Y') : '';
                    })
                    ->addColumn('action', function ($orders) {
                        $data['order'] = $orders;
                        $return = view('admin.orders.partials.action',$data)->render();
                        return $return;
                    })->rawColumns(['shipping_status','payment_status', 'action'])->make(true);
            }
        } catch (\Exception $ex) {
            return redirect()->route('drm.index')->with('failure', 'Something went wrong');
        }
    }

    /**
     * function for export orders
     */
    public function exportOrder(){
        $columns = [
            'orders.id',
            'users.name',
            'users.last_name',
            'orders.order_date',
            'orders.order_total_amount',
            'orders.order_shipping_status',
            'orders.payment_status'
        ];
        $shippingStatus = Order::$shippingStatus;
        $paymentStatus = Order::$paymentStatus;
        $orders = Order::select($columns)
            ->join('users','orders.users_id','=','users.id')
            ->where('orders.order_status',1)
            ->orderBy('orders.id', 'DESC')
            ->whereNull('orders.deleted_at')->get();
        (new FastExcel($orders))->download('orders.xlsx', function ($order) use ($shippingStatus, $paymentStatus) {
            return [
                'Order Id' => !empty($order->id) ? $order->id : "N/A",
                'Customer Name' => !empty($order->name) ? $order->name.' '.$order->last_name : "N/A",
                'Order Date' => !empty($order->order_date) ? date('d/m/Y, h:i a', strtotime($order->order_date)) : "N/A",
                'Total' => !empty($order->order_total_amount) ? number_format($order->order_total_amount,2, '.', ',') : "N/A",
                'Shipment Status' => !empty($shippingStatus[$order->order_shipping_status]) ? $shippingStatus[$order->order_shipping_status] : "N/A",
                'Payment Status' => !empty($paymentStatus[$order->payment_status]) ? $paymentStatus[$order->payment_status] : "N/A", 
            ];
        });
    }

    public function updateShippingStatus(Request $request){
        try {
            $data = $request->all();
            $shippingStatus                             = $request->shippingStatus;
            $orderId                                    = $request->orderId;
            $objshippingStatus                          = Order::findOrFail($orderId);
            $objshippingStatus->order_shipping_status   = $shippingStatus;
            if($objshippingStatus->save()){
                     $orderInfo =    Order::getOrderInfoById($orderId);
                        $data['firstName']              = $orderInfo[0]->customer_name;
                        $data['lastName']               = $orderInfo[0]->last_name;
                        $data['email']                  = $orderInfo[0]->email;
                        $data['orderShippingStatus']    = $orderInfo[0]->order_shipping_status;
                        $data['orderId']                = $orderInfo[0]->id;
                        $subject                        = 'DistiMonster: Order# ' .$orderInfo[0]->id . ' Shipping Status';

                    \Mail::to($orderInfo[0]->email)->send(new ChangeShippingStatusMail($data, $subject));

                return response(['success' => true, 'data' =>$data, 'message' => 'Record Updated successfully.']);
            }else{
                return response(['success' => false, 'data' => '', 'message' => 'Error in update record.']);
            }
        } catch (\Exception $ex) {
            return response(['success' => false, 'data' => '', 'message' => $ex->getMessage()]);
        }
    }
    public function updatePaymentStatus(Request $request){
        try {
            $data = $request->all();
            $paymentStatus                                   = $request->paymentStatus;
            $orderId                                           = $request->orderId;
            $objpaymentStatus                                   = Order::findOrFail($orderId);
            $objpaymentStatus->payment_status                   = $paymentStatus;
            if($objpaymentStatus->save()){
                $orderInfo =    Order::getOrderInfoById($orderId);
                $data['firstName']              = $orderInfo[0]->customer_name;
                $data['lastName']               = $orderInfo[0]->last_name;
                $data['email']                  = $orderInfo[0]->email;
                $data['paymentStatus']          = $orderInfo[0]->payment_status;
                $data['orderId']                = $orderInfo[0]->id;
                $subject                        = 'DistiMonster: Order# ' .$orderInfo[0]->id . ' Payment Status';

               \Mail::to($orderInfo[0]->email)->send(new ChangePaymentStatusMail($data, $subject));

                return response(['success' => true, 'data' =>'', 'message' => 'Record Updated successfully.']);
            }else{
                return response(['success' => false, 'data' => '', 'message' => 'Error in update record.']);
            }
        } catch (\Exception $ex) {
            return response(['success' => false, 'data' => '', 'message' => $ex->getMessage()]);
        }
    }
    public function orderDetails(Request $request, $id){
        $data['order']              = Order::getOrderInfoById($id);
        $data['shippingAddress']    = ShippingAddress::getShippingAddressById($data['order'][0]->shipping_address_id);
        $data['orderDetail']        = Order::getOrderDetailsById($data['order'][0]->id);
        $data['orderStatus'] = Order::$shippingStatus;
        return view('admin.orders.order-detail', $data);
    }

    /**
     * function to add or update tracking information according to the condition
     */
    public function updateTrackingInfo(Request $request) { 
        try { 
            $users = User::all(); 
            if(isset($request->order_id) && !empty($request->order_id) && isset($users[0]->id)) {
                $order_detail_id = $request->order_detail_id;
                $tracking_number = $request->tracking_number;
                $shipment_status_id = $request->shipment_status_id;
                $date_of_shipping = $request->date_of_shipping;
                $source_part_id = $request->source_part_id;
                $distributor_name = $request->distributor_name;
                $ordered_qty = $request->ordered_qty;
                $shipped_qty = $request->shipped_qty;
                $note = $request->note;
                $tracking_website = $request->tracking_website;
                $orderDetail = Order::getOrderDetailsById($request->order_id);
                if (ShippingDetail::where('order_id', '=', $request['order_detail_id'][0])->exists()) {
                    $i = 0; 
                    $deliveredStatusArr = array();
                    $shippedStatusArr = array();
                    $shippingStatusArr = array();
                    $pendingStatusArr = array();
                    foreach($order_detail_id as $record) {
                        array_push($shippingStatusArr, $shipment_status_id[$i]);
                        if($shipment_status_id[$i] == 1){  
                            array_push($shippedStatusArr, $shipment_status_id[$i]);
                        }
                        if($shipment_status_id[$i] == 4){
                            array_push($deliveredStatusArr, $shipment_status_id[$i]);
                        }
                        if($shipment_status_id[$i] == 2){
                            array_push($pendingStatusArr, $shipment_status_id[$i]);
                        }
                        DB::table('shipping_detail')
                        ->where('order_id', $order_detail_id[$i])
                        ->update(['source_part_id' => $source_part_id[$i], 'distributor_name' => $distributor_name[$i], 'ordered_qty' => $ordered_qty[$i], 'shipped_qty' => $shipped_qty[$i], 'note' => $note[$i], 'tracking_number' => $tracking_number[$i], 'tracking_website' => $tracking_website[$i], 'shipping_status' => $shipment_status_id[$i], 'date_of_shipping' => date('Y-m-d', strtotime($date_of_shipping[$i])), 'updated_by' => $users[0]->id]); 
                        $i++;
                    } 
                    if(count($shippingStatusArr) == count($shippedStatusArr)){
                        DB::table('orders')
                                    ->where('id', $request->order_id)
                                    ->update(['order_shipping_status' => 1, 'updated_at' => date('Y-m-d H:i:s')]);
                    }
                    if(count($shippingStatusArr) == count($deliveredStatusArr)){
                        DB::table('orders')
                                    ->where('id', $request->order_id)
                                    ->update(['order_shipping_status' => 4, 'updated_at' => date('Y-m-d H:i:s')]);
                    }

                    if(count($shippedStatusArr) > 0 && (count($shippingStatusArr) != count($shippedStatusArr))){
                        DB::table('orders')
                            ->where('id', $request->order_id)
                            ->update(['partially_shipped' => 1, 'updated_at' => date('Y-m-d H:i:s')]);
                        if($pendingStatusArr > 0){
                            DB::table('orders')
                                    ->where('id', $request->order_id)
                                    ->update(['order_shipping_status' => 2, 'updated_at' => date('Y-m-d H:i:s')]);
                        }    
                    }else{
                        DB::table('orders')
                            ->where('id', $request->order_id)
                            ->update(['partially_shipped' => 0, 'updated_at' => date('Y-m-d H:i:s')]);
                    }
                    $orderInfo =    Order::getOrderInfoById($request->order_id);
                    $data['firstName']              = $orderInfo[0]->customer_name;
                    $data['lastName']               = $orderInfo[0]->last_name;
                    $data['email']                  = $orderInfo[0]->email;
                    $data['orderShippingStatus']    = $orderInfo[0]->order_shipping_status;
                    $data['orderId']                = $orderInfo[0]->id;
                    $subject                        = 'DistiMonster: Order# ' .$orderInfo[0]->id . ' Shipping Status';

                    \Mail::to($orderInfo[0]->email)->send(new ChangeShippingStatusMail($data, $subject));
                    return redirect('/admin/order-details/'.$request->order_id)->with('success', 'Records updated successfully.');
                } else { 
                    $i = 0; 
                    $deliveredStatusArr = array();
                    $shippedStatusArr = array();
                    $shippingStatusArr = array();
                    $pendingStatusArr = array();
                    foreach($order_detail_id as $record) {  
                        array_push($shippingStatusArr, $shipment_status_id[$i]);
                        if($shipment_status_id[$i] == 1){  
                            array_push($shippedStatusArr, $shipment_status_id[$i]);
                        }
                        if($shipment_status_id[$i] == 4){
                            array_push($deliveredStatusArr, $shipment_status_id[$i]);
                        }
                        if($shipment_status_id[$i] == 2){
                            array_push($pendingStatusArr, $shipment_status_id[$i]);
                        }
                        DB::table('shipping_detail')
                        ->where('id', $record)
                        ->insert(['source_part_id' => $source_part_id[$i], 'distributor_name' => $distributor_name[$i], 'ordered_qty' => $ordered_qty[$i], 'shipped_qty' => $shipped_qty[$i], 'note' => $note[$i], 'tracking_number' => $tracking_number[$i], 'tracking_website' => $tracking_website[$i], 'shipping_status' => $shipment_status_id[$i], 'date_of_shipping' => date('Y-m-d', strtotime($date_of_shipping[$i])), 'order_id' => $order_detail_id[$i]]); 
                        $i++;
                    }
                    if(count($shippingStatusArr) == count($shippedStatusArr)){
                        DB::table('orders')
                                    ->where('id', $request->order_id)
                                    ->update(['order_shipping_status' => 1, 'updated_at' => date('Y-m-d H:i:s')]);
                    }
                    if(count($shippingStatusArr) == count($deliveredStatusArr)){
                        DB::table('orders')
                                    ->where('id', $request->order_id)
                                    ->update(['order_shipping_status' => 4, 'updated_at' => date('Y-m-d H:i:s')]);
                    }

                    if(count($shippedStatusArr) > 0 && (count($shippingStatusArr) != count($shippedStatusArr))){
                        DB::table('orders')
                            ->where('id', $request->order_id)
                            ->update(['partially_shipped' => 1, 'updated_at' => date('Y-m-d H:i:s')]); 
                        if($pendingStatusArr > 0){
                            DB::table('orders')
                                    ->where('id', $request->order_id)
                                    ->update(['order_shipping_status' => 2, 'updated_at' => date('Y-m-d H:i:s')]);
                        }       
                    }else{
                        DB::table('orders')
                            ->where('id', $request->order_id)
                            ->update(['partially_shipped' => 0, 'updated_at' => date('Y-m-d H:i:s')]);
                    }
                    $orderInfo =    Order::getOrderInfoById($request->order_id);
                    $data['firstName']              = $orderInfo[0]->customer_name;
                    $data['lastName']               = $orderInfo[0]->last_name;
                    $data['email']                  = $orderInfo[0]->email;
                    $data['orderShippingStatus']    = $orderInfo[0]->order_shipping_status;
                    $data['orderId']                = $orderInfo[0]->id;
                    $subject                        = 'DistiMonster: Order# ' .$orderInfo[0]->id . ' Shipping Status';

                    \Mail::to($orderInfo[0]->email)->send(new ChangeShippingStatusMail($data, $subject));
                    return redirect('/admin/order-details/'.$request->order_id)->with('success', 'Records added successfully.');
                }
            }
            return redirect('/admin/order-details/'.$request->order_id);
        } catch (\Exception $ex) {
            return response(['success' => false, 'data' => '', 'message' => $ex->getMessage()]);
        }    
    }
}