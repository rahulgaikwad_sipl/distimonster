<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 06/04/18
 * Time: 6:41 PM
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Order;
use App\UserAccountType;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\NetAccountHolder;
use Illuminate\Support\Facades\Auth;
use App\User;
use DataTables;
use App\Jobs\NetAccountStatusJob;
use App\Mail\NetAccountStatusMail;
use Carbon\Carbon;

class NetAccountController  extends Controller
{
    public function index()
    {
        $data['netAccountStatus']  = NetAccountHolder::$netAccountStatus;
        $data['accountType']        = UserAccountType::getAccountTye();
        return view('admin.net-accounts.index',$data);
    }

    public function getNetAccountRequestsList(Request $request){
        try {
            if ($request->ajax()) {
                $columns = [
                    'net_account_holders.id',
                    'net_account_holders.net_account_type',
                    'net_account_holders.applied_on_date',
                    'net_account_holders.status',
                    'users.name',
                    'users.last_name',
                    'user_account_types.name AS requested_account_type',
                    'cod_doc_url',
                    'net_term_acc_doc_url',
                    'resale_doc_url',
                    'resale_uploaded_file_name',
                    'cod_uploaded_file_name',
                    'net_term_uploaded_file_name',
                    'credit_reference_doc'
                ];
                $requestList = NetAccountHolder::select($columns)
                    ->join('users','net_account_holders.user_id','=','users.id')
                    ->join('user_account_types','net_account_holders.net_account_type','=','user_account_types.id')
                    ->whereNull('net_account_holders.deleted_at');

                $status_id = $request->input('status_id');
                if(!empty($status_id) || $status_id == '0'){
                    $requestList->where('net_account_holders.status',$status_id); 
                }
                    
                return Datatables::of($requestList)
                    ->editColumn('name', function ($requestList) {
                        return  $requestList->name.' '.$requestList->last_name;
                    })
                    ->editColumn('applied_on_date', function ($requestList) {
                        return date('m/d/Y, h:i a', strtotime($requestList->applied_on_date));
                    })
                    ->addColumn('status', function ($requestList) {
                        $data['requestData']  =  $requestList;
                        $return = view('admin.net-accounts.partials.status',$data)->render();
                        return $return;
                    })
                    ->addColumn('document', function ($requestList) {
                        $data['requestData']  =  $requestList;
                        $return = view('admin.net-accounts.partials.document-link',$data)->render();
                        return $return;
                    })->rawColumns(['status','document'])
                    ->make(true);
            }
        }  catch (\Exception $ex) {
            return response(['status' => false, 'message' => 'Something went Wrong']);
        }
    }

    public function updateNetAccountRequest(Request $request)
    {
        try {
            $data = $request->all();
            $requestStatus                      = $request->requestStatus;
            $requestId                          = $request->requestId;
            $accountType = '';
            if($request->has('accountType')){
                $accountType = $request->accountType;
            }
            $netAccountRequest                  = NetAccountHolder::findOrFail($requestId);
            $netAccountRequest->approval_date   = date('Y-m-d h:i:s');
            $netAccountRequest->status          = $requestStatus;
            $netAccountRequest->approved_account_type          = $accountType;
            if($netAccountRequest->save()){
                $userDetail                     =   User::findOrFail($netAccountRequest->user_id);
                $userData['firstName']          =   $userDetail->name;
                $subject =  'DistiMonster: Net Account Request Status';

                $accountTerm = UserAccountType::where('id', $accountType)->value('name');
                if($requestStatus == 1) {
                    $userData['reason'] = 'This email is to confirm your request for NET terms with '.'<a href="'.url('/').'"> DistiMonster.com </a>'.' has been
                                            approved for <b>'.$accountTerm.'</b> . Full details of credit limit can be viewed by logging into your  '.'<a href="'.url('/').'"> DistiMonster.com </a>'.' account and
                                            going to your account details. We look forward to working with you. If you have any questions, please
                                           '.'<a href="'.url('contact-us').'"> Click Here</a>'.' to Contact Us.';
                    \Mail::to($userDetail->email)->send(new NetAccountStatusMail($userData, $subject));
                }else if($requestStatus == 3){
                    $userData['reason']  = 'This email is to inform you that your request for Net terms with '.'<a href="'.url('/').'"> DistiMonster.com </a>'.' has been
                    denied. Please log into your account at '.'<a href="'.url('/').'"> DistiMonster.com </a>'.' for further more details. You can also
                    contact us by '.'<a href="'.url('contact-us').'"> Click Here</a>';
                    \Mail::to($userDetail->email)->send(new NetAccountStatusMail($userData, $subject));
                }

                return response(['success' => true, 'data' =>$userDetail, 'message' => 'Record Updated successfully.']);
            }else{
                return response(['success' => false, 'data' => $data, 'message' => 'Error in update record.']);
            }
        } catch (\Exception $ex) {
            return response(['success' => false, 'data' => '', 'message' => $ex->getMessage()]);
        }
    }

    public function downloadRequestFiles($id,$file){ 
        $filePath = public_path()."/files/".$id."/".$file;
        if(file_exists($filePath)) {
            return response()->download($filePath);
        }else{
           return redirect()->to('admin/net-account-requests')->with('failure', 'No file found to download.');
        }
    }
}