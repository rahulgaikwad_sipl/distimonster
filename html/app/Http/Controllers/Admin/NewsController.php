<?php

namespace App\Http\Controllers\Admin;

use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreNewsRequest;
use App\Http\Requests\Admin\UpdateNewsRequest;
use DataTables;
use DB;


class NewsController extends Controller
{
    /**
     * Display a listing of News.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::orderBy('id', 'desc')->get();
        $newsStatus = \App\Status::get()->pluck('name', 'id')->toArray();
        return view('admin.news.index', compact('news','newsStatus'));
    }

    public function getNewList(Request $request){
        try {
            if ($request->ajax()) {
                DB::statement(DB::raw('set @rownum=0'));
                $columns = [
                    'id',
                    'title',
                    'description',
                    'status',
                    'created_at',
                    DB::raw('@rownum  := @rownum  + 1 AS rownum'),
                ];
                $news = News::select($columns)
                    ->whereNull('deleted_at');
                $status_id = $request->input('status_id');
                if(!empty($status_id)){
                    if($status_id == '2')
                        $status_id = 0; 
                    $news->where('status',$status_id); 
                }

                return Datatables::of($news)
                    ->editColumn('created_at', function ($news) {
                        return date('m/d/Y, h:i a', strtotime($news->created_at));
                    })
                    ->editColumn('status', function ($news) {
                        $data['news'] = $news;
                        $return = view('admin.news.partials.toggle-switch',$data)->render();
                        return $return;
                    })
                    ->addColumn('action', function ($news) {
                        $data['news'] = $news;
                        $return = view('admin.news.partials.action',$data)->render();
                        return $return;
                    })->rawColumns(['status', 'action'])->make(true);
            }
        } catch (\Exception $ex) {
            return redirect()->route('drm.index')->with('failure', 'Oops. There is a problem. Please try again later.');
        }
    }

    /**
     * Show the form for creating new News.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['news']  = [];
        $data['status'] = \App\Status::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        return view('admin.news.create',$data);
    }

    /**
     * Store a newly created News in storage.
     *
     * @param  \App\Http\Requests\StoreNewssRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNewsRequest $request)
    { 
      try {  
          $news = new News();
          if($request->hasFile('image')) {
            $imageName = time().'.'.$request->image->getClientOriginalExtension();
            $request->image->move(public_path('uploads'), $imageName);
            $news->image = $imageName;
        }
        $news->title = $request->input('title');
        $news->description = $request->input('description');
        $news->status = $request->input('status');
        $news->save();
        return redirect()->route('admin.news.index')->with('success',"News added successfully.");
    } catch (\Exception $ex) {
        return redirect()->route('admin.news.index')->with('failure',"Oops. There is a problem. Please try again later.");
    }
}


    /**
     * Show the form for editing News.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);
        $status = \App\Status::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        return view('admin.news.create', compact('news','status'));
    }

    /**
     * Update News in storage.
     *
     * @param  \App\Http\Requests\UpdateNewssRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNewsRequest $request, $id)
    {
        try {  
            $news = News::findOrFail($id);
            if($request->hasFile('image')) {
                $imageName = time().'.'.$request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads'), $imageName);
                $news->image = $imageName;
            }
            $news->title = $request->input('title');
            $news->description = $request->input('description');
            $news->status = $request->input('status');
            $news->save();
            return redirect()->route('admin.news.index')->with('success',"News updated successfully");;
        } catch (\Exception $ex) {
            return redirect()->route('admin.news.index')->with('failure',"Oops. There is a problem. Please try again later.");
        }
    }

    /**
     * Display News.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $news = News::findOrFail($id);

        return view('admin.news.show', compact('news'));
    }


    public function deleteNews(Request $request)
    {
        try {
            $data = $request->all();
            $user = News::findOrFail($request->input('newsId'));
            $user->deleted_at = date('Y-m-d h:i:s');
            if ($user->save()) {
                return response(['success' => true, 'data' => '', 'message' => 'Record Deleted Successfully.']);
            } else {
                return response(['success' => false, 'data' => '', 'message' => 'Oops. There is a problem. Please try again later.']);
            }
        }
        catch (\Exception $ex) {
            return response(['success' => false, 'data' => '', 'message' => 'Oops. There is a problem. Please try again later.']);
        }
    }

    public function updateNewsStatus(Request $request ){
        $data = $request->all();
        $news                       = News::findOrFail($request->input('id'));
        if($request->input('status') == 1){
            $news->status               = 0;
        }else{
            $news->status               = 1;
        }
        if ($news->save()) {
            return ['success'=>true,'status' => 200, 'validation_fails' => false];
        } else {
            return ['success'=>false,'status' => 404, 'validation_fails' => false];
        }
    }
    
    /**
     * Remove News from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $new = News::findOrFail($id);
        $new->delete();

        return redirect()->route('admin.news.index')->with('success',"News deleted successfully.");
    }

    /**
     * Delete all selected News at once.
     *
     * @param Request $request
     */
    public function massDestroy(Request $request)
    {

        if ($request->input('ids')) {
            $entries = News::whereIn('id', $request->input('ids'))->get();

            foreach ($entries as $entry) {
                $entry->delete();
            }
        }
    }

}
