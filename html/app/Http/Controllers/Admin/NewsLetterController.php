<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 09/03/18
 * Time: 6:21 PM
 */

namespace App\Http\Controllers\Admin;

use App\Mail\ContentMail;
use App\NewsLetterSetting;
use App\NewsLetterSubscriber;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NewsLetterController extends Controller
{
    public function index()
    {
        $data['users']= NewsLetterSubscriber::where('is_subscribed',1)->get(['id','subscriber_email']);
        $data['newsSetting'] = NewsLetterSetting::find(1);
        return view('admin.newsletter.index',$data);
    }

    public function sendNewsLetter(Request $request)
    {
        try {
            $users = $request->users;
            $data['content'] = $request->message;
            $subject = $request->subject;
            if(count($users) > 0){
                foreach ($users as $email){
                    \Mail::to($email)->send(new ContentMail($data, $subject));
                }
            }
            $newsSetting = NewsLetterSetting::find(1);
            $newsSetting->subject = $request->subject;
            $newsSetting->message = $request->message;
            $newsSetting->save();
            return redirect()->to('admin/newsletter')->with('success', 'Newsletter has been sent successfully.');
        } catch (\Exception $ex) {
            return redirect()->to('admin/newsletter')->with('failure', 'Oops. There is a problem. Please try again later.');
        }
    }

}