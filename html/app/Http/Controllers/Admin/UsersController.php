<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Status;
use Illuminate\Http\Request;
use App\Mail\AdminAddUser;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreUsersRequest;
use App\Http\Requests\Admin\UpdateUsersRequest;
use DataTables;
use App\Jobs\AdminAddNewUserMailJob;
use Carbon\Carbon;
use DB;
use Auth;
use App\Subscribe;
use App\SubscriptionPlans;

class UsersController extends Controller{
    /**
     * Display a listing of User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $userStatus = \App\Status::get()->pluck('name', 'id')->toArray();
        return view('admin.users.index', compact('users','userStatus'));
    }

    public function getUserList(Request $request){
        try {
            if ($request->ajax()) {
                $columns = [
                    'users.id',
                    'users.name',
                    'users.last_name',
                    'users.email',
                    'users.contact_number',
                    'users.status',
                    'users.are_you_a',
                    'users.are_you_a_text',
                    'users.your_company_a',
                    'users.your_company_a_text',
                    'users.your_main_industry',
                    'users.your_main_industry_text',
                    'users.annual_component_purchase',
                    'cities.name as location'
                ];
                $user = User::select($columns)->leftjoin('cities', 'cities.id', '=', 'users.city')
                    ->where('role_id',2)
                    ->whereNull('deleted_at');
                    $status_id = $request->input('status_id');
                    $company_type = $request->input('company_type');
                    $are_you_a = $request->input('are_you_a');
                    $your_company_a = $request->input('your_company_a');
                    if(!empty($status_id)){
                        $user->where('users.status',$status_id); 
                    }
                    if(!empty($company_type)){
                        $user->where('users.your_main_industry',$company_type); 
                    }
                    if(!empty($are_you_a)){
                        $user->where('users.are_you_a',$are_you_a); 
                    }
                    if(!empty($your_company_a)){
                        $user->where('users.your_company_a',$your_company_a); 
                    } 
                    
                return Datatables::of($user)
                    ->editColumn('name', function ($user) {
                        if(!empty($user->last_name)){
                            $fullName = $user->name." ".$user->last_name;
                            return $fullName;
                        }else{
                            $fullName = $user->name;
                            return $fullName;
                        }
                    })
                    ->editColumn('contact_number', function ($user) {
                        if(!empty($user->contact_number)){
                            $contactNumber = $user->contact_number;
                            return $contactNumber;
                        }else{                            
                            return 'N/A';
                        }
                    })
                    ->editColumn('location', function ($user) {
                        if(!empty($user->location)){
                            $location = $user->location;
                            return $location;
                        }else{                            
                            return 'N/A';
                        }
                    })
                    ->addColumn('status', function ($user) {
                        $data['user'] = $user;
                        $return = view('admin.users.partials.toggle-switch',$data)->render();
                        return $return;
                    })
                    ->editColumn('are_you_a', function ($user) {
                        if(!empty($user->are_you_a) && $user->are_you_a != "Other"){
                            return $user->are_you_a;
                        }else if(!empty($user->are_you_a_text)){
                            return $user->are_you_a_text;
                        }else{
                            return 'N/A';
                        }
                    })
                    ->editColumn('your_company_a', function ($user) {
                        if(!empty($user->your_company_a) && $user->are_you_a != "Other"){
                            return $user->your_company_a;
                        }else if(!empty($user->your_company_a_text)){
                            return $user->your_company_a_text;
                        }else{
                            return 'N/A';
                        }
                    })
                    ->editColumn('your_main_industry', function ($user) {
                        if(!empty($user->your_main_industry) && $user->are_you_a != "Other"){
                            return $user->your_main_industry;
                        }else if(!empty($user->your_main_industry_text)){
                            return $user->your_main_industry_text;
                        }else{
                            return 'N/A';
                        }
                    })
                    ->addColumn('action', function ($user) {
                        $data['user'] = $user;
                        $return = view('admin.users.partials.action',$data)->render();
                        return $return;
                    })->rawColumns(['status', 'action'])->make(true);
            }
        } catch (\Exception $ex) {
                return redirect()->route('drm.index')->with('failure', 'Oops. There is a problem. Please try again later.');
        }
    }

    /**
     * Show the form for creating new User.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (! Gate::allows('user_create')) {
            return abort(401);
        }
        $user = [];
        $roles = \App\Role::get()->pluck('title', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $status = \App\Status::get()->pluck('name', 'id')->prepend('Select Status', '');
        $plans = SubscriptionPlans::Select('*')->Where('is_active', 1)->get()->pluck('type', 'plan_id')->prepend('Select subscription plan', '');
        $billingCycle[''] = 'Select Billing';
        $billingCycle['quarterly'] = 'Quarterly';
        $billingCycle['yearly'] = 'Yearly';
        return view('admin.users.create', compact('roles','status','user','plans','billingCycle'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param  \App\Http\Requests\StoreUsersRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            //Save user details
            $user = User::addUser($request);
            if($user['validation_fails']) {
                //Redirect user back with input if server side validation fails
                return redirect()->route('admin.users.create')->withErrors($user['validator'])->withInput();
            }
            if ($user['user_saved']) {
                $planType = $request->input('plan_type');
                $billingCycle = $request->input('billing_cycle');
                $subscription_plans = SubscriptionPlans::Select('*')->Where('plan_id', $planType)->Where('is_active', 1)->first();
                if(!empty($subscription_plans)){
                    if($billingCycle == 'quarterly'){
                        $grandTotalAmount =  $subscription_plans->monthly_price * 3;
                    }else if($billingCycle == 'yearly'){
                        $grandTotalAmount = $subscription_plans->yearly_price;
                    }    
                }
                
                //Billing Info Data Start
                $billingUserData = array("user_id" => $user['user']->id, "user_total_amount" => 0, "grand_total_amount" => $grandTotalAmount);
                //Billing Info Data End
                $billingUser = Subscribe::newUserBillingStore($billingUserData);

                $date['userId']         = \Crypt::encrypt($user['user']->id);
                $data['url']            = url('/');
                $data['name']           = $user['user']->name;
                $data['status']         = $user['user']->status;
                $data['email']          = $user['user']->email;
                $data['password']       = $request->input('password');
                
                // Mail::to($data['email'])->send(new AdminAddUser($users));
                $subject =  'DistiMonster: New Account Setup';
                $emailJob = (new AdminAddNewUserMailJob($user['user']->email,$data,$subject))->delay(Carbon::now()->addSeconds(1));
                dispatch($emailJob);
                //return (new AdminAddUser($data,$subject))->render();
                return redirect()->route('admin.users.index')->with('success', 'New user has been created successfully.');
            } else {
                return redirect()->route('admin.users.create')->with('failure','Oops. There is a problem. Please try again later.');
            }
        } catch (\Exception $ex) {
            return redirect()->route('admin.users.create')->with('failure', $ex->getMessage());
        }
    }

    /**
     * Show the form for editing User.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        if (! Gate::allows('user_edit')) {
            return abort(401);
        }
        $roles          = \App\Role::get()->pluck('title', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $status         = \App\Status::get()->pluck('name', 'id')->prepend(trans('quickadmin.qa_please_select'), '');
        $plans = SubscriptionPlans::Select('*')->Where('is_active', 1)->get()->pluck('type', 'plan_id')->prepend('Select subscription plan', '');
        $billingCycle[''] = 'Select Billing';
        $billingCycle['quarterly'] = 'Quarterly';
        $billingCycle['yearly'] = 'Yearly';
        return view('admin.users.create', compact('user', 'roles','status','plans','billingCycle'));
    }

    /**
     * Update User in storage.
     *
     * @param  \App\Http\Requests\UpdateUsersRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUsersRequest $request, User $user )
    {
        try {
            //Save user details
            $userData = User::updateUser($request,$user->id);
            if($userData['validation_fails']) {
                //Redirect user back with input if server side validation fails
                return redirect()->route('admin.users.edit',$user->id)->withErrors($userData['validator'])->withInput();
            }
            if ($userData['user_saved']) {
                $planType = $request->input('plan_type');
                $billingCycle = $request->input('billing_cycle');
                $subscription_plans = SubscriptionPlans::Select('*')->Where('plan_id', $planType)->Where('is_active', 1)->first();
                if(!empty($subscription_plans)){
                    if($billingCycle == 'quarterly'){
                        $grandTotalAmount =  $subscription_plans->monthly_price * 3;
                    }else if($billingCycle == 'yearly'){
                        $grandTotalAmount = $subscription_plans->yearly_price;
                    }    
                }
                
                //Billing Info Data Start
                $billingUserData = array("user_id" => $user->id, "user_total_amount" => 0, "grand_total_amount" => $grandTotalAmount);
                //Billing Info Data End
                $billingUser = Subscribe::updateUserBillingFromAdmin($billingUserData, $user->id);
                return redirect()->route('admin.users.index')->with('success', 'User details updated successfully.');
            } else {
                return redirect()->route('admin.users.index')->with('failure','Oops. There is a problem. Please try again later.');
            }
        } catch (\Exception $ex) {
            return redirect()->route('admin.users.index')->with('failure', 'Oops. There is a problem. Please try again later.');
        }
    }


    public function deleteUser(Request $request)
    {
        try {
        $data = $request->all();
        $user =    User::find($request->input('userId'));
        if ($user->delete()) {
            return response(['success' => true, 'data' => '', 'message' => 'Record Deleted Successfully.']);
        } else {
            return response(['success' => false, 'data' => '', 'message' => 'Oops. There is a problem. Please try again later.']);
        }
    }
    catch (\Exception $ex) {
            return response(['success' => false, 'data' => '', 'message' => 'Oops. There is a problem. Please try again later.']);
        }
    }

    public function updateUserStatus(Request $request ){
        $data = $request->all();
        $user                       = User::findOrFail($request->input('id'));
        if($request->input('status') == 1){
            $user->status               = 2;
        }else{
            $user->status               = 1;
        }
        if ($user->save()) {
            return ['success'=>true,'status' => 200, 'validation_fails' => false];
        } else {
            return ['success'=>false,'status' => 404, 'validation_fails' => false];
        }
    }
    /**
     * Display User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        if (! Gate::allows('user_view')) {
            return abort(401);
        }
        $bio = \App\User::where('id', $user->id)->select('are_you_a', 'are_you_a_text', 'your_company_a', 'your_company_a_text', 'your_main_industry', 'your_main_industry_text')->first();
		$companyName = \App\User::where('id', $user->id)->select('company_name', 'year_of_establish', 'company_type', 'annual_component_purchase', 'know_about_distimonster')->first();
        $subUserList = \App\User::where('parent_user_id', $user->id)->get(['id', 'name','last_name','email','company_name', 'note']);
        return view('admin.users.show', compact('companyName','bio', 'user', 'subUserList'));
    }


    /**
     * Remove User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if (! Gate::allows('user_delete')) {
            return abort(401);
        }
        $user = User::findOrFail($user->id);
        $user->deleted_at  =  date('Y-m-d h:i:s');
        if($user->save()) {
            return redirect()->route('admin.users.index')->with('success', 'User deleted successfully.');
        }else{
            return redirect()->route('admin.users.index')->with('failure', 'Oops. There is a problem. Please try again later.');
        }
    }

    public function getProfile(){
        $user = [];
        return view('admin.profile', $user)->render();
    }

    public function updateProfile (Request $request ,$id) {
        $data = $request->all();
        $profileImage = $request->file('profile_pic');
        if(isset($profileImage)) {
            $data['profile_pic_mime'] = $profileImage->getClientMimeType();
        }
        //Server side validations
        $validator = \Validator::make($data, User::updateAdminProfileValidationRules(), User::$registerUserValidationMessages);
        if ($validator->fails()) {
            //Redirect user back with input if server side validation fails
            return redirect()->to('admin/profile')->withErrors($validator)->withInput();
        }else {
            $user = User::findOrFail($id);
            $user->name         = $request->input('name');
            // set image path
            $profileImagePath    = config('app.resource_paths.profile_images');
            //Move image and thumbnail
            if($profileImage) {
                //Delete old image
                @unlink($profileImagePath.$user->profile_pic);
                //Call the function to upload image
                $imageFile = User::uploadFiles($profileImage, $profileImagePath);
                $user->profile_pic = $imageFile;
            }
            //Save user details
            if ($user->save()) {
                return redirect()->to('admin/profile')->with('success', 'Your profile updated successfully.');
            } else {
                return redirect()->to('admin/profile')->with('failure', 'Oops. There is a problem. Please try again later.');
            }
        }
    }

     /*
     * Check duplicate email
     * @param int $id
     * @param Request $request
     */
    public function checkEmail(Request $request) {
        if($request->ajax()) {
            try{
                if(!empty($request->email)){
                    $email = $request->email;
                }else{
                    $email = $request->user_email;
                }
                
                $loginUserEmail = '';
                if (Auth::check()) {
                    $loginUserEmail = Auth::user()->email;
                }
                if (!empty($email)) {
                    $userEmail = User::where('email', $email)->where('role_id', 2)->count();
                    if($loginUserEmail == $email){
                        return response( ['status'=> true, 'message'   => config('constants.EMAIL_EXIST'),'data'=>$userEmail], 200);
                    }else{
                        if($userEmail > 0){
                            return response( ['status'=> false, 'message'   =>config('constants.EMAIL_NOT_EXIST')  ,'data'=>$userEmail], 500);
                        }else{
                            return response( ['status'=> true, 'message'   => config('constants.EMAIL_EXIST'),'data'=>$userEmail], 200);
                        }
                    }
                }else{
                    return response( ['status'=> false, 'message'   => config('constants.EMAIL_NOT_EXIST')], 500);
                }
            } catch (\Exception $ex) {
                return response( ['status'=> false, 'message'   => config('constants.EMAIL_NOT_EXIST')], 500);
            }
        } else {
            return response( ['status'=> false, 'message'   => config('constants.EMAIL_NOT_EXIST')], 500);
        }
    }
}
