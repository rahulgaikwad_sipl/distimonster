<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller {
    /*
     * Name: __construct
     * Created By: SIPL
     * Created Date: 13-Feb-2018
     * Purpose: Constructor for checking user auth.
     */

    private $_user;
    private $_response = array('status' => false, 'errors' => array());
    private $_request;

    public function __construct(Request $request) {
        $this->_request = $request;
       // $this->middleware('super-admin')->except(['login', 'adminAuth', 'forgetPassword', 'forgotEmailSend']);
    }

    /*
     * Name: login
     * Created By: SIPL
     * Created Date: 13-Feb-2018
     * Purpose: For show login page.
     */

    public function login() {
        if (Auth::check()) {
            if (Auth::user()->role_id == 1) {
                return redirect('/admin/home');
            } else  {
                return redirect('/');
            }
        }
        $data['title'] = 'Login';
        return view('frontend.auth.login', $data);
    }

    /*
     * Name: userAuth
     * Created By: SIPL
     * Created Date: 13-Feb-2018
     * Purpose: For User authentication.
     */

    public function userAuth() {
        $validationRules = array('password' => 'required', 'email' => 'required|email');
        $validatorMsgs = array(
            'password.required' => config('constants.REQUIRED_PASSWORD'),
            'email.required' => config('constants.REQUIRED_EMAIL'),
            'email.email' => config('constants.VALID_EMAIL_ADDRESS'),
        );
        $validation = \Validator::make($this->_request->all(), $validationRules, $validatorMsgs);
        if ($validation->fails()) {
            return redirect()->to('/login')->withErrors($validation)->withInput();
        } else {
            if (Auth::attempt(array(
                        'email' => Input::get('email'),
                        'password' => Input::get('password'),
                        'role_id' => 1,
                        'deleted_at' => 0
                            ), true
                    )
            ) {
                               return redirect()->intended('/')->with('success_msg', config('constants.LOGIN_SUCCESS'));
            } else {
                return redirect()->intended('/login')->with('alert_msg', config('constants.INVALID_EMAIL_OR_PASSWORD'));
            }
        }
    }

    /*
     * Name: forgetPassword
     * Created By: SIPL
     * Created Date: 13-Feb-2018
     * Purpose: For show user forget password page.
     */

    public function forgetPassword() {
        if (Auth::check()) {
            return redirect('/home');
        }
        $data['title'] = 'Forgot Password';
        return view('frontend.auth.forget-password', $data);
    }

    /*
     * Name: forgotEmailSend
     * Created By: SIPL
     * Created Date: 13-Feb-2018
     * Purpose: For send temporary password on emails.
     */

    public function forgotEmailSend() {
        try {
            $validationRules = array(
                'email' => 'required|email'
            );
            $validationMessages = array(
                'email.required' => config('constants.REQUIRED_EMAIL'),
                'email.email' => config('constants.VALID_EMAIL_ADDRESS'),
            );
            $validation = \Validator::make($this->_request->all(), $validationRules, $validationMessages);
            if ($validation->fails()) {
                return redirect()->to('/forget-password')->withErrors($validation)->withInput();
            } else {

                $email = Input::get('email');
                $user = User::select('users.id', 'users.email', 'users.remember_token')
                        ->where("users.email", $email)
                        ->where('users.deleted_at', 0)
                        ->where('users.role_id', 1)
                        ->first();
                if ($user == NULL) {
                    return redirect()->intended('/forget-password')->with('alert_msg', config('constants.ERROR_FORGOT_PASSWORD'));
                } else {
                    $user = $user->toArray();

                    $randomPassword = User::randomPassword();
                    /* Update data base */
                    $updateUser = User::find($user['id']);

                    $updateUser->password = Hash::make($randomPassword);
                    $updateUser->save();
                    $user['password'] = $randomPassword;
                }
                /* Send activation email to user */
                $user['bannerImage'] = 'email-forgotpassword.jpg';
                $user['heading'] = 'Forgot Password';
                $user['link'] = url('login');
                $user["settingsURL"] = url("settings");

                Mail::send('mail.reset_password_link', $user, function($mail) use ($user) {
                    $mail->to($user['email'])
                            ->subject(config('constants.SITE_NAME') . ': Reset Password');
                });
                return redirect()->intended('/login')->with('success_msg', config('constants.FORGOT_SUCCESS'));
            }
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }

    /*
     * Name: logout
     * Created By: SIPL
     * Created Date: 13-Feb-2018
     * Purpose: For user logout functionality.
     */

    public function logout() {
        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }

    /*
     * Name: checkUserEmail
     * Created By: SIPL 
     * Created Date: 14-June-2017 
     * Purpose: Check email should be unique per admin user 
     */

    public function checkUserEmail() {
        $data = $this->_request->all();
        if (!empty($data['email'])) {
            $userId = Auth::user()->id;
            $query = \DB::table('users')
                    ->where('role_id', 1)
                    ->where('email', $data['email'])
                    ->where('id', '<>', $userId);
            $result = $query->get();
            if (count($result) > 0) {
                abort(404);
            }
        }
        echo 'Success';
    }

}
