<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 21/03/18
 * Time: 6:10 PM
 */

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use Event;
use Session;
use Cart;
use Auth;
use Input;
use App\Http\Controllers\Controller;
use URL;
use Illuminate\Support\Facades\Redirect;
use App\QuoteDetail;


use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\Transaction;
use PayPal\Api\CreditCard;
use PayPal\Api\FundingInstrument;

use App\TransactionsDetail;
use App\Order;
use App\OrderDetail;
use App\Quote;
use Illuminate\Auth\Events\Authenticated;

class PaypalController extends Controller {
    private $_api_context;
    
    protected $userSessionData;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        // setup PayPal api context
        $paypal_conf = config('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function postPayment($orderId,$orderTotalAmount){
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $orderTotalAmount  = 0.1;
        $item_1 = new Item();
        $item_1->setName('Item 1')->setCurrency('USD')->setQuantity(1)->setPrice($orderTotalAmount); // unit price
        // add item to list
        $item_list = new ItemList();
        $item_list->setItems(array($item_1));

        $amount = new Amount();
        $amount->setCurrency('USD')->setTotal($orderTotalAmount);

        $transaction = new Transaction();
        $transaction->setAmount($amount)->setItemList($item_list)->setDescription('Order made to DistiMonster.');

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('payment.status'))->setCancelUrl(URL::route('payment.status'));

        $payment = new Payment();
        $payment->setIntent('Sale')->setPayer($payer)->setRedirectUrls($redirect_urls)->setTransactions(array($transaction));
        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\config('paypal.app.debug')) {
                echo "Exception: " . $ex->getMessage() . PHP_EOL;
                $err_data = json_decode($ex->getData(), true);
                return Redirect::route('payment.error')->with('error', config('constants.COMMON_ERROR'));
                exit;
            } else {
                die('Some error occur, sorry for inconvenient');
            }
        }
        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        // add payment ID to session
        Session::put('paypal_payment_id', $payment->getId());
        Session::put('order_id', $orderId);
        if(isset($redirect_url)) {
            // redirect to paypal
            return Redirect::away($redirect_url);
        }
        Session::put('paymentfailed', 1);
        return Redirect::route('payment.error')->with('error', config('constants.COMMON_ERROR'));
    }

    public function getPaymentStatus(){
        // Get the payment ID before session clear
        $payment_id     = Session::get('paypal_payment_id');
        $orderId        = Session::get('order_id');
        // clear the session payment ID
        Session::forget('paypal_payment_id');
        if (empty(Input::get('PayerID')) || empty(Input::get('token'))) {
            Session::put('paymentfailed', 1);
            return Redirect::route('payment.error')->with('error', config('constants.PAYPAL_PAYMENT_FAILED'));
        }
        $payment = Payment::get($payment_id, $this->_api_context);
        // PaymentExecution object includes information necessary
        // to execute a PayPal account payment.
        // The payer_id is added to the request query parameters
        // when the user is redirected from paypal back to your site
        $execution = new PaymentExecution();
        $execution->setPayerId(Input::get('PayerID'));
        //Execute the payment
        $result = $payment->execute($execution, $this->_api_context);
        $paymentData = json_decode($result);
        if ($result->getState() == 'approved') {
           $transactionId    =  $result->id;
           $paymentMethod   =  $result->payer->payment_method;
            // payment made
            //Save transaction details in transaction table
            $tranaction                     =   new TransactionsDetail();
            $tranaction->transaction_id     =   $transactionId;
            $tranaction->transaction_by     =   $paymentMethod;
            $tranaction->order_id           =   $orderId;
            $tranaction->save();
            $order                                 = Order::findOrFail($orderId);
            $order->payment_status                 = 1;
            $order->order_status                   = 1;
            $order->tranaction_reference_id        = $transactionId;
            if ($order->save()) {
                Session::forget('selectedAddressId');
                Session::forget('selectedShippingMethodId');
                Session::forget('shippingMethodTypeInput');
                Session::forget('accountNumber');
                Session::forget('shippingNotes');
                Session::forget('order_id');

                foreach(Cart::instance('shoppingcart')->content() as $row){
                    if($row->options->quoteItemId !=0){
                        $quoteDetail                                = QuoteDetail::findOrFail($row->options->quoteItemId);
                        $quoteDetail->is_ordered                    = 1;
                        $quoteDetail->save();

                        $quoteItemCount                 =  QuoteDetail::where('quote_id',$row->options->quoteId)->count();
                        $orderedQuoteItemCount          =  QuoteDetail::where('quote_id',$row->options->quoteId)->where('is_ordered',1)->count();
                        if($quoteItemCount >0 && $quoteItemCount ==$orderedQuoteItemCount  ){
                            $quote                      = Quote::findOrFail($row->options->quoteId);
                            $quote->is_approved         =  4;
                            $quote->save();
                        }
                    }
                }

                Cart::instance('shoppingcart')->destroy();
                Session::put('ordersuccess', 1);
                \Helpers::sendOrderSuccessMail($orderId);
                return redirect()->route('shopping-completed', ['txNumber'=>$transactionId,'orderId'=>$orderId])->with('success', config('constants.ORDER_SUCCESS'));
            } else {
                return redirect()->route('order-process-error', ['txNumber' => $transactionId, 'orderId' => $orderId])->with('success', config('constants.PAYPAL_PAYMENT_SUCCESS_ORDER_ERROR'));
            }
        }
        Session::put('paymentfailed', 1);
        return Redirect::route('payment.error')->with('error',  config('constants.PAYPAL_PAYMENT_FAILED_ERROR'));
    }

    public function getPaypalError(){
        $orderId =  Session::get('order_id');
         $paymentFailed = Session::get('paymentfailed');
        if($paymentFailed){
            Order::where('id', $orderId)->update(['payment_status'=>2,'deleted_at' => date('Y-m-d h:i:s')]);
            OrderDetail::where('order_id', $orderId)->update(['deleted_at' => date('Y-m-d h:i:s')]);
            Session::forget('paymentfailed');
            Session::forget('order_id');
            return view('frontend.payments.paypal-error');
        }else{
            return abort(404);
        }
    }
}