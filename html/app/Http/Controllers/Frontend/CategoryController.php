<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 26/06/18
 * Time: 11:28 AM
 */

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Category;

use App\Http\Requests;
use Illuminate\Pagination\LengthAwarePaginator;
use Cart;
use Event;
use App\RequestedProduct;
use App\Quote;
use App\QuoteDetail;

use Helpers; // Important
use App\Mail\NewPartRequestMailToAdmin;
use App\Mail\NewPartRequestMailToUser;

class CategoryController  extends Controller{
        public function __construct(Request $request) {
            $this->_request = $request;
        }

    public function categoryList() {
        $data['title']              = 'Category';
        $data['catergories']        = Category::getCategoryList();
        return view('frontend.category.category-list', $data);
    }
    public function searchCategoryPart(Request $request, $slug){
        $data['title']              = 'Searched Part';
        $data['categoryFilter']     = Category::getCategoryList();
        $categoryName               = Category::getCategoryNameBySlug($slug);
        $data['category']           = $categoryName[0]['name'];
        $data['categorySlug']       = $slug;

        return view('frontend.category.category-part-list', $data);
    }

    public function getParts(Request $request){
        $data = $request->all();
        $offset                     =  $data['offset'];
        $categorySlug               =  $data['category'];
        $categoryName               =  Category::getCategoryNameBySlug($categorySlug);
        $category                    = $categoryName[0]['name'];
        $keyword                    = $data['keyword'];
        $data['parts']              = Category::getCategoryPart($category ,$keyword,$offset);
        $data['categorySlug']       =  $categorySlug;
        $data['categoryName']       = $categoryName;
        $htmlContent                = view('frontend.category.parts', $data)->render();
        if(!empty($data['parts'])) {
            if(empty($htmlContent)) {
                return response(['status' => false, 'isData' => false, 'message' => '','html'=>$htmlContent]);
            }else{
                return response(['status' => true, 'isData' => true, 'message' => '','html'=>$htmlContent]);
            }
        }else{
            return response(['status' => false, 'isData' => false, 'message' => '','html'=>$htmlContent]);
        }

    }
    public function searchPartInCategory(Request $request, $categorySlug= null , $partNum = null){
        $data = $request->all();
        /*CALL ORBEWER API START*/
        $partNumber = base64_decode($partNum);
        $finalProductData = Helpers::searchProductOrbweaver($partNumber);
        if(!empty($finalProductData)) {
            $resultListData =$result['itemserviceresult']['data'][0]['resultList'][0]['PartList'];
            // using for token search end point
            foreach ($resultListData as $partList){
                $productData=array();
                $newSourceArray = [];
                $productData['itemId']              =  $partList['itemId'];
                $productData['partNum']             =  $partList['partNum'];
                $productData['requestedQty']        =   '';
                $productData['manufacturer']        =  $partList['manufacturer']['mfrName'];
                $productData['mfrCd']               =  $partList['manufacturer']['mfrCd'];
                $productData['desc']                =  $partList['desc'];
                if(isset($partList['EnvData'])){
                    $productData['EnvData']   =  $partList['EnvData'];
                }
                $newSourceCombineArray = array();
                $newIndex = 0;
                if(isset($partList['InvOrg']['sources'])){
                    $productData['sources_count']   =  count($partList['InvOrg']['sources']);
                    //Combining all the source parts
                    foreach($partList['InvOrg']['sources'] as $sources) {
                        $sourceArray = array();
                        $newSourcePartArray = [];

                        $sourceArray['displayName'] =$sources['displayName'];
                        $sourceArray['sourceCd'] =  $sources['sourceCd'];
                        $sourceArray['currency'] =  $sources['currency'];

                        $newSourceCombineArray['displayName'] =$sources['displayName'];
                        $newSourceCombineArray['sourceCd'] =  $sources['sourceCd'];
                        $newSourceCombineArray['currency'] =  $sources['currency'];

                        $i=0;
                        foreach($sources['sourceParts'] as $sourcePart) {
                            $newSourceCombineArray['sourcesPart'][] = $sourcePart;
                            $i++;
                            $sourcePartArray = array();
                            $sourcePartArray['packSize']         =  $sourcePart['packSize'];
                            $sourcePartArray['sourcePartNumber']    =  $sourcePart['sourcePartNumber'];
                            $sourcePartArray['sourcePartId']        =  $sourcePart['sourcePartId'];
                            $sourcePartArray['dateCode']            =  $sourcePart['dateCode'];
                            $sourcePartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                            $sourcePartArray['inStock']             =  $sourcePart['inStock'];
                            $sourcePartArray['Availability']        =  $sourcePart['Availability'];
                            if(isset($sourcePart['Prices'])){
                                $sourcePartArray['Prices']              =  $sourcePart['Prices'];
                            }else{
                                $sourcePartArray['Prices']              =  [];
                            }
                            $newSourcePartArray[] =  $sourcePartArray;
                        }
                        $sourceArray['sourcesPart']   =   $newSourcePartArray ;
                        $newSourceArray[] =  $sourceArray;
                    }
                    $productData['sources'][]   =   $newSourceCombineArray ;
                }
                $finalProductData[]=$productData;
            }
            array_walk_recursive($finalProductData, 'Helpers::update_price');
            session(['productDetails' => $finalProductData]);
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($finalProductData);
            // Define how many items we want to be visible in each page
            $perPage = 500;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['finalData'] =  $paginatedItems;
            /* Set in session */
            $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));

            if(Session::has('searchProducts')) {
                $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));
                if(in_array($request->input('partNumber'), array_column(Session::get('searchProducts') , 'product_name'))) { // search value in the array
                    foreach (Session::get('searchProducts') as $keyp => $products) {
                        if ($products['product_name'] == $request->input('partNumber')) {
                            $event_data_display = Session::get('searchProducts');
                            unset($event_data_display[$keyp]);
                            Session(['searchProducts' => $event_data_display]);
                        }
                    }
                }
                $completeResult = array_merge_recursive($searchProductArray,Session::get('searchProducts'));
                Session(['searchProducts' => $completeResult]);
            } else {
                Session(['searchProducts' => $searchProductArray]);
            }
            return view('frontend.products.listing',$data);
        }else{
            $data['categoryFilter']     = Category::getCategoryList();
            $data['partNumber']  =  $partNumber;
            $data['categorySlug']    = $categorySlug;
            session(['previousroute' => 'search-part/'.$categorySlug.'/'.base64_encode($partNumber)]);
            return view('frontend.category.no-category-part-found',$data);
        }
    }

    public function saveRequestedProduct(Request $request){
        try {
            $data = $request->all(); 
            if (Auth::user()){ 
                $quote =  new Quote();
                $quote->user_id =  \Auth::user()->id;
                if(!empty($request->leadQuoteName)){
                    $quote->is_unnamed =  0;
                    $quote->quote_name =  $request->leadQuoteName;
                }else{
                    $quote->is_unnamed =  1;
                    $quote->quote_name = uniqid();
                }
                $quote->note = '';
                $quote->is_requested_lead_time   = 1;
                $quote->save();
                $partNumber = $data['partNumber'];
                $internalPart = $data['internalPart'];
                $expectedQuantity = $data['expectedQuantity'];
                $quoteDetailsInsert = [];
                $response = '';
                if(!empty($partNumber)){
                    $i = 0;
                    foreach($partNumber as $val){
                        $quoteDetailsInsert[$i]['quote_id'] = $quote->id;
                        $quoteDetailsInsert[$i]['item_name'] = $val;
                        $quoteDetailsInsert[$i]['internal_part'] = $internalPart[$i];
                        if($data['categoryName'] != ""){
                            $quoteDetailsInsert[$i]['category'] = $data['category'];
                        }else{
                            $quoteDetailsInsert[$i]['category'] = '';
                        }
                        $quoteDetailsInsert[$i]['manufacturer_name'] = 'N/A';
                        $quoteDetailsInsert[$i]['manufacturerCode'] = '';
                        $quoteDetailsInsert[$i]['quantity'] = $expectedQuantity[$i];
                        $quoteDetailsInsert[$i]['price'] = '';
                        $quoteDetailsInsert[$i]['source_part_id'] = '';
                        $quoteDetailsInsert[$i]['customer_part_id'] = $internalPart[$i];
                        $quoteDetailsInsert[$i]['date_code'] = '';
                        $quoteDetailsInsert[$i]['price_tiers'] = '';
                        $quoteDetailsInsert[$i]['available_data'] = '';
                        $quoteDetailsInsert[$i]['requested_quantity'] = $expectedQuantity[$i];
                        $quoteDetailsInsert[$i]['requested_cost'] = $data ['expectedUnitPrice'];
                        $quoteDetailsInsert[$i]['distributor_name'] = config("constants.DEFAULT_DISTRIBUTOR_NAME");
                        
                        if ($data['expectedDeliveryDate'] == "") {
                        } else {
                            $quoteDetailsInsert[$i]['requested_delivery_date'] = date('Y-m-d', strtotime($data['expectedDeliveryDate']));
                        }
                        $i++;
                    }
                    $response = QuoteDetail::insert($quoteDetailsInsert);
                }else{
                    $quoteDetails = new QuoteDetail();
                    $quoteDetails->quote_id = $quote->id;
                    $quoteDetails->item_name = $data['partNumber'];
                    
                    if($data['categoryName'] != ""){
                        $quoteDetails->category = $data['categoryName'];
                    }else{
                        $quoteDetails->category =  '';
                    }

                    $quoteDetails->manufacturer_name = 'N/A';
                    $quoteDetails->manufacturerCode = '';
                    $quoteDetails->quantity = '';
                    $quoteDetails->price = '';
                    $quoteDetails->source_part_id = '';
                    $quoteDetails->customer_part_id = '';
                    $quoteDetails->date_code = '';
                    $quoteDetails->price_tiers = '';
                    $quoteDetails->available_data = '';
                    $quoteDetails->requested_quantity =   $data['expectedQuantity'];
                    $quoteDetails->requested_cost   =   $data ['expectedUnitPrice'];
                    $quoteDetails->distributor_name = config("constants.DEFAULT_DISTRIBUTOR_NAME");
                    if ($data['expectedDeliveryDate'] == "") {
                    } else {
                        $quoteDetails->requested_delivery_date = date('Y-m-d', strtotime($data['expectedDeliveryDate']));
                    }
                    $response = $quoteDetails->save();
                }
                if ($response) {
                    $customerName       = \Auth::user()->name.' '.\Auth::user()->last_name;
                    $customerEmail      = \Auth::user()->email;
                    $data['name']       =  $customerName;
                    \Mail::to(config('constants.ADMIN_MAIL'))->send(new NewPartRequestMailToAdmin($data, config('constants.NEW_PART_MAIL_SUBJECT')));
                    \Mail::to($customerEmail)->send(new NewPartRequestMailToUser($data, config('constants.NEW_PART_MAIL_SUBJECT_USER')));
                    return response(['status' => true, 'message' => config('constants.NEW_PART_REQUEST_SUCCESS'), 'data' => '']);
                } else {
                    return response(['status' => false, 'message' => config('constants.COMMON_ERROR'), 'data' => '']);
                }
            } else {
                return response(['status' => false, 'message' => config('constants.NEW_PART_REQUEST_NOT_LOGIN'), 'data' => '']);
            }
        }catch (\Exception $ex) {
            return response(['status' => false, 'message' =>$ex->getMessage(), 'data' => '']);
        }
    }
}