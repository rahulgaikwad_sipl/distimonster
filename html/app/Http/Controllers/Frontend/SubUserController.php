<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Bio;
use App\Subscribe;
use Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Rap2hpoutre\FastExcel\FastExcel;
use Helpers; // Important
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use DB;
use App\Mail\CronPaymentSuccessMail;
use Hash;
use App\Mail\SubscribeSendWelComeMail;
use App\Mail\PaymentFailureMail;
use App\Mail\IntimationToAdminPaymentFailureMail;
use App\SubscriptionPlans;


/*
    * Controller BioController
    * Developer: Khalid
    * Date: 04/03/18
    * Purpose: Manage customer Bio Information
 */
class SubUserController extends Controller{

    protected $userSessionData;
/*
    * Create a new controller instance.
    * @return void
 */
    public function __construct()
    {
        Event::listen(Authenticated::class, function ($event) {
            $this->userSessionData = $event->user;
            if ($this->userSessionData['status'] == 0) {
                Helpers::sessionFlush();
            }
        });
    }

/*
    * List all team members
    * @return void
 */
    public function index(Request $request)
    {
        try {
        /*Get Auth User Id*/
        $userId = !empty(Auth::user()->id)?Auth::user()->id:'';
        if (!empty($userId) && empty(!empty($_POST))) {
            $user  = User::where('parent_user_id', $userId)->whereNull('deleted_at')->get(['id', 'name','last_name','email', 'note']); 
            $data['user']   = $user;
            $data['subscription_plans'] = SubscriptionPlans::Select('*')->Where('plan_id', Auth::user()->plan_id)->Where('is_active', 1)->first();
            return view('frontend.sub-user.sub-user-list', $data);
        }
        } catch (\Exception $ex) {
            return redirect('/my-team')->with('failure',config('constants.COMMON_ERROR'));
        }
    }//End Function
    
    /*function for get add user form*/
    public function getSubUserForm() {
        try {
            /*Get Auth User Id*/
            $userId = !empty(Auth::user()->id)?Auth::user()->id:'';
            if (!empty($userId) && empty(!empty($_POST))) {
                $user  = User::where('parent_user_id', $userId)->get(['id', 'name','last_name','email']);
                $data['userCount'] = count($user);
                $data['user'] = $user;
                $data['subscription_plans'] = SubscriptionPlans::Select('*')->Where('plan_id', Auth::user()->plan_id)->Where('is_active', 1)->first();
                if($data['userCount'] < $data['subscription_plans']->extra_users) 
                    return view('frontend.sub-user.add-sub-user',$data);
                else
                    return redirect('/my-team');
            }
        } catch (Exception $e) {
            return redirect('/my-team')->with('failure',config('constants.COMMON_ERROR'));
        }
    }//End Function
    
     /*function for add sub user into database*/
     public function AddSubUsers(Request $request) { 
        try {
            $userId = !empty(Auth::user()->id)?Auth::user()->id:'';
            if (!empty($userId)) {
                $userDetail = User::findOrFail($userId);
            }
            $userCount = count($request['user_email']);
            $subUserCount = User::getSubUsersCount();
            $paymentStatus = TRUE; 
            
            $subscription_plans = SubscriptionPlans::Select('*')->Where('plan_id', Auth::user()->plan_id)->Where('is_active', 1)->first();
            if(!empty($subscription_plans)){
                $freeUsers = $subscription_plans->free_users;
                $extraUsers = $subscription_plans->extra_users;
                $extraCost = $subscription_plans->user_unit_price;
                $paidUsers = $extraUsers - $freeUsers;
            }
            
            if($subUserCount >= $freeUsers){
                $grandTotal = $userCount*$extraCost;
                $paymentStatus = TRUE;
                if($userDetail->preferred_payment_type == 'credit')
                    $paymentStatus = $this->paymentAfterTrialExpire($userId, $grandTotal);
            }else {
                if($userCount >= $freeUsers){
                    $userTotalPayment = $userCount - $freeUsers;
                    $grandTotal = $userTotalPayment * $extraCost;
                    if($grandTotal > 0){
                        $paymentStatus = TRUE;
                        if($userDetail->preferred_payment_type == 'credit'){
                            $paymentStatus = $this->paymentAfterTrialExpire($userId, $grandTotal);
                        }
                    }else
                        $paymentStatus = TRUE;    
                }else{
                    $paymentStatus = TRUE;
                }
            }
            
            if($paymentStatus){
                for ($i=0; $i < $userCount; $i++) { 
                    //Generate random password and hash it
                    $subRandomPassword = $this->randString(8);
                    $subPassword = Hash::make($subRandomPassword);
                    $user = new User();
                    $user->name = $request['user_first_name'][$i];
                    $user->last_name = $request['user_last_name'][$i];
                    $user->email = $request['user_email'][$i];
                    $user->note = $request['user_note'][$i];
                    $user->password = $subPassword;
                    $user->plan_purchase_date = $userDetail->plan_purchase_date;
                    $user->free_trial_expiry  = $userDetail->free_trial_expiry;
                    $user->plan_expiry_date   = $userDetail->plan_expiry_date;
                    $user->is_sub_user        = 1;
                    $user->parent_user_id     = $userId;
                    $user->role_id            = 2;
                    $user->plan_id            = Auth::user()->plan_id;
                    $user->preferred_payment_type = $userDetail->preferred_payment_type;
                    $user->save();

                    //Subscribe Mail Data                    
                    $subUser = array();
                    $subUser['firstName'] = $request['user_first_name'][$i];
                    $subUser['emailAddress'] = $request['user_email'][$i];
                    $subUser['randomPassword'] = $subRandomPassword;
                    $subUser['note'] = $request['user_note'][$i];
                    $subUser['isSubUser'] = $user->is_sub_user;

                    if(!empty($user->id)){
                        //Send Email to Sub User
                        \Mail::to($user->email)->send(new SubscribeSendWelComeMail($subUser,config('constants.WELCOME_USER_SUBJECT')));
                    }

                }
                return redirect('/my-team')->with('success',config('constants.SUBUSER_ADDED_SUCCESS'));
            }else{
                return redirect('/my-team')->with('failure',config('constants.SUBUSER_ADDED_ERROR'));
            }  
        } catch (Exception $e) {
           return redirect('/my-team')->with('failure',config('constants.COMMON_ERROR'));
        }
    }

       /*
        * paymentAfterTrialExpire.
        * @return void
     */        
        public function paymentAfterTrialExpire($userId, $grandTotal){
            
            DB::beginTransaction();
            $checkFreeTrial = Subscribe::getUserBillingForPayment($userId);
            if(!empty($checkFreeTrial)){
                $billingId = $checkFreeTrial->sub_billing_id;
                $userId = $checkFreeTrial->user_id;
                $billingFirstName = $checkFreeTrial->billing_first_name;
                $billingLastName = $checkFreeTrial->billing_last_name;
                $grandTotalAmount = $checkFreeTrial->grand_total_amount;
                $userTotalAmount = $checkFreeTrial->user_total_amount;
                $billingEmail = $checkFreeTrial->billing_email;

                $getAuthToken = $this->getPaypalAuthToken();
                $data = array ('intent' => 'sale',  
                    'payer' =>  array (
                        'payment_method' => 'credit_card',
                        'funding_instruments' => array (
                            0 => array (
                                'credit_card_token' => array (
                                    'credit_card_id' => $checkFreeTrial->paypal_card_id,
                                    ),
                                ),
                            ),
                        ),
                    'transactions' => array (
                        0 => array (
                            'amount' => array (
                                'total' => $grandTotal,
                                'currency' => 'USD',
                                ),
                            'description' => 'Payment of your team members.',
                            ),
                        ),
                    );

                $paymentJson = json_encode($data);

                $getCard = $this->getCardFromPaypal($paymentJson, $getAuthToken);

                $paymentResponse = json_decode($getCard);

                if(!empty($paymentResponse) && $paymentResponse->state=="approved" && $paymentResponse->id!=""){
                    /*Success handling*/
                    if(!empty($paymentResponse->transactions)){
                        foreach($paymentResponse->transactions as $transaction){
                            if(!empty($transaction->related_resources)){
                                foreach($transaction->related_resources as $related_resources){
                                    if($related_resources->sale->state == "completed"){
                                        /*Payment state completed*/
                                        $updatedGrandTotal = $grandTotalAmount+$grandTotal;
                                        $updatedUserTotal = $userTotalAmount+$grandTotal;

                                        $updateBillingTable = array("grand_total_amount" => $updatedGrandTotal, "user_total_amount" => $updatedUserTotal, "is_paid" => 1, "updated_at" => date("Y-m-d H:i:s"));

                                        $updatePayment = Subscribe::updatePaymentStatus($updateBillingTable, $billingId);

                                        if($updatePayment){
                                            $transactionId = $related_resources->sale->id;
                                            $transactionArray = array(
                                                "transaction_id" => $related_resources->sale->id, 
                                                "payment_parent_id" => $related_resources->sale->parent_payment, 
                                                "transaction_by" => 1,
                                                "user_id" => $userId, 
                                                "payment_status" => $related_resources->sale->state);

                                            $transactionPayment = Subscribe::insertSubscriptionTransaction($transactionArray);

                                            if($transactionPayment['payment']!=""){
                                                //Subscribe Mail Data
                                                $subscribeUser = array();
                                                $subscribeUser['billingFirstName'] = $billingFirstName;
                                                $subscribeUser['billingLastName'] = $billingLastName;
                                                $subscribeUser['grandTotalAmount'] = $grandTotal;
                                                $subscribeUser['transactionId'] = $transactionId;
                                                $subscribeUser['billingEmail'] = $billingEmail;
                                                $subscribeUser['paymentDate'] = date("Y-m-d H:i:s");
                                                //Send Email to Subscribe User
                                                \Mail::to($billingEmail)->send(new CronPaymentSuccessMail($subscribeUser,config('constants.CRON_PAYMENT_SUCCESS')));
                                                DB::commit();
                                                return true;
                                            }
                                        }
                                    }else{
                                        /*Payment state not complete*/
                                        /*Disable User Account*/
                                        $updateUserExpiry = array("status" => 0, "updated_at" => date("Y-m-d H:i:s"));
                                        $updateUserExpiryDates = Subscribe::updateUserExpiryDates($updateUserExpiry, $userId);
                                        //Send Email to Subscribe User For Payment Failure
                                        $failureEmail = array();
                                        $failureEmail['billingFirstName'] = $billingFirstName;
                                        $failureEmail['billingLastName'] = $billingLastName;
                                        /* Below email is sent to User */
                                        \Mail::to($billingEmail)->send(new PaymentFailureMail($failureEmail,config('constants.CRON_PAYMENT_FAILURE')));

                                        /* Below email is sent to Admin of website */
                                        \Mail::to('dheeraj.solanki@systematixindia.com')->send(new IntimationToAdminPaymentFailureMail($failureEmail,config('constants.CRON_PAYMENT_FAILURE')));
                                        DB::commit();
                                        return false;
                                    }
                                }
                            }else{
                                /*Errror related resource handling*/
                                DB::rollBack();
                            }
                        }
                    }else{
                        /*Errror Transaction handling*/
                        DB::rollBack();
                    }
                }else{
                    /* Errror handling send failure email from here. */
                    /*Disable User Account*/
                    $updateUserExpiry = array("status" => 0, "updated_at" => date("Y-m-d H:i:s"));
                    $updateUserExpiryDates = Subscribe::updateUserExpiryDates($updateUserExpiry, $userId);
                    //Send Email to Subscribe User For Payment Failure
                    $failureEmail = array();
                    $failureEmail['billingFirstName'] = $billingFirstName;
                    $failureEmail['billingLastName'] = $billingLastName;
                    /* Below email is sent to User */
                    \Mail::to($billingEmail)->send(new PaymentFailureMail($failureEmail,config('constants.CRON_PAYMENT_FAILURE')));

                    /* Below email is sent to Admin of website */
                    \Mail::to('dheeraj.solanki@systematixindia.com')->send(new IntimationToAdminPaymentFailureMail($failureEmail,config('constants.CRON_PAYMENT_FAILURE')));
                    DB::commit();
                    return false;
                }                    
            }
        }//End Function
        
            
    /*
        * randString.
        * @return void
    */
        public function randString($length) {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789$#@!";
            return substr(str_shuffle($chars),0,$length);
        }
        
        public function getPaypalAuthToken(){
            
            $ch = curl_init();
            
            $clientId = config('constants.SUBSCRIPTION_CLIENT_ID');
            $secret = config('constants.SUBSCRIPTION_CLIENT_SECRET');
            $authUrl = config('constants.SUBSCRIPTION_AUTH_URL');
            
            curl_setopt($ch, CURLOPT_URL, $authUrl);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

            $result = curl_exec($ch);
            
            if(empty($result)){
                return false;
            }else{
                $json = json_decode($result);
                return $json->access_token;
            }
            curl_close($ch);
        }
        
        /*
            * getCardFromPaypal.
            * @return void
        */
        public function getCardFromPaypal($paymentJson, $authToken){
            
            $vaultUrl = "https://api.sandbox.paypal.com/v1/payments/payment";
            
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $vaultUrl);
            curl_setopt($ch1, CURLOPT_HEADER, false);
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch1, CURLOPT_POST, true);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($ch1, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Authorization: Bearer ".$authToken));
            curl_setopt($ch1, CURLOPT_POSTFIELDS, $paymentJson);

            $result1 = curl_exec($ch1);

            if(empty($result1)){
                return false;
            }else{
                return $result1;
            }
            curl_close($ch1);
        }//End Function

}//End Class