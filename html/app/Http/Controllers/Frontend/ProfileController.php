<?php
namespace App\Http\Controllers\Frontend;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Country;
use App\State;
use App\City;
use App\PaymentMethod;
use Helpers;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Authenticated;

class ProfileController extends Controller{

    private $_user;
    private $_response = array('status' => false, 'errors' => array());
    private $_request;

    protected $userSessionData;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct(Request $request)
    {
       $this->_request = $request;
       Event::listen(Authenticated::class, function ($event) {
            $this->userSessionData = $event->user;
            if ($this->userSessionData['status'] == 0) {
                Helpers::sessionFlush();
            } 
        });
    }

    /*
     * Name: login
     * Created By: SIPL
     * Created Date: 13-Feb-2018
     * Purpose: For show login page.
     */

    public function profile($id) {
        $data['title'] = 'Profile';
        $data['validationMessage'] = User::$registerUserValidationMessages;
        $data['states'] = ['' => 'Select State'];
        $data['cities'] = ['' => 'Select City'];
        $data['countries'] =   Country:: getAllCountry();
        $user           = User::findOrFail($id);
        $data['user']   = $user;
        $data['states']     = State::getSateByCountry(Auth::user()->country_id);
        $data['cities']     = City::getCityByState(Auth::user()->state_id);
        $data['paymentmethod']          =   PaymentMethod::getAllPaymentMethod();
        if(Auth::user()->id != $user->id){
            return redirect('/' )->with('failure',config('constants.COMMON_ERROR'));
        }
        return view('frontend.Profile.account-setting', $data);
    }

    public function updateProfile(Request $request, $id){
        try { 
            $data = $request->all();
            $validator = \Validator::make($data, User::updateUserProfileValidationRules(), User::$registerUserValidationMessages);
            if ($validator->fails()) {
                //Redirect user back with input if server side validation fails
                return redirect('/account-settings/'.$id)->withErrors($validator)->withInput();
            }else{
                if($request->has('change_password')){
                    $userId         =  Auth::user()->id;
                    $user           = User::findOrFail($userId);
                    if (\Hash::check($request->current_password, $user->password)) {
                    } else {
                        return redirect('/account-settings/'.$id)->with('failure',config('constants.INCORRECT_CURRENT_PASSWORD'));
                    }
                } 
                $user = User::updateUserProfile($request,$id);
                if($user['status'] == 200){
                    if($request->has('change_email')){
                        return redirect('/account-settings/'.$id)->with('success',config('constants.EMAIL_CHANGE_SUCCESS'));
                    }else if($request->has('change_email') && $request->has('change_password')){
                        return redirect()->intended('/account-settings/'.$id)->with('success',config('constants.EMAIL_PASS_CHANGE_SUCCESS'));
                    }else if($request->has('change_password')){
                        return redirect()->intended('/account-settings/'.$id)->with('success', config('constants.PASSWORD_CHANGE_SUCCESS'));
                    }

                    if(Session::get('previousroute')=='shipping' ){ 
                        return redirect()->to('shipping');
                    }else{
                        return redirect('/account-settings/'.$id)->with('success',config('constants.PROFILE_UPDATE_SUCCESS'));
                    }
                }else{
                    return redirect('/account-settings/'.$id)->with('failure',config('constants.COMMON_ERROR'));
                }
            }
        } catch (\Exception $ex) {
            return redirect()->to('profile/'.$id)->with('failure',config('constants.COMMON_ERROR'));
        }
    }
}