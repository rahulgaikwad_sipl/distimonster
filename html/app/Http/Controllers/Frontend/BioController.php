<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Bio;
use Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Rap2hpoutre\FastExcel\FastExcel;
use Helpers; // Important
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

/*
    * Controller BioController
    * Developer: Khalid
    * Date: 04/03/18
    * Purpose: Manage customer Bio Information
 */
class BioController extends Controller{

    protected $userSessionData;
/*
    * Create a new controller instance.
    * @return void
 */
    public function __construct()
    {
        Event::listen(Authenticated::class, function ($event) {
            $this->userSessionData = $event->user;
            if ($this->userSessionData['status'] == 0) {
                Helpers::sessionFlush();
            }
        });
    }

/*
    * List all bio
    * @return void
 */
    public function myBio(Request $request)
    {
        try {
        /*Get Auth User Id*/
        $userId = !empty(Auth::user()->id)?Auth::user()->id:'';
        
        if (!empty($userId) && empty(!empty($_POST))) {
            $user           = User::findOrFail($userId);
            $data['user']   = $user;
            return view('frontend.bio.mybio', $data);
        }else{
            $data = $request->all();
            $validator = \Validator::make($data, Bio::$addBioValidationRules, Bio::$addBioValidationMessages);
            if ($validator->fails()) {
                //Redirect user back with input if server side validation fails
                return redirect('/my-bio')->withErrors($validator)->withInput();
            }else{
                
                $saveBio = User::findOrFail($request->user_id);
                
                $saveBio->are_you_a = $request->are_you_a;
                if($request->are_you_a=="Other"){
                    $saveBio->are_you_a_text = $request->are_you_a_text;
                }else{
                    $saveBio->are_you_a_text = "";
                }
                
                $saveBio->your_company_a = $request->your_company_a;
                if($request->your_company_a=="Other"){
                    $saveBio->your_company_a_text = $request->your_company_a_text;
                }else{
                    $saveBio->your_company_a_text = "";
                }
                
                $saveBio->your_main_industry = $request->your_main_industry;
                if($request->your_main_industry=="Other"){
                    $saveBio->your_main_industry_text = $request->your_main_industry_text;
                }else{
                    $saveBio->your_main_industry_text = "";
                }
                $saveBio->mybio_added = 1;
                Session::Put('mybio_added', 1);
                if ($saveBio->save()) {
                    return redirect('/my-bio/')->with('success',config('constants.BIO_UPDATE_SUCCESS'));
                } else {
                    return redirect('/my-bio/')->with('failure',config('constants.COMMON_ERROR'));
                }
            }
        }
        } catch (\Exception $ex) {
            return redirect('/my-bio/')->with('failure',config('constants.COMMON_ERROR'));
        }
    }//End Function

}//End Class