<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Cart;
use Event;
use Session;
use App\Http\Controllers\Controller;
use Helpers; // Important
use App\PartDetail;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Redirect;
use App\Category;

class ProductController extends Controller{
    
    protected $userSessionData;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct(){
    }
    
    /*
    * Show the product list page.
    *
    * @return \Illuminate\Http\Response
*/
    public function productListing(){
        return view('frontend.products.listing');
    }
    
//FUNCTION TO GET DATA FROM ONLY ARROW AMERICA INVENTORY
    
    public function backupsearchProduct(Request $request, $partNum = null){
        $data = $request->all();
        if($partNum != null){
            $partNumber = $partNum;
        }else{
            $partNumber     = $request->input('partNumber');
        }
        $partArray      = array (array('partNum'=>urlencode($partNumber)));
        $finalArray     =  json_encode($partArray);
        $partNumberLength = strlen($partNumber);
        if($partNumberLength <= 3){
            $url= config('constants.ARROW_API_URL').'search/list?req={"request":{"login":"'.config('constants.ARROW_LOGIN_NAME').'","apikey":"'.config('constants.ARROW_API_KEY').'","remoteIp":"'.$_SERVER['REMOTE_ADDR'].'","useExact":"true","parts":'.$finalArray.'}}&rows=25';
            $response               = Helpers::curlRequest($url);
            $finalProductData       = [];
            $result                 = json_decode($response,true);

            if(!empty($result['itemserviceresult']['data'][0]['resultList'][0]['PartList']) && $result['itemserviceresult']['transactionArea'][0]['response']['returnCode']==0 && $result['itemserviceresult']['transactionArea'][0]['response']['success']==1) {
                $resultListData =$result['itemserviceresult']['data'][0]['resultList'][0]['PartList'];
                foreach ($resultListData as $partList){
                    $productData=array();
                    $newSourceArray = [];
                    $productData['itemId']              =  $partList['itemId'];
                    $productData['partNum']             =  $partList['partNum'];
                    $productData['requestedQty']        =   '';
                    $productData['manufacturer']        =  $partList['manufacturer']['mfrName'];
                    $productData['mfrCd']               =  $partList['manufacturer']['mfrCd'];
                    $productData['desc']                =  $partList['desc'];
                    if(isset($partList['EnvData'])){
                        $productData['EnvData']   =  $partList['EnvData'];
                    }
                    $newSourceCombineArray = array();
                    if(isset($partList['InvOrg']['sources'])){
                        $productData['sources_count']   =  count($partList['InvOrg']['sources']);
                        foreach($partList['InvOrg']['sources'] as $sources) {
                            $sourecArray = array();
                            $newSourcePartArray = [];
                            $sourecArray['displayName'] =$sources['displayName'];
                            $sourecArray['sourceCd'] =  $sources['sourceCd'];
                            $sourecArray['currency'] =  $sources['currency'];
                            $newSourceCombineArray['displayName'] =     $sources['displayName'];
                            $newSourceCombineArray['sourceCd'] =  $sources['sourceCd'];
                            $newSourceCombineArray['currency'] =  $sources['currency'];
                            foreach($sources['sourceParts'] as $sourcePart) {
                                 $newSourceCombineArray['sourcesPart'][] = $sourcePart;
                                $sourecPartArray = array();
                                $sourecPartArray['packSize']         =  $sourcePart['packSize'];
                                $sourecPartArray['sourcePartNumber']    =  $sourcePart['sourcePartNumber'];
                                $sourecPartArray['sourcePartId']        =  $sourcePart['sourcePartId'];
                                $sourecPartArray['dateCode']            =  $sourcePart['dateCode'];
                                $sourecPartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                                $sourecPartArray['inStock']             =  $sourcePart['inStock'];
                                $sourecPartArray['Availability']        =  $sourcePart['Availability'];
                                if(isset($sourcePart['Prices'])){
                                    $sourecPartArray['Prices']              =  $sourcePart['Prices'];
                                }else{
                                    $sourecPartArray['Prices']              =  [];
                                }
                                $newSourcePartArray[] =  $sourecPartArray;
                            }
                            $sourecArray['sourcesPart']   =   $newSourcePartArray ;
                            $newSourceArray[] =  $sourecArray;
                        }
                        $productData['sources'][]   =   $newSourceCombineArray ;
                      //  $productData['sources']   =   $newSourceArray ;
                    }
                    $finalProductData[]=$productData;
                    session(['productDetails' => $finalProductData]);
                }
                array_walk_recursive($finalProductData, 'Helpers::update_price');
                // Get current page form url e.x. &page=1
                $currentPage = LengthAwarePaginator::resolveCurrentPage();
                // Create a new Laravel collection from the array data
                $itemCollection = collect($finalProductData);
                // Define how many items we want to be visible in each page
                $perPage = 500;
                // Slice the collection to get the items to display in current page
                $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
                // Create our paginator and pass it to the view
                $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
                // set url path for generted links
                $paginatedItems->setPath($request->url());
                $data['finalData'] =  $paginatedItems;
                /* Set in session */
                $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));

                if(Session::has('searchProducts')) {
                    $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));
                    if(in_array($request->input('partNumber'), array_column(Session::get('searchProducts') , 'product_name'))) { // search value in the array
                        foreach (Session::get('searchProducts') as $keyp => $products) {
                            if ($products['product_name'] == $request->input('partNumber')) {
                                $event_data_display = Session::get('searchProducts');
                                unset($event_data_display[$keyp]);
                                Session(['searchProducts' => $event_data_display]);
                            }
                        }
                    }
                    $completeResult = array_merge_recursive($searchProductArray,Session::get('searchProducts'));
                    Session(['searchProducts' => $completeResult]);
                } else {
                    Session(['searchProducts' => $searchProductArray]);
                }
               
                /* Set in session */
                return view('frontend.products.listing',$data);
            }else{
                
                /* Set in session */
                $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>0));

                if(Session::has('searchProducts')) {
                    $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>0));
                    if(in_array($request->input('partNumber'), array_column(Session::get('searchProducts') , 'product_name'))) { // search value in the array
                        foreach (Session::get('searchProducts') as $keyp => $products) {
                            if ($products['product_name'] == $request->input('partNumber')) {
                                unset(Session::get('searchProducts')[$keyp]);
                            }
                        }
                    }
                    else {
                        $completeResult = array_merge_recursive($searchProductArray,Session::get('searchProducts'));
                        Session(['searchProducts' => $completeResult]);
                    }
                    //
                } else {
                    Session(['searchProducts' => $searchProductArray]);
                }
                /* Set in session */
                session(['previousroute' => 'search?partNumber='.urlencode($partNumber)]);
                return view('frontend.products.no-product-found');
            }
        }else{
            
            $slugHolder = $request->input('slug_holder');
            $dt = [];
            if(!empty($slugHolder)){
                Session(['search_part_name' => $partNumber]);
                return redirect('/category/'.$slugHolder);
            }

            // using for token search end point
            $url= config('constants.ARROW_API_URL').'search/list?req={"request":{"login":"'.config('constants.ARROW_LOGIN_NAME').'","apikey":"'.config('constants.ARROW_API_KEY').'","remoteIp":"'.$_SERVER['REMOTE_ADDR'].'","useExact":"true","parts":'.$finalArray.'}}&rows=25';

            $response           =   Helpers::curlRequest($url);
            $finalProductData   =   [];
            $result             =   json_decode($response,true);
//            echo "<pre>"; print_r($result); die();
            if($result['itemserviceresult']['data']){
               // if($result['itemserviceresult']['transactionArea'][0]['response']['returnCode']==0 && $result['itemserviceresult']['transactionArea'][0]['response']['success']==1){
                if(!empty($result['itemserviceresult']['data'][0]['resultList'][0]['PartList']) && $result['itemserviceresult']['transactionArea'][0]['response']['returnCode']==0 && $result['itemserviceresult']['transactionArea'][0]['response']['success']==1) {
                    $resultListData =$result['itemserviceresult']['data'][0]['resultList'][0]['PartList'];
                    // using for token search end point
                    // $resultListData =$result['itemserviceresult']['data'][0]['PartList'];
                    foreach ($resultListData as $partList){
                        $productData=array();
                        $newSourceArray = [];
                        $productData['itemId']              =  $partList['itemId'];
                        $productData['partNum']             =  $partList['partNum'];
                        $productData['requestedQty']        =   '';
                        $productData['manufacturer']        =  $partList['manufacturer']['mfrName'];
                        $productData['mfrCd']               =  $partList['manufacturer']['mfrCd'];
                        $productData['desc']                =  $partList['desc'];
                        if(isset($partList['EnvData'])){
                            $productData['EnvData']   =  $partList['EnvData'];
                        }
                        $newSourceCombineArray = array();
                            $newIndex = 0;
                        if(isset($partList['InvOrg']['sources'])){
                            $productData['sources_count']   =  count($partList['InvOrg']['sources']);
                            //Combining all the source parts
                            foreach($partList['InvOrg']['sources'] as $sources) {
                                //if( $sources['sourceCd'] ==  "ACNA"){
                                $sourceArray = array();
                                $newSourcePartArray = [];

                                $sourceArray['displayName'] =$sources['displayName'];
                                $sourceArray['sourceCd'] =  $sources['sourceCd'];
                                $sourceArray['currency'] =  $sources['currency'];

                                    $newSourceCombineArray['displayName'] =$sources['displayName'];
                                    $newSourceCombineArray['sourceCd'] =  $sources['sourceCd'];
                                    $newSourceCombineArray['currency'] =  $sources['currency'];

                                $i=0;
                                foreach($sources['sourceParts'] as $sourcePart) {
                                    $newSourceCombineArray['sourcesPart'][] = $sourcePart;
                                    $i++;
                                    $sourcePartArray = array();
                                    $sourcePartArray['packSize']         =  $sourcePart['packSize'];
                                    $sourcePartArray['sourcePartNumber']    =  $sourcePart['sourcePartNumber'];
                                    $sourcePartArray['sourcePartId']        =  $sourcePart['sourcePartId'];
                                    $sourcePartArray['dateCode']            =  $sourcePart['dateCode'];
                                    $sourcePartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                                    $sourcePartArray['inStock']             =  $sourcePart['inStock'];
                                    $sourcePartArray['Availability']        =  $sourcePart['Availability'];
                                    if(isset($sourcePart['Prices'])){
                                        $sourcePartArray['Prices']              =  $sourcePart['Prices'];
                                    }else{
                                        $sourcePartArray['Prices']              =  [];
                                    }
                                    $newSourcePartArray[] =  $sourcePartArray;
                                }
                                $sourceArray['sourcesPart']   =   $newSourcePartArray ;
                                $newSourceArray[] =  $sourceArray;
                                //}
                            }
                            $productData['sources'][]   =   $newSourceCombineArray ;
                           // $productData['sources']   =   $newSourceArray ;
                        }
                    }
                    
                    //Orweber API Response Merge Start
                        $finalProductData[]=$productData;
                        $orbewerData = $this->searchProductOrbweaver($partNumber, $slugHolder, $partNum = null);
                        if(!empty($orbewerData)){
                            $finalProductData[]=$orbewerData;
                        }
                    //Orweber API Response Merge End
                    
                    array_walk_recursive($finalProductData, 'Helpers::update_price');
                    session(['productDetails' => $finalProductData]);

                    // Get current page form url e.x. &page=1
                    $currentPage = LengthAwarePaginator::resolveCurrentPage();
                    // Create a new Laravel collection from the array data
                    $itemCollection = collect($finalProductData);
                    // Define how many items we want to be visible in each page
                    $perPage = 500;
                    // Slice the collection to get the items to display in current page
                    $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
                    // echo "<pre>";
                    // print_r($itemCollection->count());
                    // die;
                    // Create our paginator and pass it to the view
                    $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
                    // set url path for generted links
                    $paginatedItems->setPath($request->url());
                    $data['finalData'] =  $paginatedItems;
                    /* Set in session */
                    $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));

                    if(Session::has('searchProducts')) {
                        $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));
                        if(in_array($request->input('partNumber'), array_column(Session::get('searchProducts') , 'product_name'))) { // search value in the array
                            foreach (Session::get('searchProducts') as $keyp => $products) {
                                if ($products['product_name'] == $request->input('partNumber')) {
                                    $event_data_display = Session::get('searchProducts');
                                    unset($event_data_display[$keyp]);
                                    Session(['searchProducts' => $event_data_display]);
                                }
                            }
                        }
                        $completeResult = array_merge_recursive($searchProductArray,Session::get('searchProducts'));
                        Session(['searchProducts' => $completeResult]);
                        //
                    } else {
                        Session(['searchProducts' => $searchProductArray]);
                    }
                    
                    return view('frontend.products.listing',$data);
                
                }else{

                    $url  = config('constants.ARROW_API_URL').'search/token?login='.config('constants.ARROW_LOGIN_NAME').'&apikey='.config('constants.ARROW_API_KEY').'&remoteIp='.$_SERVER['REMOTE_ADDR'].'&search_token='.$partNumber.'&rows=25&start=1';

                    $response               =   Helpers::curlRequest($url);
                    $finalProductData       =   [];
                    $result                 =   json_decode($response,true);
                    if($result['itemserviceresult']['transactionArea'][0]['response']['returnCode']==0 && $result['itemserviceresult']['transactionArea'][0]['response']['success']==1){
                        $resultListData =$result['itemserviceresult']['data'][0]['PartList'];
                        foreach ($resultListData as $partList){
                            $productData=array();
                            $newSourceArray = [];
                            $productData['itemId']              =  $partList['itemId'];
                            $productData['partNum']             =  $partList['partNum'];
                            $productData['requestedQty']        =   '';
                            $productData['manufacturer']        =  $partList['manufacturer']['mfrName'];
                            $productData['mfrCd']               =  $partList['manufacturer']['mfrCd'];
                            $productData['desc']                =  $partList['desc'];
                            if(isset($partList['EnvData'])){
                                $productData['EnvData']   =  $partList['EnvData'];
                            }
                            $newSourceCombineArray = array();
                            $newIndex = 0;
                            if(isset($partList['InvOrg']['sources'])){
                                $productData['sources_count']   =  count($partList['InvOrg']['sources']);
                                foreach($partList['InvOrg']['sources'] as $sources) {
                                    $sourecArray = array();
                                    $newSourcePartArray = [];
                                    $sourecArray['displayName'] =$sources['displayName'];
                                    $sourecArray['sourceCd'] =  $sources['sourceCd'];
                                    $sourecArray['currency'] =  $sources['currency'];
                                    
                                    $newSourceCombineArray['displayName'] =$sources['displayName'];
                                    $newSourceCombineArray['sourceCd'] =  $sources['sourceCd'];
                                    $newSourceCombineArray['currency'] =  $sources['currency'];

                                    $i=0;
                                    foreach($sources['sourceParts'] as $sourcePart) {
                                        $newSourceCombineArray['sourcesPart'][] = $sourcePart;
                                        $i++;
                                        $sourecPartArray = array();
                                        $sourecPartArray['packSize'.$i]         =  $sourcePart['packSize'];
                                        $sourecPartArray['sourcePartNumber']    =  $sourcePart['sourcePartNumber'];
                                        $sourecPartArray['sourcePartId']        =  $sourcePart['sourcePartId'];
                                        $sourecPartArray['dateCode']            =  $sourcePart['dateCode'];
                                        $sourecPartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                                        $sourecPartArray['inStock']             =  $sourcePart['inStock'];
                                        $sourecPartArray['Availability']        =  $sourcePart['Availability'];
                                        if(isset($sourcePart['Prices'])){
                                            $sourecPartArray['Prices']              =  $sourcePart['Prices'];
                                        }else{
                                            $sourecPartArray['Prices']              =  [];
                                        }
                                        $newSourcePartArray[] =  $sourecPartArray;
                                    }
                                    $sourecArray['sourcesPart']   =   $newSourcePartArray ;
                                    $newSourceArray[] =  $sourecArray;

                                }
                                $productData['sources'][]   =   $newSourceCombineArray ;
                               // $productData['sources']   =   $newSourceArray ;
                            }
                            $finalProductData[]=$productData;
                        }
                        //Orweber API Response Merge Start
                        $finalProductData[]=$productData;
                        $orbewerData = $this->searchProductOrbweaver($partNumber, $slugHolder, $partNum = null);
                        if(!empty($orbewerData)){
                            $finalProductData[]=$orbewerData;
                        }
                        //Orweber API Response Merge End
                        session(['productDetails' => $finalProductData]);
                        
                        array_walk_recursive($finalProductData, 'Helpers::update_price');
                        // Get current page form url e.x. &page=1
                        $currentPage = LengthAwarePaginator::resolveCurrentPage();
                        // Create a new Laravel collection from the array data
                        $itemCollection = collect($finalProductData);
                        // Define how many items we want to be visible in each page
                        $perPage = 500;
                        // Slice the collection to get the items to display in current page
                        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
                        // Create our paginator and pass it to the view
                        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
                        // set url path for generted links
                        $paginatedItems->setPath($request->url());
                        $data['finalData'] =  $paginatedItems;

                        /* Set in session */
                        $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));

                        if(Session::has('searchProducts')) {
                            $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));
                            if(in_array($request->input('partNumber'), array_column(Session::get('searchProducts') , 'product_name'))) { // search value in the array
                                foreach (Session::get('searchProducts') as $keyp => $products) {
                                    if ($products['product_name'] == $request->input('partNumber')) {
                                        $event_data_display = Session::get('searchProducts');
                                        unset($event_data_display[$keyp]);
                                        Session(['searchProducts' => $event_data_display]);
                                    }
                                }
                            }

                            $completeResult = array_merge_recursive($searchProductArray,Session::get('searchProducts'));
                            Session(['searchProducts' => $completeResult]);
                            //
                        } else {
                            Session(['searchProducts' => $searchProductArray]);

                        }
                        return view('frontend.products.listing',$data);
                    }else{
                        /* Set in session */
                        $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>0));

                        if(Session::has('searchProducts')) {
                            $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>0));
                            if(in_array($request->input('partNumber'), array_column(Session::get('searchProducts') , 'product_name'))) { // search value in the array
                                foreach (Session::get('searchProducts') as $keyp => $products) {
                                    if ($products['product_name'] == $request->input('partNumber')) {
                                        unset(Session::get('searchProducts')[$keyp]);
                                    }
                                }
                            }
                            else {
                                $completeResult = array_merge_recursive($searchProductArray,Session::get('searchProducts'));
                                Session(['searchProducts' => $completeResult]);
                            }
                        } else {
                            Session(['searchProducts' => $searchProductArray]);
                        }
                        /* Set in session */
                        session(['previousroute' => 'search?partNumber='.urlencode($partNumber)]);

                        return view('frontend.products.no-product-found');
                    }
                    // Show not item page when item not found with search token api end point
                }
            }else{


                session(['previousroute' => 'search?partNumber='.urlencode($partNumber)]);
                return view('frontend.products.no-product-found');
            }
        }
    }

    /*
     * suggestPartDetails
     */
    public function suggestPartDetails(Request $request){
        //echo '[{"name": "AFGHANISTAN"},{"name": "ALBANIA"},{"name": "ALGERIA"},{"name": "AMERICAN SAMOA"},{"name": "ANDORRA"}]';
        $searchParam = $request->phrase;
            if(!empty($searchParam)){
            $data['categories'] = Category::getCategoryListByKeyword($searchParam);
            echo json_encode($data['categories']);
            die();
        }
    }
    
    /* Clear Session Function */
    function clearSearchHistory() {
        Session::forget('searchProducts');
        echo 1;
    }

//FUNCTION TO GET DATA FROM ONLY ORBWEAVER
    
    
    public function searchProduct(Request $request, $partNum = null){
            $data = $request->all();
            if($partNum != null){
                $partNumber = $partNum;
            }else{
                $partNumber     = $request->input('partNumber');
            }
            $partArray      = array (array('partNum'=>urlencode($partNumber)));
            $finalArray     =  json_encode($partArray);
            $partNumberLength = strlen($partNumber);

            $slugHolder = '';//$request->input('slug_holder');
            $dt = [];
            if(!empty($slugHolder)){
                Session(['search_part_name' => $partNumber]);
                return redirect('/category/'.$slugHolder);
            }
            /*CALL ARROW API START*/
            $arrowData = $this->searchProductArrow($partNumber, $slugHolder, $partNum = null);
            //echo "<pre>"; print_r($arrowData);die();
            /*CALL ARROW API END*/
            /*CALL ORBEWER API START*/
            //$orweaverData = $this->searchProductOrbweaver($partNumber, $slugHolder, $partNum = null);
//            echo "<pre>"; print_r($orweaverData);die();
            /*CALL ORBEWER API END*/
            $finalProductData = array();
            if(!empty($arrowData)){
                foreach($arrowData as $arr){
                    $finalProductData[] = $arr;
                }
            }
            if(!empty($orweaverData)){
                foreach($orweaverData as $orwea){
                    $finalProductData[] = $orwea;
                }
            }
//            echo "<pre>";
//            print_r($finalProductData);
//            die();
            if(empty($arrowData) && empty($orweaverData)){
                session(['previousroute' => 'search?partNumber='.urlencode($partNumber)]);
                return view('frontend.products.no-product-found');
            }
            
            // using for token search end point
            array_walk_recursive($finalProductData, 'Helpers::update_price');
            session(['productDetails' => $finalProductData]);
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($finalProductData);
            // Define how many items we want to be visible in each page
            $perPage = 500;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['finalData'] =  $paginatedItems;
            /* Set in session */
            $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));

            if(Session::has('searchProducts')) {
                $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));
                if(in_array($request->input('partNumber'), array_column(Session::get('searchProducts') , 'product_name'))) { // search value in the array
                    foreach (Session::get('searchProducts') as $keyp => $products) {
                        if ($products['product_name'] == $request->input('partNumber')) {
                            $event_data_display = Session::get('searchProducts');
                            unset($event_data_display[$keyp]);
                            Session(['searchProducts' => $event_data_display]);
                        }
                    }
                }
                $completeResult = array_merge_recursive($searchProductArray,Session::get('searchProducts'));
                Session(['searchProducts' => $completeResult]);
                //
            } else {
                Session(['searchProducts' => $searchProductArray]);
            }

            return view('frontend.products.listing',$data);

//        }else{
//            session(['previousroute' => 'search?partNumber='.urlencode($partNumber)]);
//            return view('frontend.products.no-product-found');
//        }
    }    

    public function searchProductArrow($partNumber, $slug_holder, $partNum = null){

        if($partNum != null){
            $partNumber = $partNum;
        }else{
            $partNumber     = $partNumber;
        }
      //  echo $partNumber; die();
        $partArray      = array (array('partNum'=>urlencode($partNumber)));
        $finalArray     =  json_encode($partArray);
        $partNumberLength = strlen($partNumber);
        
        if($partNumberLength <= 3){
            $url= config('constants.ARROW_API_URL').'search/list?req={"request":{"login":"'.config('constants.ARROW_LOGIN_NAME').'","apikey":"'.config('constants.ARROW_API_KEY').'","remoteIp":"'.$_SERVER['REMOTE_ADDR'].'","useExact":"true","parts":'.$finalArray.'}}&rows=25';
            $response               = Helpers::curlRequest($url);
            $finalProductData       = [];
            $result                 = json_decode($response,true);

            if(!empty($result['itemserviceresult']['data'][0]['resultList'][0]['PartList']) && $result['itemserviceresult']['transactionArea'][0]['response']['returnCode']==0 && $result['itemserviceresult']['transactionArea'][0]['response']['success']==1) {
                $resultListData =$result['itemserviceresult']['data'][0]['resultList'][0]['PartList'];
                foreach ($resultListData as $partList){
                    $productData=array();
                    $newSourceArray = [];
                    $productData['distributorName']              =  'Arrow';
                    $productData['itemId']              =  $partList['itemId'];
                    $productData['partNum']             =  $partList['partNum'];
                    $productData['requestedQty']        =   '';
                    $productData['manufacturer']        =  $partList['manufacturer']['mfrName'];
                    $productData['mfrCd']               =  $partList['manufacturer']['mfrCd'];
                    $productData['desc']                =  $partList['desc'];
                    if(isset($partList['EnvData'])){
                        $productData['EnvData']   =  $partList['EnvData'];
                    }
                    $newSourceCombineArray = array();
                    if(isset($partList['InvOrg']['sources'])){
                        $productData['sources_count']   =  count($partList['InvOrg']['sources']);
                        foreach($partList['InvOrg']['sources'] as $sources) {
                            $sourecArray = array();
                            $newSourcePartArray = [];
                            $sourecArray['displayName'] =$sources['displayName'];
                            $sourecArray['sourceCd'] =  $sources['sourceCd'];
                            $sourecArray['currency'] =  $sources['currency'];
                            $newSourceCombineArray['displayName'] =     $sources['displayName'];
                            $newSourceCombineArray['sourceCd'] =  $sources['sourceCd'];
                            $newSourceCombineArray['currency'] =  $sources['currency'];
                            foreach($sources['sourceParts'] as $sourcePart) {
                                 $newSourceCombineArray['sourcesPart'][] = $sourcePart;
                                $sourecPartArray = array();
                                $sourecPartArray['packSize']         =  $sourcePart['packSize'];
                                $sourecPartArray['sourcePartNumber']    =  $sourcePart['sourcePartNumber'];
                                $sourecPartArray['sourcePartId']        =  $sourcePart['sourcePartId'];
                                $sourecPartArray['dateCode']            =  $sourcePart['dateCode'];
                                $sourecPartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                                $sourecPartArray['inStock']             =  $sourcePart['inStock'];
                                $sourecPartArray['Availability']        =  $sourcePart['Availability'];
                                if(isset($sourcePart['Prices'])){
                                    $sourecPartArray['Prices']              =  $sourcePart['Prices'];
                                }else{
                                    $sourecPartArray['Prices']              =  [];
                                }
                                $newSourcePartArray[] =  $sourecPartArray;
                            }
                            $sourecArray['sourcesPart']   =   $newSourcePartArray ;
                            $newSourceArray[] =  $sourecArray;
                        }
                        $productData['sources'][]   =   $newSourceCombineArray ;
                      //  $productData['sources']   =   $newSourceArray ;
                    }
                    $finalProductData[]=$productData;
                }
                return $finalProductData;
            }
            
        }else{
            
            $slugHolder = $slug_holder;
            $dt = [];
            if(!empty($slugHolder)){
                Session(['search_part_name' => $partNumber]);
                return redirect('/category/'.$slugHolder);
            }
            // using for token search end point
            $url= config('constants.ARROW_API_URL').'search/list?req={"request":{"login":"'.config('constants.ARROW_LOGIN_NAME').'","apikey":"'.config('constants.ARROW_API_KEY').'","remoteIp":"'.$_SERVER['REMOTE_ADDR'].'","useExact":"true","parts":'.$finalArray.'}}&rows=25';

            $response           =   Helpers::curlRequest($url);
            $finalProductData   =   [];
            $result             =   json_decode($response,true);
            if($result['itemserviceresult']['data']){
                if(!empty($result['itemserviceresult']['data'][0]['resultList'][0]['PartList']) && $result['itemserviceresult']['transactionArea'][0]['response']['returnCode']==0 && $result['itemserviceresult']['transactionArea'][0]['response']['success']==1) {
                    $resultListData =$result['itemserviceresult']['data'][0]['resultList'][0]['PartList'];
                    // using for token search end point
                    foreach ($resultListData as $partList){
                        $productData=array();
                        $newSourceArray = [];
                        $productData['distributorName']              =  'Arrow';
                        $productData['itemId'] = $partList['itemId'];
                        $productData['partNum'] = $partList['partNum'];
                        $productData['requestedQty'] = '';
                        $productData['manufacturer'] = $partList['manufacturer']['mfrName'];
                        $productData['mfrCd'] = $partList['manufacturer']['mfrCd'];
                        $productData['desc'] =  $partList['desc'];
                        if(isset($partList['EnvData'])){
                            $productData['EnvData'] = $partList['EnvData'];
                        }
                        $newSourceCombineArray = array();
                            $newIndex = 0;
                        if(isset($partList['InvOrg']['sources'])){
                            $productData['sources_count']   =  count($partList['InvOrg']['sources']);
                            //Combining all the source parts
                            foreach($partList['InvOrg']['sources'] as $sources) {
                                $sourceArray = array();
                                $newSourcePartArray = [];
                                $sourceArray['displayName'] =$sources['displayName'];
                                $sourceArray['sourceCd'] =  $sources['sourceCd'];
                                $sourceArray['currency'] =  $sources['currency'];

                                    $newSourceCombineArray['displayName'] =$sources['displayName'];
                                    $newSourceCombineArray['sourceCd'] =  $sources['sourceCd'];
                                    $newSourceCombineArray['currency'] =  $sources['currency'];

                                $i=0;
                                foreach($sources['sourceParts'] as $sourcePart) {
                                    $newSourceCombineArray['sourcesPart'][] = $sourcePart;
                                    $i++;
                                    $sourcePartArray = array();
                                    $sourcePartArray['packSize']         =  $sourcePart['packSize'];
                                    $sourcePartArray['sourcePartNumber']    =  $sourcePart['sourcePartNumber'];
                                    $sourcePartArray['sourcePartId']        =  $sourcePart['sourcePartId'];
                                    $sourcePartArray['dateCode']            =  $sourcePart['dateCode'];
                                    $sourcePartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                                    $sourcePartArray['inStock']             =  $sourcePart['inStock'];
                                    $sourcePartArray['Availability']        =  $sourcePart['Availability'];
                                    if(isset($sourcePart['Prices'])){
                                        $sourcePartArray['Prices']              =  $sourcePart['Prices'];
                                    }else{
                                        $sourcePartArray['Prices']              =  [];
                                    }
                                    $newSourcePartArray[] =  $sourcePartArray;
                                }
                                $sourceArray['sourcesPart']   =   $newSourcePartArray ;
                                $newSourceArray[] =  $sourceArray;
                                //}
                            }
                            $productData['sources'][]   =   $newSourceCombineArray ;
                           // $productData['sources']   =   $newSourceArray ;
                        }
                    }
                    $finalProductData[]=$productData;
                    return $finalProductData;
                }else{
                    $url  = config('constants.ARROW_API_URL').'search/token?login='.config('constants.ARROW_LOGIN_NAME').'&apikey='.config('constants.ARROW_API_KEY').'&remoteIp='.$_SERVER['REMOTE_ADDR'].'&search_token='.$partNumber.'&rows=25&start=1';

                    $response = Helpers::curlRequest($url);
                    $finalProductData = [];
                    $result = json_decode($response,true);
                    if($result['itemserviceresult']['transactionArea'][0]['response']['returnCode']==0 && $result['itemserviceresult']['transactionArea'][0]['response']['success']==1){
                        $resultListData =$result['itemserviceresult']['data'][0]['PartList'];
                        foreach ($resultListData as $partList){
                            $productData=array();
                            $newSourceArray = [];
                            $productData['distributorName'] =  'Arrow';
                            $productData['itemId'] = $partList['itemId'];
                            $productData['partNum'] = $partList['partNum'];
                            $productData['requestedQty'] = '';
                            $productData['manufacturer'] = $partList['manufacturer']['mfrName'];
                            $productData['mfrCd'] = $partList['manufacturer']['mfrCd'];
                            $productData['desc'] = $partList['desc'];
                            if(isset($partList['EnvData'])){
                                $productData['EnvData'] = $partList['EnvData'];
                            }
                            $newSourceCombineArray = array();
                            $newIndex = 0;
                            if(isset($partList['InvOrg']['sources'])){
                                $productData['sources_count']   =  count($partList['InvOrg']['sources']);
                                foreach($partList['InvOrg']['sources'] as $sources) {
                                    $sourecArray = array();
                                    $newSourcePartArray = [];
                                    $sourecArray['displayName'] =$sources['displayName'];
                                    $sourecArray['sourceCd'] =  $sources['sourceCd'];
                                    $sourecArray['currency'] =  $sources['currency'];
                                    $newSourceCombineArray['displayName'] =$sources['displayName'];
                                    $newSourceCombineArray['sourceCd'] =  $sources['sourceCd'];
                                    $newSourceCombineArray['currency'] =  $sources['currency'];
                                    $i=0;
                                    foreach($sources['sourceParts'] as $sourcePart) {
                                        $newSourceCombineArray['sourcesPart'][] = $sourcePart;
                                        $i++;
                                        $sourecPartArray = array();
                                        $sourecPartArray['packSize'.$i] =  $sourcePart['packSize'];
                                        $sourecPartArray['sourcePartNumber'] =  $sourcePart['sourcePartNumber'];
                                        $sourecPartArray['sourcePartId'] =  $sourcePart['sourcePartId'];
                                        $sourecPartArray['dateCode'] =  $sourcePart['dateCode'];
                                        $sourecPartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                                        $sourecPartArray['inStock'] =  $sourcePart['inStock'];
                                        $sourecPartArray['Availability'] =  $sourcePart['Availability'];
                                        if(isset($sourcePart['Prices'])){
                                            $sourecPartArray['Prices'] =  $sourcePart['Prices'];
                                        }else{
                                            $sourecPartArray['Prices'] =  [];
                                        }
                                        $newSourcePartArray[] = $sourecPartArray;
                                    }
                                    $sourecArray['sourcesPart'] = $newSourcePartArray ;
                                    $newSourceArray[] = $sourecArray;
                                }
                                $productData['sources'][] = $newSourceCombineArray ;
                            }
                            $finalProductData[]=$productData;
                        }
                        return $finalProductData;
                    }
                }
            }
        }
    }
        
    public function searchProductOrbweaver($partNumber, $slug_holder, $partNum = null){
        if($partNum != null){
            $partNumber = $partNum;
        }else{
            $partNumber     = $partNumber;
        }
        $partArray      = array (array('partNum'=>urlencode($partNumber)));
        $finalArray     =  json_encode($partArray);
        $partNumberLength = strlen($partNumber);
        $slugHolder = $slug_holder;
        $dt = [];
        if(!empty($slugHolder)){
            Session(['search_part_name' => $partNumber]);
            return redirect('/category/'.$slugHolder);
        }
        // using for token search end point
        $url= config('constants.ORBWEAVER_API_URL').'part/lookup?lookup_value='.$partNumber.'';
        $response = Helpers::curlRequestOrbweaver($url);
        $finalProductData = [];
        $result = json_decode($response,true);
            if(isset($result) && !empty($result['offers'])){
                $resultListData =$result['offers'];
                // using for token search end point
                foreach ($resultListData as $partList){
                    $productData=array();
                    $newSourceArray = [];
                    $productData['distributorName'] = config('constants.ORBWEAVER_DISTRIBUTOR_NAME');
                    $productData['itemId'] = $partList['supplier_part_number'].'_'.$partList['manufacturer_id'];
                    $productData['partNum'] = $partList['mpn'];
                    $productData['requestedQty'] = '';
                    $productData['manufacturer'] = $partList['manufacturer_name'];
                    $productData['mfrCd'] = '';
                    $productData['desc'] =  $partList['description'];
                    if(isset($partList['compliance'])){
                        $compliance = [];
                        foreach($partList['compliance'] as $comArray){
                            foreach($comArray as $comKey){
                                if($comArray['name']){
                                    $comArray['displayLabel'] = $comArray['name'];
                                }
                                if($comArray['value']){
                                    $comArray['displayValue'] = $comArray['value'];
                                }
                            }
                            unset($comArray['name']);
                            unset($comArray['value']);
                            array_push($compliance, $comArray);
                        }
                        $productData['EnvData']['compliance']   =  $compliance;
                    }
                    $newSourceCombineArray = array();
                    $newIndex = 0;
                    $productData['sources_count']   =  count($resultListData);
                    $sourceArray = array();
                    $newSourcePartArray = [];
                    $sourceArray['displayName'] =$partList['datasource_name'];
                    $sourceArray['sourceCd'] =  '';
                    $sourceArray['currency'] =  'USD';

                    $newSourceCombineArray['displayName'] =$partList['datasource_name'];
                    $newSourceCombineArray['sourceCd'] =  '';
                    $newSourceCombineArray['currency'] =  'USD';

                    $i=0;
                    foreach($resultListData as $sourcePart) {
                        $i++;
                        $sourcePartArray = array();
                        $sourcePartArray['packSize']         =  $sourcePart['order_mult_qty'];
                        $sourcePartArray['minimumOrderQuantity']         =  $sourcePart['min_order_qty'];
                        $sourcePartArray['sourcePartNumber']    =  '';
                        $sourcePartArray['sourcePartId']        =  $sourcePart['supplier_part_number'];

                        if(!empty($sourcePart['prices'])){
                            $priceArrays = [];
                            foreach($sourcePart['prices'] as $priceArray){
                                foreach($priceArray as $priceKey){
                                    if($priceArray['unit_price']){
                                        $priceArray['displayPrice'] = $priceArray['unit_price'];
                                    }
                                    if($priceArray['unit_price']){
                                        $priceArray['price'] = $priceArray['unit_price'];
                                    }
                                    if($priceArray['from_qty']){
                                        $priceArray['minQty'] = $priceArray['from_qty'];
                                    }
                                    if($priceArray['to_qty']){
                                        $priceArray['maxQty'] = $priceArray['to_qty'];
                                    }
                                }
                                unset($priceArray['from_qty']);
                                unset($priceArray['to_qty']);
                                unset($priceArray['unit_price']);
                                unset($priceArray['currency']);
                                $sourcePartArray['Prices']['resaleList'][] = $priceArray;
                            }
                        }else{
                            $sourcePartArray['Prices']['resaleList']  =  [];
                        }
                        $sourcePartArray['Availability'][0]['fohQty'] =  $sourcePart['available_qty'];
                        $sourcePartArray['Availability'][0]['availabilityCd'] =  '';
                        $sourcePartArray['Availability'][0]['availabilityMessage'] =  !empty($sourcePart['available_qty'])?'In Stock':'No Stock';
                        $sourcePartArray['Availability'][0]['pipeline'] =  array();

                        $sourcePartArray['customerSpecificPricing'] = array();
                        $sourcePartArray['customerSpecificInventory'] = array();                                    
                        $sourcePartArray['dateCode'] = '';
                        $sourcePartArray['resources'] = array();

                        if($sourcePart['available_qty']>0){
                            $sourcePartArray['inStock'] =  1;
                        }else{
                            $sourcePartArray['inStock'] =  0;
                        }
                        $sourcePartArray['mfrLeadTime'] = $sourcePart['manufacturer_lead_time'];
                        $sourcePartArray['isNcnr'] = '';
                        $sourcePartArray['isNpi'] = '';
                        $sourcePartArray['isASA'] = '';
                        $sourcePartArray['requestQuantity'] = 0;
                        $sourcePartArray['productCode'] = '';
                        $sourcePartArray['iccLevels'] = array();

                        $sourcePartArray['cloudMfrCode'] = '';
                        $sourcePartArray['eccnCode'] = '';
                        $sourcePartArray['htsCode'] = '';
                        $sourcePartArray['countryOfOrigin'] = '';
                        $sourcePartArray['locationId'] = '';
                        $sourcePartArray['containerType'] = $sourcePart['pkg_type'];

                        $newSourcePartArray[] =  $sourcePartArray;
                        $newSourceCombineArray['sourcesPart'][] = $sourcePartArray;
                    }
                        $sourceArray['sourcesPart']   =   $newSourcePartArray ;
                        $newSourceArray[] =  $sourceArray;
                        $productData['sources'][]   =   $newSourceCombineArray ;
                }
                $finalProductData[]=$productData;
                return $finalProductData;
            }
        }

//BACKUP FUNCTION
        
    public function Backup_searchProductOrbweaver(Request $request, $slug_holder, $partNum = null){
//    public function searchProductOrbweaver($partNumber, $slug_holder, $partNum = null){

        //$data = $request->all();
        if($partNum != null){
            $partNumber = $partNum;
        }else{
//            $partNumber     = $partNumber;
             $partNumber     = $request->input('partNumber');
        }
        $partArray      = array (array('partNum'=>urlencode($partNumber)));
        $finalArray     =  json_encode($partArray);
        $partNumberLength = strlen($partNumber);
        $slugHolder = $slug_holder;
        $dt = [];
        if(!empty($slugHolder)){
            Session(['search_part_name' => $partNumber]);
            return redirect('/category/'.$slugHolder);
        }
        // using for token search end point
        $url= config('constants.ORBWEAVER_API_URL').'part/lookup?lookup_value='.$partNumber.'';
        $response = Helpers::curlRequestOrbweaver($url);
        $finalProductData = [];
        $result = json_decode($response,true);
            if(isset($result) && !empty($result['offers'])){
                $resultListData =$result['offers'];
                // using for token search end point
                foreach ($resultListData as $partList){
                    $productData=array();
                    $newSourceArray = [];
                    $productData['itemId'] = $partList['datasource_id'];
                    $productData['partNum'] = $partList['mpn'];
                    $productData['requestedQty'] = '';
                    $productData['manufacturer'] = $partList['manufacturer_name'];
                    $productData['mfrCd'] = '';
                    $productData['desc'] =  $partList['description'];
                    if(isset($partList['compliance'])){
                        $compliance = [];
                        foreach($partList['compliance'] as $comArray){
                            foreach($comArray as $comKey){
                                if($comArray['name']){
                                    $comArray['displayLabel'] = $comArray['name'];
                                }
                                if($comArray['value']){
                                    $comArray['displayValue'] = $comArray['value'];
                                }
                            }
                            unset($comArray['name']);
                            unset($comArray['value']);
                            array_push($compliance, $comArray);
                        }
                        $productData['EnvData']['compliance']   =  $compliance;
                    }
                    $newSourceCombineArray = array();
                    $newIndex = 0;
                    $productData['sources_count']   =  count($resultListData);
                    $sourceArray = array();
                    $newSourcePartArray = [];
                    $sourceArray['displayName'] =$partList['datasource_name'];
                    $sourceArray['sourceCd'] =  '';
                    $sourceArray['currency'] =  'USD';

                    $newSourceCombineArray['displayName'] =$partList['datasource_name'];
                    $newSourceCombineArray['sourceCd'] =  '';
                    $newSourceCombineArray['currency'] =  'USD';

                    $i=0;
                    foreach($resultListData as $sourcePart) {
                        $i++;
                        $sourcePartArray = array();
                        $sourcePartArray['packSize']         =  $sourcePart['order_mult_qty'];
                        $sourcePartArray['minimumOrderQuantity']         =  $sourcePart['min_order_qty'];
                        $sourcePartArray['sourcePartNumber']    =  '';
                        $sourcePartArray['sourcePartId']        =  $sourcePart['supplier_part_number'];

                        if(!empty($sourcePart['prices'])){
                            $priceArrays = [];
                            foreach($sourcePart['prices'] as $priceArray){
                                foreach($priceArray as $priceKey){
                                    if($priceArray['unit_price']){
                                        $priceArray['displayPrice'] = $priceArray['unit_price'];
                                    }
                                    if($priceArray['unit_price']){
                                        $priceArray['price'] = $priceArray['unit_price'];
                                    }
                                    if($priceArray['from_qty']){
                                        $priceArray['minQty'] = $priceArray['from_qty'];
                                    }
                                    if($priceArray['to_qty']){
                                        $priceArray['maxQty'] = $priceArray['to_qty'];
                                    }
                                }
                                unset($priceArray['from_qty']);
                                unset($priceArray['to_qty']);
                                unset($priceArray['unit_price']);
                                unset($priceArray['currency']);
                                $sourcePartArray['Prices']['resaleList'][] = $priceArray;
                            }
                        }else{
                            $sourcePartArray['Prices']['resaleList']  =  [];
                        }
                        $sourcePartArray['Availability'][0]['fohQty'] =  $sourcePart['available_qty'];
                        $sourcePartArray['Availability'][0]['availabilityCd'] =  '';
                        $sourcePartArray['Availability'][0]['availabilityMessage'] =  !empty($sourcePart['available_qty'])?'In Stock':'No Stock';
                        $sourcePartArray['Availability'][0]['pipeline'] =  array();

                        $sourcePartArray['customerSpecificPricing'] = array();
                        $sourcePartArray['customerSpecificInventory'] = array();                                    
                        $sourcePartArray['dateCode'] = '';
                        $sourcePartArray['resources'] = array();

                        if($sourcePart['available_qty']>0){
                            $sourcePartArray['inStock'] =  1;
                        }else{
                            $sourcePartArray['inStock'] =  0;
                        }
                        $sourcePartArray['mfrLeadTime'] = $sourcePart['manufacturer_lead_time'];
                        $sourcePartArray['isNcnr'] = '';
                        $sourcePartArray['isNpi'] = '';
                        $sourcePartArray['isASA'] = '';
                        $sourcePartArray['requestQuantity'] = 0;
                        $sourcePartArray['productCode'] = '';
                        $sourcePartArray['iccLevels'] = array();

                        $sourcePartArray['cloudMfrCode'] = '';
                        $sourcePartArray['eccnCode'] = '';
                        $sourcePartArray['htsCode'] = '';
                        $sourcePartArray['countryOfOrigin'] = '';
                        $sourcePartArray['locationId'] = '';
                        $sourcePartArray['containerType'] = $sourcePart['pkg_type'];

                        $newSourcePartArray[] =  $sourcePartArray;
                        $newSourceCombineArray['sourcesPart'][] = $sourcePartArray;
                    }
                        $sourceArray['sourcesPart']   =   $newSourcePartArray ;
                        $newSourceArray[] =  $sourceArray;
                        $productData['sources'][]   =   $newSourceCombineArray ;
                }
                $finalProductData[]=$productData;
                array_walk_recursive($finalProductData, 'Helpers::update_price');
                session(['productDetails' => $finalProductData]);
                // Get current page form url e.x. &page=1
                $currentPage = LengthAwarePaginator::resolveCurrentPage();
                // Create a new Laravel collection from the array data
                $itemCollection = collect($finalProductData);
                // Define how many items we want to be visible in each page
                $perPage = 500;
                // Slice the collection to get the items to display in current page
                $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
                // Create our paginator and pass it to the view
                $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
                // set url path for generted links
                $paginatedItems->setPath($request->url());
                $data['finalData'] =  $paginatedItems;
                /* Set in session */
                $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));
                if(Session::has('searchProducts')) {
                    $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));
                    if(in_array($request->input('partNumber'), array_column(Session::get('searchProducts') , 'product_name'))) { // search value in the array
                        foreach (Session::get('searchProducts') as $keyp => $products) {
                            if ($products['product_name'] == $request->input('partNumber')) {
                                $event_data_display = Session::get('searchProducts');
                                unset($event_data_display[$keyp]);
                                Session(['searchProducts' => $event_data_display]);
                            }
                        }
                    }
                    $completeResult = array_merge_recursive($searchProductArray,Session::get('searchProducts'));
                    Session(['searchProducts' => $completeResult]);
                    //
                } else {
                    Session(['searchProducts' => $searchProductArray]);
                }
//                return $data;
                return view('frontend.products.listing',$data);
            }else{
                session(['previousroute' => 'search?partNumber='.urlencode($partNumber)]);
                return view('frontend.products.no-product-found');
            }
        }
        
}
/*array_walk_recursive($finalProductData, 'Helpers::update_price');
                session(['productDetails' => $finalProductData]);
                // Get current page form url e.x. &page=1
                $currentPage = LengthAwarePaginator::resolveCurrentPage();
                // Create a new Laravel collection from the array data
                $itemCollection = collect($finalProductData);
                // Define how many items we want to be visible in each page
                $perPage = 500;
                // Slice the collection to get the items to display in current page
                $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
                // Create our paginator and pass it to the view
                $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
                // set url path for generted links
                $paginatedItems->setPath($request->url());
                $data['finalData'] =  $paginatedItems;

                $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));
                if(Session::has('searchProducts')) {
                    $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));
                    if(in_array($request->input('partNumber'), array_column(Session::get('searchProducts') , 'product_name'))) { // search value in the array
                        foreach (Session::get('searchProducts') as $keyp => $products) {
                            if ($products['product_name'] == $request->input('partNumber')) {
                                $event_data_display = Session::get('searchProducts');
                                unset($event_data_display[$keyp]);
                                Session(['searchProducts' => $event_data_display]);
                            }
                        }
                    }
                    $completeResult = array_merge_recursive($searchProductArray,Session::get('searchProducts'));
                    Session(['searchProducts' => $completeResult]);
                    //
                } else {
                    Session(['searchProducts' => $searchProductArray]);
                }*/