<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Cart;
use Event;
use Session;
use App\Http\Controllers\Controller;
use Helpers; // Important
use App\PartDetail;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Redirect;
use App\Category;

class ProductController extends Controller{
    
    protected $userSessionData;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct(){
    }
    
    /*
    * Show the product list page.
    *
    * @return \Illuminate\Http\Response
*/
    public function productListing(){
        return view('frontend.products.listing');
    }
    
    /*
     * suggestPartDetails
     */
    public function suggestPartDetails(Request $request){
        $searchParam = $request->phrase;
            if(!empty($searchParam)){
            $data['categories'] = Category::getCategoryListByKeyword($searchParam);
            echo json_encode($data['categories']);
            die();
        }
    }
    
    /* Clear Session Function */
    function clearSearchHistory() {
        Session::forget('searchProducts');
        echo 1;
    }
    
    /* FUNCTION TO GET DATA FROM ONLY ORBWEAVER */
    public function searchProduct(Request $request, $partNum = null){
        $data = $request->all();
        if($partNum != null){
            $partNumber = $partNum;
        }else{
            $partNumber     = $request->input('partNumber');
        }
        $partArray      = array (array('partNum'=>urlencode($partNumber)));
        $finalArray     =  json_encode($partArray);
        $partNumberLength = strlen($partNumber);

        $slugHolder = '';
        $dt = [];
        if(!empty($slugHolder)){
            Session(['search_part_name' => $partNumber]);
            return redirect('/category/'.$slugHolder);
        }
        /*CALL ORBEWER API START*/
        $finalProductData = Helpers::searchProductOrbweaver($partNumber, $slugHolder, $partNum = null);
        if(empty($finalProductData)){
            session(['previousroute' => 'search?partNumber='.urlencode($partNumber)]);
            return view('frontend.products.no-product-found');
        }
        
        // using for token search end point
        array_walk_recursive($finalProductData, 'Helpers::update_price');
        session(['productDetails' => $finalProductData]);
        // Get current page form url e.x. &page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        // Create a new Laravel collection from the array data
        $itemCollection = collect($finalProductData);
        // Define how many items we want to be visible in each page
        $perPage = 500;
        // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        // Create our paginator and pass it to the view
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());
        $data['finalData'] =  $paginatedItems;
        /* Set in session */
        $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));

        if(Session::has('searchProducts')) {
            $searchProductArray = array(array('product_name'=>$request->input('partNumber'), 'total_counter'=>$itemCollection->count()));
            if(in_array($request->input('partNumber'), array_column(Session::get('searchProducts') , 'product_name'))) { // search value in the array
                foreach (Session::get('searchProducts') as $keyp => $products) {
                    if ($products['product_name'] == $request->input('partNumber')) {
                        $event_data_display = Session::get('searchProducts');
                        unset($event_data_display[$keyp]);
                        Session(['searchProducts' => $event_data_display]);
                    }
                }
            }
            $completeResult = array_merge_recursive($searchProductArray,Session::get('searchProducts'));
            Session(['searchProducts' => $completeResult]);
        } else {
            Session(['searchProducts' => $searchProductArray]);
        }
        return view('frontend.products.listing',$data);
    }
}