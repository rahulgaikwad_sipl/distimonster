<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use Cart;
use Event;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Authenticated;
use DB;
use Illuminate\Support\Facades\Auth;
use Helpers; // Important
use Illuminate\Support\Facades\Redirect;
use App\Country;
use App\State;
use App\City;
use App\Subscribe;
use App\SubscriptionPlans;
use Hash;
use App\Mail\SubscribeSendWelComeMail;

class SubscribeController extends Controller{
    protected $userSessionData;
    /*
        * Create a new controller instance.
        * @return void
     */
        public function __construct(){
        }
        
    /*
        * subscriptionPlan.
        * @return void
     */        
    public function subscriptionPlan(){
        $data['subscription_plans'] = SubscriptionPlans::Select('*')->Where('is_active', 1)->get();
        return view('frontend.subscribe.subscription-plan', $data);
    }//End Function

    /*
        * Subscribe.
        * @return void
     */
        public function subscribe($planType){
            $data['countries'] = Country::getAllCountry();
            $data['subscription_plans'] = SubscriptionPlans::Select('*')->Where('is_active', 1)->get();
            $data['planType'] = $planType;
            return view('frontend.subscribe.subscribe', $data);
        }//End Function
    
    /*
        * randString.
        * @return void
    */
        public function randString($length) {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789$#@!";
            return substr(str_shuffle($chars),0,$length);
        }

    /*
        * subscribeRegister.
        * @return void
     */        
        public function subscribeRegister(Request $request){ 
            
            if(Auth::user()){
                return redirect()->intended('/')->with('warning',  config('constants.ALREADY_LOGGEDIN'));
            }
            
            DB::beginTransaction();
            try { 
                $data = $request->all();
                $plan_type = base64_decode($data['plan_type']);
                $monthly_price = base64_decode($data['monthly_price']);
                $yearly_price = base64_decode($data['yearly_price']);
                $subscription_plans = SubscriptionPlans::Select('*')->Where('type', $plan_type)->Where('monthly_price', $monthly_price)->Where('yearly_price', $yearly_price)->Where('is_active', 1)->first();
                if(empty($subscription_plans)){
                    return response(['status' => false, 'message' => config('constants.COMMON_ERROR')]);
                }
                $validation = \Validator::make($data, Subscribe::$subscribeUserValidationRules, Subscribe::$subscribeUserValidationMessages);
                if ($validation->fails()){ 
                    return response(['status' => false, 'message' => config('constants.COMMON_ERROR')]);
                }else{
                    unset($data['plan_type']);
                    unset($data['monthly_price']);
                    unset($data['yearly_price']);
                    unset($data['user_unit_price']);
                    unset($data['free_users']); 
                    //USERS TABLE
                    $companyName = $request->company_name;
                    $firstName = $request->name;
                    $lastName = $request->last_name;
                    $employee = $request->employee;
                    $titleOrJob = $request->title_or_job;
                    $department = $request->department;
                    $aboutDistimonster = $request->know_about_distimonster;
                    $companyType = $request->company_type;
                    $companyTypeOther = $request->company_type_other;
                    $phoneNumber = $request->contact_number;
                    $emailAddress = $request->email;
                    $address = $request->billing_street_address;
                    $zipCode = $request->zip_code;
                    $billingState = $request->billing_state;
                    $billingCountry = $request->billing_country;
                    $billingCity = $request->billing_city;
                    $annualComponentPurchase = $request->annual_component_purchase;
                    $a_c_contact_name = $request->contact_name;
                    $a_c_contact_email = $request->contact_email;
                    $payment_type = $request->payment_type;
                    $plan_id = $subscription_plans->plan_id;
                    $billing_cycle = $request->billing_cycle; 
                    $txtCount = 0;
                    if(!empty($request->user_first_name)){
                        $txtCount = count($request->user_first_name);
                    }
                    
                    $planPurchaseDate = date("Y-m-d");
                    $freeTrialExpiry = date('Y-m-d', strtotime($planPurchaseDate . '+30 days'));
                    $randomPassword = $this->randString(8);
                    $password = Hash::make($randomPassword);
                    $haveSubUser = 0;
                    if($txtCount>0){
                        $haveSubUser = 1;
                    }

                    if($txtCount>0){
                        $userTotalAmount = ($subscription_plans->user_unit_price * $txtCount) * 3;
                    }else{
                        $userTotalAmount = 0;
                    }

                    if($subscription_plans->type == 'Pro')
                        $userTotalAmount = 0;

                    if($billing_cycle == 'quarterly'){
                        $planExpiryDate= date('Y-m-d', strtotime($planPurchaseDate . '+90 days'));
                        $grandTotalAmount = $userTotalAmount + ($subscription_plans->monthly_price * 3);
                    }else if($billing_cycle == 'yearly'){
                        $planExpiryDate= date('Y-m-d', strtotime($planPurchaseDate . '+365 days'));
                        $grandTotalAmount = $userTotalAmount + $subscription_plans->yearly_price;
                    }

                    $userData = array("name" => $firstName, "last_name" => $lastName, "contact_number" => $phoneNumber, "email" => $emailAddress, "password" => $password, "role_id" => 2, "status" => 0, "billing_address_line1" => $address, "country_id" => $billingCountry, "state_id" => $billingState, "city" => $billingCity, "zip_code" => $zipCode, "company_name" => $companyName, "employee" => $employee, "annual_component_purchase" => $annualComponentPurchase, "company_type" => $companyType, "company_type_other" => $companyTypeOther, "know_about_distimonster" => $aboutDistimonster, "department" => $department, "title_or_job" => $titleOrJob, "have_sub_user" => $haveSubUser, "plan_purchase_date" => $planPurchaseDate, "plan_expiry_date" => $planExpiryDate, "free_trial_expiry" => $freeTrialExpiry, "is_sub_user" => 0, "parent_user_id" => 0, "plan_id" => $plan_id, "a_c_contact_name" => $a_c_contact_name, "a_c_contact_email" => $a_c_contact_email, "preferred_payment_type" => $payment_type);
                    
                    $newUser = Subscribe::registerNewUser($userData);
                    DB::commit();
                    //Subscribe Mail Data                    
                    $subscribeUser = array();
                    $subscribeUser['firstName'] = $firstName;
                    $subscribeUser['emailAddress'] = $emailAddress;
                    $subscribeUser['randomPassword'] = $randomPassword;
                    $subscribeUser['isSubUser'] = 0;
                    
                    if(!empty($newUser['user'])){ 
                        
                        //BILLING INFORMATION
                        //subscription_billing_info
                        $billingCompanyName = $request->billing_company_name;
                        $billingStreetAddress = $request->billing_street_address;
                        $billingPhone = $request->billing_phone;
                        $zipCode = $request->zip_code;
                        $billingCity = $request->billing_city;
                        $billingState = $request->billing_state;
                        $billingCountry = $request->billing_country;
                        $billingContactEmail = $request->billing_contact_email;
                        $billingCycle = $request->billing_cycle;
                        
                        //PayPal Details
                        if($payment_type == "credit"){
                            $firstNameOnCard = $request->first_name_on_card;
                            $lastNameOnCard = $request->last_name_on_card;
                            $cardNumber = (int)str_replace(' ', '',$request->card_number);
                            $expiryMonth = $request->expiry_month;
                            $expiryYear = $request->expiry_year;
                            $cvv = $request->cvv;
                            $cardType = strtolower($request->card_type);
                            $accessToken = $this->getPaypalAuthToken();
                            if(empty($accessToken)){
                                DB::rollBack();
                                return response(['status' => false, 'message' => 'Unable to make payment.']);
                            }
                        }else if($payment_type == "invoice_me"){
                            //Billing Info Data Start
                            $billingUserData = array("company_name" => $billingCompanyName, "user_id" => $newUser['user'],  "billing_first_name" => $firstName, "billing_last_name" => $lastName, "billing_address" => $billingStreetAddress, "billing_phone" => $billingPhone, "billing_zipcode" => $zipCode, "billing_city" => $billingCity, "billing_state" => $billingState, "billing_country" => $billingCountry, "billing_email" => $billingContactEmail, "user_total_amount" => $userTotalAmount, "grand_total_amount" => $grandTotalAmount, "billing_cycle" => $billingCycle, "paypal_card_id" => '####');
                            //Billing Info Data End
                            $billingUser = Subscribe::newUserBillingStore($billingUserData);
                            DB::commit();
                            if(!empty($billingUser['subUser'])){
                                //Send Email to Subscribe User
                                \Mail::to($emailAddress)->send(new SubscribeSendWelComeMail($subscribeUser,config('constants.WELCOME_USER_SUBJECT')));
                                DB::table('users')->where("id", $newUser['user'])->update(['status' => 1]);
                                DB::commit();
                            }    
                        }

                        $countryName = \Helpers::getCountryStateCityName('countries', 'sortname', 'id', $billingCountry);
                        $stateName = \Helpers::getCountryStateCityName('states', 'name', 'id', $billingState);
                        $cityName = \Helpers::getCountryStateCityName('cities', 'name', 'id', $billingCity);
                        if(!empty($accessToken)){
                            $paypalArray = array ('number' => $cardNumber, 'type' => $cardType, 'expire_month' => $expiryMonth, 'expire_year' => $expiryYear, 'first_name' => $firstNameOnCard, 'last_name' => $lastNameOnCard, 'billing_address' =>  array ('line1' => $billingStreetAddress, 'city' => $cityName, 'country_code' => $countryName, 'postal_code' => $zipCode, 'state' => $stateName, 'phone' => $billingPhone));
                            
                            $paypalArrayJson = json_encode($paypalArray);
                            $paypalResponse = $this->subscribeCardSavingOnPaypal($paypalArrayJson, $accessToken);
                            if(!empty($paypalResponse)){ 
                                $responsePaypal = json_decode($paypalResponse);
                                if(!empty($paypalResponse) && !empty($responsePaypal->id)){
                                    //Billing Info Data Start
                                    $billingUserData = array("company_name" => $billingCompanyName, "user_id" => $newUser['user'],  "billing_first_name" => $firstNameOnCard, "billing_last_name" => $lastNameOnCard, "billing_address" => $billingStreetAddress, "billing_phone" => $billingPhone, "billing_zipcode" => $zipCode, "billing_city" => $billingCity, "billing_state" => $billingState, "billing_country" => $billingCountry, "billing_email" => $billingContactEmail, "user_total_amount" => $userTotalAmount, "grand_total_amount" => $grandTotalAmount, "paypal_card_id" => $responsePaypal->id);
                                    //Billing Info Data End
                                    $billingUser = Subscribe::newUserBillingStore($billingUserData);
                                    DB::commit();
                                    if(!empty($billingUser['subUser'])){
                                        //Send Email to Subscribe User
                                        \Mail::to($emailAddress)->send(new SubscribeSendWelComeMail($subscribeUser,config('constants.WELCOME_USER_SUBJECT')));
                                        DB::table('users')->where("id", $newUser['user'])->update(['status' => 1]);
                                        DB::commit();
                                    }                                
                                }else{
                                    /*Redirect back user to subscribe form with input and error.*/
                                    DB::rollBack();
                                    return response(['status' => false, 'message' => 'Your card is invalid, please enter valid card details and try again.']);
                                    
                                }
                            }   
                        }
                        
                        /*If Sub User Added then below code executed*/
                        if($txtCount>0){
                            $parentId = $newUser['user'];
                            for($i=0;$i<$txtCount;$i++){
                                //Get post sub user data
                                $subFirstName = $request->user_first_name[$i];
                                $subLastName = $request->user_last_name[$i];
                                $subEmail = $request->user_email[$i];
                                $subNote = $request->user_note[$i];
                                
                                //Generate random password and hash it
                                $subRandomPassword = $this->randString(8);
                                $subPassword = Hash::make($subRandomPassword);
                                
                                //Sub user array for creating new record
                                $subUserData = array("name" => $subFirstName, "last_name" => $subLastName, "note" => $subNote, "email" => $subEmail, "password" => $subPassword, "role_id" => 2, "status" => 1, "have_sub_user" => 0, "is_sub_user" => 1, "plan_purchase_date" => $planPurchaseDate, "plan_expiry_date" => $planExpiryDate, "free_trial_expiry" => $freeTrialExpiry, "parent_user_id" => $parentId, "plan_id" => $plan_id, "preferred_payment_type" => $payment_type);
                                
                                //Subscribe Mail Data                    
                                $subUser = array();
                                $subUser['firstName'] = $subFirstName;
                                $subUser['emailAddress'] = $subEmail;
                                $subUser['randomPassword'] = $subRandomPassword;
                                $subUser['note'] = $subNote;
                                $subUser['isSubUser'] = 1;

                                //Call model function to store Sub user information
                                $newUser = Subscribe::subUserRegister($subUserData);
                                DB::commit();
                                //Sub user generated ID
                                $subUserId = $newUser['subUser'];
                                
                                if(!empty($subUserId)){
                                    //Send Email to Sub User
                                    \Mail::to($subEmail)->send(new SubscribeSendWelComeMail($subUser,config('constants.WELCOME_USER_SUBJECT')));
                                    DB::table('users')->where("id", $subUserId)->update(['status' => 1]);
                                    DB::commit();
                                }
                            }
                        }
                        /*If Sub User Added then above code executed*/
                    }else{
                        DB::rollBack();
                        $errorMsg = $ex->getMessage() . " on line number: " . $ex->getLine();
                        return response(['status' => false, 'message' => $errorMsg]);
                    }
                    Session::put('mybio_added', 0);
                    return response(['status' => true, 'message' => config('constants.SUCCESSFULLY_REGISTER')]);
                }
            } catch (Exception $ex) {
                DB::rollBack();
                return redirect()->back()->with('failure', $ex->getMessage() . " on line number: " . $ex->getLine())->withInput();
            }
        }//End Function
        
        public function getPaypalAuthToken(){
            
            $ch = curl_init();
            
            $clientId = config('constants.SUBSCRIPTION_CLIENT_ID');
            $secret = config('constants.SUBSCRIPTION_CLIENT_SECRET');
            $authUrl = config('constants.SUBSCRIPTION_AUTH_URL');
            
            curl_setopt($ch, CURLOPT_URL, $authUrl);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

            $result = curl_exec($ch);
            
            if(empty($result)){
                return false;
            }else{
                $json = json_decode($result);
                return $json->access_token;
            }
            curl_close($ch);
        }
        
        public function subscribeCardSavingOnPaypal($data, $authToken){
            
            $vaultUrl = config('constants.SUBSCRIPTION_VAULT_URL');
            
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $vaultUrl);
            curl_setopt($ch1, CURLOPT_HEADER, false);
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch1, CURLOPT_POST, true);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($ch1, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Authorization: Bearer ".$authToken));
            curl_setopt($ch1, CURLOPT_POSTFIELDS, $data);

            $result1 = curl_exec($ch1);

            if(empty($result1)){
                return false;
            }else{
                return $result1;
            }
            curl_close($ch1);
        }//End Function
        
        public function stateListByCountryName(Request $request){
            $data         = $request->all();
            $countryId    = $request->country_id;
            $state       =  State::getStateByCountryId($countryId);
            return response(['success' => true, 'data' => $state, 'message' => 'State list successfully' ]);
        }//End Function
        
        public function cityListByStateName(Request $request){
            try{
                $data           = $request->all();
                $cityId         = $request->cityId;
                $cities         = City::getCityByStateId($cityId);
                return response(['success' => true, 'data' => $cities, 'message' => 'City list successfully' ]);
            }catch (\Exception $ex) {
                return response(['status' => false, 'data' => config('constants.COMMON_ERROR')]);
            }
        }//End Function
    }//End Class
