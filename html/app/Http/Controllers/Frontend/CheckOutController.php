<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use Event;
use Session;
use Cart;
use Auth;
use DB;
use App\Order;
use App\User;
use App\OrderDetail;
use App\Coupon;
use Helpers;
use App\Country;
use App\ShippingAddress;
use App\Http\Controllers\Controller;
use App\TransactionsDetail;
use App\QuoteDetail;
use App\Quote;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Redirect;


class CheckOutController extends Controller
{
    protected $userSessionData;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
       Event::listen(Authenticated::class, function ($event) {
            $this->userSessionData = $event->user;
            if ($this->userSessionData['status'] == 0) {
                Helpers::sessionFlush();
            } 
        });
    }

    public function placeOrder(Request $request){
        try {
            $data = $request->all();
            $shippingAddressId                  = $request->shipping_address;
            $shippingMethodId                   = $request->shipping_method;
            $shippingCharges                    = $request->shipping_charges;
            $paymentMethod                      = $request->payment_method;
            $shippingMethodType                 = $request->input('shipping_method_type');
            $accountNumber                      = $request->input('account_number');
            $shippingNotes                      = $request->input('shipping_notes');
            if($request->input('requested_delivery_date') == ''){
                $requestedDeliveryDate              = NUll;
            }else{
                $requestedDeliveryDate              = date("Y-m-d", strtotime($request->input('requested_delivery_date')));
            }

            if($request->coupon_code_id != ''){
                $couponCodeId            =   $request->input('coupon_code_id');
                $orderAppliedDiscount    =   $request->input('order_applied_discount');
            }else{
                $couponCodeId            = 0;
                $orderAppliedDiscount    = 0;
            }

            $shippingAddressObj     =    ShippingAddress::getShippingAddressById($request->shipping_address);
            if(!empty($shippingAddressObj[0]->address_line2)){
                $shippingAddressLine2 =  $shippingAddressObj[0]->address_line2;
                $shippingAddress  =  $shippingAddressObj[0]->first_name." ".$shippingAddressObj[0]->last_name.", ".$shippingAddressObj[0]->address_line1.", ".$shippingAddressLine2.", ".$shippingAddressObj[0]->city_name.", ".$shippingAddressObj[0]->state_name.", ".$shippingAddressObj[0]->country_name.", ".$shippingAddressObj[0]->zip_code.", ".$shippingAddressObj[0]->contact_number;
            }else{
                $shippingAddress  =  $shippingAddressObj[0]->first_name." ".$shippingAddressObj[0]->last_name.", ".$shippingAddressObj[0]->address_line1.", ".$shippingAddressObj[0]->city_name.", ".$shippingAddressObj[0]->state_name.", ".$shippingAddressObj[0]->country_name.", ".$shippingAddressObj[0]->zip_code.", ".$shippingAddressObj[0]->contact_number;

            }
            $billingAddressObj =    ShippingAddress::getBillingAddressByUserId(Auth::user()->id);
            if(!empty($billingAddressObj[0]->billing_address_line2)) {
                $billingAddressLine2 = $billingAddressObj[0]->billing_address_line2;
                $billingAddress = $billingAddressObj[0]->name." " .$billingAddressObj[0]->last_name.", ".$billingAddressObj[0]->billing_address_line1.", ".$billingAddressLine2.", ".$billingAddressObj[0]->city_name.", ".$billingAddressObj[0]->state_name.", ".$billingAddressObj[0]->country_name.", ".$billingAddressObj[0]->zip_code;
            }else{
                $billingAddress = $billingAddressObj[0]->name." ".$billingAddressObj[0]->last_name.", ".$billingAddressObj[0]->billing_address_line1.", ".$billingAddressObj[0]->city_name.", ".$billingAddressObj[0]->state_name.", ".$billingAddressObj[0]->country_name. ", ".$billingAddressObj[0]->zip_code;
            }

            if($request->billing_address == 0){
                $billingAddressId                   =  0; //Billing address of user table
                $selectedBillingAddress =  $billingAddress;
            }else{
                $billingAddressId                   =  1; // Billing address same as shipping address  of the order
                $selectedBillingAddress    =  $shippingAddress;
            }
            //Get Address for Card
            $cardStreetAddress  =  '';
            $cardCityName       =  '';
            $cardStateName      =  '';
            $cardCountryCode    =  '';
            $cardZipCode        =  '';

            if($request->billing_address == 0){
                //Address from user profile address
                $countryCodeObj    =    ShippingAddress::getCountryCodeByName($billingAddressObj[0]->country_name);

                if(!empty($billingAddressObj[0]->billing_address_line2)) {
                    $cardStreetAddress  =  $billingAddressObj[0]->billing_address_line1.",".$billingAddressObj[0]->billing_address_line2;
                    $cardCityName       =  $billingAddressObj[0]->city_name;
                    $cardStateName      =  $billingAddressObj[0]->state_name;
                    $cardCountryCode    =  $countryCodeObj[0]->phonecode;
                    $cardZipCode        =  $billingAddressObj[0]->zip_code;
                }else{
                    $cardStreetAddress  =  $billingAddressObj[0]->billing_address_line1;
                    $cardCityName       =  $billingAddressObj[0]->city_name;
                    $cardStateName      =  $billingAddressObj[0]->state_name;
                    $cardCountryCode    =  $countryCodeObj[0]->phonecode;
                    $cardZipCode        =  $billingAddressObj[0]->zip_code;
                }
            }else{
                // Billing address same as shipping address  of the order
                $countryCodeObj    =    ShippingAddress::getCountryCodeByName($shippingAddressObj[0]->country_name);
                if(!empty($shippingAddressObj[0]->address_line2)){
                    $cardStreetAddress  =  $shippingAddressObj[0]->address_line1.",".$shippingAddressObj[0]->address_line2;
                    $cardCityName       =  $shippingAddressObj[0]->city_name;
                    $cardStateName      =  $shippingAddressObj[0]->state_name;
                    $cardCountryCode    =  $countryCodeObj[0]->phonecode;
                    $cardZipCode        =   $shippingAddressObj[0]->zip_code;
                }else{
                    $cardStreetAddress  =  $shippingAddressObj[0]->address_line1;
                    $cardCityName       =  $shippingAddressObj[0]->city_name;
                    $cardStateName      =  $shippingAddressObj[0]->state_name;
                    $cardCountryCode    =  $countryCodeObj[0]->phonecode;
                    $cardZipCode        =  $shippingAddressObj[0]->zip_code;
                }
            }
            $itemCount = Cart::instance('shoppingcart')->content()->count();
            if($itemCount > 0){
                $response = DB::transaction(function () use ($request,$shippingAddressId,$shippingMethodId,$shippingCharges,$paymentMethod,$shippingMethodType,$accountNumber,$shippingNotes,$requestedDeliveryDate,$couponCodeId,$orderAppliedDiscount,$billingAddressId,$selectedBillingAddress,$shippingAddress,$cardStreetAddress ,$cardCityName,$cardStateName,$cardCountryCode ,$cardZipCode) {
                
                $paymentNotes                       = $request->payment_notes;
                $consolidatedShippingCharge         = Session::get('consolidatedShippingCharge');
                $shippingPurchaseOrderNumber        = Session::get('shippingPurchaseOrderNumber');
                if(empty($consolidatedShippingCharge))
                    $consolidatedShippingCharge = 'no';
                $consolidatedShippingCharges        = 0;
                if($consolidatedShippingCharge == 'yes'){
                    $consolidatedShippingCharges = config('constants.CONSOLIDATED_SHIPPING_CHARGE');
                }
                    if($paymentMethod == config('constants.PAYMENT_METHOD.cash_on_delivery')){
                        //check cash on delivery payment method and insert cart item in db
                        $cartSubTotal                           = Cart::instance('shoppingcart')->subtotal(2,'.','') ;
                        $order                                  = new Order();
                        $userId                                 = Auth::user()->id;
                        $order->users_id                        = $userId;
                        $order->order_date                      = date('Y-m-d h:i:s');
                        $order->order_sub_total                 = $cartSubTotal;
                        $order->order_tax_amount                = 0;
                        $order->order_discount_amount           = $orderAppliedDiscount;
                        $order->coupon_code_id                  = $couponCodeId;
                        $order->order_shipping_charges_amount	= $shippingCharges;
                        $order->payment_method_id               = $paymentMethod;
                        $order->shipping_address_id             = $shippingAddressId;
                        $order->billing_address                 = $billingAddressId;
                        $order->address_billing                 = $selectedBillingAddress;
                        $order->address_shipping                = $shippingAddress;
                        $order->shipping_method_id              = $shippingMethodId;
                        $order->order_shipping_status           = 2;
                        $order->payment_status                  = config('constants.APP_CONSTANT.PAYMENT_STATUS.pending');
                        $orderTotalAmount                       = $consolidatedShippingCharges + $cartSubTotal+$shippingCharges + $order->order_tax_amount - $order->order_discount_amount;
                        $order->order_total_amount              = $orderTotalAmount;
                        $order->shipping_method_type            = $shippingMethodType;
                        $order->shipping_account_number         = $accountNumber;
                        $order->shipping_notes                  = $shippingNotes;
                        $order->requested_delivery_date         = $requestedDeliveryDate;
                        $order->consolidated_shipping_charge  = $consolidatedShippingCharge;
                        $order->payment_notes  = $paymentNotes;
                        $order->shipping_purchase_order_number  = $shippingPurchaseOrderNumber;
                        if ($order->save()) {
                            $orderedQuoteItemCount = 1;
                            $quoteItemCount= 1;
                            foreach(Cart::instance('shoppingcart')->content() as $row){ 
                                $orderDetails =  new OrderDetail();
                                $orderDetails->order_id             = $order->id;
                                $orderDetails->quantity             = $row->qty;
                                $orderDetails->distributor_name     = $row->options->distributorName;
                                $orderDetails->item_name            = $row->name;
                                $orderDetails->price                = $row->price;
                                $orderDetails->manufacturer_name    = $row->options->manufacturer;
                                $orderDetails->manufacturer_code    = $row->options->manufacturerCode;
                                $orderDetails->source_part_id       = $row->options->sourcePartId;
                                $orderDetails->date_code            = $row->options->dateCode;
                                $orderDetails->customer_part_id     = $row->options->customerPartNo;
                                if($row->options->quoteItemId !=0){
                                    $orderDetails->is_quote_item            = 1;
                                    $orderDetails->quote_id                 = $row->options->quoteId;
                                }
                                if(!empty($row->options->bomId))
                                    $orderDetails->ordered_bom_id     = $row->options->bomId;
                                $orderDetails->save();
                                if($row->options->quoteItemId !=0){
                                    $quoteDetail                                = QuoteDetail::findOrFail($row->options->quoteItemId);
                                    $quoteDetail->is_ordered                    = 1;
                                    $quoteDetail->save();
                                    $quoteItemCount                 =  QuoteDetail::where('quote_id',$row->options->quoteId)->count();
                                    $orderedQuoteItemCount          =  QuoteDetail::where('quote_id',$row->options->quoteId)->where('is_ordered',1)->count();
                                    if($quoteItemCount >0 && $quoteItemCount ==$orderedQuoteItemCount ){
                                        $quote                      = Quote::findOrFail($row->options->quoteId);
                                        $quote->is_approved         =  4;
                                        $quote->save();
                                    }
                                }

                            }
                            $orderId=  $order->id;
                            $transactionId                  =  User::generateRandomString();
                            $transaction                     =  new TransactionsDetail();
                            $transaction->transaction_id     =  $transactionId;
                            $transaction->transaction_by     =  $paymentMethod;
                            $transaction->order_id           =  $orderId;
                            if ($transaction->save()) {
                                $order                                 = Order::findOrFail($orderId);
                                $order->payment_status                 = 0;
                                $order->order_status                   = 1;
                                $order->tranaction_reference_id        = $transactionId;
                                if ($order->save()) {
                                    session(['selectedAddressId'=> '']);
                                    session(['selectedShippingMethodId' => '']);
                                    session(['shippingMethodTypeInput'=>'']);
                                    session(['accountNumber'=>'']);
                                    session(['shippingNotes'=>'']);
                                    session(['consolidatedShippingCharge'=>'']);
                                    session(['shippingPurchaseOrderNumber'                    => '']);
                                    Cart::instance('shoppingcart')->destroy();
                                    Session::put('ordersuccess', 1);
                                    \Helpers::sendOrderSuccessMail($orderId);
                                    return response(['success' => true, 'txNumber'=>$transactionId,'orderId'=>$orderId,'message' => config('constants.ORDER_SUCCESS'),'quoteItemCount'=>$quoteItemCount,'orderedQuoteItemCount'=>$orderedQuoteItemCount]);
                                }
                            }
                        } else {
                            return response(['success' => false, 'message' => config('constants.CASH_ON_DELIVERY')]);
                        }
                    }else if($paymentMethod == config('constants.PAYMENT_METHOD.paypal')){
                        //check paypal payment method and insert cart item in db
                        $cartSubTotal                           = Cart::instance('shoppingcart')->subtotal(2,'.','') ;
                        $order                                  = new Order();
                        $userId                                 = Auth::user()->id;
                        $order->users_id                        = $userId;
                        $order->order_date                      = date('Y-m-d h:i:s');
                        $order->order_sub_total                 = $cartSubTotal;
                        $order->order_tax_amount                = 0;
                        $order->order_discount_amount           = $orderAppliedDiscount;
                        $order->coupon_code_id                  = $couponCodeId;
                        $order->order_shipping_charges_amount	= $shippingCharges;
                        $order->payment_method_id               = $paymentMethod;
                        $order->shipping_address_id             = $shippingAddressId;
                        $order->billing_address                 = $billingAddressId;
                        $order->address_billing                 = $selectedBillingAddress;
                        $order->address_shipping                = $shippingAddress;
                        $order->shipping_method_id              = $shippingMethodId;
                        $order->order_shipping_status           = 2;
                        $order->payment_status                  = config('constants.APP_CONSTANT.PAYMENT_STATUS.pending');
                        $orderTotalAmount                       = $consolidatedShippingCharges + $cartSubTotal+$shippingCharges + $order->order_tax_amount - $order->order_discount_amount;
                        $order->order_total_amount              = $orderTotalAmount;
                        $order->shipping_method_type            = $shippingMethodType;
                        $order->shipping_account_number         = $accountNumber;
                        $order->shipping_notes                  = $shippingNotes;
                        $order->requested_delivery_date         = $requestedDeliveryDate;
                        $order->consolidated_shipping_charge  = $consolidatedShippingCharge;
                        $order->payment_notes  = $paymentNotes;
                        $order->shipping_purchase_order_number  = $shippingPurchaseOrderNumber;
                        if ($order->save()) {
                            foreach(Cart::instance('shoppingcart')->content() as $row){
                                $orderDetails =  new OrderDetail();
                                $orderDetails->order_id             = $order->id;
                                $orderDetails->quantity             = $row->qty;
                                $orderDetails->distributor_name     = $row->options->distributorName;
                                $orderDetails->item_name            = $row->name;
                                $orderDetails->price                = $row->price;
                                $orderDetails->manufacturer_name    = $row->options->manufacturer;
                                $orderDetails->manufacturer_code    = $row->options->manufacturerCode;
                                $orderDetails->source_part_id       = $row->options->sourcePartId;
                                $orderDetails->date_code            = $row->options->dateCode;
                                $orderDetails->customer_part_id     = $row->options->customerPartNo;
                                if($row->options->quoteItemId !=0){
                                    $orderDetails->is_quote_item            = 1;
                                    $orderDetails->quote_id            = $row->options->quoteId;
                                }
                                $orderDetails->save();
                            }
                            $orderId =  $order->id;
                            session(['consolidatedShippingCharge'=>'']);
                            session(['shippingPurchaseOrderNumber'                    => '']);
                            //redirect to paypal after successful insertion
                            return redirect()->route('paypal', ['orderId' => $orderId,'orderTotalAmount'=>$orderTotalAmount]);
                        }else{
                            return redirect('shipping')->with('error',config('constants.PAYPAL_ORDER_ERROR'));
                        }
                    }else if($paymentMethod == config('constants.PAYMENT_METHOD.net_term_account')){
                        //check Net Term  payment method and insert cart item in db
                        $cartSubTotal                           = Cart::instance('shoppingcart')->subtotal(2,'.','') ;
                        $order                                  = new Order();
                        $userId                                 = Auth::user()->id;
                        $order->users_id                        = $userId;
                        $order->order_date                      = date('Y-m-d h:i:s');
                        $order->order_sub_total                 = $cartSubTotal;
                        $order->order_tax_amount                = 0;
                        $order->order_discount_amount           = $orderAppliedDiscount;
                        $order->coupon_code_id                  = $couponCodeId;
                        $order->order_shipping_charges_amount	= $shippingCharges;
                        $order->payment_method_id               = $paymentMethod;
                        $order->shipping_address_id             = $shippingAddressId;
                        $order->billing_address                 = $billingAddressId;
                        $order->address_billing                 = $selectedBillingAddress;
                        $order->address_shipping                = $shippingAddress;
                        $order->shipping_method_id              = $shippingMethodId;
                        $order->order_shipping_status           = 2;
                        $order->payment_status                  =  config('constants.APP_CONSTANT.PAYMENT_STATUS.pending');
                        $orderTotalAmount                       = $consolidatedShippingCharges + $cartSubTotal+$shippingCharges + $order->order_tax_amount - $order->order_discount_amount;
                        $order->order_total_amount              = $orderTotalAmount;
                        $order->shipping_method_type            = $shippingMethodType;
                        $order->shipping_account_number         = $accountNumber;
                        $order->shipping_notes                  = $shippingNotes;
                        $order->requested_delivery_date                  = $requestedDeliveryDate;
                        $order->consolidated_shipping_charge  = $consolidatedShippingCharge;
                        $order->payment_notes  = $paymentNotes;
                        $order->shipping_purchase_order_number  = $shippingPurchaseOrderNumber;
                        if ($order->save()) {
                            foreach(Cart::instance('shoppingcart')->content() as $row){
                                $orderDetails =  new OrderDetail();
                                $orderDetails->order_id             = $order->id;
                                $orderDetails->quantity             = $row->qty;
                                $orderDetails->distributor_name     = $row->options->distributorName;
                                $orderDetails->item_name            = $row->name;
                                $orderDetails->price                = $row->price;
                                $orderDetails->manufacturer_name    = $row->options->manufacturer;
                                $orderDetails->manufacturer_code    = $row->options->manufacturerCode;
                                $orderDetails->source_part_id       = $row->options->sourcePartId;
                                $orderDetails->date_code            = $row->options->dateCode;
                                if($row->options->quoteItemId !=0){
                                    $orderDetails->is_quote_item            = 1;
                                    $orderDetails->quote_id                 = $row->options->quoteId;
                                }
                                if(!empty($row->options->bomId))
                                    $orderDetails->ordered_bom_id     = $row->options->bomId;
                                $orderDetails->customer_part_id     = $row->options->customerPartNo;
                                $orderDetails->save();
                                if($row->options->quoteItemId !=0){
                                    $quoteDetail                                = QuoteDetail::findOrFail($row->options->quoteItemId);
                                    $quoteDetail->is_ordered                    = 1;
                                    $quoteDetail->save();

                                    //Updating quote status to ordered if all the quote items are orderd
                                    $quoteItemCount                 =  QuoteDetail::where('quote_id',$row->options->quoteId)->count();
                                    $orderedQuoteItemCount          =  QuoteDetail::where('quote_id',$row->options->quoteId)->where('is_ordered',1)->count();
                                    if($quoteItemCount >0 && $quoteItemCount ==$orderedQuoteItemCount  ){
                                        $quote                      = Quote::findOrFail($row->options->quoteId);
                                        $quote->is_approved         =  4;
                                        $quote->save();
                                    }

                                }

                            }
                            $orderId=  $order->id;
                            $transactionId                  =  User::generateRandomString();
                            $transaction                     =  new TransactionsDetail();
                            $transaction->transaction_id     =  $transactionId;
                            $transaction->transaction_by     =  $paymentMethod;
                            $transaction->order_id           =  $orderId;
                            if ($transaction->save()) {
                                $order                                 = Order::findOrFail($orderId);
                                $order->payment_status                 = 0;
                                $order->order_status                   = 1;
                                $order->tranaction_reference_id        = $transactionId;
                                if ($order->save()) {
                                    session(['selectedAddressId' => '']);
                                    session(['selectedShippingMethodId' => '']);
                                    session(['shippingMethodTypeInput'          => '']);
                                    session(['accountNumber'                    => '']);
                                    session(['shippingNotes'                    => '']);
                                    session(['consolidatedShippingCharge'       => '']);
                                    session(['shippingPurchaseOrderNumber'                    => '']);
                                    Cart::instance('shoppingcart')->destroy();
                                    Session::put('ordersuccess', 1);
                                    \Helpers::sendOrderSuccessMail($orderId);
                                    return response(['success' => true, 'txNumber'=>$transactionId,'orderId'=>$orderId,'message' => config('constants.ORDER_SUCCESS')]);
                                }
                            }
                        } else {
                            return response(['success' => false, 'message' => config('constants.NET_TERM_ORDER')]);
                        }
                    }else if($paymentMethod == config('constants.PAYMENT_METHOD.credit_card')){
                        //check credit card  payment method and insert cart item in db
                        $cardNumber     =    $request->card_number;
                        $expiryMonth    =    $request->expiry_month;
                        $cvv            =    $request->cvv;
                        $nameOnCard     =    $request->name_on_card;
                        $lastName       =    $request->last_name;
                        $cardType       =    $request->card_type;
                        $expiryYear     =    $request->expiry_year;
                        $cartSubTotal                           = Cart::instance('shoppingcart')->subtotal(2,'.','') ;
                        $order                                  = new Order();
                        $userId                                 = Auth::user()->id;
                        $order->users_id                        = $userId;
                        $order->order_date                      = date('Y-m-d h:i:s');
                        $order->order_sub_total                 = $cartSubTotal;
                        $order->order_tax_amount                = 0;
                        $order->order_discount_amount           = $orderAppliedDiscount;
                        $order->coupon_code_id                  = $couponCodeId;
                        $order->order_shipping_charges_amount	= $shippingCharges;
                        $order->payment_method_id               = $paymentMethod;
                        $order->shipping_address_id             = $shippingAddressId;
                        $order->billing_address                 = $billingAddressId;
                        $order->address_billing                 = $selectedBillingAddress;
                        $order->address_shipping                = $shippingAddress;
                        $order->shipping_method_id              = $shippingMethodId;
                        $order->order_shipping_status           = 2;
                        $order->payment_status                  = config('constants.APP_CONSTANT.PAYMENT_STATUS.pending');
                        $ccExtra3Percent = ($cartSubTotal * config('constants.EXTRA_CREDIT_CARD_CHARGE')) / 100;
                        $orderTotalAmount = $consolidatedShippingCharges + $ccExtra3Percent + $cartSubTotal+$shippingCharges + $order->order_tax_amount - $order->order_discount_amount;
                        $orderTotalAmount = round($orderTotalAmount, 2);
                        $order->order_total_amount              = $orderTotalAmount;
                        $order->shipping_method_type            = $shippingMethodType;
                        $order->shipping_account_number         = $accountNumber;
                        $order->shipping_notes                  = $shippingNotes;
                        $order->requested_delivery_date                  = $requestedDeliveryDate;
                        $order->consolidated_shipping_charge  = $consolidatedShippingCharge;
                        $order->payment_notes  = $paymentNotes;
                        $order->shipping_purchase_order_number  = $shippingPurchaseOrderNumber;
                        if ($order->save()) {
                            foreach (Cart::instance('shoppingcart')->content() as $row) {
                                $orderDetails = new OrderDetail();
                                $orderDetails->order_id             = $order->id;
                                $orderDetails->quantity             = $row->qty;
                                $orderDetails->distributor_name     = $row->options->distributorName;
                                $orderDetails->item_name            = $row->name;
                                $orderDetails->price                = $row->price;
                                $orderDetails->manufacturer_name    = $row->options->manufacturer;
                                $orderDetails->manufacturer_code    = $row->options->manufacturerCode;
                                $orderDetails->source_part_id       = $row->options->sourcePartId;
                                $orderDetails->date_code            = $row->options->dateCode;
                                $orderDetails->customer_part_id     = $row->options->customerPartNo;
                                if($row->options->quoteItemId !=0){
                                    $orderDetails->is_quote_item            = 1;
                                    $orderDetails->quote_id                 = $row->options->quoteId;
                                }
                                if(!empty($row->options->bomId))
                                    $orderDetails->ordered_bom_id     = $row->options->bomId;
                                $orderDetails->save();
                            }
                            $orderId    = $order->id;

                            $response   = $this->creditCardPayment($orderId,$orderTotalAmount,$cardNumber,$expiryMonth,$cvv,$nameOnCard,$lastName,$cardType,$expiryYear,$cardStreetAddress ,$cardCityName,$cardStateName,$cardCountryCode ,$cardZipCode);
                            if(empty($response)){
                                return response(['success' =>false, 'data' =>'', 'message' =>'Currently there is some error with this payment method.Please choose another payment metnod for this order.']);
                            }
                            if ($response['ACK'] == 'Success') {
                                $transactionId                      =   $response['TRANSACTIONID'];
                                $transaction                        =   new TransactionsDetail();
                                $transaction->transaction_id        =   $transactionId;
                                $transaction->transaction_by        =   $paymentMethod;
                                $transaction->order_id              =   $orderId;
                                if ($transaction->save()) {
                                    $order                                 = Order::findOrFail($orderId);
                                    $order->payment_status                 = 1;
                                    $order->order_status                   = 1;
                                    $order->tranaction_reference_id        = $transactionId;
                                    if ($order->save()) {
                                        foreach(Cart::instance('shoppingcart')->content() as $row){
                                            if($row->options->quoteItemId !=0){
                                                $quoteDetail                                = QuoteDetail::findOrFail($row->options->quoteItemId);
                                                $quoteDetail->is_ordered                    = 1;
                                                $quoteDetail->save();

                                                $quoteItemCount                 =  QuoteDetail::where('quote_id',$row->options->quoteId)->count();
                                                $orderedQuoteItemCount          =  QuoteDetail::where('quote_id',$row->options->quoteId)->where('is_ordered',1)->count();
                                                if($quoteItemCount >0 && $quoteItemCount ==$orderedQuoteItemCount  ){
                                                    $quote                      = Quote::findOrFail($row->options->quoteId);
                                                    $quote->is_approved         =  4;
                                                    $quote->save();
                                                }
                                            }
                                        }
                                        session(['selectedAddressId'=>'']);
                                        session(['selectedShippingMethodId'=>'']);
                                        session(['shippingMethodTypeInput'          => '']);
                                        session(['accountNumber'                    => '']);
                                        session(['shippingNotes'                    => '']);
                                        session(['consolidatedShippingCharge'       => '']);
                                        session(['shippingPurchaseOrderNumber'                    => '']);
                                        Cart::instance('shoppingcart')->destroy();
                                        Session::put('ordersuccess', 1);
                                        \Helpers::sendOrderSuccessMail($orderId);
                                        return response(['success' => true, 'txNumber'=>$transactionId,'orderId'=>$orderId, 'data' => $response, 'message' => config('constants.ORDER_SUCCESS')]);
                                    }else{
                                        return response(['success' => false, 'txNumber'=>$transactionId,'orderId'=>$orderId, 'data' => $response, 'message' => config('constants.ORDER_SUCCESS')]);
                                    }
                                }else{
                                    return response(['success' => true, 'txNumber'=>$transactionId,'orderId'=>$orderId, 'data' => $response, 'message' => config('constants.ORDER_SUCCESS')]);
                                }
                            } else if ($response['ACK'] == 'Failure') {
                                Order::where('id', $orderId)->update(['payment_status'=>2,'deleted_at' => date('Y-m-d h:i:s')]);
                                OrderDetail::where('order_id', $orderId)->update(['deleted_at' => date('Y-m-d h:i:s')]);
                                $failureMessage = $response['L_LONGMESSAGE0'];
                                return response(['success' =>false, 'data' =>$response, 'message' =>$failureMessage]);
                            }
                        }else{
                            return response(['success' =>false, 'data' =>'', 'message' =>'Currently there is some error in processing your order with this card.Please try again later.']);
                        }
                    }
                });
                return $response;
            }else{
                return response(['success' => false, 'cart_count'=>0, 'message' => 'There is no item in cart to proceed check out.']);
            }

        }catch (\Exception $ex) {
          return response(['success' => false, 'data' => '', 'message' => $ex->getMessage().$ex->getLine()]);
       }
    }
    public function creditCardPayment($orderId,$orderTotalAmount,$cardNumber,$expiryMonth,$cvv,$nameOnCard,$lastName,$cardType,$expiryYear,$cardStreetAddress ,$cardCityName,$cardStateName,$cardCountryCode ,$cardZipCode){
        $apiVersion    = config('constants.PAY_PAL_PRO.API_VERSION');
        $apiEndpoint   = config('constants.PAY_PAL_PRO.URL');
        $apiUsername   = config('constants.PAY_PAL_PRO.USER_NAME');
        $apiPassword   = config('constants.PAY_PAL_PRO.PASSWORD');
        $apiSignature  = config('constants.PAY_PAL_PRO.SIGNATURE');
        //Address is taken from billing address
        $requestParams = array(
            'METHOD'            => 'DoDirectPayment',
            'USER'              => $apiUsername,
            'PWD'               => $apiPassword,
            'SIGNATURE'         => $apiSignature,
            'VERSION'           => $apiVersion,
            'PAYMENTACTION'     => 'Sale',
            'IPADDRESS'         => $_SERVER['REMOTE_ADDR'],
            'CREDITCARDTYPE'    => $cardType,
            'ACCT'              => $cardNumber,
            'EXPDATE'           => $expiryMonth.$expiryYear,
            'CVV2'              => $cvv,
            'FIRSTNAME'         => $nameOnCard,
            'LASTNAME'          => $lastName,
            'STREET'            => $cardStreetAddress,
            'CITY'              => $cardCityName,
            'STATE'             => $cardStateName,
            'COUNTRYCODE'       => $cardCountryCode,
            'ZIP'               => $cardZipCode,
            'CURRENCYCODE'      => 'USD',
            'AMT'               => $orderTotalAmount,
            'DESC'              => 'Payment for Order Id '.$orderId
        );
        $nvpString = '';
        foreach($requestParams as $var=>$val) {
            $nvpString .= '&'.$var.'='.urlencode($val);
        }
        // Send NVP string to PayPal and store response
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_VERBOSE, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_URL, $apiEndpoint);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $nvpString);
        $result = curl_exec($curl);
        return $this->NVPToArray($result);
    }
// Function to convert NTP string to an array
    function NVPToArray($NVPString){
        $proArray = array();
        while(strlen($NVPString))
        {
            // name
            $keypos= strpos($NVPString,'=');
            $keyval = substr($NVPString,0,$keypos);
            // value
            $valuepos = strpos($NVPString,'&') ? strpos($NVPString,'&'): strlen($NVPString);
            $valval = substr($NVPString,$keypos+1,$valuepos-$keypos-1);
            // decoding the respose
            $proArray[$keyval] = urldecode($valval);
            $NVPString = substr($NVPString,$valuepos+1,strlen($NVPString));
        }
        return $proArray;
    }
    public function shoppingCompleted($txNumber,$orderId){
        $data['txNumber'] =  $txNumber;
        $data['orderId'] =  $orderId;
        $ordersuccess        = Session::get('ordersuccess');
        if(!empty($ordersuccess)){
            Session::forget('ordersuccess');
            return view('frontend.shipping.thank-you',$data);
        }else {
            return redirect('/not-found');
        }
    }
    public function paymentError($txNumber,$orderId){
        $data['txNumber']   =  $txNumber;
        $data['orderId']    =  $orderId;
        return view('frontend.payments.payment-error',$data);
    }
    public function applyCouponCode(Request $request){
        try{
            $data = $request->all();
            $couponCode   =  $request->input('couponCode');
            $isCouponExist           =  Coupon::checkCouponCode($couponCode);
            if( count($isCouponExist)>0 ){
                $couponsUsageCount      =  Coupon::getCouponUsageQuantity($isCouponExist[0]->id);
                if($isCouponExist[0]->status == 1){
                    $expiryDate =  date('Y-m-d', strtotime($isCouponExist[0]->end_date));
                    $currentDate =  date('Y-m-d');
                    if($currentDate > $expiryDate) {
                        return response(['status' => false, 'data' => '', 'message' =>config('constants.PROMOCODE_EXPIRED')]);
                    }else{

                        if($isCouponExist[0]->code == $couponCode){
                            $cartTotalAmount = (float)str_replace(',', '', Cart::instance('shoppingcart')->subtotal(0));

                            if($cartTotalAmount >= $isCouponExist[0]->minimum_cart_amount){
                                if($couponsUsageCount >= $isCouponExist[0]->order_limit ){
                                    return response(['status' => false, 'data' => '', 'message' =>config('constants.PROMOCODE_EXPIRED')]);
                                }else{
                                    if($isCouponExist[0]->is_free == 1){
                                        $code['id']                     = $isCouponExist[0]->id;
                                        $code['is_free']                = $isCouponExist[0]->is_free;
                                        $code['discount_percentage']    = $isCouponExist[0]->discount_percentage;
                                        $code['discount_amount']        = $this->getPercentOfNumber($cartTotalAmount, $code['discount_percentage']);
                                        $code['shipping_charges']       = Session::get('shipping_charges');
                                        $code['cart_subtotal']          = $cartTotalAmount;
                                        //Order Total Calculation
                                        //Cart sub total  - Discount amount
                                        //Amount after subtract discount  will be added with shipping charges will be total order amount
                                        $newSubTotal =    $cartTotalAmount  ;
                                        $code['cart_total']             = number_format($newSubTotal + Session::get('shipping_charges'),2) ;
                                        return response(['status' => true, 'data' => $code, 'message' =>config('constants.FREE_SHIPPING_CODE')]);
                                    }else{
                                        $code['id']                     = $isCouponExist[0]->id;
                                        $code['is_free']                = $isCouponExist[0]->is_free;
                                        $code['discount_percentage']    = $isCouponExist[0]->discount_percentage;
                                        $code['discount_amount']        = $this->getPercentOfNumber($cartTotalAmount, $code['discount_percentage']);
                                        $code['shipping_charges']       = Session::get('shipping_charges');
                                        $code['cart_subtotal']          = $cartTotalAmount;
                                        $newSubTotal                    = $cartTotalAmount-$code['discount_amount'] ;
                                        $code['cart_total']             = number_format($newSubTotal + Session::get('shipping_charges'),2)  ;
                                        return response(['status' => true, 'data' => $code, 'message' => config('constants.PROMOCODE_APPLY_SUCCESS')]);
                                    }
                                }
                            }else{
                                return response(['status' => false, 'data' => '', 'message' => 'The minimum cart amount to apply this code is $'.$isCouponExist[0]->minimum_cart_amount]);
                            }
                        }else{
                            return response(['status' => false, 'data' => '', 'message' => config('constants.NO_PROMO_CODE_MATCHED')]);
                        }

                    }
                }else{
                    return response(['status' => false, 'data' => 1, 'message' => config('constants.PROMO_CODE_NOT_FOUND')]);
                }
            } else {
                return response(['status' => false, 'data' => '', 'message' =>config('constants.PROMO_CODE_NOT_FOUND')]);
            }

        }catch (\Exception $ex) {
            return response(['status' => false, 'data' => $isCouponExist, 'message'=>$ex->getMessage()]);
        }
    }

    public function removeCouponCode(Request $request){
        try {
            $data = $request->all();
            $cartTotalAmount            =  (float)str_replace(',', '', Cart::instance('shoppingcart')->subtotal(0));
            $shippingCharges            = Session::get('shipping_charges');
            $code['order_total']        =  number_format($cartTotalAmount + $shippingCharges,2) ;
            $code['discount_amount']    = 0;
            $code['id']                = '';
            return response(['status' => true, 'data' => $code, 'message' => config('constants.PROMOCODE_REMOVED')]);
        }catch (\Exception $ex) {
            return response(['status' => false, 'data' => '', 'message'=>$ex->getMessage()]);
        }
    }

    public function getPercentOfNumber($amount, $percent){

        return ($percent / 100) * $amount;

    }

}