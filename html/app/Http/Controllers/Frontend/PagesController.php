<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use Cart;
use Event;
use App\Http\Controllers\Controller;
use App\Order;
use App\ShippingAddress;
use Auth;
use DB;
use App\News;
use App\NewsLetterSubscriber;
use App\RequestADemo;
use App\Mail\WelComeNewsLetterSubscriberMail;
use App\Mail\RequestADemoMail;
use Helpers;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class PagesController extends Controller
{
    protected $userSessionData;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
        Event::listen(Authenticated::class, function ($event) {
            $this->userSessionData = $event->user;
            if ($this->userSessionData['status'] == 0) {
                Helpers::sessionFlush();
            } 
        });
    }

    public function aboutUs(Request $request){
        $data['title'] = 'About Us';
        return view('frontend.pages.about-us');
    }
public function meetTheTeam(Request $request){
        $data['title'] = 'Meet the Team';
        return view('frontend.pages.meet-the-team');
    }
    public function newUpdates(Request $request){
        $news = News::newsList();
           // Get current page form url e.x. &page=1
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
         // Create a new Laravel collection from the array data
        $itemCollection = collect($news);
        // Define how many items we want to be visible in each page
        $perPage = 7;
         // Slice the collection to get the items to display in current page
        $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
        $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
        // set url path for generted links
        $paginatedItems->setPath($request->url());
        $data['news'] =  $paginatedItems;
        return view('frontend.pages.news-update', $data);
    }

    public function faq(Request $request){
        return view('frontend.pages.faq');
    }

    public function returnPolicy(Request $request){
        return view('frontend.pages.return-policy');
    }

    public function contactUS(Request $request){
        return view('frontend.pages.contact-us');
    }

    public function requestADemo(Request $request){
        return view('frontend.pages.request-a-demo');
    }

    public function saveRequestADemo(Request $request){
        try{
            $data               = $request->all();
            $validator          = \Validator::make($data, RequestADemo::validationRulesForRequestADemo(), RequestADemo::$validationMessages);
            if ($validator->fails()) {
                //Redirect request a demo back with input if server side validation fails
                return redirect('/request-a-demo/')->withErrors($validator)->withInput();
            }else{
                $RequestADemo       =  new RequestADemo();
                //save requested contact details
                $RequestADemo->company_name =  $data['company_name'] ;
                $RequestADemo->contact_name =  $data['contact_name'] ;
                $RequestADemo->email        =  $data['email'] ;
                $RequestADemo->phone        =  $data['phone'] ;
                if($RequestADemo->save()) {
                    \Mail::to(config('constants.ADMIN_MAIL'))->send(new RequestADemoMail($data, config('constants.REQUEST_A_DEMO_SUBJECT')));
                    return redirect()->to('request-a-demo')->with('success', config('constants.REQUEST_A_DEMO_SUCCESS'));
                } else {
                    return response(['status' => false, 'data' => '', 'message' => config('constants.COMMON_ERROR')]);
                }
            }
        }catch (\Exception $ex) {
            return response(['status' => false, 'data' => $ex->getLine(), 'message' =>$ex->getMessage()]);
        }
    }

    public function orderHistory(Request $request){ 
        try {
            $columns = [
                'orders.id',
                'users.name',
                'users.last_name',
                'orders.order_date',
                'orders.order_total_amount',
                'orders.order_shipping_status',
                'orders.order_tracking_info',
                'orders.partially_shipped'
            ];
            $ordersArray =  DB::table('orders')
            ->select($columns)
            ->join('users','orders.users_id','=','users.id')
            ->where('orders.order_status',1); 
            if($request->has('tab') && $request->get('tab') != ''){
                $ordersArray = $ordersArray->where('orders.order_shipping_status', $request->get('tab'));
            }else{
                $ordersArray = $ordersArray->where('orders.order_shipping_status', 2);
            }
            $ordersArray = $ordersArray->where('orders.users_id',Auth::user()->id)
                ->orderBy('orders.id', 'DESC')
                ->whereNull('orders.deleted_at')
                ->get()->toArray(); 
                // Get current page form url e.x. &page=1
                $currentPage = LengthAwarePaginator::resolveCurrentPage();
                // Create a new Laravel collection from the array data
                $itemCollection = collect($ordersArray);
                // Define how many items we want to be visible in each page
                $perPage = 10;
                // Slice the collection to get the items to display in current page
                $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
                // Create our paginator and pass it to the view
                $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
                // set url path for generted links
                $paginatedItems->setPath($request->url());
                $data['orders']  =  $paginatedItems;
                return view('frontend.pages.order-history',$data);
        }catch (\Exception $ex) {
            return redirect()->to('order-history')->with('failure', 'Oops. There is a problem. Please try again later');
        }
    }

    public function orderDetails(Request $request ,$id){
        try{
            $order            = Order::getOrderInfoById($id);
            if(empty($order)){
                return redirect()->to('order-history')->with('failure', config('constants.COMMON_ERROR'));
            }else{
                $data['order']          = $order;
                $data['orderDetail']    = Order::getOrderDetailsById($data['order'][0]->id);
                $data['shippingAddress']    = ShippingAddress::getShippingAddressById($data['order'][0]->shipping_address_id);
                $data['orderStatus'] = Order::$shippingStatus;
                return view('frontend.pages.order-detail',$data);
            }
        }catch (\Exception $ex) {
            return redirect()->to('order-history')->with('failure', config('constants.COMMON_ERROR'));
        }
    }

    public function reOrder(Request $request ,$id){
        try{
            $order            = Order::getOrderInfoById($id);
            if(empty($order)){
                return redirect()->to('order-history')->with('failure', config('constants.COMMON_ERROR'));
            }else{
                $data['order']          = $order;
                $data['orderDetail']    = Order::getOrderDetailsById($data['order'][0]->id);
                $resultArray2         =  [];
				$partsArray = [];
				$finalProductData = [];
                foreach ($data['orderDetail'] as $key=>$partDetails){
                        $orderedPartNumber          =   $partDetails->item_name;
						$partsArray[] =   $partDetails->item_name;
                        $reorderArray = [];
                        $reorderArray['orderedQty'] =   $partDetails->quantity;
                        if($partDetails->manufacturer_code == "") {
                            $reorderArray['orderedManufacturerCode'] = $partDetails->manufacturer_name;
                        }else{
                            $reorderArray['orderedManufacturerCode'] =   $partDetails->manufacturer_code;
                        }
                        $reorderArray['orderedSourcePartId'] = $partDetails->source_part_id;
                        $reorderArray['orderedCustomerPartId'] = $partDetails->customer_part_id;
                        $resultArray2[] = Helpers::searchProductOrbweaver($orderedPartNumber,'','','', $reorderArray);
						
                }
					if(!empty($resultArray2)){
						foreach($resultArray2 as $orwea){
							foreach($orwea as $orw){
								$finalProductData[] = $orw;
							}
						}
					}
      
                    $resultedParts = array();
					foreach($finalProductData as $part){
						$resultedParts[] = $part['partNum'];
					}
					$unmantchedParts = array();
				    /* Create array of part which not found in seached */
						foreach ($data['orderDetail'] as $key=>$parts){    
							if(!in_array($parts->item_name, $resultedParts)){
								$tempArray = array();
								$tempArray['partNum']   = trim($parts->partNum);
								$tempArray['QTY']       = trim($parts->QTY);
								$tempArray['internalPart'] = trim($parts->internalPart);
								$tempArray['manufacturer'] =  '';
								$unmantchedParts[] = $tempArray;
							}
						}    
				    $data['noPartFound']        = $unmantchedParts;
					$data['bom_id'] =  $request->id;
                    array_walk_recursive($finalProductData, 'Helpers::update_price');
                    $data['finalData']          = $finalProductData;
                     return view('frontend.pages.reorder-parts',$data);
            }
        }catch (\Exception $ex) {
			echo  $ex->getMessage().$ex->getLine();die('die');
            return redirect()->to('order-history')->with('failure', $ex->getMessage());
        }
    }

    public function bomDetails(Request $request){
        return view('frontend.pages.bom-details');
    }

    public function  siteMap(){
        return view('frontend.pages.sitemap');
    }

    public function usesTerms(){

        return view('frontend.pages.terms-uses');
    }

    public function tcSale(){

        return view('frontend.pages.tc-sale');
    }

    public  function privacyPolicy(){
        return view('frontend.pages.privacy-policy');
    }

    public function tcExport(){
        return view('frontend.pages.tc-export');
    }

    public function cookiePolicy(){
        return view('frontend.pages.cookie-policy');
    }

    public function getNewsDetailsById($id){
        try{
            $getNewsDetailsById = News::getNewsDetailsById($id);
            if(count($getNewsDetailsById)==0){
                return redirect()->to('order-history')->with('failure', config('constants.COMMON_ERROR'));
            }else{
                $data['getNewsDetailsById']          = $getNewsDetailsById;
                return view('frontend.pages.news-details',$data);
            }
        }catch (\Exception $ex) {
            return redirect()->to('order-history')->with('failure', config('constants.COMMON_ERROR'));
        }
    }

    public function signUpNewsLetter(Request $request){ 
        try{
            $data               = $request->all();
            $emailNewsletter    = $request->input('emailNewsletter');
            $resultData         = NewsLetterSubscriber::checkAlreadyInSubscriberList($emailNewsletter); 
            if(empty($resultData)){
                $newsLetterSubscriber  =  new NewsLetterSubscriber();
                //save subscriber details
                $newsLetterSubscriber->subscriber_email    =  $emailNewsletter;
                $newsLetterSubscriber->is_subscribed       =  1;
                if($newsLetterSubscriber->save()) {
                    $data['email'] =  $emailNewsletter;
                    \Mail::to($emailNewsletter)->send(new WelComeNewsLetterSubscriberMail($data, config('constants.NEWSLETTER_SUBSCRIPTION_SUBJECT')));
                    return response(['status' => true, 'data' => '', 'message' => config('constants.NEWSLETTER_SUBSCRIPTION_SUCCESS')]);
                } else {
                    return response(['status' => false, 'data' => '', 'message' => config('constants.COMMON_ERROR')]);
                }
            }else{
                return response(['status' => false, 'data' => '', 'message' =>  config('constants.NEWSLETTER_SUBSCRIPTION_ALREADY')]);
            }
        }catch (\Exception $ex) {
            return response(['status' => false, 'data' => '', 'message' =>config('constants.COMMON_ERROR')]);
        }
    }

    public  function unsubscribeNewsLetter($email){
        try{
            $resultData         = NewsLetterSubscriber::checkAlreadyInSubscriberList(\Crypt::decrypt($email));
            if(!empty($resultData)){
                    $updateList = array('is_subscribed'=>0);
                    $data['success']= NewsLetterSubscriber::updateNewsLetterSubscription(\Crypt::decrypt($email),$updateList);
                if($data['success']) {
                    return redirect()->to('/')->with('success', config('constants.NEWS_LETTER_UNSUBSCRIBE_SUCCESS'));
                } else {
                    return redirect()->to('/')->with('failure', config('constants.COMMON_ERROR'));
                }
            }else{
                return redirect()->to('/')->with('failure',config('constants.SUBSCRIBE_EMAIL_NOT_FOUND'));
            }
        }catch (\Exception $ex) {
            return redirect()->to('/')->with('failure', config('constants.COMMON_ERROR'));
        }
    }
}
