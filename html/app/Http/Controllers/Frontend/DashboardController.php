<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use App\Quote;
use App\Bom;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Rap2hpoutre\FastExcel\FastExcel;
use Helpers; // Important
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

/*
    * Controller BioController
    * Developer: Khalid
    * Date: 04/03/18
    * Purpose: Manage customer Bio Information
*/
class DashboardController extends Controller{
    protected $userSessionData;
    /*
        * Create a new controller instance.
        * @return void
    */
    public function __construct(){ 
        Event::listen(Authenticated::class, function ($event) {
            $this->userSessionData = $event->user;
            if ($this->userSessionData['status'] == 0) {
                Helpers::sessionFlush();
            }
        }); 
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userData = Auth::user();
		
        if(isset($userData->id) && !empty(($userData->id))) {
            $quote = Quote::select('quotes.id')
                    ->join('quote_details','quotes.id','=','quote_details.quote_id')
                    ->where('quotes.is_approved','0')
                    ->where('quotes.user_id', $userData->id)
                    ->whereNull('quotes.deleted_at')->groupBy('quote_details.quote_id')->get();
            $data['quote'] = count($quote);

            $processedQuote = Quote::select('quotes.id')
                    ->join('quote_details','quotes.id','=','quote_details.quote_id')
                    ->where('quotes.is_approved','1')
                    ->where('quotes.user_id', $userData->id)
                    ->whereNull('quotes.deleted_at')->groupBy('quote_details.quote_id')->get();
            $data['processedQuote'] = count($processedQuote);

            $orderedQuote = Quote::getQuoteListByUserId($userData->id);
            $data['orderedQuote'] = count($orderedQuote);

            $data['working_boms'] = 0;  

            $saved_boms = Bom::select('boms.id')
                     ->where('user_id',$userData->id)
                     ->where('bom_status','published')
                    ->whereNull('boms.deleted_at')->get(); 
            $data['saved_boms'] = count($saved_boms); 
            $getUser = DB::table('users')->select('is_sub_user')->where('id', $userData->id)->get();
            if($getUser[0]->is_sub_user){
                $bomList = DB::table('boms')
                ->whereRaw('(FIND_IN_SET('.$userData->id.', shared_user_id) OR user_id='.$userData->id.')')
                ->where('bom_status','published')
                ->whereNull('deleted_at')
                ->orderBy('id','DESC')
                ->get();
            }else{
				
                $bomList = DB::table('boms')
                //->where('user_id',$userData->id)
			    ->whereRaw('(user_id ='.$userData->id.' OR shared_user_id='.$userData->id.')')
                ->whereNotNull('shared_user_id')
                ->where('bom_status','published')
                ->whereNull('deleted_at')
                ->orderBy('id','DESC')
                ->get();
            }
		    $data['shared_boms'] = count($bomList);

            for($i=0; $i<=3;$i++){
                $orders_status = Order::where('orders.order_status',1)
                                ->where('orders.payment_status',$i) 
                                ->join('payment_methods','orders.payment_method_id','=','payment_methods.id')
                                ->orderBy('orders.id', 'DESC')
                                ->whereNull('orders.deleted_at')
                                ->where('orders.users_id', $userData->id)
                                ->count();
                if($i == '0')
                    $data['orders_pending'] = $orders_status; 
            } 

            for($j=1; $j<=4;$j++){
                $orders_ship_status = Order::where('orders.order_status',1)
                                ->where('orders.order_shipping_status',$j) 
                                ->join('payment_methods','orders.payment_method_id','=','payment_methods.id')
                                ->orderBy('orders.id', 'DESC')
                                ->whereNull('orders.deleted_at')
                                ->where('orders.users_id', $userData->id)
                                ->count();
                if($j == '1')
                    $data['orders_shipped'] = $orders_ship_status;                 
                elseif($j == '2')
                    $data['orders_pending_ship'] = $orders_ship_status;
                elseif($j == '3')
                    $data['orders_intransit'] = $orders_ship_status;
                elseif($j == '4')
                    $data['orders_delivered'] = $orders_ship_status;
            }                 
        }                    
        return view('frontend.dashboard', $data);
    }
}//End Class