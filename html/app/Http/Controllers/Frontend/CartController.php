<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use Cart;
use Event;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Authenticated;
use Helpers; // Important
use Illuminate\Support\Facades\Redirect;

class CartController extends Controller
{
    protected $userSessionData;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
        public function __construct(){
        }
        public function confirmReorderAndAddToCart(Request $request){
            try {
                $data = $request->all();
                $selectedProducts       = $request->selectedProducts;
                $productIds             = $request->productId;
                $distributorName = !empty($request->distributorName)?$request->distributorName:'N/A';
                $manufacturers          = $request->manufacturer;
                $manufacturerCodes      = $request->mfrCd;
                $descriptions           = $request->desc;
                $sourcePartIds          = $request->sourcePartId;
                $quantites              = $request->quantities;
                $dateCodes              = $request->dateCode;
                $customerPartNos        = $request->customerPartNo;
                $stockAvailabilities    = $request->stockAvailability;
                $sourceMinQuantities =  $request->sourceMinQuantity;
                $prices                 = 0;
                $resaleListDataArray    = $request->resaleList;
                $availableDataArray     = $request->availableData;
                $resaleLists            = array();
                $availableDatas         = array();
                foreach ($productIds as $key => $value) {
                    foreach($selectedProducts as $keys => $vals){
                        if($productIds[$key] == $vals){
                            $productId          = $productIds[$key];
                            $manufacturer       = $manufacturers[$key];
                            $sourcePartId       = $sourcePartIds[$key];
                            $quantity           = $quantites[$key];
                            $finalQtyToAdd      =  0;
                            $description        = $descriptions[$key];
                            $dateCode           = $dateCodes[$key];
                            $customerPartNo     = $customerPartNos[$key];
                            $stockAvailability  = $stockAvailabilities[$key];
                            $sourceMinQuantity  =  $sourceMinQuantities[$key];
                            $manufacturerCode   = $manufacturerCodes[$key];
                            $price = 0;
                            $resaleList         = json_decode($resaleListDataArray[$key],true);
                            $availableData      = json_decode($availableDataArray[$key],true);
                            $availableStock     = 0;
                            $minQty             = 0;
                            if ($quantity > 0) {
                                $itemExist =  Cart::instance('shoppingcart')->content()->where('id', $productId)->where('options.sourcePartId', $sourcePartId);
                                if(empty(json_decode($itemExist))){
                                    if($stockAvailability){
                                        $availableStock = $availableData[0]['fohQty'];
                                        if($availableStock >= $quantity ){
                                            $finalQtyToAdd   =  $quantity ;
                                        }else{
                                            $finalQtyToAdd   =  $availableStock ;
                                        }
                                    }else{
                                        $availableStock   = 0;
                                    }
                                    foreach ($resaleList as $key => $prices) {
                                        if($finalQtyToAdd >= $prices['minQty'] && $quantity <= $prices['maxQty'] ){
                                            $price = $prices['price'];
                                            break;
                                        }
                                    }
                                    Cart::instance('shoppingcart')->add([['id'=>$productId,'name'=>$productId,'qty'=>$finalQtyToAdd,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'sourceMinQuantity'=>$sourceMinQuantity,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,'original_quantity' => $quantity,'quotedItem'=>0,'isQuoteApprovedItem'=>0,'quoteItemId'=>0,'quoteId'=>0, 'distributorName' => $distributorName]]]);
                                }
                            }
                        }
                    }
                }
                return response(['status' => true,  'message' => config('constants.ITEM_SUCCESS_CART'),'data'=>$data]);
            }catch (\Exception $ex) {
                return response(['status' => false,  'line'=>$ex->getLine(), 'message' => $ex->getMessage(),'data'=>'']);
            }

    }
        public function addSelectedPartToCart(Request $request){
            $productDetails ='';
            try {
                $data                   =  $request->all();
                $finalData              =  $data["formdata"];
                foreach ($finalData  as  $key1=> $arrayNewValue){
                    parse_str($finalData[$key1], $arrayValue);
                    $productIds             = $arrayValue['productId'];
                    $distributorName        = !empty($arrayValue['distributorName'])?$arrayValue['distributorName']:config("constants.DEFAULT_DISTRIBUTOR_NAME");
                    $manufacturers          = $arrayValue['manufacturer'];
                    $sourcePartIds          = $arrayValue['sourcePartId'];
                    $quantitys              = $arrayValue['quantities'];
                    $dateCodes              = $arrayValue['dateCode'];
                    $customerPartNos        = $arrayValue['customerPartNo'];
                    $stockAvailabilitys     = $arrayValue['stockAvailability'];
                    $sourceMinQuantities    = $arrayValue['sourceMinQty'];
                    $prices                 = 0;
                    $resaleLists            = array();
                    $availableDatas         = array();
                    $manufacturerCodes      = $arrayValue['mfrCd'];
                    $productDetails         = json_decode($arrayValue['productDetails'],true);
                    $availableStocks        = 0;
                    $minQtys                = 0;
                    $index                  = 0;
                    $bomId                  = $arrayValue['bomId'];
                    foreach ($productIds as $key => $value) {
                        $productId          = $productIds[$key];
                        $distributorName    = $distributorName;
                        $manufacturer       = $manufacturers[$key];
                        $sourcePartId       = $sourcePartIds[$key];
                        $quantity           = $quantitys[$key];
                        $dateCode           = $dateCodes[$key];
                        $customerPartNo     = $customerPartNos[$key];
                        $stockAvailability  = $stockAvailabilitys[$key];
                        $sourceMinQuantity  = $sourceMinQuantities[$key];
                        $price              = 0;
                        $resaleList         = array();
                        $availableData      = array();
                        $manufacturerCode   = $manufacturerCodes[$key];
                        $availableStock     = 0;
                        $minQty             = 0;
                        $index++;
                        if($quantity > 0){
                            $itemExist =  Cart::instance('shoppingcart')->content()->where('id', $productId)->where('options.sourcePartId', $sourcePartId)->toArray();
                            if(empty($itemExist)){
                                $resaleList = array();
                                /* Price calculation on the bases of Qty */
                                foreach ($productDetails['sources'] as $sourcesKey => $sources) {
                                    foreach ($sources['sourcesPart'] as $sourcePartKey => $sourcesPart) {
                                        if($sourcesPart['sourcePartId'] == $sourcePartId){
                                            $dateCode = $sourcesPart['dateCode'];
                                            if($sourcesPart['inStock']){
                                                $availableStock = $sourcesPart['Availability'][0]['fohQty'];
                                            }else{
                                                $availableStock = 0;
                                            }
                                            $availableData = $sourcesPart['Availability'];
                                            $i = 1;
											foreach ($sourcesPart['Prices']['resaleList'] as $key => $prices) {
                                                if($quantity >= $prices['minQty'] && $quantity <= $prices['maxQty'] ){
                                                    if(!empty($prices['price']))
                                                        $price = $prices['price'];
                                                    $resaleList = $sourcesPart['Prices']['resaleList'];
                                                    break;
                                                }
												if(count($sourcesPart['Prices']['resaleList']) == $i){
													if(!empty($prices['price']))
                                                        $price = $prices['price'];
													$resaleList = $sourcesPart['Prices']['resaleList'];
												}
												$i++;
                                            }
											$minQty = $sourcesPart['Prices']['resaleList'][0]['minQty'];
                                        }
                                    }
                                }
                            
                                /* Check available stock of items */
                                $originalQuantity = $quantity;
                                if($quantity > $availableStock){
                                    $quantity = $availableStock;
                                }

                                if($quantity <= $availableStock){
                                    Cart::instance('shoppingcart')->add([
                                        ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'sourceMinQuantity'=>$sourceMinQuantity,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,'original_quantity' => $originalQuantity,'quotedItem'=>0,'isQuoteApprovedItem'=>0,'quoteItemId'=>0,'quoteId'=>0, 'distributorName' => $distributorName, 'bomId' => $bomId]]
                                    ]);
                                }else{
                                    if($availableStock == 0){
                                        return response(['status' => false,  'data' => '', 'message' => 'Item is out of stock.']);
                                    }else{
                                        return response(['status' => false, 'stockError' => true, 'data' => '', 'message' => 'Availabile stock: '.$availableStock,'stock' => $availableStock]);
                                    }
                                }
                                $totalPrice = 0;
                                foreach(Cart::instance('shoppingcart')->content() as $row){
                                    $totalPrice = $totalPrice + $row->price * $row->qty;
                                }
                                $itemCount = Cart::instance('shoppingcart')->content()->count();
                            }else{
                                    /* Item Update in the cart */
                                    foreach ($itemExist as $index => $data) {
                                        $rowId          = $data['rowId'];
                                        $resaleList     =  $data['options']['resaleList'];
                                        $dateCode       =  $data['options']['dateCode'];
                                        $availableData  =  $data['options']['availableData'];
                                        $sourceMinQuantity =  $data['options']['sourceMinQuantity'];
                                        $availableStock = $availableData[0]['fohQty'];
                                    }
								
                                    $i = 1; 
									foreach ($resaleList as $key => $prices) {
                                        if($quantity >= $prices['minQty'] && $quantity <= $prices['maxQty'] ){
                                            if(!empty($prices['price']))
                                                $price = $prices['price'];
                                            break;
                                        }
										
										if(count($resaleList) == $i){
                                            if(!empty($prices['price']))
                                                $price = $prices['price'];
                                        }
										$i++;
                                        $minQty = $resaleList[0]['minQty'];
                                    }
                                    /* Check available stock of items */
                                    $originalQuantity = $quantity;
                                    if($quantity > $availableStock){
                                        $quantity = $availableStock;
                                    }
                                    if($quantity <= $availableStock){
                                        Cart::instance('shoppingcart')->update($rowId,
                                            ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'sourceMinQuantity'=>$sourceMinQuantity,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,'original_quantity' => $originalQuantity,'quotedItem'=>0,'isQuoteApprovedItem'=>0,'quoteItemId'=>0,'quoteId'=>0, 'distributorName' => $distributorName, 'bomId' => $bomId]]
                                        );
                                    }else{
                                        if($availableStock == 0){
                                            return response(['status' => false, 'data' => '', 'message' => 'Item is out of stock.']);
                                        }else{
                                            return response(['status' => false,  'stockError' => true, 'data' => '', 'message' => 'Availabile stock: '.$availableStock,'stock' => $availableStock]);
                                        }
                                    }
                                    $totalPrice = 0;
                                    foreach(Cart::instance('shoppingcart')->content() as $row){
                                        $totalPrice = $totalPrice + $row->price * $row->qty;
                                    }
                                    $itemCount = Cart::instance('shoppingcart')->content()->count();
                            }
                        }
                    }
                }
                $cartSubTotal= 0;
                foreach(Cart::instance('shoppingcart')->content() as $row){
                    $cartSubTotal = $cartSubTotal + $row->price * $row->qty;
                }
                $cartItemCount = Cart::instance('shoppingcart')->content()->count();
                return response(['status' => true,'data' => '','message' =>config('constants.ITEM_SUCCESS_CART'),'cartSubTotal'=>number_format($cartSubTotal, 2, '.', ',') ,'cartItemCount'=>$cartItemCount]);
            } catch (\Exception $ex) {
				return response(['status' => false,  'line'=>$ex->getLine(), 'message' =>$ex->getMessage(),'data'=>$productDetails]);
            }
        }
        
        public function addBomPartToCart(Request $request){

            $productDetails ='';
            try {
                $data                   = $request->all();
                $distributorName = !empty($request->distributorName)?$request->distributorName: config(constants.DEFAULT_DISTRIBUTOR_NAME);
                $productIds = $request->productId;
                $manufacturers = $request->manufacturer;
                $sourcePartIds = $request->sourcePartId;
                $quantitys = $request->quantities;
                $dateCodes = $request->dateCode;
                $customerPartNos = $request->customerPartNo;
                $stockAvailabilitys = $request->stockAvailability;
                $sourceMinQuantities =  $request->sourceMinQty;
                $prices = 0;
                $resaleLists = array();
                $availableDatas = array();
                $manufacturerCodes = $request->mfrCd;
                $availableStocks = 0;
                $minQtys = 0;
                $index = 0;
                $bomId = $request->bomId;
                foreach ($productIds as $key => $value) {
                  $productId          = $productIds[$key];
                  $manufacturer       = $manufacturers[$key];
                  $sourcePartId       = $sourcePartIds[$key];
                  $quantity           = $quantitys[$key];
                  $dateCode           = $dateCodes[$key];
                  $customerPartNo     = $customerPartNos[$key];
                  $stockAvailability  = $stockAvailabilitys[$key];
                  $sourceMinQuantity  = $sourceMinQuantities[$key];
                  $price              = 0;
                  $resaleList         = array();
                  $availableData      = array();
                  $manufacturerCode   = $manufacturerCodes[$key];
                  $availableStock     = 0;
                  $minQty             = 0;
                  $index++;
                  if($quantity > 0){
                    /* Check item is exist on cart or not  */
                    $itemExist =  Cart::instance('shoppingcart')->content()->where('id', $productId)->where('options.sourcePartId', $sourcePartId);
                    if(empty(json_decode($itemExist))){
                          $price = 0;
                          $resaleList = array();
                          /* Price calculation on the bases of Qty */
                          $productDetails = json_decode($request->productDetails,true);
                          foreach ($productDetails['sources'] as $sourcesKey => $sources) {
                            foreach ($sources['sourcesPart'] as $sourcePartKey => $sourcesPart) {
                                  if($sourcesPart['sourcePartId'] == $sourcePartId){
                                    $dateCode = $sourcesPart['dateCode'];
                                    if($sourcesPart['inStock']){
                                        $availableStock = $sourcesPart['Availability'][0]['fohQty'];
                                    }else{
                                        $availableStock = 0;
                                    }
                                    $availableData = $sourcesPart['Availability'];
                                    $i = 1;   
                                    foreach ($sourcesPart['Prices']['resaleList'] as $key => $prices) {
                                            if($quantity >= $prices['minQty'] && $quantity <= $prices['maxQty'] ){
                                                 $price = $prices['price'];
                                                 $resaleList = $sourcesPart['Prices']['resaleList'];
                                                 break;
                                           }

                                           if(count($sourcesPart['Prices']['resaleList']) == $i){
                                                $price = $prices['price'];
                                                $resaleList = $sourcesPart['Prices']['resaleList'];
                                           }
                                           $i++;
                                        }
                                     $minQty = $sourcesPart['Prices']['resaleList'][0]['minQty'];
                                  }
                            }
                          }
                           /* Check mim quantity of items */
                           if($quantity < $minQty){
                             return response(['status' => false,  'line'=>91, 'minQtyError' => true, 'data' => '', 'message' => 'Minimum order qty for part number '.$productId.' is '.$minQty.', We request you to check Buying Options first','minQty' => $minQty,'quantity' => $quantity]);
                           }
                           /* Check available stock of items */
                           $originalQuantity = $quantity;
                           if($quantity > $availableStock){
                                $quantity = $availableStock;
                           }
                      if($quantity <= $availableStock){
                        Cart::instance('shoppingcart')->add([
                          ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'sourceMinQuantity'=>$sourceMinQuantity,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,'original_quantity' => $originalQuantity,'quotedItem'=>0,'isQuoteApprovedItem'=>0,'quoteItemId'=>0,'quoteId'=>0, 'distributorName' => $distributorName, 'bomId' => $bomId]]
                        ]);
                      }else{
                        if($availableStock == 0){
                          return response(['status' => false,  'data' => '', 'message' => 'Item is out of stock.']);
                        }else{
                          return response(['status' => false, 'stockError' => true, 'data' => '', 'message' => 'Availabile stock: '.$availableStock,'stock' => $availableStock]);
                        }
                      }
                      $totalPrice = 0;
                      foreach(Cart::instance('shoppingcart')->content() as $row){
                        $totalPrice = $totalPrice + $row->price * $row->qty;
                      }
                        $itemCount = Cart::instance('shoppingcart')->content()->count();
                    }else{
                        /* Item Update in the cart */
                        foreach ($itemExist as $index => $data) {
                          $rowId = $data->rowId;
                          $resaleList = $data->options->resaleList;
                          $dateCode = $data->options->dateCode;
                          $availableData = $data->options->availableData;
                          $sourceMinQuantity = $data->options->sourceMinQuantity;

                          $availableStock = $availableData[0]['fohQty'];
                        }
                        foreach ($resaleList as $key => $prices) {
                              if($quantity >= $prices['minQty'] && $quantity <= $prices['maxQty'] ){
                               $price = $prices['price'];
                               break;
                             }
                        }
                       /* Check mim quantity of items */
                       $minQty = $resaleList[0]['minQty'];
                       if($quantity < $minQty){
                         return response(['status' => false, 'minQtyError' => true, 'data' => '', 'message' => 'Minimum order qty for part number '.$productId.' is '.$minQty.', We request you to check Buying Options first','minQty' => $minQty]);
                       }
                   /* Check available stock of items */
                       $originalQuantity = $quantity;
                       if($quantity > $availableStock){
                            $quantity = $availableStock;
                       }
                      if($quantity <= $availableStock){
                            Cart::instance('shoppingcart')->update($rowId,
                            ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'sourceMinQuantity'=>$sourceMinQuantity,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,'original_quantity' => $originalQuantity,'quotedItem'=>0,'isQuoteApprovedItem'=>0,'quoteItemId'=>0,'quoteId'=>0, 'distributorName' => $distributorName, 'bomId' => $bomId]]
                            );
                       }else{
                          if($availableStock == 0){
                            return response(['status' => false, 'data' => '', 'message' => 'Item is out of stock.']);
                          }else{
                            return response(['status' => false, 'stockError' => true, 'data' => '', 'message' => 'Availabile stock: '.$availableStock,'stock' => $availableStock]);
                          }
                       }
                        $totalPrice = 0;
                        foreach(Cart::instance('shoppingcart')->content() as $row){
                          $totalPrice = $totalPrice + $row->price * $row->qty;
                        }
                        $itemCount = Cart::instance('shoppingcart')->content()->count();
                    }
                  }
                }
                  $cartSubTotal= 0;
                  foreach(Cart::instance('shoppingcart')->content() as $row){
                      $cartSubTotal = $cartSubTotal + $row->price * $row->qty;
                  }
                  $cartItemCount = Cart::instance('shoppingcart')->content()->count();
                  return response(['status' => true,'data' => '','message' =>config('constants.ITEM_SUCCESS_CART'),'cartSubTotal'=>number_format($cartSubTotal, 2, '.', ',') ,'cartItemCount'=>$cartItemCount]);
            } catch (\Exception $ex) {
                return response(['status' => false,  'line'=>$ex->getLine(), 'message' => $ex->getMessage(),'data'=>$productDetails]);
            }
        }
        public function addToCart(Request $request){ 
            
            $productDetails ='';
            try {
                $data               = $request->all();
                $productId          = $request->productId;
                $distributorName = !empty($request->distributorName)?$request->distributorName:'N/A';
                $manufacturer       = $request->manufacturer;
                $sourcePartId       = $request->sourcePartId;
                $quantity           = $request->quantity;
                $dateCode           = $request->dateCode;
                $customerPartNo     = $request->customerPartNo;
                $stockAvailability  = $request->stockAvailability;
                $sourceMinQuantity  = $request->sourceMinQty;
                $price              = 0;
                $resaleList         = array();
                $availableData      = array();
                $dateCode           = '';
                $manufacturerCode   = $request->mfrCd;
                $availableStock     = 0;
                $minQty             = 0;
                /* Check item is exist on cart or not  */
                $itemExist =  Cart::instance('shoppingcart')->content()->where('id', $productId)->where('options.sourcePartId', $sourcePartId);
                if(empty(json_decode($itemExist))){
                    $price = 0;
                    $resaleList = array();
                    /* Price calculation on the bases of Qty */
                    $productDetails = Session::get('productDetails');
                    foreach ($productDetails as $key => $productDetail) {
                        foreach ($productDetail['sources'] as $sourcesKey => $sources) {
                            foreach ($sources['sourcesPart'] as $sourcePartKey => $sourcesPart) {
                                if($sourcesPart['sourcePartId'] == $sourcePartId){
                                    $dateCode = $sourcesPart['dateCode'];
                                     if($sourcesPart['inStock']){
                                        $availableStock = $sourcesPart['Availability'][0]['fohQty'];
                                    }else{
                                        $availableStock = 0;
                                   }
                                    $availableData = $sourcesPart['Availability'];
                                   foreach ($sourcesPart['Prices']['resaleList'] as $key => $prices) {
                                        if($quantity >= $prices['minQty'] && $quantity <= $prices['maxQty'] ){
                                            $price = $prices['price'];
                                            $resaleList = $sourcesPart['Prices']['resaleList'];
                                            break;
                                       }
                                   }
                                   $minQty = $sourcesPart['Prices']['resaleList'][0]['minQty'];
                                }
                            }
                        }
                    }

                     /* Check mim quantity of items */
                     if($quantity < $minQty){
                       return response(['status' => false,  'line'=>1, 'minQtyError' => true, 'data' => '', 'message' => 'Minimum order qty for part number '.$productId.' is '.$minQty.', We request you to check Buying Options first','minQty' => $minQty]);
                     }
                     /* Check available stock of items */
                     $originalQuantity = $quantity;
                     if($quantity > $availableStock){
                      $quantity = $availableStock;
                    }

                    if($quantity <= $availableStock){
                      Cart::instance('shoppingcart')->add([
                        ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId, 'sourceMinQuantity' =>$sourceMinQuantity,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,'original_quantity' => $originalQuantity,'quotedItem'=>0,'isQuoteApprovedItem'=>0,'quoteItemId'=>0,'quoteId'=>0, 'distributorName' => $distributorName]]
                      ]);
                    }else{
                      if($availableStock == 0){
                        return response(['status' => false, 'line'=>2, 'data' => '', 'message' => 'Item is out of stock.']);
                      }else{
                        return response(['status' => false, 'line'=>3, 'stockError' => true, 'data' => '', 'message' => 'Availabile stock: '.$availableStock,'stock' => $availableStock]);
                      }
                    }
                    $totalPrice = 0;
                    foreach(Cart::instance('shoppingcart')->content() as $row){
                        $totalPrice = $totalPrice + $row->price * $row->qty;
                    }
                    $itemCount = Cart::instance('shoppingcart')->content()->count();
                    return response(['status' => true,'data' => '','itemPrice' => number_format($price, 3, '.', ''),'totalPrice'=> $totalPrice,'itemCount'=>$itemCount,'message' =>config('constants.ITEM_SUCCESS_CART'),'availableData'=>$availableData ,'productDetails'=>$productDetails]);
                }else{
                    /* Item Update in the cart */
                    foreach ($itemExist as $index => $data) {
                        $rowId = $data->rowId;
                        $resaleList = $data->options->resaleList;
                        $dateCode = $data->options->dateCode;
                        $availableData = $data->options->availableData;
                        $availableStock = $availableData[0]['fohQty'];
                        $sourceMinQuantity = $data->options->sourceMinQuantity;
                    }
                    foreach ($resaleList as $key => $prices) {
                        if($quantity >= $prices['minQty'] && $quantity <= $prices['maxQty'] ){
                            $price = $prices['price'];
                            break;
                        }
                    }
                   /* Check mim quantity of items */
                   $minQty = $resaleList[0]['minQty'];
                   if($quantity < $minQty){
                       return response(['status' => false, 'line'=>4, 'minQtyError' => true, 'data' => '', 'message' => 'Minimum order qty for part number '.$productId.' is '.$minQty.', We request you to check Buying Options first','minQty' => $minQty]);
                   }
                   /* Check available stock of items */
                   $originalQuantity = $quantity;
                   if($quantity > $availableStock){
                       $quantity = $availableStock;
                   }
                   if($quantity <= $availableStock){
                       Cart::instance('shoppingcart')->update($rowId, ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'sourceMinQuantity'=>$sourceMinQuantity,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,'original_quantity' => $originalQuantity,'quotedItem'=>0,'isQuoteApprovedItem'=>0,'quoteItemId'=>0,'quoteId'=>0, 'distributorName' => $distributorName]]);
                   }else{
                       if($availableStock == 0){
                           return response(['status' => false, 'line'=>5,'data' => '', 'message' => 'Item is out of stock.']);
                       }else{
                           return response(['status' => false,  'line'=>6,'stockError' => true, 'data' => '', 'message' => 'Availabile stock: '.$availableStock,'stock' => $availableStock]);
                       }
                   }
                    $totalPrice = 0;
                    foreach(Cart::instance('shoppingcart')->content() as $row){
                      $totalPrice = $totalPrice + $row->price * $row->qty;
                    }
                    $itemCount = Cart::instance('shoppingcart')->content()->count();
                    return response(['status' => true, 'data' => '', 'message' => 'Item updated successfully in the cart','itemPrice' => number_format($price,3),'totalPrice'=> $totalPrice,'itemCount' => $itemCount,'availableData '=> $availableData ]);
                }
            } catch (\Exception $ex) {
                return response(['status' => false, 'line'=>361,'data'=> config('constants.COMMON_ERROR')]);
            }
        }
        
        public function getCart(){
            return view('frontend.cart.cart');
        }
        public function removeCartItem(Request $request){
            $data         = $request->all();
            $productId    = $request->product_id;
            Cart::instance('shoppingcart')->remove($productId);
            return response(['status' => true, 'data' => $data]);
        }
        public function emptyCart(Request $request){
            Cart::instance('shoppingcart')->destroy();
            return response(['status' => true, 'data' => '']);
        }
    }
