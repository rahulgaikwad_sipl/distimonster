<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 26/03/18
 * Time: 10:49 AM
 */

namespace App\Http\Controllers\Frontend;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Bom;
use Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Rap2hpoutre\FastExcel\FastExcel;
use Helpers; // Important
use App\Mail\BomSubmissionGuestMail;
use App\Mail\BomSubmissionAdminMail;
use App\Mail\BomSharedtMail;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Jobs\SendAdminBomUploadMailJob;
use App\Jobs\SendGuestUserBomUploadMailJob;
use Carbon\Carbon;
class BomController extends Controller{
    protected $userSessionData;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(){
       Event::listen(Authenticated::class, function ($event) {
            $this->userSessionData = $event->user;
            if ($this->userSessionData['status'] == 0) {
                 Helpers::sessionFlush();
            } 
        });
    }
    
    public function bom(Request $request){
        $data['validationMessage'] = Bom::$addBomValidationMessages;
        if(Auth::user()){
            $bomList =   Bom::getBomListByUserId(Auth::user()->id);
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($bomList->toArray());
            // Define how many items we want to be visible in each page
            $perPage = 8;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['bomList']   =  $paginatedItems;
            
            //Bom Login Code Start
            if(!empty($request->bom_id)){
                $bomPartList =   Bom::getBomById($request->bom_id);
                $cleanPartsArray = array();
                $finalArray = array();
                foreach ($bomPartList as $dataString) {
                    if(!empty(str_replace(array("\n\r", "\n", "\r"), '', $dataString->bom_details))) {
                        if (strpos($dataString->bom_details, ',') !== false) {
                            $explodedString                 = explode( ',', $dataString->bom_details);
                            $finalArray['partNum']          = trim($explodedString[0]);
                            $finalArray['QTY']              = trim($explodedString[1]);
                            $finalArray['manufacturer']     =  '';
                            $cleanPartsArray [] = $finalArray;
                        }else{
                            return redirect()->to('bom')->with('failure', 'Parts are not found in proper format to upload Monster-BOM.');
                        }
                    }
                }
                if(count($cleanPartsArray)> 5){
                    return redirect()->to('bom')->with('failure', 'Only 5 part number is accepted, upload csv or xlsx for more parts.');
                }else{
                    $resultArray   =   $this->searchGetBomParts($cleanPartsArray);
                    array_walk_recursive($resultArray, 'Helpers::update_price');
                    $unmantchedParts = array();
                    $resultedParts = array();
                    $finalPartsFoundArray       = array();
                    foreach ($resultArray as $part){
                        $partsFoundArray['partNum'] = $resultedParts[] =$part['partNum'];
                        $partsFoundArray['QTY'] = 1;
                        $partsFoundArray['manufacturer'] = '';
                        $finalPartsFoundArray[] = $partsFoundArray;
                    }
                    /* Create array of part which not found in seached */
                    foreach ($cleanPartsArray as $key=>$value){
                        if(!in_array($value['partNum'], $resultedParts)){
                            $tempArray = array();
                            $tempArray['partNum']   = trim($value['partNum']);
                            $tempArray['QTY']             = trim($value['QTY']);
                            $tempArray['manufacturer']    =  '';
                            $unmantchedParts[] = $tempArray;
                        }
                    }

                    $combinedArray = array_replace_recursive($finalPartsFoundArray , $cleanPartsArray);
                    $data['noPartFound']        = $unmantchedParts;
                    $data['finalData']          = $resultArray;
                    $bomPartDetailArray = array();
                    foreach ($combinedArray as $key=>$value){
                        $manufacturer ="";
                        $description ="";
                        $availabilityQty=0;
                        $inStockQty= 0;
                        foreach ($resultArray as $key1=>$newValue){
                            if(isset($newValue['partNum'] )){
                                if($newValue['partNum'] == $value['partNum']){
                                    if(!empty($newValue['sources'])){
                                        foreach($newValue['sources'] as $sources){
                                            foreach($sources['sourcesPart'] as $sellerInfo){
                                                if($sellerInfo['inStock']){
                                                    $availabilityQty =  $availabilityQty+$sellerInfo['Availability'][0]['fohQty'];
                                                }
                                            }
                                        }
                                    }
                                    $manufacturer   = $newValue['manufacturer'] ;
                                    $inStockQty     = $availabilityQty ;
                                    $description    = $newValue['desc'];

                                    break;
                                }else{
                                    $manufacturer    =  '';
                                    $inStockQty      =  0;
                                    $description     =  "";
                                }
                            }
                        }
                    }
                    $data['bom_id'] =  $request->bom_id;
                    return view('frontend.products.listing',$data);
                }
            }
            //Bom Login Code End
        }else{
            $data['bomList']  = [];
            return redirect()->to('login')->with('failure', config('constants.BOM_UPLOAD_LOGIN_ERROR'));
        }

        return view('frontend.bom.bom',$data);
    }
    
    public function uploadBom(Request $request){
        $data       = $request->all();
        $bomFile    = $request->file('bom_file');

        $validator = \Validator::make($data, Bom::sendBomValidationRules(), Bom::$addBomValidationMessages);
        if ($validator->fails()) {
            return redirect('bom')->withErrors($validator)->withInput();
        }else{
            $bom = new Bom();
            $bomFilePath = config('app.resource_paths.bom_files');
            if (empty($request->input('bom_details')) &&  !$request->hasFile('bom_file')) {
                return redirect()->to('bom')->with('failure', 'Please use any one field to upload Monster-BOM.');
            }
            if ($request->input('bom_details') && $request->hasFile('bom_file')) {
                return redirect()->to('bom')->with('failure', 'Please use only one method to upload Monster-BOM.');
            }else{
                if ($request->has('user_id') && $request->input('user_id') != '') {
                    $bom->user_id = $request->input('user_id');
                }
                $bom->full_name         = preg_replace('!\s+!', ' ',$request->input('full_name'));
                $bom->company_name      = $request->input('company_name');
                $bom->email             = $request->input('email');
                $bom->contact_no        = $request->input('contact_no');
                $bom->address           = $request->input('address');
                $bom->bom_details       = $request->input('bom_details');
                $user['full_name']      = preg_replace('!\s+!', ' ',$request->input('full_name'));
                $user['company_name']   = $request->input('company_name');
                $user['email']          = $request->input('email');
                $user['contact_no']     = $request->input('contact_no');
                $user['address']        = $request->input('address');
                $user['note']           = $request->input('note');
                if ($request->input('bom_details')) {
                    $bomPartList = explode("\n",$request->input('bom_details'));
                    $cleanPartsArray = array();
                    $finalArray = array();
                    foreach ($bomPartList as $dataString) {
                        if(!empty(str_replace(array("\n\r", "\n", "\r"), '', $dataString))) {
                            if (strpos($dataString, ',') !== false) {
                                $explodedString                 = explode( ',', $dataString );
                                
                                $finalArray['partNum']          = trim($explodedString[0]);
                                $finalArray['QTY']              = trim($explodedString[1]);
                                $finalArray['manufacturer']     =  '';
                                $cleanPartsArray [] = $finalArray;
                            }else{
                                return redirect()->to('bom')->with('failure', 'Parts are not found in proper format to upload Monster-BOM.');
                            }
                        }
                    }
                    if(count($cleanPartsArray)> 5){
                        return redirect()->to('bom')->with('failure', 'Only 5 part number is accepted, upload csv or xlsx for more parts.');
                    }else{
                        $resultArray   =   $this->searchGetBomParts($cleanPartsArray);

                        array_walk_recursive($resultArray, 'Helpers::update_price');
                        $unmantchedParts = array();
                        $resultedParts = array();
                        $finalPartsFoundArray       = array();
                        
                        $bestPrice = [];
                        foreach ($resultArray as $part){
							
							if(isset($part['partNum'])){
								$partsFoundArray['partNum']       =  $resultedParts[]   =  $part['partNum'];
								$partsFoundArray['QTY']             =  $part['requestedQty'];
								$partsFoundArray['manufacturer']    =  '';
								$finalPartsFoundArray[]             = $partsFoundArray;
							}else{
								$bestPrice[] = $part;
							}
						
                        }
                        /* Create array of part which not found in seached */
                        foreach ($cleanPartsArray as $key=>$value){
                            if(!in_array($value['partNum'], $resultedParts)){
                                $tempArray = array();
                                $tempArray['partNum']   = trim($value['partNum']);
                                $tempArray['QTY']             = trim($value['QTY']);
                                $tempArray['manufacturer']    =  '';
                                $unmantchedParts[] = $tempArray;
                            }
                        }

                        $combinedArray = array_replace_recursive($finalPartsFoundArray , $cleanPartsArray);

                        $data['noPartFound']        = $unmantchedParts;
                        $data['finalData']          = $resultArray;
                        $data['bestPrice']          = $bestPrice; 
                        $bomPartDetailArray = array();
                        foreach ($combinedArray as $key=>$value){
                            $manufacturer ="";
                            $description ="";
                            $availabilityQty=0;
                            $inStockQty= 0;
                            foreach ($resultArray as $key1=>$newValue){
                                if(isset($newValue['partNum'] )){
                                    if($newValue['partNum'] == $value['partNum']){
                                        if(!empty($newValue['sources'])){
                                            foreach($newValue['sources'] as $sources){
                                                foreach($sources['sourcesPart'] as $sellerInfo){
                                                    if($sellerInfo['inStock']){
                                                        $availabilityQty =  $availabilityQty+$sellerInfo['Availability'][0]['fohQty'];
                                                    }
                                                }
                                            }
                                        }
                                        $manufacturer   = $newValue['manufacturer'] ;
                                        $inStockQty     = $availabilityQty ;
                                        $description    = $newValue['desc'];

                                        break;
                                    }else{
                                        $manufacturer    =  '';
                                        $inStockQty      =  0;
                                        $description     =  "";
                                    }
                                }
                            }
                            $bomPartDetailArray[$key] = array('manufacturer'=>$manufacturer, 'partNum'=>$value['partNum'], 'QTY'=>$value['QTY'],'instock'=>$inStockQty,'description'=>$description);
                        }
                        $bom->bom_parts_count       = count($bomPartDetailArray);
                        $bom->bom_found_parts       = json_encode($bomPartDetailArray);
                        $bom->bom_type = 0;
                        $bom->note = $request->input('note');
                        //Save bom details
                        if ($bom->save()) {
                            //Check if login user is submitting bom
                            if ($request->has('user_id') && $request->input('user_id') != '') {
                            }else{
                                //Bom mail to admin
                                \Mail::to(config('constants.ADMIN_MAIL'))->send(new BomSubmissionAdminMail($user, 'New Monster-BOM request submitted from a user'));
                                //send mail to guest  if user is not logged in
                                \Mail::to($request->input('email'))->send(new BomSubmissionGuestMail($user, 'Monster-BOM successfully submitted to DistiMonster'));
                            }
                            $data['bom_id'] =  $bom->id;
                            return view('frontend.products.listing',$data);
                        }else {
                            return redirect()->to('bom')->with('failure', config('constants.COMMON_ERROR'));
                        }
                    }
                }elseif ($request->hasFile('bom_file')){
                    if ($bomFile) {
                        $fileName       = $bomFile->getClientOriginalName();
                        $ext            = pathinfo($fileName, PATHINFO_EXTENSION);
                        $uploadedFile   = $bomFile->getRealPath();
                        $randomFileName = User::generateRandomString();
                        if ($ext == 'csv' || $ext == 'xlsx') {
                            $fileName = $randomFileName . "_" . $fileName;
                            //Move uploaded file to the directory.
                            $uploaded       = move_uploaded_file($uploadedFile, $bomFilePath . '/' . $fileName);
                            $bom->bom_file  = $bomFilePath . $fileName;
                            $bom->file_url  = config('app.resource_paths.bom_files_path') . $fileName;
                            $recordObj      = (new FastExcel)->withoutHeaders(true)->import(public_path(config('app.resource_paths.bom_files_path').$fileName));
                            $dataArray      = $recordObj->toArray();
                            $cleanPartsArray2  = array();
                            $finalArray2 = array();
                            
                            foreach ($dataArray as $key=>$value){
                                if(!empty($value[0]) && !empty($value[1])){
                                    $finalArray2['partNum']   = strtoupper(trim($value[0]));
                                    $finalArray2['QTY']       = !empty($value[1]) ? trim($value[1]) : '';
                                    $finalArray2['internalPart'] = !empty($value[2]) ? trim($value[2]) : '';
                                    $finalArray2['manufacturer']    =  '';
                                    $cleanPartsArray2 [] = $finalArray2;
                                }
                                    
                            }

                            unset($cleanPartsArray2[0]);
							$cleanPartsArray2 = array_values($cleanPartsArray2);
                            
                            /* Search multiple prat from api*/
                            $resultArray2   =   $this->searchGetBomParts($cleanPartsArray2);

                            /* update unit price of each part in source*/
                            array_walk_recursive($resultArray2, 'Helpers::update_price');
                            $finalPartsFoundArray       = array();
                            $unmantchedParts = array();
                            $resultedParts = array();
                            $bestPrice = [];
                            $i=0; 
                            foreach ($resultArray2 as $part){
                                if(isset($part['partNum'])){
                                    $partsFoundArray['partNum']         =  $resultedParts[]  =  $part['partNum'];
                                    $partsFoundArray['QTY']             =  $part['requestedQty'];
                                    $partsFoundArray['internalPart']    =  '';
                                    $partsFoundArray['manufacturer']    =  '';
                                    $finalPartsFoundArray[]             = $partsFoundArray;
                                    foreach ($cleanPartsArray2 as $key=>$value){
                                        if($value['partNum'] == $part['partNum']){
                                            $resultArray2[$i]['internalPart'] = $value['internalPart'];
                                        }
                                    }    
                                    $i++;
                                }else{
								    $bestPrice[] = $part;
							    }   
                            }
                            /* Create array of part which not found in seached */
                            foreach ($cleanPartsArray2 as $key=>$value){
                                if(!in_array($value['partNum'], $resultedParts)){
                                    $tempArray                      = array();
                                    $tempArray['partNum']           = trim($value['partNum']);
                                    $tempArray['QTY']               = trim($value['QTY']);
                                    $tempArray['internalPart']      = trim($value['internalPart']);
                                    $tempArray['manufacturer']      =  '';
                                    $unmantchedParts[]              = $tempArray;
                                }
                            }
                            
                            $combinedArray = array_replace_recursive($finalPartsFoundArray , $cleanPartsArray2);
                            $data['noPartFound']        = $unmantchedParts;
                            $data['finalData']          = $resultArray2;
                            $bomPartDetailArray = array();
                            foreach ($combinedArray as $key=>$value){
                                $manufacturer ="";
                                $description ="";
                                $availabilityQty=0;
                                $inStockQty= 0;
                                foreach ($resultArray2 as $key1=>$newValue){
                                    if(isset($newValue['partNum'] )){
                                        if($newValue['partNum'] == $value['partNum']){
                                            if(!empty($newValue['sources'])){
                                                foreach($newValue['sources'] as $sources){
                                                    foreach($sources['sourcesPart'] as $sellerInfo){
                                                        if($sellerInfo['inStock']){
                                                            $availabilityQty =  $availabilityQty+$sellerInfo['Availability'][0]['fohQty'];
                                                        }
                                                    }
                                                }
                                            }
                                            $manufacturer   = $newValue['manufacturer'] ;
                                            $inStockQty     = $availabilityQty ;
                                            $description    = $newValue['desc'];

                                            break;
                                        }else{
                                            $manufacturer    =  '';
                                            $inStockQty      =  0;
                                            $description     =  "";
                                        }
                                    }
                                }
                                $bomPartDetailArray[$key] = array('manufacturer'=>$manufacturer, 'partNum'=>$value['partNum'], 'QTY'=>$value['QTY'], 'internalPart'=>$value['internalPart'], 'instock'=>$inStockQty,'description'=>$description);
                            }
                            $data['allPartArray']           =  $cleanPartsArray2;
                            $bom->bom_parts_count           =  count($data['allPartArray']);
                            $bom->bom_found_parts           =  json_encode($data['allPartArray']);
                            $bom->bom_type = 0;
                            $bom->note = $request->input('note');
                            //Save bom details
                            if ($bom->save()) {
                                //Check if login user is submitting bom
                                if ($request->has('user_id') && $request->input('user_id') != '') {
                                }else{
                                    //Bom mail to admin
                                    \Mail::to(config('constants.ADMIN_MAIL'))->send(new BomSubmissionAdminMail($user, 'New Monster-BOM request submitted from a user'));
                                    //send mail to guest  if user is not logged in
                                    $data['email'] =  $request->input('email');
                                    \Mail::to($request->input('email'))->send(new BomSubmissionGuestMail($user, 'DistiMonster: Monster-BOM submitted successfully.'));
                                }
                                $data['bom_id'] =  $bom->id;
                                return view('frontend.products.listing',$data);
                            } else {
                                return redirect()->to('bom')->with('failure', config('constants.COMMON_ERROR'));
                            }
                        } else {
                            return redirect()->to('bom')->with('failure', 'Only CSV/Xlsx file formats are supported.');
                        }
                    }
                }

            }
        }
    }
    
    private function _seachItemByPartName($parts, $partNum){
        $quantity  = 0;
        if(count($parts) > 0){
            foreach($parts as $val){
                if($quantity == 0 && $val["partNum"] == $partNum){
                    $quantity = $val["QTY"];
                }
            }
        }
        return $quantity;
    }

    public function removeBomById(Request $request){
        try {
            $data         = $request->all();
            $bomId         = $request->input('bomId');
            //Find details by id
            $bom             = Bom::findOrFail($bomId);
            $bom->deleted_at    =  date('Y-m-d h:i:s');
            if($bom->save()) {
                return redirect()->to('bom')->with('success', 'Monster-BOM deleted successfully.');
            } else {
                return redirect()->to('bom')->with('error', config('constants.COMMON_ERROR'));
            }
        } catch (\Exception $ex) {
            return redirect()->to('bom')->with('error', config('constants.COMMON_ERROR'));
        }
    }
    
    public function addAllBomToQuote(Request $request){
        try {
            $data                           = $request->all();
            $allBomPartArray                  = $request->allBomPartArray;
            if(empty($allBomPartArray)){
                return response(['status' => false,  'message' => 'Sorry no part found in Monster-BOM.']);
            }else{
                foreach ($allBomPartArray as $key=>$parts){
                    $itemExist                  =  Cart::instance('quotecart')->content()->where('id', $parts['productId'])->where('options.sourcePartId', $parts['sourcePartId']);
                    $price = 0;
                    $resaleList                 = array();
                    $availableData              = json_decode($parts['availableData']);
                    $resaleListDataRequestArray = json_decode($parts['resaleListData']);
                    $quantity                   = $parts['quantity'];
                    $productId                  = $parts['productId'];
                    $manufacturer               = $parts['manufacturer'];
                    $sourcePartId               = $parts['sourcePartId'];
                    $distributorName            = $parts['distributorName'];
                    $dateCode                   = $parts['dateCode'];
                    $manufacturerCode           = $parts['mfrCd'];
                    $customerPartNo             = $parts['customerPartNo'];
                    $stockAvailability          = $parts['stockAvailability'];
                    $requestedQuantity          = $parts['requestedQuantity'];
                    $requestedCost              = $parts['requestedCost'];
                    $requestedDeliveryDate      = '';

                    if(empty(json_decode($itemExist))) {
                        if(!empty($resaleListDataRequestArray)){
                            foreach ($resaleListDataRequestArray as $key => $prices) {
                                if($quantity >= $prices->minQty && $quantity <= $prices->maxQty ){
                                    $price = property_exists($prices,'price')?$prices->price:$price;
                                    $resaleList = $resaleListDataRequestArray;
                                    break;
                                }
                            }
                            $minQty = $resaleListDataRequestArray[0]->minQty;
                            if($quantity < $minQty){
                                $quantity  =  $minQty;
                            }
                        }
                        Cart::instance('quotecart')->add([
                            ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,
                                'sourcePartId'=>$sourcePartId,
                                'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,
                                'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,
                                'requestedQuantity'=>$requestedQuantity,'requestedCost' => $requestedCost, 'requestedDeliveryDate'  => $requestedDeliveryDate, 'distributorName' => $distributorName]]
                        ]);
                    }else{
                        foreach ($itemExist as $index => $data) {
                            $rowId = $data->rowId;
                            $resaleList = $data->options->resaleList;
                            $dateCode = $data->options->dateCode;
                            $availableData = $data->options->availableData;
                        }
                        foreach ($resaleList as $key => $prices) {
                            if($quantity >= $prices->minQty && $quantity <= $prices->maxQty ){
                                $price = property_exists($prices,'price')?$prices->price:$price;
                                break;
                            }
                        }
                        Cart::instance('quotecart')->update($rowId,
                            ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,'requestedQuantity'=>$requestedQuantity,'requestedCost' => $requestedCost, 'requestedDeliveryDate'  => $requestedDeliveryDate, 'distributorName' => $distributorName]]
                        );
                    }
                }
               $quoteItemCount =  Cart::instance('quotecart')->content()->count();
                return response(['status' => true, 'quoteItemCount'=>$quoteItemCount,  'message' => 'Successfully added parts in quote.']);
            }
        }catch (\Exception $ex) {
            return response(['status' => false, 'data' => $ex->getMessage().$ex->getLine()]);
        }
    }
    
    public function myBom(Request $request){
        try{
           $bomList = Bom::getBomListByUserId(Auth::user()->id);
           $subUser = Bom::getSubUserId(Auth::user()->id);
           // Get current page form url e.x. &page=1
           $currentPage = LengthAwarePaginator::resolveCurrentPage();
           // Create a new Laravel collection from the array data
           $itemCollection = collect($bomList->toArray());
           // Define how many items we want to be visible in each page
           $perPage = 4;
           // Slice the collection to get the items to display in current page
           $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
           // Create our paginator and pass it to the view
           $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
           // set url path for generted links
           $paginatedItems->setPath($request->url());
           $data['subUser']   =  $subUser;
           $data['bomList']   =  $paginatedItems;
           $data['title']     =  'Monster-BOM';
           $data['noBomMsg']  =  'Currently there are no Monster-BOMs.';
           return view('frontend.pages.my-bom',$data);
       }catch (\Exception  $ex){
            return redirect()->to('my-bom')->with('error', config('constants.COMMON_ERROR'));
       }
    }

    public function savedBom(Request $request){
        try{
            $bomList = Bom::getBomListByUserIdAndBomType(Auth::user()->id, 'saved');
            $subUser = Bom::getSubUserId(Auth::user()->id);
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($bomList->toArray());
            // Define how many items we want to be visible in each page
            $perPage = 4;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['subUser']   =  $subUser;
            $data['bomList']   =  $paginatedItems;
            $data['title']     =  'Saved Monster-BOM';
            $data['noBomMsg']  =  'Currently there are no Saved Monster-BOMs.';
            return view('frontend.pages.my-bom',$data);
        }catch (\Exception  $ex){
            return redirect()->to('my-bom')->with('error', config('constants.COMMON_ERROR'));
        }
    }

    public function sharedBom(Request $request){
        try{
				
            $bomList = Bom::getBomListByUserIdAndBomType(Auth::user()->id, 'shared');
		    $subUser = Bom::getSubUserId(Auth::user()->id);
			// Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($bomList->toArray());
            // Define how many items we want to be visible in each page
            $perPage = 4;
		
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['subUser']   =  $subUser;
            $data['bomList']   =  $paginatedItems;
            $data['title']     =  'Shared Monster-BOM';
            $data['noBomMsg']  =  'Currently there are no Shared Monster-BOMs.';
			
            return view('frontend.pages.my-bom',$data);
        }catch (\Exception  $ex){
			return redirect()->to('my-bom')->with('error', config('constants.COMMON_ERROR'));
        }
    }

    public function orderedBom(Request $request){
        try{
            $bomList = Bom::getBomListByUserIdAndBomType(Auth::user()->id, 'ordered');
            $subUser = Bom::getSubUserId(Auth::user()->id);
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($bomList->toArray());
            // Define how many items we want to be visible in each page
            $perPage = 4;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['subUser']   =  $subUser;
            $data['bomList']   =  $paginatedItems;
            $data['title']     =  'Ordered Monster-BOM';
            $data['noBomMsg']  =  'Currently there are no Ordered Monster-BOMs.';
            return view('frontend.pages.my-bom',$data);
        }catch (\Exception  $ex){
            return redirect()->to('my-bom')->with('error', config('constants.COMMON_ERROR'));
        }
    }

    public function shareBom(Request $request){ 
        try{
            if(Auth::user()){ 
                $sharedBy = Auth::user()->name .' '. Auth::user()->last_name;
                $sharedById = isset(Auth::user()->id) ? Auth::user()->id : '';
                $share_user = implode(',', $request->user_name);
                $result = \DB::table('boms')->where('id', $request->bom_id_hidden)->update(['shared_user_id' => $share_user]);
                $bomNote = $request->bom_note; 
                for($i=0;$i<count($request->user_name);$i++){  
                    $bomData = array();
                    $bomData['nameTo'] = $request->name_hidden[$i];
                    $bomData['emailTo'] = $request->email_hidden[$i];
                    $bomData['sharedBy'] = $sharedBy;
                    $bomData['bomNote'] = $bomNote; 
                    
                    $selectedUser = \DB::table('users')
                     ->select('name', 'last_name', 'email')
                      ->where('id', $request->user_name[$i])
                      ->get(); 
                    if(count($selectedUser) > 0) {  
                        $bomData['nameTo'] = $selectedUser[0]->name." ".$selectedUser[0]->last_name;
                        
                        \DB::table('shared_notes_history')->insert(['bom_id' => $request->bom_id_hidden, 'shared_by' => $sharedById, 'shared_with' => $request->user_name[$i], 'note' => $bomNote, 'created_at' => date('Y-m-d H:i:s')]);
                        //Bom mail to admin
                        \Mail::to($selectedUser[0]->email)->send(new BomSharedtMail($bomData, 'Distimonster - New BOM Shared'));
                    }    
                }  
                return response(['status' => true, 'message'=> 'Monster-BOM shared successfully.','data' =>'']);
            }
        }catch (\Exception  $ex){
             return response(['status' => false, 'message'=> 'Sorry, unable to share Monster-BOM with selected user.', 'data' =>'']);
        }
    }

    public function downloadBom(Request $request,$bomId){
        $bom = Bom::findOrFail($bomId); 
        if(!empty($bom)){ 
            $bomList = json_decode($bom->bom_found_parts);
            $list = [];
            if(is_array($bomList) && count($bomList)){ 
                foreach ($bomList as $key=>$parts){
                    $list[$key]['Part Number'] = $parts->partNum;
                    $list[$key]['Requested Qty'] = (isset($parts->QTY))?$parts->QTY:0;
                    $list[$key]['Manufacturer'] = (isset($parts->manufacturer))?$parts->manufacturer:'';
                    $list[$key]['Total Stock'] = (isset($parts->instock))?"$parts->instock":0;
                    $list[$key]['Description'] = (isset($parts->description))?$parts->description:'';

                } 
            } 
            $list = collect($list); 
            (new FastExcel($list))->download('bom'.$bom->id.'.xlsx'); 

        }else{
            return redirect()->to('bom')->with('error', config('constants.COMMON_ERROR'));
        }
    }
    
    public function bomDetails(Request $request, $bomId){
        try {
            if(Auth::user()){
                $bomInfo= BOM::getBomInfoById($bomId , Auth::user()->id);
                if($bomInfo->isEmpty()){
                    return redirect()->to('quotes')->with('failure',config('constants.BOM_NOT_EXIST'));
                }else{
                    $bomDetailsArray = json_decode(json_encode($bomInfo), true);
                    $partsArray = json_decode($bomDetailsArray[0]['bom_found_parts']);
                    $partArray = array();
                    $searchedPartArray  = array();
                   foreach ($partsArray as $key=>$parts) {
                       $partArray['partNum'] = trim($parts->partNum);
                       $partArray['QTY'] = (isset($parts->QTY)) ? $parts->QTY : 0;
                       $partArray['internalPart'] = $parts->internalPart;
                       $searchedPartArray[] = $partArray;
                   }
                    $resultArray2   =   $this->searchGetBomParts($searchedPartArray);
                    $resultedParts = array();
					foreach($resultArray2 as $part){
						$resultedParts[] = $part['partNum'];
					}
					$unmantchedParts = array();
				    /* Create array of part which not found in seached */
						foreach ($partsArray as $key=>$parts){
							if(!in_array($parts->partNum, $resultedParts)){
								$tempArray = array();
								$tempArray['partNum']   = trim($parts->partNum);
								$tempArray['QTY']       = trim($parts->QTY);
								$tempArray['internalPart'] = trim($parts->internalPart);
								$tempArray['manufacturer'] =  '';
								$unmantchedParts[] = $tempArray;
							}
                        }
                    
                    $bestPrice = [];
                            $i=0; 
                            foreach ($resultArray2 as $part){
                                if(isset($part['partNum'])){
                                    foreach ($partsArray as $key=>$value){
                                        if($value->partNum == $part['partNum']){
                                            $resultArray2[$i]['internalPart'] = $value->internalPart;
                                        }
                                    }    
                                    $i++;
                                }else{
								    $bestPrice[] = $part;
							    }   
                            }    
                       
				    $data['noPartFound']        = $unmantchedParts;
                    array_walk_recursive($resultArray2, 'Helpers::update_price');
                    $data['finalData']          = $resultArray2;
                    $data['bom_id'] =  $bomId;
                    $data['bom_name'] =  $bomDetailsArray[0]['bom_name'];
                    return view('frontend.products.listing',$data);
                }
            }else{
                return redirect()->to('/')->with('failure', config('constants.COMMON_ERROR'));
            }

        }catch (\Exception $ex) {
            return redirect()->to('my-bom')->with('failure', config('constants.COMMON_ERROR'));
        }
    }
    
    public function searchGetBomParts($partNumberArray){
        $newPartsArray = array();
        $partfinalArray = array();
        foreach ($partNumberArray as $part) {
            $partfinalArray ['partNum'] =  $part['partNum'];
            $partfinalArray ['QTY'] =  urlencode($part['QTY']);
            $newPartsArray [] = $partfinalArray;
        }

        $finalArray   = json_encode($newPartsArray);
        
        $orweaverData = $this->searchProductOrbweaver($finalArray);
        $finalProductData = array();
        if(!empty($orweaverData)){
            foreach($orweaverData as $orwea){
                foreach($orwea as $orw){
                    $finalProductData[] = $orw;
                }
            }
        }
        return $finalProductData;
    }
    
    public function searchProductOrbweaver($partNumber){
        $partNumber = json_decode($partNumber);
        $finalProductDataFinal = [];
        foreach($partNumber as $part){
            $getApiData = array();
            $getApiData = Helpers::searchProductOrbweaver($part->partNum,'','',$part->QTY);
            if(!empty($getApiData)){
                $finalProductDataFinal[] = $getApiData;
            }
        }
        return $finalProductDataFinal;
    }
    
    public function getShareBomNotes($bomId) { 
        try{
            $bomInfo= BOM::getShareNotesBomId($bomId);    
            $html = '';
            if(count($bomInfo) > 0) {
                $html .= '<table cellspacing="0" cellpadding="0" border="0" class="buying-options-table">';
				$html .= '<tr><th>Shared By</th><th>Shared To</th><th>Notes</th><th>Time</th></tr>';
                $userType = Auth::user()->is_sub_user;
                if($userType == '0') {
                     foreach($bomInfo as $info) {
				        $html .= '<tr><td>'.Helpers::viewProfile($info->shared_by).'</td>';
						$html .= '<td>'.Helpers::viewProfile($info->shared_with).'</td>';
                        $html .= '<td>'.$info->note.'</td>';
                        $html .= '<td>'.date("m/d/Y H:i a", strtotime($info->created_at)).'</td></tr>';
                    }
                } else {
                    foreach($bomInfo as $info) {
						$html .= '<tr><td>'.Helpers::viewProfile($info->shared_by).'</td>';
						$html .= '<td>'.Helpers::viewProfile($info->shared_with).'</td>';
                        $html .= '<td>'.$info->note.'</td>';
                        $html .= '<td>'.date("m/d/Y H:i a", strtotime($info->created_at)).'</td></tr>';
                    }
                }
                $html .= '</table>';    
            } else {
                $html = 'not_found';
            }
                             
            return response(['status' => true, 'message'=> '', 'data' => $html]);           
        }catch (\Exception  $ex){ 
            return response(['status' => false, 'message'=> $ex->getMessage(), 'data' =>'']);
        }    
    }
    
    public function updateBomName(Request $request){ 
        try{
            if(Auth::user()){ 
                $bom_name =  $request->bom_name;
                $update_bom_id =  $request->update_bom_id;
                $result = \DB::table('boms')->where('id', $update_bom_id)->update(['bom_name' => $bom_name, 'bom_status' => 'published']);
                if($result){
                    return response(['status' => true, 'message'=> 'BOM saved successfully.','data' =>'']);
                }else{
                    return response(['status' => false, 'message'=> 'Monster-BOM shared successfully.','data' =>'']);
                }
            }
        }catch (\Exception  $ex){
             return response(['status' => false, 'message'=> 'Sorry, unable to save Monster-BOM.', 'data' =>'']);
        }
    }
}