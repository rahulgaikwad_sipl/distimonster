<?php

namespace App\Http\Controllers\Frontend;

use App\User;
use App\Mail\ForgotPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use App\Jobs\SendWelComeMailJob;
use App\Jobs\SendPasswordResetMailJob;
use Carbon\Carbon;
use App\Mail\SendWelComeMail;
use App\NewsLetterSubscriber;
use App\Mail\WelComeNewsLetterSubscriberMail;

class AuthController extends Controller {
    /**
     * Name: __construct
     * Purpose: Constructor for checking user auth.
     *
     * @param  \Illuminate\Http\Request  $request
     */

    private $_user;
    private $_response = array('status' => false, 'errors' => array());
    private $_request;

    public function __construct(Request $request) {
        $this->_request = $request;
    }

    /**
     * Name: login
     * Purpose: For show login page.
     *
     * @return \Illuminate\Http\Response
     */

    public function login(Request $request) {
        if (Auth::check()) {
            if (Auth::user()->role_id == 1) {
                return redirect('/admin/home');
            } else {
                return redirect('/dashboard');
            }
        }
        $data['title'] = 'Login';
        
        $redirectLogin = explode('/login', $_SERVER['REQUEST_URI']);
        $data['redirectLogin'] = '';
        if(!empty($redirectLogin[1])){
            $data['redirectLogin'] = $redirectLogin[1];
        }
        return view('frontend.auth.login', $data);
    }

    public static $loginValidationRules =[
        'password' => 'required',
        'email'    => 'required|email'
    ];

    /**
     * Name: userLogin
     * Purpose:  Authenticate user.
     * @return \Illuminate\Http\Response
     */

    public function userLogin(){ 
        $loginValidationMessage = array(
            'password.required' => config('constants.REQUIRED_PASSWORD'),
            'email.required' => config('constants.REQUIRED_EMAIL'),
            'email.email' => config('constants.VALID_EMAIL_ADDRESS'),
        );
        $validation = \Validator::make($this->_request->all(), self::$loginValidationRules, $loginValidationMessage);
        if ($validation->fails()) {
            return redirect()->to('/login')->withErrors($validation)->withInput();
        } else {
            //Check Subscription Expiry Start
            $email = Input::get('email');
            $trialExpiryCheck = User::checkUserTrialExpiry($email);
            //Check Subscription Expiry End
            if($trialExpiryCheck > 0){
                $checkPaymentStatus = User::checkPaymentStatus($email);
                if(empty($checkPaymentStatus)){
                    
                    return redirect()->intended('/login')->with('failure', config('constants.SUBSCRIPTION_EXPIRED'));
                    
                }else{
                    
                    $subscriptionExpiryCheck = User::checkUserSubscriptionExpiry($email);
                    if($subscriptionExpiryCheck>0){
                        return redirect()->intended('/login')->with('failure', config('constants.SUBSCRIPTION_EXPIRED'));
                    }
                }
            } 
            $rememberMe = false; 

            if (Input::get('remember_me')) {
                $rememberMe = true;
                $expiration_time = (int) (time() + (86400 * 30 * 30 * 12));
                setcookie('distim_uid', \Crypt::encrypt(Input::get('email')), $expiration_time, "/");
                setcookie('distim_crd', \Crypt::encrypt(Input::get('password')), $expiration_time, "/");
            } else { 
                setcookie("distim_uid", NULL, time() - 3600, "/");
                setcookie("distim_crd", NULL, time() - 3600, "/");
            }

            if(Auth::user()){ 
                $userId = Auth::user()->id;
                return redirect()->intended('/account-settings/'.$userId)->with('warning',  config('constants.ALREADY_LOGGEDIN'));
            }else{ 
                $isSearchRoute=  false;
                $url = '/dashboard';
                $redirect_uri = !empty(Input::get('redirect_uri'))?Input::get('redirect_uri'):'';
                if (!empty($redirect_uri)) {
                    $url = $redirect_uri;
                    $isSearchRoute = true;
                }
                
                if (strpos(Session::get('previousroute'), 'search-part') !== false) {
                    $url   =  Session::get('previousroute');
                    $isSearchRoute = true;
                }
                if (strpos(Session::get('previousroute'), 'search') !== false) {
                    $url   =  Session::get('previousroute');
                    $isSearchRoute = true;
                }
                
                if (Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'), 'role_id' => 2, 'status'=>1, 'deleted_at' => NULL), $rememberMe !="" ? true : false)) {
                    if(Session::get('previousroute') == 'cart'){
                        return redirect()->to('shipping');
                    }else if(Session::get('previousroute') == 'quote-cart') {
                        return redirect()->to('quote-cart');
                    }elseif($isSearchRoute){
                        return redirect()->to($url);
                    }
                    else{
                        $userId = Auth::user()->id;
                        $user           = User::Select('mybio_added')->findOrFail($userId);
                        Session::put('mybio_added', $user->mybio_added);
                        return redirect()->intended('/account-settings/'.$userId)->with('success', config('constants.LOGIN_SUCCESS'));
                    }
                } else {
                    $query = \DB::table('users')
                    ->where('role_id', 2)
                    ->where('email', Input::get('email'))
                    ->where('status', 2)
                    ->whereNULL('deleted_at');
                    $result = $query->get();
                    if (count($result) > 0) {
                        return redirect()->intended('/login')->with('failure', config('constants.USER_ACCOUNT_DEACTIVATED'));
                    }else{
                        return redirect()->intended('/login')->with('failure', config('constants.INVALID_EMAIL_OR_PASSWORD'));
                    }
                }
            }
        }
    }


    /**
     * Name: registerView
     * Purpose:  For show register page..
     * @return \Illuminate\Http\Response
     */

    public function registerView() {
        if(Auth::user()){
            return redirect()->intended('/')->with('warning',  config('constants.ALREADY_LOGGEDIN'));
        }
        $data['title'] = 'Register';
        $data['validationMessage'] = User::$registerUserValidationMessages;
        return view('frontend.auth.register', $data);
    }

    /**
     * Name: register
     * Purpose:  Register a new user.
     * @return \Illuminate\Http\Response
     */

    public function saveNewRegisterUserInfo(Request $request) {
        if(Auth::user()){
            return redirect()->intended('/')->with('warning',  config('constants.ALREADY_LOGGEDIN'));
        }
        $data = $request->all();
        $validator = \Validator::make($data, User::registerUserValidationRules(), User::$registerUserValidationMessages);
        if ($validator->fails()) {
            //Redirect user back with input if server side validation fails
            return redirect('register')->withErrors($validator)->withInput();
        }else{

            $user = User::registerNewUser($request);
            if($user['status']){
               $userData['firstName'] = preg_replace('!\s+!', ' ',$request->input('first_name'));
               $userData['lastName']  = preg_replace('!\s+!', ' ',$request->input('last_name'));
                if($request->has('subscribe_newsletter') ){
                    $resultData         = NewsLetterSubscriber::checkAlreadyInSubscriberList($request->input('email'));
                    if(empty($resultData)){
                        $newsLetterSubscriber  =  new NewsLetterSubscriber();
                        //save subscriber details
                        $newsLetterSubscriber->subscriber_email    =  $request->input('email');
                        $newsLetterSubscriber->is_subscribed       =  1;
                        $newsLetterSubscriber->save();
                        $data['email'] =  $request->input('email');
                        \Mail::to($request->input('email'))->send(new WelComeNewsLetterSubscriberMail($data, config('constants.NEWSLETTER_SUBSCRIPTION_SUBJECT')));
                    }
                }

                Mail::to($request->input('email'))->send(new SendWelComeMail($userData,config('constants.WELCOME_USER_SUBJECT')));
                $rememberMe = false;

                    if (Auth::attempt(array('email' => $request->input('email'), 'password' => $request->input('password'), 'role_id' => 2, 'status'=>1, 'deleted_at' => NULL), $rememberMe !="" ? true : false)) {
                        return redirect()->intended('/account-settings/'.Auth::user()->id)->with('success', config('constants.SUCCESSFULLY_REGISTER'));
                    } else {
                        return redirect()->intended('/login')->with('registration_success', config('constants.SUCCESSFULLY_REGISTER'));
                    }


            }else{
                return redirect()->intended('/register')->with('failure', config('constants.FAILED_REGISTER'));
            }
        }
        return view('frontend.auth.register', $data);
    }


    /**
     * Name: logoutFrontUser
     * Purpose: Destory session of user and get logout.
     * @return \Illuminate\Http\Response
     */

    public function logoutFrontUser(){
        $allSearchProducts = Session::get('searchProducts');
        Auth::logout();
        Session::flush('success', 'Some goodbye message');
        Session(['searchProducts' => $allSearchProducts]);
        return redirect()->intended('/')->with('success', config('constants.LOGOUT_SUCCESS'));
    }

    /**
     * Name: forgetPassword
     * Purpose:  For show user forget password view.
     * @return \Illuminate\Http\Response
     */
    
    public function forgetPassword() {
        if (Auth::check()) {
            return redirect('/home');
        }
        $data['title'] = 'Forgot Password';
        return view('frontend.auth.forget-password', $data);
    }
   
    /*
     * Name: forgotEmailSend
     * Created By: SIPL
     * Created Date: 13-Feb-2018
     * Purpose: For send temporary password on emails.
     */

    public function sendResetPasswardMail() { 
        try {
            $validationRules = array(
                'email' => 'required|email'
            );
            $validationMessages = array(
                'email.required' => config('constants.REQUIRED_EMAIL'),
                'email.email' => config('constants.VALID_EMAIL_ADDRESS'),
            );
            $validation = \Validator::make($this->_request->all(), $validationRules, $validationMessages); 
            if ($validation->fails()) {
                return redirect()->to('/forget-password')->withErrors($validation)->withInput();
            } else { 
                $email = Input::get('email');
                $user = User::select('users.id', 'users.email','name','users.status')
                        ->where("users.email", $email)
                        ->whereNull('users.deleted_at')
                        ->where('users.role_id', 2)
                        ->first();
                if ($user == NULL) {
                    return redirect()->intended('/forget-password')->with('failure', config('constants.ERROR_FORGOT_PASSWORD'));
                } else {
                    if($user['status'] == 2){
                        return redirect()->intended('/forget-password')->with('failure', config('constants.DEACTIVE_ERROR_FORGOT_PASSWORD'));
                    }else{
                        $user = $user->toArray();
                        $userId    = \Crypt::encrypt($user['id']);
                        $url        = url("reset-password/".$userId);
                        $forgotPasswordToken =  $userId;
                        /* Update data base */
                        $updateUser = User::find($user['id']);
                        $updateUser->forgot_password_token  = $forgotPasswordToken;
                        $updateUser->save();
                        $user['resetUrl'] =  $url;
                        Mail::to($user['email'])->send(new ForgotPassword($user,config('constants.RESET_PASSWORD_SUBJECT')));
                        return redirect()->intended('/login')->with('success',config('constants.RESET_PASSWORD_LINK_SUCCESS'));
                    }
                }
                /* Send activation email to user */
            }
        } catch (\Exception $ex) {
            return redirect()->intended('/login')->with('failure',config('constants.COMMON_ERROR'));
        }
    }

    public function resetPasswordView($id){
        try {
            $data['title']              = 'Rest Password';
            $userId                     = \Crypt::decrypt(trim($id));
            $data['validationMessage']  = User::$registerUserValidationMessages;
            $data['exist']              = \App\User::checkPasswordResetToken($userId,$id);
            if(!empty($data['exist']) && count($data['exist'])>0){
                return view('frontend.auth.reset-password', $data);
            }else{
                return redirect()->intended('/forget-password')->with('failure', config('constants.PASSWORD_RESET_LINK_EXPIRED'));
            }
        } catch (\Exception $ex) {
            return redirect()->intended('/forget-password')->with('failure', config('constants.COMMON_ERROR'));
        }
    }

    public function updateNewPassword($id,Request $request){ 
        $userId                     = \Crypt::decrypt(trim($id));
        $data['isExist']            = \App\User::checkPasswordResetToken($userId,$id);
        if(!empty($data['isExist']) && count($data['isExist'])>0) { 
            $data       = $request->all();
            $validator  = \Validator::make($data, User::passwordValidationRules(), User::$registerUserValidationMessages);
            if ($validator->fails()) { 
                //Redirect user back with input if server side validation fails
                return redirect('reset-password/'.$id)->withErrors($validator)->withInput();
            }else{
               $password =  $request->input('password');
               $updatePassword = array('password'=>Hash::make($password),'forgot_password_token'=>NULL);
               $data['user'] = \App\User::updateNewPassword($userId,$updatePassword);
                if($data['user']) { 
                    return redirect('/login')->with('success', config('constants.RESET_PASSWORD_SUCCESS'));
                }else{
                    return redirect('reset-password/'.$id)->with('failure',config('constants.COMMON_ERROR'));
                }
            }
        }else{
            return redirect('/login')->with('failure',config('constants.PASSWORD_RESET_LINK_EXPIRED'));
        }
    }

    /*
     * Name: logout
     * Created By: SIPL
     * Created Date: 13-Feb-2018
     * Purpose: For user logout functionality.
     */
    public function logout() {
        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }

    /*
     * Name: checkUserEmail
     * Created By: SIPL 
     * Created Date: 14-June-2017 
     * Purpose: Check email should be unique per admin user 
     */
    public function checkUserEmail() {
        $data = $this->_request->all();
        if (!empty($data['email'])) {
            $userId = Auth::user()->id;
            $query = \DB::table('users')
                    ->where('role_id', 1)
                    ->where('email', $data['email'])
                    ->where('id', '<>', $userId);
            $result = $query->get();
            if (count($result) > 0) {
                abort(404);
            }
        }
        echo 'Success';
    }

    public function resourceNotFound(){
        abort(404, 'The resource you are looking for could not be found');
    }
    /*
     * Check duplicate email
     * @param int $id
     * @param Request $request
     */
    public function checkEmail(Request $request) {
        if($request->ajax()) {
            try{
                if(!empty($request->email)){
                    $email = $request->email;
                }else{
                    $email = $request->user_email;
                }
                
                $loginUserEmail = '';
                if (Auth::check()) {
                    $loginUserEmail = Auth::user()->email;
                }
                if (!empty($email)) {
                    $userEmail = User::where('email', $email)->where('role_id', 2)->whereNULL('deleted_at')->count();
                    if($loginUserEmail == $email){
                        return response( ['status'=> true, 'message'   => config('constants.EMAIL_EXIST'),'data'=>$userEmail], 200);
                    }else{
                        if($userEmail > 0){
                            return response( ['status'=> false, 'message'   =>config('constants.EMAIL_NOT_EXIST')  ,'data'=>$userEmail], 500);
                        }else{
                            return response( ['status'=> true, 'message'   => config('constants.EMAIL_EXIST'),'data'=>$userEmail], 200);
                        }
                    }
                }else{
                    return response( ['status'=> false, 'message'   => config('constants.EMAIL_NOT_EXIST')], 500);
                }
            } catch (\Exception $ex) {
                return response( ['status'=> false, 'message'   => config('constants.EMAIL_NOT_EXIST')], 500);
            }
        } else {
            return response( ['status'=> false, 'message'   => config('constants.EMAIL_NOT_EXIST')], 500);
        }
    }

    /**
     * Name: checkUserStatus
     * Purpose:  For checking the user status.
     * @return \Illuminate\Http\Response
     */
    public function checkUserStatus() {
        if (Auth::check()) {
            $userId = Auth::user()->id;
            $userDetails = User::where('id', $userId)->where('status', 0)->where('role_id', 2)->count();
            if($userDetails > 0){
                $allSearchProducts = Session::get('searchProducts');
                Auth::logout();
                Session::flush('success', 'Some goodbye message');
                Session(['searchProducts' => $allSearchProducts]);
                echo true;
            }else{
                echo false;
            }
        }
    }
}
