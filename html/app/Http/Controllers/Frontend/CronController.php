<?php
namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use Cart;
use Event;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Authenticated;
use DB;
use Illuminate\Support\Facades\Auth;
use Helpers; // Important
use Illuminate\Support\Facades\Redirect;
use App\Country;
use App\State;
use App\City;
use App\Subscribe;
use Hash;
use App\Mail\CronPaymentSuccessMail;
use App\Mail\TrialExpiryMail;
use App\Mail\SubscriptionExpiryMail; 
use App\Mail\CronPaymentRenewSuccessMail;
use App\Mail\PaymentFailureMail;
use App\Mail\IntimationToAdminPaymentFailureMail;

class CronController extends Controller{
    
    protected $userSessionData;
    /*
        * Create a new controller instance.
        * @return void
     */
        public function __construct(){
        }
        
        public function emailTemplate(){
            return view('emails.cron-email');
        }

/* Notification Cron Jobs Start */
        
        /*
        * checkTwentyFifthDayOfFreeTrial
        * @return void
     */        
        public function checkTwentyFifthDayOfFreeTrial(){
            
            $checkFreeTrial = Subscribe::checkTwentyFifthTrialExpiry();

            if(!empty($checkFreeTrial)){
                foreach ($checkFreeTrial as $trialExpire){
                    $userId = $trialExpire->user_id;
                    $billingFirstName = $trialExpire->billing_first_name;
                    $billingLastName = $trialExpire->billing_last_name;
                    $billingEmail = $trialExpire->billing_email;
                    //Subscribe Mail Data
                    $subscribeUser = array();
                    $subscribeUser['billingFirstName'] = $billingFirstName;
                    $subscribeUser['billingLastName'] = $billingLastName;
                    $subscribeUser['billingEmail'] = $billingEmail;
                    //Send Email to Subscribe User
                    \Mail::to($billingEmail)->send(new TrialExpiryMail($subscribeUser,config('constants.TRIAL_EXPIRY_SOON')));
                }
            }
            
        }//End Function
                
        /*
            * checkSubscriptionExpiry
            * @return void
        */        
        public function checkSubscriptionExpiry(){

            $checkSubscriptionExpiry = Subscribe::getSubscriptionExpiryUsers();
            if(!empty($checkSubscriptionExpiry)){
                foreach ($checkSubscriptionExpiry as $subscribeExpire){
                    $billingFirstName = $subscribeExpire->billing_first_name;
                    $billingLastName = $subscribeExpire->billing_last_name;
                    $billingEmail = $subscribeExpire->billing_email;
                    //Subscribe Mail Data
                    $subscribeUser = array();
                    $subscribeUser['billingFirstName'] = $billingFirstName;
                    $subscribeUser['billingLastName'] = $billingLastName;
                    $subscribeUser['billingEmail'] = $billingEmail;
                    //Send Email to Subscribe User
                    \Mail::to($billingEmail)->send(new SubscriptionExpiryMail($subscribeUser,config('constants.SUBSCRIPTION_EXPIRY_SOON')));
                }
            }
        }//End Function
        
/* Notification Cron Jobs End */

/* Trial Payment Start */
        
        /*
        * paymentAfterTrialExpire.
        * @return void
     */        
        public function paymentAfterTrialExpire(){
            
            DB::beginTransaction();
            
            $checkFreeTrial = Subscribe::checkTrialExpiryForPayment();
            
            if(!empty($checkFreeTrial)){
                foreach ($checkFreeTrial as $trialExpire){
                    
                    $billingId = $trialExpire->sub_billing_id;
                    $userId = $trialExpire->user_id;
                    $billingFirstName = $trialExpire->billing_first_name;
                    $billingLastName = $trialExpire->billing_last_name;
                    $grandTotalAmount = $trialExpire->grand_total_amount;
                    $billingEmail = $trialExpire->billing_email;
                    
                    $getAuthToken = $this->getPaypalAuthToken();
                    $data = array ('intent' => 'sale',  
                        'payer' =>  array (
                            'payment_method' => 'credit_card',
                            'funding_instruments' => array (
                                0 => array (
                                    'credit_card_token' => array (
                                        'credit_card_id' => $trialExpire->paypal_card_id,
                                        ),
                                    ),
                                ),
                            ),
                        'transactions' => array (
                            0 => array (
                                'amount' => array (
                                    'total' => $trialExpire->grand_total_amount,
                                    'currency' => 'USD',
                                    ),
                                'description' => 'Payment of your subscription.',
                                ),
                            ),
                        );
                    
                    $paymentJson = json_encode($data);
                    
                    $getCard = $this->getCardFromPaypal($paymentJson, $getAuthToken);
                    
                    $paymentResponse = json_decode($getCard);
                    
                    if(!empty($paymentResponse) && $paymentResponse->state=="approved" && $paymentResponse->id!=""){
                        /*Success handling*/
                        if(!empty($paymentResponse->transactions)){
                            foreach($paymentResponse->transactions as $transaction){
                                if(!empty($transaction->related_resources)){
                                    foreach($transaction->related_resources as $related_resources){
                                        if($related_resources->sale->state == "completed"){
                                            /*Payment state completed*/
                                            $updateBillingTable = array("is_paid" => 1, "updated_at" => date("Y-m-d H:i:s"));
                                            $updatePayment = Subscribe::updatePaymentStatus($updateBillingTable, $billingId);
                                            
                                            if($updatePayment){
                                                $transactionId = $related_resources->sale->id;
                                                $transactionArray = array(
                                                    "transaction_id" => $related_resources->sale->id, 
                                                    "payment_parent_id" => $related_resources->sale->parent_payment, 
                                                    "transaction_by" => 1,
                                                    "user_id" => $userId, 
                                                    "payment_status" => $related_resources->sale->state);

                                                $transactionPayment = Subscribe::insertSubscriptionTransaction($transactionArray);
                                                
                                                if($transactionPayment['payment']!=""){
                                                    //Subscribe Mail Data
                                                    $subscribeUser = array();
                                                    $subscribeUser['billingFirstName'] = $billingFirstName;
                                                    $subscribeUser['billingLastName'] = $billingLastName;
                                                    $subscribeUser['grandTotalAmount'] = $grandTotalAmount;
                                                    $subscribeUser['transactionId'] = $transactionId;
                                                    $subscribeUser['billingEmail'] = $billingEmail;
                                                    $subscribeUser['paymentDate'] = date("Y-m-d H:i:s");
                                                    //Send Email to Subscribe User
                                                    \Mail::to($billingEmail)->send(new CronPaymentSuccessMail($subscribeUser,config('constants.CRON_PAYMENT_SUCCESS')));
                                                    DB::commit();
                                                }
                                            }
                                        }else{
                                            /*Payment state not complete*/
                                            /*Disable User Account*/
                                            $updateUserExpiry = array("status" => 0, "updated_at" => date("Y-m-d H:i:s"));
                                            $updateUserExpiryDates = Subscribe::updateUserExpiryDates($updateUserExpiry, $userId);
                                            //Send Email to Subscribe User For Payment Failure
                                            $failureEmail = array();
                                            $failureEmail['billingFirstName'] = $billingFirstName;
                                            $failureEmail['billingLastName'] = $billingLastName;
                                            /* Below email is sent to User */
                                            \Mail::to($billingEmail)->send(new PaymentFailureMail($failureEmail,config('constants.CRON_PAYMENT_FAILURE')));

                                            /* Below email is sent to Admin of website */
                                            \Mail::to('dheeraj.solanki@systematixindia.com')->send(new IntimationToAdminPaymentFailureMail($failureEmail,config('constants.CRON_PAYMENT_FAILURE')));
                                            DB::commit();
                                        }
                                    }
                                }else{
                                    /*Errror related resource handling*/
                                    DB::rollBack();
                                }
                            }
                        }else{
                            /*Errror Transaction handling*/
                            DB::rollBack();
                        }
                    }else{
                        /* Errror handling send failure email from here. */
                        /*Disable User Account*/
                        $updateUserExpiry = array("status" => 0, "updated_at" => date("Y-m-d H:i:s"));
                        $updateUserExpiryDates = Subscribe::updateUserExpiryDates($updateUserExpiry, $userId);
                        //Send Email to Subscribe User For Payment Failure
                        $failureEmail = array();
                        $failureEmail['billingFirstName'] = $billingFirstName;
                        $failureEmail['billingLastName'] = $billingLastName;
                        /* Below email is sent to User */
                        \Mail::to($billingEmail)->send(new PaymentFailureMail($failureEmail,config('constants.CRON_PAYMENT_FAILURE')));
                        
                        /* Below email is sent to Admin of website */
                        \Mail::to('dheeraj.solanki@systematixindia.com')->send(new IntimationToAdminPaymentFailureMail($failureEmail,config('constants.CRON_PAYMENT_FAILURE')));
                        DB::commit();
                    }                    
                }
            }
        }//End Function
        
/* Trial Payment End */

/* Subscription Payment Start */
        
    /*
        * subscriptionPayment.
        * @return void
     */
        public function subscriptionPayment(){
            
            DB::beginTransaction();
            $renewPaymentSubscription = Subscribe::renewPaymentSubscription();
            if(!empty($renewPaymentSubscription)){
                foreach ($renewPaymentSubscription as $renewPayment){
                    
                    $billingId = $renewPayment->sub_billing_id;
                    $userId = $renewPayment->user_id;
                    $billingFirstName = $renewPayment->billing_first_name;
                    $billingLastName = $renewPayment->billing_last_name;
                    $grandTotalAmount = $renewPayment->grand_total_amount;
                    $billingEmail = $renewPayment->billing_email;
                    
                    $getAuthToken = $this->getPaypalAuthToken();
                    $data = array ('intent' => 'sale',  
                        'payer' =>  array (
                            'payment_method' => 'credit_card',
                            'funding_instruments' => array (
                                0 => array (
                                    'credit_card_token' => array (
                                        'credit_card_id' => $renewPayment->paypal_card_id,
                                        ),
                                    ),
                                ),
                            ),
                        'transactions' => array (
                            0 => array (
                                'amount' => array (
                                    'total' => $renewPayment->grand_total_amount,
                                    'currency' => 'USD',
                                    ),
                                'description' => 'Payment of your subscription.',
                                ),
                            ),
                        );
                    
                    $paymentJson = json_encode($data);
                    
                    $getCard = $this->getCardFromPaypal($paymentJson, $getAuthToken);
                    
                    $paymentResponse = json_decode($getCard);
                    
                    if(!empty($paymentResponse) && $paymentResponse->state=="approved" && $paymentResponse->id!=""){
                        /*Success handling*/
                        if(!empty($paymentResponse->transactions)){
                            foreach($paymentResponse->transactions as $transaction){
                                if(!empty($transaction->related_resources)){
                                    foreach($transaction->related_resources as $related_resources){
                                        if($related_resources->sale->state == "completed"){
                                            /*Payment state completed now update database */                                            
                                            /* Update New Plan Expiry Date Start */
                                            $planPurchaseDate = date("Y-m-d");
                                            $planExpiryDate= date('Y-m-d', strtotime($planPurchaseDate . '+365 days'));
                                            $updateUserExpiry = array("plan_expiry_date" => $planExpiryDate, "updated_at" => date("Y-m-d H:i:s"));
                                            $updateUserExpiryDates = Subscribe::updateUserExpiryDates($updateUserExpiry, $userId);
                                            /* Update New Plan Expiry Date End */
                                            
                                            /* Update Payment Status Start */
                                            $updateBillingTable = array("is_paid" => 1, "is_renew" => 1, "updated_at" => date("Y-m-d H:i:s"));
                                            $updatePayment = Subscribe::updatePaymentStatus($updateBillingTable, $billingId);
                                            /* Update Payment Status End */
                                            
                                            if($updatePayment){
                                                
                                                /* Update Old Status Start */
                                                $updateOldTransaction = array("is_renew" => 1, "updated_at" => date("Y-m-d H:i:s"), "deleted_at" => date("Y-m-d H:i:s"));
                                                $updateOldTransactionPayment = Subscribe::updateOldTransactionPayment($updateOldTransaction, $userId);
                                                /* Update Old Status End */
                                                
                                                if($updateOldTransactionPayment){
                                                    $transactionId = $related_resources->sale->id;
                                                    $transactionArray = array(
                                                        "transaction_id" => $related_resources->sale->id, 
                                                        "payment_parent_id" => $related_resources->sale->parent_payment, 
                                                        "transaction_by" => 1,
                                                        "user_id" => $userId, 
                                                        "payment_status" => $related_resources->sale->state);

                                                    $transactionPayment = Subscribe::insertSubscriptionTransaction($transactionArray);

                                                    if($transactionPayment['payment']!=""){
                                                        //Subscribe Mail Data
                                                        $subscribeUser = array();
                                                        $subscribeUser['billingFirstName'] = $billingFirstName;
                                                        $subscribeUser['billingLastName'] = $billingLastName;
                                                        $subscribeUser['grandTotalAmount'] = $grandTotalAmount;
                                                        $subscribeUser['transactionId'] = $transactionId;
                                                        $subscribeUser['billingEmail'] = $billingEmail;
                                                        $subscribeUser['paymentDate'] = date("Y-m-d H:i:s");
                                                        //Send Email to Subscribe User
                                                        \Mail::to($billingEmail)->send(new CronPaymentRenewSuccessMail($subscribeUser,config('constants.CRON_PAYMENT_RENEW_SUCCESS')));
                                                        DB::commit();
                                                    }
                                                }
                                            }
                                        }else{
                                            /*Payment state not complete*/
                                            /*Disable User Account*/
                                            $updateUserExpiry = array("status" => 0, "updated_at" => date("Y-m-d H:i:s"));
                                            $updateUserExpiryDates = Subscribe::updateUserExpiryDates($updateUserExpiry, $userId);
                                            //Send Email to Subscribe User For Payment Failure
                                            $failureEmail = array();
                                            $failureEmail['billingFirstName'] = $billingFirstName;
                                            $failureEmail['billingLastName'] = $billingLastName;
                                            /* Below email is sent to User */
                                            \Mail::to($billingEmail)->send(new PaymentFailureMail($failureEmail,config('constants.CRON_PAYMENT_FAILURE')));

                                            /* Below email is sent to Admin of website */
                                            \Mail::to('dheeraj.solanki@systematixindia.com')->send(new IntimationToAdminPaymentFailureMail($failureEmail,config('constants.CRON_PAYMENT_FAILURE')));
                                            DB::commit();
                                        }
                                    }
                                }else{
                                    /*Errror related resource handling*/
                                    DB::rollBack();
                                }
                            }
                        }else{
                            /*Errror Transaction handling*/
                            DB::rollBack();
                        }
                    }else{
                        /* Errror handling send failure email from here. */
                        /*Disable User Account*/
                        $updateUserExpiry = array("status" => 0, "updated_at" => date("Y-m-d H:i:s"));
                        $updateUserExpiryDates = Subscribe::updateUserExpiryDates($updateUserExpiry, $userId);
                        
                        /*Send Email to Subscribe User For Payment Failure*/
                        $failureEmail = array();
                        $failureEmail['billingFirstName'] = $billingFirstName;
                        $failureEmail['billingLastName'] = $billingLastName;
                        /* Below email is sent to User */
                        \Mail::to($billingEmail)->send(new PaymentFailureMail($failureEmail,config('constants.CRON_PAYMENT_FAILURE')));
                        
                        /* Below email is sent to Admin of website */
                        \Mail::to('dheeraj.solanki@systematixindia.com')->send(new IntimationToAdminPaymentFailureMail($failureEmail,config('constants.CRON_PAYMENT_FAILURE')));
                        DB::commit();
                    }                    
                }
            }
        }//End Function
        
/* Subscription Payment End */        
        
/* PayPal Authentication and Payment Start */
        
        /*
            * getPaypalAuthToken.
            * @return void
        */
        public function getPaypalAuthToken(){
            
            $ch = curl_init();
            
            $clientId = config('constants.SUBSCRIPTION_CLIENT_ID');
            $secret = config('constants.SUBSCRIPTION_CLIENT_SECRET');
            $authUrl = config('constants.SUBSCRIPTION_AUTH_URL');
            
            curl_setopt($ch, CURLOPT_URL, $authUrl);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

            $result = curl_exec($ch);
            
            if(empty($result)){
                return false;
            }else{
                $json = json_decode($result);
                return $json->access_token;
            }
            curl_close($ch);
        }
        
        /*
            * getCardFromPaypal.
            * @return void
        */
        public function getCardFromPaypal($paymentJson, $authToken){
            
            $vaultUrl = "https://api.sandbox.paypal.com/v1/payments/payment";
            
            $ch1 = curl_init();
            curl_setopt($ch1, CURLOPT_URL, $vaultUrl);
            curl_setopt($ch1, CURLOPT_HEADER, false);
            curl_setopt($ch1, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch1, CURLOPT_POST, true);
            curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true); 
            curl_setopt($ch1, CURLOPT_HTTPHEADER, array("Content-Type: application/json","Authorization: Bearer ".$authToken));
            curl_setopt($ch1, CURLOPT_POSTFIELDS, $paymentJson);

            $result1 = curl_exec($ch1);

            if(empty($result1)){
                return false;
            }else{
                return $result1;
            }
            curl_close($ch1);
        }//End Function

/* PayPal Authentication and Payment End */
        
}//End Class