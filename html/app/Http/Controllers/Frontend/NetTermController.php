<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 06/04/18
 * Time: 10:55 AM
 */

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Event;
use Session;
use App\User;
use Helpers;
use App\UserAccountType;
use App\NetAccountHolder;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Redirect;

class NetTermController extends Controller
{
    protected $userSessionData;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
       Event::listen(Authenticated::class, function ($event) {
            $this->userSessionData = $event->user;
            if ($this->userSessionData['status'] == 0) {
                Helpers::sessionFlush();
            } 
        });
    }

    public static  $validationMessages = [
        'first_name.required'               => 'Please enter first name',
        'first_name.min'                    => 'First name consists of at least 2 characters.',
        'first_name.max'                    => 'First name not be exceed 30 characters long.',
        'last_name.required'                => 'Please enter last name.',
        'last_name.min'                     => 'Last name consists of at least 2 characters.',
        'last_name.max'                     => 'Last name not be exceed 30 characters long.',
        'email.required'                    => 'Please enter email.',
        'email.email'                       => 'Invalid email address.',
        'account_type.required'             => 'Please select a account type.',
        'resale_doc.required'               => 'Please select Resale doc for upload.',
        'cod_application_doc.required'      => 'Please select Cod Application doc for upload.',
        'net_terms_application_doc.required'=> 'Please select Net Terms Application doc for upload.',
        'credit_reference_doc.mimes'        => 'The Credit Reference doc must be a file of type pdf.',
        'cod_application_doc.mimes'         => 'The Cod Application doc must be a file of type pdf.',
        'net_terms_application_doc.mimes'   => 'The Net Terms Application doc must be a file of type pdf.',
        'resale_doc.max'                    => 'Uploaded Document size can not be more than 1MB.',
        'cod_application_doc.max'           => 'Uploaded Document size can not be more than 1MB.',
        'net_terms_application_doc.max'     => 'Uploaded Document size can not be more than 1MB.',
        'credit_reference_doc.required'     => 'Please select credit reference doc for upload.'

    ];


    public static function documentRules($data){
        $rules = [];
        foreach($data as $doc=> $index) {
            $rules['document.'.$index] = 'required|mimes:pdf|max:1024';
        }
        return $rules;
    }


    public static function  applyNetTermValidationRules ($data)  {
            $rules['credit_reference_doc.*']    = 'required|mimes:pdf|max:1024';
            $rules['first_name']                = 'required';
            $rules['last_name']                 = 'required';
            $rules['email']                     = 'required';
            $rules['account_type']              = 'required';
            return $rules;
       }

    public function applyNetTermAccount(){
        $columns = [
            'net_account_holders.id',
            'net_account_holders.net_account_type',
            'net_account_holders.applied_on_date',
            'net_account_holders.status',
            'users.name',
            'users.last_name',
            'user_account_types.name AS requested_account_type',
            'cod_doc_url',
            'net_term_acc_doc_url',
            'resale_doc_url',
            'resale_uploaded_file_name',
            'cod_uploaded_file_name',
            'net_term_uploaded_file_name'
        ];

        $requestList = NetAccountHolder::select($columns)
            ->where('net_account_holders.user_id', Auth::user()->id)
            ->join('users','net_account_holders.user_id','=','users.id')
            ->join('user_account_types','net_account_holders.net_account_type','=','user_account_types.id')
            ->whereNull('net_account_holders.deleted_at')->get();

        if($requestList->isEmpty()){
            $data['validationMessage']  = self::$validationMessages;
            $data['accountType']        = UserAccountType::getAccountTye();
            $user                       = User::findOrFail(Auth::user()->id);
            $data['user']               = $user;
            $data['isRequestExist']     = [];
            return view('frontend.pages.apply-net-term-account',$data);
        }else{
            $data['user']               = [];
            $data['isRequestExist']     = $requestList;
            return view('frontend.pages.apply-net-term-account',$data);
        }


    }

    public function saveNetTermAccountDetails(Request $request){
        try{
            $data      = $request->all();
            $userId    = Auth::user()->id;
            $validation     = \Validator::make($data, self::applyNetTermValidationRules($request->file('credit_reference_doc')), self::$validationMessages);
            if ($validation->fails()) {
                return redirect()->to('/apply-net-term-account')->withErrors($validation)->withInput();
            } else {
                $isRequestExist  = NetAccountHolder::find($userId);
               if(empty($isRequestExist)){
                   $netAccountHolder                   = new NetAccountHolder();
                   $netAccountHolder->user_id          =  $userId;
                   $fileDir                            = config('constants.NET_ACCOUNT_DOC_PATH').'/'.$userId.'/';
                   $fileArray = array();
                   foreach ($request->file('credit_reference_doc') as $doc) {
                       $uploadedFileName                   = uniqid().'.'.$doc->getClientOriginalExtension();
                        $creditReferenceDocFilePath         = \Helpers::uploadNetTermDoc($fileDir,$doc,$uploadedFileName);
                        $filePath = $userId.'/'.$creditReferenceDocFilePath;
                       array_push($fileArray,$filePath);
                   }
                   $netAccountHolder->credit_reference_doc  = json_encode($fileArray);
                   $netAccountHolder->applied_on_date       = date('Y-m-d h:i:s');
                   $netAccountHolder->status                = 0;
                   $netAccountHolder->net_account_type      = $request->input('account_type');
                   if ($netAccountHolder->save()) {
                       return redirect()->to('account-settings/'.$userId)->with('success', config('constants.SUCCESS_NET_TERM'));
                   } else {
                       return redirect()->to('/apply-net-term-account')->with('failure', config('constants.COMMON_ERROR'));
                   }
               }else{
                   return redirect()->to('/apply-net-term-account')->with('failure', config('constants.DUPLICATE_NET_TERM_REQUEST') );
               }
            }
        }catch (\Exception $ex){
            return redirect()->to('account-settings/'.$userId)->with('failure', config('constants.COMMON_ERROR').$ex->getMessage());
        }
    }
}