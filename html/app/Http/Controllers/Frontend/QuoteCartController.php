<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Bom;
use Cart;
use Event;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Http\Controllers\Controller;
use App\Quote;
use App\QuoteDetail;
use Illuminate\Pagination\LengthAwarePaginator;
use Helpers;
use PDF;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Redirect;
use DB;

class QuoteCartController extends Controller
{
    protected $userSessionData;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
    }

    /**
     * Show the  cart view .
     *
     * @return \Illuminate\Http\Response
     */

    public function quoteCart(){
        session(['previousroute' => '']);
        return view('frontend.quote-cart.quote-cart');
    }

    /**
     * Add Searched part in quote to save
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function addToQuote(Request $request){
        try {
            $data                           = $request->all();
            $productId                      = $request->productId;
            $distributorName                = $request->distributorName;
            $manufacturer                   = $request->manufacturer;
            $sourcePartId                   = $request->sourcePartId;
            $quantity                       = $request->quantity;
            $customerPartNo                 = $request->customerPartNo;
            $stockAvailability              = $request->stockAvailability;
            $sourceMinQuantity              = $request->sourceMinQuantity;
            if(!empty($request->resaleListData))
                $resaleListDataRequestArray     = json_decode($request->resaleListData);
            else
                $resaleListDataRequestArray     = '';
            if(!empty($request->availableData))
                $availableDataRequestArray      = json_decode($request->availableData);
            else
                $availableDataRequestArray      = '';
            $price                          = 0;
            $resaleList                     = array();
            $availableData                  = array();
            $dateCode                       = $request->dateCode;
            $manufacturerCode               = $request->mfrCd;
            $requestedQuantity              = $request->requestedQuantity;
            $requestedCost                  = $request->requestedCost;
            $requestedDeliveryDate          = $request->requestedDeliveryDate;
            $availableStock                 = 0;
            $minQty                         = 0;
            $quote_type                     = $request->quoteType;

            /* Check item is exist on quote cart or not  */
            $itemExist =  Cart::instance('quotecart')->content()->where('id', $productId)->where('options.sourcePartId', $sourcePartId);
            
            if(empty(json_decode($itemExist))) {
                $price = 0;
                $resaleList = array();
                /* Price calculation on the bases of Qty */
                $productDetails = Session::get('productDetails');
                if($stockAvailability && $stockAvailability != 'N/A'){
                    $availableStock = $availableDataRequestArray[0]->fohQty;
                }else{
                    $availableStock = 0;
                }
                $availableData    =  $availableDataRequestArray;

                if(!empty($resaleListDataRequestArray)){
                    foreach ($resaleListDataRequestArray as $key => $prices) {
                        if($quantity >= $prices->minQty && $quantity <= $prices->maxQty ){
                            $price = property_exists($prices,'price')?$prices->price:$price;
                            $resaleList = $resaleListDataRequestArray;
                            break;
                        }
                    }
                    $minQty = $resaleListDataRequestArray[0]->minQty;
                }else{
                    if($quote_type != 'not_found'){
                        return response(['status' => false, 'message' => 'Sorry no prices available for this item.']);
                    }
                }
                /* Check mim quantity of items */
                if($quantity < $minQty){
                    $quantity  =  $minQty;
                    foreach ($resaleListDataRequestArray as $key => $prices) {
                        if($quantity >= $prices->minQty && $quantity <= $prices->maxQty ){
                             $price = property_exists($prices,'price')?$prices->price:$price;
                            $resaleList = $resaleListDataRequestArray;
                            break;
                        }
                    }
                }
                /* Add item to  Quote cart  */
                Cart::instance('quotecart')->add([
                    ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'sourceMinQuantity'=>$sourceMinQuantity,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability, 'requestedQuantity'=>$requestedQuantity,'requestedCost' => $requestedCost, 'requestedDeliveryDate'  => $requestedDeliveryDate, 'distributorName'=>$distributorName, 'quoteType' => $quote_type]]
                ]);
                $totalPrice = 0;
                foreach(Cart::instance('quotecart')->content() as $row){
                    $totalPrice = $totalPrice + $row->price * $row->qty;
                }
                $itemCount = Cart::instance('quotecart')->content()->count();
                return response(['status' => true,'data' => '','itemPrice' => number_format($price, 3, '.', ''),'totalPrice'=> $totalPrice,'itemCount'=>$itemCount,'message' =>config('constants.QUOTE_ITEM_SUCCESS'),'availableData'=>$availableData ]);
            }else{
                /* Item Update in the cart */
                foreach ($itemExist as $index => $data) {
                    $rowId = $data->rowId;
                    $resaleList = $data->options->resaleList;
                    $dateCode = $data->options->dateCode;
                    $availableData = $data->options->availableData;
                    $availableStock = $availableData[0]->fohQty;
                    $sourceMinQuantity= $data->options->sourceMinQuantity;
                }

                foreach ($resaleList as $key => $prices) {
                    if($quantity >= $prices->minQty && $quantity <= $prices->maxQty ){
                        $price = $prices->price;
                        break;
                    }
                }
                Cart::instance('quotecart')->update($rowId,
                        ['id'=>$productId, 'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'sourceMinQuantity'=>$sourceMinQuantity,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,'requestedQuantity'=>$requestedQuantity,'requestedCost' => $requestedCost, 'requestedDeliveryDate'  => $requestedDeliveryDate, 'distributorName'=>$distributorName,'quoteType' => $quote_type]]
                    );
                $totalPrice = 0;
                foreach(Cart::instance('quotecart')->content() as $row){
                    $totalPrice = $totalPrice + $row->price * $row->qty;
                }
                $itemCount = Cart::instance('quotecart')->content()->count();
                return response(['status' => true, 'data' => '', 'message' => config('constants.QUOTE_ITEM_UPDATE_SUCCESS'),'itemPrice' => number_format($price,3),'totalPrice'=> $totalPrice,'itemCount' => $itemCount,'availableData '=> $availableData ]);
            }
        }catch (\Exception $ex) {
            return response(['status' => false, 'data' => $ex->getMessage()]);
        }
    }
    /**
     * Removed part from  quote cart
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */

    public function removeQuoteCartItem(Request $request){
        $data         = $request->all();
        $productId    = $request->product_id;
        Cart::instance('quotecart')->remove($productId);
        return response(['status' => true, 'data' => $data,'message'=> config('constants.ITEM_REMOVE_QUOTE_SUCCESS')]);
    }
    public function emptyQuoteCart(Request $request){
        Cart::instance('quotecart')->destroy();
        return response(['status' => true, 'data' => '','message'=> config('constants.QUOTE_LIST_EMPTY_SUCCESS')]);
    }

    /**
     * Save the quote cart parts in database
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function submitQuote(Request $request){ 
        DB::beginTransaction();
        try {
            $data = $request->all(); 
            $selectedItem = explode(',',$request->hiddenItem);
            if (Auth::user() && count($selectedItem)>0){ 
                $quote =  new Quote();
                $quote->user_id =  \Auth::user()->id;
                if(!empty($request->quoteName)){
                    $quote->is_unnamed =  0;
                    $quote->quote_name =  $request->quoteName;
                }else{
                    $quote->is_unnamed =  1;
                    $quote->quote_name = uniqid();
                }
                $quote->note = $request->note;
                $quote->save();
                $quote->id;
                
                $c = 0;
                
                
                foreach(Cart::instance('quotecart')->content() as $row) {
                   
                    if(in_array($row->rowId, $selectedItem)){
                        $quoteDetails = new QuoteDetail();
                        $quoteDetails->quote_id = $quote->id;
                        $quoteDetails->item_name = $row->name;
                        $quoteDetails->manufacturer_name = $row->options->manufacturer;
                        $quoteDetails->manufacturerCode = $row->options->manufacturerCode;
                        $quoteDetails->quantity = $row->qty;
                        $quoteDetails->price = $row->price;
                        $quoteDetails->source_part_id = $row->options->sourcePartId;
                        $quoteDetails->customer_part_id = $row->options->customerPartNo;
                        $quoteDetails->date_code = $row->options->dateCode;
                        $quoteDetails->price_tiers = json_encode($row->options->resaleList);
                        $quoteDetails->available_data = json_encode($row->options->availableData);
                        $quoteDetails->requested_quantity = $row->options->requestedQuantity;
                        $quoteDetails->requested_cost = $row->options->requestedCost;
                        $quoteDetails->distributor_name = $row->options->distributorName;
                        $quoteDetails->quote_type = $row->options->quoteType;
                        if($row->options->requestedDeliveryDate ==""){
                        }else{
                             $quoteDetails->requested_delivery_date  = date('Y-m-d' ,strtotime($row->options->requestedDeliveryDate));
                        }
                        $quoteDetails->save();
                        Cart::instance('quotecart')->remove($selectedItem[$c]);
                    }else{
                        continue;
                    }
                    $c++;
                }
                DB::commit();
                return redirect()->to('quotes')->with('success', config('constants.SUCCESS_QUOTE_SAVED'));
            }else{
                DB::rollBack();
                session(['previousroute' => 'quote-cart']);
                return redirect()->intended('/login')->with('failure', config('constants.LOGIN_REQUIRED_TO_QUOTE'));
            }
        }catch (\Exception $ex) {
            echo $ex->getMessage();
            echo $ex->getLine();
            die;
            DB::rollBack();
            return redirect()->intended('quote-cart')->with('failure',config('constants.COMMON_ERROR'));
        }
    }

    /**
     * Listing of Saved Quote under my quote section
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function quoteList(Request $request){
        try {
            $quoteData =   Quote::getQuoteListByUserId(Auth::user()->id, 5);
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($quoteData->toArray());
            // Define how many items we want to be visible in each page
            $perPage = 4;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['quotes']  =  $paginatedItems;
            $data['is_approved']  =  'saved';
            return view('frontend.pages.my-quotes',$data);
        }catch (\Exception $ex) {
          return redirect()->to('/')->with('failure',config('constants.COMMON_ERROR'));
        }
    }
    
    /**
     * Quote Detail of Saved Quote under my quote section
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function quotesDetails(Request $request, $quoteId){
        try {
            if(Auth::user()){
                $quoteInfo  = Quote::getQuoteInfoById($quoteId);
                if($quoteInfo->isEmpty()){
                    return redirect()->to('quotes')->with('failure',config('constants.QUOTE_NOT_EXIST'));
                }else{
                    $data['quoteInfo']  = $quoteInfo;
                    $data['quoteItems'] = Quote::getQuoteDetailById($quoteId);
                    return view('frontend.pages.quote-details',$data);
                }
            }else{
                return redirect()->to('/')->with('failure', config('constants.COMMON_ERROR'));
            }
        }catch (\Exception $ex) {
            return redirect()->to('quotes')->with('failure', config('constants.COMMON_ERROR'));
        }
    }

    /**
     * Delete Saved Quote under my quote section
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function removeQuote(Request $request){
        try {
            $data                   = $request->all();
            $id                     = $request->quoteId;
            $quote                  = Quote::findOrFail($id);
            $quote->deleted_at      =  date('Y-m-d h:i:s');
            if($quote->save()) {
                return response(['status' => true, 'message'=> config('constants.QUOTE_DELETE_SUCCESS'),'data' =>'']);
                
            } else {
                return response(['status' => true, 'message'=> config('constants.COMMON_ERROR'),'data' =>'']);
            }
        }catch (\Exception $ex) {
            return response(['status' => true, 'message'=> config('constants.COMMON_ERROR'),'data' =>'']);
        }
    }
    public function addQuoteToCart(Request $request){
        try {
            $data           = $request->all();
            $quoteId        = $request->quoteId;
            $quoteInfo  = Quote::getQuoteInfoById($quoteId);

            if($quoteInfo->isEmpty()){
                return redirect()->to('quotes')->with('failure',config('constants.QUOTE_NOT_EXIST'));
            }else{
                $data['quoteInfo']      = json_decode(json_encode($quoteInfo->toArray()),true);
                $quoteItems             = Quote::getQuoteDetailById($quoteId)->toArray();
                $data['quoteItems']     = json_decode(json_encode($quoteItems),true);
                //check if quote is approved so  no need to call the api again
                if($data['quoteInfo'][0]['is_approved'] == 1){
                    // check if part is already ordered then no need to re add in cart
                    foreach ($data['quoteItems'] as $key=>$partNumber){
                        $manufacturerPartNumber     =   $partNumber['item_name'];
                        $price_tiers                =   json_decode($partNumber['price_tiers'],true);
                        $available_data             =   json_decode($partNumber['available_data'],true);

                        if(!$partNumber['is_ordered']){
                            Cart::instance('shoppingcart')->add([
                                ['id' => $manufacturerPartNumber, 'name' => $manufacturerPartNumber,
                                    'qty' => $partNumber['approved_quantity'], 'price' => number_format($partNumber['approved_price'], 3, '.', ''), 'options' => ['distributorName' => $partNumber['distributor_name'],'manufacturer' => $partNumber['manufacturer_name'],
                                    'sourcePartId' => $partNumber['source_part_id'],
                                    'sourceMinQuantity'=>1,
                                    'resaleList' => $price_tiers, 'dateCode' => $partNumber['date_code'],
                                    'manufacturerCode' => $partNumber['manufacturerCode'], 'availableData' =>$available_data,
                                    'customerPartNo' => $partNumber['customer_part_id'],
                                    'stockAvailability' => 1, 'original_quantity' =>$partNumber['approved_quantity'],
                                    'quotedItem' => 1, 'isQuoteApprovedItem' => 1, 'quoteItemId' => $partNumber['id'], 'quoteId' => $partNumber['quote_id']]]
                            ]);
                        }
                    }
                    return redirect()->to('/cart')->with('success', config('constants.SUCCESS_QUOTE_ADD_TO_CART'));
                }
            }
        }catch (\Exception $ex) {
            return response(['status' => false, 'data' =>$ex->getMessage()]);
        }
    }
    
    public function addToCartFromQuote(Request $request){
        try {
            $data                   =   $request->all();
            $manufacturerPartNumber =   $request->productId;
            $sourcePartId           =   $request->sourcePartId;
            $manufacturerName       =   $request->manufacturer;
            $manufacturerCode       =   $request->manufacturerCode;
            $quantity               =   $request->quantity;
            $customerPartNo         =   $request->customerPartNo;
            $manufacturerName       =   $request->manufacturerName;
            $distributorName        =   $request->distributorName;
            $partArray              =  array(array('partNum'=>urlencode($manufacturerPartNumber)));
            $finalArray             =  json_encode($partArray);
            $finalProductData = array();
            $slugHolder = '';
            if($distributorName == 'arrow'){
                /*CALL ARROW API START*/
                $arrowData = $this->searchProductArrow($manufacturerPartNumber, $slugHolder, $partNum = null);

                if(!empty($arrowData)){
                    foreach($arrowData as $arr){
                        $finalProductData[] = $arr;
                    }
                }

            }else if($distributorName == 'orbweaver'){
                /*CALL ORBEWER API START*/
                $orweaverData = $this->searchProductOrbweaver($manufacturerPartNumber, $slugHolder, $partNum = null);
                if(!empty($orweaverData)){
                    foreach($orweaverData as $orwea){
                        $finalProductData[] = $orwea;
                    }
                }
            }else{
                /*CALL ARROW API START*/
                $arrowData = $this->searchProductArrow($manufacturerPartNumber, $slugHolder, $partNum = null);

                if(!empty($arrowData)){
                    foreach($arrowData as $arr){
                        $finalProductData[] = $arr;
                    }
                }
                
                /*CALL ORBEWER API START*/
                $orweaverData = $this->searchProductOrbweaver($manufacturerPartNumber, $slugHolder, $partNum = null);
                if(!empty($orweaverData)){
                    foreach($orweaverData as $orwea){
                        $finalProductData[] = $orwea;
                    }
                }
            }
            // using for token search end point
            array_walk_recursive($finalProductData, 'Helpers::update_price');
            $finalDataArray = [];
            
            if(!empty($finalProductData)){
                $matchedArrayData       =   [];
                foreach ($finalProductData as $partList){
                    if($partList['partNum']  == $manufacturerPartNumber && $partList['manufacturer'] == $manufacturerName) {
                        $matchedArrayData['itemId']              =  $partList['itemId'];
                        $matchedArrayData['partNum']             =  $partList['partNum'];
                        $matchedArrayData['manufacturer']        =  $partList['manufacturer'];
                        $matchedArrayData['mfrCd']               =  $partList['mfrCd'];
                        $matchedArrayData['desc']                =  $partList['desc'];
                        $matchedArrayData['EnvData']             =  $partList['EnvData'];
                        $matchedArrayData['sources']              =  $partList['sources'];
                        $matchedArrayData['distributorName']              =  $partList['distributorName'];
                        $finalDataArray[] = $matchedArrayData;
                    }
                }
            }else{
                return response(['status' => false, 'message'=> config('constants.COMMON_ERROR'),'data' =>$url]);
            }

            $itemArray = [];
            array_walk_recursive($finalDataArray, 'Helpers::update_price');

            foreach ($finalDataArray as $partData){
                //Check for available sources
                if(isset($partData['sources'])){

                    foreach($partData['sources'] as $sources) {
                        // get number of source part in each source and loop through it and check source part id and qty for that id
                        foreach ($sources['sourcesPart'] as $sourcePart) {
                            if($sourcePart['inStock'] == 1 ){
                                $availableInStockQuantity      =   $sourcePart['Availability'][0]['fohQty'];
                                $sourceMinQuantity              = $sourcePart['minimumOrderQuantity'];
                                $requiredQuantity              =   $quantity;
                                $originalQuantity = $quantity;
                                if($availableInStockQuantity >= $requiredQuantity){
                                    foreach ($sourcePart['Prices']['resaleList'] as $key => $prices) {
                                        if($requiredQuantity >= $prices['minQty'] && $requiredQuantity <= $prices['maxQty'] ){
                                            $itemArray['itemId']                 =  $partData['itemId'];
                                            $itemArray['partNum']                =  $partData['partNum'];
                                            $itemArray['manufacturer']           =  $partData['manufacturer'];
                                            $itemArray['mfrCd']                  =  $partData['mfrCd'];
                                            $itemArray['desc']                   =  $partData['desc'];
                                            $itemArray['packSize']               =  $sourcePart['packSize'];
                                            $itemArray['sourcePartNumber']       =  $sourcePart['sourcePartNumber'];
                                            $itemArray['sourcePartId']           =  $sourcePart['sourcePartId'];
                                            $itemArray['dateCode']               =  $sourcePart['dateCode'];
                                            $itemArray['manufacturerLeadTime']   =  $sourcePart['mfrLeadTime'];
                                            $itemArray['inStock']                =  $sourcePart['inStock'];
                                            $itemArray['Availability']           =  $sourcePart['Availability'];
                                            $itemArray['price']                  = $prices['price'];
                                            $itemArray['resaleList']             = $sourcePart['Prices']['resaleList'];
                                            $itemArray['quantityAdded']          = $requiredQuantity;
                                            $itemExist =  Cart::instance('shoppingcart')->content()->where('id', $partData['partNum'])->where('options.sourcePartId', $sourcePartId);
                                            if(empty(json_decode($itemExist))){
                                                Cart::instance('shoppingcart')->add([
                                                    ['id'=>$partData['partNum'],'name'=>$partData['partNum'],'qty'=>$requiredQuantity,'price'=>number_format($prices['price'], 3, '.', ''),'options'=>['manufacturer'=>$partData['manufacturer'],
                                                        'sourcePartId'=>$sourcePart['sourcePartId'], 'sourceMinQuantity'=> $sourceMinQuantity, 'resaleList' => $sourcePart['Prices']['resaleList'],'dateCode' =>$sourcePart['dateCode'],'manufacturerCode' => $partData['mfrCd'],'availableData' => $sourcePart['Availability'],'customerPartNo' => $customerPartNo,'stockAvailability'=> $sourcePart['inStock'],'original_quantity' => $originalQuantity,'quotedItem'=>0,'isQuoteApprovedItem'=>0,'quoteItemId'=>0,'quoteId'=>0,'distributorName' =>  $partData['distributorName']]]
                                                ]);

                                                $cartSubTotal= 0;
                                                foreach(Cart::instance('shoppingcart')->content() as $row){
                                                    $cartSubTotal = $cartSubTotal + $row->price * $row->qty;
                                                }
                                                $cartItemCount = Cart::instance('shoppingcart')->content()->count();

                                                //Add item in cart and response.
                                                return response(['status' => true, 'isOtherSource'=>0, 'message'=> config('constants.ITEM_ADDED_IN_CART_SUCCESS') ,'data' =>'','otherSourceData'=>'','cartSubTotal'=>number_format($cartSubTotal, 2, '.', ',') ,'cartItemCount'=>$cartItemCount]);
                                            }else{
                                                return response(['status' => false, 'isOtherSource'=>0, 'message'=> config('constants.ITEM_ALREADY_EXIST_IN_CART') ,'data' =>'','otherSourceData'=>'']);
                                            }
                                        }
                                    }
                                }else if($availableInStockQuantity < $requiredQuantity) {

                                    $originalQuantity = $requiredQuantity;
                                    foreach ($sourcePart['Prices']['resaleList'] as $key => $prices) {
                                        if($availableInStockQuantity >= $prices['minQty'] && $availableInStockQuantity <= $prices['maxQty'] ){
                                            $itemArray['itemId']                 =  $partData['itemId'];
                                            $itemArray['partNum']                =  $partData['partNum'];
                                            $itemArray['manufacturer']           =  $partData['manufacturer'];
                                            $itemArray['mfrCd']                  =  $partData['mfrCd'];
                                            $itemArray['desc']                   =  $partData['desc'];
                                            $itemArray['packSize']               =  $sourcePart['packSize'];
                                            $itemArray['sourcePartNumber']       =  $sourcePart['sourcePartNumber'];
                                            $itemArray['sourcePartId']           =  $sourcePart['sourcePartId'];
                                            $itemArray['dateCode']               =  $sourcePart['dateCode'];
                                            $itemArray['manufacturerLeadTime']   =  $sourcePart['mfrLeadTime'];
                                            $itemArray['inStock']                =  $sourcePart['inStock'];
                                            $itemArray['Availability']           =  $sourcePart['Availability'];
                                            $itemArray['price']                  =  $prices['price'];
                                            $itemArray['resaleList']             =  $sourcePart['Prices']['resaleList'];
                                            $itemArray['quantityAdded']          =  $availableInStockQuantity;
                                            //Add  the quantity which is found and then
                                            $itemExist =  Cart::instance('shoppingcart')->content()->where('id', $partData['partNum'])->where('options.sourcePartId', $sourcePart['sourcePartId']);
                                            if(empty(json_decode($itemExist))){
                                                Cart::instance('shoppingcart')->add([
                                                    ['id'=>$partData['partNum'],'name'=>$partData['partNum'],'qty'=>$availableInStockQuantity,'price'=> number_format($prices['price'], 3, '.', ''),'options'=>['manufacturer'=>$partData['manufacturer'],
                                                        'sourcePartId'=>$sourcePart['sourcePartId'],'sourceMinQuantity'=> $sourceMinQuantity,'resaleList' => $sourcePart['Prices']['resaleList'],'dateCode' =>$sourcePart['dateCode'],'manufacturerCode' => $partData['mfrCd'],'availableData' => $sourcePart['Availability'],'customerPartNo' => $customerPartNo,'stockAvailability'=> $sourcePart['inStock'],'original_quantity' => $originalQuantity,'quotedItem'=>0,'isQuoteApprovedItem'=>0,'quoteItemId'=>0,'quoteId'=>0,'distributorName' =>  $partData['distributorName']]]
                                                ]);
                                                //Create array with  all the source part id with  and other details like listing page here and response
                                                foreach ($finalDataArray as $otherSourcePartData) {
                                                    $productData = array();
                                                    $newSourceArray = [];
                                                    $productData['itemId']           = $otherSourcePartData['itemId'];
                                                    $productData['partNum']          = $otherSourcePartData['partNum'];
                                                    $productData['manufacturer']     = $otherSourcePartData['manufacturer'];
                                                    $productData['mfrCd']            = $otherSourcePartData['mfrCd'];
                                                    $productData['desc']             = $otherSourcePartData['desc'];
                                                    if (isset($otherSourcePartData['EnvData'])) {
                                                        $productData['EnvData'] = $otherSourcePartData['EnvData'];
                                                    }
                                                    foreach ($otherSourcePartData['sources'] as $otherSource) {
                                                        $otherSourceArray = array();
                                                        $otherSourcePartArray = [];
                                                        $otherSourceArray['displayName'] =  $otherSource['displayName'];
                                                        $otherSourceArray['sourceCd']    =  $otherSource['sourceCd'];
                                                        $otherSourceArray['currency']    =  $otherSource['currency'];
                                                        foreach($otherSource['sourcesPart'] as $sourcePart) {
                                                            if ($sourcePart['sourcePartId'] != $sourcePartId){
                                                                $sourcePartArray = array();
                                                                $sourcePartArray['packSize']             =  $sourcePart['packSize'];
                                                                $sourcePartArray['sourcePartNumber']     =  $sourcePart['sourcePartNumber'];
                                                                $sourcePartArray['sourcePartId']         =  $sourcePart['sourcePartId'];
                                                                $sourcePartArray['dateCode']             =  $sourcePart['dateCode'];
                                                                $sourcePartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                                                                $sourcePartArray['inStock']              =  $sourcePart['inStock'];
                                                                $sourcePartArray['Availability']         =  $sourcePart['Availability'];
                                                                if(isset($sourcePart['Prices'])){
                                                                    $sourcePartArray['Prices']              =  $sourcePart['Prices'];
                                                                }else{
                                                                    $sourcePartArray['Prices']              =  [];
                                                                }
                                                                $otherSourcePartArray[] =  $sourcePartArray;
                                                            }
                                                        }
                                                        $otherSourceArray['sourcesPart']   =   $otherSourcePartArray ;
                                                        $newSourceArray[] =  $otherSourceArray;
                                                    }
                                                    $productData['sources']   =   $newSourceArray ;
                                                }
                                                $cartSubTotal= 0;
                                                foreach(Cart::instance('shoppingcart')->content() as $row){
                                                    $cartSubTotal = $cartSubTotal + $row->price * $row->qty;
                                                }
                                                $cartItemCount = Cart::instance('shoppingcart')->content()->count();
                                                return response(['status' => false, 'isOtherSource'=>1 ,'message'=>'Found only '.$availableInStockQuantity.' Quantity in stock.Click below link to check from other sources.' ,'otherSourceData'=>$productData,'cartSubTotal'=>number_format($cartSubTotal, 2, '.', ',') ,'cartItemCount'=>$cartItemCount]);
                                            }else{
                                                return response(['status' => false, 'isOtherSource'=>0 , 'message'=>config('constants.ITEM_ALREADY_EXIST_IN_CART') ,'data' =>'','otherSourceData'=>'']);
                                            }

                                        }
                                    }
                                }
                            }else{
                                //Create array with  all the source part id with  and other details like listing page here and response
                                foreach ($finalDataArray as $otherSourcePartData) {
                                    $productData = array();
                                    $newSourceArray = [];
                                    $productData['itemId']           = $otherSourcePartData['itemId'];
                                    $productData['partNum']          = $otherSourcePartData['partNum'];
                                    $productData['manufacturer']     = $otherSourcePartData['manufacturer'];
                                    $productData['mfrCd']            = $otherSourcePartData['mfrCd'];
                                    $productData['desc']             = $otherSourcePartData['desc'];
                                    if (isset($otherSourcePartData['EnvData'])) {
                                        $productData['EnvData'] = $otherSourcePartData['EnvData'];
                                    }
                                    foreach ($otherSourcePartData['sources'] as $otherSource) {
                                        $otherSourceArray = array();
                                        $otherSourcePartArray = [];
                                        $otherSourceArray['displayName'] =  $otherSource['displayName'];
                                        $otherSourceArray['sourceCd']    =  $otherSource['sourceCd'];
                                        $otherSourceArray['currency']    =  $otherSource['currency'];
                                        foreach($otherSource['sourcesPart'] as $sourcePart) {
                                            if ($sourcePart['sourcePartId'] != $sourcePartId){
                                                $sourcePartArray = array();
                                                $sourcePartArray['packSize']             =  $sourcePart['packSize'];
                                                $sourcePartArray['sourcePartNumber']     =  $sourcePart['sourcePartNumber'];
                                                $sourcePartArray['sourcePartId']         =  $sourcePart['sourcePartId'];
                                                $sourcePartArray['dateCode']             =  $sourcePart['dateCode'];
                                                $sourcePartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                                                $sourcePartArray['inStock']              =  $sourcePart['inStock'];
                                                $sourcePartArray['Availability']         =  $sourcePart['Availability'];
                                                if(isset($sourcePart['Prices'])){
                                                    $sourcePartArray['Prices']              =  $sourcePart['Prices'];
                                                }else{
                                                    $sourcePartArray['Prices']              =  [];
                                                }
                                                $otherSourcePartArray[] =  $sourcePartArray;
                                            }
                                        }
                                        $otherSourceArray['sourcesPart']   =   $otherSourcePartArray ;
                                        $newSourceArray[] =  $otherSourceArray;
                                    }
                                    $productData['sources']   =   $newSourceArray ;
                                }
                                return response(['status' => false, 'isOtherSource'=>1, 'message'=> config('constants.ITEM_OUT_OF_STOCK')  ,'data' =>'','otherSourceData'=>$productData]);
                            }

                        }
                    }
                }else{
                    return response(['status' => false, 'isOtherSource'=>0 ,'data' =>'','message'=>config('constants.COMMON_ERROR') ]);
                }
            }
        }catch (\Exception $ex) {
            return response(['status' => false, 'line'=>$ex->getLine(),'data'=>$ex->getMessage()]);
        }
    }

    public function addApprovedQuoteItemToCartFromQuote(Request $request){
        $data                   =   $request->all();
        $manufacturerPartNumber =   $request->productId;
        $sourcePartId           =   $request->sourcePartId;
        $manufacturerName       =   $request->manufacturer;
        $manufacturerCode       =   $request->manufacturerCode;
        $customerPartNo         =   $request->customerPartNo;
        $dateCode               =   $request->dateCode;
        $manufacturerName       =   $request->manufacturerName;
        $isQuoteApprovedItem    =   $request->isApproved;
        $quoteApprovedPrice     =   $request->approvedPrice;
        $quoteApprovedQuantity  =   $request->approvedQuantity;
        $quotedItem             =   $request->isQuoted;
        $quoteItemId            =   $request->quoteItemId;
        $quoteId            =   $request->quoteId;
        Cart::instance('shoppingcart')->add([
            ['id'=>$manufacturerPartNumber,'name'=>$manufacturerPartNumber,'qty'=>$quoteApprovedQuantity,'price'=>
                number_format($quoteApprovedPrice, 3, '.', ''),'options'=>['manufacturer'=>$manufacturerName,
                'sourcePartId'=>$sourcePartId,'sourceMinQuantity'=>1,'resaleList' => [],
                'dateCode' =>$dateCode,'manufacturerCode' => $manufacturerCode,
                'availableData' => [],'customerPartNo' => $customerPartNo,
                'stockAvailability'=> 1,'original_quantity' => $quoteApprovedQuantity,
                'quotedItem'=>$quotedItem,'isQuoteApprovedItem'=>$isQuoteApprovedItem,
                'quoteItemId'=>$quoteItemId,'quoteId'=>$quoteId]]
        ]);
        return response(['status' => true,'data' => '','message' =>  config('constants.SUCCESS_QUOTE_ADD_TO_CART')]);
    }

    /**
     * Quote Detail of Saved Quote under my quote section
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function addAllQuoteItemsToCart(Request $request){
        try {
            $data   =   $request->all();
            $finalResponseArray  =  [];
            $m = 0;
            $addToCartArray = [];
            foreach($data as $key => $val) {
                foreach($val as $originalKey => $value){
                    if($value['method'] == "addApprovedQuoteItemToCartFromQuote"){
                        $manufacturerPartNumber =   $value['itemname'];
                        $sourcePartId           =   $value['sourcepartid'];
                        $manufacturerName       =   $value['manufacturername'];
                        $manufacturerCode       =   $value['manufacturercode'];
                        $customerPartNo         =   $value['customerpartid'];
                        $dateCode               =   $value['datecode'];
                        $manufacturerName       =   $value['manufacturername'];
                        $isQuoteApprovedItem    =   $value['isapproved'];
                        $quoteApprovedPrice     =   $value['approvedprice'];
                        $quoteApprovedQuantity  =   $value['approvedquantity'];
                        $quotedItem             =   $value['isquoted'];
                        $quoteItemId            =   $value['id'];
                        $quoteId                =   $value['quoteinfoid'];
                        Cart::instance('shoppingcart')->add([
                            ['id'=>$manufacturerPartNumber,'name'=>$manufacturerPartNumber,'qty'=>$quoteApprovedQuantity,'price'=>
                                number_format($quoteApprovedPrice, 3, '.', ''),'options'=>['manufacturer'=>$manufacturerName,
                                'sourcePartId'=>$sourcePartId,'sourceMinQuantity'=>1,'resaleList' => [],
                                'dateCode' =>$dateCode,'manufacturerCode' => $manufacturerCode,
                                'availableData' => [],'customerPartNo' => $customerPartNo,
                                'stockAvailability'=> 1,'original_quantity' => $quoteApprovedQuantity,
                                'quotedItem'=>$quotedItem,'isQuoteApprovedItem'=>$isQuoteApprovedItem,
                                'quoteItemId'=>$quoteItemId,'quoteId'=>$quoteId]]
                        ]);
                        foreach(Cart::instance('shoppingcart')->content() as $row){                                 $addToCartArray[$m] = $row->rowId;
                        }
                    }
                }
            }
            return response(['status' => true,'data' => '','message' =>  config('constants.SUCCESS_QUOTE_ADD_TO_CART')]);
        }catch (\Exception $ex) {
            return response(['status' => false, 'line'=>$ex->getLine(),'data'=>$ex->getMessage()]);
        }
    }

    /**
     * Search similar parts when add part from quote to cart
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function searchSimilarParts(Request $request){

         $data                      =   $request->all();
         try{
             $data                  =   $request->all();
             $parts                 =   $request->input('partNumber');
             $manufacturerName      =   $request->input('manufacturerName');
             $sourcePartId          =   $request->input('sourcePartId');
             $partArray             =   array(array('partNum'=>urlencode($parts),'mfr'=>$manufacturerName));
             $finalArray            =   json_encode($partArray);
             $url                   =   config('constants.ARROW_API_URL').'search/list?req={"request":{"login":"'.config('constants.ARROW_LOGIN_NAME').'","apikey":"'.config('constants.ARROW_API_KEY').'","remoteIp":"'.$_SERVER['REMOTE_ADDR'].'","useExact":"true","parts":'.$finalArray.'}}&rows=25';
             $response              =   Helpers::curlRequest($url);
             $finalProductData=[];
             $result                =   json_decode($response,true);
             if(!empty($result['itemserviceresult']['data'][0]['resultList'][0]['PartList']) && $result['itemserviceresult']['transactionArea'][0]['response']['returnCode']==0 && $result['itemserviceresult']['transactionArea'][0]['response']['success']==1) {
                 $resultListData =$result['itemserviceresult']['data'][0]['resultList'][0]['PartList'];
                 foreach ($resultListData as $partList){
                     $productData=array();
                     $newSourceArray = [];
                     $productData['itemId']              =  $partList['itemId'];
                     $productData['partNum']             =  $partList['partNum'];
                     $productData['requestedQty']        =   '';
                     $productData['manufacturer']        =  $partList['manufacturer']['mfrName'];
                     $productData['mfrCd']               =  $partList['manufacturer']['mfrCd'];
                     $productData['desc']                =  $partList['desc'];
                     if(isset($partList['EnvData'])){
                         $productData['EnvData']   =  $partList['EnvData'];
                     }
                     $newSourceCombineArray = array();
                     $newIndex = 0;
                     if(isset($partList['InvOrg']['sources'])){
                         $productData['sources_count']   =  count($partList['InvOrg']['sources']);
                         //Combining all the source parts
                         foreach($partList['InvOrg']['sources'] as $sources) {
                             $sourceArray = array();
                             $newSourcePartArray = [];

                             $sourceArray['displayName'] =$sources['displayName'];
                             $sourceArray['sourceCd'] =  $sources['sourceCd'];
                             $sourceArray['currency'] =  $sources['currency'];

                             $newSourceCombineArray['displayName'] =$sources['displayName'];
                             $newSourceCombineArray['sourceCd'] =  $sources['sourceCd'];
                             $newSourceCombineArray['currency'] =  $sources['currency'];

                             $i=0;
                             foreach($sources['sourceParts'] as $sourcePart) {
                                     $newSourceCombineArray['sourcesPart'][] = $sourcePart;
                                     $i++;
                                     $sourcePartArray = array();
                                     $sourcePartArray['packSize']         =  $sourcePart['packSize'];
                                     $sourcePartArray['sourcePartNumber']    =  $sourcePart['sourcePartNumber'];
                                     $sourcePartArray['sourcePartId']        =  $sourcePart['sourcePartId'];
                                     $sourcePartArray['dateCode']            =  $sourcePart['dateCode'];
                                     $sourcePartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                                     $sourcePartArray['inStock']             =  $sourcePart['inStock'];
                                     $sourcePartArray['Availability']        =  $sourcePart['Availability'];
                                     if(isset($sourcePart['Prices'])){
                                         $sourcePartArray['Prices']              =  $sourcePart['Prices'];
                                     }else{
                                         $sourcePartArray['Prices']              =  [];
                                     }
                                     $newSourcePartArray[] =  $sourcePartArray;

                             }
                             $sourceArray['sourcesPart']   =   $newSourcePartArray ;
                             $newSourceArray[] =  $sourceArray;
                         }
                         $productData['sources'][]   =   $newSourceCombineArray ;
                     }
                     $finalProductData[]=$productData;
                     session(['productDetails' => $finalProductData]);
                 }
                 array_walk_recursive($finalProductData, 'Helpers::update_price');

                 $data['finalData'] =  $finalProductData;
                 $data['sourcePartId'] =  $sourcePartId;
                 return view('frontend.products.listing',$data);
             }else{
                 return view('frontend.products.no-product-found');
             }
         } catch (\Exception $ex) {
             return redirect()->to('quotes')->with('failure', config('constants.COMMON_ERROR'));
         }
     }
    /**
     * Download saved quote in pdf
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function downloadQuote(Request $request,$quoteId){
        $data                   = $request->all();
        try{
            $quote              = Quote::findOrFail($quoteId);
            $data['quoteInfo']  = Quote::getQuoteInfoById($quoteId);
            $data['quoteItems'] = Quote::getQuoteDetailById($quoteId);
            $pdf                = PDF::loadView('pdfviews/download-quote', $data);
            if($data['quoteInfo'][0]->quote_name){
                return $pdf->download('quote-'. $data['quoteInfo'][0]->quote_name.'.pdf');
            }else{
                return $pdf->download('quote-'.$quoteId.'.pdf');
            }
        }catch (\Exception $ex) {
            return redirect()->to('quotes')->with('failure', config('constants.COMMON_ERROR'));
        }
    }
    /**
     * Add similar searched part
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function addSimilarPartsToCart(Request $request){
        $data                   =   $request->all();
        try{
            $stockQty           = $request->stockQty;
            $productId          = $request->productId;
            $manufacturer       = $request->manufacturer;
            $manufacturerCode   =  $request->mfrCd;
            $sourcePartId       = $request->sourcePartId;
            $quantity           = $request->quantity;
            $dateCode           = $request->dateCode;
            $customerPartNo     = $request->customerPartNo;
            $stockAvailability  = $request->stockAvailability;
            $tierPrices         = $request->tierPrices;
            $availableData      = $request->Availability;
            $minQty             = 0;

            $itemExist =  Cart::instance('shoppingcart')->content()->where('id', $productId)->where('options.sourcePartId', $sourcePartId);
            if(empty(json_decode($itemExist))){
                $price = 0;
                $resaleList = array();
                foreach ($tierPrices['resaleList'] as $key => $prices) {
                    if($quantity >= $prices['minQty'] && $quantity <= $prices['maxQty'] ){
                        $price = $prices['price'];
                        $resaleList = $tierPrices['resaleList'];
                        break;
                    }
                }
                $minQty = $tierPrices['resaleList'][0]['minQty'];
                if($quantity < $minQty){
                    return response(['status' => false,  'data' => '', 'message' => 'Required Minimum quantity: '.$minQty, 'minQty' => $minQty]);
                }
                $originalQuantity = $quantity;
                Cart::instance('shoppingcart')->add([
                    ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,'original_quantity' => $originalQuantity,'quotedItem'=>0,'isQuoteApprovedItem'=>0,'quoteItemId'=>0,'quoteId'=>0]]
                ]);

                return response(['status' => true, 'message'=> config('constants.ITEM_ADD_TO_CART_SUCCESS'),'data' =>'']);
            }else{
                return response(['status' => false, 'message'=> config('constants.ITEM_ALREADY_EXIST_IN_CART'),'data' =>'']);
            }
        }catch (\Exception $ex) {
            return response(['status' => false, 'message'=> config('constants.COMMON_ERROR'),'data' =>'']);
        }
    }
    public function moveCartToQuote(Request $request){
       print_r( Cart::instance('shoppingcart')->content());
    }
    public function addUnSavedQuoteToCart(Request $request){
        $productDetails ='';
        try {
            $data               = $request->all();
            $productId          = $request->productId;
            $distributorName = $request->distributorName;
            $manufacturer       = $request->manufacturer;
            $sourcePartId       = $request->sourcePartId;
            $quantity           = $request->quantity;
            $dateCode           = $request->dateCode;
            $customerPartNo     = $request->customerPartNo;
            $stockAvailability  = $request->stockAvailability;
            $sourceMinQuantity = $request->sourceMinQuantity;
            $price              = 0;
            $resaleList         = array();
            $availableData      = array();
            $dateCode           = '';
            $manufacturerCode   = $request->mfrCd;
            $availableStock     = 0;
            $minQty             = 0;

            /* Check item is exist on cart or not  */
            $itemExist =  Cart::instance('shoppingcart')->content()->where('id', $productId)->where('options.sourcePartId', $sourcePartId);
            if(empty(json_decode($itemExist))){
                $price = 0;
                $resaleList = array();
                /* Price calculation on the bases of Qty */
                $productDetails = Session::get('productDetails');
                $isItemExistInQuote =  Cart::instance('quotecart')->content()->where('id', $productId)->where('options.sourcePartId', $sourcePartId);
                foreach ($isItemExistInQuote as $index => $data) {
                    $rowId            = $data->rowId;
                    $resaleList       = $data->options->resaleList;
                    $dateCode         = $data->options->dateCode;
                    $availableData    = $data->options->availableData;
                    $avlDat           = $availableData[0];
                    $availableStock   = $avlDat->fohQty;
                    $sourceMinQuantity = $data->options->sourceMinQuantity;
                }
                /* Check available stock of items */
                $originalQuantity = $quantity;

                if( $availableStock <  $quantity){
                    $quantity   =  $availableStock ;
                }
                foreach ($resaleList as $key => $prices) {
                    if($quantity >= $prices->minQty && $quantity <= $prices->maxQty ){
                        $price = $prices->price;
                        break;
                    }
                }
                /* Check mim quantity of items */
                $minQtyArray =  $resaleList[0];
                $minQty = $minQtyArray->minQty;

                /* Check mim quantity of items */
                if($quantity < $minQty){
                    return response(['status' => false,  'line'=>1, 'minQtyError' => true, 'data' => '', 'message' => 'Minimum order qty for part number '.$productId.' is '.$minQty.', We request you to check Buying Options first','minQty' => $minQty]);
                }

                if($quantity <= $availableStock){
                    Cart::instance('shoppingcart')->add([
                        ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'sourceMinQuantity'=>$sourceMinQuantity,'resaleList' =>json_decode(json_encode($data->options->resaleList),true) ,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => json_decode(json_encode($data->options->availableData),true),'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,'original_quantity' => $originalQuantity,'quotedItem'=>0,'isQuoteApprovedItem'=>0,'quoteItemId'=>0,'quoteId'=>0, 'distributorName' => $distributorName]]
                    ]);
                }else{
                    if($availableStock == 0){
                        return response(['status' => false, 'line'=>2, 'data' => '', 'message' => 'Item is out of stock.']);
                    }else{
                        return response(['status' => false, 'line'=>3, 'stockError' => true, 'data' => '', 'message' => 'Availabile stock: '.$availableStock,'stock' => $availableStock]);
                    }
                }
                $totalPrice = 0;
                foreach(Cart::instance('shoppingcart')->content() as $row){
                    $totalPrice = $totalPrice + $row->price * $row->qty;
                }
                $itemCount = Cart::instance('shoppingcart')->content()->count();
                return response(['status' => true,'data' => '','itemPrice' => number_format($price, 3, '.', ''),'totalPrice'=> $totalPrice,'itemCount'=>$itemCount,'message' => config('constants.SUCCESS_QUOTE_ADD_TO_CART'),'availableData'=>$availableData ,'productDetails'=>$productDetails]);
            }else{
                /* Item Update in the cart */
                foreach ($itemExist as $index => $data) {
                    $rowId                  = $data->rowId;
                    $resaleList             = $data->options->resaleList;
                    $dateCode               = $data->options->dateCode;
                    $availableData          = $data->options->availableData;
                    $availableStock         = $availableData[0]['fohQty'];
                    $sourceMinQuantity       = $data->options->sourceMinQuantity;
                }

                foreach ($resaleList as $key => $prices) {
                    if($quantity >= $prices['minQty'] && $quantity <= $prices['maxQty'] ){
                        $price = $prices['price'];
                        break;
                    }
                }
                /* Check mim quantity of items */
                $minQty = $resaleList[0]['minQty'];
                if($quantity < $minQty){
                    return response(['status' => false, 'line'=>4, 'minQtyError' => true, 'data' => '', 'message' => 'Minimum order qty for part number '.$productId.' is '.$minQty.', We request you to check Buying Options first','minQty' => $minQty]);
                }
                /* Check available stock of items */
                $originalQuantity = $quantity;
                if($quantity > $availableStock){
                    $quantity = $availableStock;
                }
                if($quantity <= $availableStock){
                    Cart::instance('shoppingcart')->update($rowId,
                        ['id'=>$productId,'name'=>$productId,'qty'=>$quantity,'price'=>number_format($price, 3, '.', ''),'options'=>['manufacturer'=>$manufacturer,'sourcePartId'=>$sourcePartId,'sourceMinQuantity'=>$sourceMinQuantity,'resaleList' => $resaleList,'dateCode' => $dateCode,'manufacturerCode' => $manufacturerCode,'availableData' => $availableData,'customerPartNo' => $customerPartNo,'stockAvailability'=>$stockAvailability,'original_quantity' => $originalQuantity,'quotedItem'=>0,'isQuoteApprovedItem'=>0,'quoteItemId'=>0,'quoteId'=>0, 'distributorName' => $distributorName]]
                    );
                }else{
                    if($availableStock == 0){
                        return response(['status' => false, 'line'=>5,'data' => '', 'message' => 'Item is out of stock.']);
                    }else{
                        return response(['status' => false,  'line'=>6,'stockError' => true, 'data' => '', 'message' => 'Availabile stock: '.$availableStock,'stock' => $availableStock]);
                    }
                }
                $totalPrice = 0;
                foreach(Cart::instance('shoppingcart')->content() as $row){
                    $totalPrice = $totalPrice + $row->price * $row->qty;
                }
                $itemCount = Cart::instance('shoppingcart')->content()->count();
                return response(['status' => true, 'data' => '', 'message' => config('constants.SUCCESS_QUOTE_UPDATE_TO_CART'),'itemPrice' => number_format($price,3),'totalPrice'=> $totalPrice,'itemCount' => $itemCount,'availableData '=> $availableData ]);
            }
        } catch (\Exception $ex) {
            return response(['status' => false, 'line'=>$ex->getLine(),'data'=>$ex->getMessage()]);
        }
    }


/*
    * Listing of pending Quote under my quote section
    * @param  Request $request
    * @return \Illuminate\Http\Response
 */
    public function pendingQuoteList(Request $request){
        try {
            $quoteData =   Quote::getQuoteListByUserId(Auth::user()->id, '0');
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($quoteData->toArray());
            // Define how many items we want to be visible in each page
            $perPage = 4;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['quotes']  =  $paginatedItems;
            $data['is_approved']  =  '0';
            return view('frontend.pages.my-quotes',$data);
        }catch (\Exception $ex) {
          return redirect()->to('/')->with('failure',config('constants.COMMON_ERROR'));
        }
    }//End Function

/*
    * Listing of approved Quote under my quote section
    * @param  Request $request
    * @return \Illuminate\Http\Response
 */
    public function approvedQuoteList(Request $request){
        try {
            $quoteData =   Quote::getQuoteListByUserId(Auth::user()->id, 1);
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($quoteData->toArray());
            // Define how many items we want to be visible in each page
            $perPage = 4;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['quotes']  =  $paginatedItems;
            $data['is_approved']  =  1;
            return view('frontend.pages.my-quotes',$data);
        }catch (\Exception $ex) {
          return redirect()->to('/')->with('failure',config('constants.COMMON_ERROR'));
        }
    }//End Function

    /*
        * Listing of rejected Quote under my quote section
        * @param  Request $request
        * @return \Illuminate\Http\Response
    */
    public function rejectedQuoteList(Request $request){
        try {
            $quoteData =   Quote::getQuoteListByUserId(Auth::user()->id, 2);
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($quoteData->toArray());
            // Define how many items we want to be visible in each page
            $perPage = 4;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['quotes']  =  $paginatedItems;
            $data['is_approved']  =  2;
            return view('frontend.pages.my-quotes',$data);
        }catch (\Exception $ex) {
          return redirect()->to('/')->with('failure',config('constants.COMMON_ERROR'));
        }
    }//End Function

    /*
        * Listing of expired Quote under my quote section
        * @param  Request $request
        * @return \Illuminate\Http\Response
    */
    public function expiredQuoteList(Request $request){
        try {
            $quoteData =   Quote::getQuoteListByUserId(Auth::user()->id, 3);
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($quoteData->toArray());
            // Define how many items we want to be visible in each page
            $perPage = 4;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['quotes']  =  $paginatedItems;
            $data['is_approved']  =  3;
            return view('frontend.pages.my-quotes',$data);
        }catch (\Exception $ex) {
          return redirect()->to('/')->with('failure',config('constants.COMMON_ERROR'));
        }
    }//End Function

    /*
        * Listing of ordered Quote under my quote section
        * @param  Request $request
        * @return \Illuminate\Http\Response
    */
    public function orderedQuoteList(Request $request){
        try {
            $quoteData =   Quote::getQuoteListByUserId(Auth::user()->id, 4);
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($quoteData->toArray());
            // Define how many items we want to be visible in each page
            $perPage = 4;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['quotes']  =  $paginatedItems;
            $data['is_approved']  =  4;
            return view('frontend.pages.my-quotes',$data);
        }catch (\Exception $ex) {
          return redirect()->to('/')->with('failure',config('constants.COMMON_ERROR'));
        }
    }//End Function

    /**
     * Listing of requested lead time Quote under requested lead time section
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function RequestedLeadTimes(Request $request){
        try {
            $quoteData =   Quote::getRLTQuoteListByUserId(Auth::user()->id);
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($quoteData->toArray());
            // Define how many items we want to be visible in each page
            $perPage = 4;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['quotes']  =  $paginatedItems;
            $data['title']  =  'Requested Lead Times';
            $data['notFoundMsg'] = 'It seems, there is no Requested Lead Time.';
            return view('frontend.pages.requested-lead-time',$data);
        }catch (\Exception $ex) {
          return redirect()->to('/')->with('failure',$ex->getMessage());
        }
    }

     /**
     * Listing of responded lead time Quote under responded lead time section
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function RespondedLeadTimes(Request $request){
        try {
            $quoteData =   Quote::getRespondedLTQuoteListByUserId(Auth::user()->id);
            // Get current page form url e.x. &page=1
            $currentPage = LengthAwarePaginator::resolveCurrentPage();
            // Create a new Laravel collection from the array data
            $itemCollection = collect($quoteData->toArray());
            // Define how many items we want to be visible in each page
            $perPage = 4;
            // Slice the collection to get the items to display in current page
            $currentPageItems = $itemCollection->slice(($currentPage * $perPage) - $perPage, $perPage)->all();
            // Create our paginator and pass it to the view
            $paginatedItems= new LengthAwarePaginator($currentPageItems , count($itemCollection), $perPage);
            // set url path for generted links
            $paginatedItems->setPath($request->url());
            $data['quotes']  =  $paginatedItems;
            $data['title']  =  'Responded Lead Times';
            $data['notFoundMsg'] = 'It seems, there is no Responded Lead Time.';
            return view('frontend.pages.requested-lead-time',$data);
        }catch (\Exception $ex) {
          return redirect()->to('/')->with('failure',$ex->getMessage());
        }
    }

    public function searchProductArrow($partNumber, $slug_holder, $partNum = null){

        if($partNum != null){
            $partNumber = $partNum;
        }else{
            $partNumber     = $partNumber;
        }
        $partArray      = array (array('partNum'=>urlencode($partNumber)));
        $finalArray     =  json_encode($partArray);
        $partNumberLength = strlen($partNumber);

        if($partNumberLength <= 3){
            $url= config('constants.ARROW_API_URL').'search/list?req={"request":{"login":"'.config('constants.ARROW_LOGIN_NAME').'","apikey":"'.config('constants.ARROW_API_KEY').'","remoteIp":"'.$_SERVER['REMOTE_ADDR'].'","useExact":"true","parts":'.$finalArray.'}}&rows=25';
            $response               = Helpers::curlRequest($url);
            $finalProductData       = [];
            $result                 = json_decode($response,true);

            if(!empty($result['itemserviceresult']['data'][0]['resultList'][0]['PartList']) && $result['itemserviceresult']['transactionArea'][0]['response']['returnCode']==0 && $result['itemserviceresult']['transactionArea'][0]['response']['success']==1) {
                $resultListData =$result['itemserviceresult']['data'][0]['resultList'][0]['PartList'];
                foreach ($resultListData as $partList){
                    $productData=array();
                    $newSourceArray = [];
                    $productData['distributorName']              =  'Arrow';
                    $productData['itemId']              =  $partList['itemId'];
                    $productData['partNum']             =  $partList['partNum'];
                    $productData['requestedQty']        =   '';
                    $productData['manufacturer']        =  $partList['manufacturer']['mfrName'];
                    $productData['mfrCd']               =  $partList['manufacturer']['mfrCd'];
                    $productData['desc']                =  $partList['desc'];
                    if(isset($partList['EnvData'])){
                        $productData['EnvData']   =  $partList['EnvData'];
                    }
                    $newSourceCombineArray = array();
                    if(isset($partList['InvOrg']['sources'])){
                        $productData['sources_count']   =  count($partList['InvOrg']['sources']);
                        foreach($partList['InvOrg']['sources'] as $sources) {
                            $sourecArray = array();
                            $newSourcePartArray = [];
                            $sourecArray['displayName'] =$sources['displayName'];
                            $sourecArray['sourceCd'] =  $sources['sourceCd'];
                            $sourecArray['currency'] =  $sources['currency'];
                            $newSourceCombineArray['displayName'] =     $sources['displayName'];
                            $newSourceCombineArray['sourceCd'] =  $sources['sourceCd'];
                            $newSourceCombineArray['currency'] =  $sources['currency'];
                            foreach($sources['sourceParts'] as $sourcePart) {
                                $newSourceCombineArray['sourcesPart'][] = $sourcePart;
                                $sourecPartArray = array();
                                $sourecPartArray['packSize']         =  $sourcePart['packSize'];
                                $sourecPartArray['sourcePartNumber']    =  $sourcePart['sourcePartNumber'];
                                $sourecPartArray['sourcePartId']        =  $sourcePart['sourcePartId'];
                                $sourecPartArray['dateCode']            =  $sourcePart['dateCode'];
                                $sourecPartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                                $sourecPartArray['inStock']             =  $sourcePart['inStock'];
                                $sourecPartArray['Availability']        =  $sourcePart['Availability'];
                                if(isset($sourcePart['Prices'])){
                                    $sourecPartArray['Prices']              =  $sourcePart['Prices'];
                                }else{
                                    $sourecPartArray['Prices']              =  [];
                                }
                                $newSourcePartArray[] =  $sourecPartArray;
                            }
                            $sourecArray['sourcesPart']   =   $newSourcePartArray ;
                            $newSourceArray[] =  $sourecArray;
                        }
                        $productData['sources'][]   =   $newSourceCombineArray ;
                    }
                    $finalProductData[]=$productData;
                }
                return $finalProductData;
            }

        }else{

            $slugHolder = $slug_holder;
            $dt = [];
            if(!empty($slugHolder)){
                Session(['search_part_name' => $partNumber]);
                return redirect('/category/'.$slugHolder);
            }
            // using for token search end point
            $url= config('constants.ARROW_API_URL').'search/list?req={"request":{"login":"'.config('constants.ARROW_LOGIN_NAME').'","apikey":"'.config('constants.ARROW_API_KEY').'","remoteIp":"'.$_SERVER['REMOTE_ADDR'].'","useExact":"true","parts":'.$finalArray.'}}&rows=25';

            $response           =   Helpers::curlRequest($url);
            $finalProductData   =   [];
            $result             =   json_decode($response,true);
            if($result['itemserviceresult']['data']){
                if(!empty($result['itemserviceresult']['data'][0]['resultList'][0]['PartList']) && $result['itemserviceresult']['transactionArea'][0]['response']['returnCode']==0 && $result['itemserviceresult']['transactionArea'][0]['response']['success']==1) {
                    $resultListData =$result['itemserviceresult']['data'][0]['resultList'][0]['PartList'];
                    // using for token search end point
                    foreach ($resultListData as $partList){
                        $productData=array();
                        $newSourceArray = [];
                        $productData['distributorName']              =  'Arrow';
                        $productData['itemId'] = $partList['itemId'];
                        $productData['partNum'] = $partList['partNum'];
                        $productData['requestedQty'] = '';
                        $productData['manufacturer'] = $partList['manufacturer']['mfrName'];
                        $productData['mfrCd'] = $partList['manufacturer']['mfrCd'];
                        $productData['desc'] =  $partList['desc'];
                        if(isset($partList['EnvData'])){
                            $productData['EnvData'] = $partList['EnvData'];
                        }
                        $newSourceCombineArray = array();
                        $newIndex = 0;
                        if(isset($partList['InvOrg']['sources'])){
                            $productData['sources_count']   =  count($partList['InvOrg']['sources']);
                            //Combining all the source parts
                            foreach($partList['InvOrg']['sources'] as $sources) {
                                $sourceArray = array();
                                $newSourcePartArray = [];
                                $sourceArray['displayName'] =$sources['displayName'];
                                $sourceArray['sourceCd'] =  $sources['sourceCd'];
                                $sourceArray['currency'] =  $sources['currency'];

                                $newSourceCombineArray['displayName'] =$sources['displayName'];
                                $newSourceCombineArray['sourceCd'] =  $sources['sourceCd'];
                                $newSourceCombineArray['currency'] =  $sources['currency'];

                                $i=0;
                                foreach($sources['sourceParts'] as $sourcePart) {
                                    $newSourceCombineArray['sourcesPart'][] = $sourcePart;
                                    $i++;
                                    $sourcePartArray = array();
                                    $sourcePartArray['packSize']         =  $sourcePart['packSize'];
                                    $sourcePartArray['sourcePartNumber']    =  $sourcePart['sourcePartNumber'];
                                    $sourcePartArray['sourcePartId']        =  $sourcePart['sourcePartId'];
                                    $sourcePartArray['dateCode']            =  $sourcePart['dateCode'];
                                    $sourcePartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                                    $sourcePartArray['inStock']             =  $sourcePart['inStock'];
                                    $sourcePartArray['Availability']        =  $sourcePart['Availability'];
                                    if(isset($sourcePart['Prices'])){
                                        $sourcePartArray['Prices']              =  $sourcePart['Prices'];
                                    }else{
                                        $sourcePartArray['Prices']              =  [];
                                    }
                                    $newSourcePartArray[] =  $sourcePartArray;
                                }
                                $sourceArray['sourcesPart']   =   $newSourcePartArray ;
                                $newSourceArray[] =  $sourceArray;
                            }
                            $productData['sources'][]   =   $newSourceCombineArray ;
                        }
                    }
                    $finalProductData[]=$productData;
                    return $finalProductData;
                }else{
                    $url  = config('constants.ARROW_API_URL').'search/token?login='.config('constants.ARROW_LOGIN_NAME').'&apikey='.config('constants.ARROW_API_KEY').'&remoteIp='.$_SERVER['REMOTE_ADDR'].'&search_token='.$partNumber.'&rows=25&start=1';

                    $response = Helpers::curlRequest($url);
                    $finalProductData = [];
                    $result = json_decode($response,true);
                    if($result['itemserviceresult']['transactionArea'][0]['response']['returnCode']==0 && $result['itemserviceresult']['transactionArea'][0]['response']['success']==1){
                        $resultListData =$result['itemserviceresult']['data'][0]['PartList'];
                        foreach ($resultListData as $partList){
                            $productData=array();
                            $newSourceArray = [];
                            $productData['distributorName'] =  'Arrow';
                            $productData['itemId'] = $partList['itemId'];
                            $productData['partNum'] = $partList['partNum'];
                            $productData['requestedQty'] = '';
                            $productData['manufacturer'] = $partList['manufacturer']['mfrName'];
                            $productData['mfrCd'] = $partList['manufacturer']['mfrCd'];
                            $productData['desc'] = $partList['desc'];
                            if(isset($partList['EnvData'])){
                                $productData['EnvData'] = $partList['EnvData'];
                            }
                            $newSourceCombineArray = array();
                            $newIndex = 0;
                            if(isset($partList['InvOrg']['sources'])){
                                $productData['sources_count']   =  count($partList['InvOrg']['sources']);
                                foreach($partList['InvOrg']['sources'] as $sources) {
                                    $sourecArray = array();
                                    $newSourcePartArray = [];
                                    $sourecArray['displayName'] =$sources['displayName'];
                                    $sourecArray['sourceCd'] =  $sources['sourceCd'];
                                    $sourecArray['currency'] =  $sources['currency'];
                                    $newSourceCombineArray['displayName'] =$sources['displayName'];
                                    $newSourceCombineArray['sourceCd'] =  $sources['sourceCd'];
                                    $newSourceCombineArray['currency'] =  $sources['currency'];
                                    $i=0;
                                    foreach($sources['sourceParts'] as $sourcePart) {
                                        $newSourceCombineArray['sourcesPart'][] = $sourcePart;
                                        $i++;
                                        $sourecPartArray = array();
                                        $sourecPartArray['packSize'.$i] =  $sourcePart['packSize'];
                                        $sourecPartArray['sourcePartNumber'] =  $sourcePart['sourcePartNumber'];
                                        $sourecPartArray['sourcePartId'] =  $sourcePart['sourcePartId'];
                                        $sourecPartArray['dateCode'] =  $sourcePart['dateCode'];
                                        $sourecPartArray['manufacturerLeadTime'] =  $sourcePart['mfrLeadTime'];
                                        $sourecPartArray['inStock'] =  $sourcePart['inStock'];
                                        $sourecPartArray['Availability'] =  $sourcePart['Availability'];
                                        if(isset($sourcePart['Prices'])){
                                            $sourecPartArray['Prices'] =  $sourcePart['Prices'];
                                        }else{
                                            $sourecPartArray['Prices'] =  [];
                                        }
                                        $newSourcePartArray[] = $sourecPartArray;
                                    }
                                    $sourecArray['sourcesPart'] = $newSourcePartArray ;
                                    $newSourceArray[] = $sourecArray;
                                }
                                $productData['sources'][] = $newSourceCombineArray ;
                            }
                            $finalProductData[]=$productData;
                        }
                        return $finalProductData;
                    }
                }
            }
        }
    }

    public function searchProductOrbweaver($partNumber, $slug_holder, $partNum = null){
        if($partNum != null){
            $partNumber = $partNum;
        }else{
            $partNumber     = $partNumber;
        }
        $partArray      = array (array('partNum'=>urlencode($partNumber)));
        $finalArray     =  json_encode($partArray);
        $partNumberLength = strlen($partNumber);
        $slugHolder = $slug_holder;
        $dt = [];
        if(!empty($slugHolder)){
            Session(['search_part_name' => $partNumber]);
            return redirect('/category/'.$slugHolder);
        }
        // using for token search end point
        $url= config('constants.ORBWEAVER_API_URL').'part/lookup?lookup_value='.$partNumber.'';
        $response = Helpers::curlRequestOrbweaver($url);
        $finalProductData = [];
        $result = json_decode($response,true);
        if(isset($result) && !empty($result['offers'])){
            $resultListData =$result['offers'];
            // using for token search end point
            foreach ($resultListData as $partList){
                $productData=array();
                $newSourceArray = [];
                $productData['distributorName'] =  'Orbweaver';
                $productData['itemId'] = $partList['supplier_part_number'].'_'.$partList['manufacturer_id'];
                $productData['partNum'] = $partList['mpn'];
                $productData['requestedQty'] = '';
                $productData['manufacturer'] = $partList['manufacturer_name'];
                $productData['mfrCd'] = '';
                $productData['desc'] =  $partList['description'];
                if(isset($partList['compliance'])){
                    $compliance = [];
                    foreach($partList['compliance'] as $comArray){
                        foreach($comArray as $comKey){
                            if($comArray['name']){
                                $comArray['displayLabel'] = $comArray['name'];
                            }
                            if($comArray['value']){
                                $comArray['displayValue'] = $comArray['value'];
                            }
                        }
                        unset($comArray['name']);
                        unset($comArray['value']);
                        array_push($compliance, $comArray);
                    }
                    $productData['EnvData']['compliance']   =  $compliance;
                }else{
                    $productData['EnvData'] = "";
                }
                $newSourceCombineArray = array();
                $newIndex = 0;
                $productData['sources_count']   =  count($resultListData);
                $sourceArray = array();
                $newSourcePartArray = [];
                $sourceArray['displayName'] =$partList['datasource_name'];
                $sourceArray['sourceCd'] =  '';
                $sourceArray['currency'] =  'USD';

                $newSourceCombineArray['displayName'] =$partList['datasource_name'];
                $newSourceCombineArray['sourceCd'] =  '';
                $newSourceCombineArray['currency'] =  'USD';

                $i=0;
                foreach($resultListData as $sourcePart) {
                    $i++;
                    $sourcePartArray = array();
                    $sourcePartArray['packSize']         =  $sourcePart['order_mult_qty'];
                    $sourcePartArray['minimumOrderQuantity']         =  $sourcePart['min_order_qty'];
                    $sourcePartArray['sourcePartNumber']    =  '';
                    $sourcePartArray['sourcePartId']        =  $sourcePart['supplier_part_number'];

                    if(!empty($sourcePart['prices'])){
                        $priceArrays = [];
                        foreach($sourcePart['prices'] as $priceArray){
                            foreach($priceArray as $priceKey){
                                if($priceArray['unit_price']){
                                    $priceArray['displayPrice'] = $priceArray['unit_price'];
                                }
                                if($priceArray['unit_price']){
                                    $priceArray['price'] = $priceArray['unit_price'];
                                }
                                if($priceArray['from_qty']){
                                    $priceArray['minQty'] = $priceArray['from_qty'];
                                }
                                if($priceArray['to_qty']){
                                    $priceArray['maxQty'] = $priceArray['to_qty'];
                                }
                            }
                            unset($priceArray['from_qty']);
                            unset($priceArray['to_qty']);
                            unset($priceArray['unit_price']);
                            unset($priceArray['currency']);
                            $sourcePartArray['Prices']['resaleList'][] = $priceArray;
                        }
                    }else{
                        $sourcePartArray['Prices']['resaleList']  =  [];
                    }
                    $sourcePartArray['Availability'][0]['fohQty'] =  $sourcePart['available_qty'];
                    $sourcePartArray['Availability'][0]['availabilityCd'] =  '';
                    $sourcePartArray['Availability'][0]['availabilityMessage'] =  !empty($sourcePart['available_qty'])?'In Stock':'No Stock';
                    $sourcePartArray['Availability'][0]['pipeline'] =  array();

                    $sourcePartArray['customerSpecificPricing'] = array();
                    $sourcePartArray['customerSpecificInventory'] = array();
                    $sourcePartArray['dateCode'] = '';
                    $sourcePartArray['resources'] = array();

                    if($sourcePart['available_qty']>0){
                        $sourcePartArray['inStock'] =  1;
                    }else{
                        $sourcePartArray['inStock'] =  0;
                    }
                    $sourcePartArray['mfrLeadTime'] = $sourcePart['manufacturer_lead_time'];
                    $sourcePartArray['isNcnr'] = '';
                    $sourcePartArray['isNpi'] = '';
                    $sourcePartArray['isASA'] = '';
                    $sourcePartArray['requestQuantity'] = 0;
                    $sourcePartArray['productCode'] = '';
                    $sourcePartArray['iccLevels'] = array();

                    $sourcePartArray['cloudMfrCode'] = '';
                    $sourcePartArray['eccnCode'] = '';
                    $sourcePartArray['htsCode'] = '';
                    $sourcePartArray['countryOfOrigin'] = '';
                    $sourcePartArray['locationId'] = '';
                    $sourcePartArray['containerType'] = $sourcePart['pkg_type'];

                    $newSourcePartArray[] =  $sourcePartArray;
                    $newSourceCombineArray['sourcesPart'][] = $sourcePartArray;
                }
                $sourceArray['sourcesPart']   =   $newSourcePartArray ;
                $newSourceArray[] =  $sourceArray;
                $productData['sources'][]   =   $newSourceCombineArray ;
            }
            $finalProductData[]=$productData;
            return $finalProductData;
        }
    }

}//End Class
