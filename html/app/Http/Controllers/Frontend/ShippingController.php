<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests;
use Illuminate\Http\Request;
use Cart;
use Event;
use Auth;
use App\User;
use App\Country;
use App\State;
use App\City;
use App\ShippingAddress;
use App\ShippingMethod;
use App\Http\Controllers\Controller;
use App\PaymentMethod;
use Session;
use Helpers;
use phpDocumentor\Reflection\Types\Null_;
use App\NetAccountHolder;
use Illuminate\Auth\Events\Authenticated;
use Illuminate\Support\Facades\Redirect;


class ShippingController extends Controller
{
    protected $userSessionData;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    public function __construct()
    {
       Event::listen(Authenticated::class, function ($event) {
            $this->userSessionData = $event->user;
            if ($this->userSessionData['status'] == 0) {
                Helpers::sessionFlush();
            } 
        });
    }

    public function shipping(Request $request){
        $data['states'] = ['' => 'Select state*'];
        $data['cities'] = ['' => 'Select city*'];
        $data['countries']          = Country:: getAllCountry();

        if(Auth::user()){
            $itemCount = Cart::instance('shoppingcart')->content()->count();
            if($itemCount > 0){
                session(['previousroute' => '']);
                $userId                         = Auth::user()->id;
                $data['selectedAddress']        =  [];
                $data['selectedAddressValue']   =  '';
                $data['addresses']              = ShippingAddress::getAllAddressByUserId($userId);
                $data['shippingMethods']        = ShippingMethod::getAllShippingMethod();
                if(!empty($data['addresses'])){
                    return view('frontend.shipping.list-shipping-address',$data);
                }else{
                    return view('frontend.shipping.list-shipping-address',$data);
                }
            }else{
                return redirect()->to('/')->with('failure',config('constants.CHECKOUT_CART_EMPTY'));
            }
        }else{
            session(['previousroute' => 'cart']);
            return redirect()->to('login')->with('failure', config('constants.CHECKOUT_LOGIN_ERROR'));
        }

    }
    public function shippingAddress(Request $request){
        $data['countries'] =   Country:: getAllCountry();
        $data['address'] =  [];
        $data['validationMessage']      = ShippingAddress::$addShippingAddressValidationMessages;
        $user           = User::findOrFail(Auth::user()->id);
        $data['user']   = $user;
        $data['states'] = State::getSateByCountry(Auth::user()->country_id);
        $data['cities'] = City::getCityByState(Auth::user()->state_id);
        return view('frontend.shipping.add-shipping-address',$data);
    }

    public function editShippingAddress(Request $request,$id){
        $user           = User::findOrFail(Auth::user()->id);
        $data['user']   = $user; 
        $data['countries']  = Country:: getAllCountry();
        $data['address']    = ShippingAddress::findOrFail($id);
        $data['states']     = State::getSateByCountry($data['address']->country_id);
        $data['cities']     = City::getCityByState($data['address']->state_id);
        $data['validationMessage']  = ShippingAddress::$addShippingAddressValidationMessages; 
        return view('frontend.shipping.add-shipping-address',$data); 
    }
    public function reviewOrderDetails(Request $request){
        $itemCount = Cart::instance('shoppingcart')->content()->count();
        if($itemCount > 0){
            $shippingAddressId                  = $request->shipping_address;
            $shippingPurchaseOrderNumber        = $request->shipping_purchase_order_number;
            $selectedShippingMethodId           = $request->shipping_method;

            $shippingMethodTypeInput            = $request->shipping_method_type_input;
            $accountNumber                      = $request->shipping_account_number;
            $shippingNotes                      = $request->shipping_notes;
            $requestedDeliveryDate              = $request->requested_delivery_date;
            $consolidatedShippingCharge         = $request->consolidated_shipping_charge;

            session(['selectedAddressId'        => $shippingAddressId]);
            session(['selectedShippingMethodId' => $selectedShippingMethodId]);

            session(['shippingMethodTypeInput'          => $shippingMethodTypeInput]);
            session(['accountNumber'                    => $accountNumber]);
            session(['shippingNotes'                    => $shippingNotes]);
            session(['shippingPurchaseOrderNumber' => $shippingPurchaseOrderNumber]);
            session(['previousroute' => '']);
            session(['shipping_charges' => '']);
            $consolidatedShippingCharges = 0;
            if($consolidatedShippingCharge == "yes"){
                session(['consolidatedShippingCharge' => $consolidatedShippingCharge]);
                $consolidatedShippingCharges = config('constants.CONSOLIDATED_SHIPPING_CHARGE');
            }
            if(empty($shippingAddressId) || $shippingAddressId == NULL){
                return redirect('shipping')->with('failure', config('constants.CHECKOUT_EMPTY_SHIPPING_ADD'));
            }else{
                $data['isNetAccountHolder']     =   NetAccountHolder::getNetAccountRequestStatus(Auth::user()->id);
                $data['billingAddress']         =   ShippingAddress::getBillingAddressByUserId(Auth::user()->id);
                $data['addresses']              =   ShippingAddress::getShippingAddressById($shippingAddressId);
                $data['shippingMethod']         =   ShippingMethod::getShippingMethodById($selectedShippingMethodId);
                session(['shipping_charges'     => $data['shippingMethod'][0]->shipping_charges]);

                $data['shippingMethodType']         =   $shippingMethodTypeInput;
                $data['accountNumber']              =   $accountNumber;
                $data['shippingNotes']              =   $shippingNotes;
                $data['requestedDeliveryDate']      =   $requestedDeliveryDate;

                $data['paymentmethod']          =   PaymentMethod::getAllPaymentMethod();
                $user           = User::findOrFail(Auth::user()->id);
                $data['user']   = $user;
                if(empty($data['billingAddress'][0]->zip_code)){
                    // SET VARIABLE FOR FRONT END TO CHECK BILLING ADDRESS IS EMPTY
                    $data['userId'] = Auth::user()->id;
                    $data['isEmptybillingAddress'] =  0;
                    $data['consolidatedShippingCharges'] = $consolidatedShippingCharges;
                    return view('frontend.shipping.review',$data);
                }else{
                    $data['userId'] = Auth::user()->id;
                    $data['isEmptybillingAddress'] =  1;
                    $data['consolidatedShippingCharges'] = $consolidatedShippingCharges;
                    return view('frontend.shipping.review',$data);
                }
            }
        }else{
            return redirect()->to('/')->with('failure', config('constants.CHECKOUT_CART_EMPTY'));
        }

    }
    public function stateListByCountryName(Request $request){
        $data         = $request->all();
        $countryId    = $request->country_id;
        $state       =  State::getStateByCountryId($countryId);
        return response(['success' => true, 'data' => $state, 'message' => 'State list successfully' ]);
    }
    public function cityListByStateName(Request $request){
        try{
            $data           = $request->all();
            $cityId         = $request->cityId;
            $cities         = City::getCityByStateId($cityId);
            return response(['success' => true, 'data' => $cities, 'message' => 'City list successfully' ]);
        }catch (\Exception $ex) {
            return response(['status' => false, 'data' => config('constants.COMMON_ERROR')]);
        }
    }
    public function addShippingAddress(Request $request){
        try {
            $data = $request->all();
            //Server side validations
            $validator = \Validator::make($data, ShippingAddress::addShippingAddressValidationRules(),ShippingAddress::$addShippingAddressValidationMessages);
            if ($validator->fails()) {
                //Redirect user back with input if server side validation fails
                return redirect('add-shipping-address')->withErrors($validator)->withInput();
            }else{
                $shippingAddress                            = new ShippingAddress();
                $userId                                     = Auth::user()->id;
                $shippingAddress->user_id                   = $userId;
                $shippingAddress->first_name                = preg_replace('!\s+!', ' ', $request->input('first_name'));
                $shippingAddress->last_name                 = preg_replace('!\s+!', ' ', $request->input('last_name'));
                $shippingAddress->company_name              = $request->input('company_name');
                $shippingAddress->contact_number            = $request->input('contact_number');
                $shippingAddress->country_id                = $request->input('country_id');
                $shippingAddress->address_line1             = $request->input('address_line1');
                $shippingAddress->address_line2             = $request->input('address_line2');
                $shippingAddress->state_id                  = $request->input('state_id');
                $shippingAddress->city                      = $request->input('city');
                $shippingAddress->zip_code                  = $request->input('zip_code');
                if ($shippingAddress->save()) {
                    return redirect('/shipping')->with('success', config('constants.SHIPPING_ADD_SAVE_SUCCESS'));
                } else {
                    return redirect('add-shipping-address')->with('failure',config('constants.SHIPPING_ADD_SAVE_ERROR'));
                }
            }
        }catch (\Exception $ex) {
            return redirect('add-shipping-address')->with('failure', config('constants.COMMON_ERROR'));
        }
    }
    public function updateShippingAddress(Request $request ,$id ){
        try {
            $data = $request->all();
            //Server side validations
            $validator = \Validator::make($data, ShippingAddress::addShippingAddressValidationRules(),ShippingAddress::$addShippingAddressValidationMessages);
            if ($validator->fails()) {
                //Redirect user back with input if server side validation fails
                return redirect('update-shipping-address/'.$id)->withErrors($validator)->withInput();
            }else{
                $shippingAddress                            = ShippingAddress::findOrFail($id);
                $userId                                     = Auth::user()->id;
                $shippingAddress->user_id                   = $userId;
                $shippingAddress->first_name                = preg_replace('!\s+!', ' ', $request->input('first_name'));
                $shippingAddress->last_name                 = preg_replace('!\s+!', ' ', $request->input('last_name'));
                $shippingAddress->company_name              = $request->input('company_name');
                $shippingAddress->contact_number            = $request->input('contact_number');
                $shippingAddress->country_id                = $request->input('country_id');
                $shippingAddress->address_line1             = $request->input('address_line1');
                $shippingAddress->address_line2             = $request->input('address_line2');
                $shippingAddress->state_id                  = $request->input('state_id');
                $shippingAddress->city                      = $request->input('city');
                $shippingAddress->zip_code                  = $request->input('zip_code');
                if ($shippingAddress->save()) {
                    return redirect('/shipping')->with('success', config('constants.SHIPPING_UPDATE_SAVE_SUCCESS'));
                } else {
                    return redirect('update-shipping-address/'.$id)->with('failure', config('constants.SHIPPING_UPDATE_SAVE_ERROR'));
                }
            }
        }catch (\Exception $ex) {
            return redirect('update-shipping-address/'.$id)->with('failure', config('constants.COMMON_ERROR'));
        }
    }
    public function removeShippingAddress(Request $request ){
        try {
            //Find details by id
            $shippingAddress = ShippingAddress::findOrFail($request->input('shippingAddressId'));
            $shippingAddress->deleted_at  =  date('Y-m-d h:i:s');
            if($shippingAddress->save()) {
                if(Session::get('selectedAddressId') ==$request->input('shippingAddressId') ){
                    session(['selectedAddressId' => '']);
                }
                return response(['success' => true, 'data' => '']);
            } else {
                return response(['success' => false, 'data' => '']);
            }
        } catch (\Exception $ex) {
            return response(['success' => false, 'data' =>config('constants.COMMON_ERROR')]);
        }
    }

}
