<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Country extends Model
{
    protected $table = 'countries';

    /**
     * Get all country
     * @return array
     */
    public static function getAllCountry()
    {
        //Get all country
        $countries = self::orderBy('name')
            ->pluck('name', 'id')->all();
        $default = ['' => 'Select Country*'];
        $countries = $default + $countries;
        return $countries;
    }
}