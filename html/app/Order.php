<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 09/03/18
 * Time: 6:56 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Order extends Model
{
    protected $table = 'orders';

    public static $shippingStatus = [
        ''=>'Select Status',
        1=> 'Shipped',
        2=> 'Pending',
        4=> 'Delivered'
    ];

    public static $paymentStatus = [
        ''=>'Select Status',
            0=>     'Pending',
            1=>     'Done',
            2=>     'Failed',
            3=>     'Refunded',
    ];

    public static function getOrderInfoById($id){

        $orderInfo = DB::table('orders')
        ->where('orders.id', '=', $id)
        ->select('orders.id','orders.order_date','orders.order_sub_total',
        'orders.order_tax_amount','orders.order_discount_amount','orders.payment_status',
        'orders.order_shipping_charges_amount','orders.order_status','orders.order_total_amount','orders.order_shipping_status', 'orders.order_tracking_info','orders.address_billing','orders.address_shipping',
        'orders.shipping_address_id','users.name AS customer_name', 'users.last_name','users.email AS email','payment_methods.payment_method_name', 'orders.shipping_method_type','orders.shipping_account_number','orders.shipping_notes', 'orders.requested_delivery_date'
           ,'orders.shipping_method_id','orders.partially_shipped','orders.consolidated_shipping_charge','orders.payment_method_id','orders.shipping_purchase_order_number')
        ->join('users','orders.users_id','=','users.id')
        ->join('shipping_addresses','orders.shipping_address_id','=','shipping_addresses.id')
        ->join('payment_methods','orders.payment_method_id','=','payment_methods.id')
        ->whereNull('orders.deleted_at')
        ->get()->toArray();
        return $orderInfo;
    }

    public static function  getOrderDetailsById($id){
        $orderDetails= DB::table('order_details')
                    ->where('order_details.order_id', '=', $id)
                    ->select('order_details.id','order_details.item_name','order_details.price','order_details.manufacturer_name',  'order_details.manufacturer_code','order_details.source_part_id','order_details.customer_part_id','order_details.is_quote_item','order_details.quote_id','quotes.quote_name', 'shipping_detail.tracking_number', 'shipping_detail.tracking_website', 'shipping_detail.shipping_status', 'shipping_detail.note', 'shipping_detail.shipped_qty', 'shipping_detail.date_of_shipping', 'order_details.distributor_name', DB::raw('SUM(order_details.quantity) as quantity'))
                    ->leftjoin('quotes','order_details.quote_id','=','quotes.id')
                    ->leftjoin('shipping_detail','order_details.id','=','shipping_detail.order_id')
                    ->groupBy('order_details.distributor_name', 'order_details.item_name')
                    ->get()->toArray();
        return $orderDetails;
    }

}