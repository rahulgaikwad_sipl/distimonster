<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class ShippingAddress extends Model
{
    protected $table = 'shipping_addresses';
    public static  $addShippingAddressValidationMessages = [
        'first_name.required'               => 'Please enter first name',
        'last_name.required'                => 'Please enter last name',
        'company_name.required'             => 'Please enter last name',
        'contact_number.required'           => 'Please enter last name',
        'country_id.required'               => 'Please select a country.',
        'address_line1.required'            => 'Address line 1 can not be empty.',
        'state_id.required'                 => 'Please select a state.',
        'city.required'                     => 'Please select a city.',
        'zip_code.required'                 => 'Please enter zip code.',
        'name.min'                          =>'Please enter at least 2 character for first name',
        'name.max'                          =>'The maximum length for first name is 30 characters',
        'last_name.min'                     =>'Please enter at least 2 character for last name',
        'last_name.max'                     =>'The maximum length for last name is 30 characters',
        'company_name.min'                  =>'Please enter at least 2 character for company name',
        'company_name.max'                  =>'The maximum length for company name is 50 characters',
        'contact_number.min'                =>'Please enter at least 9 character for contact number',
        'contact_number.max'                =>'The maximum length for contact number is 15 characters',
        'address.min'                       =>'Please enter at least 2 character for address',
        'address.max'                       =>'The maximum length for address is 100 characters',
        'zip_code.min'                      =>'Please enter at least 5 character for zip code',
        'zip_code.max'                      =>'The maximum length for zip code is 8 characters'

    ];
    public static function addShippingAddressValidationRules()
    {
        return [
        'first_name' => 'required',
        'last_name' => 'required',
        'company_name' => 'required',
        'contact_number' => 'required',
        'country_id' => 'required',
        'address_line1' => 'required',
        'state_id' => 'required',
        'city' => 'required',
        'zip_code' => 'required',
        ];
    }

    public static function getAllAddressByUserId($userId){
        $states = DB::table('shipping_addresses')
            ->where('shipping_addresses.user_id', '=', $userId)
            ->select('shipping_addresses.id', 'shipping_addresses.first_name','shipping_addresses.last_name',
                'shipping_addresses.company_name','shipping_addresses.contact_number',
                'shipping_addresses.country_id','shipping_addresses.address_line1',
                'shipping_addresses.address_line2','shipping_addresses.zip_code',
                'countries.name AS country_name','states.name AS state_name','cities.name AS city_name')
            ->join('countries','shipping_addresses.country_id','=','countries.id')
            ->join('cities','shipping_addresses.city','=','cities.id')
            ->join('states','shipping_addresses.state_id','=','states.id')
            ->whereNull('deleted_at')
            ->get()->toArray();
        return $states;
    }

    public static function getBillingAddressByUserId($userId){
        $billingAddress = DB::table('users')
            ->where('users.id', '=', $userId)
            ->select('users.name','users.last_name','users.billing_address_line1','users.billing_address_line2','users.zip_code', 'countries.name AS country_name','states.name AS state_name','cities.name AS city_name')
            ->leftJoin('countries','users.country_id','=','countries.id')
            ->leftJoin('cities','users.city','=','cities.id')
            ->leftJoin('states','users.state_id','=','states.id')
            ->get()->toArray();
        return $billingAddress;
    }

    public static function getShippingAddressById($addressId){
        $shippingAddress = DB::table('shipping_addresses')
            ->where('shipping_addresses.id', '=', $addressId)
            ->select('shipping_addresses.id', 'shipping_addresses.first_name','shipping_addresses.last_name',
                'shipping_addresses.company_name','shipping_addresses.contact_number',
                'shipping_addresses.country_id','shipping_addresses.address_line1',
                'shipping_addresses.address_line2','shipping_addresses.zip_code',
                'countries.name AS country_name','states.name AS state_name','cities.name AS city_name')
            ->leftJoin('countries','shipping_addresses.country_id','=','countries.id')
            ->leftJoin('cities','shipping_addresses.city','=','cities.id')
            ->leftJoin('states','shipping_addresses.state_id','=','states.id')
            ->get()->toArray();
        return $shippingAddress;
    }

    public static function getCountryCodeByName($name){
        $countryCode = DB::table('countries')->where('name', '=', $name)->select('name', 'phonecode')->get()->toArray();
        return $countryCode;
    }
}