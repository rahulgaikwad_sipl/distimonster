<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 09/03/18
 * Time: 6:56 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $table = 'order_details';
}