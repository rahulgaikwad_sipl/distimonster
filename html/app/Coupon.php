<?php
namespace App;
use GuzzleHttp\Psr7\Request;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Notifications\ResetPassword;
use Hash;
use DB;

//use Webpatser\Uuid\Uuid;
/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $role
 * @property string $remember_token
*/
class Coupon extends Authenticatable
{
    use Notifiable;
    protected $fillable = ['code', 'discount_percentage', 'order_limit', 'minimum_cart_amount', 'start_date', 'end_date'];
    
    public static function boot()
	{
		parent::boot();
	}

    /**
     * Common validation rules
     * @return array
     */
    public static function addCouponValidationRules()
    {
        return [
            'code'                   => 'required|unique:coupons',
            'order_limit'            => 'required',
            'minimum_cart_amount'    => 'required',
            'start_date'             => 'required',
            'end_date'               => 'required',
            'status'                 => 'required'

        ];
    }
    public static function updateCouponValidationRules()
    {
        return [
            'code'                   => 'required',
            'order_limit'            => 'required',
            'minimum_cart_amount'    => 'required',
            'start_date'             => 'required',
            'end_date'               => 'required',
            'status'                 => 'required'

        ];
    }
    /**
     * Common validation messages
     * @return array
     */
    public static  $registerCouponValidationMessages = [
            'code.required'                     => 'Please enter coupon code',
            'order_limit.required'              => 'Please enter Order Limit',
            'minimum_cart_amount.required'      => 'Please enter Minimum Cart Amount.',
            'start_date.required'               => 'Please enter Start Date.',
            'end_date.required'                 => 'Please enter End Date.'
        ];
    /*
    Admin Function to add and update coupon
    * */
    public static function addCoupon($request){
        $data = $request->all();
        //Server side validations
        $validator = \Validator::make($data, Coupon::addCouponValidationRules(), Coupon::$registerCouponValidationMessages);
        
        if ($validator->fails()) {
            //Redirect user back with input if server side validation fails
            return ['validation_fails' => true, 'validator' => $validator];
        }

        $startDate  =  $request->input('start_date');
        $endDate  =   $request->input('end_date');
        $coupon = new Coupon();
        $coupon->code                 = $request->input('code');
        $coupon->discount_percentage  = $request->input('discount_percentage')?$request->input('discount_percentage'):100;
        $coupon->order_limit          = $request->input('order_limit');
        $coupon->minimum_cart_amount  = $request->input('minimum_cart_amount');
        $coupon->start_date           = date("Y-m-d", strtotime($startDate));
        $coupon->end_date             = date("Y-m-d", strtotime($endDate));
        $coupon->is_free              = $request->input('is_free')?$request->input('is_free'):0;
        $coupon->status               = $request->input('status');
        
        //Save user details
        if ($coupon->save()) {
            return ['coupon_saved' => true, 'coupon' => $coupon, 'validation_fails' => false];
        } else {
            return ['coupon_saved' => false, 'coupon' => [], 'validation_fails' => false];
        }
    }
    public static function updateCoupon($request,$id){
        $data = $request->all();
        //Server side validations
        $validator = \Validator::make($data, Coupon::updateCouponValidationRules(), Coupon::$registerCouponValidationMessages);

        $validator->after(function ($validator) use ($id , $data) {
            $checkCoupon = Coupon::where('code', $data['code'])->where('id','!=',$id)->get();
            if (count($checkCoupon->toArray()) > 0) {
                $validator->errors()->add('name', 'Coupon Code Already Exists.');
            }
        });


        if ($validator->fails()) {
            //Redirect user back with input if server side validation fails
            return ['validation_fails' => true, 'validator' => $validator];
        }
        
        $coupon                       = Coupon::findOrFail($id);
        $coupon->code                 = $request->input('code');
        $coupon->discount_percentage  = $request->input('discount_percentage')?$request->input('discount_percentage'):100;
        $coupon->order_limit          = $request->input('order_limit');
        $coupon->minimum_cart_amount  = $request->input('minimum_cart_amount');
        $coupon->start_date           = date("Y-m-d", strtotime($request->input('start_date')));
        $coupon->end_date             = date("Y-m-d", strtotime($request->input('end_date')));
        $coupon->is_free              = $request->input('is_free')?$request->input('is_free'):0;
        $coupon->status               = $request->input('status');
        //Save user details
        if ($coupon->save()) {
            return ['coupon_saved' => true, 'coupon' => $coupon, 'validation_fails' => false];
        } else {
            return ['coupon_saved' => false, 'coupon' => [], 'validation_fails' => false];
        }
    }
    public static function checkCouponCode($code){
        $coupons = DB::table('coupons')
            ->where('coupons.code', '=', trim($code))
            ->where('coupons.status', '=', 1)
            ->whereNull('deleted_at')
            ->select('coupons.id', 'coupons.is_free', 'coupons.code','coupons.discount_percentage','coupons.order_limit','coupons.minimum_cart_amount','coupons.start_date','coupons.end_date','coupons.status')
            ->get();
        return $coupons;
    }

    public static function getCouponUsageQuantity($couponCodeId){
        $couponsUsageCount = DB::table('orders')
            ->where('orders.coupon_code_id', $couponCodeId)
            ->select('orders.id')
            ->count();
        return $couponsUsageCount;

    }

}
