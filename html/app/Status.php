<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
/**
 * Class Role
 *
 * @package App
 * @property string $title
 */
class Status extends Model
{
    protected $table = 'status';
    protected $fillable = ['id', 'name'];
}
