<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class ShippingMethod extends Model
{
    protected $table = 'shipping_methods';

    public static function getAllShippingMethod(){
        $states = DB::table('shipping_methods')
            ->where('shipping_methods.id',1)
            ->select('shipping_methods.id', 'shipping_methods.shipping_method_name','shipping_methods.shipping_charges')
            ->get()->toArray();
        return $states;
    }

    public static  function getShippingMethodById($selectedShippingMethodId){
        $states = DB::table('shipping_methods')
            ->where('id',$selectedShippingMethodId)
            ->select('shipping_methods.id', 'shipping_methods.shipping_method_name','shipping_methods.shipping_charges')
            ->get()->toArray();
        return $states;
    }
}