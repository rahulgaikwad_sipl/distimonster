<?php
namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PasswordReset extends Notification
{
    use Queueable;

    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $appName = config('app.name');
        return (new MailMessage)
            ->line(new \Illuminate\Support\HtmlString('<tr><td style="font-size:15px;color:#666;font-weight:normal;line-height:25px;padding:0 12px 5px 0" align="left">Thank you for visiting '.$appName.'.</td></tr>'))
            ->line(new \Illuminate\Support\HtmlString('<tr><td style="font-size:15px;color:#666;font-weight:normal;line-height:25px;padding:0 12px 5px 0" align="left">Please <a style="text-decoration:none;color:#D258A1;font-weight:bold;" target="_blank" title="Click Here" href="'.url('admin/password/reset', $this->token).'"> <strong style="color:#D258A1;">Click Here</strong> </a> to set new password.</td></tr>'))
            ->line("If you did not request a new password, just ignore this email. We'll keep your account safe.");
    }
}
