<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 09/03/18
 * Time: 6:56 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Quote extends Model
{
    protected $table = 'quotes';
    public static function getQuoteListByUserId($userId, $quoteType = ''){

        $query = DB::table('quotes')
            ->where('quotes.user_id', '=', $userId)
            ->select('quotes.id','quotes.is_unnamed','quotes.is_approved','quotes.quote_name',\DB::raw('count(quote_details.id) as part_counts'),\DB::raw('SUM(quote_details.quantity*quote_details.price) as part_total'),'quotes.created_at')
            ->join('quote_details','quotes.id','=','quote_details.quote_id')
            ->groupBy('quotes.id')
            ->orderBy('quotes.id','DESC')
            ->whereNull('quotes.deleted_at');
            
            if($quoteType==0){
               $query = $query->where('quotes.is_approved', 0);
           }elseif($quoteType==1){
               $query = $query->where('quotes.is_approved', 1);
           }elseif($quoteType==2){
               $query = $query->where('quotes.is_approved', 2);
           }elseif($quoteType==3){
               $query = $query->where('quotes.is_approved', 3);
           }elseif($quoteType==4){
               $query = $query->where('quotes.is_approved', 4);
           }else{
               $query = $query->whereIn('quotes.is_approved', [0, 1, 2, 3, 4]);
           }
            $quotes = $query->get();
            
        return $quotes;
    }

    public static function getQuoteInfoById($quoteId){
        $quote = DB::table('quotes')->where('quotes.id', '=', $quoteId)->select('quotes.id','quotes.is_unnamed','quotes.quote_name','quotes.created_at','quotes.is_approved','quotes.note')
            ->whereNull('quotes.deleted_at')
            ->get();
        return $quote;
    }


    public static function getQuoteInfoWithUserById($quoteId){
        $quote = Quote::where('quotes.id', '=', $quoteId)
            ->select('quotes.id','quotes.is_unnamed','quotes.quote_name','quotes.created_at','quotes.is_approved','quotes.note','users.name','users.last_name')
            ->join('users','users.id','=','quotes.user_id')
            ->whereNull('quotes.deleted_at')
            ->get();
        return $quote;
    }


    public static function getQuoteDetailById($quoteId){
        $quotesItems = DB::table('quote_details')
            ->where('quote_details.quote_id', '=', $quoteId)
            ->select('quote_details.*')
            ->whereNull('quote_details.deleted_at')
            ->get();
        return $quotesItems;
    }



    /**
     * function for get status name
     * @param $status
     * @return string
     */
    public static function getStatusName($status){
        $statusName = 'Pending';
        if($status == 1){
            $statusName = 'Approved';
        } elseif ($status == 2){
            $statusName = 'Rejected';
        } elseif ($status == 3){
            $statusName = 'Expired';
        } elseif ($status == 4) {
            $statusName = 'Ordered';
        }else{
            $statusName = 'Pending';
        }

        return $statusName;
    }

    /**
     * function for get requested lead time 
     * @param $status
     * @return string
     */
    public static function getRLTQuoteListByUserId($userId){
         $query = DB::table('quotes')
            ->where('quotes.user_id', '=', $userId)
            ->select('quotes.id','quotes.is_unnamed','quotes.is_approved','quotes.quote_name',\DB::raw('count(quote_details.id) as part_counts'),\DB::raw('SUM(quote_details.quantity*quote_details.price) as part_total'),'quotes.created_at','quotes.is_requested_lead_time','quotes.is_responded_lead_time')
            ->join('quote_details','quotes.id','=','quote_details.quote_id')
            ->groupBy('quotes.id')
            ->orderBy('quotes.id','DESC')
            ->whereNull('quotes.deleted_at');
            
            $query = $query->where('quotes.is_approved', 0);
            $query = $query->where('quotes.is_requested_lead_time', 1);
            $query = $query->where('quotes.is_responded_lead_time', 0);
            $getRLTQuoteList = $query->get();
            
        return $getRLTQuoteList;
    }

    /**
     * function for get responded lead time 
     * @param $status
     * @return string
     */
    public static function getRespondedLTQuoteListByUserId($userId){

        $query = DB::table('quotes')
            ->where('quotes.user_id', '=', $userId)
            ->select('quotes.id','quotes.is_unnamed','quotes.is_approved','quotes.quote_name',\DB::raw('count(quote_details.id) as part_counts'),\DB::raw('SUM(quote_details.quantity*quote_details.price) as part_total'),'quotes.created_at','quotes.is_requested_lead_time','quotes.is_responded_lead_time')
            ->join('quote_details','quotes.id','=','quote_details.quote_id')
            ->groupBy('quotes.id')
            ->orderBy('quotes.id','DESC')
            ->whereNull('quotes.deleted_at');
            
            $query = $query->where('quotes.is_approved', 1);
            $query = $query->where('quotes.is_responded_lead_time', 1);
            $getRLTQuoteList = $query->get();
            
        return $getRLTQuoteList;
    }
}