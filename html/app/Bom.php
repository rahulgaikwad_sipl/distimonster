<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 26/03/18
 * Time: 11:09 AM
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
class Bom  extends Model
{
    protected $table = 'boms';

    /**
     * Common validation messages
     * @return array
     */
    public static  $addBomValidationMessages = [
        'full_name.required'                => 'Please enter full name',
        'full_name.min'                     => 'Please enter at least 2 character for full name.',
        'full_name.max'                     => 'The maximum length for full name is 30 characters',
        'email.required'                    => 'Please enter email',
        'email.email'                       => 'Email address is invalid',
        'contact_number.min'                => 'Please enter at least 9 character in phone number',
        'contact_number.max'                => 'The Maximum length for contact number in 15.',
        'bom_file.max'                      =>'Bom file not to exceed 10MB',
        'bom_file.mimes'                    =>'Please upload only CSV file'
    ];

    public static function sendBomValidationRules()
    {
        return [
            'full_name'                => 'required',
            'email'                     => 'required|email',
            'bom_file'                  => 'max:10240'
        ];
    }

    public static function getBomListByUserId($userId){
        $columns = [
            'boms.id',
            'boms.bom_name',
            'boms.full_name',
            'boms.company_name',
            'boms.email',
            'boms.contact_no',
            'boms.address',
            'boms.updated_at',
            'boms.file_url',
            'boms.bom_parts_count',
            'boms.shared_user_id'
        ];
        $bomList = DB::table('boms')
            ->select($columns)
            ->whereRaw('FIND_IN_SET('.$userId.', shared_user_id)')
            ->orWhere('user_id',$userId)
            ->where('bom_status','published')
            ->whereNull('deleted_at')
            ->orderBy('id','DESC')
            ->get();
        return $bomList;
    }

    public static function getBomListByUserIdAndBomType($userId, $bomType){
        $columns = [
            'boms.id',
            'boms.bom_name',
            'boms.full_name',
            'boms.company_name',
            'boms.email',
            'boms.contact_no',
            'boms.address',
            'boms.updated_at',
            'boms.file_url',
            'boms.bom_parts_count',
            'boms.shared_user_id',
			 DB::raw("(SELECT  SUM(is_no_read) as unread FROM `shared_notes_history` WHERE shared_with = $userId and bom_id = boms.id GROUP BY bom_id) as unread")
   
        ];
        if($bomType == 'saved'){
            $bomList = DB::table('boms')
                ->select($columns)
                ->where('user_id',$userId)
                ->where('bom_status','published')
                ->whereNull('deleted_at')
                ->orderBy('id','DESC')
                ->get();
        }else if($bomType == 'shared'){
            $getUser = DB::table('users')->select('is_sub_user')->where('id', $userId)->get();
            if($getUser[0]->is_sub_user){
                $bomList = DB::table('boms')
                ->select($columns)
                ->whereRaw('(FIND_IN_SET('.$userId.', shared_user_id) OR user_id='.$userId.')')
                ->where('bom_status','published')
                ->whereNull('deleted_at')
                ->orderBy('id','DESC')
                ->get();
            }else{
                $bomList = DB::table('boms')
                ->select($columns)
                ->whereRaw('(user_id ='.$userId.' OR shared_user_id='.$userId.')')
                ->whereNotNull('shared_user_id')
                ->where('bom_status','published')
                ->whereNull('deleted_at')
                ->orderBy('id','DESC')
                ->get();
            }
        }else if($bomType == 'ordered'){
            $bomList = DB::table('boms')
                ->join('order_details', 'boms.id', '=', 'order_details.ordered_bom_id')
                ->select($columns)
                ->where('user_id',$userId)
                ->where('bom_status','published')
                ->whereNull('boms.deleted_at')
                ->orderBy('boms.id','DESC')
                ->get();
        }
        return $bomList;
    }

    public static function getSubUserId($userId){
        $columns = [
            'id',
            'name',
            'last_name',
            'email',
            'parent_user_id'
        ];
        if(Auth::user()->is_sub_user){
            $parentUserId = DB::table('users')
            ->select($columns)
            ->where('id', $userId)
            ->whereNull('deleted_at')
            ->first();
            $subUserList = DB::table('users')
            ->select($columns)
            ->where('parent_user_id', $parentUserId->parent_user_id)
            ->where('id', '!=', $userId)
            ->orwhere('id', $parentUserId->parent_user_id)
            ->whereNull('deleted_at')
            ->orderBy('id','DESC')
            ->get();
        }else{
            $subUserList = DB::table('users')
            ->select($columns)
            ->where('parent_user_id', $userId)
            ->where('is_sub_user', 1)
            ->whereNull('deleted_at')
            ->orderBy('id','DESC')
            ->get();
        }
        return $subUserList;
    }

    
    public static function getBomById($bomId){
        $columns = [
            'boms.id',
            'boms.full_name',
            'boms.company_name',
            'boms.email',
            'boms.contact_no',
            'boms.address',
            'boms.updated_at',
            'boms.file_url',
            'boms.bom_details'
        ];
        $bomList = DB::table('boms')
            ->select($columns)
            ->where('id',$bomId)
            ->whereNull('deleted_at')
            ->get();
        return $bomList;
    }
    public static function getBomInfoById($bomId,$userId){
        $where = 'boms.id = '.$bomId.' and (boms.user_id = '.$userId.' or shared_user_id like "%'.$userId.'%")';
        $bom = DB::table('boms')->whereRaw($where)->select('boms.id','boms.user_id','boms.bom_found_parts','boms.bom_name')
            ->whereNull('boms.deleted_at')
            ->get();
		return $bom;
    }

    public static function getShareNotesBomId($bomId){
		/*Mark Read notes*/
		DB::table('shared_notes_history')
            ->where('bom_id', $bomId)
			->where('shared_with', Auth::user()->id)
            ->update(['is_no_read' =>0]);
        $userType = Auth::user()->is_sub_user;
        if($userType == '0') {
            $notes = DB::table('shared_notes_history')
                ->join('users', 'users.id', '=', 'shared_notes_history.shared_with')
                ->where('shared_notes_history.bom_id', $bomId)
                ->select('shared_notes_history.note', 'users.name', 'users.last_name', 'shared_by','shared_with','shared_notes_history.created_at')
                ->whereNull('shared_notes_history.deleted_at')
                ->get();        
        } else {
            $notes = DB::table('shared_notes_history')
			   ->join('users', 'users.id', '=', 'shared_notes_history.shared_by')
                ->where('shared_notes_history.bom_id', $bomId)
                ->whereRaw('(shared_notes_history.shared_with ='.Auth::user()->id.' OR shared_notes_history.shared_by='.Auth::user()->id.')')
                ->select('users.name', 'users.last_name','shared_notes_history.shared_with','shared_notes_history.shared_by','shared_notes_history.note', 'shared_notes_history.created_at')
                ->whereNull('shared_notes_history.deleted_at')
                ->get();
        }        
        return $notes;
    }
}