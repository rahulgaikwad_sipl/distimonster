<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 26/03/18
 * Time: 11:09 AM
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Bio  extends Model
{
    protected $table = 'users';

    /**
     * Common validation messages
     * @return array
     */
    public static  $addBioValidationRules = [
        'are_you_a' => 'required',
        'your_company_a' => 'required',
        'your_main_industry' => 'required'
    ];
    
    /**
     * Common validation messages
     * @return array
     */
    public static  $addBioValidationMessages = [
        'are_you_a.required' => 'Please select are you a.',
        'your_company_a.required' => 'Please select your company a.',
        'your_main_industry.required' => 'Please select your main industry'
    ];
}