<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubscriptionPlans extends Model{
    
    protected $table = 'subscription_plans';
    use Notifiable;
    use SoftDeletes;
}