<?php
namespace App;
use GuzzleHttp\Psr7\Request;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Notifications\ResetPassword;
use Hash;
use DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Notifications\PasswordReset; // Or the location that you store your notifications (this is default).

//use Webpatser\Uuid\Uuid;
/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $role
 * @property string $remember_token
*/
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    protected $fillable = ['name', 'email', 'remember_token', 'role_id'];
    
    public static function boot(){
        parent::boot();
    }

    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input)
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setRoleIdAttribute($input)
    {
        $this->attributes['role_id'] = $input ? $input : null;
    }
    
    public function role()
    {
        return $this->belongsTo(Role::class, 'role_id');
    }
    
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PasswordReset($token));
    }

    /**
     * Common validation rules
     * @return array
     */
    public static function registerUserValidationRules()
    {
        return [
            'first_name'                => 'required',
            'last_name'                 => 'required',
            'email'                     => 'required|email|unique:users',
            'password'                  => 'required',

        ];
    }

    public static function addUserValidationRules()
    {
        return [
            'name'                => 'required',
            'last_name'           => 'required',
            'email'               => 'required|email|unique:users',
            'password'            => 'required',
            'status'              => 'required',
            'plan_type'           => 'required'
        ];
    }

    public static function updateUserValidationRules()
    {
        return [
            'name'                => 'required',
            'email'               => 'required|email',
            'status'              => 'required',
            'plan_type'           => 'required'
        ];
    }

    public static function updateUserProfileValidationRules()
    {
        Validator::extend('checkEmailUnique', function($attribute, $value, $parameters)
        {
            $loginUserEmail = Auth::user()->email;
            if($value != $loginUserEmail){
                $userEmail = User::where('email', $value)->where('role_id', 2)->count();
                if($userEmail > 0){
                    return false;
                }else{
                    return true;
                }
            }else{
                return true;
            }
        },'Email address is already in use.');

        return [
            'first_name'                => 'required',
            'last_name'                 =>'required',
            'email'                     =>'email|checkEmailUnique'
        ];
    }

    public static function updateAdminProfileValidationRules()
    {
        return [
            'name'                       => 'required',
            $rules['profile_pic']        = 'max:5120',
            $rules['profile_pic_mime']   = 'in:image/jpeg,image/jpg,image/png,image/bmp',
        ];
    }
    /**
     * Common validation messages
     * @return array
     */
    public static  $registerUserValidationMessages = [
            'first_name.required'               => 'Please enter first name.',
            'first_name.min'                    => 'First name consists of at least 2 characters.',
            'first_name.max'                    => 'First name should not be exceed 30 characters.',
            'name.required'                     => 'Please enter full name.',
            'last_name.required'                => 'Please enter last name.',
            'last_name.min'                     => 'Last name consists of at least 2 characters.',
            'last_name.max'                     => 'Last name should not be exceed 30 characters.',
            'email.required'                    => 'Please enter email.',
            'email.email'                       => 'Invalid email address.',
            'email.unique'                      => 'Email address is already in use.',
            'password.required'                 => 'Please enter password',
            'password.min'                      => 'Password consists of at least 6 character.',
            'password.max'                      => 'Password should not be exceed 20 characters.',
            'status.required'                   => 'Please select user status.',
            'plan_type.required'                => 'Please select subscription plan.',
            'profile_pic_mime.in'               => 'Only jpeg, jpg, png and bmp files are allowed',
            'profile_pic.max'                   => 'Uploaded image size can not be more than 5MB',
            'country_id.required'               => 'Please select a country.',
            'address_line1.required'            => 'Address line 1 can not be empty.',
            'state_id.required'                 => 'Please select a state.',
            'city.required'                     => 'Please select a city.',
            'zip_code.required'                 => 'Please enter zip code.',
            'zip_code.min'                      =>'Please enter at least 5 character for zip code.',
            'zip_code.max'                      =>'Zip code should not be exceed 8 characters.',
            'address.min'                       =>'Please enter at least 2 character for address.',
            'address.max'                       =>'Address should not be exceed 100 characters.'
        ];


    public static function passwordValidationRules()
    {
        return [
            'password'              => 'required|min:6|max:15',
            'cpassword' => 'required|min:6|max:15'
        ];
    }


    public static function checkUserCurrentPassword($userId,$currentPassword){
        $user = DB::table('users')
            ->where('id','=', $userId)
            ->where('password','=',$currentPassword)
            ->select('id','email')
            ->get()->toArray();
        return $user;
    }
    public static function registerNewUser($request){
        $data = $request->all();
        $user = new User();
        $user->name         = preg_replace('!\s+!', ' ', $request->input('first_name'));
        $user->last_name    = preg_replace('!\s+!', ' ', $request->input('last_name'));
        $user->contact_number = trim($request->input('contact_number'));


        $user->email        = $request->input('email');

        if($request->has('password')){
            $user->password        =  Hash::make($request->input('password'));
        }
        $user->role_id             = 2;
        //Save user details
        if ($user->save()) {
            //Assign role to registered user
            return ['status' => true, 'user' => $user, 'validation_fails' => false];
        } else {
            return ['status' => false, 'user' => [], 'validation_fails' => false];
        }
    }

    /*
    Admin Function to add and update user
    * */
    public static function addUser($request){
        $data = $request->all();
        //Server side validations
        $validator = \Validator::make($data, User::addUserValidationRules(), User::$registerUserValidationMessages);
        if ($validator->fails()) {
            //Redirect user back with input if server side validation fails
            return ['validation_fails' => true, 'validator' => $validator];
        }
        $user = new User();
        $user->name                 = $request->input('name');
        $user->last_name            = $request->input('last_name');
        $user->email                = $request->input('email');
        $user->role_id              = $request->input('role_id');
        $user->status               = 1;
        $user->plan_id              = $request->input('plan_type');
        $user->billing_cycle        = $request->input('billing_cycle');

        if($request->has('password')){
            $user->password        = Hash::make($request->input('password'));
        }
        $user->plan_purchase_date = date("Y-m-d");
        $planPurchaseDate = date("Y-m-d");
        if($request->input('billing_cycle') == 'quarterly')
            $user->plan_expiry_date = date('Y-m-d', strtotime($planPurchaseDate . '+90 days'));
        else if($request->input('billing_cycle') == 'yearly')
            $user->plan_expiry_date = date('Y-m-d', strtotime($planPurchaseDate . '+365 days'));
        $user->free_trial_expiry = date('Y-m-d', strtotime($planPurchaseDate . '+30 days'));
        //Save user details
        if ($user->save()) {
            return ['user_saved' => true, 'user' => $user, 'validation_fails' => false];
        } else {
            return ['user_saved' => false, 'user' => [], 'validation_fails' => false];
        }
    }

    public static function updateUser($request,$id){
        $data = $request->all();
        //Server side validations
        $validator = \Validator::make($data, User::updateUserValidationRules(), User::$registerUserValidationMessages);
        $validator->after(function ($validator) use ($id , $data) {
            $checkUser = User::where('email', $data['email'])->where('id','!=',$id)->get();
            if (count($checkUser->toArray()) > 0) {
                $validator->errors()->add('name', 'Email address already exists.');
            }
        });
        if ($validator->fails()) {
            //Redirect user back with input if server side validation fails
            return ['validation_fails' => true, 'validator' => $validator];
        }
        $user                       = User::findOrFail($id);
        $user->name                 = $request->input('name');
        $user->last_name            = $request->input('last_name');
        $user->email                = $request->input('email');
        $user->role_id              = $request->input('role_id');
        $user->status               = $request->input('status');
        $user->plan_id              = $request->input('plan_type');
        $user->billing_cycle        = $request->input('billing_cycle');
        $userPlan = User::Where('id',$id)->first();
        if($userPlan->plan_type != $data['plan_type']){
            $user->plan_purchase_date = date("Y-m-d");
            $planPurchaseDate = date("Y-m-d");
            if($request->input('billing_cycle') == 'quarterly')
                $user->plan_expiry_date = date('Y-m-d', strtotime($planPurchaseDate . '+90 days'));
            else if($request->input('billing_cycle') == 'yearly')
                $user->plan_expiry_date = date('Y-m-d', strtotime($planPurchaseDate . '+365 days'));
            $user->free_trial_expiry = date('Y-m-d', strtotime($planPurchaseDate . '+30 days'));
        }
        //Save user details
        if ($user->save()) {
            return ['user_saved' => true, 'user' => $user, 'validation_fails' => false];
        } else {
            return ['user_saved' => false, 'user' => [], 'validation_fails' => false];
        }
    }

    public static function updateUserProfile($request,$id){
        $data                               = $request->all();
        $user                               = User::findOrFail($id);
        $user->name                         = preg_replace('!\s+!', ' ', $request->input('first_name'));
        $user->last_name                    = preg_replace('!\s+!', ' ', $request->input('last_name'));
        $user->contact_number               = trim($request->input('contact_number'));
        $user->billing_address_line1        = $request->input('address_line1');
        $user->billing_address_line2        = $request->input('address_line2');
        $user->country_id                   = $request->input('country_id');
        $user->state_id                     = $request->input('state_id');
        $user->city                         = $request->input('city');
        $user->zip_code                     = $request->input('zip_code');
        $user->company_name                 = $request->input('company_name');
        $user->preferred_payment_type       = $request->input('payment_method');
        if($request->has('change_email')){
            $user->email    = $request->input('email');
        }
        if ($request->has('change_password')){
            $user->password        =  Hash::make($request->input('password'));
        }
        //Update user details
        if ($user->save()) {
            return ['sucess'=>true,'status' => 200, 'user' => $user, 'validation_fails' => false];
        } else {
            return ['sucess'=>false,'status' => 404,'user' => [], 'validation_fails' => false];
        }

    }

    public static function generateRandomString($length = 6) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }


    static function checkPasswordResetToken($userId,$forgotPasswordToken){
        $result = DB::table('users')->select('id','email')->where('id',$userId)->where('forgot_password_token',$forgotPasswordToken)->get();
        return $result;
    }

    static function updateNewPassword($userId,$dataArray){

        $result = \DB::table('users')->where('id',$userId)->update($dataArray);
        return $result;
    }

    /**
     * Upload file
     * @param $file
     * @param $dir
     * @return mixed|string
     */
    public static function uploadFiles($file, $dir)
    {
        $date = \Carbon\Carbon::now();
        $currentTimeStamp = $date->getTimestamp();
        $fileOriginalName = $currentTimeStamp.'_'.$file->getFilename().'.'.$file->getClientOriginalExtension();


        //Store file to folder
        $file->move($dir, $fileOriginalName);
        $finalDoc = $fileOriginalName;

        //Get file mime type
        $mimeType = $file->getClientMimeType();

        //Optimize if uploaded file is image
        if(self::checkImageMimes($mimeType)) {
            $imageToResize = $dir . $fileOriginalName;

            $imageName = pathinfo($fileOriginalName, PATHINFO_FILENAME);
            $finalDoc = $fileOriginalName;

            //Convert png to jpg
            if ($mimeType == 'image/png') {
                $convertedFile = $imageName . '.jpg';
                $convertedFilePath = $dir . $convertedFile;
                $imageToResize = self::convertToJpg($imageToResize, $convertedFilePath);
                $finalDoc = $convertedFile;
            }

            //Resize image if width is greater than 1200
            $imageWidth = self::getImageWidth($imageToResize);

            if ($imageWidth > 1200) {
                $finalDoc = self::resizeImage($imageToResize);
            }
        }

        return $finalDoc;
    }


    /**
     * Check if given mime is available in allowed mimes list
     * @param $mime
     * @return bool
     */
    public static function checkImageMimes($mime)
    {
        $mimes = array('image/jpeg',
            'image/jpg',
            'image/bmp',
            'image/png'
        );

        if(in_array($mime, $mimes)){
            return true;
        }
        return false;
    }


    /**
     * Convert image to jpg
     * @param $imageToConvert
     * @param $convertedFile
     * @return string
     */
    public static function convertToJpg($imageToConvert, $convertedFile)
    {
        $img = \Image::make($imageToConvert)->encode('jpg', 80)->save($convertedFile);
        unlink($imageToConvert);
        return $img->dirname.'/'.$img->basename;
    }

    /**
     * Get image width
     * @param $image
     * @return mixed
     */
    public static function getImageWidth($image)
    {
        return \Image::make($image)->width();
    }

    /**
     * Get image height
     * @param $image
     * @return mixed
     */
    public static function getImageHeight($image) {
        return \Image::make($image)->height();
    }

    /**
     * Resize image
     * @param $fileToResize
     * @return mixed
     */
    public static function resizeImage($imageToResize)
    {
        $img = \Image::make($imageToResize)->resize(1200, null)->encode('jpg', 80)->save();
        return $img->basename;
    }
    
    public static function checkUserTrialExpiry($email){
        $nowDate = date("Y-m-d");
        $user = DB::table('users')
            ->where('email', $email)
            ->where('role_id', 2)
            ->where('status', 1)
            ->where('free_trial_expiry', '<=', $nowDate)
            ->whereNULL('deleted_at')
            ->count();
        return $user;
    }
    
    public static function checkPaymentStatus($email){
        $user = DB::table('users')
            ->where('email', $email)
            ->where('status', 1)
            ->first();        

        if(!empty($user)){
            $userPayment = DB::table('subscription_transaction')
            ->where('user_id', $user->id)
            ->where('payment_status', 1)
            ->first();
            return $userPayment;
        }else{
            return FALSE;
        }
    }
    
    public static function checkUserSubscriptionExpiry($email){
        $nowDate = date("Y-m-d");
        $user = DB::table('users')
            ->where('email', $email)
            ->where('status', 1)
            ->where('plan_expiry_date', '<=', $nowDate)
            ->count();
        return $user;
    }

    public static function getSubUsersCount(){
        $userCount = DB::table('users')
            ->where('parent_user_id', Auth::user()->id)
            ->where('is_sub_user', 1)
            ->count();
        return $userCount;
    }
}
