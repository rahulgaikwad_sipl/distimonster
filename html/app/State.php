<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class State extends Model
{
    protected $table = 'states';

    public static function getStateByCountryId($countryId)
    {
        $states = DB::table('states')->where('country_id', '=', $countryId)
            ->select('name', 'id')->orderBy('name','ASC')
            ->get()->toArray();
        return $states;
    }

    public static function getSateByCountry($countryId){
        //Get all state by country id
        $states = self::orderBy('name','ASC')
            ->where('country_id', $countryId)
            ->pluck('name', 'id')
            ->all();
        $default = ['' => 'Select State*'];
        $states = $default + $states;
        return $states;
    }
}