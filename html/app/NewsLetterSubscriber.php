<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 26/04/18
 * Time: 2:25 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;

class NewsLetterSubscriber extends Model
{
    protected $table = 'newsletter_subscribers';

    public static function checkAlreadyInSubscriberList($email){
        $result = DB::table('newsletter_subscribers')
            ->where('subscriber_email', $email)
            ->where('is_subscribed' ,1)
            ->select('id','subscriber_email')
            ->get()->toArray();
        return $result;
    }

    static function updateNewsLetterSubscription($email,$dataArray){
        $result = \DB::table('newsletter_subscribers')->where('subscriber_email',$email)->update($dataArray);
        return $result;
    }

}