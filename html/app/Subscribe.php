<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscribe extends Model{
    
    protected $table = 'users';
    use Notifiable;
    use SoftDeletes;
    
    /**
     * Common validation rules
     * @return array
     */
    public static $subscribeUserValidationRules = [
        'company_name'  => 'required',
        'name' => 'required',
        'last_name' => 'required',
        'employee' => 'required',
        'title_or_job' => 'required',
        'department' => 'required',
        'know_about_distimonster' => 'required',
        'company_type' => 'required',
        'contact_number' => 'required',
        'email' => 'required|email',
        'zip_code' => 'required',
        'plan_type' => 'required',
        'billing_cycle' => 'required'
    ];
    
    public static  $subscribeUserValidationMessages = [
            'company_name.required' => 'Please enter company name.',
            'name.required' => 'Please enter first name.',
            'last_name.required' => 'Please enter last name.',
            'employee.required' => 'Please select number of employee.',
            'title_or_job.required' => 'Please enter title or job.',
            'department.required' => 'Please enter department.',
            'know_about_distimonster.required' => 'Please enter know about Distimonster.',
            'company_type.required' => 'Please select company type.',
            'phone_number.required' => 'Please enter valid phone number.',
            'email.required' => 'Please enter email.',
            'email.email' => 'Invalid email address.',
            'email.unique' => 'Email address is already in use.',
            'password.required' => 'Please enter password',
            'password.min' => 'Password consists of at least 6 character.',
            'password.max' => 'Password should not be exceed 20 characters.',
            'zip_code.required' => 'Please enter valid zipcode.',
            'zip_code.min' =>'Please enter at least 5 character for zip code.',
            'zip_code.max' =>'Zip code should not be exceed 8 characters.',
            'billing_state.required' => 'Please select a state.',
            'billing_country.required' => 'Please select a country.',
            'billing_city.required' => 'Please select a city.',
            'plan_type.required' => 'Please select plan.',
            'billing_cycle.required' => 'Please select billing.',
        ];
    
        public static function registerNewUser($userData){
            $id = DB::table('users')->insertGetId($userData);
            if ($id) {
                //Assign role to registered user
                return ['status' => true, 'user' => $id, 'validation_fails' => false];
            } else {
                return ['status' => false, 'user' => [], 'validation_fails' => false];
            }
        }//End Function
        
        public static function subUserRegister($subUserData){
            
            $id = DB::table('users')->insertGetId($subUserData);
            if ($id) {
                //Assign role to registered user
                return ['status' => true, 'subUser' => $id, 'validation_fails' => false];
            } else {
                return ['status' => false, 'subUser' => [], 'validation_fails' => false];
            }
        }//End Function
        
        public static function newUserBillingStore($billingUserData){
            $id = DB::table('subscription_billing_info')->insertGetId($billingUserData);
            if ($id) {
                //Assign role to registered user
                return ['status' => true, 'subUser' => $id, 'validation_fails' => false];
            } else {
                return ['status' => false, 'subUser' => [], 'validation_fails' => false];
            }
        }//End Function
        
        public static function updateUserBillingFromAdmin($billingUserData, $billingUserId){
            $result = \DB::table('subscription_billing_info')->where('user_id',$billingUserId)->update($billingUserData);
            if ($result) {
                //Assign role to registered user
                return ['status' => true, 'result' => $result, 'validation_fails' => false];
            } else {
                return ['status' => false, 'result' => [], 'validation_fails' => false];
            }
        }//End Function

        public static function updatePaymentStatus($updateBillingTable, $billingId){
            $result = \DB::table('subscription_billing_info')->where('sub_billing_id',$billingId)->update($updateBillingTable);
            return $result;
        }//End Function
        
        public static function updateOldTransactionPayment($updateOldTransaction, $userId){
            $result = \DB::table('subscription_transaction')->where('user_id',$userId)->update($updateOldTransaction);
            return $result;
        }//End Function
        
        public static function updateUserExpiryDates($updateUserExpiry, $userId){
            
            $result = \DB::table('users')->where('id',$userId)->update($updateUserExpiry);
            
            $SubUserExist = DB::table('users')
                ->where('parent_user_id', $userId)
                ->where('status', 1)
                ->where('role_id', 2)
                ->count();
            
            if(($result) && ($SubUserExist > 0)){
                $resultSub = \DB::table('users')->where('parent_user_id',$userId)->update($updateUserExpiry);
                return $resultSub;
            }else{
                return $result;
            }
        }//End Function
        
        public static function insertSubscriptionTransaction($transactionArray){
            $result = \DB::table('subscription_transaction')->insertGetId($transactionArray);
            if ($result) {
                //Assign role to registered user
                return ['status' => true, 'payment' => $result, 'validation_fails' => false];
            } else {
                return ['status' => false, 'payment' => [], 'validation_fails' => false];
            }
        }//End Function
        
        /* Cron Model Code Start */
                
        public static function checkTrialExpiryForPayment(){
            $nowDate = date("Y-m-d");
            $user = DB::table('users as A')
                ->select('B.sub_billing_id', 'B.company_name', 'B.billing_first_name', 'B.billing_last_name', 'B.billing_address', 'B.billing_phone', 'B.billing_zipcode', 'B.billing_city', 'B.billing_state', 'B.billing_country', 'B.billing_email', 'B.grand_total_amount', 'B.paypal_card_id', 'B.user_id')
                ->leftJoin('subscription_billing_info as B', 'A.id', 'B.user_id')
                ->where('A.status', 1)
                ->where('A.parent_user_id', 0)
                ->where('A.role_id', 2)
                ->where('B.user_id', '!=', '')
                ->where('B.is_paid', '=', 0)
                ->where('A.free_trial_expiry', '<=', $nowDate)
                ->orderBy("id", "DESC")
                ->get();
            
            return $user;
        }//End Function
                
        public static function renewPaymentSubscription(){
            $nowDate = date("Y-m-d");
            $user = DB::table('users as A')
                ->select('B.sub_billing_id', 'B.company_name', 'B.billing_first_name', 'B.billing_last_name', 'B.billing_address', 'B.billing_phone', 'B.billing_zipcode', 'B.billing_city', 'B.billing_state', 'B.billing_country', 'B.billing_email', 'B.grand_total_amount', 'B.paypal_card_id', 'B.user_id', 'C.subscription_transaction_id', DB::RAW('DATE_FORMAT(`C`.`created_at`, "%Y-%m-%d") as payment_date'))
                ->leftJoin('subscription_billing_info as B', 'A.id', 'B.user_id')
                ->leftJoin('subscription_transaction as C', 'A.id', 'C.user_id')
                ->where('A.status', 1)
                ->where('A.parent_user_id', 0)
                ->where('A.role_id', 2)
                ->where('B.user_id', '!=', '')
                ->where('B.is_paid', '=', 1)
                ->where('C.payment_status', '=', 'completed')
                ->where('C.is_renew', '=', 0)
                ->where('A.plan_expiry_date', '=', $nowDate)
                ->orderBy("id", "DESC")
                ->get();            
            return $user;
        }//End Function
        
        public static function checkTwentyFifthTrialExpiry(){
            $nowDate = date("Y-m-d");
            $user = DB::table('users as A')
                ->select('B.billing_first_name', 'B.billing_last_name', 'B.billing_email', 'B.user_id', DB::raw('DATEDIFF(NOW(), A.free_trial_expiry) as expire_day'))
                ->leftJoin('subscription_billing_info as B', 'A.id', 'B.user_id')
                ->where(DB::raw('DATEDIFF(NOW(), A.free_trial_expiry)'), '=', '25')
                ->where('A.status', 1)
                ->where('A.parent_user_id', 0)
                ->where('A.role_id', 2)
                ->where('B.user_id', '!=', '')
                ->where('B.is_paid', '=', 0)
                ->where('A.free_trial_expiry', '<=', $nowDate)
                ->orderBy("id", "DESC")
                ->get();
            
            return $user;
        }//End Function
        
        public static function getSubscriptionExpiryUsers(){
            $nowDate = date("Y-m-d");
            $user = DB::table('users as A')
                ->select('B.billing_first_name', 'B.billing_last_name', 'B.billing_email', 'B.user_id', DB::raw('DATEDIFF(A.plan_expiry_date, NOW()) as expire_day'))
                ->leftJoin('subscription_billing_info as B', 'A.id', 'B.user_id')
                ->where(DB::raw('DATEDIFF(A.plan_expiry_date, NOW())'), '=', '30')
                ->where('A.status', 1)
                ->where('A.parent_user_id', 0)
                ->where('A.role_id', 2)
                ->where('B.user_id', '!=', '')
                ->where('B.is_paid', '=', 1)
                ->where('A.plan_expiry_date', '>', $nowDate)
                ->orderBy("id", "DESC")
                ->get();
            
            return $user;
        }//End Function
        
        public static function getUserBillingForPayment($userId){
            $nowDate = date("Y-m-d");
            $user = DB::table('users as A')
                ->select('B.sub_billing_id', 'B.company_name', 'B.billing_first_name', 'B.billing_last_name', 'B.billing_address', 'B.billing_phone', 'B.billing_zipcode', 'B.billing_city', 'B.billing_state', 'B.billing_country', 'B.billing_email', 'B.user_total_amount', 'B.grand_total_amount', 'B.paypal_card_id', 'B.user_id')
                ->leftJoin('subscription_billing_info as B', 'A.id', 'B.user_id')
                ->where('A.status', 1)
                ->where('A.parent_user_id', 0)
                ->where('A.role_id', 2)
                ->where('A.id', $userId)
                ->first();
            
            return $user;
        }//End Function
        
        /* Cron Model End */
}