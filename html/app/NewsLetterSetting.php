<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsLetterSetting extends Model
{
    protected $table = 'newsletter_setting';

}
