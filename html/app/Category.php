<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 26/03/18
 * Time: 11:09 AM
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class Category extends Model
{
    protected $table = 'categories';

    public static function getCategoryList(){
        $categories = Category::where('is_active', '=', 1)->orderBy('name','ASC')->get()->toArray();
        return $categories;
    }

    public static function getCategoryListByKeyword($searchParam){
        $categories = Category::where('is_active', '=', 1)->Where('name', 'like', '%' . $searchParam . '%')->get()->toArray();
        return $categories;
    }

    public static function getCategoryNameBySlug($catName){
        $categories = Category::where('slug', '=', $catName)->orderBy('name','ASC')->get();
        return $categories;
    }

    public static function getCategoryPart($category ,$keyword,$offset){
        $limit = config('constants.PART_SEARCH_LIMIT');
        if($keyword != ''){
            $part = DB::select( DB::raw("SELECT * FROM part_details_incremental PARTITION (p0) WHERE category = '$category'  AND manufacturer_part_number ='$keyword'   LIMIT $offset,$limit") );
        }else{
            $part = DB::select( DB::raw("SELECT * FROM part_details_incremental PARTITION (p0) WHERE category = '$category'  LIMIT $offset,$limit") );
        }
        return $part;
    }
}