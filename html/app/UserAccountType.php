<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 06/04/18
 * Time: 11:37 AM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class UserAccountType extends Model
{
    protected $table = 'user_account_types';

    public static function getAccountTye(){
        $accountType = self::orderBy('id')
            ->pluck('name', 'id')
            ->all();
        $default = ['' => 'Select Net terms'];
        $accountType = $default + $accountType;
        return $accountType;
    }

}