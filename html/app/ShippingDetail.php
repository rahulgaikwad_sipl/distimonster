<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 09/03/18
 * Time: 6:56 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class ShippingDetail extends Model
{
    protected $table = 'shipping_detail';

    public static $shippingStatus = [
        ''=>'Select Status',
        1=> 'Shipped',
        2=> 'Pending',
        3=> 'In Transit',
        4=> 'Delivered'
    ];

    public static $paymentStatus = [
        ''=>'Select Status',
            0=>     'Pending',
            1=>     'Done',
            2=>     'Failed',
            3=>     'Refunded',
    ];
}