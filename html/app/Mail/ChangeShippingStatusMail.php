<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 09/04/18
 * Time: 3:40 PM
 */

namespace App\Mail;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChangeShippingStatusMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    public $subject;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data,$subject)
    {
        $this->data     = $data;
        $this->subject  = $subject;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(){
        return $this->subject($this->subject)->markdown('emails.change-shipping-status',$this->data);
    }
}