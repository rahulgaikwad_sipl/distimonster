<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class PaymentMethod extends Model
{
    protected $table = 'payment_methods';

    public static function getAllPaymentMethod(){
        $paymentMethod = DB::table('payment_methods')
            ->select('id', 'payment_method_name')
            ->orderBy('display_order','ASC')
            ->get()->toArray();
        return $paymentMethod;
    }

    public static  function getShippingMethodById($selectedShippingMethodId){
        $states = DB::table('shipping_methods')
            ->where('id',$selectedShippingMethodId)
            ->select('shipping_methods.id', 'shipping_methods.shipping_method_name','shipping_methods.shipping_charges')
            ->get()->toArray();
        return $states;
    }
}