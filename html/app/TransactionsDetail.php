<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
/**
 * Class Role
 *
 * @package App
 * @property string $title
 */
class TransactionsDetail extends Model
{
    protected $table = 'transactions_details';

}
