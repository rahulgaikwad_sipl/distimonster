<?php
/**
 * Created by PhpStorm.
 * User: mayank
 * Date: 14/05/19
 * Time: 07:43 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;

class RequestADemo extends Model
{
    protected $table = 'request_a_demo';

    public static $validationMessages = [
        'company_name.required'             => 'Please enter company name',
        'company_name.max'                  => 'Company name can be max 255 characters long',
        'contact_name.required'             => 'Please enter contact name',
        'contact_name.max'                  => 'Contact name can be max 255 characters long',
        'email.required'                    => 'Please enter email address',
        'email.email'                       => 'Email address is invalid',
        'email.max'                         => 'Email address can be max 255 characters long',
        'phone.required'                    => 'Please enter phone number',
        'phone.min'                         => 'Phone number can be minimum 12 digits long',
        'phone.max'                         => 'Phone number can be max 15 digits long',
        'phone.regex'                       => 'Phone number is invalid'
    ];

    public static function validationRulesForRequestADemo() {
        return [
            'company_name'      => 'required|max:255',
            'contact_name'      => 'required|max:255',
            'email'             => 'required|email|max:255',
            'phone'             => 'required|min:12|max:15|regex:/^[0-9]*$/'
        ];
    }
}