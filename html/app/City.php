<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class City extends Model
{
    protected $table = 'cities';

    public static function getCityByStateId($stateId)
    {
        $cities = DB::table('cities')->where('state_id', '=', $stateId)
            ->select('name', 'id')
            ->get()->toArray();
        return $cities;
    }

    public static function getCityByState($stateId){
        //Get all city by state id
        $cities = self::orderBy('name')
            ->where('state_id', $stateId)
            ->pluck('name', 'id')
            ->all();
        $default = ['' => 'Select City*'];
        $cities = $default + $cities;
        return $cities;
    }
}