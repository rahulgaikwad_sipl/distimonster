<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Webpatser\Uuid\Uuid;
use DB;
/**
 * Class News
 *
 * @package App
 * @property string $title
*/
class News extends Model
{
    protected $fillable = ['title','description','image'];

    public static function newsList(){
        $news = DB::table('news')
            ->select('*')
            ->orderBy('id','desc')
            ->whereNull('deleted_at')
            ->where('status', 1)
            ->get();
        return $news;
    }

    public static function  getNewsDetailsById($id){
    	$getNewsDetailsById = DB::table('news')
            ->select('*')
            ->where('id', $id)
            ->first();
        return (array) $getNewsDetailsById;
    }
}
