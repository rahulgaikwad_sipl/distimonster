<?php
/**
 * Created by PhpStorm.
 * User: brajesh
 * Date: 06/04/18
 * Time: 6:09 PM
 */

namespace App;
use Illuminate\Database\Eloquent\Model;
use DB;
class NetAccountHolder extends Model
{
    protected $table = 'net_account_holders';

   public static $netAccountStatus = [
       ''=>'Select',
       0=>'Pending',
       1=>'Approved',
       2=>'In Progress',
       3=>'Rejected'
   ];

   public static function getNetAccountRequestStatus($userId){
       $requestStatus = DB::table('net_account_holders')
                            ->where('user_id',$userId)
                            ->select('id', 'status','user_id')
                            ->get()->toArray();
       return $requestStatus;
   }

}