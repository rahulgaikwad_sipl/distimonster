<?php
Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('config:clear');
    return "Cache is cleared";
});
//////////////////////////////////////////////////////////
///
///   FRONT SITE ROUTES
///
/// //////////////////////////////////////////////////////
Route::group(['middleware' => ['CheckUserRole']], function () {
    Route::get('/', 'HomeController@index');
    Route::get('/what-is-distimonster', 'HomeController@whatIsDistimonster');
    Route::get('/engineering', 'HomeController@comingSoon');
    /*User Authentication Routes*/
    Route::get('/login', 'Frontend\AuthController@login');
    Route::post('/login', 'Frontend\AuthController@userLogin');
    Route::get('/login/{redirectUri}', 'Frontend\AuthController@login');
    Route::get('/login/{redirectUri}/{bom_id}', 'Frontend\AuthController@login');
    //Route::get('/send-mail', 'Frontend\AuthController@sendMail');
    /*User Registration Routes*/
    Route::get('/register', 'Frontend\AuthController@registerView');
    Route::post('/register-user', 'Frontend\AuthController@saveNewRegisterUserInfo');
    Route::get('/check-newemail', 'Frontend\AuthController@checkEmail');

    Route::get('/logout-user', 'Frontend\AuthController@logoutFrontUser');
    /*Forgot password route*/
    Route::get('/forget-password', 'Frontend\AuthController@forgetPassword');
    Route::post('/forget-password', 'Frontend\AuthController@sendResetPasswardMail');
    Route::get('reset-password/{id}','Frontend\AuthController@resetPasswordView');
    Route::post('reset-password/{id}','Frontend\AuthController@updateNewPassword');
    /*Product Route*/
    Route::get('/product-listing','Frontend\ProductController@productListing');
    /*Cart Route*/
    Route::get('/cart', 'Frontend\CartController@getCart');
    Route::post('/add-to-cart', 'Frontend\CartController@addToCart');
    Route::post('/add-unsaved-quote-to-cart', 'Frontend\QuoteCartController@addUnSavedQuoteToCart');

    Route::post('/add-bom-part-to-cart', 'Frontend\CartController@addBomPartToCart');
    Route::post('/add-selected-part-to-cart', 'Frontend\CartController@addSelectedPartToCart');
    Route::post('/remove-cart-item','Frontend\CartController@removeCartItem');
    Route::post('/empty-cart','Frontend\CartController@emptyCart');
    /*Quote Cart*/
    Route::get('/quote-cart', 'Frontend\QuoteCartController@quoteCart');
    Route::post('/add-to-quote', 'Frontend\QuoteCartController@addToQuote');
    Route::post('/remove-quote-cart-item','Frontend\QuoteCartController@removeQuoteCartItem');
    Route::post('/empty-quote-cart','Frontend\QuoteCartController@emptyQuoteCart');
    Route::post('/submit-quote','Frontend\QuoteCartController@submitQuote');

    Route::post('/add-all-bom-to-quote', 'Frontend\BomController@addAllBomToQuote');
    /*My account routes*/
    Route::group(['middleware' => ['FrontAuth']], function () {
    Route::get('/account-settings/{id}', 'Frontend\ProfileController@profile');
    Route::post('/account-settings/{id}', 'Frontend\ProfileController@updateProfile');
    /*Start Order Route*/
    Route::get('/order-history', 'Frontend\PagesController@orderHistory');
    Route::get('order-details/{id}', 'Frontend\PagesController@orderDetails');
    Route::get('reorder/{id}', 'Frontend\PagesController@reOrder');
    Route::post('/confirm-reorder-and-addtocart', 'Frontend\CartController@confirmReorderAndAddToCart');
    /*End Order Route*/

    /* Request lead time route */
    Route::get('/requested-lead-times', 'Frontend\QuoteCartController@RequestedLeadTimes');
    Route::get('/responded-lead-times', 'Frontend\QuoteCartController@RespondedLeadTimes');

    /*Start Quote Route*/
    Route::get('/quotes', 'Frontend\QuoteCartController@quoteList');
    Route::get('/pending-quotes', 'Frontend\QuoteCartController@pendingQuoteList');
    Route::get('/processed-quotes', 'Frontend\QuoteCartController@approvedQuoteList');
    Route::get('/rejected-quotes', 'Frontend\QuoteCartController@rejectedQuoteList');        
    Route::get('/expired-quotes', 'Frontend\QuoteCartController@expiredQuoteList');
    Route::get('/ordered-quotes', 'Frontend\QuoteCartController@orderedQuoteList');

    Route::post('remove-quote','Frontend\QuoteCartController@removeQuote');
    Route::post('move-cart-to-quote','Frontend\QuoteCartController@moveCartToQuote');
    Route::get('{id}/quote-details', 'Frontend\QuoteCartController@quotesDetails');
    Route::post('/add-to-cart-from-quote', 'Frontend\QuoteCartController@addToCartFromQuote');
    Route::GET('/similar-parts','Frontend\QuoteCartController@searchSimilarParts');
    Route::post('/add-approved-quote-item-to-cart', 'Frontend\QuoteCartController@addApprovedQuoteItemToCartFromQuote');
    Route::get('/add-quote-to-cart/{quoteId}', 'Frontend\QuoteCartController@addQuoteToCart');
    Route::post('/add-all-quote-items-to-cart', 'Frontend\QuoteCartController@addAllQuoteItemsToCart');
    Route::post('/add-to-cart-similar-parts','Frontend\QuoteCartController@addSimilarPartsToCart');
    Route::get('/download-quote/{id}','Frontend\QuoteCartController@downloadQuote');
    /*End Quote*/
    /*Start Bom Route*/
    Route::get('/my-bom', 'Frontend\BomController@myBom');
    Route::get('/saved-bom', 'Frontend\BomController@savedBom');
    Route::get('/shared-bom', 'Frontend\BomController@sharedBom');
    Route::get('/ordered-bom', 'Frontend\BomController@orderedBom');
    Route::post('/share-bom', 'Frontend\BomController@shareBom');
    Route::get('/download-bom/{id}','Frontend\BomController@downloadBom');
    Route::get('{id}/bom-details', 'Frontend\BomController@bomDetails');
    Route::post('/update-bom-name', 'Frontend\BomController@updateBomName');
    /*End Bom Route*/
    /* Start Apply net term account Route */
    Route::get('/apply-net-term-account', 'Frontend\NetTermController@applyNetTermAccount');
    Route::post('/apply-net-term-account', 'Frontend\NetTermController@saveNetTermAccountDetails');
    /* End Apply net term account Route */
    /*Apply coupon code*/
    Route::post('/apply-code', 'Frontend\CheckOutController@applyCouponCode');
    Route::post('/remove-coupon-code', 'Frontend\CheckOutController@removeCouponCode');
    /*Start Bio Route*/
    Route::get('/my-bio', 'Frontend\BioController@myBio');
    Route::post('/my-bio', 'Frontend\BioController@myBio');
    /*End Bio Route*/
    Route::get('/dashboard', 'Frontend\DashboardController@index');

    Route::get('/my-team', 'Frontend\SubUserController@index');
    Route::get('/add-sub-user', 'Frontend\SubUserController@getSubUserForm');
    Route::post('/add-sub-user', 'Frontend\SubUserController@addSubUsers');

});

    Route::post('/check-user-status', 'Frontend\AuthController@checkUserStatus');
    /*Shipping Address*/
    Route::get('/add-shipping-address', 'Frontend\ShippingController@addShippingAddress');
    Route::get('/shipping-addresses', 'Frontend\ShippingController@listShippingAddress');
    Route::post('/remove-shipping-address', 'Frontend\ShippingController@removeShippingAddress');
    /*End*/
    /*Review and order complete route */
    Route::get('/review', 'Frontend\ShippingController@reviewDetails');

    /*Order route*/
    Route::post('/place-order', 'Frontend\CheckOutController@placeOrder');

    /*Bom route*/
    Route::get('/bom', 'Frontend\BomController@bom');
    Route::post('/bom', 'Frontend\BomController@uploadBom');
    Route::get('/bom-details', 'Frontend\PagesController@bomDetails');
    Route::post('/remove-bom', 'Frontend\BomController@removeBomById');
    Route::get('/bom/{bom_id}', 'Frontend\BomController@bom');
    Route::get('/get-share-bom-notes/{bom_id}', 'Frontend\BomController@getShareBomNotes');
    /**/

    /*General routes */
    Route::get('/about', 'Frontend\PagesController@aboutUs');
	Route::get('/meet-the-team', 'Frontend\PagesController@meetTheTeam');
    Route::get('/news-updates', 'Frontend\PagesController@newUpdates');
    Route::get('/news/{id}', 'Frontend\PagesController@getNewsDetailsById');
    Route::get('/request-a-demo', 'Frontend\PagesController@requestADemo');
    Route::post('/save-request-a-demo', 'Frontend\PagesController@saveRequestADemo');
    Route::get('/faq', 'Frontend\PagesController@faq');
    Route::get('/return-policy', 'Frontend\PagesController@returnPolicy');
    Route::get('/contact', 'Frontend\PagesController@contactUS');
    Route::get('/sitemap', 'Frontend\PagesController@siteMap');
    Route::get('/terms-of-use', 'Frontend\PagesController@usesTerms');
    Route::get('/sale-terms-condition', 'Frontend\PagesController@tcSale');
    Route::get('/privacy-policy', 'Frontend\PagesController@privacyPolicy');
    Route::get('/export-terms-condition', 'Frontend\PagesController@tcExport');
    Route::get('/cookie-policy', 'Frontend\PagesController@cookiePolicy');
    Route::post('/signup-newsletter','Frontend\PagesController@signUpNewsLetter');
    Route::get('/unsubscribe-newsletter/{email}','Frontend\PagesController@unsubscribeNewsLetter');

    //Route::post('/signup-newsletter','Frontend\PagesController@signUpNewsLetter');
    /*End*/

    /*Check Out */
    Route::get('/shipping', 'Frontend\ShippingController@shipping');
    Route::any('/review-order', 'Frontend\ShippingController@reviewOrderDetails');

    /*Shipping Route */
    Route::get('/select-shipping-address', 'Frontend\ShippingController@shipping');
    /*End*/

    /*City &State List Route */
    Route::post('/state', 'Frontend\ShippingController@stateListByCountryName');
    Route::post('/city', 'Frontend\ShippingController@cityListByStateName');
    /*End*/

    /*Add Shipping Address*/
    Route::get('/add-shipping-address', 'Frontend\ShippingController@shippingAddress');
    Route::post('/add-shipping-address', 'Frontend\ShippingController@addShippingAddress');
    Route::get('/update-shipping-address/{id}', 'Frontend\ShippingController@editShippingAddress');
    Route::post('/update-shipping-address/{id}', 'Frontend\ShippingController@updateShippingAddress');
    /*End*/

    /*Payment Routes*/
    Route::get('paypal/{orderId}/{orderTotalAmount}', array('as' => 'paypal', 'uses' => 'Frontend\PaypalController@postPayment',));
    Route::get('credit-card/{orderId}/{orderTotalAmount}/{cardNumber}/{expiryMonth}/{cvv}/{nameOnCard}/{cardType}/{expiryYear}', array('as' => 'credit-card', 'uses' => 'Frontend\PaypalController@creditCardPayment',));
    // this is after make the payment, PayPal redirect back to your site
    Route::get('payment/status', array('as' => 'payment.status', 'uses' => 'Frontend\PaypalController@getPaymentStatus',));
    Route::get('payment/error', array('as' => 'payment.error', 'uses' => 'Frontend\PaypalController@getPaypalError',));
    Route::get('order-process-error/{txNumber}/{orderId}','Frontend\CheckOutController@paymentError');
    Route::get('shopping-completed/{txNumber}/{orderId}', array('as'=>'shopping-completed','uses'=>'Frontend\CheckOutController@shoppingCompleted'));

    /*End*/
    /*Search part */
    Route::get('/search/{partNum?}', 'Frontend\ProductController@searchProduct');
    Route::post('/get-part-details', 'Frontend\ProductController@suggestPartDetails');
    Route::post('/auth', 'Frontend\AuthController@adminAuth');
    Route::post('/send-password', 'Frontend\AuthController@forgotEmailSend');
    Route::get('check-admin-email', 'Frontend\AuthController@checkAdminEmail');
    Route::get('not-found','Frontend\AuthController@resourceNotFound');
    Route::post('/clear-search-history', 'Frontend\ProductController@clearSearchHistory');
    Route::get('products','Frontend\CategoryController@categoryList');
    Route::get('categories','Frontend\CategoryController@categoryList');
    Route::get('category/{slug}','Frontend\CategoryController@searchCategoryPart');
    Route::get('get-parts','Frontend\CategoryController@getParts');
    Route::get('search-part/{categorySlug?}/{partNumber?}','Frontend\CategoryController@searchPartInCategory');
    Route::post('request-product','Frontend\CategoryController@saveRequestedProduct');
    
    /*Subscribe Routes*/
    Route::get('subscription-plan','Frontend\SubscribeController@subscriptionPlan');
    Route::get('subscribe/{planType}','Frontend\SubscribeController@subscribe');
    Route::post('subscribe/registration','Frontend\SubscribeController@subscribeRegister');
    Route::post('subscribe/state', 'Frontend\SubscribeController@stateListByCountryName');
    Route::post('subscribe/city', 'Frontend\SubscribeController@cityListByStateName');
});

//////////////////////////////////////////////////////////
///
///   END FRONT SITE ROUTES
///
/// //////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
///
///   CRON ROUTES
///
/// //////////////////////////////////////////////////////

//$this->get('cron/email', 'Frontend\CronController@emailTemplate');
$this->get('cron/check-twenty-fifth-day', 'Frontend\CronController@checkTwentyFifthDayOfFreeTrial');
$this->get('cron/payment-after-trial-expiry', 'Frontend\CronController@paymentAfterTrialExpire');
$this->get('cron/check-subscription-expiry', 'Frontend\CronController@checkSubscriptionExpiry');
$this->get('cron/subscription-renew', 'Frontend\CronController@subscriptionPayment');

//////////////////////////////////////////////////////////
///
///   END CRON ROUTES
///
/// //////////////////////////////////////////////////////

//////////////////////////////////////////////////////////
///
///   ADMIN SITE ROUTES
///
/// //////////////////////////////////////////////////////

// Admin Authentication Routes...
Route::group(['middleware' => ['CheckAdminRole']], function () {
    $this->get('admin/login', 'Auth\LoginController@showLoginForm')->name('auth.login');
    $this->post('admin/login', 'Auth\LoginController@login')->name('auth.login');
    $this->post('admin/logout', 'Auth\LoginController@logout')->name('auth.logout');

// Admin Change Password Routes...
//$this->get('change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
    $this->get('admin/change_password', 'Auth\ChangePasswordController@showChangePasswordForm')->name('auth.change_password');
    $this->patch('admin/change_password', 'Auth\ChangePasswordController@changePassword')->name('auth.change_password');

// Admin Password Reset Routes...
    $this->get('admin/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('auth.password.reset');
    $this->post('admin/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('auth.password.reset');
    $this->get('admin/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('admin/password/reset', 'Auth\ResetPasswordController@reset')->name('auth.password.reset');

    Route::get('admin/login', 'Admin\HomeController@login');
    Route::get('admin', 'Admin\HomeController@login');
    Route::get('clear', function() {
       Artisan::call('cache:clear');
       Artisan::call('config:clear');
       Artisan::call('config:cache');
       Artisan::call('view:clear');
       return "Cleared!";
    });
});

Route::group(['middleware' => ['auth','CheckAdminRole'], 'prefix' => 'admin', 'as' => 'admin.'], function () {

    Route::get('/home', 'Admin\HomeController@index');
    Route::resource('users', 'Admin\UsersController');
    Route::get('userlist', 'Admin\UsersController@getUserList');
    Route::get('update-user-status', 'Admin\UsersController@updateUserStatus');
    Route::post('/delete-user', 'Admin\UsersController@deleteUser');

    /*Admin Profile routes*/
    Route::post('update-profile/{id}', 'Admin\UsersController@updateProfile');
    Route::get('profile', 'Admin\UsersController@getProfile');
    /*End*/
    /*News*/
    Route::resource('news', 'Admin\NewsController');
    Route::get('/newslist', 'Admin\NewsController@getNewList');
    Route::get('update-news-status', 'Admin\NewsController@updateNewsStatus');
    Route::post('/delete-news', 'Admin\NewsController@deleteNews');

    /*End*/

    /*News*/
    Route::get('newsletter', 'Admin\NewsLetterController@index');
    Route::post('send-newsletter', 'Admin\NewsLetterController@sendNewsLetter');

    /*End*/


    Route::get('/home/index', 'Admin\HomeController@index');
    /*Order */
    Route::get('/orders', 'Admin\OrderController@index');
    Route::get('/orders/{status}/{status_type}', 'Admin\OrderController@index');
    Route::get('/orderlist', 'Admin\OrderController@getOrderList');
    Route::get('/order-details/{id}', 'Admin\OrderController@orderDetails');
    Route::post('/update-shipping-status','Admin\OrderController@updateShippingStatus');
    Route::post('/update-payment-status','Admin\OrderController@updatePaymentStatus');
    Route::get('export-order', 'Admin\OrderController@exportOrder')->name('export.file');
    Route::post('/orders/update-tracking-info','Admin\OrderController@updateTrackingInfo');

    /*BOM*/
    Route::get('/boms', 'Admin\BomController@index');
    Route::get('/bomlist', 'Admin\BomController@getBomList');

    /*quotes*/
    Route::get('/quotes', 'Admin\QuoteController@index');
    Route::get('/quotes/{status}', 'Admin\QuoteController@index');
    Route::get('/quoteslist', 'Admin\QuoteController@getQuoteList');
    Route::get('/quote-details/{id}', 'Admin\QuoteController@quoteDetails');
    Route::post('/approve-quote-details/{id}', 'Admin\QuoteController@approveQuoteDetails');
    Route::get('/quote-reject/{id}', 'Admin\QuoteController@quoteReject');
    Route::get('/quote-reject-req-lead-time/{id}', 'Admin\QuoteController@quoteRejectReqLeadTime');
    /*End Quote Routes*/

    /* Requested Lead Times */
    Route::get('/requested-lead-times', 'Admin\RequestedProductController@index');
    Route::get('/requested-lead-times-list', 'Admin\RequestedProductController@requestedProductList');
    Route::get('/requested-lead-times-details/{id}', 'Admin\RequestedProductController@requestedProductDetails');
    /*End*/

    /*Net account request */
    Route::get('/net-account-requests', 'Admin\NetAccountController@index');
    Route::get('/net-account-requests-list', 'Admin\NetAccountController@getNetAccountRequestsList');
    Route::post('/update-request-status','Admin\NetAccountController@updateNetAccountRequest');
    Route::get('download-file/{id}/{file}','Admin\NetAccountController@downloadRequestFiles');

    /* Coupons Request */
    Route::resource('coupons', 'Admin\CouponsController');
    Route::get('couponlist', 'Admin\CouponsController@getCouponList');
    Route::post('/delete-coupon', 'Admin\CouponsController@deleteCoupon');
    Route::get('update-coupon-status', 'Admin\CouponsController@updateCouponStatus');


    Route::resource('roles', 'Admin\RolesController');
    Route::post('roles_mass_destroy', ['uses' => 'Admin\RolesController@massDestroy', 'as' => 'roles.mass_destroy']);
    Route::post('users_mass_destroy', ['uses' => 'Admin\UsersController@massDestroy', 'as' => 'users.mass_destroy']);
    Route::post('news_mass_destroy', ['uses' => 'Admin\NewsController@massDestroy', 'as' => 'news.mass_destroy']);
    Route::get('/check-newemails', 'Admin\UsersController@checkEmail');
});

//////////////////////////////////////////////////////////
///
///   END ADMIN  SITE ROUTES
///
/// //////////////////////////////////////////////////////
