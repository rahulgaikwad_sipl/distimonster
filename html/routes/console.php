<?php

use Illuminate\Foundation\Inspiring;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('quoteExpired', function () {
    $startTime = date('Y-m-d');
    $expiredDate = date('Y-m-d',strtotime('-20 days',strtotime($startTime)));
    $usersList = \App\Quote::join('users','users.id','=','quotes.user_id')
        ->where('approved_date', '=', $expiredDate)
        ->where('is_approved',1)
        ->get(['users.name','users.email','quotes.quote_name', 'quotes.approved_date','quotes.id']);
    if(count($usersList) > 0){
        foreach ($usersList as $user){
            \App\Quote::where('id', '=', $user->id)->update(['is_approved'=> 3]);
            $data['firstName']              = ucfirst($user->name);
            $data['quote_name']              = $user->quote_name;
            $data['approved_date']           = $user->approved_date;
            $data['content']                = 'Your quote '.$user->quote_name.' is expired. because it is approved 20 days before and till now you did not ordered it.';
            $subject                        = 'DistiMonster: Quote# ' .$user->quote_name . ' Expired';
            \Mail::to($user->email)->send(new \App\Mail\QuoteReminderMail($data, $subject));
        }

    }
})->describe('quote expired on 20th day of approval.');

Artisan::command('quoteApprovedReminder', function () {
    $startTime = date('Y-m-d');
    $sevenDayOld = date('Y-m-d',strtotime('-7 days',strtotime($startTime)));
    $seventeenDayOld = date('Y-m-d',strtotime('-17 days',strtotime($startTime)));
    $usersList = \App\Quote::join('users','users.id','=','quotes.user_id')
        ->where(function ($query) use($sevenDayOld, $seventeenDayOld) {
            $query->where('approved_date', '=', $sevenDayOld)
                ->orWhere('approved_date', '=', $seventeenDayOld);
        })->where('is_approved',1)
        ->get(['users.name','users.email','quotes.quote_name', 'quotes.approved_date']);
    if(count($usersList) > 0){
        foreach ($usersList as $user){
            $data['firstName']              = ucfirst($user->name);
            $data['quote_name']              = $user->quote_name;
            $data['approved_date']           = $user->approved_date;
            $data['content']  = 'Your quote '.$user->quote_name.' is approved on '.$user->approved_date.' and till now you did not ordered. Please order quote otherwise it will be expired.';
            $subject                        = 'DistiMonster: Quote# ' .$user->quote_name . ' Reminder';
            \Mail::to($user->email)->send(new \App\Mail\QuoteReminderMail($data, $subject));
        }

    }
})->describe('Send reminder on 7th and 17th day of quote approval');
