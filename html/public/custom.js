$(document).click(function(e) {
    if (!$(e.target).is('.panel-body')) {
        $('.collapse').collapse('hide');
    }
});

$("body").delegate("input[type=text].requested-delivery-date", "focusin", function(){
    var date = new Date();
    $(this).datepicker({
        todayHighlight: true,
        startDate: "0d",
      autoclose: true
  });
});

$(document).ready(function() {
    $('#requested_delivery_date').datepicker({
        startDate:"+1d",
        format: 'dd/mm/yyyy',
        todayHighlight:'TRUE',
        autoclose: true
    });
    /* Tooltip added */
    $('[data-toggle="tooltip"]').tooltip(); 
    /* Tooltip added */
    $('#search_part_number').submit(function() {
        $(".loader").css("display",'block');
    });
    //called when key is pressed in textbox
    $("#quantity").keypress(function(e) {
        /*if the letter is not digit then display error and don't type anything*/
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $('#paymentSection input[type=text]').on('keyup', function() {
        cardFormValidate();
    });
    window.Parsley.addValidator('imageFile', function (value, requirement) {
        var str_array = requirement.split(',');
        var fileExtension = value.split('.').pop();
        if(fileExtension === str_array[0]  || fileExtension === str_array[1]){
            return true;
        }else {
            return false
        }
    }, 32).addMessage('en', 'imageFile', 'Please upload only %s file.');

    window.Parsley.addValidator('fileextension', function (value, requirement) {
        var str_array = requirement.split('-');
        var fileExtension = value.split('.').pop();
        if(fileExtension === str_array[0]  || fileExtension === str_array[1]){
            return true;
        }else {
            return false
        }
    }, 32).addMessage('en', 'fileextension', 'Please upload only %s file.');

    window.Parsley.addValidator('pdfFile', function (value, requirement) {
        var fileExtension = value.split('.').pop();
        return fileExtension === requirement;

    }, 32).addMessage('en', 'pdfFile', 'Please upload only %s files.');


    window.Parsley.addValidator('maxFileSize', {
        validateString: function(_value, maxSize, parsleyInstance) {
            if (!window.FormData) {
                alert('You are making all developers in the world cringe. Upgrade your browser!');
                return true;
            }
            var files = parsleyInstance.$element[0].files;
            console.info(files);
            console.info(maxSize * 1024);
            return files.length != 1  || files[0].size/1024 <= maxSize * 1024;
        },
        requirementType: 'integer',
        messages: {
            en: 'This file should not be larger than %s MB'
        }
    });

    $('.dropdown.keep-inside-clicks-open .dropdown-menu').click(function(e){e.stopPropagation();});
});


var profileModule           = (function() {
    hideEmailPassword = function() {
        $('#email_pass_header').empty();
        if ($('#change_password').is(':checked') == true && $('#change_email').is(':checked') == true) {
            $('#email_pass_header').append('<p> Change Email and Password</p>');
        } else if ($('#change_email').is(':checked') == true) {
            $('#email_pass_header').append('<p> Change Email</p>');
        } else if ($('#change_password').is(':checked') == true) {
            $('#email_pass_header').append('<p> Change Password</p>');
        } else {
            $('#email_pass_header').append('<p></p>');
        }
    }
    return {
        displayPasswordSection: function() {
            if ($('#change_password').is(':checked') == true) {
                $('#password_section').css('display', 'block');
                $("#password").attr('data-parsley-required', true);
                $("#current_password").attr('data-parsley-required', true);
                $("#cpassword").attr('data-parsley-required', true);
                $("#password").attr('data-parsley-required-message','You can\'t leave this empty.');
                $("#current_password").attr('data-parsley-required-message','You can\'t leave this empty.');
                $("#cpassword").attr('data-parsley-required-message','You can\'t leave this empty.');
            } else {
                $('#password_section').css('display', 'none');
                $("#current_password").removeAttr('data-parsley-required');
                $("#password").removeAttr('data-parsley-required');
                $("#cpassword").removeAttr('data-parsley-required');
            }
            hideEmailPassword();
        },
        displayEmailSection: function() {
            if ($('#change_email').is(':checked') == true) {
                $('#email_section').css('display', 'block');
                $("#email").attr('data-parsley-required', true);
                $("#email").attr('data-parsley-required-message','You can\'t leave this empty.');
            } else {
                $('#email_section').css('display', 'none');
                $("#email").removeAttr('data-parsley-required');
            }
            hideEmailPassword();
        }
    };
})();
var cartModule              = (function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    return {
        /*This function used on product detail page*/
        increaseQtyOfSelectProduct: function(sourcePartId){
            var minQty    = $('#quantity_'+sourcePartId).attr('min');
            var newprice  = '';
            var qty       = $(".quantity").val();
            $(".quantity").val(parseInt(qty)+1);
            var newQuantityPresent = parseInt(qty)+1;
              $('.priceHolder_'+sourcePartId).filter(function () {
                if((parseInt(newQuantityPresent) >= parseInt($(this).attr('data-qtyHold'))) == true) {
                    //alert($(this).attr('data-priceHold'));
                    newprice = $(this).attr('data-priceHold');
                }
            });
              var completeTotalPrice = parseFloat(newprice) * parseInt($(".quantity").val());
              $('#selected-item-price').html('$' + completeTotalPrice);
          },
          decreaseQtyOfSelectProduct: function(sourcePartId) {
            var minQty      = $('#quantity_' + sourcePartId).attr('min');
            var newprice    = '';
            var qty         = $(".quantity").val();
            if (qty == minQty) {

            } else {
                $(".quantity").val(parseInt(qty) - 1);
                var newQuantityPresent = parseInt(qty) - 1;
                $('.priceHolder_' + sourcePartId).filter(function() {
                    if ((parseInt(newQuantityPresent) >= parseInt($(this).attr('data-qtyHold'))) == true) {
                        newprice = $(this).attr('data-priceHold');
                    }
                });
                var completeTotalPrice = parseFloat(newprice) * parseInt($(".quantity").val());
                $('#selected-item-price').html('$' + completeTotalPrice);
            }
        },
        onchangeQtyOfSelectProduct: function(sourcePartId) {
            var minQty = $('#quantity_' + sourcePartId).attr('min');
            var newprice = '';
            var newQuantityPresent = $(".quantity").val();
            if (newQuantityPresent < minQty) {} else {
                $('.priceHolder_' + sourcePartId).filter(function() {
                    if ((parseInt(newQuantityPresent) >= parseInt($(this).attr('data-qtyHold'))) == true) {
                        newprice = $(this).attr('data-priceHold');
                    }
                });
                var completeTotalPrice = parseFloat(newprice) * parseInt($(".quantity").val());
                $('#selected-item-price').html('$' + completeTotalPrice);
            }
        },
        /*This function are used on cart page only*/
        showEditPopup: function(id, row) {

            $("#popupQuantity").attr('data-parsley-min', row.options.resaleList[0].minQty);
            $("#popupQuantity").attr('data-parsley-max', row.options.availableData[0].fohQty);
            $("#popupItemSourcePartId").html(row.options.sourcePartId);
            var totalPrice = parseFloat(row.price) * parseInt(row.qty);
            $("#popupItemPrice").html("Price: $" + totalPrice.toFixed(2));
            $("#popupViewTiers").html($("#tierPrice_" + id).html());
            $("#popupAvailableItem").html(row.options.availableData[0].fohQty);

            $("#popupMinimumQty").html(row.options.resaleList[0].minQty);
            $("#popupQuantity").val(row.qty);
            $("#cartItems").val(JSON.stringify(row));
            $("#hiddenQtyId").val('quantity_' + id);
            $("#popupStockAvailability").val(row.options.stockAvailability);
            $("#popupCustomerPartNo").val(row.options.customerPartNo);
            $("#edititem").toggle();
            $('#edit_cart_form').parsley().reset();
        },
        closeEditItemModel: function() {
            $("#edititem").toggle();
        },
        increaseQtyOfPopupProduct: function() {
            var isValid = true;
            if ($('#popupQuantity').parsley().validate() !== true) isValid = false;
            if (isValid) {
                var qty = $("#popupQuantity").val();
                $("#popupQuantity").val(parseInt(qty) + 1);
            }

        },
        decreaseQtyOfPopupProduct: function() {
            var isValid = true;
            if ($('#popupQuantity').parsley().validate() !== true) isValid = false;
            if (isValid) {
                var qty = $("#popupQuantity").val();
                if (qty > 1) {
                    $("#popupQuantity").val(parseInt(qty) - 1);
                }
            }

        },
        editItemFromPopup: function() {
            var id                  = $("#hiddenQtyId").val();
            var stockAvailability   =  $("#popupStockAvailability").val();
            var row                 = JSON.parse($("#cartItems").val());
            var qty                 = $("#popupQuantity").val();
            $("#"+ id).val(qty);
            var form = $("#edit_cart_form");
            form.parsley().validate();
            if (form.parsley().isValid()) {
                var customerPartNo = $("#popupCustomerPartNo").val();
                cartModule.addToCart(id, row.id, row.options.manufacturer, row.options.manufacturerCode,row.options.sourcePartId,customerPartNo,stockAvailability);
            }
        },
        increaseQty: function(qtyInputId, id, manufacturer, manufacturerCode, sourcePartId, customerPartNo,stockAvailability) {
            var qty = $("#" + qtyInputId).val();
            $("#" + qtyInputId).val(parseInt(qty) + 1);
            cartModule.addToCart(qtyInputId, id, manufacturer, manufacturerCode, sourcePartId, customerPartNo,stockAvailability);
        },
        decreaseQty: function(qtyInputId, id, manufacturer, manufacturerCode,sourcePartId, customerPartNo,stockAvailability) {
            var qty = $("#" + qtyInputId).val();
            if (qty > 1) {
                $("#" + qtyInputId).val(parseInt(qty) - 1);
            }
            cartModule.addToCart(qtyInputId, id, manufacturer,manufacturerCode,sourcePartId,customerPartNo,stockAvailability);
        },
        addToCart: function(qtyInputId, id, manufacturer,manufacturerCode, sourcePartId, customerPartNo,stockAvailability) {
            var quantity = $("#" + qtyInputId).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var productID = id;
            var url = "/add-to-cart";
            $.ajax({
                type: "POST",
                url: BASE_URL + url,
                data: {
                    productId: productID,
                    manufacturer: manufacturer,
                    mfrCd:manufacturerCode,
                    sourcePartId: sourcePartId,
                    quantity: quantity,
                    customerPartNo: customerPartNo,
                    stockAvailability:stockAvailability
                },
                beforeSend: function() {
                    $(".loader").css("display",'block');
                },
                success: function(data) {
                    $(".loader").css("display",'none');
                    if (data.status) {
                        var currentUrl = window.location.href;
                        if (currentUrl.indexOf('cart') >= 0) {
                            window.location.href = "/cart";
                        } else {
                         window.location.href = "/cart";
                     }
                 } else {
                    alert(data.message);
                    if (data.minQtyError) {
                        $("#" + qtyInputId).val(data.minQty);
                    } else if (data.stockError) {
                        $("#" + qtyInputId).val(data.stock);
                    }
                }
            },
            error: function(data) {
                $(".loader").css("display",'none');
                console.log('Error:', data);
            }
        });
        },
        removeCartItem: function(id) {
            var productID = id;
            var url = "/remove-cart-item";
            $.ajax({
                type: "POST",
                url: BASE_URL + url,
                data: {
                    product_id: productID
                },
                beforeSend: function() {
                    $(".loader").css("display",'block');
                },
                success: function(data) {
                    $(".loader").css("display",'none');
                    location.reload();
                },
                error: function(data) {
                    $(".loader").css("display",'none');
                    console.log('Error:', data);
                }
            });
        },
        emptyCart: function() {
            var url = "/empty-cart";

            swal({
                    title: "Are you sure you want to clear cart?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            url: BASE_URL + url,
                            beforeSend: function () {
                                $(".loader").css("display", 'block');
                            },
                            success: function (data) {
                                $(".loader").css("display", 'none');
                                location.reload();
                                console.log(data);
                            },
                            error: function (data) {
                                $(".loader").css("display", 'none');
                                console.log('Error:', data);
                            }
                        });
                    }
                });
            },
    }
})();
var quoteModule             = (function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    return {
        /*This is used in details page*/
        addToQuoteOfSelectedProduct: function() {
            var qtyInputId          = $('#hiddenQtyInputId').val();
            var id                  = $('#hiddenId').val();
            var manufacturer        = $('#hiddenManufacturer').val();
            var sourcePartId        = $('#hiddenSourcePartId').val();
            var customerPartNo      = $('#customerPartNo').val();
            var stockAvailability   = $('#hiddenStockAvailability').val();
            quoteModule.addToQuote(qtyInputId, id, manufacturer, sourcePartId, customerPartNo, stockAvailability);
        },
        addToQuote: function(qtyInputId, id, manufacturer,mfrCd, sourcePartId, customerPartNo, stockAvailability,resaleListData,availableData,dateCode,requestedQuantity,requestedCost,requestedDeliveryDate) {
            var quantity = $("#" + qtyInputId).val();
            if(quantity == ""){
                quantity = 1
            }else{
                quantity  = quantity;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var productID = id;
            var url = "/add-to-quote";
            $.ajax({
                type: "POST",
                url: BASE_URL + url,
                data: {
                    productId: productID,
                    manufacturer: manufacturer,
                    mfrCd:mfrCd,
                    sourcePartId: sourcePartId,
                    quantity: requestedQuantity,
                    customerPartNo: customerPartNo,
                    stockAvailability: stockAvailability,
                    resaleListData:resaleListData,
                    availableData:availableData,
                    dateCode:dateCode,
                    requestedQuantity:requestedQuantity,
                    requestedCost:requestedCost,
                    requestedDeliveryDate:requestedDeliveryDate
                },
                beforeSend: function() {
                    $(".loader").css("display",'block');
                },
                success: function(data) {
                    $(".loader").css("display",'none');
                    if (data.status) {
                        var currentUrl = window.location.href;
                        if (currentUrl.indexOf('cart') >= 0) {
                            $("#quote-detail-modal").modal('hide');
                          window.location.href = "/quote-cart";
                      } else {
                            $("#quoteCount").html(data.itemCount);
                            $.notify(data.message, "success");
                            $("#quote-detail-modal").modal('hide');
                      }
                  } else {
                    alert(data.message);
                    if (data.minQtyError) {
                        $("#" + qtyInputId).val(data.minQty);
                    } else if (data.stockError) {
                        $("#" + qtyInputId).val(data.stock);
                    }
                }
            },
            error: function(data) {
                $(".loader").css("display",'none');
                $.notify(data, "error");
                $("#quote-detail-modal").modal('hide');
            }
        });
        },
        increaseQty: function(qtyInputId, id, manufacturer,mfrCd, sourcePartId, customerPartNo, stockAvailability,resaleListInputId,availableDataInputId,dateCode,requestedQuantity,requestedCost,requestedDeliveryDate) {
            console.log("quote page increase");
            var qty = $("#" + qtyInputId).val();
            $("#" + qtyInputId).val(parseInt(qty) + 1);
            var resaleListData    = $('#'+resaleListInputId).val();
            var availableData     = $('#'+ availableDataInputId).val();
            quoteModule.addToQuote(qtyInputId, id, manufacturer,mfrCd, sourcePartId, customerPartNo, stockAvailability,resaleListData,availableData,dateCode,requestedQuantity,requestedCost,requestedDeliveryDate);
        },
        decreaseQty: function(qtyInputId, id, manufactureaserer,mfrCd ,sourcePartId, customerPartNo, stockAvailability,resaleListInputId,availableDataInputId,dateCode,requestedQuantity,requestedCost,requestedDeliveryDate) {
            console.log("quote page decrease");
            var qty = $("#" + qtyInputId).val();
            if (qty > 1) {
                $("#" + qtyInputId).val(parseInt(qty) - 1);
            }
            var resaleListData    = $('#'+resaleListInputId).val();
            var availableData     = $('#'+ availableDataInputId).val();
            quoteModule.addToQuote(qtyInputId, id, manufacturer,mfrCd, sourcePartId, customerPartNo, stockAvailability,resaleListData,availableData,dateCode,requestedQuantity,requestedCost,requestedDeliveryDate);
        },
        changeQtyToQuote: function(qtyInputId, id, manufacturer,mfrCd ,sourcePartId, customerPartNo, stockAvailability,resaleListInputId,availableDataInputId,dateCode){
            var resaleListData    = $('#'+resaleListInputId).val();
            var availableData     = $('#'+ availableDataInputId).val();
            $('#add_quote_detail_form').parsley().reset();
            $("#quote-detail-modal").modal('show');
            var quoteItemObj =  {
                qtyInputId:qtyInputId,
                mpn:id,
                manufacturer:manufacturer,
                mfrCd:mfrCd,
                sourcePartId:sourcePartId,
                customerPartNo:  customerPartNo,
                stockAvailability:stockAvailability,
                resaleListData:resaleListData,
                availableData: availableData,
                dateCode:dateCode
            };
            $("#cartItems").val(JSON.stringify(quoteItemObj));
       },
        removeQuoteCartItem: function(id) {
        var productID = id;
        var url = "/remove-quote-cart-item";
        $.ajax({
            type: "POST",
            url: BASE_URL + url,
            data: {
                product_id: productID
            },
            beforeSend: function() {
                $(".loader").css("display",'block');
            },
            success: function(data) {
                $(".loader").css("display",'none');
                location.reload();
            },
            error: function(data) {
                $(".loader").css("display",'none');
                console.log('Error:', data);
            }
        });
    },
        showEditPopup: function(id, row) {
            $("#popupQuantity").attr('data-parsley-min', row.options.resaleList[0].minQty);
            $("#popupQuantity").attr('data-parsley-max', row.options.availableData[0].fohQty);

        $("#popupItemSourcePartId").html(row.options.sourcePartId);
        $("#popupItemPrice").html("Price: $" + parseFloat(row.price) * parseInt(row.qty));
        $("#popupViewTiers").html($("#tierPrice_" + id).html());
        $("#popupAvailableItem").html(row.options.availableData[0].fohQty);
        $("#popupMinimumQty").html(row.options.resaleList[0].minQty);
        $("#popupQuantity").val(row.qty);
        $("#cartItems").val(JSON.stringify(row));
        $("#hiddenQtyId").val('quantity_' + id);
        $("#popupCustomerPartNo").val(row.options.customerPartNo);
        $("#popupRequestedQuantityEdit").val(row.options.requestedQuantity);
        $("#popupRequestedCostEdit").val(row.options.requestedCost);
        $("#popupRequestedDeliveryDateEdit").val(row.options.requestedDeliveryDate);
        $("#edititem").toggle();
        $('#edit_quote_detail_form').parsley().reset();
    },
        emptyQuoteCart: function() {
        var url = "/empty-quote-cart";

            swal({
                    title: "Are you sure you want to clear quote list?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            url: BASE_URL + url,
                            beforeSend: function() {
                                $(".loader").css("display",'block');
                            },
                            success: function(data) {
                                $(".loader").css("display",'none');
                                location.reload();
                            },
                            error: function(data) {
                                $(".loader").css("display",'none');
                                $.notify("Sorry Something went wrong. Please try again later.", "error");
                            }
                        });
                    }
                });

    },
        editItemFromPopup: function() {
        var id                  = $("#hiddenQtyId").val();
        var stockAvailability   = $("#popupStockAvailability").val();
        var row                 = JSON.parse($("#cartItems").val());
        var qty                 = $("#popupQuantity").val();
        $("#" + id).val(qty);
        var customerPartNo                  = $("#popupCustomerPartNo").val();
        var popupRequestedQuantity          = $("#popupRequestedQuantityEdit").val();
        var popupRequestedCost              = $("#popupRequestedCostEdit").val();
        var popupRequestedDeliveryDate      = $("#popupRequestedDeliveryDateEdit").val();
        var form= $("#edit_quote_detail_form");
        form.parsley().validate();
        if (form.parsley().isValid()) {
            quoteModule.addToQuote(id, row.id, row.options.manufacturer,row.options.manufacturerCode, row.options.sourcePartId,customerPartNo,row.options.stockAvailability,JSON.stringify(row.options.resaleList),JSON.stringify(row.options.availableData),row.options.dateCode,popupRequestedQuantity,popupRequestedCost,popupRequestedDeliveryDate);
        }
    },
        removeQuote: function(quoteId,isListPage) {
        var url = "/remove-quote";
        swal({
            title: "Are you sure you want to delete this Quote?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function(isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "POST",
                    url: BASE_URL + url,
                    data: {
                        quoteId: quoteId,
                        isListPage:isListPage
                    },
                    beforeSend: function() {
                        $(".loader").css("display",'block');
                    },
                    success: function(response) {
                        $(".loader").css("display",'none');
                        if(response.status){
                            if(isListPage){
                               location.reload();
                            }else  {
                               window.location.href = "/saved-quotes";
                            }
                        }else{
                            $.notify(response.message, "error");
                            location.reload();
                        }
                    },
                    error: function(data) {
                        $.notify("Sorry Something went wrong. Please try again later.", "error");
                    }
                });
            }
        });
    },
        addToQuoteDetailFromModal: function() {
        var row                             = JSON.parse($("#cartItems").val());
        var popupRequestedQuantity          = $("#popupRequestedQuantity").val();
        var popupRequestedCost              = $("#popupRequestedCost").val();
        var popupRequestedDeliveryDate      = $("#popupRequestedDeliveryDate").val();
        var form = $("#add_quote_detail_form");
        form.parsley().validate();
        if (form.parsley().isValid()) {
            quoteModule.addToQuote(row.qtyInputId, row.mpn, row.manufacturer, row.mfrCd,row.sourcePartId, row.customerPartNo, row.stockAvailability,row.resaleListData,row.availableData,row.dateCode,popupRequestedQuantity,popupRequestedCost,popupRequestedDeliveryDate);
        }
    },
        closeAddToQuoteModel: function() {
        $("#popupRequestedQuantity").val('');
        $("#popupRequestedCost").val('');
        $("#popupRequestedDeliveryDate").val('');
        $("#quote-detail-modal").modal('hide');
    },
        addUnsavedQuoteToCart: function(qtyInputId, id, manufacturer,manufacturerCode, sourcePartId, customerPartNo,stockAvailability) {
            var quantity = $("#" + qtyInputId).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var productID = id;
            var url = "/add-unsaved-quote-to-cart";
            $.ajax({
                type: "POST",
                url: BASE_URL + url,
                data: {
                    productId: productID,
                    manufacturer: manufacturer,
                    mfrCd:manufacturerCode,
                    sourcePartId: sourcePartId,
                    quantity: quantity,
                    customerPartNo: customerPartNo,
                    stockAvailability:stockAvailability
                },
                beforeSend: function() {
                    $(".loader").css("display",'block');
                },
                success: function(data) {
                    $(".loader").css("display",'none');
                    if (data.status) {
                        var currentUrl = window.location.href;
                        if (currentUrl.indexOf('cart') >= 0) {
                            window.location.href = "/cart";
                        } else {
                            window.location.href = "/cart";
                        }
                    } else {
                        alert(data.message);
                        if (data.minQtyError) {
                            $("#" + qtyInputId).val(data.minQty);
                        } else if (data.stockError) {
                            $("#" + qtyInputId).val(data.stock);
                        }
                    }
                },
                error: function(data) {
                    $(".loader").css("display",'none');
                    console.log('Error:', data);
                }
            });
        },
}
})();
var accountSettingModule    = (function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    return {
        closeCommonModel: function (message) {
            $('#commonModalMessage').html(message);
            $("#commonModal").modal('hide');

        },
        openCommonModal: function (commonModalLabel,message){
            $('#commonModalLabel').html(commonModalLabel);
            $('#commonModalMessage').html(message);
            $("#commonModal").modal('show');
        },
        closeQtyFoundModel: function() {
            $("#qtyFoundModal").modal('hide');
            window.location.href = "/cart";
        },
        openQtyFoundModel: function(message) {
            $("#qtyFoundMessage").html(message);
            $("#qtyFoundModal").modal('show');
        },
        openOtherSourceModal:function (message) {
            $("#otherSourceMessage").html(message);
            $("#otherSourceModal").modal('show');
        },
        closeOtherSourceModal:function (message) {
            $("#otherSourceMessage").html(message);
            $("#otherSourceModal").modal('hide');
            location.reload();
        },
        showSimilarParts:function () {
            $(".loader").css("display",'block');
            $('#similar_parts_form').submit();
        },
        addToCartFromQuote : function (manufacturerPartNumber,sourcePartId,quantity,manufacturerName,manufacturerCode,customerPartNo) {
         var url = "/add-to-cart-from-quote";
         $.ajax({
             type: "POST",
             url: BASE_URL+url,
             data: {
                 productId: manufacturerPartNumber,
                 sourcePartId: sourcePartId,
                 manufacturer: manufacturerName,
                 manufacturerCode:manufacturerCode,
                 quantity: quantity,
                 customerPartNo: customerPartNo,
                 manufacturerName:manufacturerName
             },
             beforeSend: function() {
                 $(".loader").css("display",'block');
             },
             success: function(response) {
                 $(".loader").css("display",'none');
                 if (response.status) {
                     $.notify(response.message, "success");
                     $("#cartCount").html(response.cartItemCount);
                     $("#totalPrice").html(response.cartSubTotal);
              }else {
                 if(response.isOtherSource == 1){
                     $('#manufacturerName').val(manufacturerCode);
                     $('#partNumber').val(manufacturerPartNumber);
                     $('#sourcePartId').val(sourcePartId);
                     accountSettingModule.openOtherSourceModal(response.message);
                 }else{
                     var commonModalLabel =  'Error';
                     $.notify(response.message, "error");
                 }
             }
         },
         error: function(error) {
             $(".loader").css("display",'none');
             console.log('Error:', error);
         }
     });
     },
        addApprovedQuoteItemToCartFromQuote:function (manufacturerPartNumber,sourcePartId,manufacturerName,manufacturerCode,customerPartNo,dateCode,isApproved,approvedPrice,approvedQuantity,isQuoted,quoteItemId,quoteId) {
        var url = "/add-approved-quote-item-to-cart";
        $.ajax({
            type: "POST",
            url: BASE_URL+url,
            data: {
                productId: manufacturerPartNumber,
                sourcePartId: sourcePartId,
                manufacturer: manufacturerName,
                manufacturerCode:manufacturerCode,
                customerPartNo: customerPartNo,
                dateCode:dateCode,
                manufacturerName:manufacturerName,
                isApproved:isApproved,
                approvedPrice: approvedPrice,
                approvedQuantity: approvedQuantity,
                isQuoted:isQuoted,
                quoteItemId:quoteItemId,
                quoteId:quoteId
            },
            beforeSend: function() {
                $(".loader").css("display",'block');
            },
            success: function(response) {
                $(".loader").css("display",'none');
                if (response.status) {
                    window.location.href = "/cart";
                }
            },
            error: function(error) {
                $(".loader").css("display",'none');
                console.log('Error:', error);
            }
        });
    },
        confirmReorder: function() {
            var url = "/confirm-reorder-and-addtocart";
            swal({
                    title: "Are you sure you want to add available stock item in cart for reorder?",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type: "POST",
                            url: BASE_URL + url,
                            data:$('#orderedPartsForm').serialize(),
                            beforeSend: function() {
                                $(".loader").css("display",'block');
                            },
                            success: function(data) {
                                $(".loader").css("display",'none');
                                if (data.status) {
                                        window.location.href = "/cart";
                                }else {
                                    $.notify("Sorry Something went wrong. Please try again later.", "error");
                                }
                            },
                            error: function(data) {
                                $(".loader").css("display",'none');
                                $.notify("Sorry Something went wrong. Please try again later.", "error");
                            }
                        });
                    }
                });
        }
    }
})();
var checkOutModule          = (function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    getStates = function(countryID) {
        $("#city").html('<option value="">' + 'Select City*' + '</option>');

        var url = BASE_URL + '/state';
        if (countryID != '') {
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    country_id: countryID
                },
                beforeSend: function() {
                    $(".loader").css("display",'block');
                },
                success: function(data) {
                    $(".loader").css("display",'none');
                    if (data.success == true) {
                        var stateCount = data.data.length;
                        $("#state_id").append('<option value="">' + 'Select State*' + '</option>');
                        for (var i = 0; i < stateCount; i++) {
                            var stateId = data.data[i].id;
                            var stateName = data.data[i].name;
                            $("#state_id").append('<option value="' + stateId + '">' + stateName + '</option>');
                        }
                    }
                },
                error: function(data) {
                    $(".loader").css("display",'none');
                    console.log('Error:', data);
                }
            });
        }
    },
    getCity = function(cityId) {
        var url = BASE_URL + '/city';
        if (cityId != '') {
            $.ajax({
                type: "POST",
                url: url,
                data: {
                    cityId: cityId
                },
                beforeSend: function() {
                    $(".loader").css("display",'block');
                },
                success: function(data) {
                    $(".loader").css("display",'none');
                    if (data.success == true) {
                        var stateCount = data.data.length;
                        $("#city").append('<option value="">' + 'Select City*' + '</option>');
                        for (var i = 0; i < stateCount; i++) {
                            var stateId = data.data[i].id;
                            var stateName = data.data[i].name;
                            $("#city").append('<option value="' + stateId + '">' + stateName + '</option>');
                        }
                    }
                },
                error: function(data) {
                    $(".loader").css("display",'none');
                    console.log('Error:', data);
                }
            });
        }
    },
    cardFormValidate = function() {
        var cardValid = 0;
            //card number validation
            $('#card_number').validateCreditCard(function(result) {
                var cardType = (result.card_type == null) ? '' : result.card_type.name;
                console.log(result);
                if (cardType == 'Visa') {
                    var backPosition = result.valid ? '2px -163px, 260px -87px' : '2px -163px, 260px -61px';
                } else if (cardType == 'MasterCard') {
                    var backPosition = result.valid ? '2px -247px, 260px -87px' : '2px -247px, 260px -61px';
                } else if (cardType == 'Maestro') {
                    var backPosition = result.valid ? '2px -289px, 260px -87px' : '2px -289px, 260px -61px';
                } else if (cardType == 'Discover') {
                    var backPosition = result.valid ? '2px -331px, 260px -87px' : '2px -331px, 260px -61px';
                } else if (cardType == 'Amex') {
                    var backPosition = result.valid ? '2px -121px, 260px -87px' : '2px -121px, 260px -61px';
                } else {
                    var backPosition = result.valid ? '2px -121px, 260px -87px' : '2px -121px, 260px -61px';
                }
                $('#card_number').css("background-position", backPosition);
                if (result.valid) {
                    $("#card_type").val(cardType);
                    $("#card_number").removeClass('required');
                    cardValid = 1;
                } else {
                    $("#card_type").val('');
                    $("#card_number").addClass('required');
                    cardValid = 0;
                }
            });
            //card details validation
            var cardName = $("#name_on_card").val();
            var expMonth = $("#expiry_month").val();
            var expYear = $("#expiry_year").val();
            var cvv = $("#cvv").val();
            var regName = /^[a-z ,.'-]+$/i;
            var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
            var regYear = /^2017|2018|2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030|2031$/;
            var regCVV = /^[0-9]{3,3}$/;
            if (cardValid == 0) {
                $("#card_number").addClass('required');
                $("#card_number").focus();
                return false;
            } else if (!regMonth.test(expMonth)) {
                $("#card_number").removeClass('required');
                $("#expiry_month").addClass('required');
                $("#expiry_month").focus();
                return false;
            } else if (!regYear.test(expYear)) {
                $("#card_number").removeClass('required');
                $("#expiry_month").removeClass('required');
                $("#expiry_year").addClass('required');
                $("#expiry_year").focus();
                return false;
            } else if (!regCVV.test(cvv)) {
                $("#card_number").removeClass('required');
                $("#expiry_month").removeClass('required');
                $("#expiry_year").removeClass('required');
                $("#cvv").addClass('required');
                $("#cvv").focus();
                return false;
            } else if (!regName.test(cardName)) {
                $("#card_number").removeClass('required');
                $("#expiry_month").removeClass('required');
                $("#expiry_year").removeClass('required');
                $("#cvv").removeClass('required');
                $("#name_on_card").addClass('required');
                $("#name_on_card").focus();
                return false;
            } else {
                $("#card_number").removeClass('required');
                $("#expiry_month").removeClass('required');
                $("#expiry_year").removeClass('required');
                $("#cvv").removeClass('required');
                $("#name_on_card").removeClass('required');
                $('#cardSubmitBtn').prop('disabled', false);
                return true;
            }
        }
        return {
            changeCountry: function() {
                $("#state_id").empty();
                var countryID = $('#country_id').val();
                if (countryID != '') {
                    getStates(countryID);
                } else {
                    $("#state_id").empty();
                    $("#state_id").append('<option value="">' + 'Select State* ' + '</option>');
                    $("#city").empty();
                    $("#city").append('<option value="">' + 'Select City*' + '</option>');
                }
            },
            changeState: function() {
                $("#city").empty();
                var stateID = $('#state_id').val();
                if (stateID != '') {
                    getCity(stateID);
                } else {
                    $("#city").empty();
                    $("#city").append('<option value="">' + 'Select City*' + '</option>');
                }
            },
            selectShippingAddress: function(selectAddressBoxId, checkIconId, addressId) {
                $('.remove-selected-class').removeClass('list-check-active');
                $(".remove-check").hide();
                $("#" + selectAddressBoxId).toggleClass("list-check-active");
                $("#" + checkIconId).toggle();
                if ($("#" + selectAddressBoxId).hasClass("list-check-active")) {
                    $("#shipping-address").val(addressId);
                } else {
                    $("#shipping-address").val('');
                }
            },
            removeShippingAddress: function(shippingAddressId) {
                var url = "/remove-shipping-address";
                $.ajax({
                    type: "POST",
                    url: BASE_URL + url,
                    data: {
                        shippingAddressId: shippingAddressId
                    },
                    success: function(data) {
                        location.reload();
                        console.log(data);
                    },
                    error: function(data) {
                        console.log('Error:', data);
                    }
                });
            },
            selectBillingAddress:function () {
                if($('#billing_address').is(":checked")){
                    /*if checked then shipping and billing address will be same*/
                    $('#selected_shipping_address_div').show();
                    $("#billing_address_div").hide();
                    $('#billing_address').val(1);
                }
                else{
                    /*if unchecked then shipping and billing address will not be same
                    billing address is from user added billing address*/
                    $("#selected_shipping_address_div").hide();
                    $("#billing_address_div").show();
                    $('#billing_address').val(0);
                }
            },
            confirmBillingAddress:function (value,isEmptybillingAddress,userId) {
                    if($('.billing_address').is(":checked")){
                            if(value == 1){
                                 /*if checked then shipping and billing address will be same*/
                                 $('#selected_shipping_address_div').show();
                                 $("#billing_address_div").hide();
                                 $('#billing_address').val(1);
                            }else{
                                /*if unchecked then shipping and billing address will not be same
                                 billing address is from user added billing address*/
                                if(isEmptybillingAddress == '1'){
                                   $("#selected_shipping_address_div").hide();
                                   $("#billing_address_div").show();
                                   $('#billing_address').val(0);
                               }else{
                                $('#billing_address').val(0);
                                window.location = 'account-settings/'+userId;
                                }
                            }
                    }
            },
            goToPaymentReview : function() {
                $('#shipping_form').submit();
            },
            openApplyCoupon: function(){
                $('#code_message_success_div').html('');
                $('#code_message_error_div').html('');
                $("#apply_coupon").modal('show');
                $('#code_message_success_div').css('display' ,'none');
                $('#code_message_error_div').css('display' ,'none');
                $('#coupon_code').parsley().reset();
            },
            closeApplyCoupon: function() {
                $('#code_message_success_div').html('');
                $('#code_message_error_div').html('');
                $('#coupon_code').val('');
                $("#apply_coupon").modal('hide');

            },
            applyCouponCode:function () {
                var couponCode         = $("#coupon_code").val();
                var url = BASE_URL + '/apply-code';
                var isValid = true;
                if ($('#coupon_code').parsley().validate() !== true) isValid = false;
                if (isValid) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {
                            couponCode: couponCode
                        },
                        beforeSend: function() {
                            $(".loader").css("display",'block');
                        },
                        success: function(response) {
                            $(".loader").css("display",'none');
                            if(response.status){
                                $('#code_message_success_div').css('display' ,'block');
                                $('#code_message_error_div').css('display' ,'none');
                                $('#code_message_success_div').html(response.message);
                                if(response.data.is_free == 0){
                                    $('#discount_view').show();
                                    $('#discount_amount').html('$'+response.data.discount_amount.toFixed(2));
                                    $('#order_total').html('$'+response.data.cart_total);
                                    $('#coupon_code_id').val(response.data.id);
                                    $('#order_applied_discount').val(response.data.discount_amount);
                                    $('#coupon_type').val(response.data.is_free);
                                    $('#coupon_code').val('');
                                    $.notify(response.message, 'success');
                                    $("#apply_coupon").modal('hide');
                                    $('#apply_discount').hide();

                                }else{
                                    $('#discount_view').show();
                                    $('#discount_amount').html('Shipping Free');
                                    $('#order_total').html('$'+response.data.cart_total);
                                    $('#coupon_code_id').val(response.data.id);
                                    $('#order_applied_discount').val(response.data.discount_amount);
                                    $('#coupon_type').val(response.data.is_free);
                                    $('#coupon_code').val('');
                                    $.notify(response.message, 'success');
                                    $("#apply_coupon").modal('hide');
                                    $('#apply_discount').hide();
                                }
                            }else{
                                $('#code_message_success_div').css('display' ,'none');
                                $('#code_message_error_div').html('');
                                $('#code_message_error_div').css('display' ,'block');
                                $('#code_message_error_div').html(response.message);
                            }
                        },
                        error: function(data) {
                            $(".loader").css("display",'none');
                            $.notify(data, 'error');
                        }
                    });
                }
           },
           removeCouponCode: function () {
            var url = BASE_URL + '/remove-coupon-code';
            $.ajax({
                type: "POST",
                url: url,
                beforeSend: function() {
                    $(".loader").css("display",'block');
                },
                success: function(response) {
                    $(".loader").css("display",'none');
                    if(response.status){
                        $('#discount_amount').html('$'+response.data.discount_amount.toFixed(2));
                        $('#order_total').html('$'+response.data.order_total);
                        $('#coupon_code_id').val(response.data.id);
                        $('#order_applied_discount').val(response.data.discount_amount);
                        $('#coupon_type').val('');
                        $('#coupon_code_id').val('');
                        $('#order_applied_discount').val('');
                        $('#coupon_type').val('');
                        $('#discount_view').hide();
                        $('#apply_discount').show();
                        $.notify(response.message, 'success');
                    }

                },
                error: function(data) {
                    $(".loader").css("display",'none');
                    $.notify(data, 'error');
                }
            });
        },
        showNetAccountStatus:function(){
            $("#net_account_status").show();
            $("#paymentSection").hide();
            $("#paypalSection").hide();
            $("#cash_on_delivery").hide();
            $('input[name="payment_method"]').prop('checked', false);
        },
        showShippingMethodType:function(){
            if($("#shipping_method_type").is(":visible")){
                $("#shipping_method_type").hide();
                $('input[name="shipping_method_type_input"]').removeAttr('data-parsley-required');

            } else{
                $("#shipping_method_type").show();
                $('input[name="shipping_method_type_input"]').attr('data-parsley-required',true);
            }
        }
    }
})();
var bom     = (function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    return {
        removeBomItem: function(bomId) {
            var url = "/remove-bom";
            swal({
                title: "Are you sure you want to delete this bom",
                type: "info",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + url,
                        data: {
                            bomId: bomId
                        },
                        beforeSend: function() {
                            $(".loader").css("display",'block');
                        },
                        success: function(data) {
                            $(".loader").css("display",'none');
                            location.reload();
                        },
                        error: function(data) {
                            $(".loader").css("display",'none');
                            $.notify("Sorry Something went wrong. Please try again later.", "error");
                        }
                    });
                }
            });
        }
    }
})();
var productListModule       = (function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    return {
        clearSearchHistory: function() {
            var url = "/clear-search-history";
            $.ajax({
                type: "POST",
                url: BASE_URL + url,
                success: function(data) {
                    if (data==1) {
                        $('#recent_search_holder').html('No Recent Searches');
                        setTimeout(function(){ $( "#close_search_popup" ).trigger( "click" ); }, 1000);
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        },
        addToCartProductListing: function(stockQty,qtyInputId,mpn,manufacturer,mfrCd,sourcePartId,customerPartNoInputId,stockAvailability) {
            var id                  = mpn;
            var manufacturer        = manufacturer;
            var sourcePartId        = sourcePartId;
            var customerPartNo      = $('#'+customerPartNoInputId).val();
            var stockAvail          = stockAvailability;
            productListModule.addToCart(stockQty,qtyInputId, id, manufacturer,mfrCd, sourcePartId, customerPartNo,stockAvail);
        },
        addToQuoteProductListing:function (stockQty,qtyInputId,mpn,manufacturer,mfrCd,sourcePartId,customerPartNoInputId,stockAvailability,resaleListInputId,availableDataInputId,dateCode) {
            var customerPartNo    = $('#'+customerPartNoInputId).val();
            var resaleListData    = $('#'+resaleListInputId).val();
            var availableData     = $('#'+ availableDataInputId).val();
            var popupRequestedQuantity =   $("#"+qtyInputId).val();
            $("#quote-detail-modal").modal('show');
            $("#popupRequestedQuantity").val(popupRequestedQuantity);

            var quoteItemObj =  {
                qtyInputId:qtyInputId,
                mpn:mpn,
                manufacturer:manufacturer,
                mfrCd:mfrCd,
                sourcePartId:sourcePartId,
                customerPartNo:  customerPartNo,
                stockAvailability:stockAvailability,
                resaleListData:resaleListData,
                availableData: availableData,
                dateCode:dateCode
            };
            $("#cartItems").val(JSON.stringify(quoteItemObj));
     },
     selectProductList: function(itemPriceDivId, stockQty, stock, minQty, price, qtyInputId, id, manufacturer, sourcePartId) {
              /* Condition for displaying Selected text in the button */
              $(".select-buttons").removeClass("selected-button");
              $(".select-buttons").html("Select");
              $('#select_' + sourcePartId.replace(":", "_")).html('Selected');
              $('#select_' + sourcePartId.replace(":", "_")).addClass('selected-button');
              /* Condition for displaying Selected text in the button */
              /*Add border to selected box */
              $('.franchise-distributor-content').removeClass('list-check-active');
              $('#box_' + sourcePartId.replace(":", "_")).toggleClass('list-check-active');
              /*Add border to selected box */
              $('#'+qtyInputId).attr('min', minQty);
              /* Condition for Total Price */
              var totalPrice = (parseFloat(price)*minQty);
              console.log("totalPrice "+totalPrice+"  price "+price);
              $('#'+itemPriceDivId).html('$' + parseFloat(price).toFixed(3));
              /* Condition for Total Price */
              $('#'+qtyInputId).val(minQty);
          },
          increaseQtyOfSelectProduct: function(itemPriceDivId,qtyInputId,sourcePartId) {
            var newprice = '';
            var qty = $("#"+qtyInputId).val();
            var newQuantityPresent = parseInt(qty) + 1;
            $("#"+qtyInputId).val(newQuantityPresent);

            $('.priceHolder_' + sourcePartId.replace(":", "_")).filter(function() {
                if ((parseInt(newQuantityPresent) >= parseInt($(this).attr('data-qtyHold'))) == true) {
                    newprice = $(this).attr('data-priceHold');
                }
            });
            console.log(newprice);
            $('#'+itemPriceDivId).html('$' +newprice);
        },
        decreaseQtyOfSelectProduct: function(itemPriceDivId,qtyInputId,sourcePartId) {
                var newprice = '';
                var qty =  $("#"+qtyInputId).val();
                var newQuantityPresent = parseInt(qty) - 1;
                $("#"+qtyInputId).val(newQuantityPresent);

                $('.priceHolder_' + sourcePartId.replace(":", "_")).filter(function() {
                    if ((parseInt(newQuantityPresent) >= parseInt($(this).attr('data-qtyHold'))) == true) {
                        newprice = $(this).attr('data-priceHold');
                    }
                });
                $('#'+itemPriceDivId).html('$'+ newprice);
                },
            onchangeQtyOfSelectProduct: function(itemPriceDivId,qtyInputId,sourcePartId) {
                var newprice = '';
                var newQuantityPresent = $('#'+qtyInputId).val();
                $('.priceHolder_' + sourcePartId.replace(":", "_")).filter(function() {
                    if ((parseInt(newQuantityPresent) >= parseInt($(this).attr('data-qtyHold'))) == true) {
                        newprice = $(this).attr('data-priceHold');
                    }
                });

               $('#'+itemPriceDivId).html('$'+ newprice);
           },
           increaseQtyOfNoPriceProduct:function (itemPriceDivId,qtyInputId,sourcePartId) {
            var qty = $("#"+qtyInputId).val();
            var newQuantityPresent = parseInt(qty) + 1;
            $("#"+qtyInputId).val(newQuantityPresent);
        },
        decreaseQtyOfNoPriceProduct:function (itemPriceDivId,qtyInputId,sourcePartId) {
            var qty =  $("#"+qtyInputId).val();
            var newQuantityPresent = parseInt(qty) - 1;
            $("#"+qtyInputId).val(newQuantityPresent);
        },
        addToCart: function(stockQty,qtyInputId,id,manufacturer,mfrCd,sourcePartId,customerPartNo,stockAvailability) {
            var quantity                    = $("#"+qtyInputId).val();
            var sourceMinQty                = $('#source_min_qty_' + sourcePartId.replace(":", "_")).val();
            if(parseInt(quantity)< parseInt(sourceMinQty) ){
                alert('Minimum quantity is required.');
                return false;
            }
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var productID = id;
            var url = "/add-to-cart";
            $.ajax({
                type: "POST",
                url: BASE_URL + url,
                data: {
                    productId: productID,
                    manufacturer: manufacturer,
                    mfrCd:mfrCd,
                    sourcePartId: sourcePartId,
                    quantity: quantity,
                    customerPartNo: customerPartNo,
                    stockAvailability:stockAvailability
                },
                success: function(data) {
                    console.log(data);
                    if (data.status) {
                        var currentUrl = window.location.href;
                        if (currentUrl.indexOf('cart') >= 0) {
                            location.reload();
                        } else {
                            window.location.href = "/cart";
                        }
                    } else {
                        alert(data.message);
                        if (data.minQtyError) {
                            $("#" + qtyInputId).val(data.minQty);
                        } else if (data.stockError) {
                            $("#" + qtyInputId).val(data.stock);
                        }
                    }
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        },
    }
})();
var footer  = (function () {
    return{
        signUpNewsLetter: function () {
            var form = $("#newsletter_subscribe");
            form.parsley().validate();
            if (form.parsley().isValid()){
                var url = "/signup-newsletter";
                var emailNewsletter  =  $('#emailNewsletter').val();
                $.ajax({
                    type: "POST",
                    url: BASE_URL+url,
                    data: {
                        emailNewsletter: emailNewsletter,
                    },
                    beforeSend: function() {
                        $(".loader").css("display",'block');
                    },
                    success: function(response) {
                        $(".loader").css("display",'none');
                        if(response.status){
                            $('#emailNewsletter').val('');
                            $.notify(response.message, "success");
                        }else{
                            $.notify(response.message, "error");
                        }
                    },
                    error: function(error) {
                        $(".loader").css("display",'none');
                        $.notify(error, 'error');
                    }
                });
            }

        }
    }
})();


