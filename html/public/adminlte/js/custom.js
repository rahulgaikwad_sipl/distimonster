$(document).ready(function () {
    window.Parsley.addValidator('imageFile', function (value, requirement) {
        var str_array = requirement.split(',');
        var fileExtension = value.split('.').pop();
        if (fileExtension === str_array[0] || fileExtension === str_array[1] || fileExtension === str_array[2] || fileExtension === str_array[3] || fileExtension === str_array[4] || fileExtension === str_array[5] || fileExtension === str_array[6] || fileExtension === str_array[7]) {
            return true;
        } else {
            return false
        }
    }, 32).addMessage('en', 'imageFile', 'Please upload only %s file.');
    window.Parsley.addValidator('pdfFile', function (value, requirement) {
        var fileExtension = value.split('.').pop();
        return fileExtension === requirement;

    }, 32).addMessage('en', 'pdfFile', 'Please upload only %s file.');
    window.Parsley.addValidator('maxFileSize', {
        validateString: function (_value, maxSize, parsleyInstance) {
            if (!window.FormData) {
                alert('You are making all developers in the world cringe. Upgrade your browser!');
                return true;
            }
            var files = parsleyInstance.$element[0].files;
            console.info(files);
            console.info(maxSize * 1024);
            return files.length != 1 || files[0].size / 1024 <= maxSize * 1024;
        },
        requirementType: 'integer',
        messages: {
            en: 'This file should not be larger than %s MB'
        }
    });
    window.Parsley.addValidator('minFileSize', {
        validateString: function (_value, maxSize, parsleyInstance) {
            if (!window.FormData) {
                alert('You are making all developers in the world cringe. Upgrade your browser!');
                return true;
            }
            var files = parsleyInstance.$element[0].files;
            console.info(files[0].size / 1024);
            console.info(maxSize * 1024);
            return files.length != 1 || files[0].size / 1024 >= maxSize * 1024;
        },
        requirementType: 'integer',
        messages: {
            en: 'This file should not be smaller than %s MB'
        }
    });
    window.setTimeout(function () {
        $(".success-alert").slideUp(500);
    }, 4000);
});
var userModule = (function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var userDataTable;
    var objNewsDataTable;
    var deleteUser = function (userId) {
        var url = '/admin/delete-user';
        $.ajax({
            type: "POST",
            url: BASE_URL + url,
            data: { userId: userId, _token: $('#_token').val() },
            beforeSend: function () {
                $(".loader").css("display", 'block');
            },
            success: function (response) {
                $(".loader").css("display", 'none');
                if (response.success) {
                    $.notify(response.message, "success");
                    userDataTable.draw();
                } else {
                    $.notify(response.message, "error");
                    userDataTable.draw();
                }
            },
            error: function (data) {
                $(".loader").css("display", 'none');
                $.notify(data, "error");
            }
        });
    }
    var deleteNews = function (newsId) {
        var url = '/admin/delete-news';
        $.ajax({
            type: "POST",
            url: BASE_URL + url,
            data: { newsId: newsId, _token: $('#_token').val() },
            beforeSend: function () {
                $(".loader").css("display", 'block');
            },
            success: function (response) {
                $(".loader").css("display", 'none');
                if (response.success) {
                    $.notify(response.message, "success");
                    objNewsDataTable.draw();
                } else {
                    $.notify(response.message, "error");
                    objNewsDataTable.draw();
                }
            },
            error: function (data) {
                $(".loader").css("display", 'none');
                $.notify(data, "error");
            }
        });
    }
    return {
        loadUserDataTable: function () {
            var url = '/admin/userlist';
            userDataTable = $('#userTable').DataTable({
                responsive: true,
                processing: true,
                order: [[0, "asc"]],
                serverSide: true,
                language: {
                    "loadingRecords": 'Loading...',
                    "processing": '<img src= "' + BASE_URL + '/frontend/images/curved-bars.svg">',
                    "search": "Search"
                },
                ajax: {
                    url: BASE_URL + url,
                    //method: 'POST',
                    data: function (d) {
                        d.status_id = $("#status_id").val();
                        d.company_type = $("#company_type").val();
                        d.are_you_a = $("#are_you_a").val();
                        d.your_company_a = $("#your_company_a").val();
                        //d._token= "{{ csrf_token() }}";
                    }
                },
                columns: [
                    { data: 'name', name: 'name', className: "text-left" },
                    { data: 'email', name: 'email', className: "text-left" },
                    { data: 'contact_number', name: 'contact_number', className: "text-left" },
                    { data: 'location', name: 'cities.name', className: "text-left" },
                    { data: 'status', name: 'status', orderable: false, searchable: false, className: "text-center" },
                    { data: 'are_you_a', name: 'are_you_a', className: "text-center" },
                    { data: 'your_company_a', name: 'your_company_a', className: "text-center" },
                    { data: 'your_main_industry', name: 'your_main_industry', className: "text-center" },
                    { data: 'annual_component_purchase', name: 'annual_component_purchase', className: "text-center" },
                    { data: 'action', name: 'action', orderable: false, searchable: false, className: "text-center" },
                ]
            });
        },
        loadUserDataTableAgain: function () {
            userDataTable.draw();
        },
        loadNewsDataTable: function () {
            var url = '/admin/newslist';
            objNewsDataTable = $('#newsTable').DataTable({
                processing: true,
                order: [[2, "desc"]],
                serverSide: true,
                language: {
                    "loadingRecords": 'Loading...',
                    "processing": '<img src= "' + BASE_URL + '/frontend/images/curved-bars.svg">',
                    "search": "Search"
                },
                ajax: {
                    url: BASE_URL + url,
                    //method: 'POST',
                    data: function (d) {
                        d.status_id = $("#status_id").val();
                        //d._token= "{{ csrf_token() }}";
                    }
                },
                columns: [
                    { data: 'rownum', name: 'rownum', orderable: false, searchable: false },
                    { data: 'title', name: 'title', className: "text-left" },
                    { data: 'created_at', name: 'created_at', className: "text-left" },
                    { data: 'status', name: 'status', orderable: false, searchable: false, className: "text-center" },
                    { data: 'action', name: 'action', orderable: false, searchable: false, className: "text-center" },
                ]
            });
        },
        loadNewsDataTableAgain: function () {
            objNewsDataTable.draw();
        },
        changeNewsStatus: function (id, status, routeUrl) {
            var url = routeUrl;
            $.ajax({
                type: "get",
                url: BASE_URL + url,
                data: { id: id, status: status, _token: $('#_token').val() },
                beforeSend: function () {
                    $(".loader").css("display", 'block');
                },
                success: function (data) {
                    $(".loader").css("display", 'none');
                    if (data.success == true && data.status == 200) {
                        $.notify("Status Updated Successfully.", "success");
                        objNewsDataTable.draw();
                    }
                },
                error: function (data) {
                    $(".loader").css("display", 'none');
                    console.log('Error:', data);
                }
            });
        },
        changeUserStatus: function (id, status, routeUrl) {
            var url = routeUrl;
            $.ajax({
                type: "get",
                url: BASE_URL + url,
                data: { id: id, status: status, _token: $('#_token').val() },
                beforeSend: function () {
                    $(".loader").css("display", 'block');
                },
                success: function (data) {
                    $(".loader").css("display", 'none');
                    if (data.success == true && data.status == 200) {
                        $.notify("Status Updated Successfully.", "success");
                        userDataTable.draw();
                    }
                },
                error: function (data) {
                    $(".loader").css("display", 'none');
                    console.log('Error:', data);
                }
            });
        },
        confirmUserDelete: function (userId, msg) {
            swal({
                title: msg,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
                function (isConfirm) {
                    if (isConfirm) {
                        deleteUser(userId);
                    }
                });
        },
        confirmNewsDelete: function (newsId, msg) {
            swal({
                title: msg,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
                function (isConfirm) {
                    if (isConfirm) {
                        deleteNews(newsId);
                    }
                });
        }
    }
})();




var couponModule = (function () {
    var couponDataTable;
    var deleteCoupon = function (couponId) {
        var url = '/admin/delete-coupon';
        $.ajax({
            type: "POST",
            url: BASE_URL + url,
            data: { couponId: couponId, _token: $('#_token').val() },
            beforeSend: function () {
                $(".loader").css("display", 'block');
            },
            success: function (response) {
                $(".loader").css("display", 'none');
                if (response.success) {
                    $.notify(response.message, "success");
                    couponDataTable.draw();
                } else {
                    $.notify(response.message, "error");
                    couponDataTable.draw();
                }
            },
            error: function (data) {
                $(".loader").css("display", 'none');
                $.notify(data, "error");
            }
        });
    }

    return {
        confirmCouponDelete: function (couponId, msg) {
            swal({
                title: msg,
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
                function (isConfirm) {
                    if (isConfirm) {
                        deleteCoupon(couponId);
                    }
                });
        },
        loadCouponDataTable: function () {
            var url = '/admin/couponlist';
            couponDataTable = $('#couponTable').DataTable({
                processing: true,
                order: [[0, "desc"]],
                serverSide: true,
                language: {
                    "loadingRecords": 'Loading...',
                    "processing": '<img src= "' + BASE_URL + '/frontend/images/curved-bars.svg">',
                    "search": "Search"
                },
                ajax: {
                    url: BASE_URL + url,
                    //method: 'POST',
                    data: function (d) {
                        d.status_id = $("#status_id").val();
                        //d._token= "{{ csrf_token() }}";
                    }
                },
                columns: [
                    { data: 'id', name: 'id', className: "text-left", visible: false },
                    { data: 'code', name: 'code', className: "text-left" },
                    { data: 'discount_percentage', name: 'discount_percentage', className: "text-left" },
                    { data: 'order_limit', name: 'order_limit', className: "text-left" },
                    { data: 'status', name: 'status', orderable: false, searchable: false, className: "text-center" },
                    { data: 'action', name: 'action', orderable: false, searchable: false, className: "text-center" },
                ]
            });
        },
        loadCouponDataTableAgain: function () {
            couponDataTable.draw();
        },
        changeCouponStatus: function (id, status, routeUrl) {
            var url = routeUrl;
            $.ajax({
                type: "get",
                url: BASE_URL + url,
                data: { id: id, status: status, _token: $('#_token').val() },
                beforeSend: function () {
                    $(".loader").css("display", 'block');
                },
                success: function (data) {
                    $(".loader").css("display", 'none');
                    if (data.success == true && data.status == 200) {
                        $.notify("Status Updated Successfully.", "success");
                        couponDataTable.draw();
                    }
                },
                error: function (data) {
                    $(".loader").css("display", 'none');
                    $.notify(data, "error");
                }
            });
        },
    }
})();

/*var requestedProduct = (function () {
    var objReqProductTable;
    return {
        loadReqProductTable: function () {
            var url = '/admin/requested-product-list';
            objReqProductTable = $('#requestedproduct').DataTable({
                processing: true,
                order: [[8, "desc"]],
                serverSide: true,
                language: {
                    "loadingRecords": 'Loading...',
                    "processing": '<img src= "' + BASE_URL + '/frontend/images/curved-bars.svg">',
                    "search": "Search"
                },
                ajax: {
                    url: BASE_URL + url,
                    //method: 'POST',
                },
                columns: [
                    { data: 'customer_name', name: 'customer_name', className: "text-left" },
                    { data: 'customer_email', name: 'customer_email', className: "text-left" },
                    { data: 'customer_phone', name: 'customer_phone', className: "text-left" },
                    { data: 'mpn', name: 'mpn', className: "text-center" },
                    { data: 'category', name: 'category', className: "text-left" },
                    { data: 'exp_quantity', name: 'exp_quantity', orderable: false, searchable: false, className: "text-center" },
                    { data: 'exp_delivery_date', name: 'exp_delivery_date', orderable: false, searchable: false, className: "text-center" },
                    { data: 'exp_unit_price', name: 'exp_unit_price', orderable: false, searchable: false, className: "text-center" },
                    { data: 'created_at', name: 'created_at', orderable: false, searchable: false, className: "text-center" },
                    { data: 'action', name: 'action', orderable: false, searchable: false, className: "text-center" }
                ]
            });
        }
    }
})();*/

var requestedProduct = (function () {
    var objReqProductTable;
    return {
        loadReqProductTable: function () {
            var url = '/admin/requested-lead-times-list';
            objReqProductTable = $('#requestedproduct').DataTable({
                processing: true,
                order: [[0, "desc"]],
                serverSide: true,
                language: {
                    "loadingRecords": 'Loading...',
                    "processing": '<img src= "' + BASE_URL + '/frontend/images/curved-bars.svg">',
                    "search": "Search"
                },
                ajax: {
                    url: BASE_URL + url,
                    //method: 'POST',
                    data: function (d) {
                        d.status_id = $("#status_id").val();
                        //d._token= "{{ csrf_token() }}";
                    }
                },
                columns: [
                    { data: 'id', name: 'id', className: "text-left" },
                    { data: 'quote_name', name: 'quote_name', className: "text-left" },
                    { data: 'firstname', name: 'users.name', className: "text-left" },
                    { data: 'part_counts', name: 'part_counts', orderable: false, searchable: false, className: "text-center" },
                    { data: 'created_at', name: 'created_at', className: "text-left" },
                    { data: 'is_approved', name: 'is_approved', className: "text-left" },
                    { data: 'action', name: 'action', orderable: false, searchable: false, className: "text-center" }
                ]
            });
        },
        requestedProductAgain: function () {
            objReqProductTable.draw();
        }
    }
})();

var quouteModule = (function () {
    var objQuoteDataTable;
    return {
        loadQuoutesDataTable: function () {
            var url = '/admin/quoteslist';
            objQuoteDataTable = $('#quotestable').DataTable({
                processing: true,
                order: [[0, "desc"]],
                serverSide: true,
                language: {
                    "loadingRecords": 'Loading...',
                    "processing": '<img src= "' + BASE_URL + '/frontend/images/curved-bars.svg">',
                    "search": "Search"
                },
                ajax: {
                    url: BASE_URL + url,
                    //method: 'POST',
                    data: function (d) {
                        d.status_id = $("#status_id").val();
                        //d._token= "{{ csrf_token() }}";
                    }
                },
                columns: [
                    { data: 'id', name: 'id', className: "text-left" },
                    { data: 'quote_name', name: 'quote_name', className: "text-left" },
                    { data: 'firstname', name: 'users.name', className: "text-left" },
                    { data: 'part_counts', name: 'part_counts', orderable: false, searchable: false, className: "text-center" },
                    { data: 'created_at', name: 'created_at', className: "text-left" },
                    { data: 'is_approved', name: 'is_approved', className: "text-left" },
                    { data: 'action', name: 'action', orderable: false, searchable: false, className: "text-center" }
                ]
            });
        },
        loadQuoutesDataTableAgain: function () {
            objQuoteDataTable.draw();
        }
    }
})();
var bomModule = (function () {
    var objBomDataTable;

    return {
        loadBomDataTable: function () {
            var url = '/admin/bomlist';
            objBomDataTable = $('#bomtable').DataTable({
                processing: true,
                order: [[0, "asc"]],
                serverSide: true,
                ajax: BASE_URL + url,
                columns: [
                    { data: 'id', name: 'id', className: "text-left" },
                    { data: 'full_name', name: 'full_name', className: "text-left" },
                    { data: 'company_name', name: 'company_name', orderable: false, searchable: false, className: "text-center" },
                    { data: 'email', name: 'email', className: "text-left" },
                    { data: 'contact_no', name: 'contact_no', className: "text-left" },
                    { data: 'address', name: 'address', className: "text-left" },
                ]
            });
        },

    }
})();
var netTermRequestModule = (function () {
    var objNetTermRequestDataTable;
    return {
        loadNetTermRequestDataTable: function () {
            var url = '/admin/net-account-requests-list';
            objNetTermRequestDataTable = $('#net_term_request_table').DataTable({
                processing: true,
                order: [[2, "desc"]],
                serverSide: true,
                ajax: {
                    url: BASE_URL + url,
                    //method: 'POST',
                    data: function (d) {
                        d.status_id = $("#status_id").val();
                        //d._token= "{{ csrf_token() }}";
                    }
                },
                language: {
                    "loadingRecords": 'Loading...',
                    "processing": '<img src= "' + BASE_URL + '/frontend/images/curved-bars.svg">',
                    "search": "Search"
                },
                columns: [
                    { data: 'name', name: 'users.name', className: "text-left" },
                    { data: 'requested_account_type', name: 'requested_account_type', orderable: false, searchable: false, className: "text-center" },
                    { data: 'applied_on_date', name: 'applied_on_date', className: "text-left" },
                    { data: 'status', name: 'status', orderable: false, searchable: false, className: "text-center" },
                    { data: 'document', name: 'document', orderable: false, searchable: false, className: "text-center" }
                ]
            });
        },
        loadNetTermRequestDataTableAgain: function () {
            objNetTermRequestDataTable.draw();
        },
        changeRequestStatus: function (currentStatus, requestId, accountType) {
            $("#requestId").val(requestId);
            $("#requestStatus").val(currentStatus);
            $("#accountType").val(accountType);

            /*var indexVal = document.getElementById("requestStatus").length;
            for(var x=0; x<indexVal;x++) {
                if (document.getElementById("requestStatus").options[x].value == currentStatus) {
                    document.getElementById("requestStatus").options[x].selected = "selected";
                }
            }*/
            $('#update-request-status').modal('show');
        },
        updateRequestStatus: function (event) {
            var updateRequestStatusForm = $('#updateRequestStatusForm');
            updateRequestStatusForm.parsley().validate();
            if (updateRequestStatusForm.parsley().isValid()) {
                var url = BASE_URL + '/admin/update-request-status';
                var postData = {
                    requestStatus: $("#requestStatus").val(),
                    accountType: $("#accountType").val(),
                    requestId: $("#requestId").val(),
                    _token: $('#_token').val()
                };

                $.ajax({
                    type: "POST",
                    url: url,
                    data: postData,
                    beforeSend: function () {
                        $(".loader").css("display", 'block');
                    },
                    success: function (response) {
                        $(".loader").css("display", 'none');
                        if (response.success == true) {
                            $('#update-request-status').modal('hide');
                            $.notify(response.message, "success");
                            objNetTermRequestDataTable.draw();
                        } else {
                            $.notify(response.message, "error");
                            $('#update-request-status').modal('hide');
                            objNetTermRequestDataTable.draw();
                        }
                    },
                    error: function (error) {
                        $(".loader").css("display", 'none');
                        $.notify(response.message, "error");
                        $('#update-request-status').modal('hide');
                        objNetTermRequestDataTable.draw();
                    }
                });
            } else {

            }
        }
    }
})();
var orderModule = (function () {
    var objOrderDataTable;
    return {
        loadOrderDataTable: function () {
            var url = '/admin/orderlist';
            objOrderDataTable = $('#orderTable').DataTable({
                processing: true,
                order: [[0, "desc"]],
                serverSide: true,
                language: {
                    "loadingRecords": 'Loading...',
                    "processing": '<img src= "' + BASE_URL + '/frontend/images/curved-bars.svg">',
                    "search": "Search"
                },
                ajax: {
                    url: BASE_URL + url,
                    //method: 'POST',
                    data: function (d) {
                        d.shipment_status_id = $("#shipment_status_id").val();
                        d.payment_status_id = $("#payment_status_id").val();
                        //d._token= "{{ csrf_token() }}";
                    }
                },
                columns: [
                    { data: 'id', name: 'id', className: "text-left" },
                    { data: 'name', name: 'users.name', orderable: false, searchable: true, className: "text-left" },
                    { data: 'order_date', name: 'order_date', orderable: false, searchable: true, className: "text-center" },
                    { data: 'order_total_amount', name: 'order_total_amount', className: "text-left" },
                    { data: 'shipping_status', name: 'status', className: "text-center", orderable: false, searchable: false },
                    { data: 'payment_status', name: 'payment_status', className: "text-center", orderable: false, searchable: false },
                    { data: 'action', name: 'action', orderable: false, searchable: false, className: "text-center" },
                ]
            });
        },
        loadOrderDataTableAgain: function () {
            objOrderDataTable.draw();
        },
        changeShippingStatus: function (currentShippingStatus, orderId, trackingInfo) {
            $("#orderId").val(orderId);
            var indexVal = document.getElementById("shippingStatus").length;
            for (var x = 0; x < indexVal; x++) {
                document.getElementById("trackingInfo").value = "";
                if (document.getElementById("shippingStatus").options[x].value == currentShippingStatus) {
                    document.getElementById("shippingStatus").options[x].selected = "selected";
                }
                if (trackingInfo != '') {
                    document.getElementById("trackingInfo").value = trackingInfo;
                }
            }
            $('#update-shipping-status').modal('show');
        },
        updateShippingStatus: function (event) {
            var updateShippingStatusForm = $('#updateShippingStatusForm');
            updateShippingStatusForm.parsley().validate();
            if (updateShippingStatusForm.parsley().isValid()) {
                var url = BASE_URL + '/admin/update-shipping-status';
                var postData = {
                    shippingStatus: $("#shippingStatus").val(),
                    trackingInfo: $("#trackingInfo").val(),
                    orderId: $("#orderId").val(),
                    _token: $('#_token').val()
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: postData,
                    beforeSend: function () {
                        $(".loader").css("display", 'block');
                    },
                    success: function (response) {
                        $(".loader").css("display", 'none');
                        if (response.success == true) {
                            $('#update-shipping-status').modal('hide');
                            $.notify(response.message, "success");
                            objOrderDataTable.draw();
                        } else {
                            $.notify(response.message, "error");
                            $('#update-shipping-status').modal('hide');
                            objOrderDataTable.draw();
                        }
                    },
                    error: function (error) {
                        $(".loader").css("display", 'none');
                        $.notify(error, "error");
                        $('#update-shipping-status').modal('hide');
                        objOrderDataTable.draw();
                    }
                });
            } else {

            }
        },
        changePaymentStatus: function (currentPaymentStatus, orderId) {
            $("#orderId").val(orderId);
            var indexVal = document.getElementById("paymentStatus").length;
            for (var x = 0; x < indexVal; x++) {
                if (document.getElementById("paymentStatus").options[x].value == currentPaymentStatus) {
                    document.getElementById("paymentStatus").options[x].selected = "selected";
                }
            }
            $('#update-payment-status').modal('show');
        },
        updatePaymentStatus: function (event) {
            var updatePaymentStatusForm = $('#updatePaymentStatusForm');
            updatePaymentStatusForm.parsley().validate();
            if (updatePaymentStatusForm.parsley().isValid()) {
                var url = BASE_URL + '/admin/update-payment-status';
                var postData = {
                    paymentStatus: $("#paymentStatus").val(),
                    orderId: $("#orderId").val(),
                    _token: $('#_token').val()
                };
                $.ajax({
                    type: "POST",
                    url: url,
                    data: postData,
                    beforeSend: function () {
                        $(".loader").css("display", 'block');
                    },
                    success: function (response) {
                        $(".loader").css("display", 'none');
                        if (response.success == true) {
                            $('#update-payment-status').modal('hide');
                            $.notify(response.message, "success");
                            objOrderDataTable.draw();
                        } else {
                            $.notify(response.message, "error");
                            $('#update-payment-status').modal('hide');
                            objOrderDataTable.draw();
                        }
                    },
                    error: function (error) {
                        $(".loader").css("display", 'none');
                        $.notify(error, "error");
                        $('#update-payment-status').modal('hide');
                        objOrderDataTable.draw();
                    }
                });
            } else {

            }
        }
    }
})();
$.extend(true, $.fn.dataTable.defaults, {
    oLanguage: {
        sSearch: "Search",
        //sProcessing: "<img src='"+BASE_URL+"images/loading.gif'>"
    },
    lengthMenu: [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
    fnDrawCallback: function () {
        $('.toggle-switch').bootstrapToggle();
    },
});
/* Coupons Management Section */
$(".is_free_shipping").change(function () {
    if (this.checked) {
        $("#discount_percentage").attr("disabled", true);
        $("#discount_percentage").val('');
        $("#discount_percentage").attr("required", false);
    }
    else {
        $("#discount_percentage").attr("disabled", false);
        $("#discount_percentage").attr("required", true);

    }
});
$('#start_date').datepicker({
    startDate: '1d',
    format: 'mm/dd/yyyy',
    autoclose: true,
}).on('changeDate', function (selected) {
    var minDate = new Date(selected.date.valueOf());
    $('#end_date').datepicker('setStartDate', minDate);
    $(this).parsley().validate();
});

$('#end_date').datepicker({
    startDate: '1d',
    format: 'mm/dd/yyyy',
    autoclose: true,
}).on('changeDate', function (ev) {
    if ($('#start_date').val() == '') {
        alert('Please Select Start Date First.');
        $('#end_date').val('');
    }
    $(this).parsley().validate();
});

/* To show date picker for date of shipping field for Tracking Information | by @@@R on 17-05-2019 */
$('.date_of_shipping').datepicker({
    startDate: '1d',
    format: 'mm/dd/yyyy',
    autoclose: true,
});

/* To hide the status error from the admin user create form | by @@@R on 27-05-2019 */
$(document).on('change', '.status', function () {
    $('#error_status').html('');
});

/* Coupons Management Section */
