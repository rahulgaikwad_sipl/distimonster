$(document).ready(function() {
    $(window).scroll(function() {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function() {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });
    $('#back-to-top').tooltip('show');
    // This button will increment the value
    $('.qtyplus').click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name=' + fieldName + ']').val());
        // If is not undefined
        if (!isNaN(currentVal)) {
            // Increment only if value is < 20
            if (currentVal < 20) {
                $('input[name=' + fieldName + ']').val(currentVal + 1);
                $('.qtyminus').val("-").removeAttr('style');
            } else {
                $('.qtyplus').val("+").css('color', '#aaa');
                $('.qtyplus').val("+").css('cursor', 'not-allowed');
            }
        } else {
            // Otherwise put a 0 there
            $('input[name=' + fieldName + ']').val(1);

        }
    });
    // This button will decrement the value till 0
    $(".qtyminus").click(function(e) {
        // Stop acting like a button
        e.preventDefault();
        // Get the field name
        fieldName = $(this).attr('field');
        // Get its current value
        var currentVal = parseInt($('input[name=' + fieldName + ']').val());
        // If it isn't undefined or its greater than 0
        if (!isNaN(currentVal) && currentVal > 1) {
            // Decrement one only if value is > 1
            $('input[name=' + fieldName + ']').val(currentVal - 1);
            $('.qtyplus').val("+").removeAttr('style');
        } else {
            // Otherwise put a 0 there
            $('input[name=' + fieldName + ']').val(1);
            $('.qtyminus').val("-").css('color', '#aaa');
            $('.qtyminus').val("-").css('cursor', 'not-allowed');
        }
    });

    $('ul.tabs li').click(function(e) {
        var txt = $(e.target).text();
        console.log(txt);
        $('.breadcrumb span').text(txt);
        //$('.breadcrumb span').show('txt')
    });
});



// tabbed content
// http://www.entheosweb.com/tutorials/css/tabs.asp
$(".tab_content").hide();
$(".tab_content:first").show();

/* if in tab mode */
$("ul.tabs li").click(function() {

    $(".tab_content").hide();
    var activeTab = $(this).attr("rel");
    $("#" + activeTab).fadeIn();

    $("ul.tabs li").removeClass("active");
    $(this).addClass("active");

    $(".tab_drawer_heading").removeClass("d_active");
    $(".tab_drawer_heading[rel^='" + activeTab + "']").addClass("d_active");

    /*$(".tabs").css("margin-top", function(){ 
       return ($(".tab_container").outerHeight() - $(".tabs").outerHeight() ) / 2;
    });*/
});
$(".tab_container").css("min-height", function() {
    return $(".tabs").outerHeight() + 50;
});
/* if in drawer mode */
$(".tab_drawer_heading").click(function() {

    $(".tab_content").hide();
    var d_activeTab = $(this).attr("rel");
    $("#" + d_activeTab).fadeIn();

    $(".tab_drawer_heading").removeClass("d_active");
    $(this).addClass("d_active");

    $("ul.tabs li").removeClass("active");
    $("ul.tabs li[rel^='" + d_activeTab + "']").addClass("active");
});
/* Extra class "tab_last" 
   to add border to bottom side
   of last tab 
$('ul.tabs li').last().addClass("tab_last");*/
// $(function() {
//     $('#afs-accordion .content').hide();
//      //$('#afs-accordion .test').addClass('active').next().slideDown('slow');
//     $('#afs-accordion a').click(function() {
//         $expandtext = $(this);
//         $content = $expandtext.next();
//         $(this).next().slideUp('slow');
//         if ($(this).next().is(':hidden')) {
//             console.log('SHOW')
//             $('#afs-accordion .test').removeClass('active').next().slideUp('slow');
//             $(this).toggleClass('active').next().slideDown('slow');
//         }else{
//         console.log('HIDE')
//             return $content.is(":visible") ? "Hide Detail" : "View Detail";
//         }
//     });
// });


$("#afs-accordion .test").click(function() {
    //alert('hello');

    $expandtext = $(this);

    console.log($(this));
    //getting the next element
    $content = $expandtext.next();
    //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
    $content.slideToggle(500, function() {
        //execute this after slideToggle is done
        //change text of expand-text based on visibility of content div
        $expandtext.text(function() {
            //change text based on condition
            return $content.is(":visible") ? "Hide Detail" : "View Detail";
        });
    });



});

  /*New Detail Page*/
  $(".expandtext").click(function() {
      //alert('hello');
      $expandtext = $(this);
      //getting the next element
      $content = $expandtext.next();
      //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
      $content.slideToggle(500, function() {
          //execute this after slideToggle is done
          //change text of expand-text based on visibility of content div
          $expandtext.text(function() {
              //change text based on condition
              return $content.is(":visible") ? "Collapse" : "Expand";
          });
      });

  });




