function showOther(val) {
    if (val == "Other") {
        $(".company_type_hidden").show();
    } else {
        $(".company_type_hidden").hide();
    }
}

function isEmail(email) {
    return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(email);
}

//Wizard
$(document).ready(function () {
    $(".round-txt").click(function () {
        var $active = $('.wizard .nav-tabs .nav-item .active');
        $(".nav-link").removeClass("active");
        $(".tab-pane").removeClass("active");
        var $activeli = $active.parent("li");
        $($activeli).prev().find('a[data-toggle="tab"]').addClass('active');
        $($activeli).prev().find('a[data-toggle="tab"]').attr('aria-expanded', true);
        $($($activeli).prev().find('a[data-toggle="tab"]').attr('href')).addClass('active');
        $($($activeli).prev().find('a[data-toggle="tab"]').attr('href')).attr('aria-expanded', true);
        $($activeli).prev().find('a[href="javascript:void(0);"]').show();
        $($activeli).find('a[href="javascript:void(0);"]').hide();
    });

    $(".nav-link").click(function () {
        var $active = $('.wizard .nav-tabs .nav-item .active');
        var $activeli = $active.parent("li");
        if (!$(this).hasClass('disabled')) {
            $(".back-button").hide();
            $(this).next().show();
        }
    });

    var $sections = $('.tab-pane');
    function curIndex() {
        // Return the current index by looking at which section has the class 'current'
        return $sections.index($sections.filter('.active'));
    }
    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
        var $target = $(e.target);
        if ($target.hasClass('disabled')) {
            return false;
        }
    });
    $(document).on('click', '.next-step', function (e) {

        $('.msfrm').parsley().whenValidate({
            group: 'block-' + curIndex()
        }).done(function (e) {
            // Validate all input fields.
            var isValid = true;
            var emailIncrementk = 1;
            var totalFieldk = $("#czContainer_czMore_txtCount").val();
            $('.msfrm .dynamic_frm').each(function () {

                var thisElement = $(this);
                if ((thisElement.parsley().validate() !== true)) {
                    isValid = false;
                }

                if ((thisElement.parsley().validate() !== null) && isEmail(thisElement.val())) {
                    isValid = false;
                    var eml = thisElement.val();
                    var ur = BASE_URL + '/check-newemail';
                    $.ajax({
                        type: "GET",
                        url: ur,
                        data: {
                            email: eml
                        },
                        beforeSend: function () {
                            $(".loader").css("display", 'block');
                        },
                        success: function (response) {
                            $(".loader").css("display", 'none');
                            thisElement.next('ul').hide();
                            if (totalFieldk == emailIncrementk) {
                                var $active = $('.wizard .nav-tabs .nav-item .active');
                                var $activeli = $active.parent("li");
                                $($activeli).next().find('a[data-toggle="tab"]').removeClass("disabled");
                                $($activeli).prev().find('a[href="javascript:void(0);"]').hide();
                                $($activeli).next().find('a[href="javascript:void(0);"]').show();
                                $($activeli).next().find('a[data-toggle="tab"]').click();

                            }
                            emailIncrementk++;
                        },
                        error: function (response) {
                            $(".loader").css("display", 'none');
                            //                            $(".customexist").show();
                            console.log(thisElement);
                            console.log(thisElement.next('ul').html());
                            thisElement.next('ul').show();
                            console.log('Error:', response);
                            return false;
                            setTimeout(function () {
                                thisElement.next('ul').hide();
                                return false;
                            }, 5000);

                        }
                    });
                }
            });
            if (isValid == false) {
                return false;
            } else {
                console.log("OK and Processed!");
            }

            var $active = $('.wizard .nav-tabs .nav-item .active');
            var $activeli = $active.parent("li");
            $($activeli).next().find('a[data-toggle="tab"]').removeClass("disabled");
            $($activeli).prev().find('a[href="javascript:void(0);"]').hide();
            $($activeli).next().find('a[href="javascript:void(0);"]').show();
            $($activeli).next().find('a[data-toggle="tab"]').click();
        });
        $(window).scrollTop($('.shdow-box').offset().top);
    });

    /* billing cycle click event */
    $("#billingCycle").change(function () {
        if ($(this).val() != '') {
            var billing = $(this).val();
            var subscriberCount = $(".recordset").length;
            var userUnitPrice = atob($("#userUnitPrice").val());
            var userPriceTotal = parseInt(subscriberCount) * parseInt(userUnitPrice);
            if (billing == "quarterly") {
                var planPrice = atob($("#monthlyPrice").val());
                var planName = atob($("#planType").val()) + ' Plan Monthly';
                var planCost = '$' + planPrice + ' per month/billed quarterly';
            } else if (billing == "yearly") {
                var planPrice = atob($("#yearlyPrice").val());
                var planName = atob($("#planType").val()) + ' Plan Yearly';
                var planCost = '$' + planPrice + ' per year/billed yearly';
            }
            var grandTotal = parseInt(userPriceTotal) + parseInt(planPrice);
            var additionalSubscribers = '$' + userPriceTotal + ' per month/' + subscriberCount + ' subscriber';
            $("#billing").text(planName);
            $("#planCost").text(planCost);
            $("#additionalSubscribers").text(additionalSubscribers);
            $("#grandTotal").text('$' + grandTotal);
            $("#userTotalAmount").val(userPriceTotal);
            $("#grandTotalAmount").val(grandTotal);
            $("#billingBlock").show();
        } else {
            $("#billingBlock").hide();
        }
    });

    /* Form submition through ajax */
    $("#step_3_btn").click(function () {
        var unique_email = '';
        var form_submit = true;
        $('.unique_email').each(function () {
            if (unique_email == $(this).val()) {
                form_submit = false;
            }
            unique_email = $(this).val();
        });
        if (form_submit) {
            var subscribe_frm = $("#subscribe_frm").parsley();
            var actionUrl = BASE_URL + '/subscribe/registration';
            if (subscribe_frm.isValid() || subscribe_frm.isValid() === null) {
                $.ajax({
                    type: "POST",
                    url: actionUrl,
                    data: $("#subscribe_frm").serialize(),
                    beforeSend: function () {
                        $(".loader").css("display", 'block');
                    },
                    success: function (response) {
                        $(".loader").css("display", 'none');
                        if (response.status) {
                            window.location.href = BASE_URL + '/login';
                            $.notify(response.message, "success");
                        } else {
                            $.notify(response.message, "error");
                        }
                    },
                    error: function (response) {
                        $(".loader").css("display", 'none');
                    }
                });
            }
        } else {
            $.notify('Please use unique email for each user.', "error");
        }
    });

    // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
    $sections.each(function (index, section) {
        $(section).find(':input').attr('data-parsley-group', 'block-' + index);
    });

    $(".prev-step").click(function (e) {
        var $active = $('.wizard .nav-tabs .nav-item .active');
        var $activeli = $active.parent("li");
        $($activeli).prev().find('a[data-toggle="tab"]').removeClass("disabled");
        $($activeli).prev().find('a[data-toggle="tab"]').click();
    });

    /*Add Dynamic Field Rows Code Start*/
    /*Enable dynamic field Code Start*/
    $(".add_user").on('click', function () {
        if ($(this).val() == "yes") {
            $("#btnPlus").css('display', 'block');
            setTimeout(function () {
                $("#btnPlus").trigger('click');
            }, 200);
            $("#addSubscriberTopNote").show();
        } else {
            $("#btnPlus").css('display', 'none');
            $(".recordset").remove();
            $("#btnMinus").trigger('click');
            $("#addSubscriberTopNote").hide();
        }
    });
    /*Enable dynamic field Code End*/
    /*Add Dynamic Field Rows Code End*/

    /*Add Dynamic Field Rows Code Start*/
    $("#czContainer").czMore({
        max: 2,
        min: 1,
        onLoad: null,
        onAdd: null,
        onDelete: null
    });
    /*Add Dynamic Field Rows Code End*/

    $('#expiry_year').mask('0000');
    $('#expiry_month').mask('00');
    $('#card_number').mask('0000 0000 0000 0000');

    $(".payment_type").click(function () {
        var paymentType = $("input[name='payment_type']:checked").val();
        if (paymentType == "credit") {
            $("#credit_card_block").show();
            $("#invoice_me_block").hide();
            $(".card_details").attr("required", true);
            $(".invoice_me_input").attr("required", false);
        } else if (paymentType == "invoice_me") {
            $("#invoice_me_block").show();
            $("#credit_card_block").hide();
            $(".invoice_me_input").attr("required", true);
            $(".card_details").attr("required", false);
        }
    });
    var countryId = $("#billing_country").val();
    getState(countryId);
});



function getState(countryID) {
    $("#billing_city").html('<option value="">' + '-- Select City --' + '</option>');
    var url = BASE_URL + '/subscribe/state';
    if (countryID != '') {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                country_id: countryID
            },
            beforeSend: function () {
                $(".loader").css("display", 'block');
            },
            success: function (data) {
                $(".loader").css("display", 'none');
                if (data.success == true) {
                    var stateCount = data.data.length;
                    $("#billing_state").html('<option value="">' + '-- Select State --' + '</option>');
                    for (var i = 0; i < stateCount; i++) {
                        var stateId = data.data[i].id;
                        var stateName = data.data[i].name;
                        $("#billing_state").append('<option value="' + stateId + '">' + stateName + '</option>');
                    }
                }
            },
            error: function (data) {
                $(".loader").css("display", 'none');
                console.log('Error:', data);
            }
        });
    }
}

function getCity(stateID) {
    var url = BASE_URL + '/subscribe/city';
    if (stateID != '') {
        $.ajax({
            type: "POST",
            url: url,
            data: {
                cityId: stateID
            },
            beforeSend: function () {
                $(".loader").css("display", 'block');
            },
            success: function (data) {
                $(".loader").css("display", 'none');
                if (data.success == true) {
                    var cityCount = data.data.length;
                    //                        $("#billing_city").append('<option value="">' + 'Select City*' + '</option>');
                    $("#billing_city").html('<option value="">-- Select City --</option>');
                    for (var i = 0; i < cityCount; i++) {
                        var cityId = data.data[i].id;
                        var cityName = data.data[i].name;
                        $("#billing_city").append('<option value="' + cityId + '">' + cityName + '</option>');
                    }
                }
            },
            error: function (data) {
                $(".loader").css("display", 'none');
                console.log('Error:', data);
            }
        });
    }
}

function creditCardTypeFromNumber(num) {
    // first, sanitize the number by removing all non-digit characters.
    num = num.replace(/[^\d]/g, '');
    // now test the number against some regexes to figure out the card type.
    if (num.match(/^5[1-5]\d{14}$/)) {
        return 'MasterCard';
    } else if (num.match(/^4\d{15}/) || num.match(/^4\d{12}/)) {
        return 'Visa';
    } else if (num.match(/^3[47]\d{13}/)) {
        return 'AmEx';
    } else if (num.match(/^6011\d{12}/)) {
        return 'Discover';
    }
    return 'UNKNOWN';
}

$(document).on('keyup', '#card_number', function () {
    // $('#CardNumber').keyup(function(){
    var cardNum = $(this).val();
    cardNumS = cardNum.replace(/\s+/g, '');
    if (cardNumS.length >= 6) {
        cardType = creditCardTypeFromNumber(cardNumS);
        if (cardType == 'UNKNOWN') {
            $(".customCardError").show();
            $("#step_3_btn").attr('disabled', 'disabled');
        } else {
            $(".customCardError").hide();
            $("#card_type").val(cardType);
            $("#step_3_btn").removeAttr('disabled');
        }
    }
});