$(document).ready(function () {
    // Called on changing input of credit card for validation
    /* $('#paymentSection input[type=text]').on('keyup', function () {
        cardFormValidate();
    }); */
    $("#other").click(function () {
        $("#target").submit();
    });

    $(".card_details").focusout(function () {
        cardFormValidate();
    });
});

var payMentModule = (function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var regName = /^[a-z ,.'-]+$/i;
    var regMonth = /^01|02|03|04|05|06|07|08|09|10|11|12$/;
    var regYear = /^2019|2020|2021|2022|2023|2024|2025|2026|2027|2028|2029|2030|2031$/;
    var regCVV = /^[0-9]+$/i;

    cardFormValidate = function () {
        var cardValid = 0;
        //card number validation
        $('#card_number').validateCreditCard(function (result) {
            var cardType = (result.card_type == null) ? '' : result.card_type.name;
            if (result.valid) {
                $("#card_type").val(cardType);
                $("#card_number").removeAttr('data-parsley-required');
                cardValid = 1;
            } else {
                $("#card_type").val('');
                $("#card_number").attr('data-parsley-required', true);
                cardValid = 0;
            }
        });
        //card details validation
        var cardName = $("#name_on_card").val();
        var lastName = $("#last_name").val();
        var expMonth = $("#expiry_month").val();
        var expYear = $("#expiry_year").val();
        var cvv = $("#cvv").val();

        $("#customCardError").hide();
        $("#customCardMonthError").hide();
        $("#customCardYearError").hide();
        $("#customCardCVVError").hide();
        if (cardValid == 0) {
            $("#card_number").addClass('required');
            //$("#card_number").focus();
            //$("#customCardError").show();
            $("#subscribe_frm").trigger('submit');
            return false;
        } else if (!regMonth.test(expMonth)) {
            $("#expiry_month").attr('data-parsley-pattern', regMonth);
            //$("#expiry_month").focus();
            //$("#customCardMonthError").show();
            $("#subscribe_frm").trigger('submit');
            return false;
        } else if (!regYear.test(expYear)) {
            $("#expiry_year").attr('data-parsley-pattern', regYear);
            $("#expiry_year").addClass('required');
            //$("#expiry_year").focus();
            //$("#customCardYearError").show();
            $("#subscribe_frm").trigger('submit');
            return false;
        } else if (!regCVV.test(cvv)) {
            $("#cvv").attr('data-parsley-pattern', regCVV);
            //$("#cvv").focus();
            //$("#customCardCVVError").show();
            $("#subscribe_frm").trigger('submit');
            return false;
        } else if (!regName.test(cardName)) {
            $("#name_on_card").attr('data-parsley-pattern', regName);
            //$("#name_on_card").focus();
            return false;
        } else if (!regName.test(lastName)) {
            $("#last_name").attr('data-parsley-pattern', regName);
            //$("#last_name").focus();
            return false;
        } else {
            $("#expiry_month").removeAttr('data-parsley-pattern');
            $("#expiry_year").removeAttr('data-parsley-pattern');
            $("#cvv").removeAttr('data-parsley-pattern');
            $("#name_on_card").removeAttr('data-parsley-pattern');
            $("#last_name").removeAttr('data-parsley-pattern');
            return true;
        }
    },
        callPayment = function (paymentMethod) {
            var url = BASE_URL + '/place-order';
            var cardNumber = $("#card_number").val();
            var firstName = $("#name_on_card").val();
            var lastName = $("#last_name").val();
            var expMonth = $("#expiry_month").val();
            var expYear = $("#expiry_year").val();
            var cvv = $("#cvv").val();
            var cardType = $("#card_type").val();
            var shippingAddressId = $('#shipping_address').val();
            var shippingMethodId = $('#shipping_method').val();
            var shippingCharges = $('#shipping_charges').val();
            var billingAddress = $('#billing_address').val();
            var couponCodeId = $('#coupon_code_id').val();
            var orderAppliedDiscount = $('#order_applied_discount').val();
            var shippingMethodType = $('#shipping_method_type').val();
            var accountNumber = $('#account_number').val();
            var shippingNotes = $('#shipping_notes').val();
            var requestedDeliveryDate = $('#requested_delivery_date').val();

            var postData = {
                shipping_address: shippingAddressId, billing_address: billingAddress, shipping_method: shippingMethodId,
                shipping_charges: shippingCharges, payment_method: paymentMethod,
                card_number: cardNumber, name_on_card: firstName, last_name: lastName, expiry_month: expMonth,
                expiry_year: expYear, cvv: cvv, card_type: cardType,
                coupon_code_id: couponCodeId, order_applied_discount: orderAppliedDiscount,
                shipping_method_type: shippingMethodType, account_number: accountNumber, shipping_notes: shippingNotes,
                requested_delivery_date: requestedDeliveryDate
            };
            $.ajax({
                type: "POST",
                url: url,
                data: postData,
                beforeSend: function () {
                    $(".loader").css("display", 'block');
                },
                success: function (response) {
                    $(".loader").css("display", 'none');
                    if (response.success == true) {
                        //console.log(response.data);
                        var txNumber = response.txNumber;
                        var orderId = response.orderId;
                        window.location.href = '/shopping-completed/' + txNumber + '/' + orderId;
                        $.notify(response.message, "success");
                    } else {
                        if (response.cart_count == 0) {
                            $.notify(response.message, "error");
                            window.location.href = BASE_URL;
                        } else {
                            $.notify(response.message, "error");
                        }

                    }
                },
                error: function (data) {
                    $(".loader").css("display", 'none');
                    $.notify(data, "error");
                }
            });
        }
    return {
        choosePaymentMethod: function (obj) {
            var selectedMethod = obj.value;
            //check if payment method is credit card
            if (selectedMethod == parseInt(3)) {
                $("#paymentSection").show();
                $("#card_number").attr('data-parsley-required', true);
                $("#name_on_card").attr('data-parsley-required', true);
                $("#last_name").attr('data-parsley-required', true);
                $("#expiry_month").attr('data-parsley-required', true);
                $("#expiry_year").attr('data-parsley-required', true);
                $("#cvv").attr('data-parsley-required', true);
                $("#expiry_month").attr('data-parsley-pattern', regMonth);
                $("#expiry_year").attr('data-parsley-pattern', regYear);
                $("#cvv").attr('data-parsley-pattern', regCVV);
                $("#name_on_card").attr('data-parsley-pattern', regName);
                $("#last_name").attr('data-parsley-pattern', regName);
                $("#paypalSection").hide();
                $("#cash_on_delivery").hide();
                $("#net_term_account").hide();
                $("#net_account_status").hide();

            } else if (selectedMethod == 1) {
                //check if payment method is paypal
                $("#paypalSection").show();
                $("#card_number").removeAttr('data-parsley-required');
                $("#name_on_card").removeAttr('data-parsley-required');
                $("#last_name").removeAttr('data-parsley-required');
                $("#expiry_month").removeAttr('data-parsley-required');
                $("#expiry_year").removeAttr('data-parsley-required');
                $("#cvv").removeAttr('data-parsley-required');
                $("#expiry_month").removeAttr('data-parsley-pattern');
                $("#expiry_year").removeAttr('data-parsley-pattern');
                $("#cvv").removeAttr('data-parsley-pattern');
                $("#name_on_card").removeAttr('data-parsley-pattern');
                $("#last_name").removeAttr('data-parsley-pattern');
                $("#paymentSection").hide();
                $("#cash_on_delivery").hide();
                $("#net_term_account").hide();
                $("#net_account_status").hide();
            } else if (selectedMethod == 2) {
                //check if payment method is net term account
                $("#card_number").removeAttr('data-parsley-required');
                $("#name_on_card").removeAttr('data-parsley-required');
                $("#last_name").removeAttr('data-parsley-required');
                $("#expiry_month").removeAttr('data-parsley-required');
                $("#expiry_year").removeAttr('data-parsley-required');
                $("#cvv").removeAttr('data-parsley-required');
                $("#expiry_month").removeAttr('data-parsley-pattern');
                $("#expiry_year").removeAttr('data-parsley-pattern');
                $("#cvv").removeAttr('data-parsley-pattern');
                $("#name_on_card").removeAttr('data-parsley-pattern');
                $("#last_name").removeAttr('data-parsley-pattern');
                $("#paymentSection").hide();
                $("#paypalSection").hide();
                $("#cash_on_delivery").hide();
                $("#net_term_account").show();
                $("#net_account_status").hide();

            } else if (selectedMethod == 4) {
                //check if payment method is cash on delivery
                $("#card_number").removeAttr('data-parsley-required');
                $("#name_on_card").removeAttr('data-parsley-required');
                $("#last_name").removeAttr('data-parsley-required');
                $("#expiry_month").removeAttr('data-parsley-required');
                $("#expiry_year").removeAttr('data-parsley-required');
                $("#cvv").removeAttr('data-parsley-required');
                $("#expiry_month").removeAttr('data-parsley-pattern');
                $("#expiry_year").removeAttr('data-parsley-pattern');
                $("#cvv").removeAttr('data-parsley-pattern');
                $("#name_on_card").removeAttr('data-parsley-pattern');
                $("#last_name").removeAttr('data-parsley-pattern');
                $("#paymentSection").hide();
                $("#paypalSection").hide();
                $("#net_term_account").hide();
                $("#cash_on_delivery").show();
                $("#net_account_status").hide();
            }
        },
        proceedToPayment: function (event) {
            var selected = $(".payment-method-type:checked");
            var form = $('#place_order');
            form.parsley().validate();
            if (form.parsley().isValid()) {
                //Check payment option is Credit Card
                if (selected.val() == 3) {
                    callPayment(selected.val());
                } else if (selected.val() == 2) {
                    //Check payment option is net term account
                    callPayment(selected.val());
                } else if (selected.val() == 4) {
                    //Check payment option is cash on delivery
                    callPayment(selected.val());
                } else if (selected.val() == 1) {
                    //Check payment option is paypal site payment
                    $(".loader").css("display", 'block');
                    $('#place_order').submit();
                }
            } else {

            }

        }
    }
})();