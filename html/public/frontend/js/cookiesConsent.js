$(function () {
    checkCookie();
});

function checkCookie() {
    var cookiesConsent = getCookie("cookiesConsent");
    if (cookiesConsent != "") {
        if (cookiesConsent == "deny") {
            deleteCookies();
        }
    } else {
        $(function () {
            $("body").prepend('<div id="cookie-bar"><p>DistiMonster takes your privacy seriously and we only use your personal information to administer your account and to provide the products and services you have requested from us. Our website uses cookies to improve the overall best experience we can to our customers. We also use cookies to analyze site usage for our own internal communications.  Please refer to our Privacy and Cookie Policy for more information.<p><p class="text-center m-b0 cookies-btn"><a href="javascript:void(0);" class="btn btn-primary" onclick="setCookies(\'allow\');">Accept</a><a href="/terms-of-use/" class="btn btn-primary">Cookie Policy</a></p> </div>');
        });
    }
}
/*
<button id="close" onclick="popupClose();" class="btn btn-primary">Close</button>
*/
function deleteCookies() {
    var theCookies = document.cookie.split(';');
    var isDownloadForm = false;
    if ($.cookie('isDownloadForm')) {
        isDownloadForm = true;
    }
    for (var i = 0; i < theCookies.length; i++) {
        document.cookie = theCookies[i].split('=')[0] + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }
    if (isDownloadForm) {
        setFormCookie();
    }
    document.cookie = "cookiesConsent=deny; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/;";
}

function setCookies(flag) {
    document.cookie = "cookiesConsent=" + flag + "; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/;";
    $("#cookie-bar").slideUp("slow");
}

function popupClose() {
    $("#cookie-bar").slideUp("slow");
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}