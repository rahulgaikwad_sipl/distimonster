$(document).ready(function () {
    if ($(".in_stock").length) {
        $("#orderAllitem").removeAttr('disabled');
        $("#orderAllitem").prop("disabled", false);
    }

    if ($(".out_of_stock").length) {
        $("#quoteAllitem").removeAttr('disabled');
        $("#quoteAllitem").prop("disabled", false);
    }

    if ($(".checkbox-nopart-found").length) {
        $("#addUnavailableAllitem").removeAttr('disabled');
        $("#addUnavailableAllitem").prop("disabled", false);
    }
    $('.selectall').on('change', function () {
        $('.selectall').not(this).prop('checked', false);
    });
});
function alogoForQtySeperationRecFunc(sources, qty, totalAvailableQty, partNo, partId, manufacturer, distributorName) {
    var qty = qty;
    var i = 0;
	var bestPrice = '';
	var bestPricePartId = '';
	var fohQty = '' ; 
	sources[0].sourcesPart.forEach(getBestPrice);
	async function getBestPrice(item, index) {
	  if(sources[0].sourcesPart[index].Availability[0].fohQty > qty){
		  for( i in sources[0].sourcesPart[index].Prices.resaleList){
			  if(sources[0].sourcesPart[index].Availability[0].fohQty >= qty){
			  let priseList = sources[0].sourcesPart[index].Prices.resaleList;
			  for(i in priseList){
				  if( priseList[i].minQty <=  qty && priseList[i].maxQty >= qty){
					      bestPrice = priseList[i].price;
		              	  bestPricePartId = sources[0].sourcesPart[index].sourcePartId
		            	  fohQty    = sources[0].sourcesPart[index].Availability[0].fohQty;
        			}
			   }
			  }
		  }
	  }
	}
	//return false;
    var sourcesArrayLength = sources.length;
    /* Fot getting lowest price from all the remains source */
    for (i = 0; i < sourcesArrayLength; i++) {
        if (i < sourcesArrayLength - 1) {
            if (lowestPrice > sources[i].sourcesPart[0].Prices.resaleList[0].price) {
                lowestPrice = sources[i].sourcesPart[0].Prices.resaleList[0].price;
                lowestPriceArrayIndex = i;
            }
        }
    }
    if (lowestPriceArrayIndex < sourcesArrayLength && sources[lowestPriceArrayIndex].sourcesPart[0].Availability.length > 0) {
        var availableQty = sources[lowestPriceArrayIndex].sourcesPart[0].Availability[0].fohQty;
        sourcePartId = sources[lowestPriceArrayIndex].sourcesPart[0].sourcePartId;
        sourcePartId = sourcePartId.replace(":", "_");
        if(bestPrice.length > 0 && bestPricePartId.length > 0){
			sourcePartId = bestPricePartId;
			availableQty = fohQty;
		}
		sourcePartId = sourcePartId.replace(":", "_");
        var errorMessage = '';
        /* if (!qty) {
             if (qty < sources[lowestPriceArrayIndex].sourcesPart[0].minimumOrderQuantity) {
                 errorMessage = 'Minimum order qty for part number ' + partNo + ' is ' + sources[lowestPriceArrayIndex].sourcesPart[0].minimumOrderQuantity + ' please check Buying Option first'
                 if (!multiplePartAddArray.includes(errorMessage)) {
                     multiplePartAddArray.push(errorMessage);
                 }
             } else {
                 errorMessage = 'Minimum order qty for part number ' + partNo + ' is ' + sources[lowestPriceArrayIndex].sourcesPart[0].minimumOrderQuantity + ' please check Buying Option first'
                 multiplePartAddArray.splice(errorMessage, 1);
             }
         }*/

        var showMsg = true;
        if (qty > availableQty) {
            $("#quantity_" + sourcePartId).val(availableQty);
            sources[0].sourcesPart.splice(lowestPriceArrayIndex, 1);
            if (qty - availableQty > 0) {
                alogoForQtySeperationRecFunc(sources, qty - availableQty, totalAvailableQty, partNo, partId, manufacturer, distributorName);
                showMsg = false;
            }
        } else {
            if (availableQty < qty) {
                $("#quantity_" + sourcePartId).val(availableQty);
            } else {
                $("#quantity_" + sourcePartId).val(qty);
            }
        }
    }
    if (partNo != '') {
        addToCart(partId + '_' + manufacturer, distributorName, showMsg);
    }
}
var lowestPrice = 0;
var lowestPriceArrayIndex = 0;
var multiplePartAddArray = [];
var sourcePartId = 0;

function alogoForQtySeperation(sources, partId, totalAvailableQty, partNo, manufacturer, distributorName, validation = false) {
    totalAvailableQty = parseInt(totalAvailableQty);
    var isValid = true;
    if (validation) {
        if ($('#quantity_' + partId).parsley().validate() !== true) isValid = false;
    }
    if (isValid) {
        lowestPrice = 0;
        if (sources[0].sourcesPart[0].Prices.resaleList[0].length)
            lowestPrice = sources[0].sourcesPart[0].Prices.resaleList[0].price;
        sources[0].sourcesPart = sources[0].sourcesPart.filter(function (el) {
            return el.Availability[0]['fohQty'] !== 0;
        });
        var inputTxt = $('#quantity_' + partId).val();
        $('.requestQtyDiv').html(inputTxt);
        if (inputTxt == '') {
            if (validation)
                $('#message_' + partId).text('Please enter quantity.').show();
            return false;
        } else if (parseInt(inputTxt) > parseInt(totalAvailableQty)) {
            //  $('#message_' + partId).text('Entered quantity is more than available quantity.').show();
            return false;
        } else {
            $('#message_' + partId).hide();
        }
        var sourcePartIdForBlank = '';

        for (i = 0; i < sources.length; i++) {
            for (spi = 0; spi < sources[i].sourcesPart.length; spi++) {
                sourcePartIdForBlank = sources[i].sourcesPart[spi].sourcePartId;
                sourcePartIdForBlank = sourcePartIdForBlank.replace(":", "_");
                $("#quantity_" + sourcePartIdForBlank).val('');
                if (sources[i].sourcesPart[spi].Availability[0]['fohQty'] == 0) {
				//	console.log('==>>>>>'+ JSON.stringify(sources[i]));
                    sources[i].sourcesPart.splice(spi, 1);
                }
            }
        }

        lowestPriceArrayIndex = 0;
        sourcePartId = sources[0].sourcesPart[0].sourcePartId;
        var qty = $("#quantity_" + partId).val();
		alogoForQtySeperationRecFunc(sources, qty, totalAvailableQty, partNo, partId, manufacturer, distributorName);
    }
}

function submitAddCart(partNo) {
    var count = 0;
    var emptyInputCount = 0;
    var inputCount = 0;
    var isValid = true;
    // pop-internal-part_

    $('.pop-quantity-input_' + partNo).each(function () {

        inputCount++;
        var inputQty = $(this).val();
        var avaQty = $(this).attr('data-parsley-max');
        var sourcePartId = $(this).data('source');
        if (inputQty == '') {
            emptyInputCount++;
        }
        if ($('#quantity_' + sourcePartId).parsley().validate() !== true) isValid = false;
        if ($('#customerPartNo_' + sourcePartId).parsley().validate() !== true) isValid = false;


        if (parseInt(inputQty) > parseInt(avaQty)) {
            count++;
            // $('#message_' + sourcePartId).show();
        } else {
            // $('#message_' + sourcePartId).hide();
        }

    });


    if (inputCount == emptyInputCount) {
        $('.notifyjs-container').trigger('notify-hide');
        $.notify('Please enter quantity to add in cart.', "error");
        return false;
    } else {
        if (isValid) {
            if (count > 0) {
                return false;
            } else {
                addToCart(partNo);
            }
        }
    }
}

function addToCart(partNo, distributorName, showMsg = true) {
    if (multiplePartAddArray.length > 0) {
        $.each(multiplePartAddArray, function (index, item) {
            $('.notifyjs-container').trigger('notify-hide');
            //$('.notifyjs-corner').empty();
            $.notify(item, "warn");
        });
    } else {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = "/add-bom-part-to-cart";
        $.ajax({
            type: "POST",
            url: BASE_URL + url,
            data: $('#bompart_' + partNo).serialize(),
            beforeSend: function () {
                $(".loader").css("display", 'block');
            },
            success: function (data) {
                $(".loader").css("display", 'none');
                /*    console.log(data);*/

                if (data.status) {
                    var currentUrl = window.location.href;
                    if (currentUrl.indexOf('cart') >= 0) {
                        location.reload();
                    } else {
                        if (showMsg) {
                            $.notify(data.message, "success");
                        }
                        $("#cartCount").css('display', 'block');
                        $("#cartCount").html(data.cartItemCount);
                        //$("#totalPrice").html(data.cartSubTotal);
                    }
                } else {
                    $.notify(data.message, "warn");
                }
            },
            error: function (data) {
                $(".loader").css("display", 'none');
                console.log('Error:', data);
            }
        });
    }


}

function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
/*
$('.quantity-input').on('change',function(){
    var inputQty = $(this).val();
    var avaQty = $(this).data('quantity');
    if(inputQty == ''){
        $(this).parent().find('.error-max-qty').html('Please enter quantity.<br/>').show();
    }else if(parseInt(inputQty) > parseInt(avaQty)){
        $(this).parent().find('.error-max-qty').html('Entered quantity is more than available quantity.<br/>').show();
    }else{
        $(this).parent().find('.error-max-qty').hide();
    }
});
*/
$('.pop-quantity-input').on('change', function () {
    var inputQty = $(this).val();
    var avaQty = $(this).attr('data-parsley-max');
    var sourcePartId = $(this).data('source');
    if (parseInt(inputQty) > parseInt(avaQty)) {
        //  $('#message_' + sourcePartId).show();
    } else {
        //  $('#message_' + sourcePartId).hide();
    }
});

function getAllPartForQuote() {
    var allBomPartArray = [];
    var isValid = true;
    $(".out_of_stock").each(function () {
        var qtyinput = $(this).data('qtyinput');
        $('#' + qtyinput).parsley().reset();
        if ($(this).prop("checked") == true) {
            if ($('#' + qtyinput).parsley().validate() !== true) isValid = false;
        }
    });

    if (isValid) {
        if ($('.out_of_stock').is(':checked')) {
            $(".out_of_stock").each(function () {
                if ($(this).prop("checked") == true) {
                    var datecode = $(this).data('datecode');
                    var availabledatainput = $(this).data('availabledatainput');
                    var resalelistinput = $(this).data('resalelistinput');
                    var instock = $(this).data('instock');
                    var customerpartnoinput = $(this).data('customerpartnoinput');
                    var sourcepartid = $(this).data('sourcepartid');
                    var distributor = $(this).data('distributor');
                    var mfrcd = $(this).data('mfrcd');
                    var manufacturer = $(this).data('manufacturer');
                    var partnum = $(this).data('partnum');
                    var qtyinput = $(this).data('qtyinput');
                    var fohqty = $(this).data('fohqty');
                    var quoteItemObj = {
                        quantity: $("#" + qtyinput).val(),
                        productId: partnum,
                        manufacturer: manufacturer,
                        mfrCd: mfrcd,
                        sourcePartId: sourcepartid,
                        distributorName: distributor,
                        //customerPartNo: $("#" + customerpartnoinput).val(),
                        customerPartNo: customerpartnoinput,
                        stockAvailability: instock,
                        resaleListData: $("#" + resalelistinput).val(),
                        availableData: $("#" + availabledatainput).val(),
                        dateCode: datecode,
                        requestedQuantity: $("#" + qtyinput).val(),
                        requestedCost: $(this).data('requestedprice'),
                        requestedDeliveryDate: ''
                    };
                    allBomPartArray.push(quoteItemObj);
                }
            });
            var url = "/add-all-bom-to-quote";
            $.ajax({
                type: "POST",
                url: BASE_URL + url,
                data: {
                    allBomPartArray: allBomPartArray
                },
                beforeSend: function () {
                    $(".loader").css("display", 'block');
                },
                success: function (response) {
                    $(".loader").css("display", 'none');
                    if (response.status) {
                        $.notify(response.message, "success");
                        $("#quoteCount").css('display', 'block');
                        $("#quoteCount").html(response.quoteItemCount);
                    } else {
                        $.notify(response.message, "error");
                    }
                },
                error: function (error) {
                    $(".loader").css("display", 'none');
                    $.notify(error, "error");
                }
            });
        } else {
            $('.notifyjs-container').trigger('notify-hide');
            $.notify('Please select at least one part to add in Quote List.', "warn");
        }
    }

}

function qtySeparationForMultipleSelectedPartsFunctionRecurcive(sources, qty, totalAvailableQty, partNo, partId, manufacturer) {
    var qty = qty;
    var i = 0;
    var sourcesArrayLength = sources.length;
	var bestPrice = '';
	var bestPricePartId = '';
	var fohQty = '' ; 
	sources[0].sourcesPart.forEach(getBestPrice);
	async function getBestPrice(item, index) {
	  if(sources[0].sourcesPart[index].Availability[0].fohQty > qty){
		  for( i in sources[0].sourcesPart[index].Prices.resaleList){
			  if(sources[0].sourcesPart[index].Availability[0].fohQty >= qty){
			  let priseList = sources[0].sourcesPart[index].Prices.resaleList;
			  for(i in priseList){
				  if( priseList[i].minQty <=  qty && priseList[i].maxQty >= qty){
					      bestPrice = priseList[i].price;
		              	  bestPricePartId = sources[0].sourcesPart[index].sourcePartId
		            	  fohQty    = sources[0].sourcesPart[index].Availability[0].fohQty;
        			}
			   }
			  }
		  }
	  }
	}
    /* Fot getting lowest price from all the remains source */
    for (i = 0; i < sourcesArrayLength; i++) {
        if (i < sourcesArrayLength - 1) {
            if (lowestPrice > sources[i].sourcesPart[0].Prices.resaleList[0].price) {
                lowestPrice = sources[i].sourcesPart[0].Prices.resaleList[0].price;
                lowestPriceArrayIndex = i;
            }
        }
    }
    if (lowestPriceArrayIndex < sourcesArrayLength && sources[lowestPriceArrayIndex].sourcesPart[0].Availability.length > 0) {
        var availableQty = sources[lowestPriceArrayIndex].sourcesPart[0].Availability[0].fohQty;
        sourcePartId = sources[lowestPriceArrayIndex].sourcesPart[0].sourcePartId;
		
		if(bestPrice.length > 0 && bestPricePartId.length > 0){
			sourcePartId = bestPricePartId;
			availableQty = fohQty;
		}
		console.log('availableQty====>>>'+availableQty+'sourcePartId=====>'+sourcePartId);
	
        sourcePartId = sourcePartId.replace(":", "_");
        //check if the  qty for source is less then minimum order qty then create a array of error
        var errorMessage = '';
        if (qty < sources[lowestPriceArrayIndex].sourcesPart[0].minimumOrderQuantity) {
            errorMessage = 'Minimum order qty for part number ' + partNo + ' is ' + sources[lowestPriceArrayIndex].sourcesPart[0].minimumOrderQuantity + ' please check Price Breaks first'
            if (!multiplePartAddArray.includes(errorMessage)) {
                multiplePartAddArray.push(errorMessage);
            }
        } else {
            errorMessage = 'Minimum order qty for part number ' + partNo + ' is ' + sources[lowestPriceArrayIndex].sourcesPart[0].minimumOrderQuantity + ' please check Price Breaks first'
            multiplePartAddArray.splice(errorMessage, 1);
        }
        if (qty > availableQty) {
            $("#quantity_" + sourcePartId).val(availableQty);
            sources[0].sourcesPart.splice(lowestPriceArrayIndex, 1);
            if (qty - availableQty > 0) {
                qtySeparationForMultipleSelectedPartsFunctionRecurcive(sources, qty - availableQty, totalAvailableQty, partNo, partId, manufacturer);
            }
        } else {
            if (availableQty < qty) {
                $("#quantity_" + sourcePartId).val(availableQty);
            } else {
                $("#quantity_" + sourcePartId).val(qty);
            }
        }
    }
}

var qtyEmptyArray = [];
var qtyExcessArray = [];

function qtySeparationForMultipleSelectedParts(sources, partId, totalAvailableQty, partNo, manufacturer) {
    sources[0].sourcesPart = sources[0].sourcesPart.filter(function (el) {
        return el.Availability[0]['fohQty'] !== 0;
    });
    var inputTxt = $('#quantity_' + partId).val();
    $('.requestQtyDiv').html(inputTxt);
    if (inputTxt == '') {
        $('#message_' + partId).text('Please enter quantity.').show();
        if (!qtyEmptyArray.includes(partNo)) {
            qtyEmptyArray.push(partNo);
        }
        return false;
    } else if (parseInt(inputTxt) > parseInt(totalAvailableQty)) {
        //  $('#message_' + partId).text('Entered quantity is more than available quantity.').show();

        if (!qtyExcessArray.includes(partNo)) {
            qtyExcessArray.push(partNo);
        }
        return false;
    } else {
        qtyEmptyArray.splice(partNo, 1);
        qtyExcessArray.splice(partNo, 1);
        $('#message_' + partId).hide();
    }

    var sourcePartIdForBlank = '';
    for (i = 0; i < sources.length; i++) {
        for (spi = 0; spi < sources[i].sourcesPart.length; spi++) {
            sourcePartIdForBlank = sources[i].sourcesPart[spi].sourcePartId;
            sourcePartIdForBlank = sourcePartIdForBlank.replace(":", "_");
            $("#quantity_" + sourcePartIdForBlank).val('');
            if (sources[i].sourcesPart[spi].Availability[0]['fohQty'] == 0) {
                sources[i].sourcesPart.splice(spi, 1);
            }
        }
    }
	
	
    lowestPrice = 0;
    if (sources[0].sourcesPart[0].Prices.resaleList.length)
        lowestPrice = sources[0].sourcesPart[0].Prices.resaleList[0].price;
		lowestPriceArrayIndex = 0;
		sourcePartId = sources[0].sourcesPart[0].sourcePartId;
	
    var qty = $("#quantity_" + partId).val();
    qtySeparationForMultipleSelectedPartsFunctionRecurcive(sources, qty, totalAvailableQty, partNo, partId, manufacturer);
}

function addAllSelectedPartsToCart() {
	
    var isValid = true;
    $(".in_stock").each(function () {
        var qtyinput = $(this).data('qtyinput');
        $('#' + qtyinput).parsley().reset();
        if ($(this).prop("checked") == true) {
            if ($('#' + qtyinput).parsley().validate() !== true) isValid = false;
        }
    });
    if (isValid) {
        if ($('.in_stock').is(':checked')) {
            var callAddtoCart = false;
            var x = 1;
            $(".in_stock").each(function (i) {
                if ($(this).prop("checked") == true) {
                    var datsrc = $(this).data("source");
                    var sourceArray = atob(datsrc);
                    var distributorName = $(this).data("distributor");
                    var formId = $(this).data("formid");
                    var availabledatainput = $(this).data("availabledatainput");
                    var datecode = $(this).data("datecode");
                    var resalelistinput = $(this).data("resalelistinput");
                    var instock = $(this).data("instock");
                    var partNo = $(this).data("partnum");
                    var manufacturer = $(this).data("manufacturer");
                    var fohqty = $(this).data("fohqty");
                    var qtyinput = $(this).data("qtyinput");
                    var partId = $(this).data("itemid");
                    var availabilityqty = $(this).data("availabilityqty");
                    var mfrcd = $(this).data("mfrcd");
                    var form = $('#' + formId);
                    if (instock) {
                        qtySeparationForMultipleSelectedParts(JSON.parse(sourceArray), partId, availabilityqty, partNo, manufacturer);
                    }
                }
                if ($(".in_stock").length == x) {
                    callAddtoCart = true;
                }
                x++;
            });
            if (callAddtoCart) {
                addSelectedToCart();
            }
        } else {
            $('.notifyjs-container').trigger('notify-hide');
            $.notify('Please select at least one part to add in cart.', "warn");
        }
    } else {
        var errorDivHeight = ($('.filled').offset().top - 60);
        $('html, body').animate({
            scrollTop: errorDivHeight
        }, 400);
        //$(".parsley-errors-list").offset().top;

        //$("div.demo").scrollTop(300);
        $('.notifyjs-container').trigger('notify-hide');
        $.notify('Please set correct quantities for the MPNs.', "error");
    }
}

var inStockPartArray = [];

function addSelectedToCart() {
    if (qtyEmptyArray.length > 0) { } else {
        if (qtyExcessArray.length > 0) { } else {
            if (multiplePartAddArray.length > 0) {
                $.each(multiplePartAddArray, function (index, item) {
                    $('.notifyjs-container').trigger('notify-hide');
                    //$('.notifyjs-corner').empty();
                    $.notify(item, "warn");
                });
            } else {
                var serializeFormArray = [];
                var dataString = '';
                $(".in_stock").each(function () {
                    if ($(this).prop("checked") == true) {
                        var formId = $(this).data("formid");
                        var distributor = $(this).data("distributor");
                        var qtyinput = $(this).data("qtyinput");
                        var instock = $(this).data("instock");
                        var partNo = $(this).data("partnum");
                        if (instock) {
                            inStockPartArray.push(partNo);
                        } else {
                            inStockPartArray.splice(partNo, 1);
                        }
                        var inputTxt = $('#' + qtyinput).val();
                        if (inputTxt != '') {
                            serializeFormArray.push($('#' + formId).serialize());
                        }
                    }
                });

                if (inStockPartArray.length > 0) {
                    var url = "/add-selected-part-to-cart";
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + url,
                        data: {
                            formdata: serializeFormArray
                        },
                        beforeSend: function () {
                            $(".loader").css("display", 'block');
                        },
                        success: function (data) {
                            $(".loader").css("display", 'none');
                            if (data.status) {
                                serializeFormArray = [];
                                $.notify(data.message, "success");
                                $("#cartCount").css('display', 'block');
                                $("#cartCount").html(data.cartItemCount);
                                //$("#totalPrice").html(data.cartSubTotal);
                                //window.location.href = "/cart";
                            } else {
                                $.notify(data.message, "warn");
                            }
                        },
                        error: function (data) {
                            $(".loader").css("display", 'none');
                            $.notify(data, "error");
                        }
                    });
                } else {
                    $('.notifyjs-container').trigger('notify-hide');
                    $.notify('Some Selected parts are Out Of Stock, So can not added in cart.', "error");
                }

            }
        }
    }
}

/* Get method name to add product */
function addProduct(methodName, isChecked) {
    $("checkbox").prop('checked', false);
    $("#add_mpn").attr("onclick", methodName + '();');
    if (methodName == "addAllSelectedPartsToCart") {
        $('.in_stock').prop('checked', $(isChecked).prop("checked"));
        $('.out_of_stock').prop('checked', false);
        $('.checkbox-nopart-found').prop('checked', false);
    } else if (methodName == "getAllPartForQuote") {
        $('.out_of_stock').prop('checked', $(isChecked).prop("checked"));
        $('.in_stock').prop('checked', false);
        $('.checkbox-nopart-found').prop('checked', false);
    } else if (methodName == "addUnavailableProductToQuote") {
        $('.checkbox-nopart-found').prop('checked', $(isChecked).prop("checked"));
        $('.in_stock').prop('checked', false);
        $('.out_of_stock').prop('checked', false);
    }
}

$(".out_of_stock").click(function () {
    $('.in_stock').prop('checked', false);
    $('.checkbox-nopart-found').prop('checked', false);
    $("#add_mpn").attr("onclick", 'getAllPartForQuote();');
});

$(".in_stock").click(function () {
    $('.out_of_stock').prop('checked', false);
    $('.checkbox-nopart-found').prop('checked', false);
    $("#add_mpn").attr("onclick", 'addAllSelectedPartsToCart();');
});

$(".checkbox-nopart-found").click(function () {
    $('.in_stock').prop('checked', false);
    $('.out_of_stock').prop('checked', false);
    $("#add_mpn").attr("onclick", 'addUnavailableProductToQuote();');
});

function itemSelection() {
    if ($('.checkbox-qty:checkbox:checked').length == 0) {
        $.notify("Please select anyone MPN to add into the cart or quotes request.", "error");
    }
}

/* add unavailable product to quote */
function addUnavailableProductToQuote() {
    if ($(".checkbox-nopart-found:checked").length > 0) {
        var mpnVal = new Array();
        var internalPart = new Array();
        var xlxsQuantity = new Array();
        $(".checkbox-nopart-found:checked").each(function () {
            mpnVal.push($(this).val());
            internalPart.push($(this).data('internalpart'));
            xlxsQuantity.push($(this).data('xlxsquantity'));
        });
        $("#popupPartNumber").val(mpnVal);
        $("#popupInternalPart").val(internalPart);
        $("#popupRequestedQuantities").val(xlxsQuantity);
        showPopup();
    } else {
        $.notify("Please select anyone MPN to request quote.", "error");
    }
}

$('.checkbox-qty').change(function () {
    // create var for parent .checkall and group
    var group = $(this).data('group'),
        checkall = $('.checked_all');
    // do we have some checked? Some unchecked? Store as boolean variables
    var someChecked = $('.checkbox-qty:checkbox:checked').length > 0;
    var someUnchecked = $('.checkbox-qty:checkbox:not(:checked)').length > 0;
    if (someChecked) {
        //  $('#add_all_to_cart').css('display', 'block');
        //$('#add_all_to_quote').css('display', 'block');
    } else {
        multiplePartAddArray = [];
        qtyEmptyArray = [];
        qtyExcessArray = [];
        inStockPartArray = [];
        //  $('#add_all_to_cart').css('display', 'none');
        //  $('#add_all_to_quote').css('display', 'none');
    }
    checkall.prop("indeterminate", someChecked && someUnchecked);
    checkall.prop("checked", someChecked || !someUnchecked);
    // fire change() when this loads to ensure states are updated on page load
}).change();

// clicking .checkall will check all children in the same group.
$('.checked_all').click(function () {
    $('.checkbox-qty').prop('checked', $(this).prop("checked"));
    if ($('.checkbox-qty').is(':checked')) {
        //  $('#add_all_to_cart').css('display', 'block');
        // $('#add_all_to_quote').css('display', 'block');
    } else {
        multiplePartAddArray = [];
        qtyEmptyArray = [];
        qtyExcessArray = [];
        inStockPartArray = [];
        inStockPartArray = [];
        // $('#add_all_to_cart').css('display', 'none');
        // $('#add_all_to_quote').css('display', 'none');
    }
});

$(function () {
    if ($(".checkbox-nopart-found").length > 0) {
        $("#request_product-btn").show();
    }

    $('.request_product-btn').click(function () {
        if ($(".checkbox-nopart-found:checked").length > 0) {
            var mpnVal = new Array();
            $(".checkbox-nopart-found:checked").each(function () {
                mpnVal.push($(this).val());
            });

            var mpnValStr = mpnVal.join();
            $("#popupPartNumber").val(mpnValStr);
            showPopup();
        } else {
            $.notify("Please select anyone MPN to request quote.", "error");
        }
    });
});

