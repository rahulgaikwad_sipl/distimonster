function initMap() { var position = { lat: 34.236200, lng: -118.566405 }; var map = new google.maps.Map(document.getElementById('office_location_map'), { zoom: 10, center: position }); var marker = new google.maps.Marker({ position: position, map: map }); } $('.table-bom-product').on('click', '.addnote', function () {
    $(this).closest('td').find('.text-area').show()
    $(".btn-cancel").click(function () { $(".text-area").hide(); });
});

window.addEventListener("pageshow", function (event) {
    var historyTraversal = event.persisted || (typeof window.performance != "undefined" && window.performance.navigation.type === 2);
    if (historyTraversal) {
        // Handle page restore.
        window.location.reload();
    }
});

//Autosuggestion Code

//Autosuggestion Code

var optionsSuggestion = {
    url: function (phrase) {
        return BASE_URL + "/get-part-details";
    },
    getValue: function (element) {
        return element.name;
    },
    ajaxSettings: {
        dataType: "json",
        method: "POST",
        data: {
            dataType: "json"
        }
    },
    preparePostData: function (data) {
        data.phrase = $("#part_name").val();
        return data;
    },
    requestDelay: 200,
    list: {
        maxNumberOfElements: 100,
        match: {
            enabled: true
        },
        onClickEvent: function () {
            var value = $("#part_name").getSelectedItemData().slug;
            window.location.href = BASE_URL+"/category/"+value;
        }
    }
};
$("#part_name").easyAutocomplete(optionsSuggestion);

function shareBOM(frm_cls) {
    var isValid = true;
    if ($('.' + frm_cls).parsley().validate() !== true) isValid = false;
    if (isValid) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = "/share-bom";
        $.ajax({
            type: "POST",
            url: BASE_URL + url,
            data: $("." + frm_cls).serialize(),
            beforeSend: function () {
                $(".loader").css("display", 'block');
            },
            success: function (data) {
                $(".loader").css("display", 'none');
                console.log(data);
                if (data.status == true) {
                    $.notify(data.message, "success");
                    $('.' + frm_cls + ' [type="checkbox"]').removeAttr('checked');
                    $('.bom_note').val('');
                    $('.close').trigger('click');
                } else {
                    $.notify(data.message, "warn");
                }
            },
            error: function (data) {
                $(".loader").css("display", 'none');
                console.log('Error:', data);
            }
        });
    }
}