$(document).ready(function () {
    var flag = false;
    $('#bom_file').change(function () {
        var image_name = $("#bom_file").val();
        var i = $(this).prev('label').clone();
        var file = $('#bom_file')[0].files[0].name;
        $(this).prev('button').text(file);
        $('#bom_file1').val(file);
        if (!$("#bom_error_message").text().trim().length) {
            $("#bom_error_message").html('');
        }
        if (image_name != "") {
            $("#bom_details").attr('required', false);
        }
        if (image_name == "") {
            //$("#bom_error_message").html('');
            var img_name = "{{url('frontend/assets/images/upload.png')}}";
            $("#bom_file").prev('button').html('<img src="' + img_name + '" class="m-r5">Upload a file');
        }
        $("#submit_quote").trigger("click");
    });
    $("#submit_quote").click(function () {
        var image_name = $("#bom_file").val();
        var bom_details = $("#bom_details").val();
        if (image_name != "" && bom_details != "") {
            $("#bom_error_message").html('<ul class="parsley-errors-list filled" id="parsley-id-9"><li class="parsley-required">Please use only one method to upload BOM.</li></ul>');
        } else {
            $("#bom_details").attr('required', true);
            if (bom_details == "") {
                if (!$("#bom_error_message").text().trim().length) {
                    $("#bom_error_message").html('');
                }
            }
            if (image_name != "") {
                $("#bom_details").attr('required', false);
            }
            if (image_name == "") {
                // $("#bom_error_message").html('');
                var img_name = BASE_URL + 'frontend/assets/images/upload.png';
                $("#bom_file").prev('button').html('<i class="fa fa-arrow-circle-up" aria-hidden="true"></i>Drop or select a file to upload (xls, csv)');
            }
            $('#add_bom').trigger('submit');
        }
    });
    $("#bom_details").keyup(function (value) {
        if ($("#bom_details").val() == "") {
            $("#bom_error_message").html('');
        }
    });
    $('#add_bom').submit(function () {
        var form = $("#add_bom");
        form.parsley().validate();
        if (form.parsley().isValid()) {
            $('#add_bom_quote').parsley().reset();
            $('#bom_form').modal('show');
            if (flag) {
                return true;
            } else {
                return false;
            }
        }
    });
    $("#submit_bom").click(function () {
        $('#add_bom_quote').trigger('submit');
    });
    $('#add_bom_quote').submit(function () {
        var add_bom_quote = $("#add_bom_quote");
        add_bom_quote.parsley().validate();
        if (add_bom_quote.parsley().isValid()) {
            var full_name = $('#full_name').val();
            var company_name = $('#company_name').val();
            var email = $('#email').val();
            var contact_no = $('#contact_no').val();
            var address = $('#address').val();
            var note = $('#note').val();
            $('#full_name_bom').val(full_name);
            $('#company_name_bom').val(company_name);
            $('#email_bom').val(email);
            $('#contact_no_bom').val(contact_no);
            $('#address_bom').val(address);
            $('#note_bom').val(note);
            flag = true;
            submit_form();
        }
    });
});

function submit_form() {
    $('#add_bom').trigger('submit');
    $(".loader").css("display", 'block');
    $("#loader_note_message").css("display", 'block');
}

/* Save bom ajax on save bom click */
var saveBomAjax = function () {
    var form = $("#save_bom_form");
    form.parsley().validate();
    if (form.parsley().isValid()) {
        var formData = { bom_name: $("#bomName").val(), update_bom_id: $("#updateBomId").val() };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = "/update-bom-name";
        $.ajax({
            type: "POST",
            url: BASE_URL + url,
            data: formData,
            beforeSend: function () {
                $(".loader").css("display", 'block');
            },
            success: function (response) {
                $(".loader").css("display", 'none');
                if (response.status) {
                    $.notify(response.message, { autoHide: true, className: "success" });
                    setTimeout(function () {
                        window.location.href = BASE_URL + '/my-bom';
                    }, 800);
                } else {
                    $.notify(response.message, "error");
                    $("#save-bom-modal").modal('hide');
                }
            },
            error: function (data) {
                $(".loader").css("display", 'none');
                $.notify(data, "error");
                $("#save-bom-modal").modal('hide');
            }
        });

    }
};