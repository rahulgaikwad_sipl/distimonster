var limit = 9;
var isAjaxRequest = true;
var isAjaxFilter = false;
var filter = { offset: 0, category: '', keyword: '' };
var currentAjaxRequest = null;
var getBoxData = function () {
    if (isAjaxRequest == true) {
        isAjaxRequest = false;
        /* Add country filter if local legends */
        var category = $("#category_name").val();
        if (category != '') {
            filter.category = category;
        }

        /* loader inside page */
        currentAjaxRequest = $.ajax({
            url: BASE_URL + "/get-parts",
            type: "GET",
            data: filter,
            beforeSend: function () {
                $("#bottom-loader").css("display", 'block');
                if (currentAjaxRequest != null) {
                    currentAjaxRequest.abort();
                }
            },
            success: function (response) {
                $("#bottom-loader").css("display", 'none');
                if (response.status == true) {
                    var lis = $("#part-container");
                    if (lis.length > 0) {
                        $("#part_search_div").css("display", 'block');
                    }
                    var partContent = response.html;
                    $('#part-container').append(partContent);
                    if (isAjaxFilter == true) {
                        $('html, body').animate({ scrollTop: '50px' }, 800);
                    }
                    if (response.isData == true) {
                        filter.offset = filter.offset + limit;
                        isAjaxRequest = true;
                    }
                } else {
                    if (filter.offset == 0) {
                        $('#part-container').append('<div class="no-product-found"><strong>No Products found under this category at this moment.</strong> </div>');
                    }
                }
            }, error: function (data) {
                $("#bottom-loader").css("display", 'none');
                $.notify(data, "error");
            }
        });
    }
};
$("#category_name").on('change', function () {
    isAjaxRequest = true;
    var category = this.value;
    $('#part-container').empty();
    $("#label_name").text(category);
    // var url = BASE_URL+'/category/'+category;
    //   window.location.href= url;
    //alert(url);
    // getBoxData();
});
var searchPartList = function () {
    var isValid = true;
    if ($('#part_search_box').parsley().validate() !== true) isValid = false;
    if (isValid) {
        $('#part-container').empty();
        var keyword = $("#part_search_box").val();
        if (keyword != '') {
            filter.keyword = keyword;
            filter.offset = 0;
            isAjaxRequest = true;
        }
        getBoxData();
    }
};
/* On scroll use lazy loading for showing the boxex*/
function searchCategory() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("myInput");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myList");
    li = ul.getElementsByTagName("li");
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";

        }
    }
}
$("#myList").listnav({
    noMatchText: 'No parts found',
    showCounts: false,
    includeNums: false,
});

var showPopup = function () {
    $('#request_part_detail_form').parsley().reset();
    $("#popupExpectedDeliveryDate").val('');
    $("#popupExpectedQuantity").val('');
    $("#popupExpectedUnitPrice").val('');
    $("#request-part-modal").modal('show');
};
var addToRequestedPart = function () {
    var form = $("#request_part_detail_form");
    form.parsley().validate();
    if (form.parsley().isValid()) {
        var mpnVal = new Array();
        var internalPart = new Array();
        var xlxsQuantity = new Array();
        $(".checkbox-nopart-found:checked").each(function () {
            mpnVal.push($(this).val());
            internalPart.push($(this).data('internalpart'));
            xlxsQuantity.push($(this).data('xlxsquantity'));
        });
        var formData = { partNumber: mpnVal, expectedDeliveryDate: $("#popupExpectedDeliveryDate").val(), expectedQuantity: xlxsQuantity, expectedUnitPrice: $("#popupExpectedUnitPrice").val(), categoryName: $("#popupCategoryName").val(), contactNumber: $('#popupContactNumber').val(), leadQuoteName: $("#popupLeadQuoteName").val(), internalPart: internalPart };
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var url = "/request-product";
        $.ajax({
            type: "POST",
            url: BASE_URL + url,
            data: formData,
            beforeSend: function () {
                $(".loader").css("display", 'block');
            },
            success: function (response) {
                $(".loader").css("display", 'none');
                if (response.status) {
                    $.notify(response.message, { autoHide: true, className: "success" });
                    setTimeout(function () {
                        window.location.href = BASE_URL + '/quotes';
                    }, 800);
                } else {
                    $.notify(response.message, "error");
                    $("#request-part-modal").modal('hide');
                }
            },
            error: function (data) {
                $(".loader").css("display", 'none');
                $.notify(data, "error");
                $("#request-part-modal").modal('hide');
            }
        });

    }
};

var clearPartSearch = function () {
    $('#part_search_box').empty();
    window.location.reload();
};