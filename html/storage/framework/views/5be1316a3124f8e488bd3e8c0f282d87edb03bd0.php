<ul class="tabs shdow-box">
	<li  class="<?php echo e(Request::is('faq') ? 'active' : ''); ?>"rel="tab1"> <a href="<?php echo e(url('faq')); ?>">Frequently Asked Questions</a></li>
	<li class="<?php echo e(Request::is('return-policy') ? 'active' : ''); ?>" rel="tab2"><a href="<?php echo e(url('return-policy')); ?>">Return Policy</a></li>
	<li class="<?php echo e(Request::is('contact') ? 'active' : ''); ?>" rel="tab3"> <a href="<?php echo e(url('contact')); ?>">Contact</a></li>
	<li class="<?php echo e(Request::is('request-a-demo') ? 'active' : ''); ?>"  rel="tab4"> <a href="<?php echo e(url('request-a-demo')); ?>">Request a Demo</a></li>
	<li rel="tab5"> <a href="javascript:void(0);" id="chat_link">Live Chat</a></li>
</ul>