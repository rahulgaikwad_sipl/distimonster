<input type="checkbox" class="toggle-switch" <?php echo e(($news->status == 1)?'checked':''); ?>

data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger"
       data-msg="Are you sure you want to <?php echo e(($news->status == 1)?'Deactivate':'Activate'); ?> this news."
       data-href="/admin/update-news-status"  data-id="<?php echo e($news->id); ?>"  data-status="<?php echo e($news->status); ?>" data-size="small"
>
      