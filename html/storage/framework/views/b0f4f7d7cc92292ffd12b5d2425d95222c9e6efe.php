<?php $__currentLoopData = $parts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k=>$row): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <li>
	        <div class="shdow-box my-quote-detail">
	            <div class="hind-font">
	            	MPN:
	            	<a class="f-s18" href="<?php echo e(url('search-part/'.$categorySlug.'/'.base64_encode($row->manufacturer_part_number))); ?>"><?php echo e($row->manufacturer_part_number); ?></a>
	            </div>
				<div class="block m-t5 m-b10">
				    Manufacturer: <span class="black font-w500"> <?php echo e($row->manufacturer_name); ?> </span>
				</div>
				<div class="block m-t5 m-b10">
					Description:<br/><span class="black font-w500"><?php echo e($row->part_description ? $row->part_description:'N/A'); ?></span>
				</div>
				<div class="block m-t10 m-b10">
					<?php if($row->datasheet_url != ''): ?>
				    <a target="_blank" href="<?php echo e($row->datasheet_url); ?>" class="data-sheet"><img src="<?php echo e(url('/frontend/assets/images/pdf-icon.png')); ?>" width="12px"> Datasheet</a>
					<?php endif; ?>
				</div>
				<div class="block m-t10 m-b10">
					<a href="<?php echo e(url('search-part/'.$categorySlug.'/'.base64_encode($row->manufacturer_part_number))); ?>"> Search Price</a>
				</div>
					           
	        </div>
    	</li>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
