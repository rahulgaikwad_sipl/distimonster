
<?php $__env->startSection('title', '| Login'); ?>
<?php $__env->startSection('content'); ?>


    <!--Register Section-->
    <section class="gray-bg">
        <div class="container clearfix">
            <?php if(session()->has('registration_success')): ?>
                <div class="col-md-12 registration_success_message">
                    <?php echo session('registration_success'); ?>

                </div>
            <?php endif; ?>
            <div class="col-md-12">
                <div class="shdow-box">
                    <div class="register-box-content login-box-content">
                        <div class="row">
                            <div class="col-md-6 bdr-right">
                                <h2 class="block-title"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/login-icon.png')); ?>" class="img-responsive"></span>Login</h2>
                                <?php echo Form::open(['method' => 'POST', 'id'=>'login_form', 'data-parsley-validate','url' => 'login','class'=>'form-horizontal']); ?>

                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        <?php echo Form::email('email', (isset($_COOKIE['distim_uid']) && $_COOKIE['distim_uid'] != '') ? Crypt::decrypt($_COOKIE['distim_uid']): '',['class' => 'form-control', 'placeholder' => 'Email Address*','data-parsley-required','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'pattern' => config('app.patterns.email')]); ?>

                                        <p class="help-block"></p>
                                        <?php if($errors->has('email')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('email')); ?>

                                            </p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        <?php echo Form::password('password',['id'=>'password','class' => 'form-control', 'placeholder' => 'Password*','data-parsley-required','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.password_min'),'data-parsley-maxlength' => config('app.fields_length.password_max')]); ?>

                                        <p class="help-block"></p>
                                        <?php if($errors->has('password')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('password')); ?>

                                            </p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 p-l0 checkbox-rember f-s14">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <div class="input-group">
                                                    <div class="checkbox">
                                                        <label class="forgot">
                                                            <?php echo e(Form::checkbox('remember_me', 1, null, ['id'=>'login-remember',(isset($_COOKIE['distim_uid']) && $_COOKIE['distim_uid'] != '') ? "checked": ''])); ?> Remember me</label>
                                                            <div class="help-block"></div>
                                                            <?php if($errors->has('remember_me')): ?>
                                                                <div class="help-block">
                                                                    <?php echo e($errors->first('remember_me')); ?>

                                                                </div>
                                                            <?php endif; ?>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-5 text-right">
                                                <a href="<?php echo e(URL::to('forget-password')); ?>" class="forgot">Forgot Password?</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-md-11 p-l0 ">
                                        <div class="regis-caption login-caption">
                                            <p>By logging in you agree to DistiMonster <a target="_blank" href="<?php echo e(url('terms-of-use')); ?>">Privacy Policy and Terms of Use</a>.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-11 p-l0">
                                        <input type="hidden" name="redirect_uri" value="<?php echo e($redirectLogin); ?>"/>
                                        <?php echo Form::submit('Login', ['class' => 'btn btn-primary','title'=>'Login']); ?>

                                    </div>
                                </div>
                            <?php echo Form::close(); ?>

                                <!-- /form -->
                            </div>
                            <div class="col-md-6">
                                <div class="left-pad-content">
                                    <h2 class="block-title">Don't have an account?</h2>
                                    <p>Register for a DistiMonster account and get a 30 day FREE TRIAL. DistiMonster is your single source to search and buy parts. Use one PO to purchase all your components from multiple distributers. You asked for simple—that’s DistiMonster.</p>
                                    <p>
                                        Want more information? We would be happy to schedule a DistiMonster demonstration for you or your company. Contact us at 818-857-5788 or submit a Request for <a href="<?php echo e(url('request-a-demo')); ?>">Demo</a>.
                                    </p>
                                    <div class="form-group">
                                        <div class="col-md-11 p-l0">
                                            <a href="<?php echo e(url('subscription-plan')); ?>" class="btn btn-success free-trial" title="REGISTER & START YOUR FREE TRIAL">REGISTER & START YOUR FREE TRIAL</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Register Section-->

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script>
    $('#register_form').parsley();
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>