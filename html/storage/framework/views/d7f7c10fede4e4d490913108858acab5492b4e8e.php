
<?php $__env->startSection('title', '| News & Update'); ?>
<?php $__env->startSection('content'); ?>

<!--START: Cart Page-->
<section class="gray-bg news-update-content">
 <div class="container clearfix">
  <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><span>News and Updates</span></li>
                    </ul>
                </div>
            </div>
<div class="row">
 <div class="col-md-12">
  <div class="card-columns">
      <?php if(!$news->isEmpty()): ?>
           <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
               <div <?php if($new->image): ?> class="card" <?php else: ?> class="card p-0" <?php endif; ?> >
                 <?php if($new->image): ?>
                  <a href="<?php echo e(url('news/'.$new->id)); ?>" title="News Logo"><img height="260" width="375" src="<?php echo e(url('/uploads/'.$new->image)); ?>" alt="News Logo" /></a>
                 <?php else: ?>
                    <a href="<?php echo e(url('news/'.$new->id)); ?>" title="News Logo"><img  width="80%" src="<?php echo e(url('frontend/images/logo.png')); ?>" alt="News Logo"/></a>
                 <?php endif; ?>
                 <div class="card-block">
                   <a href="<?php echo e(url('news/'.$new->id)); ?>"><h4 class="card-title"><?php echo e($new->title); ?></h4></a>
                   <p class="card-text"><?php echo implode(' ', array_slice(explode(' ', $new->description), 0, config('constants.NEWS_CHARACTERS'))); ?></p>
                   <?php if(str_word_count($new->description) > config('constants.NEWS_CHARACTERS')): ?>
                     <a href="<?php echo e(url('news/'.$new->id)); ?>" class="read-more">Read More</a>
                  <?php endif; ?>
                 </div>
               </div>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

          <?php else: ?>
          <p>No News is published from <?php echo e(config('app.name')); ?></p>
        <?php endif; ?>
 </div>
</div>
</div>

<div class="pagination-list text-right clearfix">
  <?php echo e($news->appends(request()->except('page'))->links('frontend.partials.pagination')); ?>

</div>


</div>

</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>