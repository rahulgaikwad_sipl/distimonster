
<?php $__env->startSection('title', '| News | '.$getNewsDetailsById['title']); ?>
<?php $__env->startSection('content'); ?>

<!--START: Cart Page-->
<section class="gray-bg news-update-content">
 <div class="container clearfix">
  <div class="row clearfix">
   <div class="col-md-9">
    <h2 class="block-title"><?php echo e($getNewsDetailsById['title']); ?></h2>
  </div>
</div>
<div class="shdow-box my-quote-detail news-detail">
    <div class="row">
      <?php if($getNewsDetailsById['image'] != ''): ?>
        <div class="col-md-4">
          <img width="100%" src="<?php echo e(url('/uploads/'.$getNewsDetailsById['image'])); ?>" alt="News Logo" />
        </div>
          <?php else: ?>
            <div class="col-md-4">
                <img src="<?php echo e(url('frontend/images/logo.png')); ?>" alt="News Logo" />
            </div>
        <?php endif; ?>
        <div class="col-md-8">
            <div class="news-update-content">
              <p><?php echo $getNewsDetailsById['description']; ?></p>
            </div>
        </div>
    </div>
</div>
</div>
</section>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>