
<?php $__env->startSection('title', '| Contact'); ?>
<?php $__env->startSection('content'); ?>

    <!--START: Contact Page-->
    <section class="gray-bg top-pad-with-bradcrumb">
         <div class="container clearfix">
            <div class="row pagination">
               <div class="col-md-12">
                  <ul class="breadcrumb f-s14 text-gray p-l0">
                     <li class="breadcrumb-item"><a href="<?php echo e(url('')); ?>">Home</a></li>
                     <li class="breadcrumb-item active"><span>Contact</span></li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="tabs_wrapper faq-block">
                     <?php echo $__env->make('frontend.partials.help', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     <div class="tab_container" style="min-height: 191px;">
                       <a href="<?php echo e(url('faq')); ?>">  <h3 class="tab_drawer_heading" rel="tab1">FAQs</h3></a>
                        <a href="<?php echo e(url('return-policy')); ?>">  <h3 class="tab_drawer_heading" rel="tab1">Return Policy</h3></a>
                        <h3 class="tab_drawer_heading d_active" rel="tab3">Contact</h3>
                        <div id="tab3" class="tab_content order-history contact-us text-left" style="display: block;">
                           <h2 class="block-title left-block-title text-left">Contact</h2>
                           <hr>
                           <div class="row email-phone">
                              <div class="col-md-6">
                                 <h3 class="text-left black">Email Us</h3>
                                 <ul class="f-s16">
                                    <li class="text-gray">General Info: <a href="mailto:sales@distimonster.com">info@distimonster.com</a></li>
                                    <li class="text-gray">Sales and New Orders: <a href="mailto:sales@distimonster.com">sales@distimonster.com</a></li>
                                    <li class="text-gray">Suppliers: <a href="javascript:void(0)">supply@distimonster.com</a></li>
                                 </ul>
                              </div>
                              <div class="col-md-6">
                                 <h3 class="text-left black">Call Us</h3>
                                 <ul class="f-s16">
                                    <li class="text-gray">In the U.S.: <a href="tel:+1 818 857 5788">+1 818 857 5788</a></li>
                                    <li class="text-gray">International: <a href="tel:+1 818 857 5788">+1 818 857 5788</a></li>
                                    <li class="text-gray">FAX: <a href="tel:+1 818 857 5788">+1 818 857 5788</a></li>
                                 </ul>
                              </div>
                           </div>
                           <div class="row email-phone">
                              <div class="col-md-6">
                                 <h3 class="text-left black">Company Address</h3>
                                 <ul class="f-s16">
                                    <li class="text-gray">DistiMonster.com<br>19821 Nordhoff Place<br>
                                       Ste 114<br>
                                       Chatsworth, CA 91311
                                    </li>
                                 </ul>
                              </div>
                              <div class="col-md-6">
                                 <h3 class="text-left black">Get Live Help</h3>
                                 <ul class="f-s16">
                                    <li><a href="javascript:void(0)" class="f-s16 hind-font text-gray font-w500 chat_link"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/chat-icon.png')); ?>" alt="view-icon"></span> Chat Now</a></li>
                                 </ul>
                              </div>
                           </div>
                           <div class="row email-phone">
                              <div class="col-md-12 p-b30">
                                 <div>
                                    <div id="office_location_map">

                                    </div>

                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- #tab3 End-->
                     </div>
                     <!-- .tab_container -->
                  </div>
                  <div class="shdow-box">
                  </div>
               </div>
            </div>
         </div>
      </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
   <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNibHtOFv3-YKJOco-Zta63ZPiS7T1CEw&callback=initMap"></script>
   <script>
      $('.chat_link').click(function () {
          MyLiveChat.InlineChatDoExpand();
          //$('.mylivechat_sprite').trigger('click');
      });
   </script>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>