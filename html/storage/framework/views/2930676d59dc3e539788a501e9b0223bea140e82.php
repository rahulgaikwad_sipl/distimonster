<?php $__env->startSection('content'); ?>
    <div class="login-logo">
        <a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(url('frontend/images/logo.png')); ?>"></a>
    </div>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo e(ucfirst(config('app.name'))); ?> <?php echo app('translator')->getFromJson('quickadmin.qa_login'); ?></div>
                <div class="panel-body">
                    <?php if(count($errors) > 0): ?>
                        <div class="alert alert-danger">
                            <strong><?php echo app('translator')->getFromJson('quickadmin.qa_whoops'); ?></strong> <?php echo app('translator')->getFromJson('quickadmin.qa_there_were_problems_with_input'); ?>:
                            <br><br>
                            <ul>
                                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><?php echo e($error); ?></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                        <?php echo Form::open(['class'=>"form-horizontal" ,'role'=>'form', 'method' => 'POST', 'url' => url('admin/login'), 'enctype' =>"multipart/form-data", 'data-parsley-validate' ]); ?>

                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Email*</label>
                            <div class="col-md-6">
                                <?php echo Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email', 'data-parsley-required' => 'true','data-parsley-required-message' => 'Email is required']); ?>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Password*</label>
                            <div class="col-md-6">
                                <?php echo Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password', 'data-parsley-required' => true,'data-parsley-required-message' => 'Password is required']); ?>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-4"></div>
                              <div class="col-md-3">
                                <label><input type="checkbox" name="remember"> <?php echo app('translator')->getFromJson('quickadmin.qa_remember_me'); ?></label>
                            </div>
                            <div class="col-md-3">
                                <a href="<?php echo e(url('admin/password/reset')); ?>">Forgot your password?</a>
                            </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-8">
                                </div>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-success">Login</button>
                                </div>

                            </div>

                        </div>
                        <?php echo Form::close(); ?>

                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.auth', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>