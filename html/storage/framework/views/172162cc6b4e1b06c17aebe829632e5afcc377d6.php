<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo e(url('/admin/home')); ?>" class="logo"
       style="font-size: 16px;">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
           <?php echo app('translator')->getFromJson('quickadmin.quickadmin_title'); ?></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
           <?php echo app('translator')->getFromJson('quickadmin.quickadmin_title'); ?></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="hidden-xs"><?php echo ucfirst (Auth::user()->name) ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo e(Auth::user()->profile_pic ? url(config('app.resource_paths.profile_path').Auth::user()->profile_pic) : config('app.resource_paths.default_image')); ?>" class="user-image" alt="User Image" width="100" height="100">
                           <p> <?php echo ucfirst (Auth::user()->name) ?></p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo e(url('admin/profile')); ?>" class="btn btn-success  btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="#logout" onclick="$('#logout').submit();" title='Sign  out' class="btn btn-flat btn-success btn-close">Logout</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

        

    </nav>
</header>



