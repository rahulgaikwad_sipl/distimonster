<!--Navigation-->
<div class="custom-nav custom-nav-header">
    <div class="container">
        <nav class="navbar navbar-toggleable-md navbar-inverse p-l0">
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo e(url('what-is-distimonster')); ?>">WHAT IS DISTIMONSTER</a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?php echo e(url('products')); ?>">PRODUCTS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(url('bom')); ?>">MONSTER-BOM</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(url('return-policy')); ?>">SUPPORT</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(url('about')); ?>">ABOUT</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(url('contact')); ?>">Contact</a>
                    </li>
<!--                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo e(url('news-updates')); ?>">News & Updates</a>
                    </li>-->
                    
                    <?php if(Auth::check()): ?>
                    <li class="nav-item">
   <div class="dropdown">
      <a class="nav-link dropbtn" href="javascript:void(0)" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">MY ACCOUNT
      <i class="fa fa-caret-down"></i>
      </a>
      <div class="dropdown-content">
            <a class="nav-link" href="<?php echo e(url('dashboard/')); ?>">Dashboard</a>
      <a class="nav-link" href="<?php echo e(url('account-settings/'.Auth::user()->id)); ?>">Account Settings</a>
      <a class="nav-link" href="<?php echo e(url('order-history')); ?>">Order History</a>
      <a class="nav-link" href="<?php echo e(url('quotes')); ?>">Quotes</a>
      <a class="nav-link" href="<?php echo e(url('my-bom')); ?>">Monster-BOM</a>
      <a class="nav-link" href="<?php echo e(url('my-bio')); ?>">My BIO</a>
      <?php if(Auth::user()->is_sub_user == 0): ?>
        <a class="nav-link" href="<?php echo e(url('my-team')); ?>">My Team</a>
      <?php endif; ?>  
      
      <?php /*<a class="nav-link" href="{{url('apply-net-term-account')}}">Apply for a Net Term Account</a>*/?>
      </div>
   </div>
</li>
                    <?php endif; ?>
                </ul>

            </div>
        </nav>

        <div class="navbar-collapse collapse"  id="navbar">
        <div class="row">
            <div class="col-md-9 col-sm-10">
                <ul class="nav navbar-nav">
                    
                    <li><a href="<?php echo e(url('about')); ?>">About</a></li>
                    <li><a  href="<?php echo e(url('news-updates')); ?>">News &amp; Updates</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Support <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo e(url('faq')); ?>">FAQs</a></li>
                            <li><a href="<?php echo e(url('return-policy')); ?>">Return Policy</a></li>
                            <li><a href="<?php echo e(url('contact')); ?>">Contact</a></li>
                        </ul>
                    </li>
                    <li><a href="<?php echo e(url('bom')); ?>">BOM</a></li>
                    <?php if(Auth::check()): ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo e(url('account-settings/'.Auth::user()->id)); ?>">Account Settings</a></li>
                            <li> <a href="<?php echo e(url('order-history')); ?>">Order History</a></li>
                            <li><a href="<?php echo e(url('my-quotes')); ?>">My Quotes</a></li>
                            <li><a href="<?php echo e(url('my-bom')); ?>">MonsterBOM</a></li>
                            <li><a href="<?php echo e(url('my-bio')); ?>">My BIO</a></li>
                            <?php if(Auth::user()->is_sub_user == 0): ?>
                                <li><a href="<?php echo e(url('my-team')); ?>">My Team</a></li>
                            <?php endif; ?>    
                            
                            <?php /* <li> <a href="{{url('apply-net-term-account')}}">Apply for a Net Term Account</a></li>*/ ?>
                        </ul>
                    </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>
</div>
<!--Navigation end-->