<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('role_view')): ?>
    <a href="<?php echo e(route('admin.news.show',[$news->id])); ?>" class="btn btn-xs btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>
<?php endif; ?>
<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('role_edit')): ?>
    <a href="<?php echo e(route('admin.news.edit',[$news->id])); ?>" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
<?php endif; ?>
<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('role_delete')): ?>
    <button class="red fl btn btn-xs btn-danger" onclick="userModule.confirmNewsDelete('<?php echo $news->id; ?>','Are you sure you want to delete this news?')" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button>
<?php endif; ?>