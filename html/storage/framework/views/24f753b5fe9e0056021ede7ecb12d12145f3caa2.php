
<?php $__env->startSection('title', '| '.$title); ?>
<?php $__env->startSection('content'); ?>
    <section class="gray-bg top-pad-with-bradcrumb">
        <div class="container-fluid clearfix container-w-80 user-admin">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active"><span><?php echo e($title); ?></span></li>

                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
 <?php echo $__env->make('frontend.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                          
					   <div class="tab_container">
                            <h3 class="tab_drawer_heading" rel="tab5">Tab 5</h3>
                            <div id="tab5" class="tab_content">
                                <h2 class="block-title left-block-title"><?php echo e($title); ?></h2>
                                <?php if(count($bomList)>0): ?>
                                    <?php $__currentLoopData = $bomList->chunk(2); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $items): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <div class="row ">
                                            <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="col-md-6">
                                                    <div class="shdow-box my-quote-detail">
                                                        <h3 class="f-s18"><a href="<?php echo e(url($item->id.'/bom-details')); ?>" onclick="show_loader()">
                                                        <?php if(!empty($item->bom_name)): ?>    
                                                            <?php echo e($item->bom_name); ?>

                                                        <?php else: ?>
                                                            BOM - <?php echo e($item->id); ?>

                                                        <?php endif; ?>
                                                        </a></h3>
                                                        <div class="f-s14 text-gray btm-space">Total Parts: <span class="black font-w500"><?php echo e($item->bom_parts_count); ?></span></div>
                                                        <div class="f-s14 text-gray btm-space">Last modified: <span class="black font-w500"><?php echo e(date('m/d/Y', strtotime($item->updated_at))); ?></span></div>
                                                        <div class="my-quote-detail-footer ">
                                                            <ul class="list-inline m-b0">
                                                                <li class="list-inline-item">
                                                                    <a href="<?php echo e(url('download-bom/'.$item->id)); ?>" class="f-s16 font-w500 hind-font"><span class="img-icon"><i class="fa fa-download"></i></span>Download</a>
                                                                </li>
                                                                <li class="list-inline-item">
                                                                    <a href="javascript:void(0)" onclick="bom.removeBomItem('<?php  echo $item->id;?>')" class="f-s16 font-w500 hind-font"><span class="img-icon"><i class="fa fa-trash"></i></span>Remove</a>
                                                                </li>
                                                                
                                                                <li class="list-inline-item" style="float: right;">
                                                                    <a href="#" data-target=".share_modal_<?php echo e($item->id); ?>" data-toggle="modal" type="button"><i class="fa fa-share"></i> Share</a>
                                                                </li>
                                                                
                                                                <li class="list-inline-item" style="float: right; padding: 0px 7px;">
                                                                    <a href="javascript:void(0)" bom-id="<?php echo e($item->id); ?>" class="notes_modal" type="button"><i class="fa fa-file-text"></i> Notes   <?php if(!empty($item->unread)): ?> <span class="badge badge-notify"><?php echo e($item->unread); ?></span><?php endif; ?></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                            <div aria-labelledby="modalLabel" class="modal share_modal_<?php echo e($item->id); ?>" role="dialog" tabindex="-1" style="z-index: 1050;">
                                                                <div class="modal-dialog" role="document">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                            <div class="modal-title">
                                                                                <h4 class="m-b0">Share BOM</h4>
                                                                            </div>
                                                                            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"><img src="<?php echo e(url('frontend/assets/images/close-icon.png')); ?>" alt="close-icon"></span></button>
                                                                        </div>
                                                                        <form method="POST" name="bom_share_frm" class="share_frm_<?php echo e($item->id); ?>" id="bom_share_frm" data-parsley-validate="">
                                                                        <div class="modal-body">
                                                                            <div class="row p-b5">
                                                                                <div class="col-md-12">
                                                                                    <div class="my-bom-checklist">
                                                                                <?php 
                                                                                $shared_exploded_user = 0;
                                                                                if(!empty($item->shared_user_id)){
                                                                                    $shared_exploded_user = explode(',',$item->shared_user_id);
                                                                                }
                                                                                $i=0;
                                                                                ?>
                                                                                 <div class="row">
                                                                                <?php $__currentLoopData = $subUser; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                               
                                                                                    <div class="form-check form-check-inline col-md-5">
                                                                                            <?php $check = ''; ?>
                                                                                            <?php if(is_array($shared_exploded_user) && in_array($sub->id, $shared_exploded_user)): ?> 
                                                                                            <?php $check = 'checked="checked"'; ?>
                                                                                            <?php endif; ?>
                                                                                            <input type="checkbox" required="" name="user_name[]" class="form-check-input" id="user_name" <?php echo e($check); ?> value="<?php echo e($sub->id); ?>" data-parsley-required-message="Please select atleast one user to share BOM."/><label class="form-check-label" for="inlineCheckbox1"><?php echo e($sub->name); ?> <?php echo e($sub->last_name); ?></label>
                                                                                    </div>
                                                                               
                                                                                    <input type="hidden" name="name_hidden[]" value="<?php echo e($sub->name); ?> <?php echo e($sub->last_name); ?>"/>
                                                                                    <input type="hidden" name="email_hidden[]" value="<?php echo e($sub->email); ?>"/>
                                                                                <?php $i++;?>
                                                                                
                                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                </div>
                                                                                </div>
                                                                                <input type="hidden" name="bom_id_hidden" value="<?php echo e($item->id); ?>"/>
                                                                        </div>
                                                                        <div class="col-md-12"><textarea name="bom_note" maxlength="500" class="bom_note form-control bdr-radius100"></textarea></div>
                                                                        </div>
                                                                        
                                                                        </div>
                                                                        
                                                                        <div class="modal-footer p-t0">
                                                                            <button type="button" onClick="shareBOM('share_frm_<?php echo e($item->id); ?>')" class="btn btn-primary">Share Now</button>
                                                                        </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <div aria-labelledby="modalLabel" id="notes_modals" class="modal" role="dialog" tabindex="-1" style="z-index: 1050;">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <div class="modal-title">
                                                        <h4 class="m-b0">Share History Notes</h4>
                                                    </div>
                                                    <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"><img src="<?php echo e(url('frontend/assets/images/close-icon.png')); ?>" alt="close-icon"></span></button>
                                                </div>
                                                <div class="modal-body">
                                                <div id="contents">                                     
                                                </div>
                                            </div>
                                            <div class="modal-footer p-t0"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div aria-labelledby="myLargeModalLabel" id="notes_modal" class="modal fade" role="dialog" tabindex="-1">
                                            <div class="modal-dialog modal-lg modal-custom" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header bdr-none">
                                                        <div class="modal-title">
                                                            <h4>Share History Notes</h4>
                                                        </div>
                                                        <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"><img src="<?php echo e(url('frontend/assets/images/close-icon.png')); ?>" alt="close-icon"></span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <hr>
                                                        <div id="content">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    <div class="pagination-list text-right clearfix">
                                        <?php echo e($bomList->appends(request()->except('page'))->links('frontend.partials.pagination')); ?>

                                    </div>
                                <?php else: ?>
                                    <div class="alert alert-info">
                                        <?php echo e($noBomMsg); ?>

                                    </div>
                                <?php endif; ?>
                            </div>

                        </div>
                        <!-- .tab_container -->
                    </div>
                    <div class="shdow-box">
                    </div>
                </div>
            </div>
        </div>
    </section>
	 <style>
	/* CSS used here will be applied after bootstrap.css */
.badge-notify{
   background:red;
   position:relative;
 }
  </style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>
        function show_loader() {
            $(".loader").css("display", 'block');
            $("#loader_note_message").css("display", 'block');
        }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>