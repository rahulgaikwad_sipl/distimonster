<?php $__env->startSection('content'); ?>
    <tr>
        <td align="center" valign="top" style="padding:0px;"><!--main section start-->
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:650px;max-width:100%;">
                <tr>
                    <td align="left" valign="top" bgcolor="#f5f5f5"><!--content section-->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:100%;">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                        <tr>
                                            <td align="left" valign="top" style="font:Bold 14px Arial, Helvetica, sans-serif; color:#666;padding:0px;background:#fff;padding:25px 25px;">
                                                <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                                    <tr><td style="height:25px;">&nbsp;</td></tr><!--spacing-->
                                                    <tr>
                                                        <td>
                                                            <table style="border:0 none;border-spacing:0;font-family:'Poppins', Arial, sans-serif;color:#000;font-weight:normal;text-align:left;" align="center" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" style="font-size:16px;color:#333;font-weight:normal;line-height:23px;padding-bottom:10px;">Hello <?php echo e($firstName); ?> <?php echo e($lastName); ?>, </td>
                                                                </tr>
                                                                <tr>
                                                                    <?php if($paymentStatus == 0): ?>
                                                                        <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Your Order# <?php echo e($orderId); ?> payment status is changed to <?php echo e(config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.pending')); ?>.If you have any question, Please
                                                                            <a href="<?php echo e(url('contact-us')); ?>"> Click Here</a> to contact with us.</td>
                                                                    <?php elseif($paymentStatus == 1): ?>
                                                                        <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Your Order# <?php echo e($orderId); ?> payment status is changed to <?php echo e(config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.done')); ?>. </td>
                                                                    <?php elseif($paymentStatus == 2): ?>
                                                                        <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Your Order# <?php echo e($orderId); ?> payment status is changed to <?php echo e(config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.failed')); ?>.If you have any question, Please
                                                                            <a href="<?php echo e(url('contact-us')); ?>"> Click Here</a> to contact with us. </td>
                                                                    <?php elseif($paymentStatus == 3): ?>
                                                                        <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Your Order# <?php echo e($orderId); ?> payment status is changed to <?php echo e(config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.refunded')); ?>.If you have any question, Please
                                                                            <a href="<?php echo e(url('contact-us')); ?>"> Click Here</a> to contact with us. </td>
                                                                    <?php endif; ?>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr><!--//single list-->
                                                    <!--//single list-->
                                                    <tr><td style="border-left:1px solid #f5f5f5;border-right:1px solid #f5f5f5;height:25px;">&nbsp;</td></tr><!--spacing-->
                                                </table>

                                            </td>
                                        </tr>
                                        <!--footer content-->
                                    </table><!--// center content-->
                                </td>
                            </tr>
                        </table><!--content section end-->
                    </td>
                </tr><!-- // main tr-->
            </table>
        </td><!-- //main section start-->
    </tr><!--//main wrap tr-->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('emails.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>