
<?php $__env->startSection('title', '|  Thank You'); ?>
<?php $__env->startSection('content'); ?>
    <section class="gray-bg shipping-cart shipping-addres complete-order">
        <div class="container clearfix">

            <div class="row clearfix text-center">
                <div class="col-md-12">
                    <div class="complete-big-icon"><img src="<?php echo e(url('frontend/assets/images/complete-big-icon.png')); ?>" alt="complete-big-icon" ></div>
                    <h2 class="block-title left-block-title">Thank you for the order</h2>
                </div>
                <div class="col-md-12">
                    <p class="f-s14">Your order number is <?php echo e($orderId); ?>.</p>
                    <?php if(!empty($txNumber) && $txNumber!== 'cod'): ?>
                    <p class="f-s14">Your Payment Transaction number is <b><?php echo e($txNumber); ?></b>.</p>
                    <?php endif; ?>
                    <p class="f-s14">We’ve sent a confirmation email with order details and a link to track it’s progress.</p>
                    <a href="<?php echo e(url('/')); ?>" class="btn btn-primary m-t15" title="Continue Shopping">Continue Shopping</a>
                </div>
            </div>

        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $(document).on('ready', function() {
            window.history.pushState(null, "", window.location.href);
            window.onpopstate = function() {
                window.history.pushState(null, "", window.location.href);
            };
        });
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>