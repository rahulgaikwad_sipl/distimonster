<?php $request = app('Illuminate\Http\Request'); ?>
<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="<?php echo e($request->segment(1) == 'home' ? 'active' : ''); ?>">
                <a href="<?php echo e(url('/admin/home')); ?>">
                    <i class="fa fa-tachometer"></i>
                    <span class="title"><?php echo app('translator')->getFromJson('quickadmin.qa_dashboard'); ?></span>
                </a>
            </li>
            <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user_management_access')): ?>
             <li class="<?php echo e($request->segment(2) == 'users' ? 'active' : ''); ?>">
                <a href="<?php echo e(route('admin.users.index')); ?>">
                    <i class="fa fa-users"></i>
                    <span class="title"><?php echo app('translator')->getFromJson('quickadmin.users.title'); ?></span>
                </a>
            </li>

            <?php endif; ?>
            <li class="<?php echo e($request->segment(2) == 'net-account-requests' ? 'active' : ''); ?>">
                <a href="<?php echo e(url('admin/net-account-requests')); ?>">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="title">Net Account Requests</span>
                </a>
            </li>

            <li class="<?php echo e($request->segment(2) == 'orders' ? 'active' : ''); ?>">
                <a href="<?php echo e(url('admin/orders')); ?>">
                    <i class="fa fa-shopping-cart"></i>
                    <span class="title">Orders</span>
                </a>
            </li>


            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span class="title">Quotes</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo e($request->segment(2) == 'quotes' ? 'active active-sub' : ''); ?>">
                        <a href="<?php echo e(url('admin/quotes')); ?>">
                            <i class="fa fa-clipboard"></i>
                            <span class="title">Quotes</span>
                        </a>
                    </li>
                    <li class="<?php echo e($request->segment(2) == 'requested-lead-times' ? 'active active-sub' : ''); ?>">
                        <a href="<?php echo e(url('admin/requested-lead-times')); ?>">
                            <i class="fa fa-clipboard"></i>
                            <span class="title">Requested Lead Times</span>
                        </a>
                    </li>
                </ul>
            </li>


            <li class="<?php echo e($request->segment(2) == 'news' ? 'active' : ''); ?>">
                <a href="<?php echo e(url('admin/news')); ?>">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">News</span>
                </a>
            </li>
            <li class="<?php echo e($request->segment(2) == 'coupons' ? 'active' : ''); ?>">
                <a href="<?php echo e(url('admin/coupons')); ?>">
                    <i class="fa fa-clipboard"></i>
                    <span class="title">Promo Code</span>
                </a>
            </li>
            <li class="<?php echo e($request->segment(2) == 'newsletter' ? 'active' : ''); ?>">
                <a href="<?php echo e(url('admin/newsletter')); ?>">
                    <i class="fa fa-newspaper-o"></i>
                    <span class="title">Newsletter</span>
                </a>
            </li>
            <li class="<?php echo e($request->segment(1) == 'change_password' ? 'active' : ''); ?>">
                <a href="<?php echo e(url('admin/change_password')); ?>">
                    <i class="fa fa-key"></i>
                    <span class="title"><?php echo app('translator')->getFromJson('quickadmin.qa_change_password'); ?></span>
                </a>
            </li>

            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title"><?php echo app('translator')->getFromJson('quickadmin.qa_logout'); ?></span>
                </a>
            </li>
        </ul>
    </section>
</aside>
<?php echo Form::open(['route' => 'auth.logout', 'style' => 'display:none;', 'id' => 'logout']); ?>

<button type="submit"><?php echo app('translator')->getFromJson('quickadmin.logout'); ?></button>
<?php echo Form::close(); ?>

