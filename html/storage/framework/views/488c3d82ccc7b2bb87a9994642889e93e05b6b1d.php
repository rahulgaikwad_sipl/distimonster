<meta charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<meta name="cache-control" content="no-cache" />
<meta name="expires" content="0" />
<meta name="pragma" content="no-cache" />
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title><?php echo e(config('app.name')); ?>  <?php echo $__env->yieldContent('title', ''); ?></title>
<script> var BASE_URL = '<?php echo e(url('')); ?>';</script>
<!-- Bootstrap -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Hind:400,500,600,700|Nunito+Sans:400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Hind:400,500,600,700" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet">
<link href="<?php echo e(url('frontend/css/sweetalert.css')); ?>" rel="stylesheet">
<link href="<?php echo e(url('frontend/assets/css/custom-style.css')); ?>" rel="stylesheet">
<link href="<?php echo e(url('frontend/css/listnav.css')); ?>" rel="stylesheet">
<link href="<?php echo e(url('frontend/css/easy-autocomplete.min.css')); ?>" rel="stylesheet">
<link href="<?php echo e(url('frontend/css/select2.css')); ?>" rel="stylesheet">
<link rel="shortcut icon" type="image/png" href=" <?php echo e(url('frontend/assets/images/favicon.png')); ?> "/>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
