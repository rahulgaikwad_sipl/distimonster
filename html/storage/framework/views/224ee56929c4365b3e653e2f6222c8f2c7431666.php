<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(url('admin/home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="<?php echo e(url('admin/users')); ?>">Users</a></li>
            <?php if(!empty($user)): ?>
                <li class="active">Update User</li>
            <?php else: ?>
                <li class="active">Add User</li>
            <?php endif; ?>

        </ol>
    </section>
    <br/>
    <section class="content">
    <div class="row">
        <div class="col-md-12">

            <div class="box">
                    <div class="box-header with-border">
                        <?php if(!empty($user)): ?>
                            <h3 class="box-title">Update User</h3>
                        <?php else: ?>
                            <h3 class="box-title">Add User</h3>
                        <?php endif; ?>
                    </div>
                    <?php if(!empty($user)): ?>
                        <?php echo Form::model($user, ['method' => 'PUT', 'route' => ['admin.users.update', $user->id],'data-parsley-validate' => true]); ?>

                    <?php else: ?>
                        <?php echo Form::open(['method' => 'POST', 'route' => ['admin.users.store'], 'data-parsley-validate' => true]); ?>

                    <?php endif; ?>
                        <div class="box-body">
                                <div class="form-group">
                                    <label class='control-label'> First Name <span class="text-danger">*</span> </label>
                                    <?php echo Form::text('name', old('name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','minlength' => config('app.fields_length.name_min'),'maxlength' => config('app.fields_length.name_max'),'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE')]); ?>

                                    <p class="help-block"></p>
                                    <?php if($errors->has('name')): ?>
                                        <p class="help-block">
                                            <?php echo e($errors->first('name')); ?>

                                        </p>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label class='control-label'> Last Name <span class="text-danger">*</span> </label>
                                    <?php echo Form::text('last_name', old('last_name'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','minlength' => config('app.fields_length.name_min'),'maxlength' => config('app.fields_length.name_max'),'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE')]); ?>

                                    <p class="help-block"></p>
                                    <?php if($errors->has('last_name')): ?>
                                        <p class="help-block">
                                            <?php echo e($errors->first('last_name')); ?>

                                        </p>
                                    <?php endif; ?>
                                </div>

                                <div class="form-group">
                                    <label class='control-label'> Email <span class="text-danger">*</span> </label>
                                    <input type="text" name="email" id="email" value="<?php echo e($user->email?:old('email')); ?>" placeholder="Email Address" required="" class="form-control" tabindex="6" data-parsley-required-message="Please enter your valid email." pattern="<?php echo e(config('app.patterns.email')); ?>" data-parsley-trigger="focusout" data-parsley-remote="<?php echo e(url('admin/check-newemails')); ?>" data-parsley-remote-message="Email address is already in use."/>
                                    <p class="help-block email_err"></p>
                                    <?php if($errors->has('email')): ?>
                                        <p class="help-block">
                                            <?php echo e($errors->first('email')); ?>

                                        </p>
                                    <?php endif; ?>
                                </div>

                            <?php if(empty($user)): ?>

                                    <div class="form-group">
                                        <label class='control-label'> Password <span class="text-danger">*</span> </label>
                                        <?php echo Form::password('password', ['id'=>'password','class' => 'form-control', 'placeholder' => '', 'required' => '','data-parsley-minlength' => config('app.fields_length.password_min'),'data-parsley-maxlength' => config('app.fields_length.password_max'),'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE')]); ?>

                                        <p class="help-block"></p>
                                        <?php if($errors->has('password')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('password')); ?>

                                            </p>
                                        <?php endif; ?>
                                    </div>

                                    <div class="form-group">
                                        <label class='control-label'> Confirm  Password <span class="text-danger">*</span> </label>
                                        <?php echo Form::password('cpassword', ['class' => 'form-control', 'placeholder' => '', 'required' => '', 'data-parsley-equalto'=>'#password','data-parsley-equalto-message'=>"Confirm Password must equal to password."]); ?>

                                        <p class="help-block"></p>
                                        <?php if($errors->has('cpassword')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('cpassword')); ?>

                                            </p>
                                        <?php endif; ?>
                                    </div>
                            <?php endif; ?>
                            <?php echo Form::hidden('role_id', 2, ['class' => 'form-control', 'placeholder' => '']); ?>


                                <div class="form-group">
                                    <label class='control-label'> Status <span class="text-danger">*</span> </label>
                                    <?php echo Form::select('status', $status, !empty($user->status)? $user->status :old('status'), ['class' => 'form-control select2 status', 'data-parsley-errors-container'=>'#error_status', 'required' => '','data-parsley-required-message'=>'Please select status.']); ?>

                                    <div id="error_status"> </div>
                                    <p class="help-block"></p>
                                    <?php if($errors->has('status')): ?>
                                        <p class="help-block">
                                            <?php echo e($errors->first('status')); ?>

                                        </p>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label class='control-label'> Subscription Plan <span class="text-danger">*</span> </label>
                                    <?php echo Form::select('plan_type', $plans, !empty($user->plan_id)? $user->plan_id : old('plan_type'), ['class' => 'form-control select2', 'data-parsley-errors-container'=>'#error_plan_type', 'required' => '','data-parsley-required-message'=>'Please select subscription plan.']); ?>

                                    <div id="error_plan_type"> </div>
                                    <p class="help-block"></p>
                                    <?php if($errors->has('plan_type')): ?>
                                        <p class="help-block">
                                            <?php echo e($errors->first('plan_type')); ?>

                                        </p>
                                    <?php endif; ?>
                                </div>
                                <div class="form-group">
                                    <label class='control-label'> Billing <span class="text-danger">*</span> </label>
                                    <?php echo Form::select('billing_cycle', $billingCycle, !empty($user->billing_cycle)? $user->billing_cycle : old('billing_cycle'), ['class' => 'form-control select2', 'data-parsley-errors-container'=>'#error_billing_cycle', 'required' => '','data-parsley-required-message'=>'Please select billing.']); ?>

                                    <div id="error_billing_cycle"> </div>
                                    <p class="help-block"></p>
                                    <?php if($errors->has('billing_cycle')): ?>
                                        <p class="help-block">
                                            <?php echo e($errors->first('billing_cycle')); ?>

                                        </p>
                                    <?php endif; ?>
                                </div>
                        </div>
                    <div class="box-footer">
                        <a class="btn btn-default btn-close" href="<?php echo e(route('admin.users.index')); ?>">Cancel</a>
                        <?php echo Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-success']); ?>

                    </div>
                    <?php echo Form::close(); ?>


            </div>
        </div>
    </div>
    </section>

<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>