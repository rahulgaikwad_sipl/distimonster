<?php $__env->startSection('content'); ?>
    <tr>
        <td align="center" valign="top" style="padding:0px;"><!--main section start-->
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:650px;max-width:100%;">
                <tr>
                    <td align="left" valign="top" bgcolor="#f5f5f5"><!--content section-->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:100%;">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                        <tr>
                                            <td align="left" valign="top" style="font:Bold 14px Arial, Helvetica, sans-serif; color:#666;padding:0px;background:#fff;padding:25px 25px;">
                                                <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                                    <tr><td style="height:25px;">&nbsp;</td></tr><!--spacing-->
                                                    <tr>
                                                        <td>
                                                            <table style="border:0 none;border-spacing:0;font-family:'Poppins', Arial, sans-serif;color:#000;font-weight:normal;text-align:left;" align="center" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" style="font-size:16px;color:#333;font-weight:normal;line-height:23px;padding-bottom:10px;">Hello <?php echo e(ucwords($firstName)); ?>, </td>
                                                                </tr>
                                                                <?php if($isSubUser == 0): ?>
                                                                <tr>
                                                                    <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Thanks for subscribing to the DistiMonster platform. You have just made the process of searching and buying components a thousand times easier and quicker. We have a live chat on the site should you need help or you would like to schedule a live demo please email <a href = "mailto: demo@distimonster.com">demo@distimonster.com</a></td>
                                                                </tr>
                                                                <?php endif; ?>
                                                                <tr>
                                                                    <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">
                                                                        <?php if(!empty($isSubUser) && ($isSubUser == 1)): ?>
                                                                            <p>   
                                                                            <span>You are now set up as a team member on DistiMonster, you must log into your account and update your info so you can have full access to your personal dashboard and the shared BOM’s</span>
                                                                            </p><br/>
                                                                            <?php endif; ?>

                                                                        Here are your Login details:<p><strong>Username: </strong><span><?php echo e($emailAddress); ?></span></p><p><strong>Password: </strong> <span><?php echo e($randomPassword); ?></span></p>
                                                                    <?php if(!empty($note)): ?>
                                                                    <p><strong>Note: </strong> <span><?php echo e($note); ?></span><br>
                                                                    </p> 
                                                                    <?php endif; ?>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr><!--//single list-->
                                                    <tr>
                                                        <td>
                                                            <table style="border:0 none;border-spacing:0;font-family:'Poppins', Arial, sans-serif;color:#000;font-weight:normal;text-align:left;width:600px;padding-bottom:40px" align="center" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="center">
                                                                        <a href="<?php echo e(url('/login')); ?>" title="LOGIN" style="color:#0185c6;text-decoration:none" target="_blank">
                                                                            <span style="padding:5px 10px;background:#0E88CA;border-radius:150px;font-size:17px;padding:0.75rem 1.4375rem;text-align:center; color:#fff;">LOGIN</span>


                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr><!--//single list-->
                                                    <tr><td style="border-left:1px solid #f5f5f5;border-right:1px solid #f5f5f5;height:25px;">&nbsp;</td></tr><!--spacing-->
                                                </table>

                                            </td>
                                        </tr>
                                        <!--footer content-->
                                    </table><!--// center content-->
                                </td>
                            </tr>
                        </table><!--content section end-->
                    </td>
                </tr><!-- // main tr-->
            </table>
        </td><!-- //main section start-->
    </tr><!--//main wrap tr-->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('emails.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>