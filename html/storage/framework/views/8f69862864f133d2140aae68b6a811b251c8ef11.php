
<?php $__env->startSection('title', '| My BIO'); ?>
<?php $__env->startSection('content'); ?>
    <section class="gray-bg top-pad-with-bradcrumb">
        <div class="container-fluid clearfix container-w-80 user-admin">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active"><span>My BIO</span></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
                     <?php echo $__env->make('frontend.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     	<div class="tab_container">
                            <h3 class="tab_drawer_heading" rel="tab2">My BIO</h3>
                            <div id="tab2" class="tab_content">
                                <h2 class="block-title left-block-title">My BIO</h2>
                                <div class="shdow-box edit-account-setting">
                                    <div class="edit-account-setting-content">
                                        <div class="col-md-12">
                                            <h3>BIO Information</h3>
                                        </div>
                                        <?php echo Form::model($user, ['url' => url('my-bio/'), 'method' => 'POST','files' => true,'id' => 'my_bio_form', 'data-parsley-validate' => true]); ?>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php $areYouA = config('constants.ARE_YOU_A');?>
                                                <select class="form-control" name="are_you_a" id="are_you_a" required="" data-parsley-required-message="<?php echo e(config('constants.ARE_YOU_A_ERROR')); ?>">
                                                    <option value="">Select Are You A</option>
                                                    <?php $__currentLoopData = $areYouA; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($key); ?>" <?php if($user->are_you_a==$key): ?> selected="selected" <?php endif; ?>><?php echo e($value); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                                <p class="select1" <?php if($user->are_you_a!='Other'): ?>  style="display:none;" <?php endif; ?>><input type="text" name="are_you_a_text" id="are_you_a_text" class="form-control" value="<?php echo e($user->are_you_a_text); ?>" placeholder="Are you a" data-parsley-validate-if-empty data-parsley-conditionalvalue='["[name=\"are_you_a\"] option:selected", "Other"]' maxlength="100"/></p>
                                                <p class="help-block"></p>
                                                <?php if($errors->has('are_you_a')): ?>
                                                    <p class="help-block">
                                                        <?php echo e($errors->first('are_you_a')); ?>

                                                    </p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php $yourCompanyA = config('constants.YOUR_COMPANY_A');?>
                                                <select class="form-control" name="your_company_a" id="your_company_a" required="" data-parsley-required-message="<?php echo e(config('constants.YOUR_COMPANY_A_ERROR')); ?>">
                                                    <option value="">Select Your Company A</option>
                                                    <?php $__currentLoopData = $yourCompanyA; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($key); ?>" <?php if($user->your_company_a==$key): ?> selected="selected" <?php endif; ?>><?php echo e($value); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>    
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <p class="select2" <?php if($user->your_company_a!='Other'): ?>  style="display:none;" <?php endif; ?>><input type="text" name="your_company_a_text" id="your_company_a_text" class="form-control" value="<?php echo e($user->your_company_a_text); ?>" placeholder="Your company a" data-parsley-validate-if-empty data-parsley-conditionalvalue='["[name=\"your_company_a\"] option:selected", "Other"]' maxlength="100"/></p>
                                                <p class="help-block"></p>
                                                <?php if($errors->has('your_company_a')): ?>
                                                    <p class="help-block">
                                                        <?php echo e($errors->first('your_company_a')); ?>

                                                    </p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php $yourMainIndustry = config('constants.YOUR_MAIN_INDUSTRY');?>
                                                <select class="form-control" name="your_main_industry" id="your_main_industry" required="" data-parsley-required-message="<?php echo e(config('constants.YOUR_MAIN_INDUSTRY_ERROR')); ?>">
                                                    <option value="">Select Your Main Industry</option>
                                                    <?php $__currentLoopData = $yourMainIndustry; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <option value="<?php echo e($key); ?>" <?php if($user->your_main_industry==$key): ?> selected="selected" <?php endif; ?>><?php echo e($value); ?></option>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </select>
                                            </div>
                                        </div>    
                                        <div class="form-group">
                                            <div class="col-md-12">    
                                                <p class="select3" <?php if($user->your_main_industry!='Other'): ?>  style="display:none;" <?php endif; ?>><input type="text" name="your_main_industry_text" id="your_main_industry_text" class="form-control" value="<?php echo e($user->your_main_industry_text); ?>" placeholder="What is your main Industry." data-parsley-validate-if-empty data-parsley-conditionalvalue='["[name=\"your_main_industry\"] option:selected", "Other"]' maxlength="100"/></p>
                                                <p class="help-block"></p>
                                                <?php if($errors->has('your_main_industry')): ?>
                                                    <p class="help-block">
                                                        <?php echo e($errors->first('your_main_industry')); ?>

                                                    </p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-group m-t25">
                                            <div class="col-md-12">
                                                <input type="hidden" name="user_id" value="<?php echo e($user->id); ?>"/>
                                                <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

                                            </div>
                                        </div>
                                        <?php echo Form::close(); ?>

                                        <!-- /form -->
                                    </div>
                                </div>
                            </div>
                            <!-- #tab2 End-->
                        </div>
                        <!-- .tab_container -->
                    </div>
                    <div class="shdow-box">
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script>
window.ParsleyValidator.addValidator('conditionalvalue', function (value, requirements) {
        if (requirements[1] == $(requirements[0]).val() && '' == value)
            return false;
        return true;
    }, 32).addMessage('en', 'conditionalvalue', 'This field is required.');

$('#my_bio_form').parsley();
$(document).ready(function(){
    /*Are you a*/
    $("#are_you_a").change(function(){
        var areYouA = $(this).val();
        if(areYouA=="Other"){
            $(".select1").show();
        }else{
            $(".select1").hide();
        }
    });
    
    /*Your company a*/
    $("#your_company_a").change(function(){
        var areYouA = $(this).val();
        if(areYouA=="Other"){
            $(".select2").show();
        }else{
            $(".select2").hide();
        }
    });
    
    /*Your main industry*/
    $("#your_main_industry").change(function(){
        var areYouA = $(this).val();
        if(areYouA=="Other"){
            $(".select3").show();
        }else{
            $(".select3").hide();
        }
    });
});
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>