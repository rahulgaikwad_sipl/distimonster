
<?php $__env->startSection('title', '| Return Policy'); ?>
<?php $__env->startSection('content'); ?>

    <!--START: Cart Page-->
 <section class="gray-bg top-pad-with-bradcrumb">
         <div class="container clearfix">
            <div class="row pagination">
               <div class="col-md-12">
                  <ul class="breadcrumb f-s14 text-gray p-l0">
                     <li class="breadcrumb-item"><a href="#">Support</a></li>
                     <li class="breadcrumb-item active"><span>Return Policy</span></li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="tabs_wrapper faq-block">
                      <?php echo $__env->make('frontend.partials.help', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     <div class="tab_container" style="min-height: 191px;">
                        <a href="<?php echo e(url('faq')); ?>">  <h3 class="tab_drawer_heading" rel="tab1">FAQs</h3></a>

                        <!-- #tab1 End-->
                        <h3 class="tab_drawer_heading" rel="tab2">Return Policy</h3>
                        <div id="tab2" class="tab_content" style="display: none;">
                           <h2 class="block-title left-block-title">Return Policy</h2>
                           <div class="shdow-box edit-account-setting">
                              <div class="edit-account-setting-content f-s14 text-gray">
                                 <p>All product is considered accepted by the customer unless the customer notifies DistiMonster in writing
                                    within 10 days of receipt of the product for any shortages, damages or defects. No returns can be made
                                    without a RMA # (Return Merchandise Authorization) issued by DistiMonster. Any shipments made
                                    without an RMA # issued by DistiMonster are not the liability of DistiMonster and the customer is still
                                    liable for all product and costs associated with the shipment.</p>
                                 <p>All requests for an RMA # should be sent in to returns@distimonster.com with a copy of the packing slip
                                    and notes of the claim or you can also log into your account and request on your order history. Our
                                    returns department will investigate and update you within 48 hours of receipt of the email (not
                                    including weekends). If required in some cases DistiMonster will use an outside accredited lab for
                                    testing to verify claims. If claims are verified that cost is the responsibility of DistiMonster, if the claim is
                                    rejected by the test results the cost of the testing is the customers expense.</p>
                              </div>
                           </div>
                        </div>
                        <!-- #tab2 End-->
                        <a href="<?php echo e(url('contact-us')); ?>">  <h3 class="tab_drawer_heading" rel="tab1">Contact Us</h3></a>

                        <!-- #tab3 End-->
                     </div>
                     <!-- .tab_container -->
                  </div>
                  <div class="shdow-box">
                  </div>
               </div>
            </div>
         </div>
      </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
   <script>
      $('#chat_link').click(function () {
          MyLiveChat.InlineChatDoExpand();
          //$('.mylivechat_sprite').trigger('click');
      });
   </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>