<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user_edit')): ?>
    <!-- <a href="mailto:<?php echo e($quote->customer_email); ?>" class="btn btn-xs btn-primary"><i class="fa fa-reply" aria-hidden="true"></i></a> -->
    <a href="<?php echo e(url('/admin/requested-lead-times-details/'.$quote->id)); ?>" class="btn btn-xs btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>
<?php endif; ?>