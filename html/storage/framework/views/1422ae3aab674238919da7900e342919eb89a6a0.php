<?php $__env->startSection('content'); ?>
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="<?php echo e(url('admin/home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active">Change Password</li>
		</ol>
	</section>
	<h3 class="page-title"><?php echo app('translator')->getFromJson('quickadmin.qa_change_password'); ?></h3>

		<?php echo Form::open(['method' => 'PATCH', 'url' => ['admin/change_password'],'data-parsley-validate' => true]); ?>

		<!-- If no success message in flash session show change password form  -->
		<div class="panel panel-default">
			<div class="panel-heading">
				Update
			</div>

			<div class="panel-body">
				<div class="row">
					<div class="col-xs-12 form-group">
						<?php echo Form::label('current_password', 'Current Password*', ['class' => 'control-label']); ?>

						<?php echo Form::password('current_password', ['class' => 'form-control', 'placeholder' => '','data-parsley-required'=>'true','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE')]); ?>

						<p class="help-block"></p>
						<?php if($errors->has('current_password')): ?>
							<p class="help-block">
								<?php echo e($errors->first('current_password')); ?>

							</p>
						<?php endif; ?>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 form-group">
						<?php echo Form::label('new_password', 'New Password*', ['class' => 'control-label']); ?>

						<?php echo Form::password('new_password', ['class' => 'form-control', 'placeholder' => '','data-parsley-required'=>'true','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.password_min'),'data-parsley-maxlength' => config('app.fields_length.password_max')]); ?>

						<p class="help-block"></p>
						<?php if($errors->has('new_password')): ?>
							<p class="help-block">
								<?php echo e($errors->first('new_password')); ?>

							</p>
						<?php endif; ?>
					</div>
				</div>

				<div class="row">
					<div class="col-xs-12 form-group">
						<?php echo Form::label('new_password_confirmation', 'Confirm New Password*', ['class' => 'control-label']); ?>

						<?php echo Form::password('new_password_confirmation', ['class' => 'form-control', 'placeholder' => '','data-parsley-required'=>'true','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-equalto'=>'#new_password','data-parsley-equalto-message'=>"Confirm Password must equal to password."]); ?>

						<p class="help-block"></p>
						<?php if($errors->has('new_password_confirmation')): ?>
							<p class="help-block">
								<?php echo e($errors->first('new_password_confirmation')); ?>

							</p>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
			
		<?php echo Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-success']); ?>

		<a class="btn btn-success btn-close" href="<?php echo e(url('admin/home')); ?>">Cancel</a>
		<?php echo Form::close(); ?>


<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>