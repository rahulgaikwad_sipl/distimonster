<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(url('admin/home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Profile</li>
        </ol>
    </section>
<br/>
    <br/>
    <?php echo Form::open( ['url' => url('admin/update-profile/'.Auth::user()->id), 'method' => 'POST', 'id' => 'profile_form', 'files' => true,'data-parsley-validate' => true]); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            Update Profile
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    <?php echo Form::label('name', 'Full Name*', ['class' => 'control-label']); ?>

                    <?php echo Form::text('name', Auth::user()->name, ['class' => 'form-control', 'placeholder' => '','data-parsley-required','minlength' => config('app.fields_length.name_min'),'maxlength' => config('app.fields_length.name_max')]); ?>

                    <p class="help-block"></p>
                    <?php if($errors->has('name')): ?>
                        <p class="help-block">
                            <?php echo e($errors->first('name')); ?>

                        </p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    <?php echo Form::label('name', 'Email', ['class' => 'control-label']); ?>

                    <?php echo Form::text('email',Auth::user()->email , ['class' => 'form-control', 'placeholder' => '', 'readonly'=>true]); ?>

                </div>
            </div>


            <div class="row">
                <div class="col-xs-12 form-group">
                    <?php echo Form::label('profile_pic', 'Profile Image', ['class' => 'control-label']); ?>

                    <?php echo Form::file('profile_pic', ['id' => 'profile_pic','data-parsley-image-file'=>'png,jpg,jpeg,PNG,JPG,JPEG,bmp,BMP','data-parsley-min-file-size'=>'1' , 'data-parsley-max-file-size'=>'5']); ?>

                    <img src="<?php echo e(Auth::user()->profile_pic ? url(config('app.resource_paths.profile_path').Auth::user()->profile_pic) : config('app.resource_paths.default_image')); ?>" width="200" height="100">
                    <?php if($errors->has('profile_pic_mime')): ?>
                        <span class="help-block">
                            <strong><?php echo e($errors->first('profile_pic_mime')); ?></strong>
                        </span>
                    <?php endif; ?>
                    <?php if($errors->has('profile_pic')): ?>
                        <span class="help-block">
                                        <strong><?php echo e($errors->first('profile_pic')); ?></strong>
                                    </span>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>
    
    <?php echo Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn  btn-success']); ?>

    <?php echo Form::close(); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>