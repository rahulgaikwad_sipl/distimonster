
<?php $__env->startSection('title', '| Account Settings'); ?>

<?php $__env->startSection('content'); ?>
    <section class="gray-bg account-custome-view top-pad-with-bradcrumb">
        <div class="container-fluid clearfix container-w-80 user-admin">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active"><span>Account Settings</span></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
                    <?php echo $__env->make('frontend.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <div class="tab_container">
                            <h3 class="tab_drawer_heading" rel="tab2">Edit Account Settings</h3>
                            <div id="tab2" class="tab_content">
                                <h2 class="block-title left-block-title">Edit Account Settings</h2>
                                <div class="shdow-box edit-account-setting">
                                    <div class="edit-account-setting-content">
                                        <div class="col-md-12">
                                            <h3>Account Information</h3>
                                        </div>
                                        <?php echo Form::model($user, ['url' => url('account-settings/'.$user->id), 'method' => 'POST','files' => true,'id' => 'profile_edit_form', 'data-parsley-validate' => true]); ?>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php echo Form::text('first_name', !empty($user->name)? $user->name:old('first_name'), ['class' => 'form-control', 'placeholder' => 'First Name*', 'data-parsley-pattern'=>config('app.patterns.name'), 'data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.name_min'),'data-parsley-maxlength' => config('app.fields_length.name_max'),'data-parsley-minlength-message' => $validationMessage['first_name.min'],'data-parsley-maxlength-message' => $validationMessage['first_name.max']]); ?>

                                                <p class="help-block"></p>
                                                <?php if($errors->has('first_name')): ?>
                                                    <p class="help-block">
                                                        <?php echo e($errors->first('first_name')); ?>

                                                    </p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php echo Form::text('last_name', !empty($user->last_name)? $user->last_name:old('last_name'), ['class' => 'form-control', 'placeholder' => 'Last Name*','data-parsley-pattern'=>config('app.patterns.name'), 'data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.name_min'),'data-parsley-maxlength' => config('app.fields_length.name_max'),'data-parsley-minlength-message' => $validationMessage['last_name.min'],'data-parsley-maxlength-message' => $validationMessage['last_name.max']]); ?>

                                                <p class="help-block"></p>
                                                <?php if($errors->has('last_name')): ?>
                                                    <p class="help-block">
                                                        <?php echo e($errors->first('last_name')); ?>

                                                    </p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php echo Form::text('contact_number', !empty($user->contact_number)? $user->contact_number:old('contact_number'), ['class' => 'form-control', 'placeholder' => 'Contact Number*','data-parsley-required', 'data-parsley-required data-parsley-pattern'=>'^[\d\+\-\.\(\)\/\s]*$', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'), 'data-parsley-minlength' => config('app.fields_length.contact_number_min'), 'data-parsley-maxlength'=> config('app.fields_length.contact_number_max'),'data-parsley-minlength-message'=>config('constants.CONTACT_NUMBER_MIN_MESSAGE') ,'data-parsley-maxlength-message' =>config('constants.CONTACT_NUMBER_MAX_MESSAGE')]); ?>

                                                <p class="help-block"></p>
                                                <?php if($errors->has('contact_number')): ?>
                                                    <p class="help-block">
                                                        <?php echo e($errors->first('contact_number')); ?>

                                                    </p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php echo Form::text('company_name', !empty($user->company_name)? $user->company_name:old('contact_number'), ['class' => 'form-control', 'placeholder' => 'Company Name',   'data-parsley-minlength' => 2, 'data-parsley-maxlength'=> 100]); ?>

                                                <p class="help-block"></p>
                                                <?php if($errors->has('company_name')): ?>
                                                    <p class="help-block">
                                                        <?php echo e($errors->first('company_name')); ?>

                                                    </p>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="col-md-12 m-t25">
                                            <h3>Billing Address</h3>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 ">
                                                <?php echo Form::text('address_line1', !empty($user->billing_address_line1)? $user->billing_address_line1:old('billing_address_line1'), ['id' => 'address_line1', 'placeholder'=>'Address Line 1', 'class' => 'form-control', 'data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.address_min'),'data-parsley-maxlength' => config('app.fields_length.address_max'),'data-parsley-minlength-message' => $validationMessage['address.min'],'data-parsley-maxlength-message' => $validationMessage['address.max']]); ?>

                                                <?php if($errors->has('address_line1')): ?>
                                                    <span class="help-block">
                                                        <strong><?php echo e($errors->first('address_line1')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php echo Form::text('address_line2', !empty($user->billing_address_line2)? $user->billing_address_line2:old('billing_address_line2'), ['id' => 'address_line2', 'placeholder'=>'Address Line 2 (Optional)', 'class' => 'form-control','data-parsley-minlength' => config('app.fields_length.address_min'),'data-parsley-maxlength' => config('app.fields_length.address_max'),'data-parsley-minlength-message' => $validationMessage['address.min'],'data-parsley-maxlength-message' => $validationMessage['address.max']]); ?>

                                                <?php if($errors->has('address_line2')): ?>
                                                    <span class="help-block">
                                                        <strong><?php echo e($errors->first('address_line2')); ?></strong>
                                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php echo Form::select('country_id', $countries, null, ['onChange' => 'checkOutModule.changeCountry()' , 'id' => 'country_id','data-parsley-required', 'data-parsley-required-message'=>'Please select a country.','class'=>'form-control']); ?>

                                                <?php if($errors->has('country_id')): ?>
                                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('country_id')); ?></strong>
                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php echo Form::select('state_id', $states, !empty($user->state_id)? $user->state_id :'', ['onChange' => 'checkOutModule.changeState()','id' => 'state_id', 'data-parsley-required', 'data-parsley-required-message'=>'Please select a state.','class'=>'form-control']); ?>

                                                <?php if($errors->has('state_id')): ?>
                                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('state_id')); ?></strong>
                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php echo Form::select('city', $cities, !empty($user->city)? $user->city :'', ['id' => 'city', 'data-parsley-required', 'data-parsley-required-message'=>'Please select a city.','class'=>'form-control']); ?>

                                                <?php if($errors->has('city')): ?>
                                                    <span class="help-block">
                                        <strong><?php echo e($errors->first('city')); ?></strong>
                                    </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php echo Form::text('zip_code',  !empty($user->zip_code)? $user->zip_code : old('zip_code'), ['id' => 'zip_code', 'placeholder'=>'Zip Code', 'class' => 'form-control', 'data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.zip_code_min'),'data-parsley-maxlength' => config('app.fields_length.zip_code_max'),'data-parsley-minlength-message' => $validationMessage['zip_code.min'],'data-parsley-maxlength-message' => $validationMessage['zip_code.max'],'data-parsley-type'=>"integer"]); ?>

                                                <?php if($errors->has('zip_code')): ?>
                                                    <span class="help-block">
                                            <strong><?php echo e($errors->first('zip_code')); ?></strong>
                                        </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <div class="col-md-12 m-t25">
                                            <h3>Payment Options</h3>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <?php $__currentLoopData = $paymentmethod; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($payment->id != 4): ?>
                                                    <label class="radio-container form-check-label"><?php echo e($payment->payment_method_name); ?>

                                                     <?php if($payment->id == 2): ?>
                                                            <input class="payment-method-type form-check-input" type="radio" name="payment_method" id="payment_method_<?php echo e($payment->id); ?>" value="<?php echo e($payment->id); ?>" <?php if($user->preferred_payment_type == $payment->id): ?> checked <?php endif; ?>>
                                                            <span class="checkmark"></span>
                                                     <?php else: ?>
                                                            <input onclick="payMentModule.choosePaymentMethod(this);" class="payment-method-type form-check-input" type="radio" name="payment_method" id="payment_method_<?php echo e($payment->id); ?>" value="<?php echo e($payment->id); ?>" <?php if($user->preferred_payment_type == $payment->id): ?> checked <?php endif; ?>>
                                                            <span class="checkmark"></span>
                                                     <?php endif; ?>
                                                    </label>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                            <div class="col-md-12" id="net_account_status" style="display: none">
                                                <div class="col-md-12 shdow-box p-b10 p-t10 m-t25">
                                                    <a href="<?php echo e(url('apply-net-term-account')); ?>" target="_blank"> Apply for New Net Terms</a>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group f-s14 text-gray change-email-password">
                                            <div class="col-md-12">
                                                <div class="form-check form-check-inline">
                                                    <!-- <?php echo e(Form::checkbox('change_email', 1, null, [ 'onclick'=>"profileModule.displayEmailSection()" ,'class' => '','id'=>'change_email'])); ?> -->

                                                    <!-- <label class="form-check-label">Change Email</label> -->
                                                    <div class="m-t5"></div>
                                                    <?php echo e(Form::checkbox('change_password', 1, null, ['onclick'=>"profileModule.displayPasswordSection()", 'class' => '','id'=>'change_password', 'checked'])); ?>

                                                    <label class="form-check-label">Change Password</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <h3><div id="email_pass_header"></div></h3>
                                        </div>
                                        <div style="display:none;" id="email_section">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <?php echo Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Email','id'=>'email', "data-parsley-remote" => url('check-newemail'),'data-parsley-remote-message'=>$validationMessage['email.unique']]); ?>

                                                    <p class="help-block"></p>
                                                    <?php if($errors->has('email')): ?>
                                                        <p class="help-block">
                                                            <?php echo e($errors->first('email')); ?>

                                                        </p>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="password_section">
                                            <p class="m-l15">If you would like to update your password you can do so here</p>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <?php echo Form::password('current_password', ['id'=>'current_password','class' => 'form-control', 'placeholder' => 'Current Password*']); ?>

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <?php echo Form::password('password', ['class' => 'form-control', 'placeholder' => 'New Password*','id'=>'password','data-parsley-minlength' => config('app.fields_length.password_min'),'data-parsley-maxlength' => config('app.fields_length.password_max'),'data-parsley-minlength-message'=>"Password consists of at least 6 character.","data-parsley-maxlength-message"=>"Password should not be exceed 20 characters."]); ?>

                                                    <p class="help-block"></p>
                                                    <?php if($errors->has('password')): ?>
                                                        <p class="help-block">
                                                            <?php echo e($errors->first('password')); ?>

                                                        </p>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <?php echo Form::password('cpassword', ['id'=>'cpassword','class' => 'form-control', 'placeholder' => 'Confirm New Password*','data-parsley-equalto-message'=>'Please enter same as password.','data-parsley-equalto'=>"#password" ]); ?>

                                                    <p class="help-block"></p>
                                                    <?php if($errors->has('cpassword')): ?>
                                                        <p class="help-block">
                                                            <?php echo e($errors->first('cpassword')); ?>

                                                        </p>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group m-t25">
                                            <div class="col-md-12">
                                                <?php echo Form::submit('Save', ['class' => 'btn btn-primary']); ?>

                                            </div>
                                        </div>
                                        <?php echo Form::close(); ?>

                                        <!-- /form -->
                                    </div>
                                </div>
                            </div>
                            <!-- #tab2 End-->
                        </div>
                        <!-- .tab_container -->
                    </div>
                    <div class="shdow-box">
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>
        $('#register_form').parsley();
        $(function(){
           if($("#payment_method_2").prop("checked") == true){
               $('#net_account_status').show();
           }

            $('html, body').animate({
                'scrollTop' : $("#password").position().top
            });
        });
        $('#payment_method_2').click(function(){
            $('#net_account_status').show();
        });
    </script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>