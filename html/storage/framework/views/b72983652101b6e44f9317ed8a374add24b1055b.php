<?php $request = app('Illuminate\Http\Request'); ?>


<?php $__env->startSection('content'); ?>

<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="<?php echo e(url('admin/home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">News</li>
    </ol>
</section>
<h3 class="page-title">News</h3>
<div class="row">
    <div class="col-md-12">
    <div class="box ">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="status_id" class="control-label">Status: </label>
                        </div>
                        <div class="col-sm-8">
                            <select name="status_id" id="status_id" class="form-control drop_down">
                                <option value="">Select Status</option>
                                <?php
                                foreach($newsStatus  as $list => $value){
                                    echo '<option value="'.$list.'">'.ucwords($value).'</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7">
                </div>
                <div class="col-lg-2 text-right">
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('role_create')): ?>
                        <div class="form-group  label-static">
                            <a href="<?php echo e(route('admin.news.create')); ?>" class="btn btn-success">Add New News</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

            <table border="0" width="100%"  class="table table-striped table-bordered table-hover testing" id="newsTable" >
                <thead>
                <tr>
                    <th>S.No</th>
                    <th>Title</th>
                    <th>Published on</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
    </div>
    <?php $__env->stopSection(); ?>

    <?php $__env->startSection('javascript'); ?> 
    <script>
    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('role_delete')): ?>
    window.route_mass_crud_entries_destroy = '<?php echo e(route('admin.news.mass_destroy')); ?>';
    <?php endif; ?>
    $(document).ready(function() {
        userModule.loadNewsDataTable();
        $(document).on('change', '.toggle-switch', function () {
                var msg = $(this).data('msg');
                var url = $(this).data('href');
                var id = $(this).data('id');
                var status = $(this).data('status');
                var $this = $(this);
                var state = $this.prop('checked');
                $this.prop('checked', !state).bootstrapToggle('destroy').bootstrapToggle();
                swal({
                        title: msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        cancelButtonText: "No",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            userModule.changeNewsStatus(id,status,url)
                        }
                    });
            });
    });
    $(document).on('change', '#status_id', function(){
        userModule.loadNewsDataTableAgain();
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>