<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo $__env->make('frontend.partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
<div class="entire-site">
    <div style="display: none;" class="loader monster-bom-loader">
        <div class="loader-img">
            &nbsp;
            <div id="loader_note_message" style="display: none;">
                <div class="alert alert-info">
                    <strong>Please be Patient,</strong> The Monster Is Processing Your Request Against Millions of Items
                </div>
            </div>
        </div>
    </div>
    
    <?php echo $__env->make('frontend.partials.topbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('frontend.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('frontend.partials.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('content'); ?>
    <?php echo $__env->make('frontend.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('javascript'); ?>
    <!-- old: hccid=40037455 -->
    <script type="text/javascript">function add_chatinline(){var hccid=60761561;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
add_chatinline(); </script>
<?php if(Auth::check()): ?>
    <!-- Sliding div starts here -->
    <!-- <div id="slider" style="right:0px;">
        <div id="sidebar" onclick="open_panel()"><img src="<?php echo e(url('frontend/assets/images/myaccount.png')); ?>" ></div>
        <div id="header">
            <h2>My Account</h2>
            <div>
                <a class="dropdown-item" href="<?php echo e(url('account-setting/'.Auth::user()->id)); ?>">Account Settings</a>
                <a class="dropdown-item" href="<?php echo e(url('order-history')); ?>">Order History</a>
                <a class="dropdown-item" href="<?php echo e(url('saved-quotes')); ?>">Saved Quotes</a>
                <a class="dropdown-item" href="<?php echo e(url('my-bom')); ?>">My BOM</a>
                <a class="dropdown-item" href="<?php echo e(url('apply-net-term-account')); ?>">Apply for a Net<br>Term Account</a>
            </div>
        </div>
    </div> -->
    <!-- Sliding div ends here -->
    <?php endif; ?>

</div>
</body>
</html>