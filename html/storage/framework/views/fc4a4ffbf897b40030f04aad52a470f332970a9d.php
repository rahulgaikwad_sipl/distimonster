
<?php $__env->startSection('title', '| Category - '.$category); ?>
<?php $__env->startSection('content'); ?>
    <section class="gray-bg shipping-cart categories-details-page">
        <div class="container clearfix ">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/categories')); ?>">Categories</a></li>
                        <li class="breadcrumb-item active"><span>Products</span></li>
                    </ul>
                </div>
            </div>
                <div class="row clearfix">
                <div class="col-md-7">
                    <h2 class="block-title" id="label_name"><?php echo e($category); ?></h2>
                </div>
            </div>
            <div class="row aboutus clearfix">
                <div class="col-md-12">
                    <div class="shdow-box ">
                        <div class="search-filter-input">
                             <input type="hidden" id="category_name" value="<?php echo e($categorySlug); ?>" >
                            <div style="display: none" id="part_search_div" >
                                <div class="search-filter-left">
                                    <input  class="form-control"  type="text" id="part_search_box" required  minlength="2" placeholder="Search by part number here" title="Search by part number here">
                                    <a onclick="searchPartList()" class="search-btn"> Search </a>
                                </div>
                                <button class="btn btn-primary pull-right m-t9" onclick="clearPartSearch()"> Reset </button>
                            </div>

                              <!--  <div class="col-md-4">

                                    <select class="form-control m-bot15" id="category_name">
                                            <?php $__currentLoopData = $categoryFilter; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option <?php echo e($category == $cat['name'] ? 'selected="selected"' : ''); ?> value="<?php echo e($cat['slug']); ?>"><?php echo e($cat['name']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                </div>-->    
                        </div>
                        <ul id="part-container" class="pull-left categories-left"></ul>
                        <div style="display: none" id="bottom-loader">

                        </div>
                
                    </div>
                </div>
            </div>
        </div>
    
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(asset('frontend/js/jquery-listnav.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/js/category-part-search.js')); ?>"></script>
    <script>
        $(document).ready(function () {
                setTimeout(function () {
                    getBoxData();
                }, 100);
        });

        $(window).scroll(function () {
            if ($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
                if (isAjaxRequest == true) {
                    getBoxData();
                }
            }
        });
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>