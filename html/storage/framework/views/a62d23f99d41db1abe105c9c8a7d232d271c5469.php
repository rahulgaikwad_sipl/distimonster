
<?php $__env->startSection('title', '| Order History'); ?>
<?php $__env->startSection('content'); ?>

    <section class="gray-bg account-custome-view top-pad-with-bradcrumb">
        <div class="container-fluid clearfix container-w-80 user-admin">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active"><span>Order History</span></li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
                         <?php echo $__env->make('frontend.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                                           <div class="tab_container">
                            <h3 class="tab_drawer_heading" rel="tab3">Tab 3</h3>
                            <div id="tab3" class="tab_content order-history active ">
                                <h2 class="block-title left-block-title">Order History</h2>
                                <?php
                                $activeTab = '2';
                                if(request()->has('tab')){
                                    $activeTab = request()->get('tab');
                                }
                                ?>
                                <div class="">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo e(($activeTab == '2')?'active':''); ?>" data-toggle="tab" data-id="2" href="#pending">In Process</a>
                                        </li>
                                        <?php /*
                                        <li class="nav-item">
                                            <a class="nav-link {{($activeTab == '3')?'active':''}}" data-toggle="tab" data-id="3" href="#transit">In Transit</a>
                                        </li> */ ?>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo e(($activeTab == '1')?'active':''); ?>" data-toggle="tab" data-id="1" href="#shipped">Shipped</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link <?php echo e(($activeTab == '4')?'active':''); ?>" data-toggle="tab" data-id="4" href="#delivered">Delivered</a>
                                        </li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div id="pending" class="tab-pane <?php echo e(($activeTab == '2')?'active':''); ?>">
                                            <?php if($activeTab == '2'): ?>
                                            <?php if($orders->isEmpty()): ?>
                                                <h2 class="block-title left-block-title">Currently you do not have any pending orders</h2>
                                            <?php else: ?>
<div class="table-responsive">
                                                <table class="table  table-hover table-bordered">
                                                    <tr class="f-s16 font-w500 black">
                                                        <th>Order#</th>
                                                        <th>Date</th>
                                                        <th>Ship To </th>
                                                        <th>Order Total</th>
                                                        <th width="150px;">Status</th>
                                                        <!-- <th>Tracking Information</th> -->
                                                        <th></th>
                                                    </tr>
                                                    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr class="shdow-box f-s14 text-gray">
                                                            <td><?php echo e($order->id); ?></td>
                                                            <td><?php echo e(date('m/d/Y', strtotime($order->order_date))); ?>  </td>
                                                            <td><?php echo e($order->name); ?> <?php echo e($order->last_name); ?></td>
                                                            <td>$<?php echo e(number_format($order->order_total_amount,2, '.', ',')); ?></td>
                                                            <?php if($order->order_shipping_status  == 1 ): ?>
                                                                <td><span class="delivered"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.shipped')); ?></td>
                                                            <?php elseif($order->order_shipping_status  == 2): ?>
                                                                <td><span class="pending"></span><?php echo e(($order->partially_shipped  == 1) ? config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.partially_shipped') : config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.pending')); ?></td>
                                                            <?php elseif($order->order_shipping_status  == 3): ?>
                                                                <td><span class="failed"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.intransit')); ?></td>
                                                            <?php elseif($order->order_shipping_status  == 4): ?>
                                                                <td><span class="delivered"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.delivered')); ?></td>
                                                            <?php endif; ?>
                                                            
                                                            <td class="action">
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item">
                                                                        <a href="<?php echo e(url('/order-details/'.$order->id)); ?>" class="f-s14 hind-font"><span class="img-icon"><i class="fa fa-eye"></i></span> View Order</a>
                                                                    </li>
                                                                    <li class="list-inline-item">
                                                                        <a href="<?php echo e(url('/reorder/'.$order->id)); ?>" class="f-s14 hind-font" onclick="show_loader()"><span class="img-icon"><i class="fa fa-undo"></i></span> Reorder</a>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <!-- <tr class="blank-space p-0">
                                                            <td colspan="6" class="p-0">&nbsp;</td>
                                                        </tr> -->
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </table>
                                            </div>

                                                <div class="pagination-list text-right clearfix">
                                                    <?php echo e($orders->appends(['page'=>request()->except('page'),'tab'=>request()->get('tab')])->links('frontend.partials.pagination')); ?>

                                                </div>

                                            <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                        <div id="transit" class="tab-pane <?php echo e(($activeTab == '3')?'active':''); ?>">
                                            <?php if($activeTab == '3'): ?>
                                            <?php if($orders->isEmpty()): ?>
                                                <h2 class="block-title left-block-title">Currently you do not have any orders</h2>
                                            <?php else: ?>
<div class="table-responsive">
                                            <table class="table table-hover table-bordered">
                                                    <tr class="f-s16 font-w500 black">
                                                        <th>Order#</th>
                                                        <th>Date</th>
                                                        <th>Ship To </th>
                                                        <th>Order Total</th>
                                                        <th>Status</th>
                                                        <th>Tracking Information</th>
                                                        <th></th>
                                                    </tr>
                                                    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr class="shdow-box f-s14 text-gray">
                                                            <td><?php echo e($order->id); ?></td>
                                                            <td><?php echo e(date('m/d/Y', strtotime($order->order_date))); ?>  </td>
                                                            <td><?php echo e($order->name); ?> <?php echo e($order->last_name); ?></td>
                                                            <td>$<?php echo e(number_format($order->order_total_amount,2, '.', ',')); ?></td>
                                                            <?php if($order->order_shipping_status  == 1 ): ?>
                                                                <td><span class="delivered"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.shipped')); ?></td>
                                                            <?php elseif($order->order_shipping_status  == 2): ?>
                                                                <td><span class="pending"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.pending')); ?></td>
                                                            <?php elseif($order->order_shipping_status  == 3): ?>
                                                                <td><span class="failed"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.intransit')); ?></td>
                                                            <?php elseif($order->order_shipping_status  == 4): ?>
                                                                <td><span class="delivered"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.delivered')); ?></td>
                                                            <?php endif; ?>
                                                            <td><?php if(!empty($order->order_tracking_info)): ?><?php echo e($order->order_tracking_info); ?><?php else: ?> N/A <?php endif; ?></td>
                                                            <td class="action">
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item">
                                                                        <a href="<?php echo e(url('/order-details/'.$order->id)); ?>" class="f-s14 hind-font"><span class="img-icon"><i class="fa fa-eye"></i></span> View Order</a>
                                                                    </li>
                                                                    <li class="list-inline-item">
                                                                        <a href="<?php echo e(url('/reorder/'.$order->id)); ?>" class="f-s14 hind-font" onclick="show_loader()"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/reorder-icon.png')); ?>" alt="reorder-icon" /></span> Reorder</a>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <!-- <tr class="blank-space p-0">
                                                            <td colspan="6" class="p-0">&nbsp;</td>
                                                        </tr> -->
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </table></div>

                                                <div class="pagination-list text-right clearfix">
                                                    <?php echo e($orders->appends(['page'=>request()->except('page'),'tab'=>request()->get('tab')])->links('frontend.partials.pagination')); ?>

                                                </div>

                                            <?php endif; ?>
                                                <?php endif; ?>
                                        </div>
                                        <div id="shipped" class="tab-pane <?php echo e(($activeTab == '1')?'active':''); ?>">
                                            <?php if($activeTab == '1'): ?>
                                            <?php if($orders->isEmpty()): ?>
                                                <h2 class="block-title left-block-title">Currently you do not have any shipped orders</h2>
                                            <?php else: ?>
<div class="table-responsive">
                                            <table class="table table-hover table-bordered">
                                                    <tr class="f-s16 font-w500 black">
                                                        <th>Order#</th>
                                                        <th>Date</th>
                                                        <th>Ship To </th>
                                                        <th>Order Total</th>
                                                        <th>Status</th>
                                                        <th>Tracking Information</th>
                                                        <th></th>
                                                    </tr>
                                                    <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr class="shdow-box f-s14 text-gray">
                                                            <td><?php echo e($order->id); ?></td>
                                                            <td><?php echo e(date('m/d/Y', strtotime($order->order_date))); ?>  </td>
                                                            <td><?php echo e($order->name); ?> <?php echo e($order->last_name); ?></td>
                                                            <td>$<?php echo e(number_format($order->order_total_amount,2, '.', ',')); ?></td>
                                                            <?php if($order->order_shipping_status  == 1 ): ?>
                                                                <td><span class="delivered"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.shipped')); ?></td>
                                                            <?php elseif($order->order_shipping_status  == 2): ?>
                                                                <td><span class="pending"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.pending')); ?></td>
                                                            <?php elseif($order->order_shipping_status  == 3): ?>
                                                                <td><span class="failed"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.intransit')); ?></td>
                                                            <?php elseif($order->order_shipping_status  == 4): ?>
                                                                <td><span class="delivered"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.delivered')); ?></td>
                                                            <?php endif; ?>
                                                            <td><?php if(!empty($order->order_tracking_info)): ?><?php echo e($order->order_tracking_info); ?><?php else: ?> N/A <?php endif; ?></td>
                                                            <td class="action">
                                                                <ul class="list-inline">
                                                                    <li class="list-inline-item">
                                                                        <a href="<?php echo e(url('/order-details/'.$order->id)); ?>" class="f-s14 hind-font"><span class="img-icon"><i class="fa fa-eye"></i></span> View Order</a>
                                                                    </li>
                                                                    <li class="list-inline-item">
                                                                        <a href="<?php echo e(url('/reorder/'.$order->id)); ?>" class="f-s14 hind-font" onclick="show_loader()"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/reorder-icon.png')); ?>" alt="reorder-icon" /></span> Reorder</a>
                                                                    </li>
                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <!-- <tr class="blank-space p-0">
                                                            <td colspan="6" class="p-0">&nbsp;</td>
                                                        </tr> -->
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                </table>
                                            </div>

                                                <div class="pagination-list text-right clearfix">
                                                    <?php echo e($orders->appends(['page'=>request()->except('page'),'tab'=>request()->get('tab')])->links('frontend.partials.pagination')); ?>

                                                </div>

                                            <?php endif; ?>
                                                <?php endif; ?>
                                        </div>
                                        <div id="delivered" class="tab-pane <?php echo e(($activeTab == '4')?'active':''); ?>">
                                            <?php if($activeTab == '4'): ?>
                                                <?php if($orders->isEmpty()): ?>
                                                    <h2 class="block-title left-block-title">Currently you do not have any delivered orders</h2>
                                                <?php else: ?>
<div class="table-responsive">
                                                <table class="table table-hover table-bordered">
                                                        <tr class="f-s16 font-w500 black">
                                                            <th>Order#</th>
                                                            <th>Date</th>
                                                            <th>Ship To </th>
                                                            <th>Order Total</th>
                                                            <th>Status</th>
                                                            <th>Tracking Information</th>
                                                            <th></th>
                                                        </tr>
                                                        <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                            <tr class="shdow-box f-s14 text-gray">
                                                                <td><?php echo e($order->id); ?></td>
                                                                <td><?php echo e(date('m/d/Y', strtotime($order->order_date))); ?>  </td>
                                                                <td><?php echo e($order->name); ?> <?php echo e($order->last_name); ?></td>
                                                                <td>$<?php echo e(number_format($order->order_total_amount,2, '.', ',')); ?></td>
                                                                <?php if($order->order_shipping_status  == 1 ): ?>
                                                                    <td><span class="delivered"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.shipped')); ?></td>
                                                                <?php elseif($order->order_shipping_status  == 2): ?>
                                                                    <td><span class="pending"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.pending')); ?></td>
                                                                <?php elseif($order->order_shipping_status  == 3): ?>
                                                                    <td><span class="failed"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.intransit')); ?></td>
                                                                <?php elseif($order->order_shipping_status  == 4): ?>
                                                                    <td><span class="delivered"></span> <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.delivered')); ?></td>
                                                                <?php endif; ?>
                                                                <td><?php if(!empty($order->order_tracking_info)): ?><?php echo e($order->order_tracking_info); ?><?php else: ?> N/A <?php endif; ?></td>
                                                                <td class="action">
                                                                    <ul class="list-inline">
                                                                        <li class="list-inline-item">
                                                                            <a href="<?php echo e(url('/order-details/'.$order->id)); ?>" class="f-s14 hind-font"><span class="img-icon"><i class="fa fa-eye"></i></span> View Order</a>
                                                                        </li>
                                                                        <li class="list-inline-item">
                                                                            <a href="<?php echo e(url('/reorder/'.$order->id)); ?>" class="f-s14 hind-font" onclick="show_loader()"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/reorder-icon.png')); ?>" alt="reorder-icon" /></span> Reorder</a>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                            <!-- <tr class="blank-space p-0">
                                                                <td colspan="6" class="p-0">&nbsp;</td>
                                                            </tr> -->
                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    </table></div>

                                                    <div class="pagination-list text-right clearfix">
                                                        <?php echo e($orders->appends(['page'=>request()->except('page'),'tab'=>request()->get('tab')])->links('frontend.partials.pagination')); ?>

                                                    </div>

                                                <?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- .tab_container -->
                    </div>
                    <div class="shdow-box">
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>

        $('#register_form').parsley();

        $(document).ready(function(){
            $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
                $(".loader").css("display",'block');
                var currentTab = $(e.target).attr('data-id');
                var currentUrl = window.location.href;
                var redirectUrl = updateQueryStringParameter(currentUrl, 'tab', currentTab);
                 redirectUrl = updateQueryStringParameter(redirectUrl, 'page', 1);
                window.location.assign(redirectUrl);
            });
        });

        function show_loader() {
            $(".loader").css("display", 'block');
            $("#loader_note_message").css("display", 'block');
        }

        function updateQueryStringParameter(uri, key, value) {
            var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
            var separator = uri.indexOf('?') !== -1 ? "&" : "?";
            if (uri.match(re)) {
                return uri.replace(re, '$1' + key + "=" + value + '$2');
            }
            else {
                return uri + separator + key + "=" + value;
            }
        }
    </script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>