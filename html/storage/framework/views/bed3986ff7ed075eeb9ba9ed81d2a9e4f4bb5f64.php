
<?php if($paginator->hasPages()): ?>
<ul class="pagination pull-right">

<?php if($paginator->onFirstPage()): ?>
    <li class="page-item">
        <a class="page-link disabled"  aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
        </a>
    </li>
<?php else: ?>
    <li class="page-item">
        <a href="<?php echo e($paginator->previousPageUrl()); ?>"  class="page-link" aria-label="Previous">
            <span aria-hidden="true">&laquo;</span>
        </a>
    </li>
<?php endif; ?>
    
<?php $__currentLoopData = $elements; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $element): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    
    <?php if(is_string($element)): ?>
        <li class="disabled"><span><?php echo e($element); ?></span></li>
        <?php endif; ?>
    
    <?php if(is_array($element)): ?>
        <?php $__currentLoopData = $element; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $page => $url): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php if($page == $paginator->currentPage()): ?>
                <li class="page-item active"><a href="#" class="page-link"><?php echo e($page); ?></a></li>
                <?php else: ?>
                <li class="page-item"><a href="<?php echo e($url); ?>" class="page-link"><?php echo e($page); ?></a></li>
                <?php endif; ?>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

<?php if($paginator->hasMorePages()): ?>
    <li class="page-item">
        <a href="<?php echo e($paginator->nextPageUrl()); ?>" class="page-link" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
        </a>
    </li>
<?php else: ?>
    <li class="page-item">
        <a class="page-link disabled" aria-label="Next">
            <span aria-hidden="true">&raquo;</span>
        </a>
    </li>
<?php endif; ?>
</ul>
<?php endif; ?>
