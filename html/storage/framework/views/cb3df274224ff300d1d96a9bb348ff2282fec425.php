
<?php $__env->startSection('title', '| Part not found'); ?>
<?php $__env->startSection('content'); ?>
    <section class="gray-bg shipping-cart categories-page">
        <div class="container clearfix aboutus">
            <div class="shdow-box ">
                <div class="col-lg-12 text-right p-b15" >
                    <a href="<?php echo e(url('category/'.$categorySlug)); ?>"> Back to parts</a>
                </div>
                <?php if(Auth::check()): ?>
                <div class="no-product-found">
                    It seems, the part <b>"<?php echo e($partNumber); ?>"</b> is not available at the moment. But you can request lead time by clicking on "Add to RFQ" button.
                </div>
                    <button type="button" onclick="showPopup()" id="request_product-btn"  class="m-t9 btn btn-primary btn-outline p-tb5 requestquote-btn-custom" title="Request Lead Time"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/add-quote-icon.png')); ?>" alt="add-quote-icon" /></span>Add to RFQ</button>
                <?php else: ?>
                <div class="no-product-found">
                    It seems, the part <b>"<?php echo e($partNumber); ?>"</b> is not available at the moment. Please login to request a quote for this part.
                </div>
                    <a class="m-t9 btn btn-primary btn-outline p-tb5 " href="<?php echo e(url('login')); ?>">Login</a>
                <?php endif; ?>
                </div>
        </div>

        <div class="modal" id="request-part-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="modalLabel">Request for Quote</h4>
                        <button type="button" class="close" data-dismiss="modal" onclick="quoteModule.closeAddToQuoteModel()" aria-label="Close">
                            <span aria-hidden="true"><img src="<?php echo e(url('frontend/assets/images/close-icon.png')); ?>" alt="close-icon" /></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?php echo Form::open(['class'=>'form-horizontal','method' => 'POST', 'id' => 'request_part_detail_form', 'data-parsley-validate' => true]); ?>

                        <div class="price-content-box">
                            <div class="price-content-body">
                                <div class="clearfix m-b5 f-s14"></div>
                                <div class="form-group">
                                    <div class="col-md-12 p-l0">
                                        <input type="text"  readonly name="popupPartNumber"  data-parsley-required  data-parsley-maxlength="50"  value="<?php echo e($partNumber); ?>" id="popupPartNumber" placeholder="MPN*" class="quantity-box"  data-parsley-minlength="2"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 p-l0">
                                        <input type="text" readonly name="popupExpectedDeliveryDate" value="" class="requested-delivery-date quantity-box" id="popupExpectedDeliveryDate" placeholder="Expected Delivery Date"  />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12 p-l0">
                                        <input type="text" name="popupExpectedQuantity" data-parsley-required data-parsley-type="digits"  data-parsley-maxlength="10"  value="" id="popupExpectedQuantity" placeholder="Requested Quantity*" class="quantity-box"  min="1"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 p-l0">
                                        <input type="text" name="popupExpectedUnitPrice" data-parsley-pattern="^\s*(?=.*[1-9])\d*(?:\.\d{1,6})?\s*$"  data-parsley-maxlength="20" data-parsley-type="number" value="" id="popupExpectedUnitPrice" placeholder="Target Price (per unit in $)" class="quantity-box"
                                        />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 p-l0">
                                        <select disabled data-parsley-required  data-parsley-errors-container="#cat_error" class="form-control m-bot15" id="popupCategoryName">
                                            <option value="">Select</option>
                                            <?php $__currentLoopData = $categoryFilter; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option <?php echo e($categorySlug == $cat['slug'] ? 'selected="selected"' : ''); ?> value="<?php echo e($cat['slug']); ?>"><?php echo e($cat['name']); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select>

                                    </div>
                                </div>
                                <div id="cat_error"></div>
                                <div class="form-group">
                                    <div class="col-md-12 p-l0">
                                        <input type="text" name="popupLeadQuoteName" value="" id="popupLeadQuoteName" placeholder="Quote Name" class="quantity-box"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php echo Form::close(); ?>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" data-dismiss="modal" class="btn btn-primary btn-outline" title="Cancel" onclick="quoteModule.closeAddToQuoteModel()"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/pop-up-close-icon.png')); ?>" alt="pop-up-close-icon" /></span>Cancel</button>
                        <button type="button" onclick="addToRequestedPart()" class="btn btn-primary" title="Request Lead Time"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/add-white-icon.png')); ?>" alt="pop-up-edit-icon" /></span>Add to RFQ</button>
                    </div>

                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(asset('frontend/js/jquery-listnav.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/js/category-part-search.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>