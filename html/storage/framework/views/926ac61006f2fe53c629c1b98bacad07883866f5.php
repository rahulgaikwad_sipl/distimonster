<input type="checkbox" class="toggle-switch" <?php echo e(($user->status == 1)?'checked':''); ?>

data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger"
       data-msg="Are you sure you want to <?php echo e(($user->status == 1)?'Deactivate':'Activate'); ?> this user?"
       data-href="/admin/update-user-status"  data-id="<?php echo e($user->id); ?>"  data-status="<?php echo e($user->status); ?>" data-size="small"
>
      