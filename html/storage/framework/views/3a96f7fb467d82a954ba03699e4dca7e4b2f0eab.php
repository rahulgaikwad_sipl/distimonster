
<?php $__env->startSection('title', '| Site Map'); ?>
<?php $__env->startSection('content'); ?>

    <!--START: Sitemap Page-->
    <section class="gray-bg shipping-cart sitemap">
         <div class="container clearfix ">
            <div class="row pagination">
               <div class="col-md-12">
                  <ul class="breadcrumb f-s14 text-gray p-l0">
                     <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                     <li class="breadcrumb-item active"><span>Site Map</span></li>
                  </ul>
               </div>
            </div>
            <!-- <div class="row clearfix">
               <div class="col-md-7">
                  <h2 class="block-title">Site Map</h2>
               </div>
            </div> -->
            
            <div class="row sitemap-content">
               <div class="col-md-4">
                  <h4 class="f-s18 black font-w500">Main Menu</h4>
                  <hr />
                  <ul>
                     
                     <li><a href="<?php echo e(url('/about')); ?>"><h6>About</h6></a></li>
                     <li><a href="<?php echo e(url('/news-updates')); ?>"><h6>News & Updates</h6></a></li>
                     <li><h6>Support</h6>
                        <ul>
                           <li><a href="<?php echo e(url('/faq')); ?>">FAQs</a></li>
                           <li><a href="<?php echo e(url('/return-policy')); ?>">Return Policy</a></li>
                           <li><a href="<?php echo e(url('/contact')); ?>">Contact</a></li>
                        </ul>
                     </li>
                     <li><a href="<?php echo e(url('/bom')); ?>"><h6>Monster-BOM</h6></a></li>
                      <?php if(Auth::user()): ?>
                       <li><a href=href="javascript:void(0)"><h6>My Account</h6></a>
                        <ul>
                            <li><a href="<?php echo e(url('account-settings/'.Auth::user()->id)); ?>">Account Setttings</a></li>
                           <li><a href="<?php echo e(url('/order-history')); ?>">Order History</a></li>
                           <li><a href="<?php echo e(url('/quotes')); ?>">Quotes</a></li>
                           <li><a href="<?php echo e(url('/my-bom')); ?>">MonsterBOM</a></li>
                           <li><a href="<?php echo e(url('/my-bio')); ?>">My Bio</a></li>
                           <?php if(Auth::user()->is_sub_user == 0): ?>
                              <li><a href="<?php echo e(url('/my-team')); ?>">My Team</a></li>
                            <?php endif; ?>  
                           <li><a href="<?php echo e(url('/requested-lead-times')); ?>">Requested Lead Times</a></li>
                           <!-- <li><a href="<?php echo e(url('/apply-net-term-account')); ?>">Apply Net Term Account</a></li> -->
                        </ul>
                           <?php endif; ?>
                     </li>
                     <li><a href="<?php echo e(url('/terms-of-use')); ?>"><h6>Privacy Policy and Terms of Use</h6></a></li>
                  </ul>
  
               </div>
               
                  
                  
                   
                     
                        
                           
                              
                                 
                                 
                                 
                              
                        
                        
                              
                                  
                                 
                                 
                              
                        
                        
                              
                                 
                                 
                                 
                              
                        
                         
                              
                                
                                 
                                 
                              
                        
                         
                              
                                 
                                 
                                 
                              
                        
                     
                     
                  

                
                  
                   
                     
                        
                           
                              
                                 
                                 
                                 
                              
                        
                        
                              
                                  
                                 
                                 
                              
                        
                        
                              
                                 
                                 
                                 
                              
                        
               
                     
                     
                  
               
            </div>
         </div>
      </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>