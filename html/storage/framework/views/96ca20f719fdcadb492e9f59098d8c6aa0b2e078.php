
<?php $__env->startSection('title', '| Order Details'.' - Order Id:'.$order[0]->id); ?>

<?php $__env->startSection('content'); ?>
<!--START: Cart Page-->
<!--MAIN SECTION START:-->
<section class="gray-bg shipping-cart">
	<div class="container clearfix">
		<div class="row pagination">
			<div class="col-md-12">
				<ul class="breadcrumb f-s14 text-gray p-l0">
					<li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
					<li class="breadcrumb-item"><a href="<?php echo e(url('order-history')); ?>">Order History</a></li>
					<li class="breadcrumb-item active"><span>Order Detail</span></li>
				</ul>
			</div>
		</div>
		<div class="row clearfix">
			<div class="col-md-7">
				<h2 class="block-title">Order Id: <?php echo e($order[0]->id); ?>&nbsp;<span class="text-gray font-w-normal f-s14"><?php echo e(date('m/d/Y', strtotime($order[0]->order_date))); ?></span>
				</h2>
			</div>
			<div class="col-md-5 text-right quote-detail-right-icon">
				<ul class="list-inline m-b0">
					
						
					
					
						
					
					
						
					
				</ul>
			</div>
		</div>
		<hr class="m-b0" />
		<div class="row">
			<div class="col-md-12">
				<div class="f-s18 text-gray top-space total-parts">Total Parts: <span class="black font-w500"><?php echo e(count($orderDetail)); ?></span></div>
			</div>
			<div class="col-md-12">
			
				<div class="order-history order-detail-list">
				
					 <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
						<tr class="f-s16 font-w500 black">
							<th>MPN</th>
							
							<th>Manufacturer</th>
							<th>Quantity</th>
							<th>Internal Part#</th>
							<th>Price</th>
							<th>Total</th>
						</tr>
						<tr class="blank-space p-0">
							<td colspan="6" class="p-0">&nbsp;</td>
						</tr>
						<!--quoteItems Listing Content-->
						<?php
						$quoteSubTotal= 0;
						foreach($orderDetail as $item){
						?>
						<tr class="shdow-box f-s14 text-gray">
							<td>
								<b>
									<?php if(!empty($item->item_name)): ?><?php echo e($item->item_name); ?><?php else: ?> N/A <?php endif; ?>
								</b>
							</td>
							
							<td>
								<?php if(!empty($item->manufacturer_name)): ?><?php echo e($item->manufacturer_name); ?><?php else: ?> N/A <?php endif; ?>
							</td>
							<td>
								<?php if(!empty($item->quantity)): ?><?php echo e($item->quantity); ?><?php else: ?> N/A <?php endif; ?>
							</td>
							<td>
								<?php echo e($item->customer_part_id?$item->customer_part_id:'N/A'); ?>

							</td>
							<td>
								<b>
									<?php if(!empty($item->price)): ?>$<?php echo e(number_format($item->price, 2, '.', ',')); ?><?php else: ?> N/A <?php endif; ?>
								</b>
							</td>
							<td>
								<b>
									<?php if(!empty($item->price) && !empty($item->quantity)): ?>
										$<?php $totalPrice= number_format($item->price, 2, '.', ',')*$item->quantity; ?> <?php echo e(number_format($totalPrice, 2, '.', ',')); ?>

									<?php else: ?>
										N/A
									<?php endif; ?>		
								</b>
							</td>
						</tr>
						<?php
						$quoteSubTotal = $quoteSubTotal+ ($item->quantity * $item->price);
						}
						?>
					</table>
				</div>
				</div>
				<!--quoteItems Listing Content-->
				<!-- <div class="order-total text-right">
					<div class="f-s16 text-gray font-w500 m-b25">Subtotal: <span class="black font-w500">$<?php echo e(number_format($quoteSubTotal,2,'.',',')); ?></span></div>
					
				</div> -->
			</div>
			<div class="col-md-12">
				<div class="shdow-box shipping-right-content m-t15">
					<div class="row">
						<div class="col-md-5">
							<div class="price-content-box">
								<div class="row price-box-heading">
									<div class="col-md-10">
										<h2 class="block-title">Shipping Details</h2>
									</div>
								</div>
								<div class="price-content-body">
									<ul>
										<li class="clearfix">
											<span class="text-gray f-s14"><?php echo e($order[0]->address_shipping); ?></span>
										</li>
									</ul>
								</div>
							</div>
							<div class="price-content-box">
								<div class="row price-box-heading">
									<div class="col-md-10">
										<h2 class="block-title">Billing Details</h2>
									</div>
								</div>
								<div class="price-content-body">
									<ul>
										<li class="clearfix">
											<span class="text-gray f-s14"><?php echo e($order[0]->address_billing); ?></span>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-7">
							<div class="price-content-box">
								<div class="row price-box-heading">
									<div class="col-md-10">
										<h2 class="block-title">Order Summary</h2>
									</div>
									<div class="col-md-2">
										&nbsp;
									</div>
								</div>
								<div class="price-content-body">
									<ul>
										<li class="clearfix">
											<span class="pull-left text-gray f-s14">Payment Method Type</span>
											<span class="pull-right text-gray f-s14"><?php echo e($order[0]->payment_method_name); ?></span>
										</li>
										<li class="clearfix">
											<span class="pull-left text-gray f-s14">Payment Status</span>
											<?php if($order[0]->payment_status  == 0 ): ?>
											<span class="pull-right text-gray f-s14"> <?php echo e(config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.pending')); ?></span>
											<?php elseif($order[0]->payment_status  == 1): ?>
											<span class="pull-right text-gray f-s14"> <?php echo e(config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.done')); ?></span>
											<?php elseif($order[0]->payment_status  == 2): ?>
											<span class="pull-right text-gray f-s14"> <?php echo e(config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.failed')); ?></span>
											<?php elseif($order[0]->payment_status  == 3): ?>
											<span class="pull-right text-gray f-s14"> <?php echo e(config('constants.APP_CONSTANT.PAYMENT_STATUS_NAME.refunded')); ?></span>
											<?php endif; ?>
										</li>
										<li class="clearfix">
											<span class="pull-left text-gray f-s14">Shipping Method Type</span>
											
											<?php if($order[0]->shipping_method_type): ?>
											  <span class="pull-right text-gray f-s14"><?php echo e(Helpers::getShippingOptionById($order[0]->shipping_method_type)->name); ?></span>
										    <?php endif; ?>
										</li>
										<li class="clearfix">
											<span class="pull-left text-gray f-s14">Subtotal</span>
											<span class="pull-right text-gray f-s14">$<?php echo e(number_format($order[0]->order_sub_total,2, '.', ',')); ?></span>
										</li>
										<li class="clearfix">
											<span class="pull-left text-gray f-s14">Tax Amount</span>
											<span class="pull-right text-gray f-s14">$<?php echo e(number_format($order[0]->order_tax_amount,2, '.', ',')); ?></span>
										</li>
										<li class="clearfix">
											<span class="pull-left text-gray f-s14">Discount Amount</span>
											<span class="pull-right text-gray f-s14">$<?php echo e(number_format( $order[0]->order_discount_amount,2, '.', ',')); ?></span>
										</li>
										<li class="clearfix">
											<span class="pull-left text-gray f-s14">Shipping Charges</span>
											<span class="pull-right text-gray f-s14">$<?php echo e(number_format( $order[0]->order_shipping_charges_amount,2, '.', ',')); ?></span>
										</li>
										<li class="f-s16 black font-w500 clearfix">
											<span class="pull-left">Order Total</span>
											<span class="pull-right">$<?php echo e(number_format($order[0]->order_total_amount,2, '.', ',')); ?></span>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					
					
				</div>
			</div>

			<div class="col-md-12">
			<h2 class="block-title m-b5">Tracking Information</h2>
				
						<div class="order-history order-detail-list">
                        <div class="table-responsive">
						<table class="table table-responsive table-browser-view">
						<tr class="f-s16 font-w500 black">
                        		<th>Distributor Name</th>
								<th>MPN</th>
								<th>Internal Part#</th>
                        		<th>Ordered Qty</th>
                        		<th>Shipped Qty</th>
                        		<th>Tracking Number</th>
                        		<th>Note</th>
                        		<th>Shipping Status</th>
								<th>Date of Shipping</th>
						</tr>
						<tr class="blank-space p-0">
							<td colspan="8" class="p-0">&nbsp;</td>
						</tr>
                        <?php $__currentLoopData = $orderDetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <tr class="shdow-box f-s14 text-gray">
                                <td><?php if(isset($item->distributor_name) && !empty($item->distributor_name)): ?> <?php echo e(ucwords($item->distributor_name)); ?> <?php else: ?> N/A <?php endif; ?></td>
								<td><?php if(isset($item->item_name) && !empty($item->item_name)): ?> <?php echo e($item->item_name); ?> <?php else: ?> N/A <?php endif; ?></td>
								<td>
									<?php echo e($item->customer_part_id?$item->customer_part_id:'N/A'); ?>

								</td>
                                <td><?php if(isset($item->quantity) && !empty($item->quantity)): ?> <?php echo e($item->quantity); ?> <?php else: ?> N/A <?php endif; ?></td>
								<td><?php if(isset($item->shipped_qty) && !empty($item->shipped_qty)): ?><?php echo e($item->shipped_qty); ?> <?php else: ?> N/A <?php endif; ?></td>
								<td><?php if(!empty($item->tracking_number) && !empty($item->tracking_website)): ?> <a target="_blank" href="<?php if($item->tracking_website == 'fedex' && !empty($item->tracking_number)) echo config('constants.FEDEX_TRACKING_URL').'?tracknumbers='.$item->tracking_number.'&cntry_code=us';else if($item->tracking_website == 'ups' && !empty($item->tracking_number)) echo config('constants.UPS_TRACKING_URL').'?loc=en_US&tracknum='.$item->tracking_number.'&requester=WT/';?>"><?php echo e($item->tracking_number); ?> </a> <?php elseif(!empty($item->tracking_number)): ?> <?php echo e($item->tracking_number); ?> <?php else: ?> N/A <?php endif; ?></td>	
                                <td><?php if(isset($item->note) && !empty($item->note)): ?><?php echo e($item->note); ?><?php else: ?> N/A <?php endif; ?></td>
                                <td>
                                    <?php
                                    foreach($orderStatus  as $list => $value){
										$getShippingStatus = !empty($item->shipping_status) ? $item->shipping_status : '';
                                        echo $selected = (($list == $getShippingStatus) && ($list != '')) ? ucwords($value) : '';
                                    } ?>
                                </td>
                                <td><?php echo e($item->date_of_shipping ? date('m/d/Y', strtotime($item->date_of_shipping)):'N/A'); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </table>
                </div>
						</div>
					
			</div>
		</div>
	</div>
</section>
<!--SECTION END:-->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>