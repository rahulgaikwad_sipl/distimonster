<?php $__env->startSection('content'); ?>
    <tr>
        <td align="center" valign="top" style="padding:0px;"><!--main section start-->
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:650px;max-width:100%;">
                <tr>
                    <td align="left" valign="top" bgcolor="#f5f5f5"><!--content section-->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:100%;">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                        <tr>
                                            <td align="left" valign="top" style="font:Bold 14px Arial, Helvetica, sans-serif; color:#666;padding:0px;background:#fff;padding:25px 25px;">
                                                <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                                    <tr><td style="height:25px;">&nbsp;</td></tr><!--spacing-->
                                                    <tr>
                                                        <td>
                                                            <table style="border:0 none;border-spacing:0;font-family:'Poppins', Arial, sans-serif;color:#000;font-weight:normal;text-align:left;" align="center" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td align="left" style="font-size:16px;color:#333;font-weight:normal;line-height:23px;padding-bottom:10px;">Hello <?php echo e($firstName); ?> <?php echo e($lastName); ?>, </td>
                                                                </tr>
                                                                <tr>
                                                                    <?php if($orderShippingStatus == 1): ?>
                                                                        <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Great news, your Order# <?php echo e($orderId); ?> has been updated with shipping information. Please log into your account and go to your Dashboard – Order History for shipment and tracking details. Current status of your order is <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.shipped')); ?>.If you have any question, Please
                                                                            <a href="<?php echo e(url('contact-us')); ?>"> Click Here</a> to contact us.</td>
                                                                    <?php elseif($orderShippingStatus == 2): ?>
                                                                        <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Your Order# <?php echo e($orderId); ?> has been updated with shipping information. Please log into your account and go to your Dashboard – Order History for shipment and tracking details. Current status of your order is <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.pending')); ?>.If you have any question, Please
                                                                            <a href="<?php echo e(url('contact-us')); ?>"> Click Here</a> to contact us. </td>
                                                                    <?php elseif($orderShippingStatus == 3): ?>
                                                                        <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Great news, your Order# <?php echo e($orderId); ?> has been updated with shipping information. Please log into your account and go to your Dashboard – Order History for shipment and tracking details. Current status of your order is <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.intransit')); ?>.If you have any question, Please
                                                                            <a href="<?php echo e(url('contact-us')); ?>"> Click Here</a> to contact us. </td>
                                                                    <?php elseif($orderShippingStatus == 4): ?>
                                                                        <td style="font-size:16px;color:#333;font-weight:normal;line-height:23px;color:#333;padding:2px 12px 40px 0" align="left">Better news, your Order# <?php echo e($orderId); ?> has been delivered. You can also view all details by logging into your account and viewing your Dashboard – Order History. Don’t forget about the super easy re-order option, you can re-order all items or pick the items you need directly from your order history. Current Order Status <?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.delivered')); ?>.If you have any question, Please
                                                                            <a href="<?php echo e(url('contact-us')); ?>"> Click Here</a> to contact us. </td>
                                                                    <?php endif; ?>
                                                                </tr>
                                                                
                                                            </table>
                                                        </td>
                                                    </tr><!--//single list-->
                                                    <!--//single list-->
                                                    <tr><td style="border-left:1px solid #f5f5f5;border-right:1px solid #f5f5f5;height:25px;">&nbsp;</td></tr><!--spacing-->
                                                </table>

                                            </td>
                                        </tr>
                                        <!--footer content-->
                                    </table><!--// center content-->
                                </td>
                            </tr>
                        </table><!--content section end-->
                    </td>
                </tr><!-- // main tr-->
            </table>
        </td><!-- //main section start-->
    </tr><!--//main wrap tr-->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('emails.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>