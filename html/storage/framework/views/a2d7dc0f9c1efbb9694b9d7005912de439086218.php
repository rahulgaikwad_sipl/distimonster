<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(url('admin/home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Net Account Requests</li>
        </ol>
    </section>

    <h3 class="page-title">Net Account Requests</h3>
    <div class="row">
        <div class="col-md-12">
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="col-sm-3">
                            <label for="status_id" class="control-label">Status: </label>
                        </div>
                        <div class="col-sm-8">
                            <select name="status_id" id="status_id" class="form-control drop_down">
                                <?php
                                foreach($netAccountStatus  as $list => $value){
                                    if($value == 'Select'){
                                        echo '<option value="'.$list.'">Select Status</option>';
                                    }else{
                                        echo '<option value="'.$list.'">'.ucwords($value).'</option>';
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <table border="0" width="100%"  class="table table-striped table-bordered table-hover testing" id="net_term_request_table" >
                <thead>
                <tr>
                    <th>User Name</th>
                    <th>Requested Account Type</th>
                    <th>Applied on Date </th>
                    <th>Request Status</th>
                    <th>Documents</th>
                </tr>
                </thead>
            </table>
        </div>
        <div class="modal fade" id="update-request-status" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Update Request Status</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <!-- column -->
                            <div class="col-md-12">
                                <!-- form start -->
                                <?php echo Form::open(['method' => 'POST', 'id'=>'updateRequestStatusForm','data-parsley-validate' => true]); ?>

                                    <div class="box-body">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="requestStatus" class="col-sm-4 control-label">Status:<span class="required">*</span></label>
                                                <div class="col-sm-8">
                                                    <?php echo Form::select('requestStatus', $netAccountStatus, null,array('id'=>'requestStatus','class' => 'form-control', 'data-parsley-required' => true)); ?>


                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-sm-6" id="accountDiv" style="display: none;">
                                            <div class="form-group">
                                                <label for="accountType" class="col-sm-4 control-label">Net Term:<span class="required">*</span></label>
                                                <div class="col-sm-8">
                                                    <?php echo Form::select('accountType', $accountType, null,array('id'=>'accountType','class' => 'form-control')); ?>

                                                </div>
                                            </div>
                                        </div>

                                        <input type="hidden" class="form-control" name="requestId" id="requestId" />
                                        <input type="hidden" id="_token" name="_token" value="<?php echo e(csrf_token()); ?>" />

                                    </div>
                                    <!-- /.box-body -->
                                <?php echo Form::close(); ?>

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn btn-default waves-effect update-data-from-delete-form">Close</button>
                        <a href="javascript:void(0)" onclick="netTermRequestModule.updateRequestStatus()" class="btn btn-primary pull-right"> Update </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
         $(document).ready(function() {
                netTermRequestModule.loadNetTermRequestDataTable();
         });

        $(document).on('change', '#status_id', function(){
            netTermRequestModule.loadNetTermRequestDataTableAgain();
        });

         $('#requestStatus').on('change', function () {
             if($(this).val() == 1){
                 $('#accountDiv').show();
                 $("#accountType").attr('required', 'required').parsley();
             } else {
                 $('#accountDiv').hide();
                 $("#accountType").removeAttr('required').val('').parsley().destroy();
             }
         })
    </script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>