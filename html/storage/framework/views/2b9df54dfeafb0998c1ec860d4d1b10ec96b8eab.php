
<?php $__env->startSection('title', '| Reset Password'); ?>
<?php $__env->startSection('content'); ?>

    <!--Register Section-->
    <section class="gray-bg">
        <div class="container clearfix">
            <div class="col-md-12">
                <div class="shdow-box">
                    <div class="register-box-content login-box-content">
                        <div class="row">
                            <div class="col-md-6 bdr-right">
                                <h2 class="block-title"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/login-icon.png')); ?>" class="img-responsive"></span>Reset Your Password</h2>
                                <?php echo Form::open(['method' => 'POST', 'id'=>'resetpassword_form', 'data-parsley-validate',]); ?>


                                <div class="form-group m-t15">
                                    <div class="col-md-11 p-l0">
                                        <?php echo Form::password('password', ['id'=>'password','class' => 'form-control', 'placeholder' => 'Password*','data-parsley-required','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.password_min'),'data-parsley-maxlength' => config('app.fields_length.password_max'),'data-parsley-minlength-message' => $validationMessage['password.min'],'data-parsley-maxlength-message' => $validationMessage['password.max']]); ?>

                                        <p class="help-block"></p>
                                        <?php if($errors->has('password')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('password')); ?>

                                            </p>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="form-group m-t15">
                                    <div class="col-md-11 p-l0">
                                        <?php echo Form::password('cpassword', ['class' => 'form-control', 'placeholder' => 'Confirm Password*','data-parsley-required','data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-equalto-message'=>'Please enter same as password.','data-parsley-equalto'=>"#password" ]); ?>

                                        <p class="help-block"></p>
                                        <?php if($errors->has('cpassword')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('cpassword')); ?>

                                            </p>
                                        <?php endif; ?>
                                    </div>
                                </div>

                            <?php echo Form::submit('Save New Password', ['class' => 'btn btn-primary m-t15','title'=>'Save New Password']); ?>

                            <?php echo Form::close(); ?>

                            <!-- /form -->
                            </div>
                            <div class="col-md-6">
                                <div class="left-pad-content">
                                    <h2 class="block-title">Already have an account?</h2>
                                    <p>If you already have an account with us, just click “Login” below, and we’ll take you to your account in no time. Simply you need to enter your registered email address and password for accessing account.</p>
                                    <div class="form-group">
                                        <div class="col-md-11 p-l0">
                                            <a  href="<?php echo e(url('login')); ?>" class="btn btn-outline btn-primary" title="Login">Login</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--Register Section-->







<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>
        $('#forgotpassword_form').parsley();
    </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>