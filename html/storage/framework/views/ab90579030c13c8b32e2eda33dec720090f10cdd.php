<!DOCTYPE html>
<html lang="en">
   <head>
      <?php echo $__env->make('frontend.partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
   </head>
   <body>
      <div class="entire-site">
         <div style="display: none;" class="loader"></div>
         <?php echo $__env->make('frontend.partials.topbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->make('frontend.partials.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <?php echo $__env->make('frontend.partials.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
         <style>body{margin: 0;padding: 0;}</style>
         <section class="gray-bg enginer-content">
            <div class="container-fluid clearfix">
               <div class="col-md-12">
                  <div class="row row-trans">
                     <div class="col-md-12">
                        <h3 class="text-center comg-soon-txt">COMING SOON</h3>
                     </div>
                     <div class="d-md-flex justify-content-center m-auto">
                        <div class="eng-left-side">
                           <h5 class="text-center">EDA DESIGN<br>IN DISTIMONSTER</h5>
                           <div class="">
                              <ul class="list-unstyled mt-4 mb-md-4 mb-5">
                                 <li>
                                    <i class="fa fa-check"></i>
                                    <span>PCB design/buy components via DistiMonster</span>
                                 </li>
                                 <li>
                                    <i class="fa fa-check"></i>
                                    <span>build/share Monster-BOM</span>
                                 </li>
                                 <li>
                                    <i class="fa fa-check"></i>
                                    <span>one PO for all components</span>
                                 </li>
                                 <li>
                                    <i class="fa fa-check"></i>
                                    <span>5M data sheets</span>
                                 </li>
                                 <li>
                                    <i class="fa fa-check"></i>
                                    <span>cross-reference data*</span>
                                 </li>
                              </ul>
                           </div>
                           <p class="mb-0 f-s12">*COMING OCTOBER 1, 2019</p>
                        </div>
                        <div class="eng-midd-side mt-sm-5 pt-sm-3 mt-md-0 pt-md-0">
                           <div class="cloud-ims">
                              <img src="frontend/images/cloud.png" alt="cloud" class="img-fluid">
                           </div>
                           <div class="col-md-12">
                              <div class="email-noti-zone">
                                 <img src="frontend/images/MONSTER_BOM.png" alt="MONSTER_BOM" class="img-fluid mr-3">
                                 <img src="frontend/images/PO.png" alt="PO" class="img-fluid"> 
                              </div>
                              <a href="javascript:void(0)" class="email-noti-btn" title="Get email alert" id="get_email_alert">GET EMAIL ALERT</a>
                           </div>
                        </div>
                        <div class="eng-right-side text-center d-flex align-items-center justify-content-center">
                           <p>1 SOURCE<br>1 PO<br>1 SHIPMENT</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </section>
      </div>
      <div aria-labelledby="modalLabel" id="newletter_email_modals" class="modal" role="dialog" tabindex="-1" style="z-index: 1050;">
         <div class="modal-dialog" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <div class="modal-title">
                     <h5 class="m-b0">SIGN UP for the DISTIMONSTER Newsletter</h5>
                  </div>
                  <button aria-label="Close" class="close" data-dismiss="modal" type="button">
                    <span aria-hidden="true">
                      <img src="<?php echo e(url('frontend/assets/images/close-icon.png')); ?>" alt="close-icon">
                    </span>
                 </button>
               </div>
               <div class="modal-body">
                  <?php echo Form::open(['method' => 'post','class'=>"newsletter-form" ,'id'=>"newsletter_subscribe_engineering",'data-parsley-validate' => true]); ?>

                  <p>Get the latest news and information related to our engineering EDA integration.</p>
                  <div class="newsletter-form-item">
                     <?php echo Form::email('emailNewsletter', old('emailNewsletter'), ['pattern' => config('app.patterns.email'),'data-parsley-errors-container'=>'#errorContainerEmailAlert' ,'id'=>'emailNewsletterEngineering','class' => 'form-control','placeholder' => 'Email Address','data-parsley-required', 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE') ]); ?>

                     <a onclick="footer.signUpNewsLetterEngineering()" class="newsletter-btn btn mt-3 text-white mx-auto d-block w-25"> Signup </a>
                     
                  </div>
                  </form> 
                  <div id="errorContainerEmailAlert"></div>
               </div>
               <div class="modal-footer p-t0"></div>
            </div>
         </div>
      </div>
      <?php echo $__env->make('frontend.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php echo $__env->yieldContent('javascript'); ?>
      <script>
         $("#get_email_alert").click(function(){
             $("#newsletter_subscribe_engineering").parsley().reset();
            $('#newletter_email_modals').modal();
         });
      </script>
   </body>
</html>