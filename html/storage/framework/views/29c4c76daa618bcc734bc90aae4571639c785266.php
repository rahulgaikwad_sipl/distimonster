<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user_edit')): ?>
    <a href="<?php echo e(url('/admin/quote-details/'.$quote->id)); ?>" class="btn btn-xs btn-primary"><i class="fa fa-eye" aria-hidden="true"></i></a>
<?php endif; ?>