<!--START: Banner-->
<div class="banner custom-header what-is-distimonster">
    <img src="<?php echo e(url('frontend/assets/images/what-is-banner.jpg')); ?>" class="w-full img-responsive d-none d-sm-block">
    <div class="what-banner-caption">
        <h3>
            WE ARE on a mission to simplify <br>design and procurement.
        </h3>
        <div class="row">
            <div class="col-md-8 col-sm-12">
                <p>
                    DistiMonster makes it simple for engineers and buyers to search and buy all types of electronic
                    components all on one site. It’s what you asked for and we delivered.</p>
                <p> We listened to what engineers and buyers want and need to perform their jobs effectively.<br>
                    You asked for simple—that’s DistiMonster.
                </p>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div>
    <div class="clearfix">&nbsp;</div>
</div>
</div>
<!--END: Banner-->
<h2 class="index-heading text-center m-t15 font-back">SIMPLE — 1 SOURCE, 1 PO, 1 SHIPMENT — DISTIMONSTER.</h2>
<section class="p-b30">
    <div class="container">
         <div class="row m-b15 m-t15">
            <div class="col-md-3">
                <img src="<?php echo e(url('frontend/assets/images/side-img-three.png')); ?>" class="img-responsive img-box-effect">
            </div>
            <div class="col-md-9">
                <div class="right-been-asking">
                    <p class="buyer-text-new">
                        For the BUYER: Search and procure millions of parts from authorized
                        distributors and component manufacturers on ONE site. Simplify your workload and save time with a single PO for your entire project. Use Monster-BOM to upload a parts list or you can search for individual components. Use your personalized dashboard
                        to access information about:
                    </p>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="right-been-asking">
                            <ul>
                                <li>easy reorder</li>
                                <li>order status/history</li>
                                <li>PCN/EOL notifications<em>*</em></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="right-been-asking">
                            <ul>
                                <li>shipping: find details;
                                    information add/change</li>
                                <li>view/print invoices</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr style="margin-top:1.25rem" />
        <div class="row">
            <div class="col-md-3">
                <img src="<?php echo e(url('frontend/assets/images/side-img-four.png')); ?>" class="img-responsive img-box-effect">
            </div>
            <div class="col-md-9">
                <div class="right-been-asking">
                    <p>
                        For the ENGINEER: A seamless, single source for EDA design and part lifecyle management through NPI fulfillment. Cross-reference data*, build your Monster-BOM and purchase parts using your personalized dashboard. Save search time with access to millions of data sheets; easily share designs with anyone with a single click. 
                    </p>
                    <p>
                        DistiMonster is a straightforward licensed-based service. Pay a flat monthly or yearly rate with no additional product markup.
                    </p>
                    <p><small>*Available October 1, 2019 with purchase/upgrade of Pro Plan.</small></p>
                </div>
            </div>
        </div>        
    </div>
</section>
<!--START: Banner Bottom-->
<div class="why-distimonster-banner">
   <h2 class="index-heading text-center green-bg free-text">WHY DISTIMONSTER MAKES SENSE.</h2>
      <div class="banner what-is-distimonster">
               <img src="<?php echo e(url('frontend/assets/images/dm-main-banner.jpg')); ?>" class="w-full img-responsive main-banner-dm-img"> 
               <div class="container">
               <div class="row">
                <div class="col-lg-6">
                    <div class="row top-details-copy">
                        <div class="col-6 dm-copy-img">
                                <img src="<?php echo e(url('frontend/assets/images/dm-info-graph.png')); ?>" class="w-full img-responsive">
                        </div>
                            <div class="col-6 dm-copy-text">
                                <div class="single-project">
                                    <p><span class="dmsg-text">SINGLE PROJECT</span> 5 DISTRIBUTORS <br />5 POs x $160 = $800</p>
                                </div>
                                <div class="single-project single-project-disti">
                                    <p>DISTIMONSTER <br />1 PO</p>
                                </div>
                            </div>
                    </div>
                    <div class="row top-details-copy bottom-details-copy">
                        <div class="col-lg-12 dm-copy-text">
                            <p>With an average cost of $160 per PO, companies see an immediate savings in labor costs using DistiMonster. Generate one PO and procure all components per project through DistiMonster.</p>
                            <p>Why subscribe to multiple data sites when you can use DistiMonster for all your search, data information and procurement needs.</p>
                        </div>
                    </div>
                </div>
               <div class="col-lg-6"></div>
               </div>
               </div>
      </div>
   </div>
</div>
<!--END: Banner Bottom-->