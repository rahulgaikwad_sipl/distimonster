    
    <?php
    $part =  app('request')->input('partNumber');
    ?>
    <?php if($part): ?>
            <?php $__env->startSection('title', ' | '.$part); ?>
        <?php else: ?>
            <?php $__env->startSection('title', ''); ?>
    <?php endif; ?>
    <?php $__env->startSection('content'); ?>
        <!--START: Products list-->
        <section class="gray-bg p-t15">
            <div class="container clearfix">
            </div>
            <?php if(isset($bom_id)): ?>
				<div class="container-fluid">
				<div class="row">
                <div class="col-lg-10 text-left p-b15" id="add_all_to_quote">
				<div class="form-inline">
                    <?php if(Auth::check()): ?>
						
					
                    <!-- <button type="button" class="m-t9 btn btn-primary btn-outline p-tb5 " onclick="addProduct('addAllSelectedPartsToCart');" title="Adds items to cart for immediate purchase"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/cart-small-icon.png')); ?>" alt="add-quote-icon" /></span>Add all to Cart</button> -->
                  

				<div class="form-group"> 
    <div class="col-sm-12">
      <div class="checkbox">
	   <label class="f-s14 font-weight-bold text-blue mt-3"  title="All items with available stock will be added to your cart">
                    <input type="checkbox" class="selectall m-r5" id = "orderAllitem" disabled="disabled"  onclick="addProduct('addAllSelectedPartsToCart', this); " style="margin-top: -20px;"/>
           SELECT ALL ITEMS TO ORDER <br> BASED ON BEST PRICE</label>
		   
       
      </div>
    </div>
  </div>      <?php else: ?>
                    <?php $redirectUrl = $_SERVER['REQUEST_URI'].'/'.$bom_id;?>
                    <button class="m-t9 btn btn-primary btn-outline p-tb5 " type="button" name="button"  onclick="window.location.href='<?php echo e(url('login'.$redirectUrl)); ?>'" value="" title="Adds items to cart for immediate purchase"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/cart-small-icon.png')); ?>" alt="add-quote-icon" /></span> Add all to Cart </button>
                    <?php endif; ?>
					<div class="form-group"> 
    <div class="col-sm-12">
      <div class="checkbox">
	   <label class="text-blue f-s14 font-weight-bold" title="Request lead times for items out of stock">
         <input type="checkbox" class="selectall m-r5" id = "quoteAllitem" disabled="disabled" onclick="addProduct('getAllPartForQuote', this);"/>
                 
				ADD ITEMS TO RFQ</label>
		   
       
      </div>
    </div>
  </div>
  
                    
             
					 

<div class="form-group"> 
    <div class="col-sm-12">
      <div class="checkbox">
	   <label class="text-blue f-s14 font-weight-bold" title="Request a quote on items that are unidentified">
                            <input type="checkbox" class="selectall m-r5" id = "addUnavailableAllitem" disabled="disabled" onclick="addProduct('addUnavailableProductToQuote', this);"/>
   ADD UNIDENTIFIED ITEMS TO RFQ</label>
		   
       
      </div>
    </div>
  </div>                   
				<span class="text-right m-r9">
                        <button type="button" id="add_mpn" class="btn btn-primary btn-outline p-tb5 requestquote-btn-custom" title="Submit" onclick="itemSelection();">Submit</button>
                     </span>
					 </div>
                </div>
                 <div class="col-lg-2">      
				 <button type="button" id="save_bom" class="btn btn-primary btn-outline p-tb5 requestquote-btn-custom pull-right" title="<?php echo e(Request::segment(1) == 'bom' ? 'SAVE' : 'UPDATE'); ?> MONSTER-BOM" data-target="#save-bom-modal" data-toggle="modal"><?php echo e(Request::segment(1) == 'bom' ? 'SAVE' : 'UPDATE'); ?> MONSTER-BOM</button>    
       </div>
	   </div>
	   </div>
            <?php endif; ?>
            <div class="col-lg-12">
                <div class="table-dresponsive">
                    <table class="table  table-bom-product table-striped product-list-table table-fixed">
                        <tbody>
                        <tr class="f-s16 font-w500 black">
                        <?php /*     
                        @if(isset($bom_id))<th class="v-align text-center"><div class="select-all">Select All<br/><input  class="checked_all" type="checkbox" value="1" /></div></th>@endif */ ?>
                            <?php if(isset($bom_id)): ?>
                            <th class="v-align text-center">Item Selection</th>
                            <?php endif; ?>
                            <th>Distributor</th>
                            <th width="200">Part#</th>
                            <th>Price</th>
                            <th width="150">Quantity</th>
                            <th>Stock</th>
                            <th>Description</th>
                            <th width="150">Category</th>
                        </tr>

                        <?php $__currentLoopData = $finalData; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
						   
                            <?php if(isset($data['partNum'])): ?>
                                <form name="bompart_<?php echo e($data['partNum']); ?>" id="bompart_<?php echo e(str_replace(' ', '_', $data['itemId'])); ?>_<?php echo e(str_replace(' ', '_', $data['mfrCd'])); ?>" data-parsley-validate="true" >
                                    <?php $availabilityQty = 0; ?>
                                    <?php if(!empty($data['sources'])): ?>
                                        <?php $__currentLoopData = $data['sources']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sources): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $__currentLoopData = $sources['sourcesPart']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sellerInfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <?php if($sellerInfo['inStock']): ?>
                                                    <?php  $availabilityQty =  $availabilityQty+$sellerInfo['Availability'][0]['fohQty'];?>
                                                <?php endif; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>
                                    <tr class="f-s14 text-gray">
                                    <?php  $lowestPrice = 0; ?>    
                                        <?php if(Auth::check()): ?>    
                                            <?php if(!empty($data['sources'])): ?>
                                                <?php $__currentLoopData = $data['sources']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sources): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php
                                                    $lowestPrice =  \Helpers::getLowestPrice($sources);
                                                    ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        <?php endif; ?>  
                                        <?php if(isset($bom_id)): ?>
                                            <input type="hidden" name="bomId" value="<?php echo e($bom_id); ?>" />
                                            
                                            <td class="text-center">
											
											
											<?php
												/*print_r($data['bestPrice']);
												if(!empty($data['bestPrice']){
													if($data['mpnFound']){
														$data['requestedQty'] = 
													}
												}*/
												?>
											
											<?php if(!empty($data['bestPrice']) || $availabilityQty < 0): ?>
                                            <div>Select Item
                                            <input type="checkbox" class="checkbox-qty <?php echo e($availabilityQty > 0 ? 'in_stock' : 'out_of_stock'); ?>" data-requestedPrice="<?php echo e($lowestPrice); ?>" data-distributor="<?php echo e($data['distributorName']); ?>" data-formid="bompart_<?php echo e(str_replace(' ', '_', $data['itemId'])); ?>_<?php echo e(str_replace(' ', '_', $data['mfrCd'])); ?>" data-availabledatainput="availableData_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" data-datecode="<?php echo $sources['sourcesPart'][0]['dateCode'];?>" data-resalelistinput="resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>"  data-instock="<?php echo Helpers::getInStockFromSources($sources['sourcesPart']);?>"  data-customerpartnoinput="<?php echo !empty($data['internalPart']) ? $data['internalPart'] : '';?>" data-sourcepartid="<?php echo e($sources['sourcesPart'][0]['sourcePartId']); ?>" data-partnum="<?php echo $data['partNum']; ?>"  data-manufacturer="<?php echo $data['manufacturer']; ?>"   data-fohqty="<?php echo $sources['sourcesPart'][0]['Availability'][0]['fohQty']; ?>" data-qtyinput="quantity_<?php echo $data['itemId']; ?>"  data-source="<?php echo base64_encode(json_encode($data['sources'])); ?>" data-itemid="<?php echo e($data['itemId']); ?>" data-availabilityqty="<?php echo e($availabilityQty); ?>" data-partnum="<?php echo e($data['partNum']); ?>" data-mfrcd="<?php echo e(str_replace(' ', '_', $data['mfrCd'])); ?>"  id="checkbox_<?php echo e($data['itemId']); ?>"/>
                                            </div>
											<?php endif; ?>
                                        </td>
                                        <?php endif; ?>
                                        <?php if(!empty($data['distributorName'])): ?>
                                        <td><strong><?php echo e($data['distributorName']); ?></strong><input type="hidden" name="distributorName" id="distributorName" value="<?php echo e($data['distributorName']); ?>"/></td>
                                        <?php else: ?>
                                        <td>N/A</td>
                                        <?php endif; ?>
                                        <td>
                                            <div class="hind-font text-pink" href="javascript:void(0)">MPN: <?php echo e($data['partNum']); ?></div>
                                            <div class="block m-t5 m-b10">
                                                <?php if(!empty ($data['EnvData']['compliance'])): ?>
                                                    <?php if($data['EnvData']['compliance'][0]['displayLabel'] != ""): ?>
                                                        <span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/rohslogo.png')); ?>" alt="pending" /></span>RoHS Compliant
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                            <?php   $sheetData =  \Helpers::getDataSheet($data['partNum'],$data['manufacturer']);?>
                                            <div class="block m-t10 m-b10">
                                                <?php if(!empty($sheetData) && Auth::check()): ?>
                                                    <a target="_blank" href="<?php echo e($sheetData[0]['datasheet_url']); ?>" class="data-sheet"><img src="<?php echo e(url('/frontend/assets/images/pdf-icon.png')); ?>" width="12px"> Datasheet</a>
                                                <?php else: ?>
                                                <?php $redirectUrl = $_SERVER['REQUEST_URI'];?>
                                                    <a target="_blank" href="javascript:void(0);" class="data-sheet" onclick="window.location.href='<?php echo e(url('login'.$redirectUrl)); ?>'"><img src="<?php echo e(url('/frontend/assets/images/pdf-icon.png')); ?>" width="12px"> Datasheet</a>
                                                <?php endif; ?>
                                            </div>
                                            Manufacturer: <span class="black font-w500"><?php echo e($data['manufacturer']); ?></span><br>
                                            <?php if(!empty($data['internalPart'])) { ?>
                                                Internal Part: <span class="black font-w500"><?php echo e($data['internalPart']); ?></span>
                                            <?php } ?>
                                            
                                        </td>
                                        <td>
                                        <?php if(Auth::check()): ?>
                                        <span class="black font-w500" id="item_price_div_25360740">
                                        <?php  $lowestPrice = 0; ?>
                                                <?php if(!empty($data['sources'])): ?>
                                                    <?php $__currentLoopData = $data['sources']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sources): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <?php
                                                        $lowestPrice =  \Helpers::getLowestPrice($sources);
                                                        ?>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                <?php endif; ?>
                                               $<?php echo number_format($lowestPrice, 3, '.', ','); ?>
                                        </span>
                                            <br>
                                            <!-- Large modal -->
                                        <!-- Show Buying option only if  instock-->
                                        <?php if($availabilityQty): ?><a class="" onclick="alogoForQtySeperation(<?php echo e(json_encode($data['sources'])); ?>,'<?php echo e($data['itemId']); ?>',<?php echo e($availabilityQty); ?>, '','<?php echo e(str_replace(' ', '_', $data['mfrCd'])); ?>', '<?php echo !empty($data['distributorName'])?$data['distributorName']:'N/A'; ?>')" data-target=".bs-example-modal-lg-<?php echo e($data['itemId']); ?>" data-toggle="modal" href="#" type="button">Price Breaks</a><?php endif; ?>
                                            <div aria-labelledby="myLargeModalLabel" class="modal fade bs-example-modal-lg-<?php echo e($data['itemId']); ?>" role="dialog" tabindex="-1">
                                                <?php echo $data['partNum'];?>
                                                <div class="modal-dialog modal-lg modal-custom" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header bdr-none">
                                                            <div class="modal-title">
                                                                <h4>Price Breaks for <?php echo e($data['partNum']); ?></h4>
                                                            </div>
                                                            <button aria-label="Close" class="close" data-dismiss="modal" type="button"><span aria-hidden="true"><img src="<?php echo e(url('frontend/assets/images/close-icon.png')); ?>" alt="close-icon"></span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="row p-b5">
                                                                <div class="col-md-6"><span class="f-s18 text-gray">Quantity: </span><span class="black font-w500 f-s18 requestQtyDiv pinck-color" id="requestQtyDiv"></span> </div>
                                                                <div class="col-md-6 text-right">
                                                                    <span class="f-s18 text-gray">Available stock: </span><span class="text-right black font-w500 f-s18  green-color" id="totalStockQty"> <?php echo e($availabilityQty); ?></span>
                                                                </div>
                                                            </div>
                                                            <hr>
                                                            <div class="show-data">
                                                                <?php if(!empty($data['sources'])): ?>
                                                                    <table cellspacing="0" cellpadding="0" border="0" class="buying-options-table">
                                                                        <tr>
                                                                            
                                                                            <th>Distributor</th>
                                                                            <th>Min Quantity</th>
                                                                            <th>Price</th>
                                                                            <th>Available Qty</th>
                                                                            <th>Quantity</th>
                                                                        </tr>
                                                                        <?php $__currentLoopData = $data['sources']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sources): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <?php $__currentLoopData = $sources['sourcesPart']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$sellerInfo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                            <!--removed check if item not in stock-->
                                                                                <?php if(isset($sellerInfo['Prices']) && $sellerInfo['Availability'][0]['fohQty'] >0): ?>
                                                                                     <?php if(!empty($sellerInfo['Prices']['resaleList'])): ?>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <?php $minQty = 0; ?>
                                                                                            <?php  $minQty = $sources['sourcesPart'][$key]['minimumOrderQuantity'];?>
                                                                                            <?php echo !empty($sellerInfo['displayName'])?$sellerInfo['displayName']:'N/A'; ?>
                                                                                            
                                                                                            <input class="bor-radius-none quantity form-control quantity-box internal-part_pop<?php echo e($data['itemId']); ?>_<?php echo e(str_replace(' ', '_', $data['mfrCd'])); ?>" id="customerPartNo_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>" name="customerPartNo[]" placeholder="Internal part#" data-parsley-maxlength="30"  value="<?php echo !empty($data['internalPart']) ? $data['internalPart'] : '';?>" />
                                                                                                <input  type="hidden" class="bor-radius-none quantity form-control quantity-box" value="<?php echo e($minQty); ?>" id="sourceMinQty_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>" name="sourceMinQty[]" placeholder="Source Min Qty" data-parsley-maxlength="10"  />
                                                                                        </td>
                                                                                        <td>
                                                                                            <?php $__currentLoopData = $sellerInfo['Prices']['resaleList']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $resaleList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                            <?php echo  $resaleList['minQty']; ?>+ <br/>
                                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?php $__currentLoopData = $sellerInfo['Prices']['resaleList']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $resaleList): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                                            $<?php echo  !empty($resaleList['price']) ? $resaleList['price'] : ''; ?> <br/>
                                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?php if($sellerInfo['Availability'][0]['fohQty'] >0): ?>
                                                                                                <?php  echo $sellerInfo['Availability'][0]['fohQty']; ?><br />
                                                                                                <span class="green-color"><?php  echo $sellerInfo['Availability'][0]['availabilityMessage']; ?></span>
                                                                                            <?php else: ?>
                                                                                                <?php  echo $sellerInfo['Availability'][0]['fohQty']; ?><br />
                                                                                                <span class="red-color"><?php  echo $sellerInfo['Availability'][0]['availabilityMessage']; ?></span>
                                                                                            <?php endif; ?>
                                                                                        </td>
                                                                                        <td>
                                                                                            <?php if($sellerInfo['Availability'][0]['fohQty'] >0): ?>
                                                                                            <input class="bor-radius-none customerPartNo form-control quantity-box pop-quantity-input pop-quantity-input_<?php echo e($data['itemId']); ?>_<?php echo e(str_replace(' ', '_', $data['mfrCd'])); ?>" onpaste="return false;" onkeypress="return isNumber(event)" data-source="<?php echo e(str_replace(':', '_',$sellerInfo['sourcePartId'])); ?>" data-parsley-min="<?php echo e($minQty); ?>"  data-parsley-max="<?php echo e($sellerInfo['Availability'][0]['fohQty']); ?>"  id="quantity_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>" name="quantities[]" value=""  placeholder="Quantity" />
                                                                                            <?php else: ?>
                                                                                                <input class="bor-radius-none customerPartNo form-control quantity-box pop-quantity-input pop-quantity-input_<?php echo e($data['itemId']); ?>_<?php echo e(str_replace(' ', '_', $data['mfrCd'])); ?>" onpaste="return false;" onkeypress="return isNumber(event)" data-source="<?php echo e(str_replace(':', '_',$sellerInfo['sourcePartId'])); ?>" data-parsley-max="<?php echo e($sellerInfo['Availability'][0]['fohQty']); ?>"  id="quantity_<?php echo str_replace(':', '_',$sellerInfo['sourcePartId']);?>" name="quantities[]" value=""  placeholder="Quantity" />
                                                                                            <?php endif; ?>

                                                                                            <span id="message_<?php echo e(str_replace(':', '_',$sellerInfo['sourcePartId'])); ?>" class="error-max-qty text-red" style="display: none; color: red; font-size: 11px;">Entered quantity is more than available quantity.<br/></span>
                                                                                             <i class="info-text">Increments of 1 <br />Minimum <?php echo $minQty; ?></i>
                                                                                             <input type="hidden" name="productId[]" value="<?php echo e($data['partNum']); ?>" />
                                                                                            <input type="hidden" name="manufacturer[]" value="<?php echo e($data['manufacturer']); ?>" />
                                                                                            <input type="hidden" name="mfrCd[]" value="<?php echo e($data['mfrCd']); ?>" />
                                                                                            <input type="hidden" name="sourcePartId[]" value="<?php echo e($sellerInfo['sourcePartId']); ?>" />
                                                                                            <input type="hidden" name="stockAvailability[]" value="<?php echo e($sellerInfo['inStock']); ?>" />
                                                                                            <input type="hidden" name="dateCode[]" value="<?php echo e($sellerInfo['dateCode']); ?>" />
                                                                                        </td>
                                                                                    </tr>
                                                                                <?php endif; ?>
                                                                                <?php endif; ?>
                                                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                    </table>
                                                                <?php endif; ?>
                                                            </div>
                                                        </div>
                                                        <?php if(!empty($data['sources'])): ?>
                                                            <?php if($availabilityQty >0): ?>
                                                                <div class="modal-footer p-t0"><button type="button" onClick="submitAddCart('<?php echo e(str_replace(' ', '_', $data['itemId'])); ?>_<?php echo e(str_replace(' ', '_', $data['mfrCd'])); ?>')" class="btn btn-primary" title="Adds items to cart for immediate purchase">Done & Add to cart</button></div>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="productDetails" value="<?php echo e(json_encode($data)); ?>"></input>
                                            <?php else: ?>
                                            <?php if(isset($bom_id)): ?> 
                                                <?php $bomId = '/'.$bom_id;?>
                                            <?php else: ?>
                                                <?php $bomId = '';?>
                                            <?php endif; ?>
                                            <?php $redirectUrl = $_SERVER['REQUEST_URI'];?>
                                            <a class="m-t9 btn btn-primary btn-outline p-tb5 " href="<?php echo e(url('login'.$redirectUrl.$bomId)); ?>" title="Please login to view the price.">Login</a>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <div class="text-center">
                                                <?php if($availabilityQty>0): ?>
                                                <input class="bor-radius-none quantity form-control quantity-box quantity-input" onpaste="return false;" onkeypress="return isNumber(event)" id="quantity_<?php echo e($data['itemId']); ?>" data-quantity="<?php echo e($availabilityQty?$availabilityQty:0); ?>" name="quantity"  data-parsley-type="digits"  required  max='<?php echo e($availabilityQty?$availabilityQty:0); ?>'  min="1" placeholder="Enter Quantity" value="<?php echo e($data['requestedQty']); ?>">
                                                <span id="message_<?php echo e($data['itemId']); ?>" class="error-max-qty text-red" style="display: none; color: red; font-size: 11px;">Entered quantity is more than available quantity.<br/></span>
                                                <?php else: ?>
                                                    <input class="bor-radius-none quantity form-control quantity-box quantity-input" onpaste="return false;" onkeypress="return isNumber(event)" id="quantity_<?php echo e($data['itemId']); ?>" data-quantity="<?php echo e($availabilityQty?$availabilityQty:0); ?>" name="quantity"  data-parsley-type="digits"  required   placeholder="Enter Quantity" value="<?php echo e($data['requestedQty']); ?>">
                                                    <span id="message_<?php echo e($data['itemId']); ?>" class="error-max-qty text-red" style="display: none; color: red; font-size: 11px;">Entered quantity is more than available quantity.<br/></span>
                                                <?php endif; ?>
                                                    <input  type="hidden" class="bor-radius-none customerPartNo form-control quantity-box" id="customerPartNoID_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" name='customerPartNoQuote' value='' placeholder="Internal Part#"  />
                                                <?php if(isset($sources['sourcesPart'][0]['Prices'])): ?>
                                                    <input  type="hidden" class="bor-radius-none customerPartNo form-control quantity-box" value='<?php echo json_encode($sources['sourcesPart'][0]['Prices']['resaleList'])?>' id="resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" name='resaleList'  placeholder="resaleList" />
                                                <?php else: ?>
                                                    <input  type="hidden" class="bor-radius-none customerPartNo form-control quantity-box" value='<?php echo json_encode([])?>' id="resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" name='resaleList'  placeholder="resaleList" />
                                                <?php endif; ?>
                                                <input type="hidden" class="bor-radius-none customerPartNo form-control quantity-box"  value='<?php echo json_encode($sources['sourcesPart'][0]['Availability'])?>' id="availableData_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" name='availableData'  placeholder="availableData" />

                                                <?php if($availabilityQty): ?>
                                                
                                                    <?php if(Auth::check()): ?>
                                                    <?php $distributorName = !empty($data['distributorName'])?$data['distributorName']:config('constants.DEFAULT_DISTRIBUTOR_NAME');?>
                                                    <button class="btn btn-primary p-tb5 m-t10" type="button" onclick="alogoForQtySeperation(<?php echo e(json_encode($data['sources'])); ?>,'<?php echo e($data['itemId']); ?>','<?php echo e($availabilityQty); ?>','<?php echo e(str_replace(' ', '_',$data['partNum'])); ?>','<?php echo e(str_replace(' ', '_', $data['mfrCd'])); ?>', '<?php echo e($distributorName); ?>', true)" name="button" value="" title="Adds items to cart for immediate purchase"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/cart-bag-icon.png')); ?>" alt="add-quote-icon"></span> Add to cart </button>
                                                    <?php else: ?>
                                                    <button class="btn btn-primary p-tb5 m-t10" type="button" name="button"  onclick="window.location.href='<?php echo e(url('login'.$redirectUrl)); ?>'" value="" title="Adds items to cart for immediate purchase"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/cart-bag-icon.png')); ?>" alt="add-quote-icon"></span> Add to cart </button>
                                                    <?php endif; ?>
                                                <?php endif; ?>

                                                <input type="hidden" class="all-quote-items" data-allquote="1"  data-fohqty="<?php echo $sources['sourcesPart'][0]['Availability'][0]['fohQty']; ?>" data-qtyinput="quantity_<?php echo $data['itemId']; ?>"  data-partnum="<?php echo $data['partNum']; ?>"  data-manufacturer="<?php echo $data['manufacturer']; ?>"  data-mfrcd="<?php echo e($data['mfrCd']); ?>" data-sourcepartid="<?php echo e($sources['sourcesPart'][0]['sourcePartId']); ?>" data-customerpartnoinput="<?php echo !empty($data['internalPart']) ? $data['internalPart'] : '';?>"  data-instock="<?php echo $sources['sourcesPart'][0]['inStock']?>"  data-resalelistinput="resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" data-availabledatainput="availableData_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>" data-datecode="<?php echo $sources['sourcesPart'][0]['dateCode'];?>"/>

                                                <?php if(!empty($sources['sourcesPart'][0]['Prices'])): ?>
                                                     <?php if(Auth::check()): ?>
                                                        <button type="button" id="addToQuote"  onclick="productListModule.addToQuoteProductListing('<?php echo !empty($data['distributorName'])?$data['distributorName']:'N/A'; ?>', '<?php echo $sources['sourcesPart'][0]['minimumOrderQuantity']?>', '<?php echo $sources['sourcesPart'][0]['Availability'][0]['fohQty']; ?>','quantity_<?php echo $data['itemId']; ?>' ,'<?php echo $data['partNum']; ?>','<?php echo $data['manufacturer']; ?>', '<?php if(isset($data['mfrCd'])){echo $data['mfrCd'];}else{ echo $data['manufacturer']; } ?>','<?php echo $sources['sourcesPart'][0]['sourcePartId'];?>','<?php echo !empty($data['internalPart']) ? $data['internalPart'] : $sources['sourcesPart'][0]['sourcePartId'];?>','<?php echo $sources['sourcesPart'][0]['inStock']?>' ,'resaleList_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>','availableData_<?php echo  str_replace(':', '_', $sources['sourcesPart'][0]['sourcePartId']); ?>','<?php echo $sources['sourcesPart'][0]['dateCode'];?>', '<?php echo $availabilityQty > 0 ? 'in_stock' : 'out_of_stock'; ?>')" class="m-t9 btn btn-primary btn-outline p-tb5" title="<?php echo e(!empty($availabilityQty) ? 'Request specific prices' : 'Request Lead Time'); ?>"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/add-quote-icon.png')); ?>" alt="add-quote-icon" /></span>Add to RFQ</button>  
                                                     <?php else: ?>
                                                     <button type="button" id="addToQuote"  onclick="window.location.href='<?php echo e(url('login'.$redirectUrl)); ?>'" class="m-t9 btn btn-primary btn-outline p-tb5 " title="Add to RFQ"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/add-quote-icon.png')); ?>" alt="add-quote-icon" /></span>Add to RFQ</button>
                                                    <?php endif; ?>
                                                <?php endif; ?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php if($availabilityQty): ?>
                                                <span class="f-s14 success font-w500">
                                                (<?php echo $availabilityQty; ?> Available)
                                                    <input type="hidden" name="totalAvailableQty" value="<?php echo e($availabilityQty); ?>" />
                                                </span>
                                            <?php else: ?>
                                                <span class="f-s14 cancelled font-w500">Out of stock
                                                    <input type="hidden" name="totalAvailableQty" value="0" />
                                                </span>
                                            <?php endif; ?>
                                        </td>
                                        <td><?php echo e($data['desc']); ?></td>
                                        <td width="350">
                                            <?php if(!empty($sheetData)): ?>
                                                <?php echo e($sheetData[0]['category']); ?>

                                            <?php else: ?>
                                                N/A
                                            <?php endif; ?>
                                        </td>
                                    </tr>
                                </form>
                            <?php else: ?>
                            <?php endif; ?>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php if(isset($noPartFound)): ?>
                            <?php $__currentLoopData = $noPartFound; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr class="f-s14 text-gray">
                                    <td class="v-align text-center">
                                    <div>Select Item</div>
                                        <input type="hidden" value="1" />
                                        <input type="checkbox" class="checkbox-qty checkbox-nopart-found" data-internalpart="<?php echo e(!empty($value['internalPart']) ? $value['internalPart'] : ''); ?>" data-xlxsquantity="<?php echo e($value['QTY']); ?>" value="<?php echo e($value['partNum']); ?>"/>
                                    </td>
                                    <td>
                                        <a class="hind-font" href="javascript:void(0)"><span class="f-s14 cancelled  font-w500">MPN: <?php echo e($value['partNum']); ?></span></a>
                                        <br>
                                        <span class="f-s14 cancelled font-w500">
                                                    We are sorry we could not find your part
                                         </span>
                                    </td>
                                    <td><?php if(!empty($value['internalPart'])) { ?>
                                                Internal Part: <span class="black font-w500"><?php echo e($value['internalPart']); ?></span>
                                            <?php }else{
                                                echo 'N/A';
                                            } ?>
                                    </td>
                                    <td width="200">N/A</td>
                                    <td>
                                        <?php /*<button type="button" class="m-t9 btn btn-primary btn-outline p-tb5 requestquote-btn-custom request_product-btn" title="Request Lead Time"><span class="img-icon"><img src="{{url('frontend/assets/images/add-quote-icon.png')}}" alt="add-quote-icon" /></span>Add Unavailable to RFQ</button> */ ?>
                                        <button type="button" id="addToQuote"  onclick="productListModule.addToQuoteProductListing('N/A', '0', '0', '<?php echo $value['QTY']; ?>', '<?php echo $value['partNum']; ?>', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'not_found')" class="m-t9 btn btn-primary btn-outline p-tb5 requestquote-btn-custom" title="Request Lead Time"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/add-quote-icon.png')); ?>" alt="add-quote-icon" /></span>Add Unavailable to RFQ</button>
                                    </td>
                                    <td>N/A</td>
                                    <td width="350">N/A</td>
                                    <td width="350">N/A</td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <div class="modal" id="request-part-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="modalLabel">Request Lead Time</h4>
                                                <button type="button" class="close" data-dismiss="modal" onclick="quoteModule.closeAddToQuoteModel()" aria-label="Close">
                                                    <span aria-hidden="true"><img src="<?php echo e(url('frontend/assets/images/close-icon.png')); ?>" alt="close-icon" /></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <?php echo Form::open(['class'=>'form-horizontal','method' => 'POST', 'id' => 'request_part_detail_form', 'data-parsley-validate' => true]); ?>

                                                <div class="price-content-box">
                                                    <div class="price-content-body">
                                                        <div class="clearfix m-b5 f-s14"></div>
                                                        <div class="form-group">
                                                            <div class="col-md-12 p-l0">
                                                                <input type="hidden" name="popupPartNumber[]" required=""  data-parsley-required  data-parsley-maxlength="50"  value="<?php echo e($part); ?>" id="popupPartNumber" placeholder="MPN*" class="quantity-box"  data-parsley-minlength="2"/>
                                                                <input type="hidden" name="popupInternalPart[]" required=""  data-parsley-required  data-parsley-maxlength="50"  value="" id="popupInternalPart" data-parsley-minlength="2"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-12 p-l0">
                                                                <input type="text" readonly name="popupExpectedDeliveryDate" value="" class="requested-delivery-date quantity-box" id="popupExpectedDeliveryDate" placeholder="Expected Delivery Date"  />
                                                            </div>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="col-md-12 p-l0">
                                                                <input type="hidden" name="popupRequestedQuantities[]" data-parsley-required data-parsley-type="digits"  data-parsley-maxlength="10"  value="" id="popupRequestedQuantities" placeholder="Requested Quantity*" class="quantity-box" min="1"/>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-12 p-l0">
                                                                <input type="text" name="popupExpectedUnitPrice" data-parsley-pattern="^\s*(?=.*[1-9])\d*(?:\.\d{1,6})?\s*$"  data-parsley-maxlength="20" data-parsley-type="number" value="" id="popupExpectedUnitPrice" placeholder="Target Price (per unit in $)" class="quantity-box"
                                                                />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                    <div class="col-md-12 p-l0">
                                        <input type="text" name="popupLeadQuoteName" value="" id="popupLeadQuoteName" placeholder="Quote Name" class="quantity-box"/>
                                    </div>
                                </div>
                                                        <div class="form-group">
                                                            <div class="col-md-12 p-l0">
                                                                <input type="hidden" id="popupCategoryName">
                                                              </div>
                                                        </div>
                                                        <div id="cat_error"></div>
                                                    </div>
                                                </div>
                                                <?php echo Form::close(); ?>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" data-dismiss="modal" class="btn btn-primary btn-outline" title="Cancel" onclick="quoteModule.closeAddToQuoteModel()"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/pop-up-close-icon.png')); ?>" alt="pop-up-close-icon" /></span>Cancel</button>
                                                <button type="button" onclick="addToRequestedPart()" class="btn btn-primary" title="Request Lead Time"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/add-white-icon.png')); ?>" alt="pop-up-edit-icon" /></span>Request Lead Time</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                        <?php endif; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal" id="save-bom-modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title" id="savebomModalLabel">MONSTER-BOM</h4>
                                                <button type="button" class="close" data-dismiss="modal" onclick="bom.closeSaveBomModel()" aria-label="Close">
                                                    <span aria-hidden="true"><img src="<?php echo e(url('frontend/assets/images/close-icon.png')); ?>" alt="close-icon" /></span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <?php echo Form::open(['class'=>'form-horizontal','method' => 'POST', 'id' => 'save_bom_form', 'data-parsley-validate' => true]); ?>

                                                <div class="price-content-box">
                                                    <div class="price-content-body">
                                                        <div class="clearfix m-b5 f-s14"></div>
                                                        <div class="form-group">
                                                            <div class="col-md-12 p-l0">
                                                                <input type="text" name="bomName" value="<?php echo e(!empty($bom_name)?$bom_name:''); ?>" id="bomName" placeholder="Monster-BOM Name*" class="quantity-box" data-parsley-required/>
                                                                <input type="hidden" name="updateBomId" value="<?php echo e(!empty($bom_id)?$bom_id:''); ?>" id="updateBomId" data-parsley-required/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php echo Form::close(); ?>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" data-dismiss="modal" class="btn btn-primary btn-outline" title="Cancel" onclick="bom.closeSaveBomModel()"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/pop-up-close-icon.png')); ?>" alt="pop-up-close-icon" /></span>Cancel</button>
                                                <button type="button" onclick="saveBomAjax()" class="btn btn-primary" title="Request Lead Time"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/add-white-icon.png')); ?>" alt="pop-up-edit-icon" /></span>Save</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
        </section>
    <?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(asset('frontend/js/product-list.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/js/jquery-listnav.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/js/category-part-search.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/js/bom-upload.js')); ?>"></script>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>