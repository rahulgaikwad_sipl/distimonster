
<?php $__env->startSection('title', '| Add Team Members'); ?>
<?php $__env->startSection('content'); ?>
<style type="text/css">
    .parsley-errors-list li {
    margin-top: 0.1875rem;
     padding-left: 0px !important; 
}
</style>
    <section class="gray-bg top-pad-with-bradcrumb">
        <div class="container-fluid clearfix container-w-80 user-admin">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active"><span>Add Team Members</span></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
                                    <?php echo $__env->make('frontend.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <div class="tab_container">
                            <h3 class="tab_drawer_heading" rel="tab2">Add Team Member</h3>
                            <div id="tab2" class="tab_content">
                                <h2 class="block-title left-block-title">Add Team Members</h2>
                                <div class="shdow-box edit-account-setting">
                            <div class="edit-account-setting-content">
                            <p>
                            You can add team members that you want to collaborate with here. If you want to share a BOM you are working on with a colleague you can add them here.
                            </p>
            <form class="form cf msfrm" name="subuser_frm" id="subuser_frm" method="POST" action="<?php echo e(url('/add-sub-user')); ?>" data-parsley-validate="">
                                    <?php echo e(csrf_field()); ?>

                <input type="hidden" name="user_count" id="userCount" value="<?php echo e($userCount); ?>">                    
                <div id="czContainer" >
                    <div id="first">
                        <div class="col-md-12 recordset">
                            <div class="clearfix">
                                <div class="add-new-member">
                                    <table class="edit-listing edit-listing-custom">
                                        <tr>
                                            <td> <input type="text" name="user_first_name[]" id="user_first_name[]" placeholder="First Name" required="" class="form-control dynamic_frm" tabindex="7"  data-parsley-required-message="Please enter member first name."/></td>
                                            <td> <input type="text" name="user_last_name[]" id="user_last_name[]" placeholder="Last Name" required="" class="form-control dynamic_frm" tabindex="8" data-parsley-required-message="Please enter member last name."/></td>
                                            <td class="email-block"> <input type="text" name="user_email[]" id="user_email[]" placeholder="Email" required="" class="form-control dynamic_frm" tabindex="9" data-parsley-required-message="Please enter member email." data-parsley-remote="<?php echo e(url('check-newemail')); ?>" data-parsley-remote-message="Email address is already in use"/>
                                            <ul class="parsley-errors-list filled customexist" style="display: none;">
                                                <li class="parsley-remote">Email address is already in use.</li>
                                            </ul>
                                             </td>
                                            <td>
                                                <textarea name="user_note[]" id="user_note[]" placeholder="Note" class="form-control dynamic_frm" style="border-radius:100px"></textarea>
                                            </td>
                     
                                        </tr>
                                        <hr />
                                    </table>


                                   
                                </div>
                               
                                
                            </div>
                            <div class="clearfix">&nbsp;</div>
                        </div>
                    </div>
               </div>
               <div class="form-group">
                    <ul class="list-inline text-md-center">
                        <br/>
                        <li><button type="submit" id="step_3_btn" class="btn btn-lg btn-common next-step next-button">Submit</button></li>
                    </ul>
                </div>
               </form>
                                    </div>
                                </div>
                            </div>
                            <!-- #tab2 End-->
                        </div>
                        <!-- .tab_container -->
                    </div>
            <div class="shdow-box">
              
            </div>
            </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script src="<?php echo e(url('frontend/js/jquery.czMore-1.5.3.2.js')); ?>"></script>
<script>
$(document).ready(function(){
    /*Add Dynamic Field Rows Code Start*/
$("#czContainer").czMore({
        max: <?php echo $subscription_plans->extra_users - $userCount; ?>, 
        min: 1,
        onLoad: null,
        onAdd: null,
        onDelete: null
    });
/*Add Dynamic Field Rows Code End*/
   /*Validation on Not more than 2 rows adding Code Start*/
    /* $("#btnPlus").click(function(){
        var userCount = $('#userCount').val();
        var rows = 3 - parseInt(userCount);
        if($(".recordset").length > rows){
            alert("Please contact us, if you want to add more than 20 users.");
            $("#btnMinus").trigger('click');
            return false;
        }
    }); */
    <?php if(($subscription_plans->extra_users - $userCount) == 1) { ?>
        setTimeout(function(){
            $("#btnPlus").hide();
        }, 200);
        setTimeout(function(){
            //$("#btnPlus").hide();
            $("#btnMinus").hide(); 
        }, 300);           
    <?php } ?>
    $("#btnPlus").css('display','block');
        setTimeout(function(){
            $("#btnPlus").trigger('click');
        }, 200);
    $('#my_bio_form').parsley();
});
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>