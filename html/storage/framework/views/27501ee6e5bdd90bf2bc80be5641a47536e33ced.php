
<?php $__env->startSection('title', '| Home'); ?>
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('frontend.partials.banner', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<!--<section class="news-update">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2 class="block-title">News &amp; Updates <a href="<?php echo e(url('news-updates')); ?>" class="view-all">View All</a></h2>
                <div class="news-updates-block">
                    <?php if(!$news->isEmpty()): ?>
                        <ul>
                            <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $newsPost): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li class="clearfix">
                                <div class="news-img">
                                    <?php if($newsPost->image): ?>
                                    <a href="<?php echo e(url('news/'.$newsPost->id)); ?>" title="Image Not Found"><img width="100" src="<?php echo e(url('/uploads/'.$newsPost->image)); ?>"/></a>
                                    <?php else: ?>
                                    <a href="<?php echo e(url('news/'.$newsPost->id)); ?>" title="Default Image"><img width="100" src="<?php echo e(url('frontend/images/logo.png')); ?>"/></a>
                                    <?php endif; ?>
                                </div>
                                <div class="news-content">
                                    <div class="nc-top"><a href="<?php echo e(url('news/'.$newsPost->id)); ?>"><?php echo e($newsPost->title); ?></a><span class="date"><?php echo e(date("m/d/Y", strtotime($newsPost->created_at))); ?></span></div>
                                    <p><?php echo implode(' ', array_slice(explode(' ', $newsPost->description), 0, config('constants.NEWS_CHARACTERS'))); ?></p>
                                    <?php if(str_word_count($newsPost->description) > config('constants.NEWS_CHARACTERS')): ?>
                                       <a href="<?php echo e(url('news/'.$newsPost->id)); ?>" class="read-more">Read More</a>
                                    <?php endif; ?>
                                </div>
                            </li>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </ul>
                        <?php else: ?>
                        <p>No news is published from <?php echo e(config('app.name')); ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-6">
                <h2 class="block-title">Product Lines</h2>
                <div class="suppliers-block">
                    <ul class="clearfix">
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-koa.jpg')); ?>" alt="Koa" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-microchip.jpg')); ?>" alt="Microchip" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-nxp.jpg')); ?>" alt="Nxp" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-epson.jpg')); ?>" alt="Epson" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-cosel.jpg')); ?>" alt="Cosel" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-kyocera.jpg')); ?>" alt="Kyocera" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-sanken.jpg')); ?>" alt="Sanken" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-jrc.jpg')); ?>" alt="Jrc" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-ricoh.jpg')); ?>" alt="Ricoh" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-murata.jpg')); ?>" alt="Murata" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-cypress.jpg')); ?>" alt="Cypress" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-nkk.jpg')); ?>" alt="Nkk" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-bellnix.jpg')); ?>" alt="Bellnix" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-honeywell.jpg')); ?>" alt="Honeywell" /></li>
                        <li><img src="<?php echo e(url('frontend/assets/images/logo-toshiba.jpg')); ?>" alt="Toshiba" /></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>-->
<!--<?php echo $__env->make('frontend.partials.section-special', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>