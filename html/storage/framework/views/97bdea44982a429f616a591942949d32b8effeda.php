<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user_view')): ?>
<a href="<?php echo e(route('admin.users.show',[$user->id])); ?>" class="btn btn-xs btn-info"><i class="fa fa-eye" title="View BIO" aria-hidden="true"></i></a>
<?php endif; ?>

<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user_edit')): ?>
    <a href="<?php echo e(route('admin.users.edit',[$user->id])); ?>" class="btn btn-xs btn-info"><i class="fa fa-pencil-square-o" title="Edit User Info" aria-hidden="true"></i></a>
<?php endif; ?>

<?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user_delete')): ?>
    <button class="red fl btn btn-xs btn-danger" onclick="userModule.confirmUserDelete('<?php echo $user->id; ?>','Are you sure you want to delete this user?')" type="button"><i class="fa fa-trash" aria-hidden="true"></i></button>
<?php endif; ?>