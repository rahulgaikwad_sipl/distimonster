
<?php $__env->startSection('title', '| My Team'); ?>
<?php $__env->startSection('content'); ?>
<section class="gray-bg account-custome-view top-pad-with-bradcrumb">
    <div class="container-fluid container-w-80 clearfix user-dashboard user-admin">
        <div class="row pagination">
            <div class="col-md-12">
                <ul class="breadcrumb f-s14 text-gray p-l0">
                    <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                    <li class="breadcrumb-item"><a href="#">My Account</a></li>
                    <li class="breadcrumb-item active"><span>My Team</span></li>
                </ul>
                <?php if(!empty($subscription_plans) && count($user) <= $subscription_plans->extra_users): ?>
                    <?php $display = (count($user) == $subscription_plans->extra_users) ? 'display:none': '';?>
                    <a style="float:right;<?php echo $display; ?>" class="btn btn-primary btn-outline p-tb5 requestquote-btn-custom" href="<?php echo e(url('/add-sub-user')); ?>" titlle="Add Team Members"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/add-quote-icon.png')); ?>" alt="add-quote-icon" /></span>Add Team Members</a>
                <?php endif; ?>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="row">
            <div class="col-md-12">
                <div class="tabs_wrapper">
                        <?php echo $__env->make('frontend.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                       <div class="tab_container">
                        <h3 class="tab_drawer_heading" rel="tab2">My Team</h3>
                        <div id="tab2" class="tab_content">
                            <div class="shdow-box edit-account-setting">
                                <div class="edit-account-setting-content edit-custom-setting">
                                    <p>You can add a maximum of two team members within your company here at the cost of $100 a month per member. If you require more than two please contact us for volume priceing.</p>
                                    <div id="czContainer" style="width: 100% !important;">
                                        <div id="first">
                                            
                                            <?php if(Auth::user()): ?>
                                                <table id="subUserListing" class="table  table-hover table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>First Name</th>
                                                            <th>Last Name</th>
                                                            <th>Email</th>
                                                            <th>Note</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subUser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr class="shdow-box">
                                                            <td><?php echo e($subUser->name); ?></td>
                                                            <td><?php echo e($subUser->last_name); ?></td>
                                                            <td><?php echo e($subUser->email); ?></td>
                                                            <td><?php if(isset($subUser->note) && !empty($subUser->note)): ?>
                                                                    <?php echo e($subUser->note); ?>

                                                                <?php else: ?>
                                                                    N/A
                                                                <?php endif; ?>
                                                            </td>
                                                        </tr>
                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                                                    </tbody>
                                                </table>
                                                <?php endif; ?>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- #tab2 End-->
                    </div>
                    <!-- .tab_container -->
                </div>
            </div>
        </div>
    </div>
</section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#subUserListing').DataTable();
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>