<?php $__env->startSection('content'); ?>
    <tr>
        <td align="center" valign="top" style="padding:0px;"><!--main section start-->
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:650px;max-width:100%;">
                <tr>
                    <td align="center" valign="top" style="font:Bold 14px Arial, Helvetica, sans-serif;color:#666;padding:0px;background:#fff;padding:0px;width:589px;" bgcolor="#FFF">
                        <table align="center" style="border:0 none;border-spacing:0;font-family: 'Mallanna', sans-serif;color:#000;font-weight:normal;text-align:left;width:589px;margin:0 auto;" cellpadding="0" cellspacing="0">
                            <tr><td style="height:25px;">&nbsp;</td></tr><!--spacing-->
                            <tr>
                                <td style="font-size:22px;color:#333;line-height:121%;padding:0 0 15px;">Hello <?php echo e($order[0]->customer_name); ?> <?php echo e($order[0]->last_name); ?>,</td>
                            </tr>
                            <tr>
                                <td style="font-size:30px;color:#333;line-height:121%;padding:0">Thanks for ordering via <?php echo e(config('app.name')); ?>!</td>
                            </tr>
                            
                            
                            
                            
                            
                            <tr><td style="width:100%;height:10px;"></td></tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-size:14px;color:#444;line-height:110%;">
                                        <tr>
                                            <td valign="top">
                                                <table width="80%" border="0" cellspacing="0" cellpadding="0" align="left">
                                                    <tr>
                                                        <td style="padding:4px 0;">Order Number:</td>
                                                        <td style="padding:4px 0;"><strong><?php echo e($order[0]->id); ?></strong></td>
                                                    </tr>

                                                    <tr>
                                                        <td style="padding:0px 0;">Payment Type:</td>
                                                        <td><strong><?php echo e($order[0]->payment_method_name); ?></strong></td>
                                                    </tr>
                                                </table>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td colspan="2" style="width:100%;height:15px;">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><strong>Delivery Address:</strong> <?php echo e($order[0]->address_shipping); ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"><strong>Billing Address:</strong> <?php echo e($order[0]->address_billing); ?></td>
                                        </tr>
                                        <tr>
                                            <td> <strong>To check complete order information</strong> <a href="<?php echo e(url('order-details/'.$order[0]->id)); ?>" title="Order Details" style="color:#0185c6;text-decoration:none" target="_blank">Click here </a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font:Bold 14px Arial, Helvetica, sans-serif; color:#666; padding:0px;background:#fff;">
                        <table border="0" align="center" cellpadding="0" cellspacing="0" style="width:589;background:#fff;">
                            <tr>
                                <td style="height:23px;width:100px;">&nbsp;</td>
                            </tr><!--//space-->
                            <tr>
                                <td style="background:#fff;padding:0px 0 0px 0px;font-weight:normal;font-size:16px;width:589px;">
                                    <table cellpadding="0" cellspacing="0" width="589" style="padding-left: 48px;">
                                        <tr>
                                            <td width="589">
                                                <hr style="border:1px solid #666;margin:0px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:589px">
                                                <table cellpadding="0" align="center" cellspacing="0" width="589" style="color: #444;font-family: 'Mallanna', sans-serif;font-size:14px;line-height:100%;width:589px;">
                                                    
                                                    
                                                    
                                                    <tr><td colspan="5" width="589"><br></td>
                                                    </tr>
                                                    <tr>

                                                        <th align="center">Item Name</th>
                                                        <th align="center">Quantity</th>
                                                        <th align="center">Price</th>
                                                        <th align="center">Manufacturer Name</th>
                                                        <th align="center">Distributor Name</th>
                                                        <th align="center" style="padding-right:15px;">Price Total</th>

                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" height="2"  width="589">
                                                            <hr style="border-top:1px dotted #666;">
                                                        </td>
                                                    </tr>
                                                    <?php $__currentLoopData = $orderDetail; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>

                                                            <td align="center"><?php echo e($item->item_name); ?></td>
                                                            <td align="center"><?php echo e($item->quantity); ?></td>
                                                            <td align="center"><?php echo e(number_format($item->price ,2, '.', ',')); ?></td>
                                                            <td align="center"><?php echo e($item->manufacturer_name); ?></td>
                                                            <td align="center"><?php echo e($item->distributor_name); ?></td>
                                                            <td align="center"><?php echo e(number_format($item->quantity*$item->price ,2, '.', ',')); ?></td>
                                                        </tr>
                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                    <tr><td colspan="5"  width="589"><br></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="589">
                                                <hr style="border:1px solid #666">
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                            </tr><!--//single list-->
                            <tr>
                                <td style="padding-left:16px;font-weight:normal;padding:15px 0px 14px 0px;background:#fff;">
                                    <table cellpadding="0" cellspacing="0" width="98%" style="color:#444;font-family: 'Mallanna', sans-serif;font-size:14px;background:#fff;" align="right">
                                        <tr>
                                            <td width="150px">
                                            </td><td align="left" width="200px">Sub Total</td>
                                            <td align="right" width="150px"><b>$ <?php echo e(number_format($order[0]->order_sub_total,2, '.', ',')); ?></b></td>
                                        </tr>
                                        <tr>
                                            <td width="150px">
                                            </td><td align="left" width="200px">Promo Code Discount</td>
                                            <td align="right" width="150px"><b>$ <?php echo e(number_format( $order[0]->order_discount_amount,2, '.', ',')); ?></b></td>
                                        </tr>
                                        <tr>
                                            <td width="150px">
                                            </td><td align="left" width="200px"> Tax Collected</td>
                                            <td align="right" width="200px"><b>$ <?php echo e(number_format($order[0]->order_tax_amount,2, '.', ',')); ?></b></td>
                                        </tr>
                                        <tr>
                                            <td width="150px"></td><td align="left" width="200px"> Delivery Charges</td>
                                            <td align="right" width="150px"><b>$ <?php echo e(number_format( $order[0]->order_shipping_charges_amount,2, '.', ',')); ?></b></td>
                                        </tr>
                                        <?php if($order[0]->consolidated_shipping_charge == 'yes'){ ?>
                                        <tr>
                                            <td width="150px"></td><td align="left" width="200px"> Consolidated Shipping Charges</td>
                                            <td align="right" width="150px"><b>$ <?php echo e(number_format( config('constants.CONSOLIDATED_SHIPPING_CHARGE'),2, '.', ',')); ?></b></td>
                                        </tr>
                                        <?php } ?>
                                        <?php if($order[0]->payment_method_id == 3){ 
                                            $ccExtra3Percent = ($order[0]->order_sub_total * config('constants.EXTRA_CREDIT_CARD_CHARGE')) / 100;    
                                        ?>
                                        <tr>
                                            <td width="150px"></td><td align="left" width="200px"> Extra Credit Card Charges</td>
                                            <td align="right" width="150px"><b>$ <?php echo e(number_format($ccExtra3Percent,2, '.', ',')); ?></b></td>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                            <td></td>
                                            <td colspan="2">
                                                <hr style="border:1px solid #666" align="right">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="150px">
                                            </td><td align="left" width="200px"> Amount Paid</td>
                                            <td style="font-size:12pt" align="right" width="150px"><b>$ <?php echo e(number_format($order[0]->order_total_amount,2, '.', ',')); ?></b></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td colspan="3" style="padding-bottom:3px;"><hr style="border:1px solid #666" align="right"></td></tr></table>
                                </td>
                            </tr><!--//single list-->
                        </table><!--//main content table-->
                    </td>
                </tr>
            </table>
        </td><!-- //main section start-->
    </tr><!--//main wrap tr-->
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('emails.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>