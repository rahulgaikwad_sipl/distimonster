
<?php $__env->startSection('title', '| Products'); ?>

<?php $__env->startSection('content'); ?>
    <section class="gray-bg shipping-cart categories-page">
        <div class="container clearfix ">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><span>Products</span></li>
                    </ul>
                </div>
            </div>
            
               
                  
               
            

            <?php //echo "<pre>";print_r($catergories);?>
                <div class="row clearfix">
                    <div class="col-md-7">
                        <h2 class="block-title">Products</h2>
                    </div>
                </div>
                <div class="row aboutus clearfix">
                    <div class="col-md-12">
                        <div class="shdow-box ">
                            <div class="search-filter-input">
                                <input class="form-control" type="text" id="myInput" onkeyup="searchCategory()" placeholder="Search by category name here..">
                            </div>
                            <ul id="myList" class="categories-left">
                                <?php $__currentLoopData = $catergories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <li><a href="<?php echo e(url('category/'.$item['slug'])); ?>"><?php echo e($item['name']); ?></a></li>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </ul>
                        </div>
                    </div>
                </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script src="<?php echo e(asset('frontend/js/jquery-listnav.min.js')); ?>"></script>
    <script src="<?php echo e(asset('frontend/js/category-part-search.js')); ?>"></script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>