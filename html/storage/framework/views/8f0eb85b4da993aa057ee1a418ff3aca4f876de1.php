<ul class="tabs shdow-box" id="tabMenu">
		<li class="<?php echo e(Request::is('dashboard') ? 'active' : ''); ?>"><a href="<?php echo e(url('dashboard')); ?>">Dashboard</a></li>
		<li class="<?php echo e(Request::is('account-settings/*') ? 'active' : ''); ?>" > <a href="<?php echo e(url('account-settings/'.Auth::user()->id)); ?>">Account Settings </a> </li>
		<li class="<?php echo e(Request::is('order-history') ? 'active' : ''); ?>"><a href="<?php echo e(url('order-history')); ?>">Order History</a></li>
		<li class="<?php echo e(Request::is('quotes') ? 'active' : ''); ?>"><a href="<?php echo e(url('quotes')); ?>">Quotes</a></li>
		<?php if(Request::is('quotes') || Request::is('pending-quotes') || Request::is('processed-quotes')|| Request::is('expired-quotes') || Request::is('ordered-quotes') ): ?>
		<li class="p-l0 p-r0 p-t0 saved-quote-submenu">
		<ul class="tabs shdow-box ">
				<li class="<?php echo e(Request::segment(1) == 'pending-quotes' ? 'active' : ''); ?>"><a href="<?php echo e(url('pending-quotes')); ?>">Pending RFQ's</a></li>
				<li class="<?php echo e(Request::segment(1) == 'processed-quotes' ? 'active' : ''); ?>"><a href="<?php echo e(url('processed-quotes')); ?>">Processed Quotes</a></li>
		</ul></li>
         <?php endif; ?>                   
		<li class="<?php echo e(Request::is('my-bom') ? 'active' : ''); ?>"><a href="<?php echo e(url('my-bom')); ?>">Monster-BOM</a></li>
		<?php if(Request::is('my-bom') || Request::is('saved-bom') || Request::is('shared-bom') || Request::is('ordered-bom')): ?>
		<li class="p-l0 p-r0 p-t0 saved-quote-submenu">
			<ul class="tabs shdow-box ">
				<li class="<?php echo e(Request::segment(1) == 'saved-bom' ? 'active' : ''); ?>"><a href="<?php echo e(url('saved-bom')); ?>">Saved Monster-BOM</a></li>
				<li class="<?php echo e(Request::segment(1) == 'shared-bom' ? 'active' : ''); ?>"><a href="<?php echo e(url('shared-bom')); ?>">Shared Monster-BOM</a></li>
				<li class="<?php echo e(Request::segment(1) == 'ordered-bom' ? 'active' : ''); ?>"><a href="<?php echo e(url('ordered-bom')); ?>">Ordered Monster-BOM</a></li>
			</ul>
		</li>
		<?php endif; ?> 
		<?php if(!Session::get("mybio_added")): ?>  
			<li class="<?php echo e(Request::is('my-boi') ? 'active' : ''); ?>"><a style="color:#1889BD;" href="<?php echo e(url('my-bio')); ?>"><strong>My BIO</strong></a></li>
		<?php else: ?>
			<li class="<?php echo e(Request::is('my-boi') ? 'active' : ''); ?>"><a href="<?php echo e(url('my-bio')); ?>">My BIO</a></li>
		<?php endif; ?>
		<?php if(Auth::user()->parent_user_id==0): ?>
			<li class="<?php echo e(Request::is('my-team') ? 'active' : ''); ?>"><a href="<?php echo e(url('my-team')); ?>">My Team</a></li>
		<?php endif; ?>
	</ul>