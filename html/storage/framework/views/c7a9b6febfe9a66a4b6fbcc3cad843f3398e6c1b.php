    
    <?php $__env->startSection('title', '| Monster-BOM'); ?>
    <?php $__env->startSection('content'); ?>
    <section class="gray-bg">
        <div class="container clearfix bom-page">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><span>Monster-BOM</span></li>
                    </ul>
                </div>
            </div>
            <div class="shdow-box m-b10">
                <div class="left-pad-content">
                    <h2 class="block-title">DISTIMONSTER MAKES IT SIMPLE TO UPLOAD A BOM.</h2>
                    <p class="f-s14 text-gray">Use Monster-BOM to upload your BOM in an XLS or CVS file format or search for components by entering individual part numbers. Once your Monster-BOM is loaded, available inventories are accessed through our partners with current pricing and availability. And with one PO, you can purchase all available product through DistiMonster.</p>
                    <p class="f-s14 text-gray">You have the option to create a REQUEST FOR QUOTE with delivery dates and target pricing using your uploaded file. Or email the file to <a href="mailto:sales@distimonster.com">sales@distimonster.com</a> (create autolink to email) and a DistiMonster Account Manager will upload the Monster-BOM for you; then access the quote through your personalized dashboard.</p>
                    <p class="f-s14 text-gray"> 
                        Want more information? We would be happy to schedule a DistiMonster demonstration for you or your company. Contact us at 818-857-5788 or submit a Request for <a href="<?php echo e(url('request-a-demo')); ?>">Demo</a>.
                    </p>
                </div>
            </div>
            <div class="shdow-box">
                <?php echo e(Form::open(array('files' => true,'method' => 'POST','url' => 'bom', 'enctype' => 'multipart/form-data', 'data-parsley-validate','class' => 'form-horizontal', 'id' => 'add_bom', 'data-parsley-no-focus'))); ?>

                <div class="register-box-content request-quote">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="bom-small-box small-box-left">
                                <span class="or-text">or</span>
                                <h3>Upload a file</h3>
                                <div class="bom-small-box-inner">
                                    <?php if(Auth::check()): ?>
                                    <?php echo Form::hidden('user_id', Auth::user()->id, ['class' => 'form-control']); ?>

                                    <?php else: ?>
                                    <?php echo Form::hidden('user_id' ,null, ['class' => 'form-control']); ?>

                                    <?php endif; ?>
                                    <div class="form-group">
                                        <div class="upload-btn-wrapper">
                                            <button class="btn-upload"><!-- <img src="<?php echo e(url('frontend/assets/images/upload.png')); ?>" class="m-r5"> --><i class="fa fa-arrow-circle-up" aria-hidden="true"></i> Drop or select a file to upload (xls, csv)</button>
                                            <?php echo Form::file('bom_file', ['id' => 'bom_file', 'class'=>'form-control-file f-s14 text-gray btn-upload' , 'data-parsley-fileextension'=>'csv-xlsx','aria-describedby'=>'fileHelp']); ?>


                                            <p class="help-block"></p>
                                            <?php if($errors->has('bom_file')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('bom_file')); ?>

                                            </p>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="bom-small-box-footer">
                                    <div class="btn-dot f-s14"><a target="_blank" href="<?php echo e(url('sample-document/partsheets_with_header.xlsx')); ?>" ><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/excel.png')); ?>" alt="Resale Certificate" width="15px" /></span>Download Sample File</a></div>
                                </div>
                                <label for="exampleInputFile" class="f-s12 text-gray mt-3">Upload Monster-BOM (not to exceed 10MB)<span class="text-danger"></span></label>
                                <span id="bom_error_message" class="text-center"></span>

                            </div>


                            <!-- /form -->
                        </div>
                        <div class="col-md-6">
                            <div class="bom-small-box small-box-right">
                                <h3>Enter part number</h3>
                                <div class="bom-small-box-inner"><div class="form-group">

                                        <?php echo Form::textarea('bom_details',null,
                                        ['rows'=>'3' ,'cols'=>'5' ,'id' => 'bom_details', 'placeholder'=>'Enter part number(s) with quantity. Separate quantities with a comma.           
Example:    
BAV99, 12 
HCM895LP43TR, 5', 'class' => 'form-control', 'data-parsley-required-message'=>'Please select any one field to upload bom.', 'data-parsley-errors-container'=>"#bom_error_message"]); ?>

                                        <p class="help-block"></p>
                                        <?php if($errors->has('bom_details')): ?>
                                        <p class="help-block">
                                            <?php echo e($errors->first('bom_details')); ?>

                                        </p>
                                        <?php endif; ?>

                                </div></div>
                                <div class="bom-small-box-footer">
                                    <input type="hidden" id="full_name_bom" name="full_name" value="<?php echo e(old('full_name')); ?>">
                                    <input type="hidden" id="company_name_bom" name="company_name" value="<?php echo e(old('company_name')); ?>">
                                    <input type="hidden" id="email_bom" name="email" value="<?php echo e(old('email')); ?>">
                                    <input type="hidden" id="contact_no_bom" name="contact_no" value="<?php echo e(old('contact_no')); ?>">
                                    <input type="hidden" id="address_bom" name="address" value="<?php echo e(old('address')); ?>">
                                    <input type="hidden" id="note_bom" name="note" value="<?php echo e(old('note')); ?>">

                                        <input id="submit_quote" class="btn btn-primary" title="Submit" type="button" value="Submit">

                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <?php echo Form::close(); ?>

            </div>

            <div class="row clearfix">
            </div>
            <?php if(Auth::user()): ?>

                    <div class="my-bom">
                    <h2 class="block-title">My Saved Monster-BOMs</h2>
                    <?php if(count($bomList)>0): ?>
                        <?php $__currentLoopData = $bomList->chunk(4); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $items): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <div class="row ">
                            <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="col-md-3">
                                <div class="shdow-box my-quote-detail">
                                    <h3 class="f-s18"><a href="<?php echo e(url($item->id.'/bom-details')); ?>">BOM - <?php echo e($item->id); ?></a></h3>
                                    <div class="f-s14 text-gray btm-space">Total Parts: <span class="black font-w500"><?php echo e($item->bom_parts_count); ?></span></div>
                                    <div class="f-s14 text-gray btm-space">Last modified: <span class="black font-w500"><?php echo e(date('m/d/Y', strtotime($item->updated_at))); ?></span></div>
                                    
                                    <div class="my-quote-detail-footer ">
                                        <ul class="list-inline m-b0">
                                            <li class="list-inline-item">
                                                <a href="<?php echo e(url('download-bom/'.$item->id)); ?>" class="f-s16 font-w500 hind-font"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/download-small-icon.png')); ?>" alt="download-small-icon" /></span> Download</a>
                                            </li>
                                            
                                                
                                            
                                            <li class="list-inline-item">
                                                <a href="javascript:void(0)" onclick="bom.removeBomItem('<?php  echo $item->id;?>')" class="f-s16 font-w500 hind-font"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/remove-icon.png')); ?>" alt="remove-icon" /></span> Remove</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <div class="pagination-list text-right clearfix">
                            <?php echo e($bomList->appends(request()->except('page'))->links('frontend.partials.pagination')); ?>

                        </div>
                        <?php else: ?>
                            <div class="alert alert-info">
                                It seems, there is no saved Monster-BOMs.
                            </div>
                        <?php endif; ?>
                </div>

            <?php endif; ?>
        </div>
        <!-- BOM Modal Start here -->
        <div class="modal fade" id="bom_form" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title f-s18" id="modalLabel">Please fill following details in order to process your Monster-BOM request</h4>
                    </div>
                    <div class="modal-body">
                        <?php
                            if(Auth::user()){
                                $userBillingAddress =  \Helpers::getBillingAddressByUserId(Auth::user()->id);
                                if($userBillingAddress[0]->billing_address_line2 != ''){
                                    $address =  $userBillingAddress[0]->billing_address_line1.' ,'. $userBillingAddress[0]->billing_address_line2.','.$userBillingAddress[0]->city_name.','.$userBillingAddress[0]->state_name.' ,'.$userBillingAddress[0]->country_name.','. $userBillingAddress[0]->zip_code;
                                }
                                else{
                                   $address =  $userBillingAddress[0]->billing_address_line1.' ,'.$userBillingAddress[0]->city_name.','.$userBillingAddress[0]->state_name.' ,'.$userBillingAddress[0]->country_name.','. $userBillingAddress[0]->zip_code;
                                }
                            }
                        ?>
                        <div class="register-box-content request-quote">

                                <form method="POST" action="javascript:void(0);" accept-charset="UTF-8" enctype="multipart/form-data" data-parsley-validate="" class="form-horizontal" id="add_bom_quote" novalidate="">
                                    <div class="form-group">

                                            <?php if(Auth::check()): ?>

                                            <?php echo Form::text('full_name', Auth::user()->name.' '.Auth::user()->last_name, ['class' => 'form-control', 'id' => 'full_name', 'placeholder' => 'Name*', 'data-parsley-pattern'=>config('app.patterns.name'),'data-parsley-required',  'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-minlength' => config('app.fields_length.name_min'),'data-parsley-maxlength' => config('app.fields_length.name_max'),'data-parsley-minlength-message' => $validationMessage['full_name.min'],'data-parsley-maxlength-message' => $validationMessage['full_name.max']]); ?>

                                            <?php else: ?>
                                            <?php echo Form::text('full_name', old('full_name'), ['class' => 'form-control', 'id' => 'full_name', 'placeholder' => 'Your Name*', 'data-parsley-pattern'=>config('app.patterns.name'),'data-parsley-required',  'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE') ,'data-parsley-minlength' => config('app.fields_length.name_min'),'data-parsley-maxlength' => config('app.fields_length.name_max'),'data-parsley-minlength-message' => $validationMessage['full_name.min'],'data-parsley-maxlength-message' => $validationMessage['full_name.max']]); ?>

                                            <?php endif; ?>
                                            <p class="help-block"></p>
                                            <?php if($errors->has('full_name')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('full_name')); ?>

                                            </p>
                                            <?php endif; ?>

                                    </div>
                                    <div class="form-group">
                                        <?php if(Auth::check()): ?>
                                            <?php echo Form::text('company_name',Auth::user()->company_name, ['class' => 'form-control', 'id' => 'company_name', 'placeholder' => 'Company Name','data-parsley-maxlength'=>"50"]); ?>

                                        <?php else: ?>
                                            <?php echo Form::text('company_name',old('company_name'), ['class' => 'form-control', 'id' => 'company_name', 'placeholder' => 'Company Name','data-parsley-maxlength'=>"50"]); ?>

                                        <?php endif; ?>
                                            <p class="help-block"></p>
                                            <?php if($errors->has('company_name')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('company_name')); ?>

                                            </p>
                                            <?php endif; ?>

                                    </div>
                                    <div class="form-group">

                                            <?php if(Auth::check()): ?>
                                            <?php echo Form::email('email', Auth::user()->email, ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Email Address*','data-parsley-required',  'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE') ,'pattern' => config('app.patterns.email')]); ?>

                                            <?php else: ?>
                                            <?php echo Form::email('email', old('email'), ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Email Address*','data-parsley-required',  'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE') ,'pattern' => config('app.patterns.email')]); ?>

                                            <?php endif; ?>
                                            <p class="help-block"></p>
                                            <?php if($errors->has('email')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('email')); ?>

                                            </p>
                                            <?php endif; ?>

                                    </div>
                                    <div class="form-group">
                                        <?php if(Auth::check()): ?>
                                            <?php echo Form::text('contact_no', Auth::user()->contact_number, ['class' => 'form-control', 'id' => 'contact_no', 'placeholder' => 'Phone', 'data-parsley-pattern'=>'^[\d\+\-\.\(\)\/\s]*$' ,'data-parsley-minlength' => config('app.fields_length.contact_number_min'),'data-parsley-maxlength' => config('app.fields_length.contact_number_max'),'data-parsley-minlength-message' => $validationMessage['contact_number.min'],'data-parsley-maxlength-message' => $validationMessage['contact_number.max']]); ?>

                                        <?php else: ?>
                                            <?php echo Form::text('contact_no', old('contact_no'), ['class' => 'form-control', 'id' => 'contact_no', 'placeholder' => 'Phone', 'data-parsley-pattern'=>'^[\d\+\-\.\(\)\/\s]*$' ,'data-parsley-minlength' => config('app.fields_length.contact_number_min'),'data-parsley-maxlength' => config('app.fields_length.contact_number_max'),'data-parsley-minlength-message' => $validationMessage['contact_number.min'],'data-parsley-maxlength-message' => $validationMessage['contact_number.max']]); ?>

                                        <?php endif; ?>
                                            <p class="help-block"></p>
                                            <?php if($errors->has('contact_no')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('contact_no')); ?>

                                            </p>
                                            <?php endif; ?>

                                    </div>
                                    <div class="form-group">
                                            <?php if(Auth::check()): ?>
                                                <?php echo Form::textarea('address', $address, ['rows'=>'3' ,'cols'=>'5' ,'id' => 'address', 'placeholder'=>'Address', 'class' => 'form-control','data-parsley-maxlength'=>"150"]); ?>

                                            <?php else: ?>
                                                <?php echo Form::textarea('address', null, ['rows'=>'3' ,'cols'=>'5' ,'id' => 'address', 'placeholder'=>'Address', 'class' => 'form-control','data-parsley-maxlength'=>"150"]); ?>

                                            <?php endif; ?>
                                            <p class="help-block"></p>
                                            <?php if($errors->has('address')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('address')); ?>

                                            </p>
                                            <?php endif; ?>

                                    </div>
                                    <div class="form-group">

                                            <?php echo Form::textarea('note', null, [ 'data-parsley-required-message'=>config('constants.COMMON_EMPTY_FIELD_MESSAGE'),'data-parsley-maxlength'=>255,'rows'=>'3' ,'cols'=>'5' , 'id' => 'note', 'placeholder'=>'Note(Max. 255 characters are allowed)', 'class' => 'form-control']); ?>

                                            <p class="help-block"></p>
                                            <?php if($errors->has('note')): ?>
                                            <p class="help-block">
                                                <?php echo e($errors->first('note')); ?>

                                            </p>
                                            <?php endif; ?>

                                    </div>
                                    <div class="form-group m-t25">

                                            <?php echo Form::button('Submit', ['class' => 'btn btn-primary', 'id' => 'submit_bom', 'title'=>'Submit']); ?>


                                    </div>
                                </form>
                                <!-- /form -->

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BOM Modal End here -->
    </section>
    <?php $__env->stopSection(); ?>
    <?php $__env->startSection('javascript'); ?>
        <script src="<?php echo e(asset('frontend/js/bom-upload.js')); ?>"></script>
    <?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>