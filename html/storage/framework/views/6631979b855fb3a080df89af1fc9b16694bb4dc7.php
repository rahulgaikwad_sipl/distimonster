
<?php $__env->startSection('title', '|  Review & Payment'); ?>

<?php $__env->startSection('content'); ?>
   <section class="gray-bg shipping-cart shipping-addres">
      <div class="container clearfix">
         <!---Wizard Step-->
         <div class="row wizard-step">
            <div class="col-md-7 offset-md-3 text-center">
               <ul>
                  <li>
                     <a href="<?php echo e(url('shipping')); ?>">
                        <span class="circle"><i class="fa fa-check"></i></span>
                        <span>Shipping</span>
                     </a>
                  </li>
                  <li class="active">
                     <a href="javascript:void(0)" >
                        <span class="circle"><i class="fa fa-check"></i></span>
                        <span>Review & Payment</span>
                     </a>
                  </li>
               </ul>
            </div>
         </div>
         <!---Wizard Step-->
         <hr/>
         <div class="row clearfix">
            <div class="col-md-9">
               <h2 class="block-title left-block-title">Payment</h2>
            </div>
         </div>
         <?php echo Form::open(['url' => url('place-order'), 'autocomplete'=>'off', 'method' => 'POST', 'id' => 'place_order','data-parsley-validate'=>""]); ?>

         <div class="row">
            <div class="col-md-8 payment-left-content-block">
               <!--Shiiping Address Listing Content-->
               <div class="shdow-box">
                  <!--Shipping Addres-->
                  <div class="shipping-address-content-second">
                     <div class="row">
                        <div class="col-md-9">
                           <input class="form-check-input" type="hidden"  name="billing_address" id="billing_address" value="1">
                           <div class="form-check form-check-inline f-s14 text-gray m-b15">
                              
                              <h2 class="block-title">My Billing and Shipping address are the same?</h2>

                              </br>
                              <label class="radio-container form-check-label">Yes
                                 <input checked="true" onclick="checkOutModule.confirmBillingAddress(1,'<?php echo $isEmptybillingAddress?>','<?php echo $userId;?>')" data-parsley-group="first"  class="form-check-input billing_address" type="radio" name="billing_address_radio" id="billing_address_">
                                 <span class="checkmark"></span>
                              </label>

                              <label class="radio-container form-check-label">No
                                 <input onclick="checkOutModule.confirmBillingAddress(2,'<?php echo $isEmptybillingAddress?>','<?php echo $userId;?>');"   data-parsley-group="first" class="form-check-input billing_address" type="radio" name="billing_address_radio">
                                 <span class="checkmark"></span>
                              </label>

                           </div>
                           <div style="display: none;" id="billing_address_div" class="address-detail-list f-s14 text-gray shdow-box p-t20 p-b20">
                              <p><?php echo e($billingAddress[0]->name); ?> <?php echo e($billingAddress[0]->last_name); ?></p>
                              <p><?php echo e($billingAddress[0]->billing_address_line1); ?></p>
                              <?php if(!empty($billingAddress[0]->billing_address_line2)): ?>
                                 <p><?php echo e($billingAddress[0]->billing_address_line2); ?></p>
                              <?php endif; ?>
                              <p>
                                 <?php if(!empty($billingAddress[0]->city_name)): ?> <?php echo e($billingAddress[0]->city_name); ?>, <?php endif; ?>
                                 <?php if(!empty($billingAddress[0]->state_name)): ?> <?php echo e($billingAddress[0]->state_name); ?>, <?php endif; ?>
                                 <?php if(!empty($billingAddress[0]->country_name)): ?> <?php echo e($billingAddress[0]->country_name); ?>, <?php endif; ?>
                                 <?php if(!empty($billingAddress[0]->zip_code)): ?> <?php echo e($billingAddress[0]->zip_code); ?> <?php endif; ?>
                              </p>
                           </div>
                           <div style="display: block;" id="selected_shipping_address_div" class="address-detail-list f-s14 text-gray shdow-box p-t20 p-b20">
                              <p><?php echo e($addresses[0]->first_name); ?> <?php echo e($addresses[0]->last_name); ?></p>
                              <p><?php echo e($addresses[0]->address_line1); ?></p>
                              <?php if(!empty($addresses[0]->address_line2)): ?>
                                 <p><?php echo e($addresses[0]->address_line2); ?></p>
                              <?php endif; ?>
                              <p><?php echo e($addresses[0]->city_name); ?>,<?php echo e($addresses[0]->state_name); ?>,<?php echo e($addresses[0]->country_name); ?>, <?php echo e($addresses[0]->zip_code); ?></p>
                              <p><?php echo e($addresses[0]->contact_number); ?></p>
                           </div>
                        </div>
                     </div>
                     
                     
                     
                     
                     
                     
                     
                     
                  </div>
                  <!--Shipping Addres-->
               </div>
               <!--Shiiping Address Listing Content-->
               <div class="shdow-box">
                  <div class="shipping-address-content-second">
                     <div class="row clearfix">
                        <div class="col-md-12">
                            <?php
                            //    echo "<pre>";
                            //    print_r($isNetAccountHolder);
                            //    echo "</pre>";
                            ?>
                           <h2 class="block-title">Payment Methods</h2>
                        </div>
                        <div id="optionsError"> </div>
                        <div class="col-md-12">
                           <?php $__currentLoopData = $paymentmethod; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $payment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <?php if($payment->id != 4): ?>
                                    <label class="radio-container form-check-label"><?php echo e($payment->payment_method_name); ?>

                                 <?php if($payment->id == 2): ?>
                                    <?php if(!empty($isNetAccountHolder)): ?>
                                       <?php if($isNetAccountHolder[0]->status !=1): ?>
                                           <input disabled='disabled' onclick="payMentModule.choosePaymentMethod(this);"  data-parsley-required-message="Please select at least one payment method." data-parsley-errors-container="#optionsError" data-parsley-group="first" required="" class="payment-method-type form-check-input" type="radio" name="payment_method" id="payment_method_<?php echo e($payment->id); ?>" value="<?php echo e($payment->id); ?>" <?php if($user->preferred_payment_type == $payment->id): ?> checked <?php endif; ?>>
                                           <span class="checkmark disabled"></span>
                                                <a href="javascript:void(0)" onclick="checkOutModule.showNetAccountStatus()" title="help"><img src="<?php echo e(url('frontend/assets/images/question.png')); ?>" alt="Help" /></a>
                                       <?php else: ?>
                                          <input onclick="payMentModule.choosePaymentMethod(this);"  data-parsley-required-message="Please select at least one payment method." data-parsley-errors-container="#optionsError" data-parsley-group="first" required="" class="payment-method-type form-check-input" type="radio" name="payment_method" id="payment_method_<?php echo e($payment->id); ?>" value="<?php echo e($payment->id); ?>" <?php if($user->preferred_payment_type == $payment->id): ?> checked <?php endif; ?>>
                                          <span class="checkmark"></span>
                                       <?php endif; ?>
                                    <?php else: ?>
                                       <input disabled='disabled' onclick="payMentModule.choosePaymentMethod(this);"  data-parsley-required-message="Please select at least one payment method." data-parsley-errors-container="#optionsError" data-parsley-group="first" required="" class="payment-method-type form-check-input net-term-info" type="radio" name="payment_method" id="payment_method_<?php echo e($payment->id); ?>" value="<?php echo e($payment->id); ?>" <?php if($user->preferred_payment_type == $payment->id): ?> checked <?php endif; ?>>
                                       <span class="checkmark disabled"></span>
                                             <a href="javascript:void(0)" onclick="checkOutModule.showNetAccountStatus()" title="help"><img src="<?php echo e(url('frontend/assets/images/question.png')); ?>" alt="Help" /></a>
                                    <?php endif; ?>
                                 <?php else: ?>
                                    <input  onclick="payMentModule.choosePaymentMethod(this);"  data-parsley-required-message="Please select at least one payment method." data-parsley-errors-container="#optionsError" data-parsley-group="first" required="" class="payment-method-type form-check-input" type="radio" name="payment_method" id="payment_method_<?php echo e($payment->id); ?>" value="<?php echo e($payment->id); ?>" <?php if($user->preferred_payment_type == $payment->id): ?> checked <?php endif; ?>>
                                    <span class="checkmark"></span>
                                 <?php endif; ?>
                              </label>
                           <?php endif; ?>
                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                        <div class="col-md-12" id="net_account_status" style="<?php if($user->preferred_payment_type != 2): ?> display: none <?php endif; ?>">
                        <?php if(!empty($isNetAccountHolder)): ?>

                              <div class="col-md-12 shdow-box p-b10 p-t10 m-t25">
                                 <?php if($isNetAccountHolder[0]->status == 0): ?>
                                    You have already applied for Net Term Account and current status for request is  <b>Pending.</b>
                                 <?php endif; ?>
                                 <?php if($isNetAccountHolder[0]->status == 1): ?>
                                       Your request for Net Term Account is  <b>Approved.</b> ,Now you can check out
                                 <?php endif; ?>
                                 <?php if($isNetAccountHolder[0]->status == 2): ?>
                                   Your request for Net Term Account is <b>In Progress.</b>, We will communicate you once verification will be done.
                                 <?php endif; ?>
                                 <?php if($isNetAccountHolder[0]->status == 3): ?>
                                   Your request for Net Term Account is <b>Rejected.</b> You can <a href="<?php echo e(url('apply-net-term-account')); ?>">Re Apply</a> from here.
                                 <?php endif; ?>
                              </div>
                           <?php else: ?>
                                 <div class="col-md-12 shdow-box p-b10 p-t10 m-t25">
                                    <a href="<?php echo e(url('apply-net-term-account')); ?>"> Apply for New Net Terms</a>
                                 </div>
                        <?php endif; ?>
                        </div>

                        <div class="card-payment col-md-12" id="paypalSection" style="<?php if($user->preferred_payment_type != 1): ?> display: none <?php endif; ?>">
                           <div class="col-md-12 shdow-box p-b10 p-t10 m-t25">
                              Once you have reviewed and submitted your order, you will then be redirected to the PayPal website to complete the transaction.
                           </div>
                        </div>
                        <div class="card-payment col-md-12" id="cash_on_delivery" style="<?php if($user->preferred_payment_type != 4): ?> display: none <?php endif; ?>">
                           <div class="col-md-12 shdow-box p-b10 p-t10 m-t25">
                            Collect on delivery.
                           </div>
                        </div>
                        <div class="card-payment col-md-12" id="net_term_account" style="display: none">
                           <div class="col-md-12 shdow-box p-b10 p-t10 m-t25">
                              Payment will be done on the Net Account Terms.
                           </div>
                        </div>
                        <div class="card-payment col-md-12" id="paymentSection" style="<?php if($user->preferred_payment_type != 3): ?> display: none <?php endif; ?>">
                           <div class="col-md-9 shdow-box p-b10 p-t10 m-t25">
                              <div class="form-group">
                                 <div class="col-md-12 p-l0 p-r0">
                                    <input type="text" value="" class = 'form-control' placeholder="Card Number" id="card_number" name="card_number">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="row">
                                    <div class="col-md-6 p-r5">
                                       <input type="text" value="" class = 'form-control' placeholder="Expiry Month(MM)" maxlength="5" id="expiry_month" name="expiry_month">
                                    </div>
                                    <div class="col-md-6 p-l5">
                                       <input type="text" value="" class = 'form-control' placeholder="Expiry Year(YYYY)" maxlength="5" id="expiry_year" name="expiry_year">
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-md-12 p-l0 p-r0">
                                    <input type="password" value="" class = 'form-control' placeholder="CVV" id="cvv" name="cvv">
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="row">
                                    <div class="col-md-6 p-r5">
                                       <input type="text"  value="" class = 'form-control' placeholder="First Name " id="name_on_card" name="name_on_card">
                                    </div>
                                    <div class="col-md-6 p-l5">
                                       <input type="text"  value="" class = 'form-control' placeholder="Last Name" id="last_name" name="last_name">
                                    </div>
                                 </div>
                              </div>
                              <div class="form-group">
                                 <div class="col-md-12 p-l0 p-r0">
                                    <input type="hidden" name="card_type" id="card_type" value="visa"/>
                                 </div>
                              </div>
                              <div id="orderInfo" style="display: none;"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-6  p-b10 p-t10 m-t25">
                  <label> Note:</label>
                  <?php echo Form::textarea('payment_notes', null, ['data-parsley-maxlength'=>255,'rows'=>'3' ,'cols'=>'5' ,'id' => 'payment_notes','placeholder'=>'Note(Max. 255 characters are allowed)*', 'class' => 'form-control']); ?>

                  <p>
                     Your order could be affected by the current tariff situation.  If there is any tariff fee added you will see detailed info included in your invoice. You can see updated information regarding SECTION 301-CHINA 
add a link to SECTION 301-CHINA as follows <a href="https://ustr.gov/issue-areas/enforcement/section-301-investigations/section-301-china" target="_blank">Section 301 China</a>
                  </p>
               </div>
               <div class="apply-discount" id="apply_discount"><a onclick="checkOutModule.openApplyCoupon()" href="javascript:void(0)" class="f-s16 font-w500 hind-font">Apply Promo Code</a></div>
            </div>
            <div class="col-md-4">
               <input type="hidden" name="shipping_address" id="shipping_address" value="<?php echo e($addresses[0]->id); ?>">
               <input type="hidden" name="shipping_method" id="shipping_method" value="<?php echo e($shippingMethod[0]->id); ?>">
               <input type="hidden" name="shipping_charges" id="shipping_charges" value="<?php echo e($shippingMethod[0]->shipping_charges); ?>">
               <input type="hidden" name="coupon_code_id" id="coupon_code_id"  value=""/>
               <input type="hidden" name="order_applied_discount" id="order_applied_discount"  value=""/>
               <input type="hidden" name="coupon_type" id="coupon_type"  value=""/>

               <input type="hidden" name="shipping_method_type" id="shipping_method_type"  value="<?php echo e($shippingMethodType); ?>"/>
               <input type="hidden" name="account_number" id="account_number"  value="<?php echo e($accountNumber); ?>"/>
               <input type="hidden" name="shipping_notes" id="shipping_notes"  value="<?php echo e($shippingNotes); ?>"/>
                <input type="hidden" name="requested_delivery_date" id="requested_delivery_date" value="<?php echo e($requestedDeliveryDate); ?>" />

               <div class="shdow-box shipping-right-content payment-right-content m-t0">
                  <div class="price-content-box">
                     <div class="row price-box-heading">
                        <div class="col-md-10">
                           <h2 class="block-title">Order Summary</h2>
                        </div>
                        <div class="col-md-2">
                           &nbsp;
                        </div>
                     </div>
                     <div class="price-content-body">
                        <ul>
                           <li class="clearfix">
                              <span class="pull-left text-gray f-s14">Subtotal</span>
                              <span class="pull-right text-gray f-s14">$<?php echo e(Cart::instance('shoppingcart')->subtotal()); ?> </span>
                           </li>
                           <li class="clearfix">
                              <span class="pull-left text-gray f-s14">Shipping&nbsp;&nbsp;(<?php echo e($shippingMethod[0]->shipping_method_name); ?>) </span>
                              <span class="pull-right text-gray f-s14">$<?php echo e($shippingMethod[0]->shipping_charges); ?> </span>
                           </li>
                           <?php if(Session::get('consolidatedShippingCharge')): ?>
                           <li class="clearfix">
                              <span class="pull-left text-gray f-s14">Consolidated Shipping Charge</span>
                              <span class="pull-right text-gray f-s14">$<?php echo e($consolidatedShippingCharges); ?> </span>
                           </li>
                           <?php endif; ?>
                              <li id="discount_view" class="clearfix" style="display: none">
                                 <span class="pull-left text-gray f-s14">Discount (<a class="f-s16" onclick="checkOutModule.removeCouponCode()" href="javascript:void(0)">Remove Code</a>)</span>
                                 <span class="pull-right text-gray f-s14" id="discount_amount">$0.00</span>
                              </li>

                           <li class="f-s16 black font-w500 clearfix">
                              <span class="pull-left">Order Total</span>
                              <span class="pull-right" id="order_total">$<?php echo e(number_format( Cart::instance('shoppingcart')->subtotal(2,'.','') + $shippingMethod[0]->shipping_charges + $consolidatedShippingCharges,2, '.', ',' )); ?> </span>
                           </li>
                           <li>
                              <div class="f-s14 font-w500 item-cart-count"><?php echo e(Cart::instance('shoppingcart')->content()->count()); ?>

                              <?php echo e((Cart::instance('shoppingcart')->content()->count() == 1) ? 'Item' : 'Items'); ?>

                               in cart</div>
                           </li>
                        <?php foreach(Cart::instance('shoppingcart')->content() as $row) { ?>
                        <!-- <li class="clearfix ">
                           <div class="row shipping-address-right-summary">
                               <div class="col-md-6">
                                   <div><a href="javascript:void(0)" class="hind-font"><?php echo e($row->name); ?></a></div>
                                   <div class="f-s12"><?php echo e($row->options->has('manufacturer') ? $row->options->manufacturer : ''); ?></div>
                                   <div class="f-s12 text-gray quantity">Quantity: <?php echo e($row->qty); ?></div>
                               </div>
                               <div class="col-md-6 text-right">
                                   <div class="black f-s16">$<?php echo e($row->price*$row->qty); ?></div>
                               </div>
                           </div>
                           </li> -->
                            <?php } ?>
                        </ul>
                     </div>
                     <div class="price-content-footer">
                        <a href="javascript:void(0)" onclick="payMentModule.proceedToPayment()" class="btn btn-primary"> Place Order </a>
                        
                     </div>
                  </div>
               </div>
               <!--Right Shipping Address-->
               <div class="shdow-box shipping-right-content payment-right-content m-t0">
                  <div class="price-content-box">
                     <div class="row price-box-heading">
                        <div class="col-md-9">
                           <h2 class="block-title">Shipping Address</h2>
                        </div>
                        <div class="col-md-3 text-right">
                           <a href="<?php echo e(url('shipping')); ?>" class="f-s16 hind-font font-w500"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/edit-thine-icon.png')); ?>" alt="edit-icon" /></span> Edit</a>
                        </div>
                     </div>
                     <div class="price-content-body">
                        <div class="address-detail-list f-s14 text-gray shdow-box p-t20 p-b20 ship-add-div">
                           <p><?php echo e($addresses[0]->first_name); ?> <?php echo e($addresses[0]->last_name); ?></p>
                           <p><?php echo e($addresses[0]->address_line1); ?></p>
                           <?php if(!empty($addresses[0]->address_line2)): ?>
                              <p><?php echo e($addresses[0]->address_line2); ?></p>
                           <?php endif; ?>
                           <p><?php echo e($addresses[0]->city_name); ?>, <?php echo e($addresses[0]->state_name); ?> , <?php echo e($addresses[0]->country_name); ?>, <?php echo e($addresses[0]->zip_code); ?></p>
                           <p><?php echo e($addresses[0]->contact_number); ?></p>
                        </div>
                     </div>
                  </div>
               </div>
               <!--Right Shipping Address-->
               <!--Right Shipping Methods-->
               <div class="shdow-box shipping-right-content payment-right-content m-t0">
                  <div class="price-content-box">
                     <div class="row price-box-heading">
                        <div class="col-md-9">
                           <h2 class="block-title">Shipping Methods</h2>
						   </div>
                        <div class="col-md-3 text-right">
                           <a href="<?php echo e(url('shipping')); ?>" class="f-s16 hind-font font-w500"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/edit-thine-icon.png')); ?>" alt="edit-icon" /></span> Edit</a>
                        </div>
                     </div>
                     <div class="price-content-body">
                        <div class="address-detail-list  f-s14 text-gray shdow-box p-t20 p-b20 ship-add-div">
                           <div class="f-s14 text-gray font-w500"><?php echo e($shippingMethod[0]->shipping_method_name); ?> </div>
							    <?php if(Session::get('shippingMethodTypeInput')): ?>
							    <?php echo e(Helpers::getShippingOptionById(Session::get('shippingMethodTypeInput'))->name); ?>

								<?php endif; ?>
                        </div>
                     </div>
                  </div>
               </div>
               <!--Right Shipping Methods-->
            </div>
         </div>
         <?php echo Form::close(); ?>

      </div>
   </section>
   <!-- Modal -->
   <div class="modal" id="apply_coupon" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title" id="modalLabel">Apply Promo Code</h4>
               <button type="button" class="close" data-dismiss="modal" onclick="checkOutModule.closeApplyCoupon()" aria-label="Close">
                  <span aria-hidden="true"><img src="<?php echo e(url('frontend/assets/images/close-icon.png')); ?>" alt="close-icon" /></span>
               </button>
            </div>

            <div class="modal-body">

               <div class="price-content-box">
                  <div class="price-content-body">

                     <div class="alert alert-success" id="code_message_success_div" style="display: none">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                     </div>

                     <div class="alert alert-danger" id="code_message_error_div"  style="display: none">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                     </div>

                     <div class="clearfix m-b5 f-s14"></div>
                     <div class="quantity customer-part f-s14 text-gray">
                        <input type="text" name="coupon_code" value="" id="coupon_code" placeholder="Code" class="quantity-box"  required/>
                     </div>
                  </div>
               </div>

            </div>
            <div class="modal-footer">
               <button data-dismiss="modal" type="submit" class="btn btn-primary btn-outline" title="Cancel" onclick="checkOutModule.closeApplyCoupon()"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/pop-up-close-icon.png')); ?>" alt="pop-up-close-icon" /></span>Cancel</button>
               <button type="button" onclick="checkOutModule.applyCouponCode()" class="btn btn-primary" title="Edit"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/apply.png')); ?>" alt="pop-up-edit-icon" /></span>Apply</button>
            </div>


         </div>
      </div>
   </div>
   <!-- Modal -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>