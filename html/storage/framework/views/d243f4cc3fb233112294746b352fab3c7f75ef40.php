<!--START:Top Navigation-->
<!--Header-->
<nav class="navbar p-t0 p-b0">
    <div class="nav-container custom-header p-b0">
        <div class="container">
            <button class="navbar-toggler navbar-toggler-right d-md-none d-lg-none" type="button" data-toggle="collapse"
                data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-brand mb-0 "><i class="fa fa-bars"
                        aria-hidden="true"></i></span></button>
            <div class="row">
                <div class="navbar-header col-md-2">
                    <a class="navbar-brand" href="<?php echo e(url('/')); ?>"><img src="<?php echo e(url('frontend/images/logo.png')); ?>"
                            class="img-responsive"></a>
                </div>
                <div class="col-md-10 ">
                    <div class="row">
                        <div class="col-md-12">
                            <!--START: Header Top-->
                            <div class="header-top">
                                <div class="text-right">
                                    <ul class="nav navbar-nav navbar-right checkout m-t15 hidden-xs">
                                        <?php if(Cart::instance('quotecart')->content()->count() > 0): ?>
                                        <li><a href="<?php echo e(url('/quote-cart')); ?>">
                                                <div class="icon">
                                                    <span class="fa-icon"><i class="fa fa-file-text"
                                                            aria-hidden="true"></i></span>
                                                    <em class="cart-count"
                                                        id="quoteCount"><?php echo e(Cart::instance('quotecart')->content()->count()); ?></em>
                                                    <span style="font-weight: 500; color: #000; vertical-align: bottom">
                                                        Quote Requests</span>
                                                </div>
                                            </a>
                                        </li>
                                        <?php else: ?>
                                        <li><a href="<?php echo e(url('/quote-cart')); ?>">
                                                <div class="icon">
                                                    <span class="fa-icon"><i class="fa fa-file-text"
                                                            aria-hidden="true"></i></span>
                                                    <em style="display: none;" class="cart-count" id="quoteCount"></em>
                                                    <span> Quote Requests</span>
                                                </div>
                                            </a>
                                        </li>
                                        <?php endif; ?>
                                        <?php if(Cart::instance('shoppingcart')->content()->count() > 0): ?>
                                        <li><a href="<?php echo e(url('/cart')); ?>">
                                                <div class="icon">
                                                    <span class="fa-icon"><i class="fa fa-shopping-cart"
                                                            aria-hidden="true"></i></span>
                                                    <em class="cart-count"
                                                        id="cartCount"><?php echo e(Cart::instance('shoppingcart')->content()->count()); ?></em>
                                                    <span> Cart</span>
                                                </div>
                                            </a>
                                        </li>
                                        <?php else: ?>
                                        <li><a href="<?php echo e(url('/cart')); ?>">
                                                <div class="icon">
                                                    <span class="fa-icon"><i class="fa fa-shopping-cart"
                                                            aria-hidden="true"></i></span>
                                                    <em class="cart-count" id="cartCount" style="display:none">0</em>
                                                    <span class="cartCount">Cart</span></div><br><span
                                                    id="totalPrice"></span>
                                            </a></li>
                                        <?php endif; ?>
                                    </ul>
                                    <ul class="header-top-right">
                                        <?php if(Auth::check()): ?>
                                        <li class="hello-user dropdown">
                                            <a href="#" class="dropdown-toggle" href="javascript:void(0)"
                                                id="dropdown02" data-toggle="dropdown" aria-haspopup="true"
                                                aria-expanded="false">
                                                <span class="fa-icon"><i class="fa fa-user-o"
                                                        aria-hidden="true"></i></span>
                                                Hello <?php echo e(Auth::user()->name? Auth::user()->name:'Guest!'); ?></a>
                                            <div class="dropdown-menu" aria-labelledby="dropdown02">
                                                <a class="dropdown-item"
                                                    href="<?php echo e(url('account-settings/'.Auth::user()->id)); ?>">Account
                                                    Settings</a>
                                                <a class="dropdown-item" href="<?php echo e(url('order-history')); ?>">Order
                                                    History</a>
                                                <a class="dropdown-item" href="<?php echo e(url('quotes')); ?>">Quotes</a>
                                                <a class="dropdown-item" href="<?php echo e(url('my-bom')); ?>">Monster-BOM</a>
                                                <a class="dropdown-item" href="<?php echo e(url('my-bio')); ?>">My BIO</a>
                                                <?php if(Auth::user()->is_sub_user == 0): ?>
                                                <a class="dropdown-item" href="<?php echo e(url('my-team')); ?>">My Team</a>
                                                <?php endif; ?>
                                                
                                                <?php /*<a class="dropdown-item" href="{{url('apply-net-term-account')}}">Apply for a Net Term Account</a>*/?>
                                            </div>
                                        </li>
                                        <li><a href="<?php echo e(url('logout-user')); ?>">Logout</a></li>
                                        <?php else: ?>
                                        <li class="hello-user"><span class="fa-icon"><i class="fa fa-user-o"
                                                    aria-hidden="true"></i></span><span> Hello Guest!</span></li>
                                        <li>
                                            <div class=" authentication-right-btn">
                                                <a href="<?php echo e(url('login')); ?>">Login &nbsp;or&nbsp;
                                                Register</a>
                                            </div>
                                        </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                            <!--END: Header Top-->
                        </div>
                    </div>

                    <div class="row p-t15 m-b15">
                        <div class="col-lg-12 col-md-12 col-sm-12 inner-page-search">
<?php if((Route::getCurrentRoute()->uri() != '/')): ?>
                            <?php echo Form::open(['method' => 'get', 'url' => url('search'), 'class'=>"form minisearch"
                            ,'id'=>"search_part_number",'data-parsley-validate' => true]); ?>

                            <div class="field search clearfix">
                                <?php if(!empty(app('request')->input('partNumber'))){
                        $sr_param = app('request')->input('partNumber');
                    }else{
                        $sr_param = Session::get('search_part_name');
                    }?>
                                <input id="part_name" type="text" name="partNumber" value="<?php echo e($sr_param); ?>"
                                    data-parsley-errors-container="#error-container" data-parsley-required
                                    data-parsley-required-message="Please enter any part number"
                                    placeholder="Search by part number" class="input-text" />
                                <input type="hidden" name="slug_holder" id="slug_holder" value="" />
                                <button type="submit" title="Search" class="action search">
                                    <span>Search</span>
                                </button>
                            </div>
                            <div id="error-container"> </div>
                            <div class="search-history">
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#search-history">Search
                                    History <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                            </div>
                            <?php echo Form::close(); ?>                            
                            <?php endif; ?>
                        </div>                     
                    </div>
                    <?php if(Auth::check()): ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="quick-acc">
                                <h4 class="myacc-heading">Quick Account Options</h4>
                                <ul class="myacc-menu">
                                    <li><a href="<?php echo e(url('dashboard/')); ?>">Dashboard</a></li>
                                    <li><a href="<?php echo e(url('account-settings/'.Auth::user()->id)); ?>">Account Settings</a>
                                    </li>
                                    <li><a href="<?php echo e(url('order-history')); ?>">Order History</a></li>
                                    <li><a href="<?php echo e(url('quotes')); ?>">Quotes</a></li>
                                    <li><a href="<?php echo e(url('my-bom')); ?>">Monster-BOM</a></li>
                                    <?php if(!Session::get("mybio_added")): ?>
                                        <li><a href="<?php echo e(url('my-bio')); ?>" style="color:#1889BD;"><strong>My BIO</strong></a></li>
                                    <?php else: ?>
                                        <li><a href="<?php echo e(url('my-bio')); ?>">My BIO</a></li>
                                    <?php endif; ?>
                                    
                                    <?php if(Auth::user()->is_sub_user == 0): ?>
                                    <li><a href="<?php echo e(url('my-team')); ?>">My Team</a></li>
                                    <?php endif; ?>
                                    
                                    <?php /*<li><a href="{{url('apply-net-term-account')}}">Apply for a Net Term Account</a></li>*/?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                </div>
                <!-- <div class="col-md-6">
                    <ul class="nav navbar-nav navbar-right checkout m-t15 hidden-xs">
                        <?php if(Cart::instance('quotecart')->content()->count() > 0): ?>
                            <li><a href="<?php echo e(url('/quote-cart')); ?>"><div class="icon"><img src="<?php echo e(url('frontend/assets/images/icon-quote-cart.png')); ?>" alt="Quote Requests" />  <em class="cart-count" id="quoteCount"><?php echo e(Cart::instance('quotecart')->content()->count()); ?></em>  </div> <span>Quote Requests</span> <br><span style="font-weight: 500; color: #000;"> </span></a></li>
                        <?php else: ?>
                            <li><a href="<?php echo e(url('/quote-cart')); ?>"><div class="icon"><img src="<?php echo e(url('frontend/assets/images/icon-quote-cart.png')); ?>" alt="Quote Requests" />  <em style="display: none;" class="cart-count" id="quoteCount"></em>  </div> <span>Quote Requests</span> <br><span style="font-weight: 500; color: #000;"> </span></a></li>
                        <?php endif; ?>
                        <?php if(Cart::instance('shoppingcart')->content()->count() > 0): ?>
                                <li><a href="<?php echo e(url('/cart')); ?>"><div class="icon"><img src="<?php echo e(url('frontend/assets/images/icon-cart.png')); ?>" alt="Your Cart" /> <em class="cart-count" id="cartCount"><?php echo e(Cart::instance('shoppingcart')->content()->count()); ?></em></div><span>Your Cart</span> <br><span id="totalPrice"><?php echo e(Cart::instance('shoppingcart')->subtotal()); ?></span></a></li>
                        <?php else: ?>
                            <li><a href="<?php echo e(url('/cart')); ?>"><div class="icon"><img src="<?php echo e(url('frontend/assets/images/icon-cart.png')); ?>" alt="Your Cart" /> <em class="cart-count" style="display: none;" id="cartCount"></em></div><span>Your Cart</span> <br><span id="totalPrice"></span></a></li>
                        <?php endif; ?>
                    </ul>
                
                
                    <ul class="myacc-menu">
                        <li><a href="#">Account Setting</a></li>
                        <li><a href="#">Order History</a></li>
                        <li><a href="#">Quotes</a></li>
                        <li><a href="#">My BOM</a></li>
                        <li><a href="#">Apply dor a Net Term Account</a></li>
                    </ul>
                </div> -->
            </div>
        </div>
        <?php if(!Auth::check()): ?>
                        <?php if((Route::getCurrentRoute()->uri() != '/coming-soon') || (Route::getCurrentRoute()->uri() ==
                        '/')): ?>
                            <div class="free-trial-box">
                                <a href="<?php echo e(url('/subscription-plan')); ?>" class="btn btn-success text-right free-trial"
                                    style="pointer-events: visible;">START YOUR FREE TRIAL</a>
                            </div>
                        <?php endif; ?>
                        <?php endif; ?>
    </div>
</nav>

<!--Header end-->
<!--Navigation-->