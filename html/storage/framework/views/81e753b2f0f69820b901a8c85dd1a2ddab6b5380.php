<!DOCTYPE html>
<html lang="en">
<head>
    <?php echo $__env->make('frontend.partials.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</head>
<body>
<div class="entire-site">
    <div style="display: none;" class="loader"></div>
    
    <?php echo $__env->make('frontend.partials.topbar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

    <nav class="navbar ">
        <div class="nav-container custom-header">
            <div class="container">

                <div class="row">
                    <div class="navbar-header col-md-4">
                        <a class="navbar-brand" href="<?php echo e(url('/')); ?>"><img src="<?php echo e(url('frontend/images/logo.png')); ?>" class="img-responsive"></a>
                    </div>
                    <div class="col-md-8 col-sm-6 hidden-sm">
                        <div class="row" >
                            <div class="col-sm-12 m-b25" >

                                    <?php echo Form::open(['method' => 'get', 'url' => url('search'), 'class'=>"form minisearch" ,'id'=>"search_part_number",'data-parsley-validate' => true]); ?>

                                    <div class="field search clearfix">
                                        <input id="part_name" type="text" name="partNumber" data-parsley-errors-container="#error-container"  data-parsley-required   data-parsley-required-message="Please enter any part number" value="<?php echo e(app('request')->input('partNumber')); ?>" placeholder="Search for product" class="input-text" />
                                        <button type="submit" title="Search" class="action search" >
                                            <span>Search</span>
                                        </button>
                                    </div>
                                    <div id="error-container"> </div>
                                    <?php echo Form::close(); ?>

                                    <div class="search-history">
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#search-history">Search History <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                                    </div>

                            </div>
                            <?php if(Auth::check()): ?>
                                <div class="col-sm-12">
                                    <h4 class="myacc-heading">Quick Account Options</h4>
                                    <ul class="myacc-menu">
                                        <li><a href="<?php echo e(url('account-settings/'.Auth::user()->id)); ?>">Account Setting</a></li>
                                        <li><a href="<?php echo e(url('order-history')); ?>">Order History</a></li>
                                        <li><a href="<?php echo e(url('saved-quotes')); ?>">Saved Quotes</a></li>
                                        <li><a href="<?php echo e(url('my-bom')); ?>">My BOM</a></li>
                                        <li><a href="<?php echo e(url('apply-net-term-account')); ?>">Apply for a Net Term Account</a></li>
                                    </ul>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <!-- <div class="col-md-6">
                    <ul class="nav navbar-nav navbar-right checkout m-t15 hidden-xs">
                        <?php if(Cart::instance('quotecart')->content()->count() > 0): ?>
                    <li><a href="<?php echo e(url('/quote-cart')); ?>"><div class="icon"><img src="<?php echo e(url('frontend/assets/images/icon-quote-cart.png')); ?>" alt="Quote Requests" />  <em class="cart-count" id="quoteCount"><?php echo e(Cart::instance('quotecart')->content()->count()); ?></em>  </div> <span>Quote Requests</span> <br><span style="font-weight: 500; color: #000;"> </span></a></li>
                        <?php else: ?>
                    <li><a href="<?php echo e(url('/quote-cart')); ?>"><div class="icon"><img src="<?php echo e(url('frontend/assets/images/icon-quote-cart.png')); ?>" alt="Quote Requests" />  <em style="display: none;" class="cart-count" id="quoteCount"></em>  </div> <span>Quote Requests</span> <br><span style="font-weight: 500; color: #000;"> </span></a></li>
                        <?php endif; ?>
                <?php if(Cart::instance('shoppingcart')->content()->count() > 0): ?>
                    <li><a href="<?php echo e(url('/cart')); ?>"><div class="icon"><img src="<?php echo e(url('frontend/assets/images/icon-cart.png')); ?>" alt="Your Cart" /> <em class="cart-count" id="cartCount"><?php echo e(Cart::instance('shoppingcart')->content()->count()); ?></em></div><span>Your Cart</span> <br><span id="totalPrice"><?php echo e(Cart::instance('shoppingcart')->subtotal()); ?></span></a></li>
                        <?php else: ?>
                    <li><a href="<?php echo e(url('/cart')); ?>"><div class="icon"><img src="<?php echo e(url('frontend/assets/images/icon-cart.png')); ?>" alt="Your Cart" /> <em class="cart-count" style="display: none;" id="cartCount"></em></div><span>Your Cart</span> <br><span id="totalPrice"></span></a></li>
                        <?php endif; ?>
                        </ul>


                        <ul class="myacc-menu">
                            <li><a href="#">Account Setting</a></li>
                            <li><a href="#">Order History</a></li>
                            <li><a href="#">Saved Quotes</a></li>
                            <li><a href="#">My BOM</a></li>
                            <li><a href="#">Apply dor a Net Term Account</a></li>
                        </ul>
                    </div> -->
                </div>





            </div>
        </div>
    </nav>
    <?php echo $__env->make('frontend.partials.navigation', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('content'); ?>
    <?php echo $__env->make('frontend.partials.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('javascript'); ?>
    <!-- old: hccid=40037455 -->
    <script type="text/javascript">function add_chatinline(){var hccid=60761561;var nt=document.createElement("script");nt.async=true;nt.src="https://mylivechat.com/chatinline.aspx?hccid="+hccid;var ct=document.getElementsByTagName("script")[0];ct.parentNode.insertBefore(nt,ct);}
        add_chatinline(); </script>
    
    
        
            
            
                
                
                    
                    
                    
                    
                    
                
            
        
        
    
</div>
</body>
</html>