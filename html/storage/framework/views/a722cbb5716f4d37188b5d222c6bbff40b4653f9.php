<?php $request = app('Illuminate\Http\Request'); ?>

<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(url('admin/home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Users</li>
        </ol>
    </section>
    <h3 class="page-title">Users</h3>
    <div class="row">
        <div class="col-md-12">

    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-3">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label for="status_id" class="control-label">Status: </label>
                            </div>
                            <div class="col-sm-8">
                                <select name="status_id" id="status_id" class="form-control drop_down search_filter">
                                    <option value="">Select Status</option>
                                    <?php
                                    foreach($userStatus  as $list => $value){
                                        echo '<option value="'.$list.'">'.ucwords($value).'</option>';
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label for="are_you_a" class="control-label">Are You A: </label>
                            </div>
                            <div class="col-sm-8">
                                <?php $areYouA = config('constants.ARE_YOU_A');?>
                                <select name="are_you_a" id="are_you_a" class="form-control drop_down search_filter">
                                    <option value="">Select Are You A</option>
                                    <?php $__currentLoopData = $areYouA; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($key); ?>"><?php echo e($value); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label for="your_company_a" class="control-label">Your Company A: </label>
                            </div>
                            <div class="col-sm-8">
                                <?php $yourCompanyA = config('constants.YOUR_COMPANY_A');?>
                                <select name="your_company_a" id="your_company_a" class="form-control drop_down search_filter">
                                    <option value="">Select Your Company A</option>
                                    <?php $__currentLoopData = $yourCompanyA; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key => $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($key); ?>"><?php echo e($value); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-sm-3">
                                <label for="company_type" class="control-label">Main Industry: </label>
                            </div>
                            <div class="col-sm-8">
                                <select name="company_type" id="company_type" class="form-control drop_down search_filter">
                                    <option value="">-- Select Company Type --</option>
                                    <option value="OEM &dash; Original Equipment Manufacturer">OEM &dash; Original Equipment Manufacturer</option>
                                    <option value="ODM &dash; Original Design Manufacturer">ODM &dash; Original Design Manufacturer</option>
                                    <option value="EMS &dash; Electronic Manufacturing Service">EMS &dash; Electronic Manufacturing Service</option>
                                    <option value="University">University</option>
                                    <option value="Other">Other</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">    
                </div>
                <div class="col-lg-2 pull-right">
                    <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user_create')): ?>
                        <div class="form-group  label-static">
                            <a href="<?php echo e(route('admin.users.create')); ?>" class="btn btn-success">Add New User</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div>

               <table border="0" width="100%"  class="table table-striped table-bordered table-hover testing" id="userTable" >
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact Number</th>
                    <th>Location</th>
                    <th>Status</th>
                    <th>Are you a?</th>
                    <th>Your company a</th>
                    <th>Main Industry</th>
                    <th>Annual Component Purchase</th>
                    <th style="width:10%!important;">Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>
        $(document).ready(function() {
            userModule.loadUserDataTable();
            $(document).on('change', '.toggle-switch', function () {
                var msg = $(this).data('msg');
                var url = $(this).data('href');
                var id = $(this).data('id');
                var status = $(this).data('status');
                var $this = $(this);
                var state = $this.prop('checked');
                $this.prop('checked', !state).bootstrapToggle('destroy').bootstrapToggle();
                swal({
                        title: msg,
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Yes",
                        cancelButtonText: "No",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function (isConfirm) {
                        if (isConfirm) {
                            userModule.changeUserStatus(id,status,url)
                        }
                    });
            });
        });
        $(document).on('change', '.search_filter', function(){
            userModule.loadUserDataTableAgain();
        });
        <?php if (app(\Illuminate\Contracts\Auth\Access\Gate::class)->check('user_delete')): ?>
            window.route_mass_crud_entries_destroy = '<?php echo e(route('admin.users.mass_destroy')); ?>';
        <?php endif; ?>
    </script>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>