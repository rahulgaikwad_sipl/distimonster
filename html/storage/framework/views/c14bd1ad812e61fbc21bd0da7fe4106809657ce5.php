
<?php if($is_approved == '0'): ?>
    <?php $__env->startSection('title', '| Pending RFQ’s'); ?>
<?php elseif($is_approved ==1): ?>
    <?php $__env->startSection('title', '| Processed Quotes'); ?>
<?php elseif($is_approved ==2): ?>
    <?php $__env->startSection('title', '| Rejected Quotes'); ?>
<?php elseif($is_approved ==3): ?>
    <?php $__env->startSection('title', '| Expired Quotes'); ?>
<?php elseif($is_approved ==4): ?>
    <?php $__env->startSection('title', '| Ordered Quotes'); ?>
<?php elseif($is_approved == 'saved'): ?>
    <?php $__env->startSection('title', '| Quotes'); ?>
<?php endif; ?>
<?php $__env->startSection('content'); ?>
    <section class="gray-bg top-pad-with-bradcrumb">
        <div class="container-fluid clearfix container-w-80 user-admin">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <?php if($is_approved == '0'): ?>
                            <li class="breadcrumb-item active"><span>Pending RFQ’s</span></li>
                        <?php elseif($is_approved ==1): ?>
                            <li class="breadcrumb-item active"><span>Processed Quotes</span></li>
                        <?php elseif($is_approved ==2): ?>
                            <li class="breadcrumb-item active"><span>Rejected Quotes</span></li>
                        <?php elseif($is_approved ==3): ?>
                            <li class="breadcrumb-item active"><span>Expired Quotes</span></li>
                        <?php elseif($is_approved ==4): ?>
                            <li class="breadcrumb-item active"><span>Ordered Quotes</span></li>
                        <?php elseif($is_approved =="saved"): ?>
                            <li class="breadcrumb-item active"><span>Quotes</span></li>
                        <?php endif; ?>
                        
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
                         <?php echo $__env->make('frontend.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                   <div class="tab_container">
                            <div id="tab4" class="tab_content">
                                <?php if($is_approved == '0'): ?>
                                    <h2 class="block-title left-block-title">Pending Quotes</h2>
                                <?php elseif($is_approved ==1): ?>
                                    <h2 class="block-title left-block-title">Processed Quotes</h2>
                                <?php elseif($is_approved ==2): ?>
                                    <h2 class="block-title left-block-title">Rejected Quotes</h2>
                                <?php elseif($is_approved ==3): ?>
                                    <h2 class="block-title left-block-title">Expired Quotes</h2>
                                <?php elseif($is_approved ==4): ?>
                                    <h2 class="block-title left-block-title">Ordered Quotes</h2>
                                <?php elseif($is_approved =="saved"): ?>
                                    <h2 class="block-title left-block-title">Quotes</h2>
                                <?php endif; ?>
                                    <?php if(count($quotes)>0): ?>
                                        <?php $__currentLoopData = $quotes->chunk(2); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $items): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <div class="row">
                                                <?php $__currentLoopData = $items; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <div class="col-md-6">
                                                    <div class="shdow-box my-quote-detail">
                                                        <?php if($item->is_unnamed): ?>
                                                            <h3 class="f-s18"><a href="<?php echo e(url($item->id.'/quote-details')); ?>" >Unnamed</a></h3>
                                                        <?php else: ?>
                                                            <h3 class="f-s18"><a href="<?php echo e(url($item->id.'/quote-details')); ?>" ><?php echo e($item->quote_name?$item->quote_name:"Unnamed-".$item->id); ?></a></h3>
                                                        <?php endif; ?>
                                                        <div class="f-s14 text-gray btm-space">Quote Status:
                                                            <?php if($item->is_approved	  == 0 ): ?>
                                                                <span class="img-icon waiting-icon">
                                                                    <!-- <img src="<?php echo e(url('frontend/assets/images/pending.png')); ?>" alt="pending" />--><span class="waiting waiting-btn">Pending</span></span> 
                                                                <?php elseif($item->is_approved ==1): ?>
                                                                <span class="img-icon approved"><img src="<?php echo e(url('frontend/assets/images/approved.png')); ?>" alt="Processed" /><span class="success">Processed</span></span>
                                                                <?php elseif($item->is_approved ==2): ?>
                                                                <span class="img-icon rejected"><img src="<?php echo e(url('frontend/assets/images/rejected.png')); ?>" alt="rejected" /><span class="cancelled">Rejected</span></span>
                                                                <?php elseif($item->is_approved ==3): ?>
                                                                <span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/expiry.png')); ?>" alt="expired" /><span class="expiry">Expired</span></span>
                                                                <?php elseif($item->is_approved ==4): ?>
                                                                <span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/cart-small-icon.png')); ?>" alt="ordered" /><span class="textprimary">Ordered</span></span>
                                                                <?php endif; ?>
                                                        </div>
                                                        <div class="f-s14 text-gray btm-space">Total Parts: <span class="black font-w500"><?php echo e($item->part_counts); ?></span></div>
                                                        <div class="f-s14 text-gray btm-space">Last modified: <span class="black font-w500"><?php echo e(date('m/d/Y', strtotime($item->created_at))); ?></span></div>
                                                        
                                                        <div class="my-quote-detail-footer ">
                                                            <ul class="list-inline m-b0">
                                                                <li class="list-inline-item">
                                                                    <a href="<?php echo e(url('download-quote/'.$item->id)); ?>" class="f-s16 font-w500 hind-font btn-dark-green"><span class="img-icon"><i class="fa fa-download"></i></span>Download</a>
                                                                </li>
                                                                <?php if($item->is_approved ==1): ?>
                                                                    <li class="list-inline-item">
                                                                        <a href="<?php echo e(url('add-quote-to-cart/'.$item->id)); ?>" class="f-s16 font-w500 hind-font btn-dark-green"><span class="img-icon"><i class="fa fa-shopping-cart"></i></span>Add Quote to Cart</a>
                                                                    </li>
                                                                <?php endif; ?>
                                                                <li class="list-inline-item text-right">
                                                                    <a href="javascript:void(0)"  onclick="quoteModule.removeQuote('<?php echo $item->id;?>' ,1)"  class="f-s16 font-w500 hind-font pull-right remove-icon-pois"><span class="img-icon"><i class="fa fa-trash"></i></span></a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </div>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <div class="pagination-list text-right clearfix">
                                            <?php echo e($quotes->appends(request()->except('page'))->links('frontend.partials.pagination')); ?>

                                        </div>
                                    <?php else: ?>
                                        <div class="alert alert-info">
                                            <?php if($is_approved == '0'): ?>
                                                It seems, there are no Pending RFQ's.
                                            <?php elseif($is_approved ==1): ?>
                                                It seems, there are no Processed Quotes.
                                            <?php elseif($is_approved ==2): ?>
                                                It seems, there are no Rejected Quotes.
                                            <?php elseif($is_approved ==3): ?>
                                                It seems, there are no Expired Quotes.
                                            <?php elseif($is_approved ==4): ?>
                                                It seems, there are no Ordered Quotes.
                                            <?php elseif($is_approved == 'saved'): ?>
                                                It seems, there are no Quotes.
                                            <?php endif; ?>
                                        </div>
                                    <?php endif; ?>
                        </div>
                        <!-- .tab_container -->
                    </div>
                    <div class="shdow-box">
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>