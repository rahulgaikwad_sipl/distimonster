<input type="checkbox" class="toggle-switch" <?php echo e(($coupon->status == 1)?'checked':''); ?>

data-on="Active" data-off="Inactive" data-onstyle="success" data-offstyle="danger"
       data-msg="Are you sure you want to <?php echo e(($coupon->status == 1)?'Deactivate':'Activate'); ?> this coupon ?"
       data-href="/admin/update-coupon-status"  data-id="<?php echo e($coupon->id); ?>"  data-status="<?php echo e($coupon->status); ?>" data-size="small"
>