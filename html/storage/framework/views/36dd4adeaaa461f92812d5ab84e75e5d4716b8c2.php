<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <ol class="breadcrumb"> 
            <li><a href="<?php echo e(url('admin/home')); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li class="active">Requested Lead Times</li> 
        </ol>
    </section>
    <h3 class="page-title">Requested Lead Times</h3>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="status_id" class="control-label">Status: </label>
                                </div>
                                <div class="col-sm-8">
                                    <select name="status_id" id="status_id" class="form-control drop_down">
                                        <option value="">Select Status</option>
                                        <?php
                                        foreach($quoteStatus  as $list => $value){
                                            $selected = ('"'.$status.'"' == '"'.$value.'"') ? 'selected="selected"' : '';
                                            echo '<option '.$selected.' value="'.$value.'">'.ucwords($list).'</option>';
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <table border="0" width="100%"  class="table table-striped table-bordered table-hover testing" id="requestedproduct" >
                        <thead>
                        <tr>
                            <th>Quote Id</th>
                            <th>Quote Name</th>
                            <th>Customer Name</th>
                            <th>Number of parts</th>
                            <th>Creation date</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('javascript'); ?>
    <script>
        $(document).ready(function() {
            requestedProduct.loadReqProductTable();
        });
        $(document).on('change', '#status_id', function(){ 
            requestedProduct.requestedProductAgain();
        });
    </script>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>