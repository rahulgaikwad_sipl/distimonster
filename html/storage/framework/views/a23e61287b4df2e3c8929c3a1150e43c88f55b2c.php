
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="border-top:4px solid #0E88CA;">
    <tr>
        <td>
            <table border="0" align="center" cellpadding="0" cellspacing="0" style="width:650px;max-width:100%;">
                <tr>
                    <td align="center" style="padding:17px 0 28px;">
                        <a href="<?php echo e(url('/')); ?>" target="_blank" title="<?php echo e(config('app.name')); ?>"><img src="<?php echo e(url('frontend/assets/images/logo.png')); ?>" alt="DistiMonster" style="display:block;width:100% !important;height:auto !important;max-width:116px; " /></a> <!--/ logo-image-->
                    </td>
                    <!--//logo-->
                </tr> <!-- // main tr-->
            </table>
        </td>
    </tr><!--logo-->
    <tr>
        <td>
            <table border="0" align="center" cellpadding="0" cellspacing="0" style="text-align:center;width:650px;max-width:100%;background-color:#0E88CA;">
                <tr>
                    <td valign="top" style="background-color:#0E88CA;font-size:15px;color:#fff;height:32px;vertical-align:middle;width:235px;">
                        <a target="_blank" style="color:#fff;" href="<?php echo e(url('/about-us')); ?>" title="About Us">About Us</a>
                    </td><!--link first-->
                    <td valign="top" style="background-color:#0E88CA;font-size:15px;color:#fff;height:32px;vertical-align:middle;width:270px;">
                        <a target="_blank" style="color:#fff;" href="<?php echo e(url('/news-updates')); ?>" title="News & Updates">News &amp; Updates</a>
                    </td><!--link second-->
                    <td valign="top" style="background-color:#0E88CA;font-size:15px;color:#fff;height:32px;vertical-align:middle;width:270px;">
                        <a target="_blank" style="color:#fff;" href="<?php echo e(url('/bom')); ?>" title="BOM">BOM</a>
                    </td><!--link third-->
                </tr>
            </table>
        </td>
    </tr><!--links-->