
<?php $__env->startSection('title', '| Dashboard'); ?>
<!-- <link href="<?php echo e(url('/adminlte/css/AdminLTE.min.css')); ?>" rel="stylesheet"> -->

<?php $__env->startSection('content'); ?>
    <section class="gray-bg account-custome-view top-pad-with-bradcrumb">
        <div class="container-fluid container-w-80 clearfix user-dashboard user-admin">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">My Account</a></li>
                        <li class="breadcrumb-item active"><span>Dashboard</span></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabs_wrapper">
                   
                    <?php echo $__env->make('frontend.partials.sidebar', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                        <div class="tab_container">
                            <h2 class="block-title left-block-title">Dashboard</h2>
                            <div id="tab2" class="tab_content">
                                
                                            <div class="row">
                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="<?php echo e(url('/pending-quotes')); ?>">
                                                        <div class="small-box bg-green">
                                                            <div class="inner">
                                                                <h3>
                                                                    <?php if(isset($quote) && !empty($quote)): ?>
                                                                        <?php echo e($quote); ?>

                                                                    <?php else: ?>
                                                                        0
                                                                    <?php endif; ?>    
                                                                </h3>
                                                                <p>Pending RFQs</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-hourglass-start"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="<?php echo e(url('/processed-quotes')); ?>">
                                                        <div class="small-box bg-darkblue">
                                                            <div class="inner">
                                                                <h3>
                                                                    <?php if(isset($processedQuote) && !empty($processedQuote)): ?>
                                                                        <?php echo e($processedQuote); ?>

                                                                    <?php else: ?>
                                                                        0
                                                                    <?php endif; ?>    
                                                                </h3>
                                                                <p>Processed Quotes</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-newspaper-o"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="<?php echo e(url('/ordered-quotes')); ?>">
                                                        <div class="small-box bg-darkorange">
                                                            <div class="inner">
                                                                <h3>
                                                                    <?php if(isset($orderedQuote) && !empty($orderedQuote)): ?>
                                                                        <?php echo e($orderedQuote); ?>

                                                                    <?php else: ?>
                                                                        0
                                                                    <?php endif; ?>    
                                                                </h3>
                                                                <p>Ordered Quotes</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-file-text-o"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="<?php echo e(url('/order-history?tab=1&page=1')); ?>">
                                                        <div class="small-box bg-yellowgreen">
                                                            <div class="inner">
                                                                <h3>
                                                                    <?php if(isset($orders_shipped) && !empty($orders_shipped)): ?>
                                                                        <?php echo e($orders_shipped); ?>

                                                                    <?php else: ?>
                                                                        0
                                                                    <?php endif; ?>
                                                                </h3>
                                                                <p>Shipped Orders</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-cart-plus"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="<?php echo e(url('/order-history?tab=2&page=1')); ?>">
                                                        <div class="small-box bg-yellow">
                                                            <div class="inner">
                                                                <h3>
                                                                    <?php if(isset($orders_pending_ship) && !empty($orders_pending_ship)): ?>
                                                                        <?php echo e($orders_pending_ship); ?>

                                                                    <?php else: ?>
                                                                        0
                                                                    <?php endif; ?>
                                                                </h3>
                                                                <p>In Process Orders</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-clipboard"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="<?php echo e(url('/order-history?tab=4&page=1')); ?>">
                                                        <div class="small-box bg-darkpink">
                                                            <div class="inner">
                                                                <h3>
                                                                    <?php if(isset($orders_delivered) && !empty($orders_delivered)): ?>
                                                                        <?php echo e($orders_delivered); ?>

                                                                    <?php else: ?>
                                                                        0
                                                                    <?php endif; ?>
                                                                </h3>
                                                                <p>Delivered Orders</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-shopping-bag"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="javascript:void(0);">
                                                        <div class="small-box bg-purple">
                                                            <div class="inner">
                                                                <h3>
                                                                    <?php if(isset($working_boms) && !empty($working_boms)): ?>
                                                                        <?php echo e($working_boms); ?>

                                                                    <?php else: ?>
                                                                        0
                                                                    <?php endif; ?>
                                                                </h3>
                                                                <p>Working Monster-BOM</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-wpforms"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="<?php echo e(url('/saved-bom')); ?>">
                                                        <div class="small-box bg-gray">
                                                            <div class="inner">
                                                                <h3>
                                                                    <?php if(isset($saved_boms) && !empty($saved_boms)): ?>
                                                                        <?php echo e($saved_boms); ?>

                                                                    <?php else: ?>
                                                                        0
                                                                    <?php endif; ?>
                                                                </h3>
                                                                <p>Saved Monster-BOM</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-floppy-o"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>

                                                <!-- ./col -->
                                                <div class="col-lg-3 col-md-6 col-xs-6">
                                                    <!-- small box -->
                                                    <a href="<?php echo e(url('/shared-bom')); ?>">
                                                        <div class="small-box bg-brown">
                                                            <div class="inner">
                                                                <h3>
                                                                    <?php if(isset($shared_boms) && !empty($shared_boms)): ?>
                                                                        <?php echo e($shared_boms); ?>

                                                                    <?php else: ?>
                                                                        0
                                                                    <?php endif; ?>
                                                                </h3>
                                                                <p>Shared Monster-BOM</p>
                                                            </div>
                                                            <div class="icon">
                                                                <i class="fa fa-share-square-o"></i>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                        
                                   
                                
                            </div>
                            <!-- #tab2 End-->
                        </div>
                        <!-- .tab_container -->
                    </div>
                    <div class="shdow-box">
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
    <script>
        $('#register_form').parsley();
    </script>
<?php $__env->stopSection(); ?>





<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>