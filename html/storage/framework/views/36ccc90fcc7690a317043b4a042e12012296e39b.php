<?php $__env->startSection('content'); ?>
    <h3 class="page-title"><b>User Deatails: </b><?php echo e($user->name); ?> <?php echo e($user->last_name); ?></h3>

    <div class="panel panel-default">
        
        <div class="panel-body table-responsive">
            <div class="row">
                <div class="col-md-6">
				<div class="panel-heading">
            <b>My BIO</b>
        </div>

                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Are you a?</th>
                            <td field-key='name'><?php echo e($bio->are_you_a); ?></td>
                            <?php if($bio->are_you_a=='Other'): ?>
                            <td field-key='name'><?php echo e($bio->are_you_a_text); ?></td>
                            <?php else: ?>
                            <td field-key='name'>&nbsp;</td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <th>Is your company a?</th>
                            <td field-key='email'><?php echo e($bio->your_company_a); ?></td>
                            <?php if($bio->your_company_a=='Other'): ?>
                            <td field-key='name'><?php echo e($bio->your_company_a_text); ?></td>
                            <?php else: ?>
                            <td field-key='name'>&nbsp;</td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <th>What is your main Industry?</th>
                            <td field-key='role'><?php echo e($bio->your_main_industry); ?></td>
                            <?php if($bio->your_main_industry=='Other'): ?>
                            <td field-key='name'><?php echo e($bio->your_main_industry_text); ?></td>
                            <?php else: ?>
                            <td field-key='name'>&nbsp;</td>
                            <?php endif; ?>
                        </tr>
                        <tr>
                            <th>Address</th>
                            <td field-key='role'><?php echo e($user->billing_address_line1 . $user->billing_address_line2 . $user->zip_code?:'N/A'); ?></td>
                            <td field-key='name'>&nbsp;</td>
                        </tr>
                        <tr>
                            <th>Phone</th>
                            <td field-key='role'><?php echo e($user->contact_number?:'N/A'); ?></td>
                            <td field-key='name'>&nbsp;</td>
                        </tr>
                        <tr>
                            <th>Accounts Payable Contact Name</th>
                            <td field-key='role'><?php echo e($user->a_c_contact_name?:'N/A'); ?></td>
                            <td field-key='name'>&nbsp;</td>
                        </tr>
                        <tr>
                            <th>Accounts Payable Email</th>
                            <td field-key='role'><?php echo e($user->a_c_contact_email?:'N/A'); ?></td>
                            <td field-key='name'>&nbsp;</td>
                        </tr>
                    </table>
                </div>
				  <div class="col-md-6">
				  <div class="panel-heading"><b>Company Information</b></div>

                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Company Name</th>
                            <td field-key='name'><?php echo e($user->company_name?:'N/A'); ?></td>
							 <th>Year of Established</th>
                            <td field-key='name'><?php echo e($user->year_of_establish?:'N/A'); ?></td>
                           
                        </tr>
                         <tr>
                            <th>Number of Employee</th>
                            <td field-key='name'><?php echo e($user->employee?:'N/A'); ?></td>
							 <th> Company Type</th>
                            <td field-key='name'><?php echo e($user->company_type?:'N/A'); ?></td>
                           
                        </tr>
                         <tr>
                            <th>Annual Component Purchase</th>
                            <td field-key='name'><?php echo e($user->annual_component_purchase?:'N/A'); ?></td>
							 <th>How you found Distimonster</th>
                            <td field-key='name'><?php echo e($user->know_about_distimonster?:'N/A'); ?></td>
                           
                        </tr>
                    </table>
                  <div class="panel-heading"><b>Subscription Information</b></div>

                    <table class="table table-bordered table-striped">
                        <tr>
                            <th>Subscription Date</th>
                            <td field-key='name'><?php echo e(date('m/d/Y', strtotime($user->plan_purchase_date))); ?></td>
						</tr>
                        <tr>
                            <th>Subscription Expiry Date</th>
                            <td field-key='name'><?php echo e(date('m/d/Y', strtotime($user->plan_expiry_date))); ?></td>
						</tr>
                        <tr>
                            <th>Subscription Free Trial Expiry Date</th>
                            <td field-key='name'><?php echo e(date('m/d/Y', strtotime($user->free_trial_expiry))); ?></td>
						</tr>
                    </table>  
                </div>
            </div>
			
            <h3 style="margin-bottom:20px;">Team Members</h3>
            <table id="subUserListing" class="table table-striped table-bordered table-hover testing">
                                                    <thead>
                                                        <tr>
                                                            <th>First Name</th>
                                                            <th>Last Name</th>
                                                            <th>Email</th>
                                                            <th>Note</th>
                                                            <th>Subscription Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $__currentLoopData = $subUserList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subUser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                        <tr>
                                                            <td><?php echo e($subUser->name); ?></td>
                                                            <td><?php echo e($subUser->last_name); ?></td>
                                                            <td><?php echo e($subUser->email); ?></td>
                                                            <td><?php if(isset($subUser->note) && !empty($subUser->note)): ?>
                                                                    <?php echo e($subUser->note); ?>

                                                                <?php else: ?>
                                                                    NA
                                                                <?php endif; ?>
                                                            </td>
                                                            <td><?php echo e(date('m/d/Y', strtotime($subUser->plan_purchase_date))); ?></td>
                                                        </tr>
                                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>      
                                                    </tbody>
                                                </table>
            <p>&nbsp;</p>

            <a href="<?php echo e(route('admin.users.index')); ?>" class="btn btn-default"><?php echo app('translator')->getFromJson('quickadmin.qa_back_to_list'); ?></a>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#subUserListing').DataTable();
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>