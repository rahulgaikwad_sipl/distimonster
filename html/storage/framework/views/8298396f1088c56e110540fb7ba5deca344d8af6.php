<?php if($order->order_shipping_status  == 1 ): ?>
    <a class='label label-primary' title="Click To Change Status"  onclick="orderModule.changeShippingStatus('<?php echo  $order->order_shipping_status  ?>',  '<?php echo $order->id ?>',  '<?php echo $order->order_tracking_info ?>')"  href="javascript:void(0);"><?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.shipped')); ?></a>
<?php elseif($order->order_shipping_status  == 2): ?>
    <a class='label label-danger' title="Click To Change Status"  onclick="orderModule.changeShippingStatus('<?php echo  $order->order_shipping_status  ?>',  '<?php echo $order->id ?>',  '<?php echo $order->order_tracking_info ?>')"  href="javascript:void(0);">
    <?php echo e(($order->partially_shipped  == 1) ? config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.partially_shipped') : config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.pending')); ?></a>
<?php elseif($order->order_shipping_status  == 3): ?>
    <a class='label label-warning' title="Click To Change Status"  onclick="orderModule.changeShippingStatus('<?php echo  $order->order_shipping_status  ?>',  '<?php echo $order->id ?>',  '<?php echo $order->order_tracking_info ?>')"  href="javascript:void(0);"><?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.intransit')); ?></a>
<?php elseif($order->order_shipping_status  == 4): ?>
    <a class='label label-success' title="Click To Change Status"  onclick="orderModule.changeShippingStatus('<?php echo  $order->order_shipping_status  ?>',  '<?php echo $order->id ?>',  '<?php echo $order->order_tracking_info ?>')"  href="javascript:void(0);"><?php echo e(config('constants.APP_CONSTANT.ORDER_SHIPPING_STATUS.delivered')); ?></a>
<?php endif; ?>
