
<?php $__env->startSection('title', '| Cart'); ?>
<?php $__env->startSection('content'); ?>

    <!--Cart Listing Section-->
    <section class="gray-bg shipping-cart">
        <div class="container clearfix">
            <div class="row pagination">
                <div class="col-md-12">
                    <ul class="breadcrumb f-s14 text-gray p-l0">
                        <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><span>Cart</span></li>
                    </ul>
                </div>
            </div>

            <div class="row clearfix">
                <div class="col-md-9">
                    <h2 class="block-title">Shopping Cart</h2>
                </div>
                <?php
               // echo "<pre>";
             //   print_r(Cart::instance('shoppingcart')->content());
            // echo "</pre>";
//die;
                ?>
                <?php if(count(Cart::instance('shoppingcart')->content()) > 0): ?>
                <div class="col-md-3 text-right">
                    
                        
                    
                    <div class="d-inline-block f-s16">
                        <a  title="Clear All" onclick="cartModule.emptyCart()" href="javascript:void(0)" class="f-s16 hind-font"><i class="fa fa-times-circle"></i></span> Clear All</a>
                    </div>
                </div>
                <?php endif; ?>
            </div>

            <hr/>
<?php // echo "<pre>"; print_r(Cart::instance('shoppingcart')->content()); echo "</pre>"; ?>
            <div class="row">
                <div class="col-md-12"></div>
                <div class="col-md-8">
                    <div class="row cart-heading">
                        <?php if(count(Cart::instance('shoppingcart')->content()) > 0): ?>
                        <div class="col-md-4">
                            <span class="f-s16 font-w700 black">Product Detail</span>
                        </div>
                        <div class="col-md-3">
                            <span class="f-s16 font-w700 black">Price</span>
                        </div>
                        <div class="col-md-3">
                            <span class="f-s16 font-w700 black">Quantity</span>
                        </div>
                        <div class="col-md-2">
                            <span class="f-s16 font-w700 black">Subtotal</span>
                        </div>
                        <?php else: ?>
                         <div class="col-md-12">
                            <span class="f-s16 font-w500 black">Cart is empty</span>
                        </div>
                        <?php endif; ?>
                    </div>
                    <?php //echo "<pre>"; print_r(Cart::instance('shoppingcart')->content()); die();?>
                    <!--Shiiping Listing Content-->
                    <?php foreach(Cart::instance('shoppingcart')->content() as $row) :?>
                    <div class="shdow-box ">
                        <div class="shipping-listing-content clearfix">
                            <div class="row">
                                <div class="col-md-4 product-detail-content">
                                    <div class="f-s18 m-b5"><a href="javascript:void(0)" id="" class="hind-font">  MPN:  <?php echo e($row->name); ?></a></div>
                                    <div class="text-gray f-s14" style="display: none;">Source Id <a href="javascript:void(0)" data-toggle="tooltip" title="This ID is for internal use only and does not reflect anything about the MPN."><i class="fa fa-question-circle"></i></a> : <span class=" black font-w500 m-l5"> <?php echo e($row->options->sourcePartId); ?></span></div>
                                    <div class="text-gray f-s14">Manufacturer: <span class="black font-w500 m-l5">  <?php echo e($row->options->has('manufacturer') ? $row->options->manufacturer : 'N/A'); ?></span></div>
                                    <div class="text-gray f-s14">Date Code: <span class="black font-w500 m-l5"><?php echo e($row->options->dateCode?$row->options->dateCode:'N/A'); ?></span></div>
                                    <div class="text-gray f-s14">Internal Part#: <span class="black font-w500 m-l5"><?php echo e($row->options->customerPartNo ? $row->options->customerPartNo : 'N/A'); ?></span></div>
                                    <div class="text-gray f-s14">Distributor: <span class="black font-w500 m-l5"><strong>
                                                    <?php echo e($row->options->has('distributorName') ? $row->options->distributorName : 'N/A'); ?>

                                                </strong></span></div>
                                </div>
                                <div class="col-md-3 product-price">
                                    <div class="f-s18 m-b5">&nbsp;</div>
                                    <div class="black font-w500 f-s14">$<?php echo e(number_format($row->price, 3, '.', ',')); ?></div>
                                    <?php if($row->options->isQuoteApprovedItem !=1): ?>
                                    <div class="dropdown f-s14 keep-inside-clicks-open">
                                        <a href="javascript:void(0)" class="dropdown-toggle"  id="about-us" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Price Breaks
                                        </a>
                                        <div class="dropdown-menu " aria-labelledby="about-us" id='tierPrice_<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>' >
                                             <?php $__currentLoopData = $row->options->resaleList; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tierPrices): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                
                                                <a class="dropdown-item" href="javascript:void(0)">+<?php echo e(isset($tierPrices->minQty) ? $tierPrices->minQty : $tierPrices['minQty']); ?> - $<?php echo e(number_format(isset($tierPrices->displayPrice) ? $tierPrices->displayPrice : $tierPrices['displayPrice'], 3, '.', ',')); ?>

                                                </a>
                                             <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </div>
                                    </div>
                                        <?php endif; ?>
                                </div>
                                <div class="col-md-3 product-quantity">
                                    <?php if($row->options->original_quantity > $row->qty): ?>

                                    <div class="error-icon">
                                        <a href="javascript:void(0)" data-toggle="tooltip" title="The quantity entered exceeded the available quantity and has been automatically changed."><img src="<?php echo e(url('frontend/assets/images/error.png')); ?>" alt="Error" class="error-img" /></a>
                                    </div>
                                    <?php endif; ?>
                                    <div>
                                    </div>
                                        <?php if($row->options->quotedItem == 1 && $row->options->isQuoteApprovedItem): ?>
                                            <div class="col-md-2 product-subtotal">
                                                <div class="f-s18 m-b5">&nbsp;</div>
                                                <div class="black font-w500 f-s14"><?php echo e($row->qty); ?></div>
                                                <input type="hidden" readonly min="1" max="99999"  value='<?php echo e($row->qty); ?>' class='qty' style="margin-bottom: 0px !important" />
                                            </div>
                                        <?php else: ?>
                                            <div class="f-s18 m-b5">&nbsp;</div>
                                            <div class="black font-w500 f-s14"><?php echo e($row->qty); ?></div>
                                            <input type="hidden" min="1" max="99999" onblur="cartModule.addToCart('quantity_<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>' ,'<?php echo $row->id; ?>','<?php echo $row->options->manufacturer; ?>','<?php echo $row->options->manufacturerCode; ?>','<?php echo $row->options->sourcePartId;?>','<?php echo $row->options->customerPartNo;?>','<?php echo $row->options->stockAvailability;?>');" id='quantity_<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>' name='quantity' value='<?php echo e($row->qty); ?>' class='qty' style="margin-bottom: 0px !important" onkeypress='return event.charCode >= 48 && event.charCode <= 57'/>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-2 product-subtotal">
                                    <div class="f-s18 m-b5">&nbsp;</div>
                                    <div class="black font-w500 f-s14">$<?php echo e(number_format($row->price * $row->qty, 3, '.', ',')); ?></div>
                                </div>

                                <input type="hidden" class="bor-radius-none customerPartNo form-control quantity-box" value='<?php echo json_encode($row->options->resaleList)?>' id="resaleList_<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>" name='resaleList'  placeholder="resaleList" />
                                <input type="hidden" class="bor-radius-none customerPartNo form-control quantity-box"  value='<?php echo json_encode($row->options->availableData)?>' id="availableData_<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>" name='availableData'  placeholder="availableData" />
                            </div>
                            <div class="shipping-footer">
                                <div class="row">
                                    <div class="col-md-12 text-right">
                                        <?php if($row->options->isQuoteApprovedItem !=1): ?>
                                            <div class="d-inline-block f-s16">
                                                <a data-toggle="modal" data-target="#edititem" href="javascript:void(0)" onclick='cartModule.showEditPopup("<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>",<?php echo json_encode($row); ?>)' class="f-s16 hind-font"><span class="img-icon"><i class="fa fa-edit"></i></span> Edit</a>
                                            </div>
                                            <div class="d-inline-block f-s16">
                                               <?php /*<a title="Request specific prices" href="javascript:void(0)" onclick="quoteModule.changeQtyToQuote('<?php echo $row->options->distributorName;?>', '<?php echo $row->options->sourceMinQuantity ?>', 'quantity_<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>' ,'<?php echo $row->id; ?>','<?php echo $row->options->manufacturer; ?>','<?php echo $row->options->manufacturerCode; ?>', '<?php echo $row->options->sourcePartId;?>','<?php echo $row->options->customerPartNo;?>','<?php echo $row->options->stockAvailability;?>','resaleList_<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>','availableData_<?php echo  str_replace(':', '_', $row->options->sourcePartId); ?>','<?php echo $row->options->dateCode; ?>')" class="f-s16 hind-font"><span class="img-icon"><img src="{{url('frontend/assets/images/add-quote-icon.png')}}" alt="quote-icon" /></span> Add to Request for Quote</a>*/ ?>
                                            </div>
                                            <div class="d-inline-block f-s16">
                                                <a title="Remove"  href="javascript:void(0)" class="f-s16 hind-font" onclick="cartModule.removeCartItem('<?php echo $row->rowId;?>')"><span class="img-icon"><i class="fa fa-trash"></i></span> Remove</a>
                                            </div>
                                        <?php else: ?>
                                            <div class="d-inline-block f-s16 pull-left">
                                              <p> * This is a custom approved quote, hence cannot be edited </p>
                                            </div>

                                            <div class="d-inline-block f-s16">
                                                <a title="Remove"  href="javascript:void(0)" class="f-s16 hind-font" onclick="cartModule.removeCartItem('<?php echo $row->rowId;?>')"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/remove-icon.png')); ?>" alt="remove-icon" /></span> Remove</a>
                                            </div>
                                        <?php endif; ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach;?>
                
                  
                
                    <!--Shipping Listing Content-->
                </div>
                <?php if(Cart::instance('shoppingcart')->content()->count()): ?>
                <div class="col-md-4">
                    <div class="shdow-box shipping-right-content">
                        <div class="price-content-box">
                            <div class="row price-box-heading">
                                <div class="col-md-10">
                                    <h2 class="block-title">Order Summary</h2>
                                </div>
                                <div class="col-md-2">
                                    &nbsp;
                                </div>
                            </div>
                          <!--  <div class="price-content-body">
                                <ul>
                                    <li class="f-s16 font-w500">Estimate Shipping and Tax </li>
                                    <li class="clearfix">
                                        <span class="pull-left text-gray f-s14">Subtotal</span>
                                        <span class="pull-right text-gray f-s14">$<?php echo Cart::instance('shoppingcart')->subtotal(); ?></span>
                                    </li>
                                    <li class="clearfix">
                                        <span class="pull-right text-gray f-s14">$0.00</span>
                                    </li>
                                    <li class="f-s16 black font-w500 clearfix">
                                        <span class="pull-left">Order Total</span>
                                        <span class="pull-right">$<?php echo Cart::instance('shoppingcart')->subtotal(); ?></span>
                                    </li>
                                </ul>
                            </div>-->
                            <div class="price-content-body">
                                <ul>
                                    <li class="f-s16 black font-w500 clearfix">
                                        <span class="pull-left">Sub Total</span>
                                        <?php
                                        $finalTotal =  0;
                                        $subTotal=0;
                                        foreach(Cart::instance('shoppingcart')->content() as $row) {
                                            $subTotal =  floatval($row->price) *$row->qty;
                                            $finalTotal =  $subTotal+$finalTotal;
                                        }?>
                                        <span class="pull-right">$<?php echo  number_format($finalTotal, 2, '.', ',');  //number_format(Cart::instance('shoppingcart')->subtotal(), 2, '.', ',') ; ?></span>
                                    </li>
                                </ul>
                            </div>
                            <div class="price-content-footer">
                                <a href="<?php echo e(url('shipping')); ?>" type="submit" class="btn btn-primary" title="Proceed to Checkout">Proceed to Checkout</a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <!--Cart Listing Section-->
    <!-- Modal -->
    <div class="modal" id="edititem" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabel">Update Item</h4>
                    <button type="button" class="close" data-dismiss="modal" onclick="cartModule.closeEditItemModel()" aria-label="Close">
                        <span aria-hidden="true"><img src="<?php echo e(url('frontend/assets/images/close-icon.png')); ?>" alt="close-icon" /></span>
                    </button>
                </div>
                <div class="modal-body">
                    <?php echo Form::open(['class'=>'form-horizontal','method' => 'POST', 'id' => 'edit_cart_form', 'data-parsley-validate' => true]); ?>

                    <div class="price-content-box">
                        <div class="price-content-body">
                          <div class="clearfix m-b5 f-s14">
                              <div class="text-gray" style="display: none;">Source Id: <a href="javascript:void(0)" class="f-s18 hind-font" id="popupItemSourcePartId"></a> </div>
                          </div>
                            <div class="clearfix m-b5">
                                <span class="black f-s18 font-w500 pull-left" id="popupItemPrice"></span>
                                <div class="dropdown f-s14 pull-left viewtiers keep-inside-clicks-open">
                                    <a href="javascript:void(0)" class="dropdown-toggle "  id="about-us" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Price Breaks
                                    </a>
                                    <div class="dropdown-menu"  aria-labelledby="about-us" id="popupViewTiers">
                                  </div>
                                </div>
                            </div>
                            <div class="clearfix m-b5 f-s14">
                                <div class="text-gray">Available: <span class="black font-w500" id="popupAvailableItem"></span>  </div>
                            </div>
                            <div class="clearfix m-b5 f-s14">
                                <div class="text-gray">Minimum: <span class="black font-w500" id="popupMinimumQty"></span>&nbsp;&nbsp;&nbsp;Increment: <span class="black font-w500">1</span></div>
                            </div>
                            <div class="clearfix m-b5 f-s14"></div>
                            <div class="quantity">
                                <div class="quantity-box">
                                    <form id='myform' method='POST' action='#' class="numbo">
                                        <input type='text' data-parsley-errors-container="#qty-error" data-parsley-required data-parsley-type="digits"  data-parsley-maxlength="10" placeholder="Quantity" name='quantity' id="popupQuantity" value='1' class='qty' style="margin-bottom: 0px !important" />
                                        <input type='button' onclick="cartModule.increaseQtyOfPopupProduct()" value='+' class='qtyplus ' field='quantity' style="font-weight: bold;" />
                                        <input type='button' onclick="cartModule.decreaseQtyOfPopupProduct()" value='-' class='qtyminus' field='quantity' style="font-weight: bold;" />
                                    </form>
                                </div>
                                <span id="qty-error"></span>
                            </div>

                            <div class="quantity customer-part f-s14 text-gray">
                               <!--  <div class="quantity-box m-b0">
                                    Customer Part #
                                </div> -->
                                <input type="text"  name="popupCustomerPartNo" value="" id="popupCustomerPartNo" placeholder="Internal Part#" data-parsley-maxlength="30" class="quantity-box" />
                            </div>
                        </div>
                    </div>
                    <?php echo Form::close(); ?>

                </div>

                    <div class="modal-footer">
                    <button data-dismiss="modal" type="submit" class="btn btn-primary btn-outline" title="Cancel" onclick="cartModule.closeEditItemModel()"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/pop-up-close-icon.png')); ?>" alt="pop-up-close-icon" /></span>Cancel</button>
                    <input type="hidden" name="hiddenQtyId" id="hiddenQtyId" value="" />
                    <input type="hidden" name="cartItems" id="cartItems"  value="" />
                        <input type="hidden" name="hiddenSourceMinQty" id="hiddenSourceMinQty"  value="" />

                    <input type="hidden" name="popupStockAvailability" id="popupStockAvailability"  value="" />

                    <button type="button" onclick="cartModule.editItemFromPopup()" class="btn btn-primary" title="Edit"><span class="img-icon"><img src="<?php echo e(url('frontend/assets/images/pop-up-edit-icon.png')); ?>" alt="pop-up-edit-icon" /></span>Update</button>
                </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>