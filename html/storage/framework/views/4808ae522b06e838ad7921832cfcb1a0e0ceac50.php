<section class="section-special-fbox">
   <!--  <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="special-fbox-media">
                    <div class="special-fbox-media-img">
                        <img class="media-object" src="<?php echo e(url('frontend/assets/images/icon-free-shipping.png')); ?>" alt="Free Shipping">
                    </div>
                    <div class="special-fbox-media-body">
                        <h4 class="media-heading">Free Shipping</h4>
                        <p>Lorem 15% ipsum dolor sit amet, cons ectetur. Limited time offer.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="special-fbox-media">
                    <div class="special-fbox-media-img">
                        <img class="media-object" src="<?php echo e(url('frontend/assets/images/icon-special-pricing.png')); ?>" alt="Special Pricing">
                    </div>
                    <div class="special-fbox-media-body">
                        <h4 class="media-heading">Special Pricing</h4>
                        <p>Save 10% Lorem ipsum dolor sit amet, Order now.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="special-fbox-media">
                    <div class="special-fbox-media-img">
                        <img class="media-object" src="<?php echo e(url('frontend/assets/images/icon-military.png')); ?>" alt="Government and Military">
                    </div>
                    <div class="special-fbox-media-body">
                        <h4 class="media-heading">Government and Military</h4>
                        <p>Save 5% on your entire purchase. You must have a .gov or .mil email address to qualify.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>