<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=2.0" />
    <title>DistiMonster - New BOM Received</title>
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Hind:400,500,600,700" rel="stylesheet">
    <style type="text/css">
        body{background:#f8f8f8;width:100%;margin:0px;padding:0px;text-align:center;font-family:'Poppins', Arial, sans-serif;}
        html{width:100%;}
        img{border:0px; text-decoration:none;outline:none;}
        a, a:hover{color:#ee7716;text-decoration:none;}
        table{border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;}
        .main-bg{}
        a:hover{text-decoration:none;}
        table,td,a,p{font-family:'Poppins', Arial, sans-serif;}
        @media  only screen and (max-width:640px){body{width:auto!important;}
            .main{width:440px !important;margin:0px;padding:0px;}
            .two-left, .m-width{width:100% !important;text-align: center!important;}
            .m-width{text-align:left !important;}
            .image-hide{display:none !important;}
            .m-width{margin-top:0px;}
            .m-padding{padding-left:0px !important;text-align:center !important;}
            h4{margin-bottom:6px;width:100% !important}
            .m-width-space{width:8% !important;}
            .img-reponsive img{width:100% !important;}
        }
        @media  only screen and (max-width:479px) {
            body{width:auto!important;}
            .main{width:280px !important;margin:0px;padding:0px;}
            .two-left{text-align: center !important;}
        }
        .img-top,.img-bottom{background:#f5f5f5;}
        .img-top{border-left:1px solid #f5f5f5;border-right:1px solid #f5f5f5;border-top: 1px solid #f5f5f5;border-radius:5px 5px 0 0;-moz-border-radius:5px 5px 0 0;-ms-border-radius:5px 5px 0 0;-o-border-radius:5px 5px 0 0;-webkit-border-radius:5px 5px 0 0;}
        .img-bottom{border-radius:0 0 5px 5px;-moz-border-radius:0 0  5px 5px;-ms-border-radius:0 0 5px 5px;-o-border-radius:0 0 5px 5px;-webkit-border-radius:0 0  5px 5px;border-left:1px solid #f5f5f5;border-right:1px solid #f5f5f5;border-bottom:1px solid #f5f5f5;}
    </style>
</head>
<body style="color:#f5f5f5;">
<?php echo $__env->make('emails.layout.head', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldContent('content'); ?>
<?php echo $__env->make('emails.layout.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</body>
</html>