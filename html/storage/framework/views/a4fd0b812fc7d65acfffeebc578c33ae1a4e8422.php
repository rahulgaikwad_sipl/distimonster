<?php $__env->startSection('content'); ?>
    <tr>
        <td align="center" valign="top" style="padding:0px;"><!--main section start-->
            <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:650px;max-width:100%;">
                <tr>
                    <td align="left" valign="top" bgcolor="#f5f5f5"><!--content section-->
                        <table border="0" align="center" cellpadding="0" cellspacing="0" class="main" style="width:100%;">
                            <tr>
                                <td align="center" valign="top">
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                        <tr>
                                            <td align="left" valign="top" style="font:Bold 14px Arial, Helvetica, sans-serif; color:#666;padding:0px;background:#fff;padding:25px 25px;">
                                                <table border="0" align="left" cellpadding="0" cellspacing="0" class="two-left">
                                                    <tr><td style="height:25px;">&nbsp;</td></tr><!--spacing-->
                                                    <tr>
                                                        <td style="font-size:22px;color:#666;line-height:121%;padding:0 0 15px;">Hello <?php echo e(ucwords($firstName)); ?>,</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="font-size:15px;color:#666;font-weight:normal;line-height:25px;padding:0 12px 5px 0" align="left">We have received your application for an open account. Please allow up to 3 working days to process. Once our accounting department has processed you will receive an email with a full update. You can process orders via Credit Card or PayPal in the meantime</td>
                                                    </tr>
                                                </table>

                                            </td>
                                        </tr>
                                        <!--footer content-->
                                    </table><!--// center content-->
                                </td>
                            </tr>
                        </table><!--content section end-->
                    </td>
                </tr><!-- // main tr-->
            </table>
        </td><!-- //main section start-->
    </tr><!--//main wrap tr-->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('emails.layout.master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>