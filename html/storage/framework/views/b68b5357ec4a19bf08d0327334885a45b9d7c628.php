
<?php $__env->startSection('title', '| Return Policy'); ?>
<?php $__env->startSection('content'); ?>

    <!--START: Cart Page-->
 <section class="gray-bg top-pad-with-bradcrumb">
         <div class="container clearfix">
            <div class="row pagination">
               <div class="col-md-12">
                  <ul class="breadcrumb f-s14 text-gray p-l0">
                     <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Home</a></li>
                        <li class="breadcrumb-item active"><span>About</span></li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="tabs_wrapper faq-block">
                      <?php echo $__env->make('frontend.partials.about', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     <div class="tab_container" style="min-height: 191px;">
                        <a href="<?php echo e(url('faq')); ?>">  <h3 class="tab_drawer_heading" rel="tab1">FAQs</h3></a>

                        <!-- #tab1 End-->
                       
                        <div id="tab2" class="tab_content" style="display: none;">
                           <!-- <h2 class="block-title left-block-title">About</h2> -->
                           <div class="shdow-box edit-account-setting">
                              <div class="edit-account-setting-content f-s15 text-gray">
                                 
                          <h2 class="block-title f-s19">DISTIMONSTER — AN INDUSTRY FIRST.</h2>
                          <div class="sub-title">
                          <p class="f-s15 text-gray">DistiMonster was founded in 2013 and launched in 2019 by industry veterans with a knowledge base that includes distribution, supply chain and digital transformation.</p>
                           
                           <p class="f-s15 text-gray">YOU ASKED FOR IT. WE DELIVERED.<br>An industry-first platform created for buyers and engineers — it’s what they want and need to perform their jobs efficiently. DistiMonster simplifies the tedious process of sourcing and buying components from multiple distributors. With a single Monster-BOM upload AND a single PO, you can find and buy components for your project.</p>
                            <p class="f-s15 text-gray">Immediately reduce labor costs with less time wasted searching multiple sites for the same parts. And with an average cost of $160 to generate each purchase order for each distributor, companies will see additional cost savings.</p>
                            <p class="f-s15 text-gray">A personalized dashboard contains all the information about the projects you are working on — from orders and reorders to invoices to tracking information—DistiMonster makes it simple. You can collaborate and share dashboard data with other DistiMonster subscribers in your company.</p>
                            <p class="f-s15 text-gray">DistiMonster is a straightforward subscription based service. You pay a flat monthly or yearly fee with no additional product markup.</p>
                            <p class="f-s15 text-gray">SIMPLE —1 SOURCE, 1 PO, 1 SHIPMENT —DISTIMONSTER.</p>

								 </div>
                           </div>
                        </div>
                        <!-- #tab2 End-->
                        <a href="<?php echo e(url('contact-us')); ?>">  <h3 class="tab_drawer_heading" rel="tab1">Contact Us</h3></a>

                        <!-- #tab3 End-->
                     </div>
                     <!-- .tab_container -->
                  </div>
                  <div class="shdow-box">
                  </div>
               </div>
            </div>
         </div>
      </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
   <script>
      $('#chat_link').click(function () {
          MyLiveChat.InlineChatDoExpand();
          //$('.mylivechat_sprite').trigger('click');
      });
   </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>