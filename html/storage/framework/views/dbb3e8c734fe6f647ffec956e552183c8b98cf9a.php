
<?php $__env->startSection('title', '| FAQ'); ?>
<?php $__env->startSection('content'); ?>
    <!--START: FAQ Page-->
    <section class="gray-bg top-pad-with-bradcrumb">
         <div class="container clearfix">
            <div class="row pagination">
               <div class="col-md-12">
                  <ul class="breadcrumb f-s14 text-gray p-l0">
                     <li class="breadcrumb-item"><a href="#">Support</a></li>
                     <li class="breadcrumb-item active"><span>FAQs</span></li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="tabs_wrapper faq-block">
                    <?php echo $__env->make('frontend.partials.help', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
                     <div class="tab_container" style="min-height: 191px;">
                        <h3 class="tab_drawer_heading" rel="tab1">FAQs</h3>
                        <div id="tab1" class="tab_content">
                           <h2 class="block-title left-block-title">FAQs</h2>
                           <div id="accordion" class="accordion">
                              <div class="card mb-0">
                                 <!--Collapse One Start-->
                                 <div class="shdow-box">
                                    <div class="card-header collapsed" data-toggle="collapse" href="#que_1">
                                       <a class="card-title">
                                       Q. What is DistiMonster?
                                       </a>
                                    </div>
                                    <div id="que_1" class="card-body collapse" data-parent="#accordion">
                                       <div class="detail">
                                          <span class="answer f-s18 black font-w500">A:</span>
                                          <p class="f-s14 text-gray">DistiMonster is a consolidator with intelligence for design through procurement of electronic components. We consolidate inventory available from multiple authorized distributor partners. Whether you&#8217;re designing or buying (or both) DistiMonster is your one source, one PO, and one shipment solution provider. Crossing parts, PCN alerts, personalized dashboard to re-order, all here while you save time and cost placing multiple PO&#8217;s.</p>
                                       </div>
                                    </div>
                                 </div>
                                 <!--Collapse One End-->
                                 <!--Collapse Second Start-->
                                 <div class="shdow-box">
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#que_2">
                                       <a class="card-title">
                                       Q. Does DistiMonster own any inventory?
                                       </a>
                                    </div>
                                    <div id="que_2" class="card-body collapse" data-parent="#accordion">
                                       <div class="detail">
                                          <span class="answer f-s18 black font-w500">A:</span>
                                          <p class="f-s14 text-gray">Absolutely none. All product is coming from the distributors listed in the search results.</p>
                                       </div>
                                    </div>
                                 </div>
                                 <!--Collapse Second End-->
                                 <!--Collapse Third Start-->
                                 <div class="shdow-box">
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#que_3">
                                       <a class="card-title">
                                       Q. What if I get registered or contract pricing?
                                       </a>
                                    </div>
                                    <div id="que_3" class="collapse" data-parent="#accordion">
                                       <div class="card-body">
                                          <div class="detail">
                                             <span class="answer f-s18 black font-w500">A:</span>
                                             <p class="f-s14 text-gray">Please contact a representative at DistiMonster and we can work with the distributors to get your contract pricing to be applied to your account.</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <!--Collapse Third End-->
                                 <div class="shdow-box">
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#que_4">
                                       <a class="card-title">
                                          Q. Do I need to register for an account to access all the services DistiMonster offers?
                                       </a>
                                    </div>
                                    <div id="que_4" class="collapse" data-parent="#accordion">
                                       <div class="card-body">
                                          <div class="detail">
                                             <span class="answer f-s18 black font-w500">A:</span>
                                             <p class="f-s14 text-gray">Yes, you do need to register for an account. No &#8220;level&#8221; for barrier of entry. We give you ALL the prime benefits at one price. Simple. If you register now you will get a 30 day free trial. <a href="<?php echo e(url('subscription-plan')); ?>">Click here</a> to register</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="shdow-box">
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#que_5">
                                       <a class="card-title">
                                          Q. What payment types does DistiMonster accept?
                                       </a>
                                    </div>
                                    <div id="que_5" class="collapse" data-parent="#accordion">
                                       <div class="card-body">
                                          <div class="detail">
                                             <span class="answer f-s18 black font-w500">A:</span>
                                             <p class="f-s14 text-gray">We accept Visa/MC/AMEX, PayPal. Net terms and COD on approval</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="shdow-box">
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#que_6">
                                       <a class="card-title">
                                          Q. Can I submit a BOM or part request direct to a representative?
                                       </a>
                                    </div>
                                    <div id="que_6" class="collapse" data-parent="#accordion">
                                       <div class="card-body">
                                          <div class="detail">
                                             <span class="answer f-s18 black font-w500">A:</span>
                                             <p class="f-s14 text-gray">Yes, you can. Just log into your account, pull up your dashboard, and submit the request. Simple. Once the request is processed an email will be sent to you that the quote is available.</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="shdow-box">
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#que_7">
                                       <a class="card-title">
                                          Q. Can I place an order offline with an account representative?
                                       </a>
                                    </div>
                                    <div id="que_7" class="collapse" data-parent="#accordion">
                                       <div class="card-body">
                                          <div class="detail">
                                             <span class="answer f-s18 black font-w500">A:</span>
                                             <p class="f-s14 text-gray">You sure can. If you have an account set up already your account manager is listed for you with their email address. You can also email <a href="mailto:sales@DistiMonster.com">sales@DistiMonster.com</a> , use Live Chat or call us, we love to talk with our customers.</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="shdow-box">
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#que_8">
                                       <a class="card-title">Q. Can DistiMonster be integrated into our ERP?</a>
                                    </div>
                                    <div id="que_8" class="collapse" data-parent="#accordion">
                                       <div class="card-body">
                                          <div class="detail">
                                             <span class="answer f-s18 black font-w500">A:</span>
                                             <p class="f-s14 text-gray">Yes it can, with some work using our API. Please contact an Account Manager for details.</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="shdow-box">
                                    <div class="card-header collapsed" data-toggle="collapse" data-parent="#accordion" href="#que_15">
                                       <a class="card-title">
                                          Q. I am Franchise Distributor or OCM & I want to list our inventory with DistiMonster.com
                                       </a>
                                    </div>
                                    <div id="que_15" class="collapse" data-parent="#accordion">
                                       <div class="card-body">
                                          <div class="detail">
                                             <span class="answer f-s18 black font-w500">A:</span>
                                             <p class="f-s14 text-gray">Please contact us at 818.718.4100 or you can email <a href="mailto:partners@DistiMonster.com">partners@DistiMonster.com</a> </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <a href="<?php echo e(url('return-policy')); ?>">  <h3 class="tab_drawer_heading" rel="tab2">Return Policy</h3></a>
                        <a href="<?php echo e(url('contact-us')); ?>">  <h3 class="tab_drawer_heading" rel="tab3">Contact Us</h3></a>
                        <!-- #tab3 End-->

                     </div>
                     <!-- .tab_container -->
                  </div>
                  <div class="shdow-box">
                  </div>
               </div>
            </div>
         </div>
      </section>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
   <script>
      $('#chat_link').click(function () {
          MyLiveChat.InlineChatDoExpand();
      });
   </script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>