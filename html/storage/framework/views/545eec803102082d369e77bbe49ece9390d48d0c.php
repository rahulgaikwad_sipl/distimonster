<?php $__env->startSection('content'); ?>
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="<?php echo e(url('admin/home')); ?>"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="<?php echo e(url('admin/coupons')); ?>">Promo Code</a></li>
            <?php if(!empty($coupon)): ?>
                <li class="active">Update Promo Code</li>
            <?php else: ?>
                <li class="active">Add Promo Code</li>
            <?php endif; ?>
        </ol>
    </section>
    <br/>
    <section class="content">
        <div class="row">
        <div class="col-md-12">

            <div class="box">

                    <div class="box-header with-border">
                        <?php if(!empty($coupon)): ?>
                            <h3 class="box-title">
                                Update Promo Code
                            </h3>
                        <?php else: ?>
                            <h3 class="box-title">
                                Add Promo Code
                            </h3>
                        <?php endif; ?>
                    </div>
                    <?php if(isset($coupon) && count($coupon->toArray()) > 0): ?>
                        <?php echo Form::model($coupon, ['method' => 'PUT', 'route' => ['admin.coupons.update', $coupon->id],'data-parsley-validate' => true]); ?>

                    <?php else: ?>
                        <?php echo Form::open(['method' => 'POST', 'route' => ['admin.coupons.store'], 'data-parsley-validate' => true]); ?>

                    <?php endif; ?>
                <div class="box-body">

                    <div class="form-group">
                        <label class='control-label'> Promo Code <span class="text-danger">*</span> </label>
                        <?php echo Form::text('code', old('code'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','minlength' => config('app.fields_length.name_min'),'maxlength' => config('app.fields_length.name_max')]); ?>

                        <p class="help-block"></p>
                        <?php if($errors->has('code')): ?>
                            <p class="help-block">
                                <?php echo e($errors->first('code')); ?>

                            </p>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <?php echo Form::checkbox('is_free', 1, (isset($coupon)&& ($coupon->is_free==1)) ? true : false, ['class' => 'is_free_shipping']); ?>

                        <?php echo Form::label('is_free', 'Its a free shipping promo code'); ?>

                    </div>
                    <div class="form-group">
                            <label class='control-label'> Discount Percentage <span class="text-danger">*</span> </label>

                            <?php echo Form::text('discount_percentage', old('discount_percentage'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','pattern' => config('app.patterns.number'),'data-parsley-type'=>'digits', 'data-parsley-min'=>0,'max'=>99]); ?>

                            <p class="help-block"></p>
                            <?php if($errors->has('discount_percentage')): ?>
                                <p class="help-block">
                                    <?php echo e($errors->first('discount_percentage')); ?>

                                </p>
                            <?php endif; ?>

                    </div>
                    <div class="form-group">

                            <label class='control-label'> Order Limit <span class="text-danger">*</span> </label>
                            <?php echo Form::text('order_limit', old('order_limit'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','pattern' => config('app.patterns.number'),'min'=>1]); ?>

                            <p class="help-block"></p>
                            <?php if($errors->has('order_limit')): ?>
                                <p class="help-block">
                                    <?php echo e($errors->first('order_limit')); ?>

                                </p>
                            <?php endif; ?>
                    </div>
                    <div class="form-group">

                            <label class='control-label'> Minimum Cart Amount <span class="text-danger">*</span> </label>
                            <?php echo Form::text('minimum_cart_amount', old('minimum_cart_amount'), ['class' => 'form-control', 'placeholder' => '', 'required' => '','pattern' => config('app.patterns.number'),'min'=>1]); ?>

                            <p class="help-block"></p>
                            <?php if($errors->has('minimum_cart_amount')): ?>
                                <p class="help-block">
                                    <?php echo e($errors->first('minimum_cart_amount')); ?>

                                </p>
                            <?php endif; ?>

                    </div>
                    <div class="form-group">
                        <?php echo Form::label('start_date', 'Start Date', ['class' => 'control-label']); ?><span class="text-danger">*</span>
                        <?php echo Form::text('start_date' , (isset($coupon)) ? date('m/d/Y', strtotime($coupon->start_date)) : '' , ['class' => 'form-control', 'placeholder' => '', 'required' => '']); ?>

                            <p class="help-block"></p>
                            <?php if($errors->has('start_date')): ?>
                                <p class="help-block">
                                    <?php echo e($errors->first('start_date')); ?>

                                </p>
                            <?php endif; ?>

                    </div>


                    <div class="row">
                        <div class="col-xs-12 form-group">
                            <?php echo Form::label('end_date', 'End Date', ['class' => 'control-label']); ?><span class="text-danger">*</span>
                            <?php echo Form::text('end_date', (isset($coupon)) ? date('m/d/Y', strtotime($coupon->end_date)) : '' , ['class' => 'form-control', 'placeholder' => '', 'required' => '']); ?>

                            <p class="help-block"></p>
                            <?php if($errors->has('end_date')): ?>
                                <p class="help-block">
                                    <?php echo e($errors->first('end_date')); ?>

                                </p>
                            <?php endif; ?>
                        </div>
                    </div>

                    <?php echo Form::hidden('role_id', 2, ['class' => 'form-control', 'placeholder' => '']); ?>

                    <div class="row">
                        <div class="col-xs-12 form-group">
                            <label class='control-label'>Status<span class="text-danger">*</span> </label>
                            <?php echo Form::select('status', $status, !empty($coupon->status)? $coupon->status :'', ['class' => 'form-control select2 status','data-parsley-errors-container'=>'#error_status', 'required' => '']); ?>

                            <div id="error_status"> </div>
                            <p class="help-block"></p>
                            <?php if($errors->has('status')): ?>
                                <p class="help-block">
                                    <?php echo e($errors->first('status')); ?>

                                </p>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>
                <div class="box-footer">
                    <a class="btn btn-default btn-close" href="<?php echo e(route('admin.coupons.index')); ?>">Cancel</a>
                    <?php echo Form::submit(trans('quickadmin.qa_save'), ['class' => 'btn btn-success']); ?>

                </div>
                    <?php echo Form::close(); ?>


            </div>
        </div>
    </div>
    </section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>