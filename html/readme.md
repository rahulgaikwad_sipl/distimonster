# Laravel 5.5 based shopping portal.

#### How it works

It's a shopping portal for electronic items.

```
## Used packages

- [AdminLTE theme](https://adminlte.io/themes/AdminLTE/)

## How to use

- Clone the repository with __git clone__
- Copy __.env.example__ file to __.env__ and edit database credentials there
- Run __composer install__
- Run __php artisan key:generate__
- Run __php artisan migrate --seed__ (it has some seeded data for your testing)

## License
